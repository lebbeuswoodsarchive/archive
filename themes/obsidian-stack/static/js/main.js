//JS wait for DOM to load
document.addEventListener('DOMContentLoaded', function(event) {


  // images loaded - multiple elements
  var content = document.querySelectorAll('.article-grid');

  if (typeof(content[0]) != 'undefined' && content[0] != null) {

    imagesLoaded( content, function() {

      var elem = document.querySelector('.article-grid');
      
      var msnry = new Masonry( elem, {
        // options
        itemSelector: '.default-grid',
        columnWidth: '.default-grid',
        percentPosition: true,
        gutter: 15
      });
  
    });
  }
  

})

