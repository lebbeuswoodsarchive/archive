
---
title: MEASURING LIGHT
date: 2012-04-30 00:00:00
---

# MEASURING LIGHT


## 


[![](media/metrinstr2a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr2a-2.jpg)


*(above) Light Metrical Instrument. Designed by LW, made by LW together with architect Leo Modrcin, c. 1987.*


It has long been my contention that light does not reveal the presence of objects, but the other way around: objects reveal the presence of light. This flipping of the phrase is more than a trick with words, though it may seem like that at first. What it does is shift the emphasis of meaning. If light reveals the object, than it is the object that is important. But if objects reveal the light, it is the light that is important. In other words, the simple turn of phrasing changes the relative values of objects and light.


For me, light is the main thing.


Light is a natural physical phenomenon the complexity of which reveals the structure of human consciousness. Objects, including buildings, in their absorption and reflection of light, stimulate a human brain's neural networks, in effect activating the brain. The more complex and nuanced the stimulation, the more fully the brain comes to life.  Shape, edge, texture, color, shadow, highlight, playing with and against one another, effectively enable the brain to make the most subtle distinctions, thereby imbuing human experience with a richness and complexity that defines it.


Whatever else is involved, the perception of light is central.


*(below) Details from the Light Metrical Instruments series, 1987. If we can think of architecture as an instrument revealing the presence of light and therefore giving it a precise measure, then these instruments are proto-architectural. With their richness in variation of shapes, colors, textures, they inform the design of more complex programmatic structures.*


[![](media/metrinstr7a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr7a1.jpg)


.


[![](media/metrinstr5a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr5a1.jpg)


.


[![](media/metrinstr6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr6a1.jpg)


.


[![](media/metrinstr2b2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr2b2.jpg)


.


[![](media/metrinstr4a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr4a1.jpg)


.


[![](media/metrinstr7b1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr7b1.jpg)


.


[![](media/metrinstr4b1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr4b1.jpg)


.


[![](media/metrinstr2a-31.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/metrinstr2a-31.jpg)


LW




 ## Comments 
1. [Cristian Pandele](http://durere.wordpress.com)
4.30.12 / 4pm


Every time I see you making a physics-related statement (albeit always more philosophically-leaning), I reflect on it for a couple of minutes, thinking ‘this can't possibly come from an architect', only to realize seconds afterwards that there are probably only a few humanities studies more dependent on the ability of the ‘practician' to play with physics. Manipulating light might as well be the principal activity in the work day of an architect, from creating the impression (or the illusion) of wide, open spaces to — quite the opposite — proposing dark, oppressive chambers…


More to the point of this post: I wholeheartedly agree with your statement (that it is the objects revealing the presence of light). In strict scientific terms, it is always dark in the interstellar space, as there is nothing to bounce the light towards you (and your eyes). Sure, you will see the stars, but your immediate proximity will be completely dark. I am honestly not too keen to experience this sensation, as the absence of nearby sources of light (be they particles of dust or, literally, anything) must be the most depressing thing one can experience. Truly an experience devoid of any content or meaning…


Might I add a personal touch to your theory: objects are as able to reveal the presence of light, as they are able to withhold the presence of colour. As light bounces of the surface of an object, it absorbs portions of the light's spectrum, presenting us with only what escapes the collision between the light ray and the object. The ray of light is like a snake shedding its skin as crawls from here to there… Then, I ask, is an object's true colour what we see (what escapes it), or quite the opposite (what it absorbs)?


Thank you for challenging what we believe to be banal about the way we perceive the world around us.
3. dina
5.9.12 / 8am


It reminds me of washed up sea-shells. Whether that sounds bad or not. It's pretty cool~ I wonder what it sounds like
5. Pedro Esteban
6.14.12 / 4am


What do you mean with proto-architectural?
7. [David Zilber](http://recidivism.ca)
6.22.12 / 4pm


If you think like this, this book was made for you.


<http://www.amazon.com/The-Speed-Light-Constancy-Cosmos/dp/0253220866>
