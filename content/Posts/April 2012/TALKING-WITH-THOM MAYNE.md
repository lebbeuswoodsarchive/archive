
---
title: TALKING WITH THOM MAYNE
date: 2012-04-23 00:00:00
---

# TALKING WITH THOM MAYNE


[![](media/portrait-of-thom-by-reiner.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/portrait-of-thom-by-reiner.jpg)


*(above) Thom Mayne, 2011. Photo by Reiner Zettl.*


## **Excerpts of a Candid* Conversation between Thom Mayne and Lebbeus Woods**


**Recorded in the privacy of LW's studio, transcribed by Dave Irwin**


*Note: a minimal amount of editing has been made, in order to preserve the spontaneity of the occasion.*


*****I do pay attention to my critics. In another forum, my use of the word “candid” was dismissed as disingenuous, I suspect because my comments to Thom were too supportive of his work and thought. It is true that I find little to quarrel with and much to be excited about in his recent book on architecture and urbanism. Still, I contend that agreement doesn't preclude a deeper exploration of ideas, which I think the candid talk between us did—indeed it might be necessary for it. I thank my critics and readers for enriching the conversation with their comments, pro and con. [LW]


**GROUNDED IN ORGANIZATION**


**LW:** OK. But the truth that you're seeking is a truth about the nature of how things come into being; how things grow, this idea of their morphology. The idea of how do things come into being in the form that they are.


This is what concerns you. It is not whether there is God or no god—that has nothing to do with your discourse. It has to do with how do things come to be what they are. That is an incredibly important question.


So you are applying it to a kind of world of formation and someone like [Rupert Sheldrake](http://en.wikipedia.org/wiki/Rupert_Sheldrake) would be a great….


**TM*:*** *Evolution of Evolution* – Sheldrake, that was about 20 years ago, but absolutely.


**LW:** His idea of “morphogenetic fields.' How do the same cells containing the same DNA ‘know' to become a nose, while others become a finger. OK, but the point is– these are questions that are still undecided, even unexplored.


So what you're doing is exploring them. You're an architect and you want to bring them as kinds of gifts into the real world, so that we can experience them. Your Shanghai [[Giant](http://morphopedia.com/search?q=Giant+campus&f=)] project begins to suggest that we actually can use these ideas of truth and beauty to create a world, an actual world, a world we can live in. I see the people active in the spaces, swimming, laughing, playing….


**TM:** But that's a leap, [these new models](https://lebbeuswoods.wordpress.com/2010/11/25/thom-maynes-mind/) we're making are conceptual territories. Like the last one I've shown you.


By the way, I've been doing this with one person in my office very quietly. The drawings and study models keep showing up on the walls, and it's really fascinating– the office keeps looking at the walls saying, *‘What's going on?'*


**LW:** *(laughs)*


**TM:** Finally I've brought in the key characters I work with and we talked about the drawings and we're using them as a broad direction that we are aspiring to. The drawings are setting up more specific notions of desire, let's say.


Desire precedes everything we do, right?


**LW:** Of course.


**TM:** It's trying to give us something more grounded in organization.


**LW:** I think you're looking for a way to proceed towards a *profoundly* *meaningful architecture. Beyond* style; beyond any kind of idea of beauty or ugliness—


You are looking for a way to move forward with architecture. And of course you're going to go to strange places. These models are already strange places because we haven't seen them before.


But whether they have any applicability to architecture is the question.


However, your Shanghai project does begin to show that your way of thinking and working may have a broader impact. I think [your book on urbanism,](http://morphopedia.com/news/combinatory-urbanism-the-complex-behav-1) which I guess is….*(Thom shows Lebbeus his book)….*Ah, there it is!


**TM:** Two more months and its out!


**LW:** OK. I'm not sure how well it will argue—


**TM:** This is more formal. That's one part of my world *(in reference to his latest work).*This is another part of my world *( in reference to his latest book on urbanism).*


*(laughs)* I'm very stretched out right now. I'm trying to hang on to all these different pieces!


**A NEW WAY**


**LW:** Your message is really a philosophical message to architects. You're trying to show us how we can build cities and break out of the old modes of urban planning and urban design.


And even thinking about or imagining cities that we have had for the past few hundred years, you're offering a new way. I don't know anyone else that's done that today. Maybe someone will say Colin Rowe. OK—*Collage City*, but this goes far, far beyond *Collage City* and any urban theory of Corbusier or anyone else.


Now—how do you state your case? Your publications, your lectures—but I think it's still something people will have a hard time approaching because they look at these and I say, *‘Hey, wow they are so amazing, however, these are arbitrary artistic constructions, the guy is a brilliant.'* Whatever. People want to put it in the mold of something already known.


What you're trying to explain with [*Combinatory Urbanism*,](http://morphopedia.com/news/combinatory-urbanism-the-complex-behav-1) your book, is part of a systematic, philosophical, bigger picture. That is what philosophy is, systematic thinking, and you're doing it.


**TM:** If you're exploring just the level of organizational types because that's usually what we do—we organize the world and we literally concretize it. We bring a certain order. That order is a response to how we locate ourselves in the world in philosophical terms—in the total sense; all the disciplines….


I'm looking for…or I am challenging…or I am myself searching for what that means with those organizational forms. I am looking for something that I see as more contemporary and that is connected to the 21st century thinking and it seems as though it's moving from the mechanical to the biological. There is a thrust that is becoming clear to us in terms of where we look for our ideas, right? We are paralleling in terms of the modern notion of who we are.


I'm looking for something that is discursive, that is understandable, ihat doesn't depend totally on personal facility, meaning it's private—because these are collective projects. I work with another person that is actually drawing the specific nature of the model.


And I would have said that the formation of these are first and foremost the operational strategy, which is the most powerful thing. Then, after that, it's some combination of my critique; my development of that process and my engagement with the person actually mechanically putting them together. So it is a very complicated, already collective process of even how they come together and it's not by me at all.


**LW:** Well, that brings up another question….


**TM:** I'm changing personalities and actually seeing where the personality fits in—and it's less than you would think, strangely enough.


It's more about the critique of where they're going *(in reference to a drawing / model).*


Each one of these has taken a couple weeks and we go through many, many reiterations to find the one we think is working and I could define the rules in terms of differentiation. For each one of these, there is a discourse.


**LW:** So, any iteration won't do. There are some iterations that are better than others?


**TM:** That is still a messy territory because again I look at this and there is no notion—they are a messy dynamic—and they operate on just about an infinite scale and if I actually look at the thousands of specific engagements going on here, there could be, as far as I know, an infinite number of solutions.


That is another question I am asking—in terms of specificity, is there a single rule? I say no, it is incredibly relaxed. But then I'm saying with program, building a Giant (the Shanghai campus)  instead of having a very fixed program. I started my career looking at symmetries, the kind of collage with *Lawrence House.*


Everything was gauged on a certain form. If anything was moved—it would be Kahn vs. Le Corbusier.


At Exeter, *he's screwed*. He has a strategy that is absolutely impossible to do within reality.


And he's stuck with the symmetry and so one corner is a stair and the other is a stair and a closet, you know, that kind of thing—impossible to solve.


Corbusier early on figured that part of the infrastructural piece was fixed and then there were other elements that were lax and loose and they followed his language—such as the stair.


There was a relaxedness that allowed him flexibility in the reality of human engagement that requires change.


In the Giant project, people can make all kinds of changes—I don't even care. We just move it around—it's open and flexible. I'm not depending on any fixed idea of form, which no longer makes any sense in a world like ours that's operating in a completely different sense of time than ever before. We're undergoing continual change in our architecture. if you are looking for an organizational type, it needs a type that allows for a flexibility that is not to the detriment of the final artistic demand of the project.


So Giant very much says that. I can move things around and will it be the same project? Yes. Would it be different in a literal sense? Of course. But if I move these things I have a huge flexibility that allows me to go in the same broad direction.


**LW:** Let me ask you a question with the subject of collaboration with these constructions—these models. How do you factor in, or how do you consider social issues that impact the city in your methodology—in your process? Because I am sure that's a question that will be asked by people that start to say; *‘city'*, they will then ask, *‘well what about the rich and the poor, what about social injustice, what about…'*


**TM:** Architecture can't answer those questions—that's early modernism. I have no belief that architecture can solve those problems. Those are not architectural problems—those are political, social and cultural problems.


It's an interesting question at this very moment in time because as we've shifted our agreement with the basic notion that starts with empathy and an agreement of what society should be and what this country should look like in economic, social and cultural terms. That agreement has shifted radically since the 1980's, say, since Reagan. We are now approaching an economic disparity closer to Guatemala and there's somehow a comfort with that. As an architect, this is something I am extremely interested in but I cannot solve that problem.


In fact in today's world as you know, as architects, if you are of that ilk that are still part of the middle of the 20th century as an ideal, you're kind of fucked.


I've done one project with social housing and that's in Spain where that is still a problem and they still engage in that issue. But that is not a problem of the US., it starts at a political level, it's not an architectural problem. it's definitely a problem that architecture can engage with and participate in, but it's not an architectural problem.


**LW:** Your work, let's just say that the ideas—I'm just being the devil's advocate again—let's just say the ideas that you have been talking about, this *Combinatory Urbanism* and these different systems that get integrated, they will impact people's lives—hugely. Any urban shift, any change in the way things relate to each other opens public space; landscape; nature; housing; private spaces; administrative spaces, all of these have an impact on how people live, right? There's no doubt.


So is it possible, that in the future your approach exhibits, not only in the models—and your approach to the models that I would call exploratory urban constructions, but also to projects like Giant—If they were applied to broader urban conditions *could* more people participate in the process?


**TM:** Absolutely, it sets up a frame work that is both more fluid than I'm talking about and it sets up one that's interested in complexities that can deal with multiple constituencies—I thought you were going someplace else…


**LW:** You got the drift of my question right?


**TM:** Absolutely, I thought you were going to go somewhere else.


**MATERIALISM IS WANING**


There was an interesting article that I caught a couple of days ago in the *New York Times* it was David Brooks discussing the shift taking place in the young generation—that there is a complete shift going on in the materialistic demands of our generation. And what is taking place with the facebook and the google generation, is that they're realizing—and he's justifying the lower standard of living taking place with the next generation—that it's actually OK.


It's a really interesting argument that they're moving out of the materialist mode that all of us have accepted and they're moving into an information and quality of life mode. I read it on a plane and I was tired, but I have it in my briefcase now and I will go back home and read it—but the discussion is going to be great news for intelligence in general and for artists and architects because the members of the new generation are going to be more and more interested in—and they have more room to explore—their interests and intelligences. They're not just collecting things and they need access to information.  They both have more time and they're defining the quality of their life not by material things, but by the access to things that interest them; things that expand them in human terms—a really interesting argument, right?


Instead of everyone going, *‘oh dear, the sky is falling and we don't get what our parents get'*. Instead, they're questioning, *‘what does ‘get' mean'*. Does that mean only these material objects? No.


It's actually a kind of relief to be out of this materialistic system that is hopeless.


**LW:** More people are saying, “I'm burdened by too many credit card payments to pay for all this shit, you know?”


**TM:** Yes – and it actually frees you for something more interesting, that's good news isn't it?


**LW:** It could be—


**TM:** But meaning that the potential, from our point of view, is that they're going to be able to live a more creative life versus being burdened.


**LW:** Now we get to the idea of education. The whole education system and including our mass media; that is—part of the people's education is what they see on TV and now on the internet—pretty much has established materialism and consumerism as a standard of the good life.


So, it's only through education of younger people that they're going to understand what you're talking about, which is that a good life doesn't require all these material possessions. It actually has more to do with the time that you want to spend doing the things that matter to you and that you care about. Not the time you spend earning the money to buy those material things.


**TM:** Or doing a job you hate, just to have the objects—


**LW:** Because our generation, certainly many people in our generation, took jobs for big corporations that paid them very well and they could buy all those things. But they hated their job, they didn't get to read, they didn't get to play around with paints. They didn't get any of that time because they were devoted to earning money to pay for the possessions, so we know that…


**TM:** And the huge, *good* news is that it changes from status to enhancement, right?


**LW:** OK, so all of this has a huge social implication, a changing of social values and it starts with a younger generation; it doesn't start with our generation. People in our generation or even people in their 40's or 50's are going to still be stuck in that old syndrome, most people.


So, it's a process. Now I'm intrigued by your idea of urbanism. You're not satisfied to make this a kind of artistic process by which you can produce masterpieces of art or of architecture. You have the aspiration for this to mean something for everyone. You talk about cities, and they are everyone, every social class—every category of human being. So your ambition for this to mean something for cities seems to be an incredibly significant part to what you want to achieve. How do you want to go about doing that? How do you hope to have an impact on the growth of cities?


**TM:** I guess at that level it's going to be quite different. I'm quite the pessimist or realist.


You know we just came out with this urban book and we just did another project in Qatar, a competition—let me tell you the Qatar story real quickly.


We were up against Rem and Grimshaw and it's a huge piece of land next to an airport that's three miles long. They gave us a program– the stuff of a city etc. We did a study of Doha and it's pocketed with empty space, it has a 6 month summer of 120 degrees (f). There was a question of should there be a city there? Number one– it would be the same issues of Phoenix or Tucson; and why they're growing and if they're sustainable at all—will they make economic and ecologic sense in 50 years from now?


The assumption could be no and it wouldn't take long to prove that these are odd places to be building large cities and the fastest growing cities. We looked at this and decided that we need to base our project on certain agreements. The first agreement that we made was that we're going to contain the city and we're not going to continue the growth. And they're in the middle of building huge infrastructure, subways and things…


With the subways you want to know where you will be 50-100 years from now because they're expensive to maintain and if you can build half the subways it seems like it would make more sense. So our discussion is, why don't you contain the city and have more growth then you would need for 50-100 years and that will be the first assumption. We looked at our piece of land and said, well we wouldn't even build there.


So in the beginning our response to the competition, which we are getting paid for and we're going against two good architects, is we are challenging the *a priori* notion of the whole project. Why would you even assume that we're building here? We came up with a scheme and we're pushing all the developed part that can be condensed to a third of the whole site. We pushed it to the water and we took the whole site and said, well what do we want to use it for?


We came up with an idea for hydroponics—a huge, huge scale of produce production and went to work with a whole team and created a system where we can produce 75% of all the produce for the country.


**LW:** Amazing.


**TM:** These are very practical problems. I'm interested in large scale problems of policy and complexity.


We then realized we might have—I'm going to know in a couple of weeks—presented to the wrong group because they're architects and urban people—they just wanted to look for a plan, which both Rem and Grimshaw gave them.


I am interested in another level of first-principle stuff and in some way these are very, very limited aspirations and they're in some way provocations. We did the same thing in New Orleans; we said why don't you just abandon the lowlands and move them. We did an economic study again with *RAND* that it's cheaper to do that then to build a levee. It gave them all kinds of reasons, economic, social, etc.


These are very real problems but they're at the macro-scale and they require big thinking and that's not where we are at this time in history. We're in an incremental time in history.


I'm kind of disappointed and astonished with what's going on with the current [Obama] administration. Saying *‘thinking out of the box'—*we know what ‘thinking out of the box' really means. I don't want to be bullshitting about *‘thinking out of the box'*. That's what I do for a living, right?


**LW:** *(laughs.* Bullshitting or thinking out of….


**TM:** So, I'm going to take you at your word when you say *‘thinking out of the box'*. I say, OK—I got it, let's go for it, right? And it's precisely needed at this time in history. We have to change radically. It is such an interesting time.


There was an article in the *New York Times* about…


**LW:** You have to finish your sentence. You have to change radically, what?


**TM:** It's the perfect time for radical change in how we deal with basic problems such as city- making, if they are going to be economically, ecologically and culturally sustainable. It's that simple.


You can't open a paper today that's not discussing the fascination about where we are—and we're in this middle space where these huge changes are taking place socially, economically and politically.


There was an article in the *Times* on people that check Caucasian, Black, Asian- other. Did you read that?


**LW:** Yeah, the census.


**TM:** Instead of having *other*, they have Irish, Mexican, Puerto Rican– so you're checking multiple boxes.


And different governments have different check boxes, so you don't always get the same check boxes.


The discussion was, this is a completely irrelevant means of information in the way we live today and especially in this country which is made up of heterogeneity. It's the culture that invented the global culture, and you cannot say that about China, Japan, etc.


But this is a country that bases its essence on our discovery, our invention of the global culture and has been since the beginning of the first explorers.


Those types of issues keep appearing in everything that you talk about today. We are in this place of having to redefine and reconfigure the basic notion of who we are at every level and of course with architecture. City-making is especially part of that conversation. We have to completely reconfigure how we deal with problems. Nobody really wants or is interested in having that conversation today.


**LW:** Well I think they are. But I think they're afraid of it. I think many people in our field are, but I think the public is as well. They're afraid of it because they themselves don't have the answer and they don't see too many people trying to get the answer, so it's a question of avoidance.


Here is an architect, you and your colleagues' firm, *Morphosis*—a collaborative, that is approaching this and I think it's significant. Whether any of what you are doing makes any sense at all will have to be judged by many people. But the very fact that you are doing it suggests that it's worth the discussion.


So, your new book is published and will be out, It needs to be read. Whether this book reaches its audience, we'll have to see. I think the audience is architects. Architects need to discuss these issues that you raise. It's not to say yes or no to what you think. It's more to deal with the issues you are raising by your work.


So, I think in the big picture, the strongest thing you are doing is raising the very issue about what principles we are going to operate on in the creation of these mega-cities that are growing over night, on our planet as the population expands exponentially.


We're not just talking about adjusting a few boulevards in Paris or London. We're talking about building cities that are growing very quickly.


**TM:** And the project we were looking at—your Light Pavilion project with *Steven Holl* in China—if I remember right, that's a three-million sf. project. A single project, by a single architect done over a short period of time and I'm interested in the complexities; spatial and organizational complexities you could bring to that problem which are quite mimetic of the historic city. Maybe more the pre-modern city actually, that is loaded with the more organic fabric, which represents a richer differentiation that took place with the universalization process of modernism which was interested in stripping away—In fact I am interested in the exact opposite; in putting back all of the idiosyncratic, all the specific, the accidents, the unpredicted.


**LW:** The accidental?


**TM:** They are the *human*, I would say. They give us this endless occupation with the complexity of the city over time developed by many, many people.


The most complex construction of the human character possible is the city, and then we're talking within design terms, within urban terms, within social and cultural terms.


And of course we're right in the middle of it. Here in New York we're in a sea that's endless in its possibilities.


**LW:** Yes, but our city, New York is slow and small compared to cities in China or South America that are expanding literally overnight into the mega-millions because of migration. Or, in Africa, you look at cities like Lagos that are just expanding over night by literally the millions and mostly end up being slums because there is no thinking on the part of the authorities, the government, the planning agencies, about how they will exist.


**TM:** But you look at China and Shenzhen. That's a 20-year-old city, a fishing port that now has somewhere between 12 and 20 million people—I'm not sure—who knows? It developed in a completely *ad hoc* way. There is no structure. It just happened.


**LW:** In the best of all possible worlds the planning agencies, the authorities who govern the land and how it's going to be used, have a system of law and of government in China. If they had a strategy; if they adopted just as a scenario, your strategy, right? Let's just say they adopted something that you had offered as an overall strategy as *Combinatory Urbanism*—


Then as a principle the city would become something different than an add-on or endless accumulation of random buildings; there would be a process, or how you put it—an *organic process.*


So, that's the possible potential impact of the kind of thinking that you are proposing for cities. It would be very easy for people to just look at your work and say, “*It's just amazing, or awful, Thom Mayne stuff.'*


But it's your desire to push it further into the realm of an urbanism that would involve many. *That* makes it extremely dangerous, controversial, and hopeful for me. I believe in what you're doing in the conceptual sense, but not everyone will, and this then is the battle to be won.


*Recorded February 17, 2011*


.



 ## Comments 
1. Aldorossi
4.24.12 / 5am


How ironic that the materialism Mr. Mayne seems to resent in fact fuels the chaotic growth that his urban designs seek to accommodate.


Mr. Mayne says that issues of social justice can't be answered by Architecture. You then suggest that his desire is to push it further in to the realm of an urbanism that would involve many; a contradiction, I think.


The control required to realize the coherent urban manifestations posited by Mr. Mayne would rule out the participation of the many. Quite frankly, that authority exists, in China especially, now. And conversely I don't think he has made the argument that there is a means in his approach by which the “Many” would participate.


The process; the “strategy” of Urban Planning will either be a result of or an imposition upon a system of social justice. You cannot decouple the two.
3. [m.byrne](http://byrmartin.wordpress.com)
4.24.12 / 10pm


a very minor point in an otherwise very rich discussion – the waning of materialism. while i am forever loathe to agree with david brooks, he may be correct, but its not necessarily in the same vein that mr brooks means it, i think. in my opinion, the reason the younger generations [of which i am a part] are less interested in materialism is that they realize that they CAN'T be. ecological disaster has been forced down our throats since we've old enough to swallow information – so to imagine to continue the trends of consumption is pure lunacy. not to mention on top of the massive ecological debt we are inheriting, we've also been corralled into piling on our own personal debt that is literally devastating. mr. woods almost got there with the education comment, but i think that both mr woods and mr mayne are not fully cognizant of how painfully shackling these debts are. no, we may not work jobs we hate to buy shit we don't need, but rather work jobs we hate to pay for the loans we took out to get the education for the jobs we can't seem to get.


now, beyond that minor [maybe not so minor?] point, i think the most fascinating topic was that people are afraid of and avoid discussing how we deal with the real problems. this is fascinating because of mr mayne's seemingly contradictory attitude towards architecture and politics. at first he mentions that architecture is capable of solving these problems, but then later gives the example of Doha, where his *discussion of the real problems* completely changed his submission for the competition. so perhaps the question is not to discuss or not to discuss but to realize when we are actually discussing the real issues and when we are fighting straw men. for the most part, mr mayne does attempt a discussion of real issues but for some reason doesn't want to admit it. why is that? 


is it a dance around the real issues because we can see the specter of the early failed moderns leering at us from behind our adversaries? or is it that we are actually discussing the issues in ways that are much more robust than the early moderns; we fear shaming the names of our forebears? perhaps we're only able to gain this clear perspective because we're operating in a late waning of american capitalism?
5. [Conversations with Lebbeus Woods and Thom Mayne « Just Urbanism](http://justurbanism.com/2012/04/25/conversations-with-lebbeus-woods-and-thom-mayne/)
4.25.12 / 4am


[…] Conversations with Lebbeus Woods and Thom Mayne. Share this:TwitterFacebookLike this:LikeBe the first to like this post. Tagged architecture, […]
7. [metamechanics](http://metamechanics.wordpress.com)
5.7.12 / 3am


if you participate fully in the practice of architecture in the world that is natrually policitcal there is nothing contradictory in what Mayne is saying, he is right. the Doha project further makes this point clear, he didn't really look at it as an architect, he looked at as a politician and developer and then he tried to present an architectural solution. 


the waning of materialism was predicted I'm pretty sure in one way or another when people deemed this the “Information Age”, this isn't a suprise to anyone who grew up trying to invent websites that could make you millions, long before facebook. now it's not about websites but rather social apps (another way of saying computer programs that enhance social connections). and being part of the Napster generation, all information should be free. in short the – materials are information now, and it should be free, and can be made free easily.


I read somewhere in a NY Times like article that Urban Planning has become the world of Landscapers and not architects for many reasons.


With regard to the information age and what I interpret from Mayne's conversation, he may be trying to take Urban Planning back and his approach is less traditional architecturally – departing from Parc de La Villette (Tschumi) – slightly agreeing with LeFevres point that architects language isn't read by the inhabitors – sounds like he is creating systems that create systems based on human behavior in a free society (not a socialist, a free society, one that organically evolves – like american capitalism did). architecture as a social network app.


(Mayne in appearance always reminds me of Steve Jobs)
9. zale
5.7.12 / 6pm


i saw an interview with thom mayne and charlie rose. he can really get into the conversations. it's fun to watch him talk
