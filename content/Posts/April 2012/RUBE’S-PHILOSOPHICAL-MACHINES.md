
---
title: RUBE’S PHILOSOPHICAL MACHINES
date: 2012-04-18 00:00:00
---

# RUBE'S PHILOSOPHICAL MACHINES


[![](media/rb-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rb-6.jpg)


Ever hear of Thomas Edison? Well, I would hope so, considering he was the greatest American inventor, ever. Ok, how about Henry Ford? If your answer is ‘yes,' it is no doubt because of his giant automobile plants worldwide, but he, too, was a great inventor, most notably of the ‘assembly line.' So, how about [Rube Goldberg](http://en.wikipedia.org/wiki/Rube_Goldberg)? No? Well, no big surprise. Goldberg was also a great inventor, but not in the same way as the others—he invented what should be called absurd machines, that is, devices that performed the simplest tasks in the most complicated ways, making them commentaries on and critiques of machine technology that, at the time he was making his work, was transforming both the human and natural worlds.


Goldberg's preferred medium of publication was the cartoon. His work appeared in daily newspapers throughout the U.S., reaching millions of people on a daily or weekly basis, providing them with laughter, to be sure, but also subversively undermining the credibility of new technology that business interests were working hard to sell, literally, to the American masses. His not-so-distant cousin was the Murphy who formulated Laws such as “if something can go wrong, it will.” He was a pessimist in the guise of an optimist, believing, it seems, that his inventions were a boon to humankind and that they could actually work—not so different from his optimistic counterparts in business and industry.


There is one aspect of his inventions that remains in advance of most technological devices today: their integration of the artificial and the natural. In many of his machines singing birds or random gusts of wind work together with levers, pulleys, and clocks to produce a desired effect, mixing chance with mechanical certainty, indeterminacy with predictability. Philosophically speaking, he achieved a higher level of invention well worth striving for today.


LW


[![](media/rb-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rb-1.jpg)


.


[![](media/rb-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rb-4.jpg)


.


[![](media/rb-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rb-5.jpg)


.


[![](media/rg-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rg-2.jpg)


.


[![](media/rg-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/04/rg-3.jpg)



 ## Comments 
1. [kkoense](http://kkoense.wordpress.com)
4.18.12 / 8pm


I love the Rube drawings and sentiment. Have you ever heard of, or seen this book before – <http://www.amazon.com/Teach-Your-Chicken-Trevor-Weekes/dp/0898157404> – it's wonderful, and very similar to these Goldberg inventions. I had to do a thorough review, just because it was presented in such a convincing fashion.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.19.12 / 12pm
	
	
	kkoense: Glad you did—thanks!
3. [metamechanics](http://metamechanics.wordpress.com)
4.19.12 / 12am


Glad you are back, was going brain dead just plain working…read the two (2) new posts and comments back on the memory page…


Key concepts for me: process, memory, why, how


Process of architecture or any other visual design is something schools of design and architecture spend a lot of time on (or even waste). The high road theoretical approach to process in architecture is very similar to rube's cartoon's above. From an industrialists stand point – an absolute waste of time and highly unproductive. The cartoons are humorous because they are absurd enough to force someone to ask the question ‘why' as they laugh at the ineffiecent ‘how'. 


Let's be rational and scientific about this:


If the final product or point is desired, then why develop methods for arriving there when thru fictional memory (imagination) you could envision the final product or point.


Perhaps we can't envision the final product (your previous collider post). Then the method for arriving must be developed and the method with the fewest steps is always the winner.


Why bother studying process over and over especially in a Rube way?


If you can't form memories of the future then process is required and representation of what you can't remember (imagine) is also necessary, but inefficiency isn't really required or desired.


If you need other people to arrive at the same memory somehow, but inefficiency still makes little sense here.


If you want to suprise yourself, throw in some analog method of calculating and remove the digital and predictable ineffiency becomes welcome.


If creativity doesn't appear in the form of memories but rather requires time like the production of music to create the final product ineffeciences could be beautiful.


—–


Let's say creativity appears in the form of solution or final product as a vision or memory. John Fogerty sings “let me remember things I don't know”. God gives you a vision. Let's say that's how architecture or a visual art or even a song/sound becomes created?


What question do we ask ‘why' or ‘how'?


The usual response is ‘why' and from there the silly reasearch into process starts. To paraphrase a silly statement Gehry once made – I am trying to find the right question to ask and then the solution – hence all the models. To the industrial mind – I'd ask for my money back, if you don't understand the question then you sure as hell won't understand the answer, right?


Well, no, eventually everyone will understand the question and answer. Hence one requirement on an IQ test is time, how fast you can get the right answer. (Industrial isn't it?)


So as design IQ would rationally follow, those enthralled with process because they think ‘why' is more important than ‘how' actually have design learning disabilities, and by industrialized standards should not be practicing anything creative.


Why hire an architect who spends hours trying to solve a design problem when there are other architects forming memories of unbuilt architecture in split seconds?


Well by industrialized capitalistic rational effiecient honest factual without meaning standards – the end result is all that matters and the faster you get there the better you are at what you do, the rest is just your opinion (the for why).


—–  

You could assume by my dislike for process I am a fake memory creator, no pencil and paper necessary and no reason why. Lebbeus as I had mentioned in your dream post on fellini I started modeling my dreams since they are fake memories I don't conscioussly intend.


One part I am modeling now is a gaming table, a mix of air hockey, pool, and random topography. The random topography is a direct collaged image of the cover to Bernard Caches book – Earth Moves. A book I read over 12 years ago.


Silly me, I asked the why question and starting reading that book again.


What I discovered was the following about Cache and Deleuze (cache attended deleuz lectures and later deleuze referenced cache)


-Actualizing the virtual-. (That's the real discovery)


A dream or a fake memory (imagination) falls under virtual and the actualization thereof requires two questions to be asked – why and how.


I have been fairly unproductive asking ‘why' and have discovered by asking ‘how' more paths of creativity begin to appear, unimagined solutions, the process of asking how to a fake memory has only made more fake memories, things I have never seen before or dreamt.


So is ‘why' really that important? Or another way to put it – is meaning that important? Why ask why when you can ask how? And is that the line between science and art (I presume this a theme in your posts)


From a blackberry





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.19.12 / 12pm
	
	
	metamechanics: Any chance you might do a guest post, as we once discussed here, on your computer-visualized dreams?
5. [i.slover](http://islover.wordpress.com)
4.19.12 / 9pm


I do not tend to make such a clear distinction between ‘unconscious' and ‘conscious' nor ‘fake' and ‘real' nor ‘virtual' and ‘real'. Dreams are thoughts that occur with reduced sensory input. Sensory input produces certain intensities in the human which manifest in thoughts and actions. While sleeping sensory inputs are limited by virtue of the eyes being closed and the body remaining reclined. Thoughts might be said to occur while sleeping, though the thoughts are among recordings of prevoius sensory inputs because “present” sensory input is decreased.  

In regards to ‘Metamechanics' dialogue about process and efficiency, I am quite concerned with these ideas. Your axiom that addresses ‘efficiency' can be expanded. The manner in which you have correlated the efficiency of an architect's process linearly to the efficiency of the production is a bit narrow in scope. It could be that the most circuitous design process produces the most efficient design artifact. In this case the client would pay the designer to discover the most productive design artifact, which is not necessarily produced by the most efficient ‘process'. In an expanded dialogue I would articulate this concept with the idea of nested hierarchies.  

 I agree that there is no shortage of designers who are willing to offer “memories of unbuilt architecture in split seconds”. Within the design process and the industrialization of such a process those that can offer instantaneous propositions are reducing the cost and duration of design, although there is no logical correlate that the solutions these designers might be offering will return more or less profits for the client. The client is searching for a balance between the cost of the design and the returns they will receive from the construction. It is a delicate balance in which the designer is able to ask a set of original questions relating the the present proposal, which will occur over a duration and accumulate cost, and the willingness of the client to pay for this type of questioning which might reveal certain systems of organization that will produce even more value than the client invested in the design process.  

I likewise look forward to seeing the “visualized dreams” of metamechanics”.





	1. [metamechanics](http://metamechanics.wordpress.com)
	4.20.12 / 11pm
	
	
	Mr woods still working on it (I can't bill for dreams, hence further down the priority list).
	
	
	“Visualized dreams” – this is an incorrect description regarding what I am doing and this misunderstanding is a key philosophical point related to “actualization of the virtual”.
	
	
	To visualize a dream in the computer using 3d modeling and rendering software requires building the objects first with real dimensions and proper spatial organization, finding the right materials to apply to the objects, and adjusting the lighting so that the computer render output matches the vision (memory).
	
	
	To actualize the virtual you reverse engineer the vision.
	
	
	I.Slover your description of architectures value is appreciated, but I think the future is unfolding very differently for us.
	
	
	Rubes cartoons are interesting ways to reverser engineer an action or common process. Its further worth noting by reverse engineering as Rube does interesting assemblies of components lead to what appear to be inventions, although mainly useless given the inefficiency.
7. FH
4.20.12 / 3am


You might also be familiar with chindogu, a sort of Japanese absurdist design-art very much in the spirit of Goldberg, though maybe more or a parody of certain aspects of consumerism than of mechanization itself:  

<http://en.wikipedia.org/wiki/Chind%C5%8Dgu>  

Worth doing an image search of, in any case.
9. [StratisMort](http://arcsickocean.wordpress.com)
5.3.12 / 10am


based on the same idea i saw recently this video <http://vimeo.com/40539993>, filming a kind of Rube's style machine in a suitcase and how it works. We often see similar works produced, with extraordinary mechanisms of turning a page and playing with the idea of time, but i'd actually say that Goldberg made our imagination travel further with his precise and ‘comic' drawings..  

Great idea to share these very interesting inventions.
