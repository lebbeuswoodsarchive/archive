
---
title: STARCHITECTS DEFENDED
date: 2007-12-19 00:00:00
---

# STARCHITECTS DEFENDED


Nicolai Ouroussoff, the New York Times Architecture Critic, wrote an article published on Sunday, 16 December, entitled [“Let Starchitects Work all the Angles.”](http://www.nytimes.com/2007/12/16/weekinreview/16ouroussoff.html?ref=weekinreview) In particular, he singles out Frank Gehry, Jean Nouvel, Rem Koolhaas, and Santiago Calatrava as the subjects of what can only be called his apologia for their deals with various mega-developers to design large, high-profile commercial buildings around the world. Regardless of what we might think of the projects, the architects and their deals, the Ouroussoff piece does them no service. It is a weakly conceived, sloppily written, wincingly defensive piece of PR. 


Ouroussoff's underlying theme–the article's tagline– is, “A big name on the blueprint doesn't mean sellout at play. It may mean visionary at work.” But, only ‘may.' It might also mean ‘sellout,' after all? Apparently some people have thought so, and I guess the Critic is worried that the architects in question might be stung by such doubts. After all, they are enjoying great success, so why should they be denied anything–including the admiration of “those inside the profession who see…the pact between high architects and developers as a Faustian bargain.” After conceding that “there's some truth to this, of course,” the Critic sets out to convince the skeptics.


He launches several arguments. The first of these is that the architects in question finally have a chance to test in the public domain their visionary ideas, which–he says–were once a critique of what was wrong with the profession. Sadly, he offers no examples of either the visionary ideas of, say, Calatrava, or Gehry, or the public projects where they have been/are being tested. It would have been helpful, maybe even essential to this argument.


Another argument is more nitty-gritty. “To my mind, if these architects are also getting a cut of the pie, why begrudge them?” It's not clear, exactly, which pie he's referring to, but it's clearly one that's rich in fat and carbohydrates. He continues, seeking to assuage any guilt involved, by saying, “In this new world, no hands are clean; there are good guys and bad guys on all sides.” So much for ethical distinctions.


He goes on. “…from the architect's perspective, working with mainstream developers is also a chance to step out of the narrow confines of high culture and have a more direct impact on centers of everyday life that were once outside their reach, from shopping malls to entire business districts.” Sounds good. No doubt public culture will benefit from a Nouvel shopping mall, or a Koolhaas business center. It would have been helpful, for the skeptics at least, if the Critic had explained exactly how. The democratization of high culture? Or its banalization? How does the public benefit from visionary ideas reduced to mundane ones? 


And on. “Architects have no control over a development's scale or density; nor do they control the underlying social and economic realities that shape it.” True enough, once starchitects are in the game. But they don't have to join the game. Instead they could turn their visionary abilities to different sorts of problems, ones that we don't already know how to solve. Do we really need visionaries to solve the urgent problems of shopping malls and office towers? But, no. “…why should such an immense responsibility be turned over to hacks?” Someone's got to do blockbuster developments, why not “our most imaginative talents?” Sounds good. But if the architects are as powerless over the realities as the Critic says, then it's a bring down, not a step up. 


Ourousoff stumbles badly here. The question remains, why did he feel the need to rise to the defense of several of the most powerful architects in the profession?


LW 



 ## Comments 
1. [orhan ayyuce](http://archinect.com)
12.19.07 / 8pm


*The question remains, why did he feel the need to rise to the defense of several of the most powerful architects in the profession?*


because he owes it to them and to their clients.
3. [Stephen Lauf](http://www.quondam.com)
12.19.07 / 8pm


Perhaps the article was attempting theater (review) of the *absurd*.
5. as
12.27.07 / 12am


Is there a blurring of the lines between professionalism and creative talent? Ouroussoff cites Bernini as a 17th century analog to the ‘starchitects' of today– which is absurd; Bernini was an exquisite sculptor, possibly a child prodigy, who translated his talents to architectural work. From artistry in stone to building in stone, his work possesed an understanding of the material at hand. Conversely, many of these starchitects today have been steeped in academia, excluding Calatrava who inspires another argument altogether; they work with ideas, which is without a doubt vital to the progression of architecture, but fail to (in my humble opinion) negotiate the physical aspect of their work. There is no sensitivity to the nuances of material, the sublte impact of space and form on a person– I feel that these aspects and more have been forgotten in favor of the glamorous superficiality of in-your-face conceptualism (a la Koolhaas and others).
7. Patricia Deveraux
1.3.08 / 6pm


Maybe because the new generations of architecture students disdain them. But to throw out the baby with the bath water is neither revolutionary, nor smart; only common.


Starchitecture is the product of its time. To *ignore* its value runs the risk of repeating its flaws. Maybe that's why he felt the need to defend it.
9. kh
1.26.08 / 6pm


to patricia  

serpentine they may seem, drinking their venom will not purge the ailment they cause. your statement aptly captures the hidden fatalism so prolific to our culture. noone suggests ignoring the present condition. what disturbs me more than all else is the persistent absence of an audible dissent. where are the radical thinkers of the coming generation? mr. ourusoff speaks of a “new world” with blurred distinctions of good and evil. bandy about the world's newness all you want, i see no change from the old one. mr. woods, your work adheres to an honesty that others might do well to embrace.
11. kh
1.26.08 / 7pm


“as folly, though it has gotten what it wanted, yet never thinks it has obtained enough; so wisdom is always content with the present and never displeased with itself” [cicero]
