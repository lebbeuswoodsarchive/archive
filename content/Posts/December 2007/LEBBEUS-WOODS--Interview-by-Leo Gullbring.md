
---
title: LEBBEUS WOODS  Interview by Leo Gullbring
date: 2007-12-29 00:00:00
---

# LEBBEUS WOODS: Interview by Leo Gullbring


This interview is published, in edited form, in the Canadian journal AZURE, January-February 2008 issue, page 34.


Leo Gullbring: What projects are you working on right now?


Lebbeus Woods: I'm doing a lot of writing, for my blog, but also what I call independent writing, meaning I have something to say, but no place to publish it yet. Of course, as you might expect, I'm working on conceptual projects, several of which can be seen on my website, under the rubric ‘current.' Also, I've been working on a small architectural project, which I call the Technological Knot, within the context of a major project by Steven Holl Architects in Chengdhu, China. A rare foray, for me, into the realm of building.


LG: Conflict and confrontation are recurrent starting points for your experimental architecture, for example, in the free zones in Berlin from the beginning of the ‘90s. This concern with public space is in opposition to it being privatized, invaded by commercialism and so on, but begs the questions what public space is today, what function might it play?


LW: My projects have always been concerned with boundaries, but never with property lines. The idea of ‘owning' a part of the earth does not appeal to me personally, though I understand that it has been, and remains, a great influence on how we live and are able to live. In the Berlin and Zagreb free-zone projects, I proposed spaces that are frontiers that challenge our existing modes of habitation. Only those willing to explore new modes of thinking and living can inhabit them. They are neither public nor private in the prevailing use of the terms. I extended these proposals in different forms to reconstruction projects in Sarajevo and Havana and a new earthquake architecture in San Francisco and the Terrain projects. Conceptually, these ‘freespace' projects are a new stage in the evolution of ideas of inhabitation.


LG: Does the free form architecture of Gehry, Hadid, Prix and others really present anything new? Or is it just the modernist originals again as icons? What conditions – other than international finance capital – are necessary for the creation of architecture today? How radical can architecture be in the era of world capitalism?


LW: What is new about the architecture of the gifted architects you mention is that it is without political or social intention. The work of earlier ‘free-form' architects always carried some message, usually of liberation, or even utopian reform. Today's counterparts are making pure art—it exists in a sphere entirely its own. Icons, yes, but expressing what? Certainly they are celebrations of human uniqueness–the uniqueness of genius. It is the uniqueness of an elite. I adore genius, and God knows there's not too much of it at any time, but, today, we need more than that in architecture. 


Modernism pretended—or actually hoped—that architecture would be the instrument for making a better world for most people. That idea has melted away in architectural discourse. It will take someone wiser than me to explain why. Maybe it is because many became disillusioned with ideals. Maybe it is because everyone-for-themselves capitalism has been so successful, materially.


Architecture, regardless of successes or not, can still be radical in its hopes for making a better world for many. It's up to the architects to set the agenda.


LG: Your work can be seen as a dialectic interplay between civilization and the urban fabric, where radical change arises out of extreme circumstances. What kind of extreme circumstances confront us today? What challenges are confronting architecture, and are we willing to address them?


LW: Number one: poverty and slums. Industrialization, allied with socialism, or with an enlightened capitalism, was supposed to have created plenty for everyone. Well, vast numbers of people are trapped in the lowest economic stratum, untouched by material gains of the last century, and their numbers are growing rapidly. Architects cannot change the economic system that is causing poverty, and must work within it. But there is plenty of room to imagine and invent scenarios and practical approaches to improving the living conditions in slums. I am working on a particular approach and will publish it when it's a bit more developed. Surely there are other architects who feel the same and are working toward the same end. 


Number two: the rediscovery, or reinvention, of a bond between ethics and aesthetics–the relationship between the way a thing looks and what it does. This is not the quest for a new functionalism, but for a new authenticity. If architectural aesthetics is reduced to style, then living is reduced to appearances. 


LG: You have long argued for the crisis of the city as a condition of civilization, a sign of life. Is the western world today keen on avoiding conflicts of any kind? Are we uninterested in creating new social relations among people? Are we unwilling to acknowledge the crises inherent in our modern world, in our life-styles, in our treatment of nature?


LW: The first nature I am concerned with is human nature. Conflict is inevitable in living–only the dead are free of it. The question is, which conflicts are beneficial to the development of human nature? Which conflicts are constructive–a vital part of growing, learning, creativity, adaptation–and which conflicts are destructive, and will only push us back, toward a darker human past? 


I believe that your phrase “new social relationships among people” gets it right. The new conditions of living demand a rethinking of our relationships with others. Our new global community cuts across older ideas of race, language, gender, class–all the stereotypes of difference. At the same time, architects are designing the same old building typologies in new wrappers, as though nothing has changed. That in itself is a major crisis. Once at least a few leading architects engage it, new conceptions of space–and the way it is created–will emerge.


LG: Change is a recurring thematic in a lot of your work. What changes are the most interesting today, and how do you think they can affect architecture?


LW: If we move away from the field of architecture, which is hardly changing at all, we see that communications technology is dramatically changing society. Entire new communities are being created, but they have not yet found ‘analog,' only digital, form. Architects need to concentrate more on conceiving lighter, more transitory, more agile and interacting networks of space, and be less devoted to big, heavy, inflexible monuments. 


LG: Is the issue of change the reason that your projects are, to quote Eric Owen Moss, ‘the buildable unbuilt'?


LW: Not to quibble, but all of my work has been built, though most of it only on paper or as models. Recently, I've starting making what, in the art world, are called installations–fast, temporary, exploratory spaces. It's my hope that these may at some point break out of museums and into the city. A bit of that happened in the System Wien project. I am moving in that direction.


LG: What values in contemporary culture do you detest, and which ones do you embrace?


LW: Oh, my! Don't get me started! Well, I detest the sheepish, uncritical way many people follow leaders, trends, ideologies. I distrust religion–today it doesn't even sponsor great art and architecture, just hatred, fear, intolerance. Pretty much the same for political ideologies. 


I adore the open and lively extravagance of contemporary culture. It's unruly and can no longer be constrained by narrow notions of taste and propriety. It's chaotic and free-wheeling, and very much alive. Also, very inspiring for an architect! 


LG: Space is of primary importance to architecture; nevertheless the ‘tyranny of the objects' still prevails. But with digital tools we can invent new spaces as never before. Will this let us grasp space in a new sense with new possibilities?


LW: Yes, absolutely. It'll take a while, because the digital revolution is so young, the tools so new. A new architecture is coming. You can feel it, stirring beneath the surface.


But let's not neglect the analog revolution, and its tools. Don't forget that the first electronic computers were analog computers, electrical circuits set up to simulate the resistances and flows within particular systems, like weather, or language, or social structures. The results were abstract, and had to be interpreted by meteorologists, linguists, sociologists, but that was their great strength–they could be more richly mined for understanding than representational digital results. I predict a resurgence of analog computation, one that is enabled by the digital, but goes far beyond it.


Architecture is analog and that–in the same way–is its great strength.


LG: Will narration and space tell another story beyond the objects?


LW: They always have. Most people are only tangentially aware of architecture's objecthood, as they live the stories of their lives. But, as I have written elsewhere, we will always need objects to give space a perceivable form. The task awaiting us as architects is to shift more of the perception away from the objects, to the space itself, enabling newer and more complex stories to be lived.


LG: Destruction and construction, death and life, in what way should these vital elements of existence influence architecture?


LW: I'll just say that architecture seems most invested in creating something new, beautiful, harmonious, upbeat. As an art form, and a field of knowledge about the world, and as a medium of experience, architecture–as I see it–must engage the whole of experience, including the so-called negative aspects. Simply creating something new puts the emphasis on a certain joy in life, on the will to go forward, but our natures demand that we engage reality in all its aspects. To do that is even more life-affirming.



 ## Comments 
1. [seier+seier](http://www.flickr.com/photos/seier/503702796/)
1.15.08 / 2pm


thank you for naming the slums in your interview. it is somehow difficult to imagine this happening with the (other) starchitects.


when we travelled to Rio de Janeiro a good year ago, I believe we witnessed the beginning of what you are calling for:


the central favelas were being included in the official maps of Rio. previously, these areas had been left blank – a true representation of their fate, left in a kind of political limbo, but a terrible fate indeed. we saw streets being named and rights being gained.


the Brazilians we spoke to were sceptical of this development, but their disappointment over the years with the impotence and corruption of police and politicians dealing with the favelas is understandable: the power of the drug lords who control most of the slums is formidable and frankly unbelievable to a northerner. the drug lords have been instrumental in the isolation of these areas within Rio and it could be argued that the municipality is acting in denial of reality.


hopefully, though, the recognition of the favelas as a potentially permanent layer of the city could prove a key to their future inclusion and development. after all, who is going to invest in something temporary?


it should be noted that many of Rio's favelas are older, better built and better equipped than other shanty towns in other developing countries. this is a field in which there are only local solutions.


it would be interesting if Brazilian readers of your blog would comment on how things are developing.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.16.08 / 8pm


Seier+Seier: You make a very important point. It's easier to ignore a problem if it's never officially recognized. The very mapping of favelas gives them proto-legal status, giving substance to demands for government actions to improve conditions. This is an aspect that needs much, much more exploration. Given today's precise satelittle imagery, it seems entirely doable.
5. [brief introduction to Lebbeus Woods « The earth is dying and we do not notice it.](http://theearthisdying.wordpress.com/2010/03/13/brief-introduction-to-lebbeus-woods/)
3.13.10 / 6am


[…] the full story here. “Without Walls:” BLDGBLOG interview with Woods “LEBBEUS WOODS:” Interview by Leo Gullbring “Subtopia:” […]
