
---
title: (a break)
date: 2010-03-09 00:00:00
---

# (a break)


I will take a break from posting for a couple of weeks. During that time I will look in regularly to read comments and respond when need be. As always, the blog will remain open as an archive. Until later,


LW



 ## Comments 
1. [Kyu Kim](http://www.designalternatives.org)
3.10.10 / 8am


you will be missed.


best.
3. [pysallruge](http://pysallruge.wordpress.com)
3.10.10 / 10am


Can't wait for your return. Your posts are inspirational. Good luck finishing the book.


– Lucas Gray
