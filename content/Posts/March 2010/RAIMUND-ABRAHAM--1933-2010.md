
---
title: RAIMUND ABRAHAM, 1933-2010
date: 2010-03-04 00:00:00
---

# RAIMUND ABRAHAM, 1933-2010


Raimund Abraham, one of the great architects of our time has died last night in Los Angeles. He was a powerful creative force in architecture, and not least a dedicated teacher and loyal friend. It is time to mourn our loss, but also to be grateful that such a unique soul lived among us.


LW


[![](media/vienna98.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/vienna98.jpg)


Raimund Abraham and LW in Vienna, 1998


New York Times obituary:


<http://www.nytimes.com/2010/03/06/arts/design/06abraham.html?ref=obituaries>


The Architect's Newspaper tribute:


<http://www.archpaper.com/e-board_rev.asp?News_ID=4406>



 ## Comments 
1. Amir Shahrokhi
3.4.10 / 8pm


Very sad news indeed. Raimund Abraham will be dearly missed. He was a committed educator and an uncompromising architect, who had profound influence on me and countless others that he touched.
3. -----
3.4.10 / 8pm


As a former student I was very sad to hear about this. He was infectiously passionate. He took a lot of joy in the work our studio produced.
5. [Alex Gil](http://www.alexgildesign.com)
3.4.10 / 8pm


He will be missed, his work was a force to be reckoned with. Though I did not have him personally at Cooper, the work he extracted from students was always rigorous & impressive. 


Who can verify the story of him burning a student model that was not up to par?
7. Christopher
3.4.10 / 10pm


Very sad news! I was there last night for his last lecture. Everyone in the room was very inspired and are deeply saddened by this tragedy.
9. M Dub
3.4.10 / 11pm


His presence in my thinking only grew as time passed, and I'm certain it will continue do so. 


Thanks, Professor Abraham.
11. Alejandro
3.4.10 / 11pm


I will never forget the ending of his last lecture, a very deep message dedicated to architecture students. I was at SCI-Arc last night.. I had the luck to meet him and I was very impacted by his work and his perception of life.  

Unforgettable to me.
13. [Marshall Shuster](http://marshallshusterarchitecture.com)
3.5.10 / 12am


This is terrible news. While Raimund was stubborn and brutally honest, I took his lessons to heart. He was my favorite professor while I was in graduate school at City College. I believe that I may have been in the last studio class he ever taught. Please inform me of any plans for his funeral.
15. Nick Pevzner
3.5.10 / 12am


From him I learned about the essence of architecture (The Section!), about the gravity of building (When you build you cut the Earth!), about drawing (Your pencil is sharp like a knife!), about literature (Read Poetry!). His voice reverberates in the back of my mind as I design, and for this I am forever grateful. Thank you Professor Abraham.
17. Lis Cena
3.5.10 / 1am


Even though I was not a student of Professor Abraham, I am deeply moved as I remember my first encounter with his work. His drawings were the first poetry I discovered and I am always so grateful to have entered the world of architecture through the mysteries and provocations of his powerful work. 


The news of his parting deeply saddens me.


He has been and continues to be a source of learning and immense inspiration for all of us, for he has left so much behind!
19. Anna
3.5.10 / 1am


I have only known prof. Abraham through his extraordinary body of work and his student's work, until prof. Woods invited him as guest critic for first year studio's critiques. I am deeply saddened. I am indebted to prof. Woods for inviting prof. Abraham to Cooper, resounding in the lobby the discourse I longed to hear there so much.
21. Hunter Bater
3.5.10 / 2am


It's too bad he didn't make more buildings. The Austrian Cultural Forum is perhaps the coolest building in NYC.
23. Anthony Vidler
3.5.10 / 3am


Dear Lebbeus,  

You and I have known each other a long time. But I first met Raimund in1967,at which moment he revealed to me that we had emigrated to the States together in1965, when he noticed (as he recounted to me later) that in front of him was a strange Englishman with long hair unpacking the Corbusier Oeuvre Complète for the customs. When asked what books were in his bag by by the same customs inspector who understood than two architects were coming through, Raimund said in true contrariwise manner, “I have Proust.” Raimund and I have been arguing about this non-differeence ever since. Raimund was one of the most influential teachers for over thirty years at Cooper, and his architectonics course was perhaps the best ever to succeed the Bauhaus version. He was both iconoclastic and radical (which at present does not seem so antipathetical) and a powerful figure in our collective history. Perhaps, however he should not be put so quickly into history — for his was a vision that pointed towards a future not a past — for this Cooper and the world will, as do I,miss him so much, as, at the same time, we celebrate his powerful architectural legacy. I send my condolences to all family, former students, and friends who, as did I, loved Raimund. As your photograph records, his laugh was greatly infectious.  

Tony Vidler
25. Gio
3.5.10 / 3am


I remember his love for the art of making..drawing.


The love for the student's work..


but most of all his utmost love for the work of the hand and the vernacular.
27. Dan Sherer
3.5.10 / 5am


A tragic day indeed. A very sad day. I will remember the review I was on with him and Massimo Scolari, in Diane Lewis' studio, for the rest of my life.  

He was a very powerful thinker, and a great teacher, and even when one disagreed with him, one learned from him.
29. Peter Zaharatos
3.5.10 / 5am


I had the privelege of studying under professor Raimund Abraham in 1993 when he was at Pratt Institute. My first great lessons of architecture as sacred realm were experienced under his guidance. His words while he was educator/provocateur have always resonated within me. “The greatest architecture has yet to be built”.  

Peter Z
31. Julia Weber
3.5.10 / 7am


Dear Raimund,


We just had you over for dinner a couple of weeks ago and you impressed me once again with your unceasing intellectual curiosity and your charming stubbornness. You fervently taught us how to properly arrange the logs in the fireplace. At the end of the night the room was equally full of smoke (fire and cigars) as it was filled with your wholehearted spirit. We will miss you.
33. Anne Romme
3.5.10 / 8am


Raimund was the most passionate architect and teacher, and there is hardly a day where his sentence “precision is an obligation” does not go through my mind. Thank you Professor Abraham.
35. Chest Rockwell
3.5.10 / 6pm


A tragic day in architectural education. His inspirational words, and reverence towards student's will and personal accomplishments will never be forgotten. Another one of Cooper's giants has departed from us. 


Thank you so much for inspiring your students towards greatness in architecture.
37. Jeff Brown
3.6.10 / 3am


Hi Lebbeus,  

I remember many sacred moments in the atelier, while holding tears at bay; I will try to describe one. On a summer night after a long day of drawing and fighting contractors, it was 3am at the Bond St. Studio, we were slowly moving an inverted cantilevered balcony section of the ACI model through the tip of a fully cranked table saw blade to rid the edge of a 52nd of an inch of material, my right hand keeping pressure toward the fence and the left ensuring zero rotation….knees trembling from lack of sleep. Raimund was eye to eye opposite his hand over my right pulling gently. It took 10 minutes but felt like a lifetime. As he quietly laughed we knew that THAT was a MOMENT. There were many moments like that in both love and pain but one damned thing is for sure, I learned how to see with Raimund. I learned how to see 3 feet into a drawing and while backing out netting those unseen conditions. I spent a long time within those drawings, and am lucky to have shared those times with him. He always gave 110% and expected the same. I can only say to those he touched it is time to carry the baton, no excuses. My deepest thoughts also go out to you, Joan and Diane…..and others too.  

Jeff Brown
39. devdutt shastri
3.6.10 / 4am


at that precise moment in a drawing when the hand and the pencil no longer know which is drawing and which is being drawn along, all speculation ceases and a pure act of creativity is born.  

at that moment, we are no longer drawn along in this life  

we draw – life. 


raimund drew life even as he drew his last breath.
41. Christopher Otterbine
3.6.10 / 12pm


Leb  

I am sure everyone who has had any contact or interaction with Raimund has memories, moments that are burned into thier consciousness. When you were with this man he left you with a deep feeling that at any given moment something important was either happening, or about to happen.  

Publically, causual was not his long suit.  

I recall. The day after Jay Fellows was taken from his parents apartment in a body bag we had a fourth year review in the lobby at Cooper. No one knew of the event that had taken place the day before. Of all of Raimund's peers, Jay was one of the few who captured his respect. They were good friends.  

During a break Raimund went into the office. He came back about a half hour later, sat down in his chair in front of some student work, and announced that Jay had died.  

I was as close to Jay as any student can become with a teacher and for me the news was more than information.  

I don't know how Raimund was able to continue with the critique… but he did.  

I stood directly behind him, watching Raimund's shoulders for the rest of the day. That day I really began to appreciate Raimund as more than a man of outright, brash, power. He taught people things he didn't even know he was teaching !  

I know you have lost a great friend and partner. When I got the news about Raimund's accident I immediately thought of you.  

I know you differently, and I know this a deep felt loss. My thoughts are with you.  

c.
43. [Suchitra Van](http://www.sthana.net)
3.6.10 / 3pm


What a great loss and so rare in the current world to find people like him.


I remember him not only as a gutsy teacher but as a solid neighbor, seeing him regularly at Cafe Dante, with Diane Lewis, or with Kevin Bone in Soho. Sometimes I walked in the Cafe and greeted him with my expanding family. He was glad to hear that I was mostly self employed.  

My rarest image of him was during a snow storm on Macdougal street, with his long brown coat and his cigar. I always thought of him as the color Brown.
45. ali soltani
3.6.10 / 3pm


Raimund is a hinge, in thought, in existence, in his final cut; you always feel at the edge of the abyss and the voided thing appears like a dormant essence and i don't know which i will feel more, his presence or absence
47. Yan
3.6.10 / 5pm


Meeting Minds for a Moment


At an intersection we begin a walk towards a path. Here is a bottle of water, drink. Here is your coat, put this on. Here are my eyes, use them.


What do you see with my eyes? Do you see the tree in the earth? Do you see the tree penetrate the horizon? What color is the shadow the tree has made?


Here is a pencil and and some paper…
49. [eduardo blanco](http://www.eduardoblancoarquitecto.blogspot.com)
3.6.10 / 6pm


I'm a chilean architect, I don't had the chance to met him, but the built and unbuilt work of Mr. Abraham has always inspired me, I think that this is and will be a enormous loss for the discipline.  

Kind Regards.
51. Christophe DM BARLIEB
3.6.10 / 6pm


There was a moment when everything I made and thought aligned to reveal space as I never had imagined. “All it took” was a stick; about 36” long with a 1/4″ square section coated in black china ink pinned on a wall, along with five photographs of the interior of a model that posited the death of a building. At that moment, I knew, I was standing before something far greater than myself: a cemetery in a black hole, a passageway between our universe and the next. It was the moment I learned to see. I owe it to one gentleman, Raimund Abraham.
53. Wes Rozen
3.6.10 / 7pm


In the presence of Raimund, Professor Abraham, I always felt the significance of my decisions, my agency in the world.


How many students made one more drawing the night before a critique, or shaved an extra 1/16” of an inch off of a model base, because “precision is a moral obligation”? How many friends of Raimund would do a little ‘gut-check' before dialing his phone number or ringing the buzzer to his Bond Street studio? He spoke to the core, and as a student or friend, in agreement or opposition, he inspired dignity and passion in the statements we make.


Like so many of my peers, whenever I'm digging deep, searching for creative clarity, I hear his charm and his convictions. In recalling conversations with Raimund, or his work, I always feel empowered to define the ethical dimension of a situation and attack it with an attitude.


Now that he is resting, I find joy in remembering how much he loved to dream and know that his legacy will have agency.


—


I also wanted to say how grateful I am for this forum as a place to share and come to terms with this sudden loss. Thank you Lebbeus.
55. Nenad Stjepanovic
3.6.10 / 7pm


‘Drawing architecture means you don't have to become a slave in a corporate office or groupie of a celebrity architect, because all you need is a piece of paper, a pencil and the desire to make architecture.'


This is lasting truth
57. [Alexander Gorlin](http://www.gorlinarchitects.com)
3.6.10 / 8pm


I worked for Raimund Abraham in the summer of 1976 on his Venice Biennalle Entry “The Seven Gates to Eden” with John Maruszczak while at Cooper. John built the models and my job was to coat them with aluminum putty to give them a monolithic appearance, simulating Raimund's original intention of constructing them out of soild lead, an idea that even legendary Max, the head of the shop thought to be unfeasible and dangerous. When I discovered the putty to be have harmful fumes and brought a gas mask to filter the air, Raimund was thrilled as he saw the congruence of the theme of death in the “Gates” reverberating in the actual making of the project. Like Adolf Loos, Raimund believed that all architecture belonged to the “monument or the tomb”. The entry to each tomb-like suburban house was an underground stair that a car could drive onto, and asphyxiate the inhabitants with carbon monoxide. One of the model cars we used was Hilter's Mercedes. Like Anselm Kieffer, and his friend Walter Pichler, Abraham presented the ultimate fragility of life and the body, through the lens of the Nazi horrors of WWII. Architecture was to embody the most primal of gestures: lying, sitting, standing, and straight out of Dr. Strangelove, bodily fluids of all possible kinds.  

 The empty loft we worked in was on Bond Street, long before it was chic; the Bowery was derelict and filled with homeless people. Raimund chose to live here because he believed architecture could only be created in a place of harsh reality, not in a pampered setting of luxury. Architecture to him was a violent act of destruction and creation, every line of the pencil was a cut, a ripping of the flesh of the paper.  

When I saw him in the street years later and asked about the Austrian Cultural Center, he said the process was “stupefying” dealing with all the consultants and the builder.  

Of course, no one could live up to Raimund's impossible standards, but his memory recalls an almost antediluvian time, before typing at the computer was considered drawing; when the architect drew with his entire body and soul and brought forth an architecture that either rose from the horizon of life, or fell back lifeless into the grave.
59. Christopher Otterbine
3.7.10 / 1am


Alex Gorlin you are an asshole and you can go fuck yourself, and I would be the man behind Raimund, in his old age, helping him thrust a mighty load across your face!  

And in your state of excitment, being in the midst of some event, watch you swallow hard, cause you are soo careful not to offend or confront in life; so in silence you can offend!  

Asshole !!!
61. Christopher Otterbine
3.7.10 / 1am


Lebbeus  

Anyone who will toss off easy digs at Raimund needs to be buried and flushed out !!! He had few friends. For good reason !  

Nice thing is, he never wore cologne, but he never smelled like the compost pile!!!
63. Jeff Brown
3.7.10 / 3am


Most architects have experienced and understand that great Architecture gives off a life…. long after the architect is gone. This kind of work exudes and bears witness to how much effort was put into it long before it exists physically. Raimund confronted and struggled tirelessly with resolutions, never giving up. He also expected as much from the few he had in his studio. He settled for nothing less. It is unfortunate that some architects do not see this, and only hold onto a moment when he offended. There are, unfortunately, few architects that have put something in the world that even comes close to what Raimund has done…yet, these same architects are suspicious and critical of the prospect and relish in the hope he will fail. Why? They do not want to understand the person and find it easier to write an individual off.  

There is no doubt in my mind that Raimund immersed himself in Architecture at every level and at any cost.  

The tireless drawings, models and construction administration on the ACI Forum was difficult, an experience that took some time to recover from. Yet, having worked in many offices prior to, and after the 5 or 6 years with him, nothing compares to that experience.


To Raimund, my teacher, I believe in what you have done… no doubt you ARE the horizon for me now…. 


Jeff
65. BinMar Leto
3.7.10 / 8am


Reflecting on his architectural discourse, drawings, words, and precise gaze of those eyes…resonation. The collective memory of Raimund Abraham ensures his entry into the pantheon of the immortals.
67. james richards
3.7.10 / 3pm


My favorite raimund memory was when he went to the hardware store that used to be near cooper to buy an alarm clock and the sales person said to him “this one works like a dream” to which he replied “but it is designed to shatter them…”
69. Roger Broome
3.8.10 / 9pm


Professor Abraham was a complex person and, although I enjoyed having him as a professor and admired much of his work, I have distinct memories of some very poor behavior towards students and faculty that I will not repeat here. 


I very much appreciated his strong support of the work that was done by the teachers and students in Cooper's Saturday Program for high school students and the push he gave to have many of these students admitted to the architecture school.


I have always wondered why the new brick for the infill of the arches at the Anthology Film Archive was such a poor match for the existing brick of the building–or why he did not use a more contrasting one to eliminate the clash between the two. For someone who emphasized the importance of quality this seemed like a glaring oversight. Now I guess I will never be able to ask him about this detail.
71. Aida Miron
3.9.10 / 7am


Raimund Abraham taught me that architecture is a discipline, something sacred, poetic, and that it is the responsibility of the architect to be ethical in his/her intentions, for architecture also has the potential to be an oppressive force. He was the first to introduce me to the precision of drawing, geometry, the elemental, ideas, to the rapport between thought, drawing, the articulable and the built. In an architecture where the “section” never lies, and possibilities of multiple realities exist; where dreams and poetry have the potential to enter drawing just as site and program do, without hierarchy. He taught me about poetic visions in architecture that could be inspired by literature, language, film, landscapes and the city. He was my best introduction to New York, to the avant-garde, L.E.S poets, the Film Anthology, Jonas Mekas, Peter Kubelka, Gaston Bachelard. I will never forget him squinting before a drawing, his voice resonating throughout the drawing studio. When he entered, one was confronted by a radical presence, one whose influence will continue as a force to confront everything that is inauthentic. This presence will continue affecting the work and thoughts produced in this space. As Jonas Mekas has said: “the avant-garde is always the front line of any field… moving ahead into some totally unknown area, the future…that's where usually it's all very fragile, and on the front line is where usually most of the bullets hit you.” Through his works and architectural visions he made way for a possible future in architecture, as a force that will continue confronting untruths with true thought.
73. Diego Peñalver
3.9.10 / 1pm


Very sorry to hear of this, together with Lebb, Raymund Abraham was one of the best teachers I,ve had at Pratt in the 80s. His drawings always captured and stimulated my imagination, his guidance and aesthetic sensibilty for architecture will always be in my heart.  

Sorry Lebb, I know he was a close collegue and friend..  

my regards to you, to his close relatives and friends.
75. [Frederieke Taylor](http://www.frederieketaylorgallery.com)
3.9.10 / 8pm


What a terrible loss, I am certain that his architecture and drawing will be even more appreciated as time goes on. I have a wonderful memory of him when he installed his show of the Beijing JingYa Center at my gallery in 2005. The show was to open on Thursday; by Wednesday evening nothing in the gallery. When I arrived Thursday morning there it was, gorgeous models and drawings – a miracle! What he was most interested in though was in preparing a delicious lamb stew to serve his friends afterwards which made him miss most of his opening. Farewell to a wonderful man and a great architect, Frederieke
77. Mark Morris
3.10.10 / 3am


When I got the news, W.H. Auden's poem “Stop all the clocks, cut off the telephone” rushed in:


Let aeroplanes circle moaning overhead  

Scribbling on the sky the message He Is Dead,  

Put crepe bows round the white necks of the public doves,  

Let the traffic policemen wear black cotton gloves. 


I offer my condolences to Lebbeus.
79. [William Stout](http://www.stoutpublishers.com)
3.11.10 / 10pm


It may have been a dream I had, but I remember seeing Raimund with in a hat, coat and tie with a cigar in his mouth discussing one of his drawings in a lecture I was attending. I was lucky enough to acquire one of his books called Elementare Arkitektur, by Residenz Verlag 1963 where he did a book of Josef Dapra's wonderful black and white photographs of primitive buildings in the country side. Hay barns, silos etc. I believe Walter Pichler was his assistant on the first book. The small Catalogue from the Galerie Grunangergasse 1973 in Vienna shows some of his first drawings where his house with curtains is shown. I have a signed print of that house and it hangs in one of my library rooms. Raimund was a model for us and whenever I need inspiration I pick up one of his books to either view his drawings or read his texts. Raimund will be missed but not forgotten.
81. Lee Becton
3.17.10 / 7pm


When I got the news, Barbra Striesand's “The way we were” lyrics rushed in


Memories  

Light the corners of my mind  

Misty watercolor memories  

Of the way we were  

Scattered pictures  

Of the smiles we left behind  

Smiles we gave to one another  

For the way we were


Can it be that it was all so simple then  

Or has time rewritten every line  

If we had the chance to do it all again  

Tell me – Would we? Could we?


Memories  

May be beautiful and yet  

What's too painful to remember  

We simply choose to forget


So it's the laughter  

We will remember  

Whenever we remember  

The way we were


So it's the laughter  

We will remember  

Whenever we remember  

The way we were





	1. Roger Broome
	3.18.10 / 3pm
	
	
	Lee, you really summed it up. Thanks so much.
83. Giancarlo M
3.22.10 / 5pm


Very sad news. Raimund was an inspiring professor and architect. I will always remember the moments we all shared in our Pratt studio..,his presence and absence in studio was amazingly influential. Thank you
85. Patrick Girvin
3.24.10 / 4am


It was a fine, bright day, and I chanced to see Raimund Abraham walking across 4th Avenue toward Broadway. He looked up, and abruptly stopped in the street, transfixed and smoking. A large soap bubble was floating past, about 40 feet up, moving slowly through the air. Raimund observed it carefully until it passed out of sight, and then continued on.


I've thought about that for years.  

….  

I went to see Raimund shortly after he gave my work a particularly scathing critique. “I had to do that”, he said, “because we are an endangered species.” “Who is?”, I asked. “Human beings.”  

….  

Raimund's life added dimension to mine.
87. Ajay Manthripragada
4.8.10 / 6am


So many seem to remember singular phrases of Professor Abraham, such were the power of his words. I attended three of his lectures, including his last, and have likewise been indelibly marked by his exacting, haunting language. He spoke of the “tragic temporal dimension of human existence;” an attribute with which we are now too familiar.
89. Andrew Ferentinos
4.11.10 / 3pm


I worked for Raimund at his atelier on the Bowery during my study at Cooper Union. I will never forget when I was building a model for him. From far across his studio, he noticed that one of the ten or so, 3″ slanted wood columns that I had glued down was off by, at the most, 1/64″ of an inch. This uber-20/20 supervision dumbfounded me. Acknowledging this, he said that his eye for precision was both a gift and a punishment – a punishment because it was inextricably linked to an overarching desire for perfect form. “Is it perfect yet?” He would pose this rhetorical question over and over again. It applied to everything we did, and for that we suffered.


Raimund was an amazing human being. I will never forget his passion for architecture and his ethical commitment to teaching. In his atelier, even when we were busy with a deadline, he spared a generous amount of time to train me or discuss architecture. I learned early on that architecture, learning, and ideas came first, business and client demands second. I remember when a heat wave hit New York City. His studio on the Bowery was sweltering. We had a deadline. I was in a rush to finish drafting. Raimund hovered over my shoulder – cigar clenched in teeth, classic fedora on head, heavy smoke enveloping us in a hot, dry, dark cloud. He sat there, supervision on my drafting, scrutinizing every line I drew, (particularly the sequence of lines forming the geometrical structure of the figure). He hovered by no means as a micro-manager but rather as an architect who had mastered drawing and as a committed teacher who, without a doubt, enjoyed guiding a student along. That was the Raimund Abraham I knew, worked for, learned from, and will surely miss.
91. Krystina Kaza
5.3.10 / 1am


Raimund taught me in my first year at Cooper. He had a profound influence on me. One day when I was in quite a frantic state, struggling with some aspect of my design in studio, he approached my desk and said,  

 “Krystina, you cannot work in a state of panic. You must work with joy!”  

More than anything else I learned in studio, I remember this.
93. Charles Matter
4.20.11 / 9am


I had the distinct good fortune to study under Prof Abraham as a beginning architecture student at RI School of Design in 1968 and 1969. His unique craziness tempered with steely craftsmanship and love of the Design Process quite literally focused my whole life as an artist. We became friends. When a conservative political upheaval in the RISD Architecture dept nixed the prospect of continuing under his mentorship for the rest of my studies there; I left architecture.. and transferred to Photography. He disapproved of my leaving but he approved of my stance. Sadly we lost touch over the years; but what he taught me still guides me.
