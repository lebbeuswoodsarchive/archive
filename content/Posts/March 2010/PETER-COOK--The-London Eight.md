
---
title: PETER COOK  The London Eight
date: 2010-03-25 00:00:00
---

# PETER COOK: The London Eight


[![](media/5a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/5a1.jpg)


*(above) Virtually Venice (2004), by  architect CJ Lim.*


*The following is excerpted from the catalogue of the SCI-Arc Library exhibition, “London Eight,” curated by Sir Peter Cook, which can be seen at the Southern California Institute of Architecture in Los Angeles, March 19 through May 16, 2010:*


**THE FINER GRAINS OF RADICALISM**


Richness of investigation that can be found in the grain of the work distinguishes certain exploratory architecture that has been going on in London for the last 30 years. Given that the resulting compositions are sometimes weird, there is a tendency for outside observers to admire its craftsmanship (as drawing or physical fashioning—or its will to surf between various modes and programs of digitalization). Such observers often seem shy to discuss its uncomfortable relationship with either the Broad Mainstream of built and projected architecture or the more shrilly political/theoretical thrusts of European Statements and Positions. A clue might be found in listening to a Review session at the Bartlett School—where all of the exhibiters are to be found. A certain tradition of discussion still lingers—far less “positional” than an American session and less declamatory than in France or Germany. The non-British are often puzzled that a discussion can stray far away from the motives or maneuvers of the project and engage in speculation, whimsy, pernickety detail, gossip, nostalgia, raconteurship and probably avoid (at all costs) the definition of the project as “good” or “bad.” The same waywardness has often been observed in the English Novel, where identification of good, evil, heroism or villainy are replaced by messy and complex situations—but full of intriguing side-issues that months later strike you as containing the essence of the message. Of course only two of our exhibitors are English by birth and the territories that they severally explore range from the imagined to the prosaic, the delirious to the stubbornly measurable. Yet the common culture they share is of teaching or studying in a school where individuality has been cherished and the teachers have a tendency to re-define their interests without notice. Those same speculations, details and an assumed mandate to slither from one territory to another are then interpreted through formal objects and drawn imagery.


Sometimes the Bartlett is assumed to be hung up on gadgetry, sometimes on an over-refinement of the drawn (or plotted) line. Sometimes it's believed to wallow in fantasy: yet its graduates are statistically the most desired by offices in a city with a dozen nearby schools—as well as the plane-loads that come over from the *technische hochschulen*. When Eric Moss asked me to round up some specimens, I had no second thoughts that these three syndromes were the ones to have—anyhow, anywhere and not as much to make a pedagogic point as to simply get some great stuff on the wall. All of them at their best when being full-on—or should I say, fully indulgent. The art of indulgence is a threatened species. God preserve us from all those *reasonable* architects, those *cool* manners, those *careful* Buildings—go look elsewhere for that—there's plenty of it around.


Now, join me in watching just a little sample of **CJ Lim**'s prodigious output. The “veteran” of the group whose work first started to be published (can we believe?) some 25 years ago! Fluent, mannered, the personification of that quality of “eye” that can be cultivated and nurtured—but with an extra and unfathomable and built-in tic of visual discrimination that knows how to seduce the desirable from out of the circumstantial.


His first prizes were won for deft small houses within a controlled language where maybe the later Meier met the earlier Hadid. Able to draw almost anything, so that Christine Hawley could describe it in 1996 as “exquisite tailoring,” which might have been enough at that point, but a pursuit that he has been at pains to overlay with much else in the years since. A break point may well have been his competition-winning project for the Cultural Centre for UCL. The jury of Rogers, Miralles and Hasegawa could immediately recognize substance. A complex building thoroughly engineered (he worked closely then as now, with the structural engineer Matthew Wells) and drawn, painted, projected, layered, honed every which way. So now, 50, maybe 100 competitions later he is at last being recognized in China and given commissions. His palette has become broader and his confidence has rarely wavered as he has somehow (without making finished buildings), comprehended issues of scale and detail. Yet he has surprised us every three or four years with a major augmentation of his vocabulary. Given the opportunity to fill a room in the British Pavilion at the Venice Biennale of 2004, he displayed work of an extraordinary finesse and very wide range. His organizational skill had not left him, but was challenged by his three-dimensional paper items, where flat drawing literally folds upward to become a three-dimensional lacework. More recently, he has produced a series of urban or super-urban scale projects in Chinese competitions. Already at Venice he was drawing attention to the paradox of East and West: basing his narrative on the history of Kublai Khan and by inference, his own immersion into England (clue: he adores the cartoons of William Heath-Robinson!) but with an increasingly sympathetic observance of his Chinese-Malay roots.


**CJ LIM:**


Battersea Dogs Home: A Dating Agency, London, 2005:



**[![](media/9b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/9b.jpg)**


Sky Transport for London: Redevelopment of the Circle Line, 2007:


[![](media/10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/10.jpg)


The Nocturnal Tower: Smithfield Market, London, 2007:


[![](media/11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/11a.jpg)


JellyHouse: An Homage to John Hejduk, 2008:


[![](media/11b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/11b1.jpg)


CJ has chosen **Pascal Bronner** to exhibit with him. Born in Malaysia and brought up in Germany, receiving a string of awards, his New Malacovia city is a created parody of the city—the idea of “playing” the city is indeed made possible by being able to carry it in an attaché-case and Bronner's atmospheric digitalization enables him to take us up-and down, in-and-out ofany scale, of any phenomenon. Scary!


**PASCAL BRONNER:**


New Malacovia—A portable city blueprint:


**[![](media/12b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/12b.jpg)**


Image from New Malacovia city:


**[![](media/5c.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/5c.jpg)**


Potatoes are used to harvest renewable energy:


**[![](media/13a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/13a.jpg)**


Fiber-optics garden in the inverted laboratory harvest light through infinite windows:


**[![](media/13b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/13b.jpg)**


The city bed of bottle corks, buoyantly synchronizes with the Danube River:


**[![](media/14a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/14a.jpg)**


The city of New Malacovia is a vast prairie of windows–mimics a flattened Nevsky Avenue in St. Petersburg:


**[![](media/14b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/14b.jpg)**


The cultural overlays are equally intriguing in the work of **marcosandmarjan**—the practice name of Marcos Cruz and Marjan Colletti—yet they have not yet displayed many signs of nostalgia for the old homestead. In Marcos' case it is difficult to figure out quite where that would lie. He is coy about his birthplace: Oporto (especially when I rave about its weirdness and cuteness) embarrassed by its over-strong connotation with Alvaro Siza. Studying in its Academy (not the Siza school) and then in Barcelona he joined the same Bartlett M.Arch class as Marjan—though they did not become buddies until much later. There, “Skins” and “Flesh” rapidly became his territory: bodies inhabiting the flesh, flesh deflecting, flesh recalling the fetishes and the dark areas of Western Culture. Moving with it to such an extent that his Doctoral Thesis takes the preoccupation to the scale of 530 pages and two thousand drawings and illustrations (and wins him the big RIBA Research Medal). Quickly recognized as a very rounded, knowledgeable critic, his recent appointment as Director of the Bartlett surprises no careful observer of his total immersion into the culture of architecture. Anyone assuming that Marjan is the milder of the two would be quite wrong. In the first week of his Bartlett studies he presented a group of soft-toy animals “Velcro'd” to his shaved head* as a project related to comfort: even at a period when motorized waste bins-as-robots and models that *intentionally* blew up were not unknown, it was a bit of a show-stopper. The Clue came (inevitably) via gossip at a Vienna eatery. Under the scrutiny of dingy Hermann Czech lamps, it was Volker Giencke—Marjan's former professor—who let out the secret that Colletti was the craziest and most brilliant of all. Subsequent years have seemed to be a virtual tug-of-war between Innsbruck and the Bartlett on the teaching front. His Master's project involved “besking” for somehow, the animals became bed-desks. Yael Reisner (his tutor) would, like Giencke, come back with weirder and weirder stories of where it was all going to lead. Yet the determined and systematic manner in which work of extreme beauty has emerged from (I was about to write, “his pen” for it is so fluent) his computer. Both growth and something that goes far beyond the exigencies of the Art Nouveau coalesce to make a digital architecture that is far from parametric orthodoxy. Indeed, a clue to their position and that of their key students was made very clear on the occasion of a review at Innsbruck. In the room were Patrik Schumacher, Theodore Spyropoulos, Ali Rahim, Marjan, Marcos and myself. A Schumacher student project and a Colletti student project were presented in turn. The ruthless proceduralism of the former was challenged by the (frankly)Baroque qualities of the latter. Whether they like it or not, there is a sheer architecturalness in their competition work. Look at their clever sections and play with light sources and the professionalism with which they have exhibited them in almost every known Biennale and Triennale of recent years.


**MARCOSANDMARJAN:**


New Godet Club, Istanbul, 2003:


[![](media/5b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/5b.jpg)


Tomihiro Museum, 2002:


[![](media/15b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/15b.jpg)


Agropolis, 2009:


[![](media/16.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/16.jpg)


MELAC, 2005:


[![](media/17a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/17a.jpg)


It is a base from which marcosandmarjan can guide talents like **Yousef Al -Mehdari** through work that seems as if the Gothic dream (with digital modeling) is no longer a dream. The exploration of ritual and religion is interpreted by writhing bodies that become an evocation of architecture that can reside as the intestines of a Byzantine church. Once again, scary!


**YOUSEF AL-MEHDARI:**


Palm Chapel:


[![](media/6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/6a1.jpg)


The New Polyhydric Body:


[![](media/19a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/19a.jpg)


The Place of Postration:


[![](media/19b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/19b.jpg)


The Monesthetic Shrine:


[![](media/20.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/20.jpg)


The calm and reticence of the English scene might seem to be in deep contrast to all of this exotica. **Laura Allen** and **Mark Smout** arrived at the Bartlett from other British schools and brought a typically English air of determined workmanship overlaid by throwaway matter-of-factness that, in their case, concealed virtuoso work. For them, fewer competitions and biennales. Indeed, the heroic model for the Grand Egyptian Museum competition was not even started when the competition closed. Yet they honed it and delved into it: creating a piece of sculpted urbanism that dominated any room in which it was shown and winning the Bovis Prize at the Royal Academy. A double irony here, for its competitors included some of our most famous architectural Names and the money given by a giant, plodding hands-on construction company!


Daughter of an engineer, son of an artist yet her early work was graphically exquisite and his was full of gadgetry. In “The Retreating Village” the rapid erosion of the coast of Norfolk (Laura's county), is treated less as a tragedy than as a fusion of their sensitivity and technical skill. So easily could it just remain a clever, timely theoretical piece—but look closely at its dexterity and its attention to the stuff of architecture. I am intrigued that with its publication as *Pamphlet Architecture No 28* the invitations to exhibit and give workshops have flooded in. I am curious to see how their quiet quizzicality can start to find feet in the Nevada and Australian deserts and even further afield.


**SMOUT ALLEN:**


Model 1, Prototype for an Envirographic Architecture: Water:


[![](media/232.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/232.jpg)


Sketchbook, Prototypes for an Envirographic Architecture: Lanzanrote:


[![](media/22a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/22a.jpg)


[![](media/22b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/22b.jpg)



With **Johan Hybschmann** as their outrider we are taken into a secret world of precise laser-cuts to form the pages of a book that gradually builds up as a representation of Alexander Sokurov's film *Russian Ark*. Yet, according to Geoff Manugh of BLDGBLOG, the author of this reverie is not some intense weirdo, but a guy who is ‘contagiously good-humored'.


**JOHAN HYBSCHMANN:**


The Book of Space: initial studies of perceptual connections of spaces directly referring to the one sequence film, *The Russian Ark*, by Alexander Sukorov:


[![](media/24b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/24b.jpg)


The Federal Hall: Redesign of the Federal Hall, New York. Study of internal spatial distribution:


[![](media/25a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/25a1.jpg)


The Federal Hall. Initial studies of facade elements:


[![](media/25b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/25b.jpg)


The Federal Hall. Internal photo of final proposal.


[![](media/26a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/26a.jpg)


The Winter Palace: Abstract elements are designed to appear specific and recognizable when turning with great speed using an inbuilt elastic mechanism, creating images of pillars, tables, staircases, windows, and corridors:


[![](media/26b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/26b.jpg)


So we bring these people to SCI-ARC—who else in the US of A will open-minded enough to host them I wonder? There is essential reading matter: the several books of CJ Lim's work, the Springer Verlag book (in the Fresh Architecture series) on marcosandmarjan and the “Pamphlet Architecture” referred-to already. The periodical *AD* has a special on *Plasmatic Design* issue edited by Cruz, which will be capped by one that is due out any second on *Exuberance*, edited by Colletti. And now Smout, Allen, Al-Mehdari and Hybschmann drop onto our laps from the enthusiastic jet stream of BLDGBLOG. The rest to follow, no doubt.


**PETER COOK**


London, 2010


Complete catalogues can be obtained by sending an email to Wendy Heldmann at [public\_programs@sciarc.edu](webmailto('public_programs@sciarc.edu');)



 ## Comments 
1. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2010/03/25/peter-cook-the-london-eight/)
3.25.10 / 1pm


**Social comments and analytics for this post…**


This post was mentioned on Twitter by BibliOdyssey: Some conceptual architecture (at least I *think* that's what it is) <http://bit.ly/dvKOUF> Lebbeus Woods is never boring….
3. [Perpetual motion and urban drifts « things magazine](http://thingsmag.wordpress.com/2010/03/31/perpetual-motion-and-urban-drifts/)
3.31.10 / 11pm


[…] of the Hohm Energy System, a sort of private sector Carbon Trust / Lebbeus Woods on the London Eight, a Peter Cook-curated show of architects and mentors currently on show at SCI-Arc / short films at […]
5. spencer
4.1.10 / 1am


mmmm. wonderful and a lot to digest.
7. [slothglut](http://fairdkun.multiply.com)
4.4.10 / 6am


‘So we bring these people to SCI-ARC—who else in the US of A will open-minded enough to host them I wonder?'


How about Cooper-Union? Maybe?
9. [FEATURE(S) – Pleasure Reading | with.these.ingredients…](http://withtheseingredients.com/2010/05/24/features-pleasure-reading/)
5.25.10 / 8pm


[…] Visit the blog, Lebbuswoods for further reading about the London Eight Exhibition. The writer goes into depth about exhibit and […]
