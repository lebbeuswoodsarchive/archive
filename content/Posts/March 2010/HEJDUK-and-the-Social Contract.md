
---
title: HEJDUK and the Social Contract
date: 2010-03-25 00:00:00 
tags: 
    - architecture
    - Berlin
    - Kreuzburg
    - tower
---

# HEJDUK and the Social Contract


[![](media/lwblog-hejduk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwblog-hejduk-2.jpg)


*(above) Kreuzburg social housing tower, Berlin, 1987, by architect John Hejduk.*


**A STATEMENT BY DIANE LEWIS**


The Architects Newspaper, [“Defacing Hejduk,](http://blog.archpaper.com/wordpress/archives/6899)” illuminates the impending threat to John Hedjuk's Friedrichstrasse tower but may defeat its advocacy with an opening salvo that reminds us of an earlier era of lukewarm press on John Hedjuk.


The John-Hejduk-built-very-few-buildings introduction is rooted in the old propaganda, the sieve through which the poetic, anti-commercial ethics of Hedjuk's view on architectural practice was strained. I am quite sure that the author does not wish to inherit that tone, in light of their impassioned plea to save the architectural project.


At the outset of the struggle to save the character of this first of the Hejduk IBA [[International Building Exhibition in Berlin](http://jimhudsonwriting.wordpress.com/2009/10/13/blueprint-magazine-comment-piece/), 1979-87] projects to be threatened by new development, it is time to address the comprehensive objectives of the type of practiceembodied in this building to secure its integrity.


Any echo of the tired attempt to undermine the main thrust of the type of resistance that secured Hejduk's presence among the group of architects who formed the architecture of the city discourse in the discipline rather than the business of architecture debilitates the potential to save this project in a manner that doesn't render it impotent.


The spirit of the effort to sustain and preserve this building must extend to the *intent* of Hejduk's position to open a new era of potent action. First, acknowledge that the preservation of this civic complex is a recognition that its historic value is in its challenge to the assessment of “quantity” over selectivity in the oeuvre of the architect. As “the house for the inhabitant who agreed to participate,” this threat to the architecture of consent leads to the issue of whether an integrity of both form and program should be advocated by our discourse. And to the question of whether separating the preservation of the building form without examining its relationship to the intended civic functionis a cause that Hejduk would care to support.


The Hejduk tower is no longer being inhabited to serve its original humanist program. It is no longer social housing [state subsidized for low-low/middle incomes] and has been sold to a developer who wants to transform it into profitable real estate. Consequently, it seems that they are trying to gut them and reconfigure them in spatial organization, programmatic function and tone. Here we are faced with the embarrassment that the IBA social housing is being converted to luxury condos without any grasp of what will inevitably be lost to the memory of Berlin.


If the city of Berlin were to honor the social vision of the IBA projects and the social structure they supported, this project would not be in danger. This is a very consequential issue and one that I think must be seen within a larger perspective. It is more than a design issue of character, maintenance, or style.


All of the architects who responded to the city's invitations—under the leadership of  architect Josef Paul Kleihues—and designed these innovative interventions for social housing with domesticated civic space now are historically important projects endangered after the fall of the Wall and the new socio-economic aspirations of Berlin.


The question of how to frame the support for the authenticity of the project is important, as is the question of who in Berlin is working to support the integrity of the IBA accomplishments within the urban fabric. I think that the architecture community should go after this with a strong statement on the authenticity and relationship of form to program and social integrity.


The quantity versus selective practice approach contravenes the fact that Hedjuk limited his participation to projects that he was consciously willing to address, illuminate and define as consistent with his commitment to “architects as the keepers of civilization.”


His position is quality and selective quantity versus a practice of pure commercial and material demonstration, and here the social housing of IBA supplied him a definitive condition. The IBA projects he participated in to see constructed, address the site, objectives, social program and civic body to which he took responsibility and identified with. Here he supported the construction of his architecture and the framework of the IBA objectives, and restraints, in order to serve the advancement and civilizing effect on its city, Berlin.


The IBA projects were built in Berlin as innovative social housing which is currently threatened in its very raison d'etre, and consequently in the expression of tectonic and iconographic invention and nuance of character. The cooperation between architects and community in the conception, process of design, and construction of the projects is a milestone in the history of international architectural practice.


The threat of defacing the housing that Hedjuk designed is a vanguard issue that will lead the argument that must be formulated now within our architectural community with full consciousness of what John understood to be the total accomplishment. The work at hand is not only preservation; rather, it implies a re-framing of the commitment to the integrity of the social contract for its use within a new social challenge that cannot be addressed by arguments for mere physical conservation.


A purely design oriented preservation argument on the beauty of the project justifying its salvation will not fully advocate the consciousness that I believe John would want to be brought to light at this time and that threatens the very fundaments of the IBA endeavor. His social housing and many of the other projects successfully gave form to a series of intimate civic spaces within the body of a Berlin that wished to integrate a new social order and conscience.


It is important to remember that the first post-war building IBA in Berlin [[Interbau](http://en.wikipedia.org/wiki/Interbau), 1957]  is preserved, and intact within the Tiergarten and the Hansaviertel sector of the city. Its examples of social housing, built by Le Corbusier, Aalto, Niemeyer, Gropius and others, survive in the context of the great park of Berlin, a pastoral context. Here, with a struggle to preserve the  80s IBA which followed the model of the earlier modern movement predecessor, the challenge is to sustain social conscience for intimate civic space and affordable domiciles within the urban real estate. It is a profound idea that the fight for this highly civilizing cause is now led by the luminous presence of a John Hejduk masterpiece as the gauntlet for the battle.


I reiterate that the fight must consider the language used to advocate conditions for the future of the Hedjuk social housing tower and civic courtyard buildings. The project exists within the larger idealistic framework of the IBA. The aspirations addressed before the Wall came down are now being insulted by the threat to this building. The words that insure the survival of authenticity must work toward a continuity of the vision and courage essential to the amazing integration of architecture and the social contract that was achieved with these projects.


The example of resistance John Hejduk set in New York and internationally resulted in his place within the Berlin IBA. He demonstrated that a selective and resistant practice could signify and support humanistic programs and civic ethics. This is the idea central to the survival of his architectural works in their built form. And it is the necessary backbone to any argument that will succeed in saving this Friedrichstrasse project today.


**DIANE LEWIS, Architect and Professor at The Cooper Union, New York City**


[![](media/lwblog-hejduk-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwblog-hejduk-12.jpg)


*Text of petition to save the tower from defacement, sponsored by **RENATA HEJDUK, PhD****., the daughter of John Hejduk:*


To:  BerlinHaus Verwaltung GmbH


Concern is growing in the architectural community at unsympathetic alterations to the late John Hejduk's ‘Kreuzberg Tower' in Berlin – one of only a handful of buildings he realised during his lifetime, and one of the largest of his built works.   After many years of neglect, the buildings are currently undergoing refurbishment works that radically alter the exterior appearance, in particular the colour scheme and significant facade elements.   The alterations offer little or no significant improvement to the apartments or their surroundings, other than repairing the decay the buildings have suffered, and could just as easily, and at no significant additional cost, be achieved in a manner consistent with the original execution.


Doctor Renata Hejduk, daughter of John Hejduk and an architectural historian, contacted the building's new owner, BerlinHaus Verwaltung GmbH, earlier this year to discuss the matter, but received a dismissive response.


We the undersigned:  


– welcome the fact that refurbishment works are being undertaken on these buildings after many years of neglect.


 – object strongly to the nature of the proposed facade alterations, in particular the new colour scheme and replacement of facade elements.  


– regard the design of the alterations as inappropriate to the architect's intent and ruinous to the integrity of the architectural ensemble.


And therefore demand that:  


– work on the facades of the buildings be stopped immediately.  


– the owners undertake a thorough consultation with the Estate of John Hejduk and architectural experts to establish refurbishment guidelines which are both in harmony with the intent of the original, and respect the architect's rights of authorship.  


– amended proposals are developed in collaboration with, and with the approval of the above parties, in order to maintain the architectural integrity of this important cultural artifact.


See the signers of this petition and sign, if you want to help:


<http://www.petitiononline.com/hejduk/petition.html>



#architecture #Berlin #Kreuzburg #tower
 ## Comments 
1. David
3.25.10 / 9pm


“Any echo of the tired attempt to undermine the main thrust of the type of resistance that secured Hejduk's presence among the group of architects who formed the architecture of the city discourse in the discipline rather than the business of architecture debilitates the potential to save this project in a manner that doesn't render it impotent.”


I may not be the only one yet to be shed of fear that this kind of triple-negative doublespeak is not entirely disassociated with the lack of ease with which those who have not had the ostensible benefit of an architectural education imbibe and digest the discourse to which you don't hesitate to praise.
3. aes
3.25.10 / 9pm


thank you Diane Lewis for making an important and articulate case, not only for the defense of this project, but for the defense of an essential kind of architectural practice that this project exemplifies.
5. Derek Fekkai
3.25.10 / 11pm


It is unfortunate that John Hejduk was not an unemployed, unmarried African-American lesbian in a wheelchair. His built argument for the expression of the putative “social contract” would have been so much more poignant if it was coming from a place of experience rather than one of architectural pretension based on a visually cartoonish narrative.
7. TK Robinson
3.26.10 / 1am


I suspect Diane Lewis is an intelligent woman. Certainly she demonstrates an ability to absorb and precisely regurgitate the formulaic linguistic response and predictable sophistry that is so tiresomely common in academia today. But that should not be mistakenly for or confused with serious and experientially-based criticism. Indeed the narrow-minded and insular views she espouses (as evidenced by the jargonistic “archispeak”), does neither herself nor the Kreuzberg Tower any useful service. 


As a plea for preservation this predictable analysis might be effective …. provided the Board representing the building's new owners is heavily populated with Cooper Union graduates, or others easily impressed by the rather tired and cliched liguistic prancing around that passes for “discourse” these days.


But that probability is highly unlikely. I'm not betting in it anyway. So this verbose exercise with two-dollar words grasping at dime-store ideas becomes just another meaningless example of early twenty-first century pseudo babble. Perhaps we should preserve it as such. 


I'll start a petition. Any takers?
9. William Astor
3.26.10 / 10am


When contemporary architects alter a traditional or historic structure with a non-sympathetic addition they describe their design as a legitimate “intervention”. But when one of their buildings are modified, even slightly, they cry “defacement”.


This sort of hypocrisy and the selective application of a double standard undermines the credibility of Diane Lewis' argument.
11. Mark George
3.27.10 / 12am


Personally, I can not look at that building and find an ounce of empathy (let alone generosity) in the design for the inhabitants. I see strong efforts for a strict formalism. I don't see light, air, or any desire to build community.


Changing a quantity of low income housing to upper class housing is an important matter, but preservation of this building as built, does not enter my concerns.
13. e fuller
3.27.10 / 5am


diane lewis had a deeply intellectual and professional relationship with john hedjuk. this should not be an issue in evaluating this article. but the critique has lead me to believe that it is necessary. The content is the character and what is taken otherwise is detrimental to the emotive understanding and value of his work. An architect will be reading this.
15. [chris teeter](http://www.metamechanics.com)
3.29.10 / 12pm


Apologize for the drunkin rant (albanian grain alcohol, who knew)


Its on friedrichstrasse? That's like a museum for great architecture and unbuilt work by mies (the glass skyscraper)


If that was built in the 80s in the kreuzberg I remember that's an amazing bulding. I have gone out to the corb housing building and to be honest its a depressing building, yet still standing and I have a photo of graffit in the stairwell stating “corb du schwein” (corb you pig)  

I bet the graffiti on hejduks building if any is much nicer.


If kreuzberg was like williamsburg 10 years ago I imagine being on friedrichstrasse now is like being located in manhattan.


The developer should market it as living in art or something, its rare existence as one of few hejduks works should be a selling point.
17. [Save Hejduk's Kreuzberg tower! « visual arts lab](http://visualartslab.wordpress.com/2010/04/02/save-hejduks-kreuzberg-tower/)
4.2.10 / 8pm


[…] <https://lebbeuswoods.wordpress.com/2010/03/25/hejduk-and-the-social-contract/> […]
19. [seier+seier](http://www.flickr.com/photos/seier)
4.4.10 / 2pm


so many words. how many have been to see the house?


I went there years ago. I had studied the project beforehand and I didn't like it one bit. the postmodernist images, the symmetry. it seemed so hostile, formal.


when we arrived after hours of seeing the modernist hits and misses of berlin, we were bowled over by the humanity of scale and the poetry of the place. the courtyard garden was wonderful, if somewhat in need of care. wild rabbits were running around between the buildings. but it is exactly the relaxed atmosphere that makes berlin so much more attractive than other german cities.


it struck me that the apparent formality of the plans represented a kind of precision: the relationship between the two housing wings and the courtyard they flank seemed perfect, it was a true social room for the people living there, not some urban in-between. ground floor flats opening onto it. it was wonderful.


and the tower…it is tiny tiny tiny. just one flat per floor, if I remember the plans. it told the story of a very different but equally valid way of urban life: of isolating yourself from the drama of it, keeping it at arms length, staying aloof. at the same time, it was too small to have the problems of typical tower blocks.


when I returned some years later, it was clear that maintenance was an issue. more rabbits too, but that is hardly the most threatening thing you can come across in european social housing. the poetry of the place was still as striking but with the decay, maybe it became a little clearer that hejduk had drawn on some collective recollection of workers housing for an emotional response. well, it worked for me.


social housing or not social housing, that is a question of politics. use of will always change and then change again and again. but some buildings are worth keeping, including their significant details and surfaces…this was one.


the basic quality of the spaces hejduk created will remain, I think, the scale of the place too – and they represent no small achievement on behalf of the architect, but the poetry is lost.
21. aes
4.4.10 / 4pm


Surely belated, but an offering of my two cents nevertheless:


When Colin Rowe critiqued the IBA experiment, he offered two essential insights:


Firstly, that the massive scale of the blocks, combined with restriction place on each architect of being limited to build only on one side of the street, makes the street “a battle between contending powers . . . the street with the IBA is a version of the Rhine, hopelessly flowing between France and Germany.” 


Secondly, that the program of the IBA should not have been limited to housing only, and that, for all its admirable achievements, it should have gone farther– and that in its inception it lacked the public realm, the res publica.


So the implicit critique is this: That the architect, not the politicians, should give form to the res publica, that the architect is not merely a builder of objects, or blocks, but the builder (and guardian) of social fabric.


To me, among all the projects the IBA produced, many of them undoubtedly beautiful and sometimes even consequential, among all the dizzying array of then- and future-star-architects and their virtuosic buildings (what Rowe termed as a “species of architectural zoo”)– to me, it seems that Hejduk's project is the only project that addresses these two critiques, not by filling-in and covering-over, but by crystallizing the void. The two wings frame the space, and the tower marks it and gives it scale, as does a needle in a piazza, or the towers in Bologna. 


Hejduk's was the only IBA project that re-built (by understanding what not to build) what Rowe referred to as Hannah Arendt's table– without which “two persons sitting opposite to each other [would be] no longer separated but would be entirely unrelated by anything concrete.”


I have been to see this house, this place, and I understand it not only to be a spatial void but an historical one, a charged and concrete void that lends Berlin not only room, but memory. The defense of this rare and beautiful work is not only a defense of a single work of architecture, but a defense of a kind of architectural practice that guards the quality of a city.
23. [seier+seier](http://www.flickr.com/photos/seier)
4.5.10 / 10am


incredibly, the owners appear to have stopped work on site following the reactions on the internet. maybe the buildings can still be saved – in some form…





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.5.10 / 6pm
	
	
	seier+seier: thank you for the update.
25. [Berlin masks: de-facing of social housing « mnemeden](http://mnemeden.wordpress.com/2010/04/08/berlin-masks-de-facing-of-social-housing/)
4.7.10 / 10pm


[…] <https://lebbeuswoods.wordpress.com/2010/03/25/hejduk-and-the-social-contract/> […]
27. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2010/03/25/hejduk-and-the-social-contract/)
4.8.10 / 4pm


**Social comments and analytics for this post…**


This post was mentioned on Twitter by troubleinxanadu: Lebbeus Woods wants to preserve the philosophical intent of Hejduk's social housing in addition to its physical form. <http://bit.ly/cILQyE>…
