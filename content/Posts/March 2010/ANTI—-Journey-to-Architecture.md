
---
title: ANTI—-Journey to Architecture
date: 2010-03-09 00:00:00
---

# ANTI—-Journey to Architecture


[![](media/front-cover-lr.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/front-cover-lr.jpg)


*(above) Front cover of the book, as conceived and designed by Raimund Abraham and LW.*


Raimund Abraham and I were colleagues for more than twenty years. We taught at The Cooper Union and met many times on reviews of students' work, when we sometimes clashed, which was inevitable, given our different philosophies about architecture. We even co-taught two design studios, thanks to the benevolent mischievousness of our Dean, John Hejduk, who loved to stir things up for the students and faculty, keeping everyone on their toes. Both studios were disappointing in terms of the resulting projects, not for lack of collegial effort on our part, nor on the part of the students—but I suppose you can stretch the dialectic just so far. The students survived the experiment and moved on—a testament to their resilience and intelligence, and in some strange way, the experiences brought Raimund and I closer together. Gradually, but surely, and almost because of our differences, we became friends.


Now maybe it is a ‘male thing,' or just the peculiar natures of Raimund and I, but friendship is ultimately founded on mutual respect. In our case, respect could only have to do with our work in architecture. Raimund used to say to me, “there's only a few of us left.” It would be easy to dismiss this as an old man's sentiment about the dying off of one's generation and the things it valued most, except that Raimund was not an old man. His mind and spirit were young. Being a teacher and working with young and aspiring people, always going with them toward the horizon of their dreams and hopes, keeps a man young, whatever his age. But there is more. Raimund was always pushing himself in his projects, always testing his assumptions, his beliefs. So, what he meant by his remark is that so few architects who reach maturity in their work can overcome the seduction of success, if they achieve it, and stay true to their youthful ideals, whatever the cost, even if that cost is their success. And he was right.


In the summer of 2007, Raimund and I met, as we occasionally did, for lunch at the BBar on the Bowery. Without preamble, he said that we should go to La Tourette for a week, stay in the monk's cells, meet daily and discuss architecture. Not so much the state of architecture generally, but our own work and the quite different, even conflicting ideas that drove it. It was a surprising proposal, and a daunting prospect, because I knew from past encounters that Raimund was extraordinarily quick, insightful, and articulate. A most formidable adversary, if that is what he was to be. Still, and for reasons I cannot say or do not know, I agreed.


So, in October of that year, we met at JFK airport, air tickets in hand, and set off on our journey. It was a wild trip, poorly planned, no doubt intentionally so by both of us, that took us by car from Zurich to Ronchamp, Besançon and the Royal Salt Works by Ledoux, to Lyon and Eveux-sur-L'Abresle and La Tourette, to Firminy and back across France and Switzerland to Zurich, with many unecpected incidents and adventures along the way. At the heart of the seven days we spent together were many discussions, dialogues and plain disputes about the nature of architecture, carried on from a morning cafe where we were accosted by two drunken prostitutes, to a cold dark monk's cell in Le Corbusier's monastery. When we returned to New York, we met and continued our discourse, as Raimund preferred to call it, and while we never concluded our talks, we both understood that there was no conclusion, but only continued journeys into the realm of ideas.


At the time of Raimund's death, we were working on a book about our journey and the discussions that occurred along the way. It is my goal—no, my mission—to see the book completed and published.


LW


*(below) Back cover of the book, as conceived and designed by RA and LW:*


[![](media/back-cover-lr2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/back-cover-lr2.jpg)



 ## Comments 
1. Jeff Brown
3.9.10 / 3am


Beautiful!  

jeff
3. [Kyu Kim](http://www.designalternatives.org)
3.9.10 / 4am


mission possible.
5. lturner
3.9.10 / 4am


I can't wait to see the book, Lebbeus. I'm sorry for your loss, and for architecture with RA's passing.
7. Mark George
3.9.10 / 9am


You describe a friendship and professional relationship I hope to enjoy as deeply and fruitfully with friends and colleagues as yourself and Abraham.


Thanks for sharing this.
9. [Woods on Abraham | no ideas but in things](http://www.quangtruong.net/?p=986)
3.9.10 / 2pm


[…] I have to point to Lebbeus Woods blog, which is always a joy to read–an amazing ongoing document of a restless and inquisitive […]
11. sos
3.10.10 / 12am


power to the ANTI
13. Francisco
3.10.10 / 4am


Raimundo was teaching one the most beautiful studios at sciarc in the last years. I wanted to be in his class in the future… 


Anyways his last lecture was really powerful. I would never forget it.
15. Christopher Lauriat
3.10.10 / 4pm


I was sorry to hear of Abraham's passing. May the void he leaves behind be filled with equal, no, surpassing intensity and inquiry. Thank you for sharing your thoughts, here and in the book to come. I look forward to the challenges and insights your long collaboration will most assuredly offer.
17. [Miller Taylor](http://www.middle-grey.com/images/)
3.23.10 / 4pm


We've all lost a very powerful force. This project, I'm sure, will illustrate the intensity you both have brought to the field. 


I eagerly await what will surely be a meaningful and insightful collaboration.
