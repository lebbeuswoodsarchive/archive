
---
title: SCOLARI’S SHADOWS
date: 2010-03-04 00:00:00
---

# SCOLARI'S SHADOWS


I was preparing a post on Massimo Scolari's ink drawings, particularly those lingering at the edge of darkness, when word of Raimund Abraham's death reached me. I post the drawings now without further comment.


LW


[![](media/sc-8a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-8a1.jpg)


[![](media/sc-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-2a.jpg)


[![](media/sc-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-3a.jpg)


[![](media/sc-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-4a.jpg)


[![](media/sc-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-6a.jpg)


[![](media/sc-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-7a.jpg)


[![](media/sc-5a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-5a1.jpg)


[![](media/sc-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/sc-1a.jpg)



 ## Comments 
1. [Michael Phillip Pearce](http://www.carbon-vudu.us)
3.5.10 / 4am


Darkness is a beautiful experience, even more so when the fear is gone and appreciation sets in. Fascinating drawings. I wonder if death is like this…  

Thanks LW!
3. Alejandro
3.5.10 / 9am


Bravo
5. [Fontana a Monselice | Massimo Scolari « arkinet](http://arkinetblog.wordpress.com/2010/03/05/fontana-a-monselice-massimo-scolari/)
3.5.10 / 10am


[…] painter and designer. We found his work trough a post published today at Lebbeus Woods's BLOG and foud his fascinating ink drawings, among some paintings and projects. Scolari graduated in […]
7. [more « Imagination](http://archimagination.wordpress.com/2011/12/23/more/)
12.23.11 / 2pm


[…] SCOLARI'S SHADOWS […]
