
---
title: of A HUMAN NATURE
date: 2010-03-31 00:00:00
---

# of A HUMAN NATURE


[![](media/lwb-arcadia-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwb-arcadia-1a.jpg)


Appearances do not lie,


or nothing is true.


Beneath them, then, a layer,


unseen and out of reach,


mocks the idea of order.


[![](media/lwb-arcadia-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwb-arcadia-31.jpg)



Difference shifts. Free forces


become unexpected edges.


Still, difference defends each,


and the form cannot again


ever be the same.


[![](media/lwb-arcadia-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwb-arcadia-2.jpg)


The house that is


a fragment of itself,


becomes the world.


Shared by two orders,


It is inhabited only once.



[![](media/lwb-arcadia-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/lwb-arcadia-4a.jpg)


Architecture's quality


might rightly be judged,


not by the problems it solves,


but by the problems


it creates.



LW









 ## Comments 
1. [Michael Phillip Pearce](http://www.carbon-vudu.us)
4.1.10 / 3am


Love this type of post and perhaps one of the most amazing topics Humans and Nature. My precept of this anthropomorphic montage of images is seeing it through your vision of order, human order, LW. I like how your plant is clearly a LW plant, and not just an ornamental steel replication of a strict interpretation or form copy of perceived biology. 


Anthropomorphically speaking, from the way my brain transfers knowledge, [The world of my human nature], image 2: I see a sentry, a facial sentry, of multiple face like those of helmet profiles in the left structure. And, in the middle would be a woman, a 1940's era woman being protected as chivalry such in the day or perhaps it is a child with what feels like a yellow contrasting dress to the figure or structure to the far right… see the legs of the man and his back to us with his arm around her. A protective huddle so to speak. It should almost be raining.


Image 3: the obvious eyes and nose of the face, protected by a sheet of steel sheets, with a aft and near probe study. A sheet of protection from nature perhaps, everything we our and fear. I do not know…but the anthro-eyes, almost as blunt as the Big Brother Posters is watching you back in the day.


Image 4: A folly. This is not fear but play. the bent over anthro-structure, that appears to have a itch that is being scratched on the right . The left anthro-structure, seems to be looking at a spectacle from the left, where the action just may be, a regatta guessing from the nauticalness of the sketch.


This may be a deep tangent, but this is what I perceive on a Rorschach level of sketching and exploring thoughts. It is amazing we all see humans and nature differently, is this of “A Human Nature”? Yet we seem to crave the same, humans, the touch, the embrace and nature, the wild untamed spirit of what we are.
3. Ken Jones
4.1.10 / 5am


That just might be the best thing you've said yet.  

And only from you did I learn this thing.  

It has put so much of the human condition into a new and vivid perspective for me.  

Why great artistic achievements are indeed great.  

Thank you.
5. [wm](http://www.asasku.blogspot.com)
4.2.10 / 4am


You reminded me of why i fell in love with architecture in the first place..


Thank you LW
7. Sam
4.3.10 / 12am


Your work has dramatically inspired me. With today's actualized buildings giving into commercialism and globalization. It is ideas like this that gives me hope for the future. Like Artaud, who tried to put a death to literature said in his manifesto, “The truth of life lies in the impulsiveness of matter. The mind of man has been poisoned by concepts. Do not ask him to be content, ask him only to be calm, to believe that he has found his place. But only the madman is really calm.”
9. [Chris Teeter](http://www.metamechanics.com)
4.3.10 / 10pm


Sam nice qoute, i'm keeping that one.


LW, I showed some of your work from the Radical Reconstruction book to the rendering class and literally it was a gasp and an aww sound…trying to get them inspired beyond realistic overly photoshoped images…(that sells architecture)…then you buy the apartment and well it's not quite as sunny as the rendering had shown it.


if i were to reinterpret this…science is post rationalization, concepts are abstractions of abstractions…the computer (doing math quickly) makes the abstractions too accurate describers of reality, fooling us into believing the world of matter is formed by concepts, from mind to matter…the materials/forces (E=mc^2) underneath our perception/mind make those differences which force the edges of perceived existince (like you put it so well)..and matter yet again forces our mind to change.


your images make me think about that Matrix qoute by Mr Smith, humans may really be a virus…or of alien descent (god, whatever you like to call pre-historic super beings)


thanks for the moderation, maybe i should look into it, but then again Hyde should push matter to the edges of Dr Jecklys conceptual universe, liquid exposes matter sometimes.
11. blackdog
4.7.10 / 9am


It could be a new idea of simbiosis betwen nature and architecture
13. [Shlomo Korzac](http://kafee.wordpress.com)
4.9.10 / 7am


What I love in those drawings is not only their implicit and explicit embedded linguistic rhetoric, but also and mainly their unusual visual impact inducing a streaming need for more drawings awaiting an empty drawing paper.
