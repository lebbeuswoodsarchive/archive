
---
title: ANOTHER ONE
date: 2010-12-13 00:00:00
---

# ANOTHER ONE


We'd better get used to it. This is the face of the future. New cities—or city wannabes—built from scratch on cheap land somewhere, anywhere, nowhere. Utopian visions sponsored by, built by, paid for by private developers backed by state capitalism. In the case below, the place is Saudi Arabia, looking very like [Ordos](https://lebbeuswoods.wordpress.com/2010/10/20/ordos-revisited/), Inner Mongolia, and Dubai's [displaced Manhattan](https://lebbeuswoods.wordpress.com/2008/03/05/delirious-dubai/) by Koolhaas. A formula that works, so it seems. New versions are just over the horizon.


.


*In this post, captions are below their respective images.*


[![](media/saudi-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-1.jpg)


[![](media/saudi-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-1a.jpg)


.


[![](media/saudi-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-2.jpg)


[![](media/saudi-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-2a.jpg)


.


[![](media/saudi-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-3.jpg)


[![](media/saudi-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-3a.jpg)


.


[![](media/saudi-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-4.jpg)


[![](media/saudi-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-4a.jpg)


.


[![](media/saudi-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-5.jpg)


[![](media/saudi-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-5a.jpg)


.


[![](media/saudi-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-6.jpg)


[![](media/saudi-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-6a.jpg)


‘


[![](media/saudi-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-7.jpg)


[![](media/saudi-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-7a.jpg)


.


[![](media/saudi-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-8.jpg)


[![](media/saudi-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-8a.jpg)


.


[![](media/saudi-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-9.jpg)


[![](media/saudi-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-9a.jpg)


.


[![](media/saudi-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-10.jpg)


[![](media/saudi-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-10a.jpg)


.


[![](media/saudi-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-11.jpg)


[![](media/saudi-11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/saudi-11a.jpg)


*The above is excerpted from an [article](http://www.nytimes.com/2010/12/13/arts/design/13desert.html?ref=arts) in the New York Times.*


.



 ## Comments 
1. Pedro esteban
12.14.10 / 7pm


Why this??? Why?? Until when architects are to take this abuse?  

And not only Saudi Arabia, it is all over the world. See Florida.  

The worst is the price of all those structures!!! Mies was right less is more…….. in some contexts!





	1. Biel
	12.15.10 / 11am
	
	
	Mies wasn't right, less is less…thats why modernism is dead, he took modernism with him a few years before he died…and certainly nothing left for us…Parametricism???hehe
	
	
	
	
	
		1. Pedro Esteban
		12.16.10 / 10pm
		
		
		Parametricism isn't similar to less is more?
		3. [Pedro P](http://www.ofiarte.pt)
		1.8.11 / 2am
		
		
		Mies vision will always be alive. The real truth of things lies in their essence. His words and architectural approach follows that rule – Less is More.. More space and more time for what really matters. Decorative posture leads to visual pollution and can only be pleasant to eye, in a illusional sense.
3. [Stephen](http://skorbichartist.blogspot.com/)
12.16.10 / 4pm


It seems like this the architectural connection to your previous post.  

These projects are responding to peoples inner desires.  

I know too many people who feel that Disneyland is truly the happiest place on earth.  

The Kool-Aid tastes sweet.





	1. Pedro Esteban
	12.16.10 / 11pm
	
	
	The Kool-Aid tastes sweet.
	
	
	My mom once goes out of Cuba, goes with the family to Dysneyland and told me: you will find a lot of job as an architect on Dysneiland, because there are so many things creatives !!!  
	
	I preffered don't answer to my unconscious mom, the architecture is contaminated, see how the virus works.
	
	
	On Cuba there is a new ´´movement of architecture´´ by ¨own means¨, it's awfull, the population has become architects!!! It's quite destructible to the city, it's different to this subject but, at the end shows how much like the Kool-Aid.  
	
	LW do you will be interested on publish some example of this on Cuba?
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		12.17.10 / 1am
		
		
		Pedro Esteban: Yes. Do you have written and visual material that I could post? Do let me know. It sounds trite to say, but I love Havana and its people. But I hate the style of Soviet Communism that Fidel tried to implement there, with long-term disastrous results. He could have created a uniquely Cuban style of socialism, with the same style of clay-tile architecture that was already growing at the music, dance and arts center in the outskirts of Havana, now a mostly neglected ruin. Instead he sold out to the USSR and the Eastern Bloc. What a tragically wrong decision! Please send me anything you have to [lebbeuswoods@mac.com](mailto:lebbeuswoods@mac.com).
5. Pedro Esteban
12.17.10 / 2pm


Here goes a large one!!! haha This is an habitual topic!!  

I love the habaneros and the city as you, and more one mojito on Bodeguita!!!!!!


In my life has read a better comment, of the reality of architecture, in Cuba!!!! Totally agree with you, in fact I say the same of architecture in Cuba, why we sold our tropical way to the coldest russian construction??? But all the revolutions in the world has done the same!! The russians with constructivism is the most clearly example, the other day was speculating if the russian revolution will accept the constructivism as his formal(in every sense) language, interesting things will happend!!!!


The schools of art, the more revolutionary architecture, the architecture who Fidel (and all that name means, socialism etc) was looking for!!! But he don't see this on that moment. In fact schools was seems like a reminiscence of capitalism; at all, the architects who make the schools has thought that architecture, on that way!! But……  

In this moment are trying to save the schools, a proccess quite non ethic, after all the repudiation who they has suffer. 


I don't think the architecture of the population who I speak about here is, that interesting architecture, you has seeing on Cuba on Habana Vieja, where persons without money and without SPACE have creating a new way of make architecture( you drawed this new way, on your projects for Habana)  

This is the other part of that architecture, because this new architecture is from a new emergent class (yes!!!! in the socialism, exist some kind of ”classes”!!!)  

They have more money and space. They want to express his superior economical situation trought the architecture, and in Cuba the visuality is so poor, that they are beginnig to make an eclectic architecture and the kitsch has become the new language, but, a quite different kistch!!! Miami is the new aspiration and from there, they takes examples!! Too is very often the use of classic architecture, first because the human has in his brains some milleniums of classic architecture, and if you add to this, the fact that Eusebio Leal put the classic architecture on the cubans as the right way, of course we do an eclectic architecture.  

Cubans, at the end, are adopting the language of the new bourgeoisie, the kitsch, but this is possible on a socialist society?? Yes, go to Cuba, and after drink your Mojito, go to see the other face!!! At the end the process sounds quite interesting but formally isn't the same as people make on Habana VIeja.


Now I'm on Italy, at least, can go out!!! I don't have pictures with me of that process, I can ask to some friends if they have something, and I send it to you. I'm sorry but, my writings, are in spanish, thanks any way. Sorry for the english!!
7. dana
12.30.10 / 5pm


I figured you were super busy. Obviously. How do you like teaching at the cooper union? – do you mind sharing what class(es) you teach? I have gone through your education pieces, that seem to involve general ideas on study…not sure exactly what level we are talking about—undergrad/grad? have you ever taught undergrad or seriously looked into their education/have some opinion on the undergrad programs in this country? 


 “In any event, there are two groups of people unconcerned about professional degrees: those who want to study architecture, but not practice it, and the idealists, who will find their own ways to practice, and on their own terms.” I smile at this. At 27 years young, as a female, with no degree – it feels really good to keep this dream alive, with words like this.


I wonder how often you give a talk – it would be nice for you to have a schedule of events prior to the actual event on your blog here… especially if you are open to seminars/wrkshops on your drawing. 


LW, thank you for responding. I'm sorry this is not the page to be discussing any of this… clearly, after not saying anything for so long, waiting in anticipation -… it was my intention to prompt you. =)
