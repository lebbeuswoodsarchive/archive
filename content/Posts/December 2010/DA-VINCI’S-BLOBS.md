
---
title: DA VINCI’S BLOBS
date: 2010-12-03 00:00:00 
tags: 
    - blob_architecture
    - drawing
    - Leonardo_Da_Vinci
---

# DA VINCI'S BLOBS


[![](media/blobs-1d-det1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1d-det1.jpg)


Blobs were all the rage in architecture a few years back—does anyone recall? While that fashion has passed, to be replaced by ‘parametrics,' these two trends do share the characteristic of being inspired by the ease with which digital computers can generate complex shapes. Or perhaps in the case of blobs we should say ‘non-shapes?' given that these particular forms have, shall we say, *fluid* boundaries, evoking indeterminacy and something in continual flux. Hence their generic name, which may have its roots in the science fiction movie about an alien lifeform that oozed its omnivorous way through a small American town in the 1950s. In any event, and putting aside the omnivorous-ness of digital computation, blobs have more of a history than was generally acknowledged at the height of their popularity, one that reaches back to pre-computer days—the ‘inflatables' of the 60s, pushed to prominence by Haus-Rucker, Coop Himmelblau, and Archigram—and much, much further back to, well, the Italian Renaissance.


Now, it is true that no architect of that period proposed blob buildings. But it was a time when artists—and most notably [Leonardo Da Vinci](http://en.wikipedia.org/wiki/Leonardo_da_Vinci)—were taking an intense interest in the exact workings of nature. Da Vinci, as well as Michelangelo, dissected cadavers in order to understand the human body's musculature and skeletal structure, the better to paint its form and movement convincingly. Da Vinci, and later, Durer, studied plants in vivid detail, satisfying their innate curiosity, no doubt, but again with the aim of painting them as living things and not as mere props. In their art, they had the great ambition to project an entire living world, in which the human and the natural were co-dependent and unified not through mere symbolism but by a richness of differences and diversity. Da Vinci's paintings were philosophical in a sense that paintings before his had never been, manifesting what we today call humanist philosophy, which gives human beings the central role because of their ability to understand all other things. His paintings are, in an exquisitely inspired way, ‘encyclopedic.'


For Da Vinci, drawing was his prime means of analyzing the phenomena of the living world. Painting was the synthesis. Analyzing in the way I use it here, however, is not ‘taking apart' something observed or experienced, nor is synthesis ‘putting parts back together.'  Da Vinci's method of analysis was by *analogy*. Rather than pick apart a phenomenon, separating what he perceived as its components, he created in a drawing a parallel world, an analog to reality. Working with analogs, he could emphasize the features of phenomena he considered most important. Da Vinci's blobs—drawn masses of turbulent water or stormy air—are prime examples of his analogical method of analysis.


Looking at a rapidly flowing stream or a thunderstorm leaves a strong visual impression, but many aspects of what is actually happening remain hidden from or are simply beyond the reach of observation, either by the naked eye or instruments. They have to be inferred from what can be observed, and this is a matter of interpretation, of imagination. It is very much the method Albert Einstein used in developing his theories of Relativity, because he could not directly observe objects moving close to the speed of light, or the movements of stars in interstellar space. In science it is called making a hypothesis, and the application of this method took modern physics far beyond empiricism (Newton had proudly claimed, “I make no hypotheses”), which was based strictly on what could be observed. Da Vinci, in this way as in others, anticipated future developments—he created hypothetical worlds that revealed the hidden structures of nature. These, in turn, helped him create paintings of great originality that are imbued with a lasting aura of conceptual power.


Ironically, Da Vinci was—as an architect—derivative of other architects of his time, such as Filarete. It never occurred to him to propose his ethereal fluid blobs as habitable structures as, most notably in our time, Yves Klein did with his Air Architecture, and most recently, Diller+Scofidio, with their [Blur Building.](http://www.designboom.com/eng/funclub/dillerscofidio.html) Interesting enough, though, Da Vinci drew his storms engulfing landscapes, villages, and their inhabitants, inadvertently imagining an architecture of radical transformation, close to the spirit of our own times.


.


(following below) The ‘blob' drawings of Leonardo Da Vinci:


Again and again, Da Vinci hypothesized in drawings the flow of water, continually refining his understanding of it in all its differences and subtle detail:


[![](media/blobs-1b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1b.jpg)


[![](media/blobs-1b-det.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1b-det.jpg)


[![](media/blobs-1c.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1c.jpg)


(below) A relatively rare example of a ‘taking apart' form of analysis. An aside: there has always been speculation about why Da Vinci wrote “backwards.” Note here how a few numbers are not written backwards. This would argue against one prominent theory—that he was dyslexic. My own favorite  theory is that, since he was left-handed, he wrote from right to left in order to avoid smudging the ink as he wrote. It was easiest to write the sequence of letters the same—and to think NOT backwards but IN REVERSE:


[![](media/blobs-1c-det.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1c-det.jpg)


(below) Turbulence in a smoothly flowing stream of water (or air) is caused by the insertion into the stream of an obstacle. Da Vinci made many studies of such a situation and was especially interested in the effect of geometric obstacles. A completely different form of turbulence is caused by a smoothly flowing fluid stream pouring into—and colliding with—a still fluid body.


[![](media/blobs-1d.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-1d.jpg)


(below) In Da Vinci's analog, a fluid stream is composed of a bundle of smaller streams, or strands, much like human hair. By emphasizing this feature, he is able to determine how the different strands interact with each other, creating an overall effect. He knew that actual water did not look like this but, he understood, was structured in an analogous way:


[![](media/blobs-3a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-3a-2.jpg)


A note on spirals: When any particle with mass begins to lose energy (a particle of water or a sub-atomic particle) it enters a spiral until its energy is spent. It is more than interesting that Da Vinci hypothesized this in his drawings, as it difficult if not impossible to observe with the unaided eye.


[![](media/blobs-2a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-2a1.jpg)


(below) Storms in the fluid atmosphere behave similarly to water, in moving strands or streams, but interact in a less contained, less bounded form. His storm studies always reflect the storms' much larger scale and more destructive power, as they are often shown driving lakes or seas before them against vulnerable landscapes and towns. It is important to note that while he used pen and ink, or a finely sharpened chalk, to make his water studies, he used only a blunt piece of chalk for his studies of storms:


[![](media/blobs-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-4a.jpg)


[![](media/blobs-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-5a.jpg)


[![](media/blobs-5a-det.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-5a-det.jpg)


[![](media/blobs-5b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-5b.jpg)


[![](media/blobs-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-6a.jpg)


[![](media/blobs-6b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-6b.jpg)


(below) For this study, which is more of water and wind than of storm, he used sharpened chalk and pen and ink:


[![](media/blobs-7-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/blobs-7-2a.jpg)


In some ways, Da Vinci's drawings are warnings, yet they are not without hope. Human beings cannot control nature, whose power can destroy them, but through their understanding of it, they can adapt themselves when necessary. His understanding of the changing forces liberated by fluid dynamics anticipates key developments in modern science and art. Arguably, he was the first architect of their indeterminate form.


LW


#blob_architecture #drawing #Leonardo_Da_Vinci
 ## Comments 
1. tom
12.4.10 / 1pm


It is very interesting what you are talking about, though a little ironic these days.


On the last few days, there is a huge fire/ Conflagration that stroke the slopes of Mount Carmel (Haife, Israel). The bloody fire has killed 41 people, and still counting.


A Conflagration is a random phenomenon, an uncontrolled burning that the Humans cannot control, and whose power can destroy them and anything they ever built.


but on this case (unlike, ” understanding of the changing forces liberated by fluid dynamics”) even though the humanity could understand the power and the danger  

 of the flames, the firefighters (or should I say fire-men) cannot predict any thing because of the misleading winds whose change the hole perspective. 


Beyond that, for my opinion the disaster would provide us some wonderful pictures – An aesthetics of destruction.


Condolences to the families.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.4.10 / 3pm
	
	
	tom: It is sad to say that at any given moment, somewhere in the world, human life and property are being destroyed by some form of ‘natural disaster.' In the majority of cases, the cause is not nature itself, but the human decision to build in a place or a manner that has a well-known chance of being struck hard by earthquake, hurricane, tsunami, wildfire. This does not make the human loss less poignant or tragic. We humans are intrepid creatures. We take chances, and sometimes we lose. Our redeeming quality is that we are capable of learning from our mistakes, especially when the loss is terrible. Maybe the ‘pictures' you speak of will help us do that.
3. [Diego](http://opacos.wordpress.com/)
12.5.10 / 12pm


As an architect I see these poetic strategies necessary to get a project that can be called architecture, but beyond experimentation, when you receive a private commission, your freedom is very limited and is difficult to maintain the poetic force of the original idea. Maybe I'm aesthetically ambitious, but I worry because it is private development the more present in the landscape, and not works arising from competition or customers with notions of aesthetics and architecture.
5. Andrei
12.6.10 / 7am


Hello Mr Woods, this is little off topic but I was looking at your website recently and i was very intrigued by the T-knot Chengdhu project and wanted to know a little bit more about the concept. An image that was particularly interesting was a computer model of your construct which started to bring the unforgiving precision and reality to an otherwise fantastical proposal: the question that bothers me is how did you reconcile the idea of freespace with clearly defined typology described on the website as “technological pavilion” (or if freespace has any role in this at all).


More on the topic, however, we are all products of our time and the men of the Renaissance period were essentially on a quest to finding the absolute truth through rigorous analysis of natural phenomenon: there are presumably rules by which matter behaves: and it is true for almost all of the observed things man has encountered until that point. Liquid state of matter, as you pointed out, can be analyzed by reducing it down to trajectory lines that interact with objects, other lines or themselves creating almost a blob-like geometry from clear rule-based interactions (which computers have made increasingly more manageable to grasp and represent over the last decade or so). 


In my view, however (this is my opinion mind you), the work of LW stands in stark contrast: it is not rule based but quite the opposite: a study of indeterministic interactions, a Heisenberg uncertainty principle: they are not deterministic trajectory lines but quantum shifts that behave in a manner that undermines the very UNDERSTANDING of knowledge. These particle waves/trajectories display rules within rules within rules. 


But what I am not a fan of is blob architecture: in a lot of instances it feels like form for the sake of form. Its not like we are trying to understand the underlying principles behind these interactions because of better tools: architects have a new toy so they play with it to no end: the resultant is simply new forms. There is either standard boring program embedded in a new shell (think Gehry) or no program at all! To me form is irrelevant: what architect is trying to do is a much more important question in my opinion.
7. [SLIPSTREAMING « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/18/slipstreaming-2/)
12.18.10 / 1am


[…] turbulence of a waterfall on a placid pond, or of a thunderstorm breaking suddenly in calm skies. Da Vinci's drawings capture the drama of such violent events but were not mere illustrations of them. Rather, their […]
9. [SOTIRIOS KOTOULAS: Emission Architecture « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/27/sotirios-kotoulas-emission-architecture/)
1.27.11 / 3pm


[…] on two previous posts (1, 2) concerned with the structure of fluid dynamic bodies, the following research by Sotirios […]
