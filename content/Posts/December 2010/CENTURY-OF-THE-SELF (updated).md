
---
title: CENTURY OF THE SELF (updated)
date: 2010-12-18 00:00:00
---

# CENTURY OF THE SELF (updated)


[![](media/centself-4b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/centself-4b.jpg)


*The following is a commentary by [George Prochnik](http://www.nytimes.com/2006/12/24/books/review/Kramer.t.html) about the documentary* The Century of the Self, *one of the most controversial films broadcast on the BBC in recent years. While it is not concerned with the field of architecture in a direct way, both the film and the commentary engage contemporary issues that clearly implicate architecture in what Prochnik calls a ‘grand conspiracy theory.' In my view, they are required seeing and reading for any architect working today.*


*The four parts of* The Century of the Self *were shown in two installments—Parts 1 and 2 on November 12; Parts 3 and 4 on December 10, 2010—at the Theresa Lang Center of [The New School](http://www.newschool.edu/) and were co-sponsored by the [National Psychological Association for Psychoanalysis (NPAP)](http://www.npap.org), [Cabinet Magazine](http://www.cabinetmagazine.org), and The New School. Prochnik gave a version of his commentary and answered questions at the latter showing.*


The Century of the Self *can be seen on YouTube. Following are links to Parts Three and Four, which relate most directly to the commentary:*


PART THREE  **There's a Policeman Inside Our Heads,** [part 1](http://www.youtube.com/watch?v=AzE9ocIGVlQ&feature=related), [part 2](http://www.youtube.com/watch?v=k2VtKHIYKPM&feature=related), [part 3](http://www.youtube.com/watch?v=VdnyY2xNcA8&feature=related), [part 4](http://www.youtube.com/watch?v=XPFFSRoBqhc&feature=related), [part 5](http://www.youtube.com/watch?v=UNBXfo8TFGs&feature=related), [part 6](http://www.youtube.com/watch?v=R3dqvg1T1L8&feature=related).


PART FOUR  **Eight People Sipping Wine in Kettering,** [part 1](http://www.youtube.com/watch?v=R1VJrp0IvPs), [part 2](http://www.youtube.com/watch?v=1cuG_Dp2Gqk&feature=related), [part 3](http://www.youtube.com/watch?v=5beead2uWmQ&feature=related), [part 4](http://www.youtube.com/watch?v=9u1b1j0kKpI&feature=related), [part 5](http://www.youtube.com/watch?v=34eFXaXVwmY&feature=related), [part 6](http://www.youtube.com/watch?v=2Tdwg9YHTlw).


## Free downloading and streaming of the entire film:


<http://www.archive.org/details/AdaCurtisCenturyoftheSelf_0>


.


*The Prochnik commentary:*


How did British and American liberal politics devolve to the point where eight people sipping wine and popping back Cheerios have gained the power to define the policy choices of a national leader? How did our society become so selfish, trivial, materialistic and enslaved to its bottom-most register of desires? What enabled our businesses to gain a dictatorial control over the public imagination? Why did everything get so dark, dystopic and just plain dumb?


Adam Curtis's important film, *The Century of the Self*, supplies an answer to all these questions—and more. “There's A Policeman Inside All Our Heads: He Must Be Destroyed,” and “Eight People Sipping Wine in Kettering” are the two concluding episodes of a sweeping production. Curtis tracks the path by which a broad cultural revulsion against the notion of being possessed by corporate *sprachen* inspired a multifold liberation movement that crumpled into a new, ultimately vapid, ideal of self-empowerment, which, in turn, provided fertile soil for big business interests to sow a hyper-crop of rampant consumer desire. For the final twist, Curtis shows how the techniques developed by corporations to sell products were imported into the political arena. As mass surveys, aimed at determining how best to pander to lifestyle preferences of swing voters, became the strategy-of-choice for the Democratic Party under Clinton, and then—copycat killer-style—for New Labor in the election that brought Tony Blair to power, the higher social values of liberal politics became inexpedient, lost in a wave of redundancies. These two episodes present a devastating portrait of the reduction of the great democratic experiment to radically self-centered *niche*politics. The grand social collective doodles off into cocktail party banter.


There is, undoubtedly, a tremendous amount to learn here. No one who cares about—well—anything worth caring about, could applaud what passes for political process in this country today. We all want to know how we got here. Many of us, whatever historical methodology we espouse, cling to the hope that grasping how we arrived where we are today is theprerequisite to getting somewhere more palatable tomorrow.


Adam Curtis draws a crystalline vector of the steps that led from ‘there' to ‘here'. Grossly telescoped, Curtis's arc to the apocalypse of automaton-frivolousness goes something like this: Sigmund Freud develops a theory of psychology based on the premise that vast, dark, irrational, unconscious forces drive the individual, and that these forces must be restrained if civilization is not to be drawn into the abyss. Inheriting the mantle of her father's movement, Anna Freud adopts an even more restrictive, lockstep structural approach to both the internal politics and practice of psychoanalysis—one that cripples the lives of the more heterodox analysts and stifles many children who come under her sway. Freud's nephew, Edward Bernays, takes up Freud's theories and realizes that the same irrational forces his uncle had identified as destructive, could be tapped by corporations to make consumers see specific products as capable of satisfying their deepest, most primitive desires, while concurrently emancipating them from various socially-imposed shackles. Corporations take up the theories of Edward Bernays and establish focus groups geared to understand how to cater to a radicalized youth movement by creating a new consumer landscape of insidiously individualized products. Finally, in a process wherein Freud's great-grandson, Matthew Freud, plays a seminal role in Britain, politicians adopt the practices the corporate world had itself previously adopted from Freud's nephew.  And yet, Robert Reich eloquently remarks near the end of the film, “Politics and leadership are about engaging the public in a rational discussion and deliberation about what is best and treating people with respect in term of their rational abilities to debate what is best. If it's not that, if it is Freudian, if it is basically a matter of appealing to the same basic unconscious feelings that business appeals to, then, why not let business do it?”


If we follow the dancing cigar from the beginning of the film to the end, the narrative goes: Freud to Freud, to Freud to Freud—to contemporary corporate fascism.


 


*The Century of the Self* is intelligent, often mesmerizing and, I would argue, ultimately utterly mad. Mad not in its details, which are often sharp and revealing; but in all that's forgotten and which, in its potent absence, having become subject to a kind of massive anti-Freudian slip, enables the dots to be connected in the neat, master-convergence style common to all grand conspiracy theories.


The problem is not, really, that the movie betrays a kind of occult fascination with the Freud family gene pool, one which hints at a primitive curse passing through the different avatars of Freud's name to subvert progressive forms of collective identification across the generations. The problem, as a psychoanalyst might frame it, is that the fabric of the Unsaid is so great here that it pressures and forces distortion in the Said—making the Said everywhere symptomatic rather than etiological. What does it say that in a discussion of movements and cultural upheaval in the 60s and 70s there would be no mention of the role of Eastern philosophy, of drugs, Marx, the Civil Rights Movement, or of any of the indigenous American utopian movements and ideas of both the self and the social collective that long predated Freud?


One quote seems to flash subliminally beneath many scenes of the “There's A Policeman Inside All Our Heads”: “The one thing which we seek with insatiable desire is to forget ourselves, to be surprised out of our propriety, to lose our semipiternal memory and to do something without knowing how or why; in short to draw a new circle. Nothing great was ever achieved without enthusiasm. The way of life is wonderful; it is by abandonment.” Jerry Rubin? Huey Newton? Wilhelm Reich? In fact, no, Ralph Waldo Emerson, writing at a moment when ideals of the self and the social collective were so porous as to be promiscuous—achieving a fraught symbiosis, which in fact forms a recurrent pre-Freudian strain in American thought.


Even the film's completely understandable valorization of FDR is riddled with contradictions. If this was the Century of the Self, how did FDR and the New Deal ever come about? FDR's leadership didn't just birth a collective social self. It was involved with trying to harness the power of a massive American left-leaning political tide, more radical and socialist than the New Deal itself. Part of Roosevelt's task involved, indeed, attempting to co-opt left  wing ideals from the populist parties and tone them down for Washington in a manner suggestive of the struggle faced by the remnant of Republican moderates today to effectively harness Tea Party-type rage. Neither the powerful grassroots left wing of the American political process that helped drive many of FDR's policy decisions, nor the fundamentalist Christian-inspired new right have much, if anything, to do with Freud or Edward Bernays. This problem of the absence of context for FDR's appearance in the film points toward a much larger omission: The film provides no historical perspective through which we might begin thinking about the nature of the self humanity possessed before Freud. The implication in this absence is that, had there been no intervention of Freudian thought, class-based identities would have evolved by effective leadership and a natural process of human development into positive, self-sacrificing universal socialism. Really? How can one even glance at Twentieth Century history without acknowledging that humanity's capacity to form self-abnegating social collectives has at least as much potential to produce ghastly destruction as it does to nurture good?


Of course it is true that no film, however long, can include anything like everything. But, when a film purports to explain so much and yet leaves out everything, save one sinister Vienna-whelped strand, we must wonder what's actually going on.


I would ask that, in watching this film, you do as much as you can to look at the manner in which its polemic is presented: watch the nervous, quick cuts; listen to the disturbing music; track the tics and repetitions; follow the syntactical turns; study its slips and evasions. Pretend, if you will, that *The Century of the Self* is, itself, a patient that you've been called upon to psychoanalyze. Then pull out your personal version of the DSM.


In the end, what may be most interesting about *The Century of the Self* is the particular strain of nostalgia in which it indulges. Using a fast-slash, often frenetic visual style splashed with disparate soundbite interviews, Curtis postulates an essentially monolithic explanation for an enormously polyphonic phenomenon. For this reason, the documentary is more revealing as a harbinger of the Twenty-first Century decidedly post-Freudian cultural pathologies, than it is of the Freudian legacy. What *The Century of The Self* exposes is a whole other register of irrational desires from the ones for which Freud attempted to provide a taxonomy.


The film demonstrates that, along with our deep, dark cravings for sexual delight and territorial domination, we labor under a profound lust for overarching coherent narratives. In our own Century of the Scrambled Self, issues of mass distraction, ADD, ADHD, and all the overstimulation-driven kith and kin of these syndromes threaten to overwhelm our capacity to construct a cohesive self. At the same time that they also degrade our ability to function as a responsible electorate, they give us the comfort of imagining we can still peek behind the curtain to find one smoking wizard—pinpoint A Villain—and follow the yellow brick through-thread to our self-annihilatingly dissipated Ozymandias estate. However frightening this film might seem, how much more frightening it would be if—instead of this sophisticated, yet BBC-viewer-level accessible explanation for why we're so screwed—we had to confront an exurban-style sprawl of historical trends and individuals that all converged, overlapped and rear-ended one another to leave us floundering and fearful.


What if the thing we had most to fear right now was precisely the consequences of surrendering to belief in any master-narrative—of foregoing the granular excavation of all those sedimentary and disrupted strata of the self and society alike? Where does this kind of hyper-compressed history—McHegel, iHistory—that provides a playlist of easy targets for our encompassing dread actually leave us? Think, at the end of the film, about how you feel. When everything wrong is tracked through one channel emanating from a single font, everything must be undone or there is no hope. What possibilities for social change remain if this full arc is taken on faith?


In fact, at the end, what we might find ourselves remembering is that it is against just this sort of repressed-material-riddled history that psychoanalysis, in its most expansive, least dogmatically shrill iterations, can be wielded. The best of the Freudian legacy involves a patient, broadly attentive examination of the past that attempts not to shear off the complex root system nurturing the patient's frame of reference. It is an approach, we might add, in which the individual self, the socially constructed self, and the social collective itself are highly interpenetrative. If we're ever to come to a more fruitful understanding of why our current political system is so rotten, I suspect it will happen when we give over the idea of this *Weltanschauung* to which Freud himself was certainly susceptible—when we examine the range of phenomenon in and of themselves and go light on the ways they link together; when we accept that the very idea of periodization intrinsic to identifying a Century of the Self already entails an act of concealment. Might not the film as easily have been called *The Century of the Masses*, *The Century of War*, *The Century of Self-Annihilation*? What sort of history transpires under the rubric of the Century of Anything?


How might the film and its portrayals of myriad fascinating characters have been different? Well, I think of a remark by one Freud who goes unmentioned in this film. Freud's grandson, the painter Lucian, once noted in his very quiet, very precise, exquisitely non-alarmist voice: “I paint people not because of what they are like, not exactly in spite of what they are like, but how they happen to be.”


**George Prochnik**



 ## Comments 
1. carlhigdon
12.20.10 / 9pm


This was a perfect piece, thank you. Very cogent points and it greatly enhances the documentary itself.
3. [Mark Webster](http://motiondesign.wordpress.com/)
12.23.10 / 8pm


Fantastic critique on a documentary film that I have just discovered.
5. [Documentary: “The Century of Self Happiness Machines” | Yours Ambiguously, E.E. Talisman](http://emadtalisman.wordpress.com/2011/01/26/documentary-the-century-of-self-happiness-machines/)
1.26.11 / 4pm


[…] check out this commentary about the documentary. Thanks […]
7. [The Century of the Self 01/04/11 | The Final Year of This Degree](http://blogfor2011.wordpress.com/2011/04/08/the-century-of-the-self-010411/)
6.13.11 / 1pm


[…] topic of the creation of the consumerist, individualist society. However, I believe that the review by author George Prochnik as posted on Lebbeus Wood's blog correctly identifies the issues. […]
9. [The Century of the Self | A BBC Documentary » aperture | blog](http://csxlab.org/aperture/2011/10/the-century-of-the-self-a-bbc-documentary/)
10.31.11 / 10pm


[…] an Architect point of view, i would like to redirect to Lebbeus Woods blog, where he comments and presents the words of George Prochnik about this documenta… Bookmark on Delicious Digg this post Recommend on Facebook Share on Linkedin Share with Stumblers […]
