
---
title: SLIPSTREAMING
date: 2010-12-18 00:00:00
---

# SLIPSTREAMING


[![](media/lwblog-slipstrm-davinci1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwblog-slipstrm-davinci1.jpg)The characteristic of fluids that most interested Leonardo Da Vinci was turbulence. He lived in a politically turbulent age. He had a turbulent, restless mind. The world he lived in felt most strongly the cultural turbulence of new discoveries and new ideas, and he was driven by the desire to be part of what we look back on as ‘the rebirth' of an intellectual freedom that Europe had not seen since the collapse of the Roman Empire more than a millennium before. The effect of this freedom on the intellectually repressive, dogmatic stasis of the Middle Ages was like the turbulence of a waterfall on a placid pond, or of a thunderstorm breaking suddenly in calm skies. [Da Vinci's drawings](https://lebbeuswoods.wordpress.com/2010/12/03/da-vincis-blobs/) capture the drama of such violent events but were not mere illustrations of them. Rather, their rigorous analyses of how the complex interplay of forces actually worked were events in themselves, the first stirrings of scientific and technological imagination that would, over the next few centuries, transform the world.


And then:


[![](media/flow41-2v.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/flow41-2v.jpg)


The turbulence that most interests us today is of a different order than that which fascinated Da Vinci and characterized his time. It is the turbulence called a slipstream, created by a body—either fluid or solid—moving rapidly within a larger fluid body, which may itself be moving or at rest. The historically recent advent of propulsion systems capable of moving aircraft, boats, cars, and pulses of gas, fluids, or solids at high speed has resulted in the creation of a new type of turbulent space in their wake.


[![](media/flow21v.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/flow21v.jpg)


The slipstream is a highly dynamic space active with forces that impel a direction and that itself moves, together with the moving body continuously creating it. Its boundaries are variable, depending on the velocity and shape of that moving body, and also the characteristics—density, viscosity, and the like—of the fluid through which it moves. All of this can be described precisely with mathematics. However, as with all descriptions, they cannot bring us to full analytical understanding. We need other perspectives, and for that must turn to the analog.


[![](media/flow13v1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/flow13v1.jpg)


The analog emphasizes some aspects of a slipstream space, while it ignores others. In effect, it creates a fictional construct based on facts, or at least selected ones. However, it is important to realize that every description—even the most rigorous scientific one, backed up by mathematics—is fictional. Every theory that has been widely accepted enough to become a ‘law,' is an invention by human beings who emphasize what they consider the most salient characteristics of phenomena. In a sense, all of science is an analog of nature, of the reality of the world. Its relative success or failure depends not on absolute truth—who possesses such?—but on human consensus. How many colleagues are willing to endorse your fiction? That will of course depend on how well it fits into their fictions or theoretical constructs and therefore how useful it is in a particular scientific context. Rarely, someone invents a new theory that forces everyone to change their theories, such as Copernicus' theory of the solar system, or Newton's theory of gravity.


[![](media/flow41v.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/flow41v.jpg)


Slipstream space can be inhabited by people and sometimes is. Auto racers get extra speed while spending less fuel by following fast cars in front of them, as do drivers of eighteen-wheelers, whose boxy trailers create exceptionally violent turbulence and powerful slipstreams, which is why you will often see, on the highway, two or more of these trucks following each other in a tightly spaced line. Separated from the slipstream space by their vehicles' enclosure surfaces these drivers do not experience the space directly, but only through the instruments of their machines. Modern technological living provides many such indirectly experienced spaces, perhaps most prominently cyberspace. Its virtuality is, on an experiential level, analogical, and in exactly the terms of selectiveness described here. Virtual and analog experiences emphasize some characteristics of a phenomenon while ignoring others. That is the price to be paid for vastly expanding the boundaries of our experience, and for exploring the imaginary in the real.


LW



 ## Comments 
1. quadruspenseroso
12.19.10 / 4am


Right on, I find the Slipstream drawings a non-territorial ex nihilo space, a balanced flux of mind and matter, yes on the scary edge of monism but definitely a maturing Manhattan Hylozoism. 


They put me in a comfortable Zoroastrian-Graeco mood, bringing to mind … wait a minute, who's that knocking on the door? What? Oh no, it's that pesky, sans invitation, Freud, the party crashing prick gets past the doorman every time – soon enough he's rambling on about Da Vinci sublimating his Libido in order to investigate everything in site. Bastardo. Toasted on organic Prosecco dal Veneto, filled with encomiums, he says zie guten Zoro forgot about “good sex”. Before we let him psych the matter further, a distinguished guest of renowned anonymity, politely whispered in Mr. Freud's left ear, Wilhelm Reich would be arriving soon, man did Sig clear the d( )ck fast – good riddance.
3. [Tweets that mention SLIPSTREAMING « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/12/18/slipstreaming-2/?utm_source=pingback&utm_campaign=L2)
12.21.10 / 1am


[…] This post was mentioned on Twitter by Davis Doersam, KpaxsArchi. KpaxsArchi said: SLIPSTREAMING: The characteristic of fluids that most interested Leonardo Da Vinci was turbulence. He lived in a… <http://bit.ly/dTkzdp> […]
5. [General.la](http://Www.general.la)
12.27.10 / 7am


I find the images, regressive. because even if the condition of hyper-fluid dynamics is relevant to some pseudo psyche plane of perceptual consciousness, their investigation is but a moment of their pure capacity, and thus presuppose a simplification unequivocal to their nature.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.27.10 / 3pm
	
	
	General.la: I appreciate your comment. Would you clarify what exactly is regressive? It seems that what you are saying is that—if the drawings capture only fleeting moments in a continuous process—no single moment can reveal the character of ‘fluidity.' Is that what you mean? If so, ‘regressive' would seem to be the wrong term to describe the drawings' inadequacy.
	
	
	
	
	
		1. [generalla](http://generalla.wordpress.com)
		9.18.11 / 6am
		
		
		Somehow I missed this reply earlier, and am revisiting it now. Perceiving last years point of view, I also believe the word ‘regressive' to also not entirely communicate what I intended to say. In fact, I find my earlier comment somewhat abrasive.. what I meant to say, is that I believe to understand these drawings form a point of view of practice, experience. I also have seen quite a bit of your work, as well as DaVinci's. I saw these sketches as.. sketches.. surely capturing their moment, yet still about the practice of both mark-making, as well as the proportion and relationship of line to itself, space to itself, and line to space. A fabric of moments, woven with consideration for the space in-between. the idea of fluidity, however, seemed to escape all but their implied motion en force. this didn't do justice to fluidity in my eyes a year ago… purely because of a close relationship I have also held with these principles. This would best be illustrated, rather than explained. Although i've noticed your discouragement of links, The ‘catalogue' menu tab on my wordpress (linked from the gravitar, i believe) is the best explanation i could provide.
7. [Stephen](http://skorbichartist.blogspot.com/)
12.28.10 / 12am


I keep thinking about this slip stream space idea and your drawings. The drawings have a very streamlined feel, which seems to suggest a laminar flow produced by a wing or bullet shape. While I am not reading a “pocket” of calm or the turbulence caused by a slipstream, I think I get what you are saying.  

I ride motorcycles and am very familiar with how air moves a body around. I am always searching for that sweet slipstream coming off the bikes fairing as it breaks through the air. Motorbikes are not very aerodynamic (even the full fairing racers) and must leave a hell of a wake at plus 100 mph speed.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.29.10 / 2pm
	
	
	Stephen: Thanks for your personal ‘testimony.' These drawings are not intended to be definitive but evocative—a graphic analogy that you have understood in the same spirit. Their purpose is to open the imagination to new spatial possibilities.
9. [Emmanuele](http://www.piliaemmanuele.wordpress.com)
12.28.10 / 10pm


the problem to represent flows and vortex not only addressed some of Leonardo: mannerism has pursued for a long time the idea of transcending the “form “. Sometimes it is well managed. Titian shows it well!  

[![](media/tiziano_rape-europa.jpg)](http://www.artinvest2000.com/tiziano_rape-europa.jpg)  

Probably the idea of the Sublime was influenced by this type drawing…
11. [SOTIRIOS KOTOULAS: Emission Architecture « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/27/sotirios-kotoulas-emission-architecture/)
1.27.11 / 3pm


[…] on two previous posts (1, 2) concerned with the structure of fluid dynamic bodies, the following research by Sotirios Kotoulas […]
13. [Transportation notes « Balancing Age; Time as Relative](http://ruthsumner.wordpress.com/2011/03/23/transportation-notes/)
3.23.11 / 1am


[…] slipstream, created by a body, moving within a larger body which in itself may be moving or at rest. (ie. moving through wgtn in high winds) […]
15. David A. Palmieri
5.8.11 / 4pm


Lebbeus,  

I am a follower of your blog, and an admirer of your work. This particular thread though, has intrigued me the most, and even inspired to me think about the implications of slipstreaming as a spatial construct. This semester at Penn State University, I, along with several other students, had the opportunity to explore the phenomenon, through an installation. Early on, we made it our intention to express this idea experientially, to create a spatial procession that ultimately evoked feelings of turbulence. 


The design itself was conceived as a parametric structure, one was that was determined by contextual forces and relationships. Through several iterations of dynamic computation, we were able to define numerous vector fields, with varying lengths and directionality. The resulting space is dramatic and made up of two forms of materiality, bamboo, and lexan. The bamboo takes on the role of the vectors while the lexan becomes a floating subsurface, penetrated by the vectors of the slipstream. The installation spans over a high traffic pedestrian path of the campus and is about forty feet in length.


Fabrication has finally come to completion. I found it necessary to inform you of the impact your blog has on the work and discourse present in academia. If you are interested in seeing the design, I will forward you some drawings and images. If you are even inclined to experience the space yourself, then be sure to come before it is deconstructed on the 20th of May. Thank you for the inspiration.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.9.11 / 5pm
	
	
	David A. Palmieri: Very exciting news, indeed. I would very much like to see your project photos. I will email you shortly.
	3. [Joey Sarafian](http://josephsarafian.com)
	8.17.12 / 6pm
	
	
	David, is this the project you are referring to? I see that you're sited in the thanks section. I'm interested to hear your take on this interpretation of your drawings, Lebbeus.  
	
	<http://www.suckerpunchdaily.com/2012/08/17/slipstream-2/#more-24331>
	
	
	Thanks and keep up the great work.
