
---
title: QUESTIONS
date: 2010-12-10 00:00:00
---

# QUESTIONS


Stepping back from my own perspectives and opinions for once, I would like to ask you, dear readers of this blog, a crucial question: what do you think are the most urgent questions to put before architects today? This is not a survey. Rather, I am asking because I want to know what you think are the important issues confronting the wide—or narrow, depending on your perception—field of architecture today.


*The most compelling and difficult questions will be put before a panel of fearless discussants in an on-line symposium I will convene on a [special blog](http://lebsallnightcafe.wordpress.com/2011/01/26/were-open-2/) after the upcoming holidays. Go there to find out how it will work.*


I realize the broadness of the question will make it unappealing to some, too difficult for others. Understand that its breadth is simply my way of leaving the door open to any question related to architecture. Hopefully, many of you will be able to respond in the next two or three weeks, by posting Comments to this post. Also, please keep in mind that what I hope you will offer is *a question, not its answer*. Indeed, as I have said before, the only question worth asking is one for which you do not already have an answer.


LW



 ## Comments 
1. Jeff Brown
12.10.10 / 2am


…believing.





	1. Pedro Esteban
	12.31.10 / 5pm
	
	
	the best for me, but in what?
3. [Pablo Lara H](http://pablolarah.cl/)
12.10.10 / 3am


I am not an architect. I barely speak english. But I think the architects must ask themselves if what they are building is something of human beings size. I mean, and this a question for designers too, if they are designing or building for human beings. Or giants or dwarfs.
5. [nick](http://nickaxel.net)
12.10.10 / 3am


What is architecture's contemporary role as a subversive discourse and practice? Is a truly subversive architecture realizable?
7. [Chris Landau](http://theolinstudio.com)
12.10.10 / 4am


Pablo is right. The human scale is critical. I think this is why we will see a paradigm shift towards landscape architecture. Humans evolved out of doors, Nature is supreme, and systems are cool. I think what you said the other day about representing the future of architecture was key. It is no longer about imagining what structures are possible but what structural systems can be instrumental to change. And you also mentioned architecture as landscape in another post. I believe your hunch is correct.
9. [Nick Porcino](http://meshula.net)
12.10.10 / 5am


As fast as we create digital art, it can decay and evaporate. In a digital age should architecture also be ephemeral, perhaps built up fast and destroyed as quickly? Should architects design transient structures that last only in our memories, but not upon the ground? Is there a special role for the permanent and monumental, like the absurd monoliths of Speer or Ferris?





	1. Liz Snow
	1.4.11 / 3pm
	
	
	How can structures create memories if they are, by nature, transient? Are memories not made out of repetition over time, the places and spaces of memory burnished into one's, and perhaps a community's collective, subconscious?
	
	
	Monumentality breeds the perception of architecture as Image, 2D and static. Can monumentality exist in 4D (time-based) space?
11. Pedro Esteban Galindo Landeira
12.10.10 / 8am


that who it's wrong here, isn't the scale, are the humans!!! the scale??? what is the problem with the scale?
13. Sebastian Vidal
12.10.10 / 9am


Perhaps the problem is not the scale of what is going to build because for me there is a problem in the amount of construction to be built, beyond the scale.  

I would ask the architects if it would be better to rehabilitate existing buildings so as to adapt to a changing world moves ever faster and less time people lived in one house or you need to destroy what has already been built and make new buildings?  

(sorry de english, not is my lenguage)
15. Antonio
12.10.10 / 10am


-where do we come from?  

-what it is next?  

-when are we going to accept that modernism is long gone?  

-less is less  

-where does the audience play a role in todays architecture, where most of it seems to be finished products?
17. [Deema](http://www.deemaland.blogspot.com)
12.10.10 / 10am


would it be a space infinitely changing in scale, or a space of our immediate choice of scale?
19. [Deema](http://www.deemaland.blogspot.com)
12.10.10 / 10am


I asked about scale, then found the previous commentators asking about it too, that gave me relief !


anyway, I have another question, how can we touch or experience the inside of a building, I mean its thoughts rather than its meaning, in reference to angles of sight and the intensities of recognition?
21. [Maurits](http://www.nightlybuilt.org)
12.10.10 / 11am


Architects are losing control over an increasingly complex urban environment, that is being defined by rapid urbanization and the emergence informal developments. 


Architects are aware of the fact that they are losing ground. Koolhaas says that ‘the speed of international demands is out of pace with the ability of traditional designers to respond'. Eisenman says that ‘seductive renderings of impossible buildings are the narcissistic death rattle of a discipline lost in the tidal wave of image-dependent media'. The devaluation architecture is most notably being reflected in architect's fees, which are in a race to the bottom.


The deficiency of design leads to cities that are increasingly inhabitable. The horizon of design tends to narrow down and exclude everything that cannot be perceived. This produces an urban patchwork of enclaves that are strictly segregated by class, which, according to The Pentagon, leads to a permanent state of war, albeit low-intensity. See last month's military operations in the favelas of Rio de Janeiro as an extreme example. Apartheid and the Civil Rights Movement taught us that segregation leads to arrested development, which obstructs the Pursuit of Happiness, a universal human right.


Modernism has proven that architecture cannot change society. Is architecture stalemated, doomed to decorate only the enclaves of the rich?
23. Mustafa Teksoy
12.10.10 / 11am


Is an architectural idea genareted by the architect who deals with it; or, is it the architect who become to be a body to realize that already existing idea?
25. [chris teeter](http://www.metamechanics.com)
12.10.10 / 1pm


How will the more or less useless and irrelevant profession redeem both global market value and maintain an experimental edge for the advancement of human society? (See Woods last post 5 percent according to UN(interviewer))


How do academic leaders intend on preparing students to be profitable employees or employers by construction industry market standards? If such a student can be produced, how will formal conceptual design remain a basis for valuable construction industry market decisions? What formal conceptual design is the right system if any that is both pragmatically profitable and beneficial to the well being of humanity?


How will experimental conceptual formal design be educated to students who intend to making gainful employment expected at the level of a professional be delivered to the minds of young students paying massive tuition bills?


How will the AIA ensure the architects expected market value as a service component in the construction industry while ensuring the profession pushes the envelope?
27. Manuel R
12.10.10 / 1pm


Dear Mr Woods,


Well, i think i have an answer, but if what interests you is not a proper answer, then i'll try to expose the doubts behind the answer.


When and how it will happen that architects become crucial in ordinary people's lives?


How sustainable is it to both our cities and citizens to promote the common use of architecture – as a discipline and as accessible professionals – in order to respond to the various rates of growth, hope and development still to come?


How can architects support and confront the huge amount of work in front of us?


More than hope or search for new ways, why not the obvious at an extraordinary fast, almost collapsing, rate?


Uncertainly,


MR
29. Jared Karr
12.10.10 / 2pm


Why is architecture in western culture still almost solely the domain of the rich, the white, and the male?
31. [scutanddestroy](http://scutanddestroy.wordpress.com)
12.10.10 / 3pm


As time passes there is increasing evidence that suburban sprawl has negative consequences on both our environment and our economic stability. Despite warning signs, the suburbs continue to expand rapidly in North America.


How can architects convince middle-class families to abandon their suburban dwellings?
33. chen
12.10.10 / 4pm


1. Architects only design about five percent of the world's buildings. How can architects play a role in the other ninety five percent?





	1. Liz Snow
	1.4.11 / 3pm
	
	
	Or how can they learn from the other 95%?
35. D Pearson
12.10.10 / 5pm


Within the US: how can architects approach the growing polarity in American ideology? Specifically for designers how this affects society's value of democratic public space?


World: how are the effects of digital connectivity on our perceptual environment redefining physical space? From the perspective of the democratization of information (ex. wikileaks) to social media to an approaching singularity.
37. [Bruce F](http://greenroofgrowers.blogspot.com/)
12.10.10 / 9pm


I would like to hear architects responses to the questions posed by social scientists, such as David Harvey.


He thinks that there is a group of questions that all hang together and that should be addressed in total by any architect or planner asking what our cities should look like:


1. What is the proposed designs relation to the natural world — carbon footprint, etc.  

2. What technologies are embedded in the design — communications, transport, waste disposal  

3. What is the impact of the design on social relations — gender relations, social hierarchy  

4. What type of production models will come from the design — any alternative economies?  

5. How would daily life be affected — children, street life  

6. How would the design change people's mental conception of their world  

7. How are institutional arrangements changed — governing structure, etc.


As these seven areas are constantly in motion, how does the design allow for perpetual change?


For a more detailed explanation of Mr. Harvey's thoughts, check out this video of a recent talk he gave to the Croatian Architect's Association in Zagreb.
39. Ryan J. Simons
12.10.10 / 9pm


When will schools of Architecture stop relying on status quo, chronic relationships with agencies– NAAB, NCARB, AIA, sponsored studios, etc.- that retard intellectual investigation in order to promote an esoteric agenda that has little to no effect on the advancement of invention & forward thinking?
41. [Joshua Perez](http://www.untitleddesigner.tumblr.com)
12.11.10 / 12am


In a world where only 2% of the built environment is actually designed by architects, what can we do to ensure that we as architects and designers begin to acquire a larger percentage of the pie and act with resourceful responsibility to social and natural systems? 


What do architects of the now and of the future need to do in order to increase the perceived seriousness of the role we have vowed to play?





	1. [Diego](http://opacos.wordpress.com/)
	12.15.10 / 4pm
	
	
	Does the remaining 98% is built by other professionals or bad architects?
43. Kamil
12.11.10 / 11pm


WHAT IS THE FUTURE OF ARCHITECTURE?


I believe this question will tell you everything there is to know about the archihtect it is asked to, their own views and priorities within the field regardless of what is needed in the world by them and their buildings.
45. Laura
12.12.10 / 10am


My question is an Oscar Wilde's quote: “If Nature had been comfortable, mankind would never have invented Architecture”
47. Chen
12.12.10 / 3pm


2. How can architects play a role in the virtual communities? 


3. What will the architect profession be like fifty years from now? Will the profession be doomed? 


4. The growth of wealth and human development seems to be correlated with the decrease in population. How can architects adapt to population decline?
49. [Zach](http://www.architecture360.net/)
12.12.10 / 7pm


Technological and theoretical advances will continue to enrich the practice of architecture but I believe the client service / economic aspect of the field has a larger impact on what is built, and is a more complex and critical issue than is often understood. Questions include:  

1. Who within architecture offices really gets to make the major design decisions (building shape, color, etc) for built buildings? Why are they, rather than others (who carry out the ‘minor' aspects), doing so?  

2. How does the feast-or-famine nature of the practice affect architect's lives? How does it affect the built environment?  

3. Why are buildings so often built larger than they should be? Even in brimming cites like New York or Shanghai, many buildings are under-utilized. In depressed cities, it is much worse.
51. 660
12.12.10 / 8pm


– architectural education should empower individual choice over predecessor indoctrination?  

– no one says starchitect in this post-economic gloom – why?  

– hand-drawings are a new currency?
53. Bob
12.12.10 / 10pm


Perhaps a bit vague but I believe it lies somewhere near the root of the sustainability problem: How does the Architect Resolve the duality of Creativity and Control- in relation to social and environmental systems- or in other words how can we get at both Variety and Efficiency at once. Or how can we transcend this problem?
55. Mario
12.13.10 / 1am


The most urgent question for me is that architects should ask themselves the reason why they are architects and find out if the answer have a sense in their lifes.
57. Jordan Johnson
12.13.10 / 3am


Doesn't the field of architecture require a speed limit?
59. Cesar Cheng
12.13.10 / 4am


Rather than getting into details and addressing questions like scale, sustainability, obsolescence etc, I think that the most urgent question and simple at the same time is: 


+What is the role of architecture in our current times,  

+ How it differs from the past and  

+What fundamental changes are emerging in the current practice of architecture.


 To give a hint of what I think, I suggest to look at the leading architects of the moment as well as to the emerging architects and practitioners to understand what is going on. I am particularly interested in the work of AMO not OMA, The why factory not MVRDV, MAD talks not MAD buildings. I think architecuture in this century extends beyond the real of buildings. Architecture is about ideas no so much about buildings, the least architectural thing of architecture is a building.
61. [Simon](http://cygielski.com/blog)
12.13.10 / 4am


Architecture seems divided into the effete academic “discipline” bent on creating “invention & forward thinking” as someone above said, and the day-to-day drudgery of putting up unimaginative structures. The first is irrelevant to all but a few intellectuals, the second is becoming irrelevant through its own inertia.  

The rest of the world is starting to notice that architects aren't really needed (i.e. engineers take the responsibility for structural safety, which was the main reason why the profession became as exclusive as it is, and everyone from interior designers to electronic media artists contribute to the cultural/artistic aspect of architecture. With very few exceptions architects have become little more than space planners or graphic artists).  

The main question facing architects seems whether in this state of things architecture is really needed any more, or is it just one more profession destined to go the way of the fletcher and the flypaper salesman.
63. pdx
12.13.10 / 5am


Why are architects so concerned with looking for any meaning beyond the reality of construction?
65. [Stephen](http://skorbichartist.blogspot.com/)
12.13.10 / 4pm


How will architects survive in an atmosphere of irrelevance?  

Are architects digging their own grave by not being able to respond on a true scientific or technological level when other players in the construction and engineering field are so far ahead?  

Will architects be able to demand better fees so that more than just the sons and daughters of the rich be the ones doing design of any significance?  

How will architects survive as professionals?  

If we can't survive, all these other questions are moot.
67. [WE'RE OPEN (updated) « Leb's All-Night Cafe](http://lebsallnightcafe.wordpress.com/2010/12/08/were-open/)
12.14.10 / 1pm


[…] am receiving questions on the LW blog, selecting and correlating them for the Menu, which will be available on this blog by the first […]
69. Gio
12.15.10 / 1am


1)  

Form making and the relationship to Urban space, Public space, Residential space and Personal space in the age of recycling, environmental disasters and social unbalance.  

2)  

Is proportion a dying art?  

3)  

Is the art of making something by hand a lost art as well?
71. [Diego](http://opacos.wordpress.com/)
12.15.10 / 4pm


Leaving aside all the architecture that emerged from a client that respects a good design (might be 1% of the total built), that daily show in five specialized publications simultaneously, I would like to focus on remaining 99% of unnamed battlefield where, to our dismay, we build (destroy) the landscape. My questions are based on my experience in the practice of architecture, that is, when ideas are judged by private and public clients, realtors, austere budgets and especially the absurd zoning laws designed by lawyers rather than architects, sociologists, anthropologists, artists, poets, and other social agents.


CULTURAL LANDSCAPE


\_ How to build cultural identity? 


 personal memory?  

 material memory?  

 economic memory?  

 social memory?  

 arquitecture memory?


\_ Is it possible to maintain cultural identity while globalization is altering its own principles?


\_ Is it still possible to think globally and act locally?


\_ Is the heterogeneity the true identity of our time?


\_ Do we like the generic architecture, although it masquerades as the healthiest sustainability?


SOCIAL LANDSCAPE


\_ Is there good architecture without a good client?


\_ Can we survive rejecting bad clients?


\_ Maybe the architects underestimated the intelligence of the customer?


\_ Architects are we willing to sacrifice the aesthetic coherence of our creations to satisfy a customer?


\_ Is there any school for clients?


\_ Can we wait for them?


\_ Can anyone turn off the TV?


How difficult is to find an answer that does not involve its own answer Mr Woods, and above all, a question that goes beyond our previous knowledge!, as Bachelard said, “to create, you must forget what you learned”
73. [Simon](http://cygielski.com/blog)
12.17.10 / 4am


As a follow-up to my previous post – perhaps a more constructive question would be, “What should architects do to evade the field's increasing irrelevance?” There are generally two major ways: go back to the way things used to be (i.e. take charge of the design and building process, and resume responsibility for the functional aspects of the resulting structure) or move forward in some fashion (the question being what area of responsibility could architects assume that a) hasn't been taken over by another profession, b) answers a real existing need that has not been addressed yet, and c) is reasonably related to the field of architecture (just to keep things on track)).
75. Pedro Esteban
12.22.10 / 7pm


I'm tired of questions I have so many questions but any answer, so try to put answers!!! I think isn't the time for questions is the time for acting……….. but we are beginning, at least……..Like some questions… specially that who speaks about speed!!!  

Yes, we need speed but wihtout limits!!!!! Another question how I can use speed?
77. Pedro Esteban
12.22.10 / 8pm


we can change the brain, or the process in the eyes, to have a new way of representation??? as architects we can do that? yes, of course!
79. spencer
12.28.10 / 5am


Are we asking the right questions?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.29.10 / 2pm
	
	
	spencer: Glad you're interested. There are a few good questions asked by readers, and many that are not very clearly stated, although the topics they raise are well worth thinking about. It seems that asking a clear question is at least as difficult as giving an answer. In fact, I am ever more convinced that architects' inability to ask pointed questions is at the root of a certain intellectual stagnation afflicting our field today.  
	
	Anyway, I am culling readers' responses and selecting the best ones to put on the menu of Leb's All-Night Cafe—people are lining up outside the door!
	
	
	
	
	
		1. spencer
		1.18.11 / 2am
		
		
		All,
		
		
		Sorry, on second reading my question sounds a little flippant. I meant no insult to poorly worded questions prior to (or after) mine. 
		
		
		I am actually serious when asking my question. Are we asking the right questions?
		
		
		I often wonder if, as architects, are we solving the correct problems? Do we ever stop and ask is this solution better suited as something else? Or, is this part of a building even right? I guess I wonder with all the architects I know if we see a bigger picture or are we so focused on making buildings that we are missing what is really needed?
		
		
		Specifically, I remember seeing a project a few years ago that was a great solution to sheltering the homeless. It was given many accolades but in my opinion it missed the mark because it only solve the simple problem of sheltering someone who didn't have a shelter. Homeless people need more than walls and a roof and although project was a great solution I had to ask, was this creativity put to its most productive use?
		
		
		Anyway, there are many good questions posted here and I didn't want anyone to think I am being a jerk by disregarding the notion of a good question. And, as my Dad's mentor Bob Randski once said, “There are no bad questions, except for the ones that are never asked.”
	3. Pedro Esteban
	12.31.10 / 2am
	
	
	I'm if you ask yourself first what architecture should be, and what it is.  
	
	But yes there some questions quite, well I don' know the word!!! hahaha
	5. PE
	1.18.11 / 8am
	
	
	I agree with you, in some points, can you post the link to the project for homeless? Thank you, and that's a big true
	
	
	“There are no bad questions, except for the ones that are never asked.”
81. [torossian stephane](http://www.image-in-sato.com)
12.28.10 / 8am


More and more people will be oblige to move from their not owned places… what an architect can do for people who are nomads ?





	1. Pedro Esteban
	12.29.10 / 2pm
	
	
	A lot of things!!!!!! Like so much the answers I have for you question!  
	
	Can create a new tipology, if we can call it in this way, to begin….
83. Gio
12.29.10 / 11pm


More questions:


4)  

Can a philosophical discourse provide an answer to the creation of Architecture and the relationship to human space in our times?


5)  

What is Space in Architecture? Do space changes with time?


6)  

If we call Built-Real space, “Architecture”….what is then Digital-Built space? Can we call it Architecture too?


7) Is Architecture ruled by fashion? and as one, touched by time? Can we train ourselves to create timeless works of art?


There have been very few buildings designed by architects, where time or the passing of years can not impose the seal of “old” or belonging to a period, where you can take any of these buildings and put it anywhere in time's history and they will always look as if they belong to a timeless era, (some examples are): Ronhamp, Firminy, The assembly in India by L.K., Any Mayan building, The Egyptian pyramids,  

the Brion cemetary by Scarpa…but there are more works done by engineers where is nearly impossible to do this:  

Any silo-grain elevators, any electrical refineries, chemical refineries, nuclear reactors…and so on.  

Untouched by time.
85. Gustavo B
12.31.10 / 1am


Why don't architects try to use design as a means for solving the worlds problems? Why isn't anyone trying to pursue solutions that will help our fellow beings?  

Are Architects scared of a challenge? How would one even start to solve a problem like the slums in Mumbai?
87. Pedro Esteban
12.31.10 / 5pm


We have a langauge? We need a language? Language?
89. Pedro Esteban
12.31.10 / 5pm


I like……  

Maybe the architects underestimated the intelligence of the customer?………….Please!!!  

Why are architects so concerned with looking for any meaning beyond the reality of construction?………  

How can architects support and confront the huge amount of work in front of us?…….working, I supoused, hahahahaha





	1. Gustavo B
	1.1.11 / 7pm
	
	
	Pedro, I applaud your question about meaning beyond the reality of construction. Hope it gets chosen.
	
	
	
	
	
		1. [Maurits](http://www.nightlybuilt.org)
		1.1.11 / 9pm
		
		
		If architects would not be thinking about meaning beyond the reality of construction, they would be called engineers.
		3. Pedro Esteban
		1.1.11 / 10pm
		
		
		The irony, a double edge sword………..I prefer modernism is a fact!!!  
		
		My god what I have done?  
		
		Gustavo sorry, that are the questions I don't like, to me these sound ridiculous.  
		
		Here I put an essay who I done for the dean of my school, the excersice was to make an essay in english language speaking how the tecnology is art in architecture, for me that was ridicoulus, a dean can't asks somethings like that, and less to students of second year, well there are another reasons but at the end this is what I wrote. I made a simple parragraf in a few mins, maybe you can understand whit it, why is so important go beyond construction, the dean told me: you couldn't write that!!! She think too, architecture is only construction, she is a great lady, quite know for his intelligence!!!
		
		
		LW can I put for this time in english and after the essay in spanish, thank you.
		
		
		You can refer to a book called, in spanish La Materia de la Invencion, de ENRICO MANZINI, I can't read it, only can see his pages one day in a library in Cuba and that book change my life forever!
		
		
		The dead discussion about the inclusion of the architecture in the arts, isn't a mark in this essay, we take for a fact architecture is art. To make contemporary art means communicating, is to say something, to speak by any means, whatever objective, in any language, just to say. To make contemporary architecture is saying new things, it´s to explain through which means we can face in a different way what it´s said by the precedent laws, and how to translate this new form of communication to the consumers of the art. It's to speak through materials.  
		
		The material dictates the direct expression of architecture, is in architecture what language is in literature, is the way in which the building communicates with its surrounding landscape. All the materials used in architecture communicate to the context the ideas of the architect whatever the form of the buildings made by him. The form can be infinite but the materials will be his communication, will be the speaking part on the architecture. The final expression of any architectonic form will be communicated by the material. The tectonic part will be the direct translator of the creator thoughts.  
		
		How to communicate in architecture will be our goal when we dominate the technology. As in literature, communicating means knowing the exact words, its uses and misuses, the same way in architecture we most know technology, dominate it, learn it in any way possible. It's like the redaction tool in literature; by knowing how to ‘technologize' we'll learn how to communicate architecturally. 
		
		
		 La discusión muerta acerca de la inclusión de la arquitectura en las artes, no es punto en este ensayo, dejamos declarado que la arquitectura es arte. Hacer arte contemporáneo es comunicarse, es decir algo, es hablar mediante cualquier medio, con cualquier obetivo, en cualquier lengua, sólo es decir. Hacer arquitectura contemporánea es decir cosas nuevas, es ver mediante que medio podemos afrontar de una forma diferente lo que ya está planteado y como hacer llegar esa nueva forma de decir a los consumidores del arte. Es hablar mediante una forma, que esta cubierta por un material.  
		
		El material dicta la expresión directa de la arquitectura, es en la arquitectura el lenguaje en la literatura, es la forma en la cual el edifcio se comunica con su contexto. Los materiales dicen al contexto las ideas del arquitecto independientemente de la forma de los edificios, la forma puede ser infinita pero el material será la comunicación, será la parte parlante de la arquitectura. La expresión final de una forma arquitectónica será comunicada por el material. La tectónica será portavoz directa de los pensamientos de la razón de ser del edificio.  
		
		Cómo saber comunicarse en arquitectura, lo dominaremos dominando la tecnología. El conocimiento de las propiedades del material, su forma de aplicación, su interaccion con otros elementos nos lo dirá esta rama. Es la herramienta de redacción en la literatura, al saber tecnologizar sabremos comunicar arquitectónicamente. La tecnología es la clave de la buena comunicación.
		5. Gustavo B
		1.4.11 / 12am
		
		
		Pedro, the reason I applaud your question is not because I believe that architecture should have no meaning but rather the fact that you bring up a question that seems to be debated with a some of my colleagues. Some are concerned that Architecture is purely structure, and nothing else….no meaning, no idea, no feeling, just something there to shelter us, to in-house us. Architecture may have those characteristics, but I believe there is just more to that than just structure. Something much more deeper, but few around me seem to not understand or comprehend that. The reason I want this question to be discussed is because I want to hear what others think, I want to hear responses that embrace in what I believe. The question may sound ridiculous to you, which I understand completely but i feel its a question that is somehow not really answered.
		7. PE
		1.18.11 / 8am
		
		
		Are you an architect?
		9. Gustavo B
		1.19.11 / 12am
		
		
		PE, I can not say that I am an Architect because I do not have a license to be called that, rather I am a student studying architecture. Not sure why you intend in asking such a question, or if its even aimed at me.
		11. PE
		1.19.11 / 1am
		
		
		where do you study? because you're so concerned with technological meanings, I'm student too, and I allways was discussing(fighting indeed) with my professors for ideas like those of architecture as construction
		13. Gustavo B
		1.19.11 / 8am
		
		
		Pedro I study en la Escuela Politécnica de California en San Luis Obispo. Not all my professors see architecture as pure construction. There are a good amount of professors who really see past the ‘just construction' aspect of architecture. Some of the professors see architecture in such a practical sense that it looses all its meaning. But other professors have really made an impact on me with applying technical advancements in architecture and meaning. Those inspiring professors remind me everyday that there is so much to architecture then just construction, and even meaning beyond the reality of construction. Of course they also remind me that knowing how things go together (especially in construction) enhances your ability as an Architect. There is so much to take into consideration when one is designing. That is why I love architecture. Its just more than form, its more than space. Its a combination of so many things that it truly takes a committed passion for it to fully understand it and even at times, you still need to search even further to sometimes even make sense of it.
91. Diego
1.3.11 / 8am


It seems that a person with an uncommon intelligence has answers for everything …
93. LV
1.5.11 / 4pm


to what extent should architects pursue aesthetic agenda? that is to say in exchange for how much practicality, finance…etc?


I hear it often that the best architecture is a compromise between practicality and aesthetics -yes- but what is the ratio? 


Ive always been interested in this. I think this has to do with what Gustavo B is writing. How can one substantiate beauty, poetry in architecture?


LV
95. Pedro Esteban
1.5.11 / 11pm


A mark, not a question.  

ARCHITECTURE IN SCHOOLS SINCE THE FIRST GRADE!!!!!!!  

COMUNICATION WITH THE MASS!!! RIGHT NOW!!!
97. [Pearce](http://www.carbon-vudu.com)
1.6.11 / 11pm


All this crap we do not need is abundantly spreading. Are we really in a recession? If so why do we need our military to fly planes over the heads of people for inaugurations? Really we need this? 


Confrontations regarding architecture/society/people/us:  

Bureaucracy.  

People who do not know better.  

People who know better but do nothing.  

Poor leadership that sets precedents for knowledge.


The questions are endless, like urban sprawl to the desire to build the tallest building…





	1. Gustavo B
	1.7.11 / 5am
	
	
	Pearce, very true.
99. Diego
1.7.11 / 4pm


This is an educational initiative that seeks to explain the architecture as a discipline that protects and adds value to the landscape…follow this link


<http://proxectoterra.coag.es/>
101. Diego
1.8.11 / 4pm


As Gustavo said, without feeling, idea, that is without passion, our actions make no sense, are automatic. The problem is how to explain to society the benefits of a well designed space if you have never experienced. In many cases in our cities there are examples of architecture, museums, churches etc.. Experience tells us that these spaces are not massively internalized, perhaps because they are built with codes that come in conflict with the archetype space we learn in our homes, at school, on television, and in many cases the architecture is seen as a luxury item or a reason for the snobbery, as ultimately unreal. In the society of consumption goods should be easy to assimilate, we must use our energies to work and consume. No space for reflection.
103. Pedro Esteban
1.26.11 / 5pm


LW your question?
