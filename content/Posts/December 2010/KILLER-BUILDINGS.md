
---
title: KILLER BUILDINGS
date: 2010-12-31 00:00:00 
tags: 
    - buildings
    - disasters
---

# KILLER BUILDINGS


The reason we so often stand mute before images like these is because “they speak for themselves.” But do they? Certainly, we are hesitant to speak for fear of uttering clichés in the presence of human tragedy. We have an innate sense that we must not profane what is nothing less than a sacred site—a place where human beings have been killed. Yes, ‘killed' and not ‘died' in the natural course of their lives. What has killed them, and why?


[![](media/lwb-killer-91.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-91.jpg)


The first and most obvious answer is that the building killed them. Suddenly, unexpectedly, the building collapsed, crushing those who lived within under its immense weight. So, the building has a particular kind of volition. It can independently decide to collapse—no person needs to command it to do so. Who would? But, actually, it has been programmed by people to collapse, under certain conditions.


[![](media/lwb-killer-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-10.jpg)


.In the first place, architects and structural engineers have established the building's materials and their ‘elastic limits'—the points at which the materials will fail. In the second place, the building's constructors activate the material's limits, which will inevitably deviate from those anticipated by the designers. Buildings may be thought of as low-tech cyborgs, encoded with instructions about how to live and when to die. But that brings us to the third place. All buildings have their own intimate relationships with conditions around them and thus a small degree of independent life. When confronted by certain conditions, they can self-destruct without warning, taking their inhabitants with them.


[![](media/lwb-killer-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-11.jpg)


.


Shapeless clouds of smoke disturb a city's geometric order. The geometry is predictable and reassuring. The smoke obscures the geometry, challenges the order, and is unsettling, even alarming. Mathematicians and poets may tell us that the smoke is only the form of a different order, but we are not reassured. Instead, such descriptions only confirm our fears of disorder and the looming emotional chaos of loss.


[![](media/lwb-killer-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-5.jpg)


.As the smoke diminishes, we see that only a single building—a single example of the order we depend on—is burning. Standing almost alone in a vast geometric landscape, the fire that consumes it is within containable, acceptable limits, at least of the designing architects and engineers, but also of our abilities to tolerate threats to the stability of our immediate world. Then, we recoil in horror at that thought—there may be people still inside—maybe all could not escape. Knowing how tall the building is, the probable shutdown of elevators, the possible blockage of stairwells by smoke and debris, if not flames—


[![](media/lwb-killer-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-6.jpg)


From the black vultures circling the tower—the news helicopters that can take but not give—we can look into the building's high windows and see the burning interiors of apartments, their once useful furniture absurdly ablaze. People lived in these spaces, high above the city. Squinting our eyes, we easily imagine those who may have died and wildly fear seeing any who have not.  It is only then we ask why such houses, which—in a major catastrophe—offer  little chance of escape or rescue are built at all. It would be comforting in a strange sense if the answer would be that the risk is the price people gladly pay for such privileged perches in the sky, but we know this is not true.


[![](media/lwb-killer-43.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-43.jpg)


People live and work in towers because they're given no choice. Land in the city is expensive. Also, it only slowly becomes available for new buildings, so the only answer for those who build offices and houses is to stack them as high as they can. Or at least that is the only answer that architects and planners have so far been able to devise. Until other concepts are invented, towers will continue to rise. When night comes over this city, searchlights are turned onto the tower, transforming it into a dazzling spectacle whose uncanny beauty mocks its terrible reality.


[![](media/lwb-killer-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-8.jpg)


.


An offshore oil-drilling platform is a kind of utopian community. It houses workers who share a single mission: extracting a precious mineral from beneath the deep-sea floor. There is a religious character to their quest, in its single-mindedness, its precise interaction with wider nature, that is reflected in the form of the structure they inhabit. Symmetry and hierarchy, varied by sheer pragmatics—as in a Medieval church—dominate the platform and the order it imposes on rituals of work and daily living.


[![](media/lwb-killer-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-2.jpg)


At the heart of the platform, where the drill-shaft is driven from a high tower into the sea and deep below, workers attend to the process that will finally yield the oil. Playfully they pose in the course of their work, hardly oblivious to its dangers. Taking from the earth a mineral that was formed over millions of years is something of a Promethean act, not unlike stealing fire from the gods. Sometimes the earth, or the gods, will have their revenge.


[![](media/lwb-killer-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-3.jpg)


[![](media/lwb-killer-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/lwb-killer-1.jpg)


LW


Article on the Deepwater Horizon fire:


<http://www.nytimes.com/2010/12/26/us/26spill.html?hp>


First article on the Shanghai tower fire:


<http://www.nytimes.com/2010/11/16/world/asia/16shanghai.html?scp=1&sq=shanghai%20tower%20fire&st=cse>


Follow-up article on the Shanghai tower fire:


<http://www.nytimes.com/2010/11/17/world/asia/17shanghai.html?scp=3&sq=shanghai%20tower%20fire&st=cse>


Article on the New Delhi building collapse:


<http://www.nytimes.com/2010/11/17/world/asia/17india.html?scp=3&sq=building%20collapse%20india&st=cse>


#buildings #disasters
 ## Comments 
1. Elan
1.3.11 / 7am


Perhaps not only ‘killer' buildings, but ‘murdered' ones as well:  

<http://www.guardian.co.uk/artanddesign/2011/jan/02/detroit-ruins-marchand-meffre-photographs-ohagan>
3. tom
1.3.11 / 4pm


The floating platform reminds me the deserted island of Deleuze.  

Deleuze's deserted island is place in the middle of the desert, but at the same time is a non-place, it deserted.


The floating-oil-drilling-platform is really utopian, it provides its residents all their needs, but they in return injuring the heart of the earth  

 deriving a precious resource with a forceful action.


It's a little difficult to agree that the platform is a place of holiness, it's a place that challenges the very creation – it is not a praise for the action of creation, it makes the opposite.


Deleuze says that the water are only a cover for the soil beneath them, in the event of the fire on the platform, the soil beneath (the earth) said the last word, and reminded everyone of the man's place.
