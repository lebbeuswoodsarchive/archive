
---
title: ARCHITECTURE  the solid state of thought [complete]
date: 2010-12-10 00:00:00
---

# ARCHITECTURE: the solid state of thought [complete]









**PART 1**
.
*The following is reprinted from the [cluster website](http://www.cluster.eu/)*
*.*





Thursday, November 18, 2010 15:59
.
An interview with [Lebbeus Woods](http://en.wikipedia.org/wiki/Lebbeus_Woods) by [Corrado Curti](http://www.cluster.eu/corrado-curti)


The works and writings of **Lebbeus Woods** unveil a deep and hidden fracture that severs the intellectual and critical practice of architecture – often exercised by a handful of high-profile, and occasionally aloof, theorists – from its business-as-usual, labour-intensive counterpart – no matter how big the firm, or how glossy the cover of the magazine featuring its projects.


![edge-2](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/edge-2.jpg "edge-2")  

*Man on a Wire – Philippe Petit image via [Arts Meme](http://artsmeme.com/2010/07/06/philippe-petit-wired-even-when-not-on-wire/)*


The Experimental Architect¹ inhabits the space that lies between these two extremes. And in this obscure grey area he or she, rather like Johnny Cash's “A Boy Named Sue”, travels alone along unexplored paths and trajectories that bridge the two shores of failure: a “Stranger in a Strange Land”.


.


***CC**: [Feynman](http://en.wikipedia.org/wiki/Richard_Feynman) said that science can be seen as: “the result of the discovery that it is worthwhile rechecking by new direct experience, and not necessarily trusting the [human] race[‘s] experience from the past” in his discourse dedicated to What is Science?² In 1966. [Karl Popper](http://en.wikipedia.org/wiki/Karl_Popper) on the other hand, in The Logic of Scientific Discovery³ , wrote: “we shall have to get accustomed to the idea that we must not look upon science as a “body of knowledge”, but rather as a system of hypotheses, or as a system of guesses or anticipations that in principle cannot be justified, but with which we work as long as they stand up to tests, and of which we are never justified in saying that we know they are “true”.*


*Although architecture cannot be considered a Positive Science, these definitions could be appropriate for architecture too. To what extent, and how, can scientific thinking be applied to Architecture?*


**LW**: Science on the level **Feynman** and **Popper** refer to is highly speculative and creative, relying on imaginative people to give it some new and original form. There is plenty of lesser science that simply recasts and reshuffles things we already know to put a finer point on it. Architecture is much the same.


There is the everyday architectural practice that recasts and reshuffles the already known – office buildings, housing projects, shopping malls – that simply refine for tastes of the moment the familiar typologies, meeting the demand for something new, but not too new, that is, new to the point that we don't feel comfortable with it, demanding that we have to change our habits too much. The upper, really creative levels of science are very inspiring, but only to architects who aspire to make a breakthrough, to solve a problem that has never existed before or has not yet been solved. For the other, everyday architects, this level of science is of little use or interest.


It's important to understand here that there are, with respect to knowledge, crucial differences between science and architecture. Science presumes that there is some ‘truth' that must be discovered and defined, say, the structure of the atom or the origins of the universe. All the creative types in science can get to work on such well-defined problems, or aspects of them. In architecture, there is no such equivalent truth to be found, defined, or understood. Architecture is existential. Its hypotheses and theories, the problems that it confronts are of the constantly shifting conditions of being human. While they may hold some basic human traits – physical and mental – the goal for architects should not be to enshrine these in eternal laws and forms, but to enable people to live to their full potential, whatever that is or may be. Scientific thinking can only be of limited help in this task.


***CC**: The term Experimental is so widely used in contemporary architectural discourse that it often becomes vague and generic. Today, various different approaches such as: the integration of software and informatics in design processes to generate and control forms, the introduction of new manufacturing technologies to building techniques and processes, and the application of innovative materials to buildings may all be referred to as experimental. During the course of your career you have researched into the possibility of establishing the field of Experimental Architecture as a distinct branch within the discipline. How do you define it?*


**LW**: An experiment, simply put, is a test of an idea or a hypothesis, a ‘what if,' to see if it works in reality.  

An experiment is NOT the creation of the hypothesis – that belongs to the realm of theory. It is also NOT the application of its results to reality – that belongs to the realm of practice. The experiment is an in-between realm. It happens, to use the scientific term, in a laboratory – a personal space and under controlled conditions. In architecture it necessarily takes some spatial, visual form – drawings and models by hand and computer are most common. These can be evaluated post-facto by the architect and others regarding their confirmation of the hypothesis and also their potential usefulness in practice.


![image_1_lebbeus-woods](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/image_1_lebbeus-woods.jpg "image_1_lebbeus-woods")  

*Magdeburger-Halbkugeln, Kupferstich Gaspar Schotts zu von Guerickes Halbkugel-Experiment, via [Wikimedia.org](http://commons.wikimedia.org/wiki/File:Magdeburger-Halbkugeln.jpg)*


It has always seemed important to me to establish the experimental as a distinct activity in the field of architecture because of the changing nature of the field in the contemporary world. In the past, the role of buildings and the architects who designed them was clearly defined, because society itself was clearly defined in its hierarchical structure. There was little need for theory because people knew what buildings were supposed to do, the only variables being of style and arrangement of architecture's already known, historically precedented components. Consequently, there was no use for the experimental or an in-between stage of design. Whatever experimenting the architect did was folded into the normal design process. There were no new hypotheses to confirm before the architect and a client committed themselves to full-scale construction.


But all this changed when our society – ever more global in scope and technological in character – itself began to change more and more quickly. There are new technologies introduced almost every day and these change the ways people live. Political and economic changes are happening almost as frequently. It seems obvious that the pace of change is accelerating to the point where the changes are qualitative and structural. The need for new kinds of space is growing, but architects – in their practices – don't have either the temperament or the time to explore new possibilities. This creates the need for the experimental architect, who is devoted to such exploration. This is a historically new situation, and I believe that the field of architecture has yet to respond to it. RIEA, I have hoped, will create the example of what might be done.


***CC**: You founded the Research Institute for Experimental Architecture in 1988, and have been directing it now for over 20 years. What role do you envisage for RIEA in relation to the issues discussed?*


![lebbeus_woods_2](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/lebbeus_woods_2.jpg "lebbeus_woods_2")  

*(above) Image taken from Peter Cook, RIEA The First Conference, Princeton Architectural Press, New York, 1989. (Seated around the table, counterclockwise: Gordon Gilbert, Michael Webb, Hani Rashid, Michael Sorkin, Lebbeus Woods, Ken Kaplan, Ted Krueger, Peter Cook, Neil Denari.*


**LW**: As you know, RIEA is being directed by a Board of Directors now and is more open and democratic than in the days when I had the greatest influence. I think that is a good development, especially considering the complexity of problems in the world today that confront architecture and that RIEA is engaging with. It is really my hope that RIEA will show leadership in identifying the most critical problems, clearly formulating them, and then contribute both concepts as well as analytical and design techniques towards some practical solutions.  

Of course, this is already overly ambitious, but I think a group such as RIEA may be one of the few that can freely set the highest goals, because we don't have a political or economic agenda. If we only get halfway there, that will still amount to a lot.


***CC**: The Building Industry is possibly the most reluctant sector to embrace innovation, both from a cultural and technological point of view. Tradition is a highly respected cultural value – even though it often hides cheap vernacularism and low-cost-low-quality building techniques – while technological innovation is mostly produced in other sectors – military, aeronautical and mechanical – before it is translated into architecture.*


*Frankly, I think to justify this by saying that only certain sectors can afford to invest in R & D just isn't good enough. The truth is that architects are struggling to find ways of boosting innovation in their own sector.*


*The UN estimates that architecture, as a profession, is affecting no more than 5% of what is being built worldwide. Proportionally, this indicates that architects are only responding to the needs of an extremely small, and affluent percentage of the world's population. Does this mean that we, as architects, are incapable of forging change and becoming real innovators for fear of the risks and responsibility involved in addressing different fields of work and research? What role can architectural education adapt in relation to these questions?*


**LW**: The most important part of education is asking questions. Young people entering schools of architecture are naturally curious, though they don't always know what are the most important questions to ask for the future. With more experience in the world generally and in the field of architecture itself, their teachers should offer guidance by framing the most critical questions about what it means to be an architect in today's world. This should be done in the design studio, where everything comes together, and not only in separate elective courses on ethics, politics, history.


In the high-pressure realm of professional practice, the really important questions usually don't get asked and architects ride along on a wave of assumptions about what their responsibilities are and to whom. Architects' associations are little more than dues-supported clubs. It's only schools, really, where the tough questions get asked.


***CC:** What are the most urgent fields of research that Experimental Architecture should engage in today, and in the immediate future?*


**LW**: The most critical and difficult problem is the rapid growth of cities. The widening gap between the poor and the rich means that much of this growth is in the form of slums and other kinds of unsustainable – in human and environmental terms – newly built urban landscapes. Architecture can't directly affect the economic disparities, but it can refuse to cooperate with the social institutions that create them. Also, architecture can propose the best possible solutions to slums and deteriorating urban conditions, and not only those that serve prevailing political and economic powers. This is because RIEA does not depend on those powers for its existence.


Certainly, there is a great need for low-cost housing – for housing the growing urban populations. I'm sure that the term ‘housing' has to change, because it is a typology based on rigid social categories that are today outmoded. Housing meant ‘mass-housing' for masses of workers employed on factory assembly lines, originally in the form of ‘company towns.' Later, housing ‘projects' were designed and built for masses of lower-income urbanites, an underclass that still exists but is much too diverse to be massed together and in effect ghetto-ized.


Today's increasingly service-based economy, one fragmented by computers and internet niches, has created a need for an entirely new typology of living quarters and their groupings, one that has yet to be invented and tested. This is priority for experimental architects.


Another priority – an even more difficult task – is how to improve the living conditions in existing slums. The best thing would be to eliminate slums – the ones existing today – and make sure no more are built ever again. But this would require the elimination of poverty. Not only is that way out of any architect's domain, but it's a task that society as a whole must be committed to achieving – a commitment that has not yet begun to be made. Until it is – if it ever is – architects can and must look for ways for strategies and techniques that slum-dwellers can use to help themselves, if necessary a small step at a time. Something will be better than nothing – which is more or less what's happening today – architects don't want to get near the problem.


Finally, for now, I would say that architects who want to explore the most cutting edge issues must confront the relationship of aesthetics – the way things look – to ethics – what things mean in the most fully human sense of the term. Consumerism and mass-marketing have ghetto-ized architects, and especially the most visually talented ones, as product designers, stylists who dress-up conventional products to make them more marketable or simply serve as a form of prestige advertising. Breaking out of and away from this captivity is the highest priority challenge to the field of architecture. What makes it difficult is that architects must develop their social commitments without sacrificing their aesthetic ideals.


It's not sustainability OR beauty, practicality OR poetry, but both that have to be accomplished at the same time, in total harmony and support of each other.


***[Due to the lengthiness of this content-rich interview we have decided to publish it in two parts. Part 2 will be posted later this month.Corrado Curti.]***


——————————————————————————————————————————————————————————————————-


*¹[lebbeuswoods.wordpress.com/2010/08/12/the-experimenta](https://lebbeuswoods.wordpress.com/2010/08/12/the-experimenta)l*


**² The famous discourse was originally presented by Richard Feynman at the fifteenth annual meeting of the National Science Teachers Association in 1966, in New York City. The text is fully available at: [www.fotuva.org/feynman/what\_is\_science.html](http://www.fotuva.org/feynman/what_is_science.html)**


***³Popper K, The Logic of Scientific Discovery, Routledge Classics, London 2002, ISBN 0415278449, A brief excerpt is available at: bio.classes.ucsc.edu/bio160/Bio160readings/Logic%20of%20Scientific%20Discovery.pdf***


****4The First RIEA conference on Experimental Architecture was held at Emmond Farms, Oneonta, New York 1989, The First Conference featured: Peter Cook, Lise Anne Couture, Neil Denari, Godon Gilbert, Ken Kaplan, Ted Krueger, Hani Rashid, Micheal Sorkin, Micheal Webb, Lebbeus Woods****


*****5Lebbeus Woods has written extensively about architectural education and the ideal form of an architectural school on his blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, [101](https://lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101/), [102](https://lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102/), [201](https://lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201/), [202](https://lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202/), [301](https://lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301/), [302](https://lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302/), and [401](https://lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401/); also the AS401 series.*****


*****.*****


**PART 2**


*This interview is reprinted from the [www.cluster.eu](http://www.cluster.eu) website.*



*Friday, December 3, 2010*

An interview with **[Lebbeus Woods](http://en.wikipedia.org/wiki/Lebbeus_Woods)** by **[Corrado Curti](http://www.cluster.eu/corrado-curti)**






This post is the continuation of Corrado Curti's interview with Lebbeus Woods: Architecture As the Solid State of Thought, Part I of which was posted [here](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/) on November 22 2010. We preferred to keep the content of their dialogue unedited, but have divided it into two parts due to its lengthiness. Part I is largely focused on the practice of experimental architecture, while part II unfolds and illustrates, with a series of Woods' projects, an architect's journey through the inbetween realm of experimentation from the late 1970s to date – the full version of this interview is available for download in pdf [here](http://www.cluster.eu/v4/wp-content/uploads/2010/11/Architecture%20as%20the%20Solid%20State%20of%20Thoughts.pdf).


![lebbeus_woods_3_ptii](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/lebbeus_woods_3_ptii.jpg "lebbeus_woods_3_ptii")  

*Model of the six-kilometer long terrace, cantilevered over the sea, able to rotate under flood pressure to become a vertical barrier protecting the city of La Habana (Model by Lebbeus Woods in collaboration with Ekkerard Refeldt), image taken from lebbeuswoods.wordpress.com/*


**CC:** *Looking back at your early works we see a highly inspiring and refined formal research, while your recent projects adopt a more abstract visual language.*


*At the time when you were working on projects like War and Architecture, La Habana Nueva, and Aerial Paris5, mainstream architecture was still divided between “safe for capitalism” modernism and post-modernist decoration, while in the last 15 years formal explorations have given rise to hundreds of built and never-built urban icons characterized by extremely articulated, complex and varied shapes, often celebrating nothing more than the status of their affluent commissioners. Has this condition, where aesthetics and ethics are distanced, and self-celebrating formal research is pushed to unprecedented limits, influenced the shift in your recent works towards a cleaner and essential formal language?*


**LW**: I have always been driven in my work by a few questions, a handful of ideas, having to do with the changing nature of existence and its necessary, usually difficult, transformations. I chose architecture as my field of thought and work because it seemed to me to encompass the whole range of human experience and aspirations, and at the same time to be instrumental – literally a tool – that would be useful in our adaptation to change.


Over many years, the way that I have used the tool as an instrument of thought and action has naturally evolved. This is partly because of my own evolution and partly because our world has itself changed in its forms and dynamics. In earlier years I thought it was most important to visualize scenarios of new types of buildings responding to crises of change. More recently, I've turned to questions that are more about the possible structures of change itself, and how they might be manifest in tectonic – that is, constructed – form. In the earlier work, I wanted to reach a broad public; in the more recent work, my aim has been to address architects in particular.  

You are right to judge that the flood of form-making due to the rendering capabilities of computers has made visualizing ‘the new' seem to me much less urgent. I sometimes think that there is far too much of it now, and that, very often, spectacular imagery is used to dress up old ideas, or no ideas at all. In any event, my interest has turned elsewhere.


**CC:** *Very often in architectural literature, the terms: Visionary, Utopian and Experimental, when used to describe works and projects, become blurred. Yet, if we are to compare Architecture with the Sciences, where the experiment is a way of observing and investigating reality in relation to hypothesis, this ambiguous terminology is highly misleading.*


*Taking a closer look at your works, I think it is possible to grasp what distinguishes an experimental approach from merely visionary and/or purely utopian works of architecture. All of your projects are grounded into a very specific “terrain”, whether physical and/or conceptual, from which they stem. They are not evocative sketches of a positive, universal, displaced (un-grounded) solution: Panacea-projects or Ideal Cities. They are explorations of alternative architectural responses to very specific and often extreme conditions: the quake in San Francisco, the wall between Israel and Palestine, and the war in Sarajevo to name a few.*


![lebbeus_woods_4_ptii](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/lebbeus_woods_4_ptii.jpg "lebbeus_woods_4_ptii")  

*images from Divided Cities: Belfast, Beirut, Jerusalem, Mostar, and Nicosia (The City in the Twenty-First Century): A book by Jon Calame, Esther Ruth Charlesworth, and Foreword by Lebbeus Woods*


![lebbeus_woods_5_ptii](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/lebbeus_woods_5_ptii.jpg "lebbeus_woods_5_ptii")  

*The Wall Game, 2004, Dialogical Architectural Game, Credits: Lebbeus Woods, concept design, drawing wall/seperation/fence green line/Israel/West Bank*


*The radical interpretation (the hypothesis) of such specificity allows the projects to become tools to investigate the architecture-reality binding (the experiment), and to push and extend the boundaries of architecture as discipline (the results of the experiment). Do you agree with this reading?*


*What (other) methodological tools can be considered specific of the experimental approach in architecture?*


**LW**: Your reading is reasonable. The only qualification I would make is that the conditions you describe have little architectural history for my experimental projects to be alternatives to! Architects have largely ignored earthquakes and other natural, if violent, transformations, political walls, and wars, feeling that they were outside of architecture's proper domain, which is to design the known building typologies for the known and stable social conditions.


Instability or volatility has been considered the domain of politicians and humanitarian aid workers and the military and not the legitimate concern of architects. As one objector once said to me in a lecture on my work for Sarajevo during the war there, “We have fighter planes, don't we? They will take care of it!” Oh, yes – it's always a ‘they' who will assume responsibility for the problems we don't know how to solve.


This brings me to the methodology question you raise. We architects, in addressing the dynamic contemporary world need to adapt our habits of mind. Most importantly, we must go beyond thinking of architecture only as objects or products. Buildings are obviously important and necessary and we want them to be well designed, but at the same time they are only part of a complex human fabric that is being constantly woven and rewoven by many people, events, ideas. Architects need to see their work within this larger frame of reference and therefore need to develop broad points of view. This takes time and a continuous effort and commitment.


There is no ‘they' who can do this for the architect. Architecture in this way is a very philosophical field, yet very personally philosophical, and strongly existential, not to be simply taken from philosophers and their books. Every really good architect develops – either visually or verbally or both – a coherent world-view. It is up to each how this is formulated, that is, what method he or she uses to express a personal philosophy. But it is very certainly depends on having a disciplined method of doing so.  

 **CC:** *Looking at your recent projects and installations – La Chute, The Storm, The Wall Game 6 – it appears to me that your investigation directly addresses issues like: indeterminacy, change versus permanence, space as the result of a dynamic field of interactions rather than fixed formal definition, architecture as the product of a set of “rules of the game” rather than the direct materialization of a project.*


*What role do these concepts have in your research, and how relevant do you consider them to be in relation to the future development of architectural research?*


![lebbeus_woods_6_ptii](https://i0.wp.com/www.cluster.eu/v4/wp-content/uploads/2010/11/lebbeus_woods_6_ptii.jpg "lebbeus_woods_6_ptii")  

*The Fall installation at the Cartier Foundation for Contemporary Art, Paris.*


**LW**: As I said, making new forms as such does not seem a high priority for me, as it once did. This does not mean that there no longer exists the need for ‘a new architecture.' Actually, that need is more urgent than ever, as human society evolves in its forms and needs in an accelerating way. Nor does it mean that we do not need entirely new types of buildings – which was always the focus of my work. It does mean, however, that what is truly new will emerge from new concepts of living and of space and of design processes. New ideas. New ways of thinking and working. New approaches to designing. Of course, forms are important, but they can no longer be imposed on conditions. Rather, they will emerge from the new ideas and design methods.


The more recent projects of mine that you mention experiment with this hypothesis. No ‘finished' or ‘final' form was envisioned from the start and indeed does not exist. The forms of the spaces and their defining elements were not designed in the traditional sense of the term, but resulted from the direct engagement with particular material conditions. This approach turns away from ideas of the architect as a ‘master builder' or an ‘auteur' controlling the ultimate results. The architect must still exercise control and take responsibility for choices, but over (as you call it) the rules of the game.


In this way the shaping of the human environment becomes more democratic, more open to the input of many people involved in building, more a fertile ground for really new ideas. Oh, yes, it's risky – these projects did not result always in spaces I liked, or would have preferred according to my own tastes, but that is the price of experimentation and, I'm sure, of a more daring and demanding human future – if we are to meet it creatively.


**CC:** *What projects are you working on, right now?*


**LW**: I'm mostly writing and teaching – the two are intertwined. My blogis a major project, taking a lot of my time and interest. The online space is an emerging world of many new dimensions. I've only begun exploring it. As for design projects, there is at the moment only the Light Pavilion in Chengdu, China, which is currently under construction.


———————————————————————————————————


*5 Projects, Works and Bibliography by Lebbeus Woods can be found at: [lebbeuswoods.net](http://lebbeuswoods.net/)*


*6 [lebbeuswoods.net](http://www.cluster.eu/2010/12/03/architecture-as-the-solid-state-of-thought-a-dialogue-with-lebbeus-woods-part-ii/lebbeuswoods.net)*


















 ## Comments 
1. [kuu world » CIRCUMVENT](http://www.kuuworld.com/2010/12/circumvent/)
12.15.10 / 2am


[…] Another way to circumvent this “event” business is, well, the in-depth interview (not to be mistaken with other fastfood 10-questions versions). See this excellent example from Lebbeus Woods here. […]
