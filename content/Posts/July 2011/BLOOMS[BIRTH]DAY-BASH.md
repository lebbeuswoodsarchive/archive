
---
title: BLOOMS[BIRTH]DAY BASH
date: 2011-07-21 00:00:00 
tags: 
    - Bloomsday
    - Joyce's_Ulysses
    - party
---

# BLOOMS[BIRTH]DAY BASH


It's rare that I will post something that seems more appropriate for the gossip pages of a local tabloid, but the *Blooms[Birth]Day* party held in our house on June 16 will be the exception. Not only was it a fun party, but the honorees—Leopold Bloom, Diane Lewis, and myself—lent a unique atmosphere to the event. Bloom, the Joycean character who lived a lifetime in a single day—June 16—reinventing not only Dublin but also, perhaps, the world thereafter, was commemorated by our guests' celebratory mood, as well as readings by the gifted actor, Penny Bittone. Penny moved unannounced from room to room, taking devotees of *Ulysses* with him. Diane, whose birthday it was, surrounded by friends, was in top form. I, whose birthday it wasn't (but was being belatedly noted) enjoyed Lebanese food and took in the very animated landscape, a hermit briefly out of his lair.


Aleksandra and I are party-givers, it is true, and this was one of our best. For this reason I thought I'd share it with you.


LW


.


[![](media/thebar-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/thebar-1.jpg)


(above) At the bar (l. to r.) Steven Holl, Aleksandra Wagner, Penny Bittone (background), Chris Mann, LW.


[![](media/theroom-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/theroom-1.jpg)


The main room.


[![](media/thebar-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/thebar-21.jpg)


At the bar: Michael Sorkin, David Gersten (background), and Steven Holl.


[![](media/penny-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/penny-1.jpg)


(l. to r.) Peter Schubert, Penny Bittone (reading), Joan Copjec, Steven Holl.


[![](media/thebar-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/thebar-3.jpg)


At the bar: (l. to r.) Vladan Nikolic, Anthony Titus, Carl Jacobs and Daniel Meridor (background), Mersiha Veledar.


[![](media/dianesanda.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/dianesanda.jpg)


(l. to r.) Dan Sherer (background), Diane Lewis, Aleksandra Wagner.


[![](media/dianeleb1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/dianeleb1.jpg)


Diane Lewis and LW toasting birthdays and Bloomsday.


[![](media/kentlisadan.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/kentlisadan.jpg)


(l. to r.) Kent Kleinman, Lisa Pincus, Dan Sherer.


[![](media/thebar-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/thebar-4.jpg)


At the bar: Mersiha Veledar and James Lowder.


[![](media/vic-penny-anthony.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/vic-penny-anthony.jpg)


(l. to r.) Victoria Woods, Penny Bittone, Anthony Titus.


The photographs above are by **James Ward.**


#Bloomsday #Joyce's_Ulysses #party
 ## Comments 
1. [metamechanics](http://metamechanics.wordpress.com)
7.21.11 / 10pm


Anyone do a keg stand?


How many books in that library of yours?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.21.11 / 11pm
	
	
	chris: What is a ‘keg stand?'  
	
	A few thousand. Aleksandra and I and Victoria and I are book people. But V and I are very much online people, too.
3. [Guy](http://www.glad.ch)
7.22.11 / 11am


dear lebbeus 


looking back of sharing some “round tables” and parties with you, the warm images proof again what a great host you and your family are


all the very best 


love 


guy





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.24.11 / 6pm
	
	
	Guy: I wish you had been there! It's been too long, old friend.
5. quadruspenseroso
7.26.11 / 1am


Stephen Holl's cellphone images were hacked: Dan Scherer (Mies?). Diane Lewis & Aleksandra Wagner visibly gob-smacked & giggly-tingly after the Master recited (while puffing on that Monte Christo non-the-less) a few lines from Robert Browning's poem “Andrea del Sarto”: 


No sketches first, no studies, that's long past:  

I do what many dream of, all their lives,  

–Dream? strive to do, and agonize to do,  

And fail in doing. I could count twenty such  

On twice your fingers, and not leave this town,  

Who strive—you don't know how the others strive  

To paint a little thing like that you smeared  

Carelessly passing with your robes afloat,–  

Yet do much less, so much less, Someone says,  

(I know his name, no matter)–so much less!  

Well, less is more, Lucrezia: I am judged.  

There burns a truer light of God in them,  

In their vexed beating stuffed and stopped-up brain,  

Heart, or whate'er else, than goes on to prompt  

This low-pulsed forthright craftsman's hand of mine.


[![](media/mies-dianesanda.jpg)](http://quadruspenseroso.files.wordpress.com/2011/02/mies-dianesanda.jpg)





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.26.11 / 4pm
	
	
	dear q.: How clever of you to see through my hoax. I pasted in Dan Scherer's image over the more pensive one of Mies, because I was afraid that the Great Man's presence would shake up readers too much. Oddly, no one at the party, except for Diane and Aleksandra, recognized M at all! *Sic transit gloria mundi.*
7. [Penny Bittone](http://www.pennybittone.com)
7.26.11 / 6pm


Dear Lebbeus,


It was an exquisite night, I am honored to have joined you, and I am so pleased to have met your wonderful family and friends.  

You and the lovely Woods ladies are truly great hosts. 


Thank you again, and I hope to see you soon.  

All the best, 


Penny
9. [metamechanics](http://metamechanics.wordpress.com)
7.29.11 / 12am


A keg stand is a fratastic drinking manuever that is intent on defying the laws of gravity. The measure of one's keg standing ability is based on duration of defiance. The manuever typically involves two fellow party colleagues and yourself. Your colleagues grab both your legs and turn you upside down into a handstand of sorts with the spout of the keg tap in your mouth. Other party colleagues then begin to chant numbers in an upward pattern. The keg stander with the highest chanted number is the master.


Now given your massive book collection I would further enhance the art of keg standing by requiring one to read a great philosophy book, which I know your surely own. The game would be about reading as many lines as possible and when put back in a standing upright position the keg stander would have to be able to summarize the text in a clean and concise manner. If the keg stander fails miserably they must once again stand to his head upon the keg and continue to expand their philosophical mind.


Just standard rural mid western undergrad college stuff, except for my east coast adaptation which I believe I will put into practice at Teeterfest. Just to make it challenging we'll read Deleuze's “Difference and Repetition”.


Nothing wrong with the occasional fun blog mr woods.
