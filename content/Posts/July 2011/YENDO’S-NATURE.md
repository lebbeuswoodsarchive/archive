
---
title: YENDO’S NATURE
date: 2011-07-29 00:00:00 
tags: 
    - architecture_and_nature
    - pencil_drawing
    - visionary_architecture
---

# YENDO'S NATURE


[![](media/yendo-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-3.jpg)


There are several levels on which we can approach this small series of drawings by Masahiko Yendo. Made at the same time as his celebrated projects published in *[Ironic Diversion](http://www.amazon.com/Ironic-Diversion-RIEAeuropa-Book--Yendo/dp/3211834923/ref=sr_1_1?ie=UTF8&qid=1311952313&sr=8-1)*, these virtuoso pencil drawings are lyrical visions of a harmonious meeting of human and natural worlds. In them the natural landscapes seem as constructed as the buildings seem to have grown and evolved through geo-tectonic processes. While they remain distinctly different from one another, the buildings and landscapes interact creatively, somehow dependent on each other. The drawings' lyricism—their exaltation of form and texture and meaning—differentiates them from actual situations where industrial architecture pollutes virgin landscapes. Something in the drawings convinces us that they belong together, even though our experience insists that they do not. Here we touch on Yendo's sense of the ‘ironic' or, perhaps the paradoxical nature of reality, which emerges from contradictions that can only be resolved through art and the heightened sensibilities it inspires.


Of the many pathways these drawings open for exploration is that of drawing. I have no interest or intention of reopening old discussions of the pros and cons of hand versus computer drawings—they simply go nowhere. I'm willing to grant, for the sake of exploration, that one day a computer will be able to draw exactly like Masahiko Yendo. I repeat, exactly, with all the infinitely varied tonality and all the nuance of texture, shading, and illusion of light and darkness. For that to happen, of course, the pixels of the computer drawing would have to be infinitely small, creating the actual spatial continuity of the hand drawing. Assuming that this technological feat could be achieved, what difference would there be between the hand and the computer drawing?


Absolutely none—if we consider only the drawing itself, as a product, as an object, which—in our present society—is our habitual way of perceiving not only drawings, but also the buildings they describe.


I repeat: absolutely none. IF, however, we think of drawings—even the most seductively product-like ones shown here—as evidence of a process of thinking and making, the difference is vast. Indeed, there is no way to close the gap between them. In the hand-drawn image, every mark is a decision made by the architect, an act of analysis followed by an act of synthesis, as the marks are built up, one by one. In the computer-drawn image, every mark is likewise a decision, but one made by the software, the computer program—it happens in the machine, the computer, and does not involve the architect directly. In short, in the latter case, the architect remains only a witness to the results of a process the computer controls, learning only in terms of results. In the former case, the architect learns not only the method of making, but also the intimate connections between making and results, a knowledge that is essential to the conscious development of both.


LW


[![](media/yendo-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-1.jpg)


.


[![](media/yendo-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-7.jpg)


.


[![](media/yendo-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-4.jpg)


.


[![](media/yendo-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-2.jpg)


.


[![](media/yendo-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/yendo-5.jpg)


#architecture_and_nature #pencil_drawing #visionary_architecture
 ## Comments 
1. [Michael Phillip Pearce](http://www.facebook.com/pearce.mp)
7.29.11 / 4pm


This is already happening, just not at the accuracy of what you are describing. If we compare our minds to computers, analyzing them amongst each other it is a huge understanding of how we perceive information. Tor Nørretranders, in his book, The User Illusion, has done this to what I feel a great extent, and it is a wonderful read about understanding information exchange—bits per second via human and machine, conscious and unconscious.


Computers have already given us the insight that everybody can be seen as a program, what they do, support, believe in. They can upgrade themselves through hard work, or simply have offspring to ensure the data is upgraded and protected—a constant state of evolution. I love referring us to programs, because it is much easier to deconstruct yourself and relate to others.


Perhaps this is not the direction you were headed in but I wish you a great weekend. Love the sketches from a man that really listened to his surroundings and his inside voice.  

Thanks for posting! Wish you a great weekend LW,  

Michael
3. [Guy](http://www.glad.ch)
7.29.11 / 8pm


an interesting issue what you lebbeus mention is the aspect of product and process. reffering to the product the computer will, or allready does, certainly deliver the same as mas does, but, regarding the architectural human process it will never ever do so, that is in my regards the main driving force of architecture, thank you for the post, best to mas yendo, allways max respect for him and his work, enjoy the week end, 1. of august is swiss national day, i will fire a rocket for this post , cheers, guy
5. [Digital Drawing: A Response to Lebbeus Woods « Heumann Design/Tech](http://heumanndesigntech.wordpress.com/2011/07/29/digital-drawing-a-response-to-lebbeus-woods/)
7.29.11 / 8pm


[…] is the quote from his blog: I have no interest or intention of reopening old discussions of the pros and cons of hand versus […]
7. [chris teeter](http://www.metamechanics.com)
7.29.11 / 11pm


Hey lebbeus this is a touchy subject for me, ha…so forgive the ramble/venting.


when I first started I thought all you old guys who drew architecture were frankly just mentally slow and dumb, hence all the drawings over and over for a simple piece of architecture. By the time you sketched the detail out multiple times on tracing paper I could of built the damn thing.


How would I know what to build? I imagined it, boom done.


In math class if you could solve the problem without ever picking up the pencil you were smart, this is my basis for design intelligence in architecture, if you can design the architecture without picking up a pencil you are smart.


But I think I know where you old guys are coming from. Its probably the same way I look at the construction of a building. In NYC restoration and renovation is what most architects do, and every time you open the wall there is a story one can read. The story always involves politics, technology, and economy – a serious bit of history expressed in material. I imagine this is how you guys look at drawings as you describe above regarding process.


With that said, maybe like many of my fellow peers I should of just gone into construction and not architecture, not just for money but because it appears most architecture has more conceptual and idealistic things going on than the material and energy of real architecture…mind you Mies van der Rohe expressed himself in materials first and if ever in concept later and I think we all agree Mies was one of the best. 


Now with computer, I see where you are coming from, this automatic thinking programmed by those with experience for those with less experience (usually). I hate Apple products for this very reason, its streamlined for an assumed task unlike PC's that allow you to do anything assuming you know what you what and you can figure out how to do it. 


I grew up on computers, taught myself commodore C64 basic programming language when I was 10, so you can say computer language is nearly innate. (No I am not a genius, it took me weeks to figure things out, unlike those whiz kids who imagine logic without testing it first)


When I use software I quickly realize the programmers logic and its limitations. I also hate sketchup because it was made to make life easy for those who don't want to figure it all out from ground up and although I never use it, if you ask me a techncial sketchup question, usually in 10 minutes I will give you a response, 5 minutes to learn the logic of the software, and 5 minutes to figure out it can't be done easily.


General rule of thumb with software, if streamlined for easy usage by computer illiterate for a task that was once done manually, the software is extremely limited.


I would never use Revit for restoration work. Even 2d autoCAD is tough for classical restoration. You know what takes 5 minutes when dealing with classical restoration work , a pencil on gridded paper.


The most cutting edge computer architecture these days is that rhino grasshopper stuff, that is full fledged automatic thinking and work by computer with a serious amount of pre game (input of concepts and logic) prior to processing. Parametrics is as the word describes, translate design decisions into parameters (limitations) and let the computer solve it based on adjustable variables. But shortly before grasshopper the craze was scripting (a scriptor developed grasshopper, must of seen the trend in architectural scripting). scripting is write the commands out at the most Basic language possible to tell a worker what to do, the more basic language would be machine language, which is essentially speaking in electric currents with circuit boards…george boole, turing, etc.. spoke this language and man is boole's laws of thought a stupid hard read, never made it past chapter 1.


Anyway…


The computer allows visions of architecture to come to life far quicker than a drawing ever could. Now coming to life isn't expressing a concept but rather making it nearly real – photographed or filmed architecture – nearly experienced. Sure concepts help in the development of architecture but if you can imagine the architecture before picking up a pencil, drawing seems like a waste of time when you could just boot up the computer and in about the same time model in a 3d world the space with imagined materials and do a physical test on lighting before the pencil sketch of a concept is done.


Granted like bullshit and picking a girl up at the bar, sometimes a pencil sketch is far more convincing to a client than a real image. 


And Of course the pro computer arguement above is if you think architecture is a product. Not sure what else it could be? Its a product first right, a significant fact (mies frampton reference) and everything else is superflous-get-people-up in the morning religious stuff right?


Don't worry the study of process will become a history class in the future.
9. [Nick Porcino](http://twitter.com/meshula)
7.29.11 / 11pm


These images you have posted are extraordinary, and I aspire that my work in traditional media carry such weight.


I personally became a graphics programmer in order to close that gap you mention; when my works are rendered via the computer, I personally crafted every decision and equation the computer uses to create the image and I feel that the pixels, though generated via automated process, represent my intent to a precise degree. If they don't, I change them.


Also, not disputing your argument, but with regards your statement of infinite resolution, it is more precise to say that the resolution of the computer generated image must be finer than the tangle of graphite dust with paper fiber, but not moreso fine.
11. nolandlab
7.30.11 / 8pm


what about if computer representation is a completely different discipline to that of representations achieved through the mind and the hand of the designer? what if we are comparing things that are just not the same but look the same?
13. [Gio](http://gravatar.com/gb427)
7.31.11 / 5am


Impressive work indeed, There is nothing left to add to any of these drawings, quite different than those by Spiller.  

I understand your position of not getting into old discussions about the hand vs the computer, so I will not getting into that.


I met in my first year of architecture school at Pratt a man also by that name, he was doing his master program up in the very top then old gone rooms for the graduate students. I just a freshman made good friends with the graduate students who were doing their master programs and invited me to their parties and reviews. I remember that they used to call him just max and I saw him working on a pencil drawing of an interesting tower and I was so impressed with the facility and concentration of executing his drawings. I think we are talking about the same person.


I almost sure than these images are probably not really touching at all the complexity of his work, they seem to be created as means to an end and not as a searching process..or discovery….I could be wrong but that is how I feel them. I know they are beautiful and probably took him many hours but I wish there was more of the process with their mistakes in these drawings.


In this light,….I spent the last two days looking at some old drawings of The Dominican Monastery by Louis I. Kahn, were you can actually see the struggles, thoughts, lines searching, finding where the finished product -drawing-still carry these qualities.


I find all the routes valid, but the ones that really move me are the one that share the above mentioned feelings.


I also find fascinating those old structures of grain elevators, electrical stations, machine parts, chemical handling tanks, destroyed factories and buildings….I can see how he is trying to create a language with these forms and juxtaposing them against a landscape. There is a sense of the absence of time in some of his drawings except in the last one which does not move me at all.


Thanks Mr. Woods for your posts.
15. AD
8.25.11 / 6pm


I think the nature of this discussion has a problem at is root. Why are we positioning the computer and hand against one another as if one needs to prevail? Or whats being suggested here that we need to make the computer look like the hand or the hand look like the computer. They are just tools and different ones at that. Suggesting for one to do the others job seems antithetical to the nature of each and is giving too much weight to their role in the process of creating. 


I took a welding class for two years while an undergrad and being that I was new to the process, and the tools of the welder, I often found myself making decision about my work based not on my intentions but on my comfort with using the tools and what I thought I could do with them. This proved to make some interesting work however I had to accept the reality that I was not in control of it, which might be okay for a short term exploration or at least for making a sculpture. If I were to practice welding long enough I would eventually come back to square one where the tools are second nature to me and all that is left is my intentions. If I can weld anything the questions of what to make becomes of an entirely different nature and the tools become subordinate to my effort.


A craftsman comes to a point in his work were he says “ah, I have the exact tool for this”, not the reverse which is where architects are today saying “wow, that tool can do that, I bet we can use it to make something interesting”. 


We have become enslaved to technology and I do not believe we need to worry about it evolving but rather how we as the authors need to evolve in order to use it. I think we are in an awkward phase where the digital tools are still too new and exciting and our obsession with them has created at times a one dimensional effort of seeing what they can produce.


In the example of Mr. Yendo's work I do not believe it is solely in the nature of the tool being used in that the pencil is not defining his work and therefore I don't see why the computer should be seen as having a determinant effect either. Mr. Yendo as the author is not obsessed with graphite nor is he unfamiliar with his ability to use it and the result is a freedom to express his intentions. I think it is the authors that need to catch up to the technology and regain control of our intentions.
