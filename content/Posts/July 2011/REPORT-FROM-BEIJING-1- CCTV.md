
---
title: REPORT FROM BEIJING 1  CCTV
date: 2011-07-08 00:00:00
---

# REPORT FROM BEIJING 1: CCTV


***See [the article](http://www.nytimes.com/2011/07/13/arts/design/koolhaass-cctv-building-fits-beijing-as-city-of-the-future.html?hp) by Nicolai Ouroussoff in the New York Times. Intelligent, balanced, but overlooking the anti-urban nature of the CCTV building. LW comment at the end of the post.***


*The LW blog has a new feature: regular reports from Beijing, People's Republic of China (PRC), by **Cheng Feng Lau.** An earlier attempt to launch this feature was aborted when the correspondent withdrew for personal reasons. Happily, our new correspondent has agreed to make reports that will give us candid insights about the architectural scene in Beijing and other parts of China.  What's happening there today is of crucial interest as it increasingly impacts people everywhere.*


*Cheng's reports are, like this blog, expressions of personal experiences and understanding. As such they form a contrast with more institutional sources of information to be found in newspapers, magazines, television and a growing number of online sites.*


.


[![](media/biad.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/biad.jpg)


Too many things are happening in China these days. I find it difficult to organize my observations. Perhaps the best way for me to talk about these observations would be to present them in the same fragmented way I have experienced them. All these fragments should be open for further elaboration. However, at this point, I feel it is more important for me to send these fragments as they are, rather than summarizing them for you in advance.


Perhaps you already know from Rem Koolhaas's book “Great Leap Forward,” traditionally, the only legitimate form of architectural practice was government-owned architectural institutions. Thus there are always limitations within which architects might be able to engage this discipline as a form of social critique.


One can't help wondering how an ordinary Chinese would consider the relationship between a building's form and a social comment? Art as a form of political propaganda has always been there in Chinese culture, but is the counterforce equally strong? Is an ordinary, not very well educated, Chinese capable of fully assessing social criticism? Universities should be the place to shape critical thinking, but it doesn't happen. First, because most Chinese feel it is harder to get into a university than through it. As in much of Asia, the universtiy entrance exam is a life-determining experience in China. *This one exam determines the rest of one's life.* Once accepted to the university, most people will work hard, but there is the widely accepted fact that as long as you go to class and show up at exams, you will get your degree. Universities in China are good at teaching the students practical skills to be used when they enter the workforce, but they do not offer students enough methodology and not enough critical thinking. One of my friends commented recently that Chinese architecture students do not know how ask the right questions.


The Beijing Institute for Architectural Design (BIAD) is one of the two biggest institutions of architecture in China today.. There is an unwitten rule within this institute that one's job position is related to how long one has worked there. This is to say, even if one doesn't know anything; one does not know how to design; one does not understand architecture practice, as long as one stays there long enough, one is guaranteed higher positions and better pay. This means that many people in charge are completely incapable of delivering good work. But they are experts in BS. They can talk loudly and clearly as though they were real experts. Many people are frustrated about this situation. In recent years, there have been reformations going on inside some institutions. Studio style practices have begun to emerge under their umbrella, and it is more merit- and talent- based. Thus many younger people who have the ability have been able to direct projects. This was unthinkable in earlier on.


There is still a major problem. Architects in China are paid by the case (lump-sum) rather than by the time they spend on the project. Except on a few prestige projects, it is the same as in the United States. How much one is willing to push the limts with little reward is completely up to the architect.


2011 marks the 90th birthday of Chinese Communist Party. All of a sudden most of the TV dramas were switched to stories about the revolution years. Two major movies came out recently. One about the creation of Chinese Communist Party (CCP), the other about the creation of The People's Republic of China (PRC). In those movies, the CCP is protrayed as a true savior, and as true revolutionaries who suffered and fought alongside the people, especially in a drama about the creation of missiles and the atomic bomb in China. The entire country was fighting together, under the leadership of the CCP. At the same time, these films warn against forgetting the hardships caused by foreign invasions. The current Chinese national anthem, which was in fact written at the time of Japenese War, says, “…Use our flesh and blood to create our new Great Wall..,” and “…when the Chinese were under the most dangerous threat, every single person was forced to a final roar… ”


[CCTV](http://en.wikipedia.org/wiki/China_Central_Television) & TVCC


Many unfortunate things happened in relation to the CCTV and TVCC towers.


First of all, those who paid for them are still not quite sure they got their money's worth.


Second, no matter how sublime the cantilever of CCTV is, it does not quite justify the fact there is really no space inside the cantilever. It is mostly trusses.


It is rumored that the interior space at those upper levels is similar to the claustrophobic space inside a submarine, which leads to the third unfortunate aspect: not many people have seen what is inside, perhaps largely because of the fire at the neighboring TVCC?


The two buildings were supposed to be ready for the 2008 Beijing Olympics. Now we are simply not sure how long the world will have to wait. The burned façade of TVCC has been taken down. Now the reconstruction is in progress.


**CFL**


[![](media/cctv_1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/cctv_1.jpg)


.


[![](media/cctv_2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/cctv_2.jpg)


.


[![](media/cctv_3-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/cctv_3-1.jpg)


.


[![](media/cctv_4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/cctv_4.jpg)


The CCTV building is certainly a symbol of the power of the Chinese Communist Party and perhaps—as the Times critic maintains—a metaphor of China's headlong rush into the future, but its almost suburban isolation from the city around it, as well as the pathetic ‘park' at its base and the brutal way the slick glass-cladded masses cut into the ground without a hint of approach or entry, broadcasts its anti-urban spirit better than any TV propaganda produced within it. To the knowing, this is supposed to be read as ‘irony,' or ‘a critique.' To regular people it is just business as usual in an authoritarian state.


LW



 ## Comments 
1. James Polachek
1.24.12 / 9pm


This and the following two pieces on Beijing architecturing are the first online articles I've EVER encountered with something to say, and to be learned from. Keep up the good work and great photos!
