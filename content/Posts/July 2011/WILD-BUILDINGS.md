
---
title: WILD BUILDINGS
date: 2011-07-19 00:00:00 
tags: 
    - architectufre
    - spontaneous_building_of_the_Mediterranean
    - wild_building
---

# WILD BUILDINGS


[![](media/rc-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/rc-1.jpg)


*(above) A glimpse of the ‘wild building,' on the hills overlooking the southern Italian city of Reggio Calabria, in 1999. This illegal community has been named San Sperato by its new settlers, after the patron saint of desire and of hope.*


On the coasts circumventing the Mediterranean, a type of construction has proliferated in the last thirty years or so that is usually referred to as “wild building.” What is wild about it is not the buildings' designs, which are utterly conventional, but rather their lack of legal status in the towns and cities to which they are loosely attached.


Usually built in unplanned, shapeless clusters, they are like ‘squatter' communities in the same sense as slums anywhere, except they are not like typical slums. The first big and most noticeable difference is the materiality of the buildings; they are constructed of reinforced concrete, with clay tile infill—very durable and permanent. The second difference is that the people who build and inhabit these structures are not poor, but rich enough to commission such sturdy construction. In fact, they are most often from rural areas, where as farmers they earned quite well, often benefiting from government subsidies as well as the profitability of their crops and livestock.


What makes the comparison with slums at all appropriate is that, when the new settlers migrate with their families to the city, they do so for the same reasons as all slum-dwellers: to improve their economic prospects. In these cases, though, they aren't looking for low-pay factory or service jobs. They have enough money to open really profitable businesses that they plan to keep in their families for generations.


Another point of comparison is that they build on land not legally zoned by the city for residential construction, but in this case only for agricultural purposes. When they begin to build, inspectors and zoning officials tell the owners that they cannot continue, and levy ‘fines.'  Once paid, the construction continues.


Yet another similarity with slums everywhere is that these wild communities are not provided any services by the city—electricity, water, sanitation. To obtain these, the owner-residents have to find ways to generate their own or, more commonly, they simply tap into nearby city services and illegally take what they need. Once again, fines are levied and paid.


Family is the keyword in these ‘wild' communities. The buildings are constructed one floor at a time. The founding generation of a family builds the first floor of the house, extending the concrete columns, with the reinforcing bars, above, ready to receive the next floor. This will be added by the eldest son or daughter of the family, when he or she marries and founds the next generation. The structure of the house is engineered (in most cases by ad hoc methods) to receive several floors over the coming generations.


This is a city, or at least a community, that grows from the inside out, according to rules that are informal at best. Without glamorizing it, we might say it is an architecture, and an urbanism, setting an example of ad hoc growth informing a future sure to be governed by uncertainty.


.


[![](media/rc-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/rc-3.jpg)


*.*


[![](media/rc-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/rc-2.jpg)


*(above) A single family house in San Sperato, housing three generations of one famiky. It is interesting to note  the differences of architectural expression each generation has made in the vertical assemblage of the house.*


.


**LW**


#architectufre #spontaneous_building_of_the_Mediterranean #wild_building
 ## Comments 
1. Caleb Crawford
7.20.11 / 2pm


I remember seeing such dwellings when I went to Naples. I did not realize the illegal aspects, nor the aspects of family. I assumed that it was purely an issue of finances: they got enough money to build the first floor. As the family prospered, a second would be added. I am troubled by the annexation of prime farmland. This is the fate of so much of America due to suburban development like this. Long Island was incredibly fertile soil, much like NJ. Historically, such dwellings would have occupied the steeper slopes, now developers prefer the flat land. Rather a twisted economic calculus. 


In an odd way, my grandmother's house in Floral Park, Long Island was built like these settlements. Only the ground floor was built out. The vast attic space and cellar were unfinished. This allowed a family to move in, and as the family grew, additional space would be colonized in the attic and cellar. My grandmother's had half the attic finished, and the other half not. Other houses on the street had up to 3 bedrooms on the second floor. My grandfather had a “man cave” in the cellar, outfitted with a bar and card table. 


Though in no way equivalent, I like the notion of the open space, that the residents have a level of freedom to further develop the space, that the “architecture” is in a constant state of becoming, rather than a finished condition.
3. [aborham](http://drawingparallels.blogspot.com/)
7.21.11 / 1pm


this is the case also with most of the informal areas around Cairo whether it expanded over agriculture or desert land. as it contains a demographic mixture of wealthy people who originally migrated from rural areas and are deeply rooted in these areas either socially as most of their neighbors are relatives as well, or economically as there private business are established within these settlements and depending on strong networks between workshops and storage places. more on informal areas of Cairo can be found in a publication issued by GTZ (<http://www.gtz.de/en/index2.htm>) in this link: <http://www.citiesalliance.org/ca/sites/citiesalliance.org/files/CA_Docs/resources/Cairo's%20Informal%20Areas%20Between%20Urban%20Challenges%20and%20Hidden%20Potentials/CairosInformalAreas_Ch1.pdf>  

and for whoever interested in the whole publication it can be found in this link:  

[Click to access CairosInformalAreas\_fulltext.pdf](http://www.citiesalliance.org/ca/sites/citiesalliance.org/files/CA_Docs/resources/Cairo's%20Informal%20Areas%20Between%20Urban%20Challenges%20and%20Hidden%20Potentials/CairosInformalAreas_fulltext.pdf)

  

a parallel to this can be found in the gecekondus of Istanbul  

more on this can be found here:  

<http://drawingparallels.blogspot.com/2011/04/demolition-cant-happen-here.html>
5. [Salvatore D'Agostino](http://wilfingarchitettura.blogspot.com/)
7.24.11 / 5am


aborham,  

informal areas are not.  

There are no dwelling houses for the indigent.  

Give a way to live.  

Some links in Italian (the translators used in the network):  

[Una storia come le case senza tetto di Vincent Filosa](http://wilfingarchitettura.blogspot.com/2011/05/0047-speculazione-una-storia-come-le.html)  

[Andrea Pertoldeo | Marina di Strongoli](http://wilfingarchitettura.blogspot.com/2011/06/0017-b-uso-andrea-pertoldeo-marina-di.html)  

 [San Sperato now seen by google map google map](http://maps.google.it/maps?q=San+Sperato&hl=it&ll=38.092046%2C15.695536&spn=0.010403%2C0.024719&sll=41.442726%2C12.392578&sspn=20.098543%2C33.881836&t=h&z=16&layer=c&cbll=38.091953%2C15.69553&panoid=xNDT8UKXAFO1b3GgsdgFcQ&cbp=12%2C197.45%2C%2C0%2C-13.91) 





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.24.11 / 11am
	
	
	Salvatore D'Agostino: Thanks for a great set of references that expands the WILD BUILDINGS post. One of the most striking aspects of this urban phenomenon is the dreariness and mediocrity of its architecture. The builders of these houses are following the only models they know—it's a pity that they're not aware of the potential of the design of space to enable more fully their new way of living, which cries out for a new architecture.
	
	
	
	
	
		1. [Salvatore D'Agostino](http://wilfingarchitettura.blogspot.com/)
		7.24.11 / 5pm
		
		
		Lebbeus Woods,  
		
		i can translate and put this article on my blog [Wilfing Architettura](http://wilfingarchitettura.blogspot.com/)?
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		7.25.11 / 2pm
		
		
		Salvatore D'Agostino: Yes, you have my permission.
7. wilfingarchitettura
7.27.11 / 8am


Lebbeus Woods,  

thank you.  

I can not find your email.  

As soon as you step the link will be published.





	1. [Salvatore D'Agostino](http://wilfingarchitettura.blogspot.com/)
	9.13.11 / 5pm
	
	
	Lebbeus Woods,  
	
	[here](http://wilfingarchitettura.blogspot.com/2011/09/0020-b-uso-lebbeus-woods-san-sperato.html) is the translation.  
	
	Thanks,  
	
	Salvatore D'Agostino
9. [daniela](http://gravatar.com/danielamonaco1)
12.14.11 / 8am


i was on one of those cars….i remember when I and other seven of us (in that period we use to call us “ibrido group”) arrived in that place to project a pubblic space for the faculty of architecture of reggio calabria…we immediatly thought that a place like that must be known by a genio like lebbeus woods, someone who could give us a new, different point of view. we looked for him in all over the world, until we knew he was in switzerland. we hadn't lot of money, so we all togheter payed a tiket for one (or two..i cannot remember now) to meet him in vico morcote, lugano. from there (he arrived in reggio calabria with all his students of Riea.ch, then we went in vico morcote to finish our semester) it was a new amazing experience that changed our lives as students of architecture first, and as architects later. thank you lebbeus





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.14.11 / 11pm
	
	
	daniela: Working with you, Donatella, and the others from Reggio, first in San Sperato and then in Vico Morcote, was certainly a great experience for me. It is I who wish to thank you. And then came Taormina….!
