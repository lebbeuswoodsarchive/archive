
---
title: SLICED CITY
date: 2011-07-13 00:00:00 
tags: 
    - architecture
    - city
    - story-telling
---

# SLICED CITY


[![](media/00-5k.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/00-5k.jpg)


.


Abruptly, the city was attacked. The enemy had the strategy of destroying the command and control centers of the city defenses, not with bombs and other crude explosive weapons, but rather with powerful laser devices that selectively sliced through the buildings, leaving clean gaps where the corporate offices governing the military had once been. At that time long ago, armies had been privatized and were, in a sense, branches of vast companies that produced everything from the mostly robotic weaponry to organic shampoo, neatly integrating every aspect of people's lives on a free-market basis. Government independent of the companies, such as there was remaining of it, concerned itself with public entertainment in the form of ritualistic spectacles satisfying what was called “people's nostalgia for the infinite.” At any rate, the enemy's laser beams, powered by the Earth's rotation, created a side effect of levitating the upper masses of buildings, so they didn't collapse into the building masses below. Rather, they created what amounted to an open space across the city, from one bounding river to the other, high above the level of once bustling streets and sidewalks below. Within a year or two after the first attack, the city's millions fled for safer or at least less embattled places. During this period, the rivers rose, flooding the city's deep canyons, as if in fulfillment of evironmentalists' direst warnings about human technology wreaking irreversible change on the planet. All the worst things, it seemed, happened within a few dramatic years.


The city's defenders had suffered serious setbacks, but they were not defeated. Even though they occupied a depopulated city, their corporate loyalties placed great value on real estate, empty or not. And because the enemy had left the buildings for the most part undamaged, the defenders believed that there were huge profits to be made in repopulating the city when the war was over—if only they could hold out.


So they devised a plan. They established a final redoubt high in several of the abandoned towers, filled with the still powerful technology needed for defense—an ultimate command and control center. Electronically shielding it from the enemy's detection, all that remained was to create a physical pathway that would give them the mobility to move about in the city, but one that could not be followed by the enemy. Cleverly realizing that the levitational forces that held the upper part of the sliced towers suspended in the air could be organized to make a pathway also suspended through and between the buildings, they devised a method of constantly shifting walking surfaces—floating beams and planks—controlled by codes that only the defenders knew. The defenders counted on the greed of the enemy to prevent them from simply leveling the city, and they were right. For some years, they were able to conduct the war, which by now had spread to other cities, from their ultimate center of command.


But the enemy was clever, too, if a bit slow to devise a counter-plan. Finally, though, they determined to solve the puzzle of the floating pathways, and sent swarms of drone-probes throughout the city to detect any physical movement among the shifting beams, and perhaps to unlock the secrets of their guiding codes.


It is precisely here that we leave the story.


.


[![](media/1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/1.jpg)


.


 


[![](media/2-2-2d.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/2-2-2d.jpg)


.


 


.[![](media/3-3-2f.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/3-3-2f.jpg)


*The above story and drawings are copyrighted by Lebbeus Woods. All right are reserved.*


#architecture #city #story-telling
 ## Comments 
1. djeak
8.10.11 / 10pm


This could make a great video game — something like Mirror's Edge meets Tetris. Or possibly even in augmented-reality.


And the victory condition would be … more real estate? Or the harnessing and re-purposing of this magnetic side effect into new non-war living conditions? Commercialized security mazes? Things unforseen…?
