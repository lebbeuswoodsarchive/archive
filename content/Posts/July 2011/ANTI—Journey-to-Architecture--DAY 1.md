
---
title: ANTI—Journey to Architecture  DAY 1
date: 2011-07-10 00:00:00 
tags: 
    - architecture
    - road_trip
    - Ronchamp
---

# ANTI—Journey to Architecture: DAY 1


## 


*(above) Raimund Abraham's first sketch of our projected journey. Note that the destination is shown and circled as Lyon, France, our idea of the location of Le Courbusier's monastery of La Tourette.* *(below) Drawing of the actual path of our jouney, showing the actual location of La Tourette to the west of Lyon:*


## 


## .


## **THE ANTI JOURNEY**


**(as it turned out)**


## **DAY 1**


## **21 Sunday**


Zurich


Ronchamp


(police incident)


Besançon


## **DAY 2**


## **22 Monday**


Besançon


Arc-et-Senans (Salines Royal)


Omans (Gustave Courbet house/museum)


Besançon


(hospital incident)


## **DAY 3**


## **23 Tuesday**


Besançon


Tour-de-Pin


(car breakdown)


Lyon


## **DAY 4**


## **24 Wednesday**


Lyon


(Saint-Exupéry airport)


L'Abresle/La Tourette


Roanne


## **DAY 5**


## **25 Thursday**


Roanne


L'Abresle/La Tourette


Roanne


## **DAY 6**


## **26 Friday**


Roanne


Firminy (Firminy-Vert)


Zurich (via Geneva, Montreux, Bern)


## **DAY 7**


## **27 Saturday**


Zurich


JFK


.


## **DAY 1–21 October**


It has always been exciting for me to land on the continent of Europe. My generation of Americans was late to go for the first time—I was thirty-eight—but we had grown up steeped in European lore. I developed a romantic idea of the place, or rather the places, packed with history, compressed, by American standards, into so little geography. For most of us then, Europe was a point of origin, a beginning so old and deep that it predated living memory. It was not only the source of all our myths but was itself mythic. America, we believed as I was growing up, was just an extension of Europe, its wild and open western edge. All that has changed, of course, and such perceptions are rightly understood as naïve, at best. Still, a bit of the old romance remains, even on an early morning in the Zurich airport.


Baggage collected, car rented, we set off by nine or so in the direction of France, that is, westward. Both R. and I are drivers, but he seemed most eager to drive and did. I was the navigator, although the truth was neither of us had brought any detailed road maps and hadn't thought to buy any at the airport. All we had was a general idea of direction, taken from an old German atlas Raimund had used to sketch out our general route, which showed beautifully rendered topography but few roads and none with numbers. So we followed road signs, first past Zurich to Basel, then across the border into France. Our first destination was Ronchamp.


The weather was cool but very clear, the highway good, and the car powerful. I quickly understood that R. liked to drive fast, and rather than sit nervously watching his every move, I decided to sit back and focus on my self-appointed task, which was to record our conversations, wherever they might go. I pulled out my little tape recorder, turned it on, placed it bwteen us in the front seats, and we began to talk. It was a curious moment. The presence of a tape recorder always has the effect of inducing a degree of self-consciousness, and this was—for me at least—amplified by the fact that here we were, in the middle of nowhere, speeding across an unknown landscape, with only a general notion of why. Added to this strangeness was an uncertainty caused by a nearly total lack of planning. We had no timetable, had booked no hotels, and had agreed only to stop at Le Corbusier's chapel at Ronchamp on the way to our principal destination, the monastery designed by him at La Tourette. There was another factor that made the taping of our conversation as we drove a bit awkward, and that was the conversations themselves. Although we had known each other for years and exchanged views and comments in teaching situations, such as reviews of students' work, we had not spent much time together and certainly had discussed architecture very little. Yet we had come a long way to see works of another architect and use the occasion to exchange our thoughts about them and other architectural matters of concern. The goal we had discussed was to use them as the core of a book, hence the recordings which would be transcribed. Like everything else about this trip, that is as far as we had gone. All remained vague, but open.


Looking back, I am amazed at our naïve, intuitive faith in ourselves, or our sheer sense of adventure. Looking at it one way, we had little to lose but our time and a bit of money, if things went badly, or just not well enough. But from another perspective, we had much to lose. We were gambling our friendship and what could be called our seriousness about architecture—in exactly the same way as with every drawing, every design that each of us had made over many years. There are no free rides when one works, or when one speaks about architecture. Everything that is drawn or said matters. Everything is always at stake, even though it is not spoken about explicitly. As we rode through the French countryside that day, we were of the same mind about this, I was sure, though it remained unsaid.


Of course, we talked mostly about how to get where we were going. Finally, we stopped at a highway shop and bought a highly detailed road atlas for all of France. Who knew where we might go? With this in hand, we followed the right roads and within an hour or so, we were approaching Ronchamp.


R. became very excited. He had come there in 1957, when the Chapel Notre Dame du Haut had just been completed, on a young Austrian architect's pilgrimage to a new masterwork. He remembered a long, narrow road leading through small villages until a great green hill appeared at the end, with the gleaming white chapel at its crest. Indeed, there was a long, narrow road, though it no longer passes through small villages, but rather through small towns thick with car traffic, chain stores, and the depressing pall of sameness that speaks of commercialization anywhere today. Passing through the towns, we drove between empty fields under a gray October sky. R. remembered large trees that had lined the road, but now there were none, only relentlessly barren vistas of fields on either side. We kept our gaze straight ahead, looking for the great hill. Eventually it did appear, though it was not green, but a flat autumn gray, and the chapel could not be seen from below. We followed the well-marked road up the hill. I had never been there before and had no expectations. But R's mood had turned dark and critical. No doubt he was disappointed and also had a sense of what lay ahead.


The road up ended in a large parking lot next to an ugly little building that advertised itself as a “tourist center.” This was the gateway to the masterpiece. We had to buy tickets there and did. Walking up a path festooned with plaques of commemoration and warnings about what not to do, interspersed with neat little pots of flowers, we finally arrived at the chapel. Looking to me just as I had expected—a great strange white object sitting on a vast, eternally watered and groomed green lawn—I was neither startled nor amazed. It was a building I already knew well, not only from countless photos, but almost more so from my imagination which was informed by the photos, but also the architect's drawings. Through them, I had walked around the chapel many times, struck by the differences in the four elevations and their aesthetic inconsistencies. Seeing it first-hand, I recalled the first photo I had seen of the building, also in 1957, when it was shown to me by a friend who was already in architecture school. It was the latest thing, really hot, he assured me. I could only think that it was the ugliest building I had ever seen: awkward, misshapen, odd and graceless—hardly a building at all by any standards I had at the time, and these were much influenced by De Stijl and the works of Mies. That same year I had traveled with friends to Chicago and seen another just-finished masterpiece, Crown Hall at IIT, and was deeply impressed with its clarity, economy and harmony. The chapel had none of this, nor did it have another quality I admired in the Mies building: a feeling for history that projected a sense that it was extending into the modern age a great, classical tradition. Le Corbusier's building seemed alien, as if from another planet, another civilization, one that celebrated the primitive and the ugly. While this first impression never entirely dissipated, I eventually began to appreciate these very qualities, and to see the paradoxical complexity of the building as a virtue. The chapel became more deeply planted in my psyche than most other buildings, precisely because my feelings about it remained unresolved.


The chapel is an impressive building, a bit larger than I imagined. It is hard to judge its scale from photos and drawings, because it lacks familiar references, such as normal size doors. Everything is made as though for another scale, another breed of sentient beings—indeed is seems like an alien object that landed here, and took root. That aspect appeals immensely. We are there an hour, then leave, driving down the green hill to the highway west. On the way we stop at a dirt road leading to an abandoned mine elevator building, monumental, austere, deserted. After the hilltop, its unselfconscious architectural presence, laden with evocations of past and present, refreshes the spirit.


Needing to find a hotel for the night, we head for the nearest larger town on the map, Besançon. Halfway there, I am flagged down by a highway policeman in a snappy blue uniform and jackboots who shouts at me in French, which I cannot speak. After gesturing to me that I get out of the car, he indicates on a slip of paper that I have been speeding and takes me to a waiting van. Thinking that I will be taken to jail, I start to get in, when I see that the policemen has sat down at a desk inside the back and is writing a ticket. Handing it to me, I see that the fine is 90 Euros, which Raimund and I cobble together from the few bills we have, and pay the man so we can proceed. Raimund demands, in English, a receipt, which I am given, and we are again on our way. All the while, I am thinking that the Frenchman is angry mostly because he is not being paid in French Francs!


Besançon, when we arrive with R. at the wheel, is fine and old. We follow a few hotel signs and arrive finally at a small hotel with a courtyard where we can put the car, and are greeted by the middle-aged owner, who indeed has two rooms. It is quaint and old and comfortable. After going to a nearby restaurant with the most indifferent food and wine, we retire for the night.


.


*(below) Photos by Raimund Abraham. My task was to record as much as possible with a camcorder. The tickets and trinkets shop just outside the entrance to the Chapel's grounds, an egregiously banal little building that is the visitor's introduction to the seminal work of architecture beyond:*


[![](media/ron-ra-31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-31.jpg)


*The gated community of the Chapel Notre Dame du Haut, at Ronchamp:*


[![](media/ron-ra-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-8.jpg)


*The strength of the Chapel's design is most palpable where its rather discordant forms meet one another:*


 [![](media/ron-ra-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-1.jpg)


[![](media/ron-ra-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-2.jpg)


[![](media/ron-ra-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-4.jpg)


[![](media/ron-ra-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-5.jpg)


*Coming back down the long road from the hill crowned by the Chapel, we discovered an abandoned structure we thought had once been used as a mining elevator. We were struck by its strangely sacred character, as though digging into the earth to extract its precious minerals had once been considered an almost religious act:*  

[![](media/ron-ra-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-6.jpg)


[![](media/ron-ra-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ron-ra-7.jpg)


After seeing the Chapel, we consulted out map in order to find a place to spend the night. The closest town of any size was Besançon. Adjoining this charming square we found a hotel with vacancies—a true bit of good luck:


[![](media/bes-ra-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-ra-1.jpg)


[![](media/bes-ra-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-ra-2.jpg)


**LW**


*to be continued*


#architecture #road_trip #Ronchamp
 ## Comments 
1. John Maas
7.10.11 / 4pm


Thanks ever so much for the pictures of the industrial relic along the road up to from Ronchamps to the chapel. It drew me to look at it in 1984 and I am a bit surprised it is still empty. It would make for an interesting retrofit into a house. Any idea about the burned ground?
3. [Gio](http://gravatar.com/gb427)
7.12.11 / 7pm


Thanks for posting something so personal, Mr Woods.


I have never been to Europe, all I know of it is what I have learnt through books and drawings and videos, I hope one day to go.


Mr. Woods, I have a small book of Le corbusier's work about the construction and design of Ronchamp and in it says that Le corbusier used the “visual echo of the landscape” as the design generator of the forms. So I ask, looking to the North, South, East and West do you recall images that can be seen in the elevations or the plan? I know that now all looks different as you posted it but there must be something of the mountains and valleys left for us to see.


The day I go there, that would be the first thing I will draw..all the four vistas.  

Thanks for your post again.


Gio
5. Flavin
11.15.11 / 12am


This is great, I've jumped back from the 6th installment to restart here. Thanks


Flavin
