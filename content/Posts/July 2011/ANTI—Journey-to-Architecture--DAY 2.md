
---
title: ANTI—Journey to Architecture  DAY 2
date: 2011-07-27 00:00:00
---

# ANTI—Journey to Architecture: DAY 2


## 


*(above) Raimund Abraham and LW approaching the Governor's House at the Royal Saltworks by Claude Nicolas Ledoux, 1779. A frame from the video by LW.*


## **Day 2—October 22**


We came to see La Tourette, to stay there and have a dialogue over a few days about architecture, but we are a long way from our destination. Not only that, we've made no advance arrangements, no hotel reservations or plans of any kind. In truth, we're not even sure where La Tourette is, besides vague notions that it's near Lyon. In the morning over a small breakfast, we agree that we might as well visit Ledoux's project, the Royal Salt Works, not too distant from Besançon. It's a long way from Le Corbusier to Ledoux, but off we go, following a few simple directions.


The countryside here is beautiful, in the normal sense of the word. Rolling green hills and gentle valleys, with manicured farms that speak of strict laws about what can be done in the countryside—for example, no roadside advertising of any kind. Every vista is a postcard. A controlled bucolic world, perfect for admiring tourists.


We arrive at the Salt Works too early. It doesn't open until after noon, so we drive about a bit, then stop at a bustling little restaurant, filled with working people, where we eat a little and dare a glass of wine. French driving laws concerning alcohol are very strict—perfect for a culture of morning-to-night wine drinkers—and Raimund is concerned about what he imagines as my ‘drinking problem'. He is nervous because he saw a bottle of vodka in my baggage.


At the appointed hour, we enter the Salt Works, together with a handful of others. It is sunny, cool, and very windy. The vast. semi-circular open space that Ledoux meant to somehow be public, strikes me as imperial in scale and almost military in mood—a parade ground for long-ago armies of workers, hardly a park for meanderings and picnics and play. The Governor's House and salt factory buildings sit rigidly on axis, a mere confirmation of authority. Ledoux, clearly, was a royalist through and through. His buildings are in a rustic classical style, with no relief from symmetry or overbearing hierarchy, and I wonder why he is considered as a forerunner of Modern architecture, as eminent historians claim. No doubt this idea comes from his imaginative use of abstract geometrical forms, as in some of his houses, but little of that is in evidence here, where he seems very much a part of an old world—*anciens regime*—that has, thankfully, been superseded by more egalitarian messiness. I dislike the place intensely.


Oddly enough, the centrally placed Governor's House has been reformed into a museum of Utopian architecture and city-planning, from Ledoux to Koolhaas, with many wonderfully made models and displayed reproductions of drawings. Ledoux is, of course, considered a Utopian, the architect of an Ideal of the human condition, but his Ideal is antithetical to most of the more recent projects displayed—perhaps that's the point of the exhibition.


It is not clear to me how Raimund feels about this rigorously symmetrical ensemble of buildings, beautifully revealed in the stark afternoon sunlight, which are more related to much of his work than to mine. Perhaps a clue is that he objects to the interior remodelings of the Governor's House and flanking factory buildings, which have been converted to grand exhibition spaces ignoring their original uses. Better that they remain as ruins than this sort of restored tourist attraction, he says. I take his point as a sign of empathy with the place and its architecture.


We returned, driving through Onans, where we visited the house of Gustave Courbet, the great 19th century painter, which is now a small museum containing many of his works of varying quality. A quaint, elegaic experience, especially after the Ledoux. Courbet's realism added to the emotional effect that was vaguely melancholy. Driving on to Besançon, through a hilly, twilit pastoral landscape broken incongruously only by a line of high cliffs, we arrived at the hotel to change for dinner somewhere in the town. Later, on the walk through the city, I had a severe attack of ‘vertigo'—loss of balance—a condition that was not medically diagnosed until several months later as the result of an inner ear infection. Raimund thought I had one too many—the bottle of vodka—and then that I was having a heart attack. To keep from falling down, I clung to a chain-link fence on the pedestrian way we had been following. I wanted to get a taxi to take us back to the hotel, but Raimund enlisted some passersby to call an ambulance and I was taken to the emergency room of a local hospital. After a few tests and an excruciating half-dozen hours lying in the emergency ward, listening to moans and howls in French, I was released at three the next morning. Feeling much more steady on my feet, I returned by taxi to the hotel and got a few hours sleep.


.


[![](media/ledoux-plan.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/ledoux-plan.jpg)


*(above) Ledoux's vision of the Royal Saltworks, Only half of it was actually built.*


[![](media/cafe3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/cafe3.jpg)


*LW and RA in a cafe near the Saltworks, waiting for it to open. A frame from the video.*


[![](media/entry-saltworks.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/entry-saltworks.jpg)


*The entry buildings.*


[![](media/gov-house-factories.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/gov-house-factories.jpg)


*Inside the vast open space within the complex, facing the Governor's House and flanking factory buildings.*


[![](media/lw-govs-house-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/lw-govs-house-1.jpg)


*LW in the colonnade of the Governor's House.*


[![](media/bes-going-to-dinner.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-going-to-dinner.jpg)


LW in Besançon, on the way to dinner.


[![](media/bes-store-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-store-1.jpg)


*Raimund was fascinated by shop windows and felt they told you more about a city than the officially important sites.*


[![](media/bes-store-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-store-2.jpg)


.


[![](media/bes-store-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/bes-store-3.jpg)


**LW**


*All photos by Raimund Abraham, except as noted.*


*To be continued*




 ## Comments 
1. tom
8.6.11 / 8am


I think one of the things that's interesting about Boulee and Ledoux is just how irrational their designs become. Like Ledoux's riverkeeper's house, that has a pipe running through it, and also the river. And Boulee's Newton cenotaph, that has the rocky landscape inside of it.


thanks for posting the images
3. Sotirios
8.11.11 / 4pm


I love Raimund's storefront pics, they reveal his sharp witty sense of humor and his hunt for authentic stuff. Your bus shelter shot with the twisted giraffe femme is also pretty funny. He knew how to turn the grotesque into a frightening comedy.
