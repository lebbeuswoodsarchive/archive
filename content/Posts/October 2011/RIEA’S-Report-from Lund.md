
---
title: RIEA’S Report from Lund
date: 2011-10-02 00:00:00 
tags: 
    - architectural_installation
    - experimental_architecture
    - Lund_University
    - RIEA
---

# RIEA'S Report from Lund


[![](media/lund-ws-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lund-ws-4.jpg)


*All photos in this report are by **Fredrik Dahl.***


*The following is the report of a workshop held recently by the Research Institute for Experimental Architecture (RIEA), at Lund University in Sweden. The form of the report is more academic than articles usually published on this blog, but its content is very much in the spirit of critical thinking that underlies most of the posts. RIEA is an ongoing collective of architects, theorists, experimentalists, and educators who investigate the purposes and practices of experimental thinking and design, the results of which are published in online and print journals, and in the Institute's Book and Concepts series by Springer Wien/New York.*


*I find of particular interest the ‘political' story unfolded in the report. Individuals and groups who resist changes in thinking or design often turn to laws enforcing conventionality, using them to block or undo experiments in the name of public safety—even though the experiments in question conform to the rules established by law. So it was in Lund. Still, the collaborative team in this case was able to achieve its goals, however briefly, by acting quickly and decisively. There are useful lessons to be learned from their experience.*


***LW** [Note: I was a founder of RIEA, but have not been active in its activities for some years, including the workshop in Lund.]*


## **Space(s) Innovation: Aspects of Behavior and Codes in Disguise**


”*Design can be a means of controlling human behavior, and of maintaining this behavior into the future.*”[[1]](#_ftn1)


-Lebbeus Woods


The thirty-hour workshop *Space(s) Innovation* departure from the hypothesis that architectural and urban space comprises a capacity to foster innovation. Headed by faculty Caroline Dahl, Guy Lafranchi, and Per-Johan Dahl, and executed 14-15 September 2011 in collaboration with *ESS MAX IV i regionen* – *TITA* (<http://essmax4tita.skane.com/en>) and the *Innovation in Mind* conference (www.innovationinmind.se), the workshop aimed to test this hypothesis by claiming Helgo Zettervall's neo-classical atrium of 1882 in Lund, Sweden, as a site for experimentation. By drawing from a set of traits that seems to be common to the disruptive innovators, *Space(s) Innovation* merged the concept of prefabrication and on-site construction to produce a series of large scale installations that investigated and communicated the generative aspects of space. Reconceptualizing the aesthetics and spatial attributes of electrical tubes and wiring, the workshop introduced a second layer of innovation by exploring the tectonic qualities of a material that generally is hidden inside walls and thus devoted solely to the slavery of electrics.


*Space(s) Innovation* gathered fifty-three students from the disciplines of architecture, landscape architecture, and interactive design. The interdisciplinary setting met with the objective of mobilizing an autonomous community that, on instant command, could engage in rapid design research and large scale construction.[[2]](#_ftn2) Guided by Hasso Plattner's design thinking methodology, which has been cultivated by Lafranchi's teaching at the architecture department of Bern University of Applies Sciences, the workshop formulated a six-step process that interconnected data assembly and processing with brain-storming and prototyping.[[3]](#_ftn3) The community was subdivided into eight groups and assigned an architectural or urban element that, in a tectonic fashion, could be used to demarcate spatial attributes.[[4]](#_ftn4) Drawing from New York Times journalist Steve Lohr's remark that “you can't engineer innovation, but you can increase the odds of it occurring,” the workshop deployed Hal B. Gregersen's observation that specific behaviors seems to be common to the disruptive innovators.[[5]](#_ftn5) When cross-referencing Gregersen with Lebbeus Woods's argument about design's ability to control human behavior, then it seems possible to create spaces that foster innovation.


The key approach to Gregersen and Woods was to experiment with space constructions that challenged the normative modes of inhabiting the atrium space. The installation redirected various entrances and cross-ways; it amplified the soundscape; and it utilized the affect of tubes to manipulate scale relations, sight-lines, and the haptic qualities of building components. Strict geometries were interconnected with filed conditions to generate a landscape that, almost in a surreal fashion, operated in stark contrast to the neo-classical architecture of the atrium. The coherent materiality was augmented by the different structural forms, which together directed a complex constellation of forces that coincided to shape the temporary installation.


The pace and scale of space construction was managed by the community, which operated as a fluid social construct. The space between expertise and labor was flattened due to the instant alternation of strategies and tactics that served to constitute both specific goals and provisional feedback. The workshop deployed an interior to host the installation, but the process shouldn't be limited to any traditional dichotomy of inside and outside. On the contrary, the mobilization of the workforce describes an operational procedure possible to deploy at multiple scales. As the theme of the workshop was proclaimed by Eva Dalman of Lund Department of City Planning, the relationship to urbanism, for example, was explicit throughout the working process. Due to the current planning of Brunnshog in north-eastern Lund – a new city district geared towards research and development businesses – the results from the workshop will be processed by the city planning office.


The working process produced a successive transformation of the atrium, which proved to be highly successful in its capacity to challenge normative behaviors. Positive reactions from participants and expertise at the conference were mixed with aggression from building administrators and security staff. Maria Nordh of the National Property Board Sweden, for example, forced a premature disassembly. Utilizing incorrect references to the Swedish building code, Nordh demanded the installation to be removed immediately after the closure of the conference.[[6]](#_ftn6) The premature disassembly was, on the one hand, unfortunate for students, faculty, and organizers as it hampered valuable exposure outside the realm of the conference. On the other hand, however, it pointed to the success of the workshop in providing evidence about the generative aspects of space.


The installation altered conformist behavior, but it also complied with the Swedish building code. Indeed, neither emergency exit nor access was blocked by the structure, and the plastic material of the tubes didn't prove exceptional inflammability. The circumstances that triggered Nordh's reaction, hence, had nothing to do with the material that temporary occupied the atrium, but with her perception of how to inhabit the space. When the installation altered her normative behavior, then she used the authority of codes to force eviction. And it is exactly at the intersection of the normal and the unfamiliar that the *Space(s) Innovation* workshop can be problematized. All codes and regulations strive to normalize the unknown. They use, or misuse, sovereign powers to process the space in-between security and opportunity. The key issue to be extracted from the premature disassembly of the installation complies with the demarcation of power. All innovation implies the exploration of the unknown. If we draw from Lewis Tsurumaki Lewis's argument that “beneath the surface of the normal or familiar exists the strange or the unfamiliar,” then we can conclude that the spaces capable of bypassing normative behaviors are the spaces that hold the potentials of innovation.[[7]](#_ftn7) Indeed, the recognition of the unpredictable conditions that Lafranchi is concerned about enables us to advance the spaces of excellence.[[8]](#_ftn8) The outcome of the workshop, hence, points to the argument that the Lund Department of City Planning will succeed to stimulate innovative environments at Brunnshog only if their power structures are adaptable to the paroxysmal trajectories of the unknown.


Venice, CA


27 September 2011


## Per-Johan Dahl


.


**Literature**


Dyer, Jeff, Hal B. Gregersen, and Clayton M. Christensen. *The Innovator's DNA: Mastering the Five Skills of Disruptive Innovators*. Boston, MA: Harvard Business Press, 2011.


Lafranchi, Guy. *Archonpoison*. Vienna: Springer-Verlag, 1999.


Lewis Tsurumaki Lewis. “Snafu.” In *Situaion Normal… (Pamphlet Architecture 21)*, 4-13. New York: Princeton Architectural Press, 1998.


Lohr, Steve. “Reaping the Rewards of Risk-Taking.” *New York Times*, August 27, 2011.


Plattner, Hasso, Christoph Meinel, and Ulrich Weinberg. *Design-Thinking*. München: mi-Wirtschaftsbuch, 2009.


Semper, Gottfried. *Gottfried Semper: The Four Elements of Architecture and Other Writings*. Translated by Harry Francis Mallgrave and Wolfgang Herrmann. Cambridge, Mass: Cambridge University Press, 1989.


Woods, Lebbeus. “Experimental Architecture: A Commentary.” *Avant Garde: A Journal of Theory and Criticism in Architecture and the Arts* 2 (Summer 1989): 6-19.


———. *Radical Reconstruction*. New York: Princeton Architectural Press, 1997.





---



[[1]](#_ftnref1) Lebbeus Woods, *Radical Reconstruction* (New York: Princeton Architectural Press, 1997), 23.




[[2]](#_ftnref2) The term autonomy is used here with reference to Lebbeus Woods's discourse on the experiment. Woods argues that “[t]he experimental architect of today is the precursor of the free and autonomous individual of a cultural heterarchy yet to come.” The mobilization of an autonomous community, hence, safeguards the formation of a cultural constellation capable to liberate ideas and initiatives from surrounding norms and preconceived behaviors. See ———, “Experimental Architecture: A Commentary,” *Avant Garde: A Journal of Theory and Criticism in Architecture and the Arts* 2(Summer 1989): 18.




[[3]](#_ftnref3) Read more about design thinking in Hasso Plattner, Christoph Meinel, and Ulrich Weinberg, *Design-Thinking* (München: mi-Wirtschaftsbuch, 2009).




[[4]](#_ftnref4) Gottfried Semper described the tectonic through four architectural elements. Read about Semper's elements in, for example, Gottfried Semper, *Gottfried Semper: The Four Elements of Architecture and Other Writings*, trans. Harry Francis Mallgrave and Wolfgang Herrmann (Cambridge, Mass: Cambridge University Press, 1989).




[[5]](#_ftnref5) Steve Lohr, “Reaping the Rewards of Risk-Taking,” *New York Times* August 27, 2011. Read about Hal B. Gregersen's behaviors in Jeff Dyer, Hal B. Gregersen, and Clayton M. Christensen, *The Innovator's DNA: Mastering the Five Skills of Disruptive Innovators* (Boston, MA: Harvard Business Press, 2011).




[[6]](#_ftnref6) Maria Nordh of the National Property Board Sweden argues in email correspondence, September 16, 2011, that the installation violated the premises of “fire, emergency exit, and access.” These premises are regulated by the Swedish building code BBR. The code measures pass ways for emergency exit and access to minimum 1.3 meters, which was confirmed by the installations. The code also regulates the inflammability of materials within an area demarcated to insolate fire. No attempts were made to verify the inflammability of polystyrene, which constitutes the material of the electric tubes.




[[7]](#_ftnref7) Lewis Tsurumaki Lewis, “Snafu,” in *Situaion Normal… (Pamphlet Architecture 21)* (New York: Princeton Architectural Press, 1998), 4.




[[8]](#_ftnref8) Guy Lafranchi has theorized the space of unpredictability in various texts and projects. See, for example, Guy Lafranchi, *Archonpoison* (Vienna: Springer-Verlag, 1999).


.


[![](media/lund-ws-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lund-ws-3.jpg)


.


[![](media/lund-ws-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lund-ws-1.jpg)




#architectural_installation #experimental_architecture #Lund_University #RIEA
 ## Comments 
1. joe schmoe
10.6.11 / 1pm


If you can run for Maria Nordh's position if it's an electable position, etc…or join the board that determines how the code is interpreted and approved, that would be the next step I would think…not form and space making in the traditional architectural academic manner.


My experience in NYC has proven that codes, zoning, etc… enforcement, interpretation are completely social, political and economical. Although the legal language is extremely straightforward, the clients push their professionals (architect and engineers) to find innovative ways of interpreting such language which usually later warrants a memo update by the building department further clarifying the language often denouncing the innovative reading of the code, etc…putting the clients often in awkard half built positions requiring a reconsideration by the building department….seriously – if the slab and structure is up are you really going to ask someone to tear it down based on a sentence writing on paper?
3. [Per-Johan Dahl](http://gravatar.com/pjdahl)
10.24.11 / 1pm


The problem is not so much about the physical structure but about the normalization and control of spatial perception. Nordh's reaction summons up the history of building and planning regulations. Introduced in the early twentieth-century to obstruct some health crisis, building and planning regulations have ever since been used frequently for other purposes than protecting public good. This is particularly true today when new codes are instantly introduced in Europe and the U.S. to, in the name of sustainability, mask the implementation of historically looking architecture and developer oriented city building.
