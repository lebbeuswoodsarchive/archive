
---
title: CORRIDOR OF RECOLLECTION
date: 2011-10-12 00:00:00 
tags: 
    - architectural_installation
    - exhibition_design
    - experimental_design
    - Tracy_Myers
---

# CORRIDOR OF RECOLLECTION


[![](media/corr-101.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/corr-101.jpg)


*(above) Sketch by LW of a corridor in the Heinz Architectural Center of the Carnegie Museum of Art, Pittsburgh, part of the exhibition “Lebbeus Woods: Experimental Architecture” (2004), curated  by Tracy Myers.*


The idea of the exhibition was not only to show a number of experimental projects, but to make their installation an experiment in displaying two-dimensional images that also reforms an existing space. To this end, digital prints ten feet high and lengths from four to twenty feet were mounted to rigid panels placed at varying angles to the walls and each other, supported by bent aluminum tubes. Together, these elements modulated the corridor space in ways that subtly interact with the illusory spaces of the mounted project images. A series of floor plates with etched keywords replaced the usual wall-mounted introductory text. The goal was to create an environment that not only proclaimed something new, but fulfilled its promise, even though in a modest way.


Exhibitions are temporary. The one shown here is long gone. The architecture (and I claim this installation for its domain, though many would disagree) of transience seems to me to speak of our contemporary ways of living more precisely and more poignantly that many a grand monument designed to enshrine the ideas of the permanent and enduring. It is not, though, a matter of either-or—the human world needs both. Still, little talent is devoted to the transient architecture, simply because it does not memorialize its architects. So be it.


*The photos below are by the architect, LW.*


[![](media/corr-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/corr-1.jpg)


.


[![](media/corr-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/corr-4.jpg)


.


[![](media/corr-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/corr-6.jpg)


.


[![](media/corr-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/corr-3.jpg)


A corridor links two perhaps very different spaces. It is in effect a space of transition between them. Transition. Transience. A pause. A place of reflection on what we are leaving and what we are coming to. These are the very essence of the exploratory, the speculative, and the experimental.


LW


*Note: The very fine [booklet](http://www.amazon.com/Lebbeus-Woods-Experimental-Karsten-Harries/dp/0880390433/ref=sr_1_7?ie=UTF8&qid=1318433649&sr=8-7) produced by the Museum for this exhibition is still available, but because of its rarity, at an absurd price. Also, it does not include documentation of the exhibition itself.*


#architectural_installation #exhibition_design #experimental_design #Tracy_Myers
 ## Comments 
1. Pedro Esteban
10.13.11 / 4am


CORRIDOR oh! interesting space, the path from here to the beyond, what happens when that space, that boundary becomes a plane?  

When we only have a plane as boundary, as corridor?


Good choice, express those ideas in a corridor and to project users within the language…
3. Andrei
10.13.11 / 6am


I was wondering about a minor point: what is architecture's domain?
5. [lebbeuswoods](http://www.lebbeuswoods.net)
11.7.11 / 5am


Matthew Yungert: It never occured to me. But it's an intriguing interpretation—parallax is a perceptual offset, betraying, or enhancing, our understanding of reality. In this case, the reality mixes up the actual space of the corridor was the spaces created by the drawings. Very complex. I prefer any interpretation that increases the complexity, hence the richness of reality.
