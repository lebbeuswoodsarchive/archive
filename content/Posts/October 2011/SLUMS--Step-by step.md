
---
title: SLUMS  Step by step
date: 2011-10-24 00:00:00 
tags: 
    - slum_improvement
    - slums
---

# SLUMS: Step by step


# 


# Rescued By Design


### By [MICHAEL KIMMELMAN](http://topics.nytimes.com/top/reference/timestopics/people/k/michael_kimmelman/index.html?inline=nyt-per "More Articles by Michael Kimmelman")



I JOINED the line to get into the United Nations the other day, fiddling with my iPhone before shuffling through security. The couple in back (he was toting an iPad) mused about what a design guru Steve Jobs had been. They headed toward the information desk and I toward “Design With the Other 90 Percent: Cities,” an infelicitously titled but inspired show organized by the Cooper-Hewitt National Design Museum and now installed (since the museum is closed for renovations) in the United Nations visitors' lobby.


Design shows may conjure up fizzy displays of Van Cleef & Arpels or stylish tributes to Helvetica and classic automobiles. Design implies for most people the beautiful things an affluent society makes for itself.


This show is not about that kind of design. The objects here tend to look rugged and sometimes embarrassingly simple, as in “Why hadn't anyone come up with that idea before?” Their beauty lies elsewhere: in providing economical, smart solutions to address the problems of millions of the world's poorest people.


[![](media/rd-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-3.jpg)


If the genius of Mr. Jobs was giving us sleek and effortless products that answered questions consumers hadn't thought to ask yet (can my mobile phone feature a speech-recognition system that reminds me to pick up my dry cleaning?), the designs in this case wrestle with what have long seemed intractable crises. I left stirred by how designers have tackled global pandemics like the proliferation of slums and the spread of infectious disease.


This is a design show about remaking the world, in other words. And that's thrilling, whether it's happening in Cupertino, Calif., or Uganda, where H.I.V. infects hundreds of people a day, and the latest news cellphone-wise has been the design and distribution of a text-messaging system that spreads health care information.


Text to Change, as the project is called, entails a collaboration by a pair of Dutch communication and technology specialists with local phone service providers and health care organizations. In Kibera, an area of Nairobi, Kenya, and one of the densest slums in Africa, the challenge was different. Traditional wood and charcoal fires cause rampant respiratory disease there. Refuse fills the streets. So a Nairobian architect designed a community cooker, fueled by refuse residents collect in return for time using the ovens.


From cellphones and cookers to cities: in Thailand, a public program called Baan Mankong Community Upgrading has, for the last eight years, been improving conditions in hundreds of that country's 5,500 slums, bringing residents together with government and nongovernment agencies to design safer, cleaner places to live.


[![](media/rd-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-1.jpg)


Along the Bang Bua Canal in Bangkok, where thousands of families have long squatted in rickety stilt houses linked by flimsy walkways, all teetering above polluted floodwater, architects from nearby Sripatum University were enlisted to devise row houses, detached houses and semidetached houses, along the lines of what residents said they wanted. Hundreds of decrepit stilt structures were demolished, new homes built in their stead, often from recycled doors and timber, on solid ground and near the former stilt houses, so that communities would not be broken up and families uprooted.


More than just upgrading housing and infrastructure, the strategy in Bang Bua included low-interest loans and renewable 30-year leases for slum residents, making them, for the first time, legal stakeholders in their properties. This helped to end the old cycle of evictions (in many slums around the world evictions are used to clear space for shopping malls and expressways) that left the poor in Bangkok perennially helpless and hopeless. With money from the loans, the residents of Bang Bua also decided to build a center for the elderly and disabled, and to set aside a fund for libraries, child care and school fees for the poorest families.


“It's easy to build a house, much harder to build a community,” as Cynthia E. Smith, the show's curator, told me. “Cities are very complex, and what the best designers illustrate is how to give form to sometimes very simple ideas. Good design involves bringing not just a fresh eye to problems but, most of all, listening to the people who live in those communities. We're talking about a billion people living in informal settlements today,” she added. “You can see them as a billion problems or a billion solutions.”


This show follows a smaller one Ms. Smith organized in 2007 at the Cooper-Hewitt. That exhibition included among its 34 objects a filtered drinking straw that prevents the spread of typhoid and cholera; a bamboo treadle pump that helps poor farmers in Cambodia and India extract groundwater during the dry seasons; and the Q Drum, a doughnut-shaped plastic container, easily rolled, even long distances by children, which is used to transport up to 13 gallons of water.


The 2007 exhibition set the stage for this larger undertaking about whole cities, which couldn't be timelier. We live in an era of unprecedented urban migration. Ms. Smith mentioned the billion people living in informal settlements, or slums. That number is projected to double by 2030, triple by 2050, according to the United Nations Human Settlements Program. By then one of three people on the planet will supposedly be living in favelas in Brazil, barrios in Ecuador, shack settlements in South Africa, bidonvilles in Tunisia or chapros in Nepal — the names are nearly as endless as the number of these sprawling, unplanned, impoverished places.


Ms. Smith spent a couple of years seeing what designers have been doing to improve living conditions in them. As at Bang Bua, one lesson seems strikingly obvious: the need to solicit the people living in poverty to come up with their own solutions. In so many slums — Dharavi in Mumbai, India; Corail in Bangladesh; Cape Town, South Africa; and in American cities too — the poor are left out of the process. But urban-renewal projects always work best when they're ground up, not top down.


“The poor,” Somsook Boonyabancha, founding director of the Asian Coalition for Housing Rights, puts it in the show's catalog, “are the creators and implementers of the most comprehensive and far-reaching systems for solving problems of poverty, housing and basic services.”


[![](media/rd-7a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-7a.jpg)


So in Diadema, a huge industrial city outside São Paulo, Brazil, 30 percent of the population used to live in favelas during the 1980s, when the homicide rate was shooting into the stratosphere. The government turned to residents for advice, asking them to help set priorities for the city budget, suggest upgrades for neighborhoods and approve construction projects, which employed workers who lived in the communities.


A land-tenure program awarded residents the right to stay on their property for 90 years, encouraging them to maintain their homes and invest in the neighborhoods. Residents helped to widen and pave streets, install clean-water and sanitation systems. Today, according to Ms. Smith, three percent of Diadema's residents live in favelas, and the annual homicide rate, a standard measure of civic order and public health, has dropped to 14.3 per 100,000 from its high of 140 during the 1990s.


[![](media/rd-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-6.jpg)


Ms. Smith also includes in the exhibition the by-now textbook case of Medellín, Colombia, once the world capital of drug cartels, murder and despair. Progressive political leaders there, starting roughly a decade ago, decided somewhat counterintuitively to invest most heavily in the worst slums, building a cable car system to link the city's center with the isolated, crime-ridden areas that blanketed the surrounding hills. New libraries and parks, public schools and pedestrian walkways were built around the pylons of the transport system so that the most beautiful and ambitious public architecture in the city went into the poorest neighborhoods. Medellín became a changed place.


In Dakar, Senegal, designers working with community organizers developed an irrigation system to recycle wastewater in the crowded slum of Yoff. And in La Vega, one of the settlements lining the steep slopes around Caracas, Venezuela, a team of architects, engineers working with a geologist, again taking cues from residents, devised a series of new stairs and plazas, so impassable climbs became manageable, neighborhoods were linked and nobody was forced to move out of their homes.


[![](media/rd-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-2.jpg)


[![](media/rd-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-5.jpg)


I was struck by a map in the show that located 238 schools in Kibera, that dense settlement in Nairobi, which occupies a territory smaller than Central Park. I recall discovering a similarly astonishing number of schools and universities on trips to Gaza. Ms. Smith points out the case of Pune, one of India's many booming cities, where laborers constantly move from one informal settlement to another, following construction projects and taking their families with them. As a consequence their children often aren't enrolled in school.


In response a team of designers decided a decade ago to bring the schools to them, via buses equipped with classrooms for 25 that pick the students up where they live. Students receive old-fashioned handbooks that keep track of academic progress and list phone numbers for education centers around Pune (a second bus network is under way in Mumbai) so that when the children move, their parents can find schools in the new neighborhoods and teachers can pick up where the students left off.


Simplicity itself.


One last example, from Bangladesh: Mohammed Rezwan, a local architect, has designed “community lifeboats” that serve as floating schools, libraries and health clinics. (*see photo above).*With sea levels rising, nearly 20 percent of the land there is predicted to be under water by 2050. The low-lying Ganges-Brahmaputra Delta, the most densely populated area in the world, will flood. Working with native boat builders Mr. Rezwan adapted the traditional flat-bottom bamboo riverboat to create his Noah's Arks. He outfitted the boats with waterproof roofs and solar panels, installed computers, high-speed Internet and portable solar lamps made from recycled kerosene lanterns. Traditional materials, local building techniques and renewable energy sources produced a model of contextual design.


I'm hoping to check out this fleet and also a few of the cities the show celebrates, to see how they're doing, firsthand. Meanwhile I gather there are now 54 of Mr. Rezwan's boats in operation in Bangladesh, serving 90,000 families.


That's design guru stuff too.


[![](media/rd-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/rd-8.jpg)


*The above article originally [appeared in the New York Times.](http://www.nytimes.com/2011/10/23/arts/design/for-some-of-the-worlds-poor-hope-comes-via-design.html?ref=design)*



#slum_improvement #slums
 ## Comments 
1. [SLUMS step by step // michael kimmelman « INTERFERENCIAS](http://coninterferencias.wordpress.com/2011/10/25/slums-step-by-step-michael-kimmelman/)
10.25.11 / 11am


[…] Woods recupera en su blog SLUMS: step by step, un artículo publicado en The New York Times, acerca de la capacidad del diseño y la arquitectura […]
3. [Link Roundup « Baltimore Slumlord Watch](http://slumlordwatch.wordpress.com/2011/10/27/link-roundup-30/)
10.27.11 / 9pm


[…] around the world — before and after.  Communities really can come together to improve the lives of the poor, in meaningful cost-effective ways.  They just don't do it in America — the richest […]
5. teresa
10.29.11 / 6am


If some of the best solutions can come from a so-called “bottoms-up” approach, why does design tend to be a top-to-bottom effort. Why is it that in some academic contexts/a.k.a my own, are we asked to solve problems from the removed perspective of the student within the academic bubble? I don't argue for a grounded, realistic and literal approach to design. I ask why our programs and problems aren't based on these realities. I don't argue that we should always consider the strict realities of budget, structure, material, etc. as student, but that we should consider the realities of context, of world, of present.  

Mr. Kimmelman's article is one of present (that includes a past and a future), an article that argues for the relevance of context, the relevance of the actual components: people, communities, cities…as a real way of solving problems. I only say we could have more of this in the academic setting. If not, at least I applaud the above mentioned efforts.  

I have lived my life in a third world country where, even if I cannot even try to define “architecture,” I have become aware that the idealistic exploration of architectural “skins,” and other very similar seemingly nonsensical trends, does not contribute towards an architecture of understanding, of solution, of human presence. If only there was more of this…
7. [A subject that I hate to love « sparkdbl](http://sparkdbl.wordpress.com/2011/11/23/a-subject-that-i-hate-to-love/)
11.23.11 / 5pm


[…] I came across this article in the NY Times, and re-posted at Lebbeus Wood's blog. Michael Kimmelman's article on the new show of design work in the world's slums is […]
