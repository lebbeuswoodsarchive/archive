
---
title: DA BOYZ
date: 2011-10-03 00:00:00
---

# DA BOYZ


[![](media/da-boyz.jpg)](https://lebbeuswoods.files.wordpress.com/2011/10/da-boyz.jpg)


*The above photo-montage comes via Daniel Dendra on Facebook. The montage-maker has not yet been identified.*


Today is World Architects Day. Or was it yesterday. Or tomorrow—if it exists at all. Thinking about it, I can hardly imagine any global social institution establishing such  a commemorative day, except as an excuse to round them all up and…make them live in buildings they designed.


Whichever.


There is a certain fascination with the montage—who's shown and, more interestingly, who isn't. Who is in the first row and who is in the back. Who is next to whom? Lots of room for narcissistic gratification and injury. Considering the price an architect has to pay to become prominent enough (successful?) to be included in such a roundup, is it better to be in the picture or left out? And, does any of this really matter?


Oh, the games people—yes, all of us—play….


The most glaring and disturbing perspective the montage presents is the overwhelming maleness of the group. Considering that for many years easily half of students in the schools where I've taught have been women, what accounts for their scarcity here? The bias of the montage-maker? The bias of the profession? The bias of the whole society?


For those who find such questions tiresome, there is always the game of how many faces can you identify. No risks there.


LW



 ## Comments 
1. Micah
10.4.11 / 1am


Looking through the faces from front to back, it was a little eerie coming across the repetition of Mies van der Rohe and Gaudi's faces in the back rows. Like a reoccurring dream or an unconscious inspiration that you notice surfacing in your work, just sitting there staring back at you. 


Also: what, no Frank Lloyd Wright?
3. Jared Karr
10.4.11 / 2am


Wrong file? All I see is a portrait of Mies.





	1. Tom
	10.4.11 / 9am
	
	
	I know their buildings not their faces 😦
5. [francisco](http://cenascoisasetal.blogspot.com)
10.4.11 / 12pm


like every list, it has it's flaws.  

and i know that what i'm going to ask is not the point at all but: how come an architects list doesn't include any portuguese? and i'm not even referring to a carrilho da graça nor the aires mateus brothers, but to “sacred monsters” like álvaro siza or souto de moura.  

I know that we, portuguese architects, are very found of our architecture and sometimes tend to be a bit selfcentered. But, really, i find a bit disturbing the lack of international projection we have. Even more disturbing is the fact that there's no work in portugal, nothing is being built, the whole country has stopped with this crisis. So, if we're not building here, and they don't know us out there… where are we building???


by the way, the blog is amazing
7. [nick](http://nickaxel.net)
10.4.11 / 1pm


why do you assume that the sexual disproportion is due to a ‘bias'? as if it is necessarily something that would not be-as-it-is-otherwise?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.5.11 / 8pm
	
	
	nick: I can't think of any other reason. Can you?
	
	
	
	
	
		1. guest
		10.13.11 / 10am
		
		
		Maybe women aren't as good architects?
		3. JoshuaV
		10.14.11 / 3pm
		
		
		Another thing to consider is the average age of these architects, I would presume they're primarily in their 50s-60s when they became huge, and then look at the percentage of women in architecture school 30-40 years ago. Sure, today we see a more even balance in schools, but was it balanced in the 60s? I'm too young to know the answer to that, but I will say that I see very few women in their 50s & 60s in the field at all, let alone famous ones. I'm not saying it's not a big boys club, but I have to believe there's more to it than that.
9. JD
10.4.11 / 7pm


Out of the front row I can only name Zumthor, Eisenman, and Koolhaas. Who are the others?
11. teresa
10.5.11 / 1am


I find it disheartening for female architects to hear that Mr. Woods, who has been in the field for years, especially as a professor, cannot explain the possible reason for this bias. Coming from a third-world country where men are still preferred in almost every field, I am surprised to learn that the situation is similar around the world, at least in architecture. Knowing very little about the field, I am curious as to where/how gender becomes a determining factor in architecture and construction.  

(This is not necessarily in reference to the above photo-montage but rather in relation to the larger context, such as the coverage of architecture in media, which in turn seems to strongly influence and reinforce the anonymity of female architects).





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.5.11 / 8pm
	
	
	teresa: I, too find it disheartening, but not hopeless. When I went to the University of Illinois School of Architecture in 1960, there were 800 students—only 1 was a woman. But I wonder what happens to the many women who graduate today. They show as much promise as the men. Based on their talent, originality, and mental quickness, many should be/become prominent.
13. [memo](http://archimemorandum.blogspot.com)
10.5.11 / 9pm


Who is the lady beside Mies?
15. [SHANGYLCHEN](http://shangylchen.wordpress.com)
10.7.11 / 11am


Interesting post and photo montage. I am curious about those people in the back. There are many good architects around the world and we just don't know about them and their works.
17. chenoehartc
10.8.11 / 8pm


It's hard enough to recognize all of the architects in the front rows. I don't think that more than 5 percent of the population will be able to identify even the big-name starchitects, and that figure seems high.
19. Caleb Crawford
10.10.11 / 7pm


FLW missing is a big one. But also, among contemporaries, where's Thom Mayne?  

Lebbeus, is that you in the middle left?  

I had some pretty strong women among my contemporaries in both undergrad and grad school, but none have really risen to the top (then again, its not exactly like my face is on this collage!). I think that possibly my female students may fare better.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.11.11 / 12pm
	
	
	Caleb: Thom is glowering darkly behind Wolf Prix. I'm on the upper left, alone as usual. And laughing (I think).
21. sidahmed9
10.12.11 / 5pm


Looking through the faces, I could only recognize a few. And yes, not alot of women there. I see the lady in the lower left corner is Zaha Hadid may be?  

I have to say Mr lebbeus woods, I am a very big fan of yours. Although I am just in my first year of architecture (and in a third world country) but your ideas are nothing like what I have studied before. They are amazing 🙂
23. RDGarcía
10.15.11 / 7am


funny how i got here from watching Alien 3 … i cared to look for the concept architect. and found it is Lebbeus Woods, searched, and got here.. Very interesting and amazing the work in Alien and i wonder if it surprises you some 2nd year student cares to watch alien in 2011, and no, my design professor didnt assigned to watch it, a movie that came out 1 year after i was born and in which i can appreciate the true art of concept architecture (made real in the movie) , not very common in todays diversity, distraction and vastness of information… thank you, and i will continue to read and analize your veru interesting questions and perspectives R





	1. RDGarcía
	10.15.11 / 9am
	
	
	Ok, i just read the ALIEN PAST entry and its a sad it didnt came as planned, alien3 did failed to make the “alien-religion connection” , alien past would have definitely given depth to the movie. Anyways its still enjoyable to see architecture involved, its just different concepts.. in my opinion a wooden structure/environment is too earthly and the metal/concrete environment of the prison is more of what a space dwelling could be made of. Awesome drawings and use of descriptive geometry, it would be absolutely amazing to follow the process of turning such an original and radical concept into the reality of the movie. i know nothing about this but i think the true art of concept architecture could be the process of making possible what at first is impossible.. its very interesting
25. Ben Dronsick
11.13.11 / 9pm


A well dressed bunch.
27. Lulu Jackson
12.29.11 / 8pm


With all the hucksters(Eisenman) that are thrown in with some of the greats(Mies, Corbu), he might as well have George Costanza in the mix.
