
---
title: YENDO’S EX-PROSTHESIS
date: 2012-01-20 00:00:00 
tags: 
    - architecture
    - design_with_waste
---

# YENDO'S EX-PROSTHESIS


In his latest projects, Masahiko Yendo is presenting us with some of the strangest designed objects I have ever seen. To confound their mystery (I cannot think of a better word) he describes them in terms that suggest these objects serve highly rational purposes. The objects, some of which appear to be dwelling places for human beings and others as instruments for human use, are fraught with decay and detritus-like character, raising many questions. Are they designs for things yet to be built? Or, are they readymades or found objects that are being adapted for re-use? Or, are they artifacts from the past that we are discovering in a museum collection of things belonging to a lost or forgotten civilization?


Of these and other interpretations I would imagine them as objects yet to be made in a coming world, one in which people have no choice but to work with whatever materials can be scavenged, simply because the manufacture of new materials, even for entirely new purposes, no longer sufficiently occurs.


I admit that this is rather trite and shop-worn scenario—the stuff of pulp science-fiction—but that doesn't make it less vivid or even less valid. Just from reading the daily newspapers and the spate of new books on various global crises, including the population explosion and a growing scarcity of natural resources, including food, anyone can imagine a coming world in which the overcrowding of cities and the careless squandering of natural and human resources forces the kind of unexpected recycling that Yendo's work depicts.


So what? we might say. There are plenty of Jeremiads and Cassandra-like predictions out there—who needs another?


In my view, Yendo's designed objects are much more than a warning. While they are not the solution to a problem whose causes lie far beyond the realm of architecture, they effectively strive to go around the problem, rather then engage it head-on, by addressing not its causes but its effects.


This is a classic problem-solving tactic. Albert Einstein employed it in his postulation of the Special Theory of Relativity. The big debate at the end of the 19th century was about the nature of the “aether,' an elemental substance that was believed to pervade all of space, enabling everything from the movement of elemental particles like light  to that of the planets. Physicists were at a loss to explain much about it, so they were also unable to effectively address other elemental problems. In developing his theory of “the electrodynamics of moving bodies,” Einstein simply ignored the aether as a hypothesis, constructing his hypothesis in terms independent of it. As a result of the verified success of his theory, the aether became irrelevant in all future discourse. Also—most importantly—entirely new vistas in physics, such as quantum theory, were opened for exploration.


A less exalted example is everyday algebra. If you have an unknown in an equation that you cannot solve for, you simply define it in terms of what you do know, and you will solve for the problematic unknown by indirection.


A downright vulgar example is from American football. If you have a great runner who keeps slamming into a wall of defensive players without any forward progress, you devise a play where he runs around the end of the defensive line, effectively avoiding the problem at the center.


Science and art and daily life are filled with such end runs, and that is what Yendo does with the projects shown here. Rather than throw up his hands in frustration over running up against intractable social and economic problems, he chooses to work with the given conditions and make of them the best he can in human terms. The structures he has designed under the general title of *EX-PROSTHESIS are* spatially cramped and aesthetically ugly by contemporary standards. Still, they emanate  a peculiarly affirmative quality related to their necessity and not their desirability. The inventive human spirit can and will, Yendo seems to tell us, prevail under even the most limiting and difficult of conditions.


LW


[![](media/masyendo-biomechrr1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-biomechrr1.jpg)


.


[![](media/masyendo-biomechrr2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-biomechrr2.jpg)


.


[![](media/masyendo-biomechrr3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-biomechrr3.jpg)


.


[![](media/masyendo-biomechrs1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-biomechrs1.jpg)


.


[![](media/masyendo-biomechrs2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-biomechrs2.jpg)


.


[![](media/masyendo-brainpod11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-brainpod11.jpg)


.


[![](media/masyendo-brainpod2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-brainpod2.jpg)


.


[![](media/masyendo-housegorillas1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-housegorillas1.jpg)


.


[![](media/masyendo-listener1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-listener1.jpg)


.


[![](media/masyendo-listener2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-listener2.jpg)


.


[![](media/masyendo-listener31.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-listener31.jpg)


.


[![](media/masyendo-swinefarm1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/masyendo-swinefarm1.jpg)


#architecture #design_with_waste
 ## Comments 
1. [natchard](http://natchard.wordpress.com)
1.21.12 / 6pm


Really nice to see some new Mas Yendo work. Looks darker, more sinister and even better than before. Looking forward to more.
3. dana
1.22.12 / 3am


omg i am in love





	1. dana
	1.22.12 / 3am
	
	
	still mad i missed the cooper deadline. sigh. not like i would get in anyway. great
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		1.22.12 / 7pm
		
		
		dana: why did you miss the deadline? If you really want to get in, you'll find a way to meet the deadline. Cooper doesn't admit many students, but the ones it does admit are very diverse. Don't be so sure you won't be admitted, if you actually apply.
		3. danaz
		2.3.12 / 6am
		
		
		Thanks Lebbeus. You are right.
5. [General](http://www.general.la)
1.26.12 / 9am


Leb: this reminds me of the SteamPunkesque aesthetic applied to so many dioramas in the growing movement of low-brow pop-surealist group shows that seem to be spurn from cracks in the concrete of downtown LA… And the dark dreams of Giger.. yet their bizzarely-architectural implication and ‘scale-model world' dimension invoke the miniatures of Lee Bontecou. …many correlations.  

Dana: in the meantime a few colleagues and myself are creating a thinktank on the westcoast (here in LA) for the ambitious yet somewhat mis-placed.. Its composed of select talents with the passion to think-further. I'd suggest pursuing your formal credentials most certainly, but while waiting for the next deadline you may find this “experimental organization” complimentary to your goals… something to keep us all sharp and on the edge… p.s. hope you're excelling at rhino 😉
7. danaz
2.3.12 / 6am


You know. I actually wrote that comment and haven't been back since. It's been hard but I'm glad I came back to this. Thanks, General. Coffee soon.
