
---
title: POMO PROMO
date: 2012-01-12 00:00:00 
tags: 
    - architecture
    - post-modernism
    - postmodernism
---

# POMO PROMO


[![](media/pomo-43.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/pomo-43.jpg)


“You must be joking!”


Postmodernism is dead and buried.


“But what have we got HERE?”


A Resurrection, or


a remake of *NIGHT OF THE LIVING DEAD….*


[![](media/pomo-71.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/pomo-71.jpg)


.


*[For more from beyond the grave:  http://www.domusweb.it/en/architecture/reconsidering-postmodernism/](http://www.domusweb.it/en/architecture/reconsidering-postmodernism/)*


**LW**


#architecture #post-modernism #postmodernism
 ## Comments 
1. paolo polledri
1.12.12 / 11pm


No, we shouldn't. Just speaking for myself: it was a bad, bad dream, or as Reyner Banham called it, “building in drag.”
3. [jimmy the bat](http://www.google.com)
1.13.12 / 12am


I think we are actually in Post-Architecture now. 


If any creator of the environment: developers, landlords, home owners, renderers, draftsmen, “designers”, builders, interior designers, decorators, urban planners, landscapers, sculptors, painters, etc…were legally allowed to call themselves “architects” and allowed due process for permitting of work there might be a come back…


Architecture (USA) as of today is the glorified act of pushing paper with rubber stamps to further support the role and value of lawyers in this society (US), with the burden of liability and insurance premiums and overhead that is impossible, and the knowledge of “design intent only” and not reality or real “architecture”…while making no money at all…dangling like a carrot in front of you is the oppurtunity to have authorship on design of architecture with that licensure…meanwhile consultants and owners authorize their designs with a rubber stamp, slave to the law(yer).


Only rich kids (grandpas) with hobbies get together and talk about building styles…must be in a recession or something.
5. [kristoferkelly](http://gravatar.com/kristoferkelly)
1.14.12 / 6pm


“Post” implies that another paradigm has emerged.


After-modern or after -architecture might be a more accurate assessment or the current state of things. That said, the undefined periods are full of possibility.
7. Victor Linus Engels
1.19.12 / 1pm


What is the name of building on the first picture? I have seen it before. But where?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.20.12 / 2pm
	
	
	Victor Linus Engels: The San Cataldo Cemetery building, by Aldo Rossi.
	
	
	
	
	
		1. Victor Linus Engels
		1.22.12 / 5pm
		
		
		Thanks you!
