
---
title: KISS ME, DEADLY
date: 2012-01-06 00:00:00 
tags: 
    - 1950s_America
    - film_noir
---

# KISS ME, DEADLY


[![](media/kmd-0.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/kmd-0.jpg)


.


# SPOILER ALERT: The following reveals the surprising ending to the 1955 movie *Kiss Me, Deadly.*


.


Ten years after atomic bombs were dropped by the United States on the Japanese cities of Hiroshima and Nagasaki, in August of 1945, the American public had little understanding about what made these bombs different from so-called normal bombs, other than the demonstrated fact that their destructive power was much greater. Americans' lack of understanding that the true difference was qualitative as much or more than quantitative, created an atmosphere of acceptance for nuclear weapons—Americans had no problem with bigger bombs that brought about more destruction so long as they had more of them than their enemies. If they had understood, though, that atomic—nuclear—bombs had aftereffects in some ways more devastating than their explosive power, the public might have been less enthusiastic about building ever more powerful weapons. If they had known, for example, that a powerful bomb—many times more devastating than the Hiroshima bomb—would not only obliterate a city in a stroke, but also poison the earth it had rested on, making it uninhabitable for perhaps twenty thousand years, then plans for rebuilding and restoring a society as it had been, or maybe even better, would have been seen for what they are: dangerous fantasies justifying using the bombs in the first place. This understanding, fortunately, is what eventually happened, thanks to the educating efforts of many individuals who took it upon themselves to wake people up to the real dangers of nuclear weapons. The U.S. government deserves no credit in this regard and, in fact, spent more energy playing down the dangers, so it could leave open its options for future use of nuclear weapons.


One example of this educating effort is the 1955 film, *Kiss Me, Deadly*, a Robert Aldrich production based on a novel by then-popular writer Mickey Spillane. With a good script, it's a well-directed and well-acted crime thriller (despite its lurid advertising), involving the hunt for “a great whatsit,” that costs many people their lives at the hands of a shadowy international conspiracy trying to get hold of it. This whatsit is small enough to be hidden easily and contains something very precious.


“Diamonds, rubies, perhaps?” asks the mastermind of the conspiracy, but only rhetorically, as he already knows what it is. “Even narcotics?” The whatsit turns out to be a cubic leather case that contains a metal box. Mike Hammer—the archetypal Spillane private detective—is the first to find the coveted box hidden at the bottom of a gym locker. Eventually it winds up in the hands of the mastermind and a woman he used to get the box away from Mike. “What's in it?” she asks. The foreign-accented conspirator, who has a penchant for quoting parables and adages, answers with the story of Pandora's Box that, when opened, “let loose all the evils in the world.”


“I don't care about the evil—what's in the box?”


“Did you ever hear of Lot's wife?” he replies obliquely, telling the story of the woman killed by her curiosity.


Unsatisfied with the Biblical reference, she persists. He then tells her that it contains “the head of the Medusa, and whoever looks upon it will be turned not into a pillar of salt, but of brimstone and ashes.”


Out of patience, she just shoots him, taking the box for herself. With his dying breath, he begs her—“like Cerberus barking with all his heads at the gates of Hell”—not to open the box.


But she does.


The intense light streaming from the box emits a sound more sinister and terrifying than any I have ever heard in cinema. It ranks as one of the artform's greatest special effects, lending the iridescent substance in the box a quality at once utterly alien and familiar, like hellish, primeval screams long suppressed within ourselves. By now, as the woman is incinerated by what is clearly some form of radiation, we feel deeply the meaning of the ancient parables of annihilation and are confronted by the dreadful reality of the manmade radioactive material we know is at the core of every nuclear weapon: it doesn't need to explode to kill us, or to destroy the world.


LW


.


*(below) “I don't care about the evil….”*


[![](media/kmd-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/kmd-2.jpg)


[![](media/kmd-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/kmd-4.jpg)


[![](media/kmd-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/kmd-1.jpg)


*The coveted box contains“the head of the Medusa, and whoever looks upon it will be turned not into a pillar of salt, but of brimstone and ashes.”*


[![](media/kmd-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/kmd-3.jpg)


*Mike Hammer and his secretary, Velda Whitman, looking “at the end of the world.”*


.


For an interesting discussion of the history of the film's ending that is highly relevant to this post, follow this link: <http://www.dvdtalk.com/dvdsavant/s2356kiss.html>


#1950s_America #film_noir
 ## Comments 
1. [metamechanics](http://metamechanics.wordpress.com)
1.8.12 / 1am


not to side track you from your point, but this reminds of the good ol' days in a small apartment in washington heights (UUWS, ha) on bad movie night (not enough money to go out, cheap bodega beer) and often Ed Wood movies.  

the intellectual content is impressive, but the delivery?
