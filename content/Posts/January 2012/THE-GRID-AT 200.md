
---
title: THE GRID AT 200
date: 2012-01-10 00:00:00 
tags: 
    - Manhattan_grid
    - urban_model
---

# THE GRID AT 200


*The new New York Times architecture critic gives a good account of what makes the Manhattan street grid different from that of other cities and, in some important ways, better.*


[![](media/nyc-grid-1811-copy1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/nyc-grid-1811-copy1.jpg)


 


**THE GRID AT 200: LINES THAT SHAPED MANHATTAN**


**By** **Michael Kimmelman**


In the old photograph, a lonely farmhouse sits on a rocky hill, shaded by tall trees. The scene looks like rural Maine. On the modern street, apartment buildings tower above trucks and cars passing a busy corner where an AMC Loews multiplex faces an overpriced hamburger joint and a Coach store.


They are both the same spot. Not so long ago, all things considered, the intersection of Broadway and 84th Street didn't exist; the area was farmland. [“The Greatest Grid: The Master Plan of Manhattan, 1811-2011,”](http://www.mcny.org/exhibitions/current/The-Greatest-Grid.html) now at the Museum of the City of New York, unearths that 1879 picture of the Brennan Farm among other historic gems. The show celebrates the anniversary of what remains not just a landmark in urban history but in many ways the defining feature of the city.


After all, before it could rise into the sky, Manhattan had to create the streets, avenues and blocks that support the skyscrapers. The grid was big government in action, a commercially minded boon to private development and, almost despite itself, a creative template. With 21st-century problems — environmental, technological, economic and social — now demanding aggressive and socially responsible leadership, the exhibition is a kind of object lesson.


Simeon De Witt, Gouverneur Morris and John Rutherfurd were entrusted with planning the city back in 1811. New York huddled mostly south of Canal Street, but it was booming, its population having tripled to 96,373 since 1790 thanks to the growing port. Civic boosters predicted that 400,000 people would live in the city by 1860. They turned out to be half-right. New York topped 800,000 before the Civil War.


The planners proposed a grid for this future city stretching northward from roughly Houston Street to 155th Street in the faraway heights of Harlem. It was in many respects a heartless plan. There were virtually no parks or plazas. The presumption was that people would gravitate east and west along the numbered streets to the rivers when they wanted open space and fresh air, and not spend lots of time moving north or south. That partly explains why there were only a dozen avenues.


In the abstract, the idea was really nothing revolutionary; grid plans went back to ancient Greece and Rome. But installing one in Manhattan was deeply subversive because, while still undeveloped, the island was already parceled into irregularly shaped, privately owned properties.


This meant the appropriation of land and reconstruction. First, Manhattan had to be surveyed, a task that took years. Property lines had to be redrawn, government mobilized for decades on end to enforce, open, grade and pave streets. Some 60 years passed before the grid arrived at 155th Street. Streets were still “rough and ragged” tracks for a long while, as one diarist observed in 1867, describing a recently opened stretch around 40th Street and Madison Avenue as a mess of “mud holes, goats, pigs and geese.”


Even so, the grid gave the island a kind of monumentality and order.


Was it monotonous? Yes. Frederick Law Olmsted was among those who thought so. Other city plans are certainly more sophisticated (Paris) or elegant (Barcelona) or stately [(Savannah, Ga.)](http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-2547).


But New York's grid had its virtues. For one thing, it proved flexible enough to adapt when the city's orientation did shift north-south, flexible enough to accommodate Olmsted's Central Park, the genius of which lies in the contrast between its own irregularity and the regularity of the grid.


For another thing, the grid turned out to be, far beyond what anyone could have envisioned in 1811, a windfall for those same landowners who first opposed it, but then whose newly rejiggered lots on subdivided blocks came to be worth fortunes. New York property values boomed thanks to the grid, which effectively created the real estate market. In 1965 John Reps, an urban historian at Cornell, wrote in “The Making of Urban America” that the city commissioners “were motivated mainly by narrow considerations of economic gain.” Professor Reps thought they put money before aesthetics.


They did, but that view now seems a little uncharitable. Money and aesthetics aren't antithetical, and the grid has proved itself oddly beautiful.


I'm referring not just to the sociability it promotes, which Jane Jacobs identified, or to the density it allows, which Rem Koolhaas celebrates, or even to the ecological efficiency it sustains, which now makes New York, on a per-capita basis, a very green place. I'm also referring to a kind of awareness it encourages.


It's true that Manhattan lacks the elegant squares, axial boulevards and civic monuments around which other cities designed their public spaces. But it has evolved a public realm of streets and sidewalks that creates urban theater on the grandest level. No two blocks are ever precisely the same because the grid indulges variety, building to building, street to street.


Painters from Poussin to Seurat, Picasso to Mondrian, Pollock to Chuck Close have exploited the special power of grids to create order yet also highlight small differences. Manhattan's grid is not perfectly regular. Some blocks are longer than others. Some avenues are wider. Broadway cuts diagonally across six north-south streets, and those cuts have made room for public spaces (Union Square, Madison Square, Herald Square, Times Square, Columbus Circle, Verdi Square).


We feel all these shifts in the grid, alert to changes thanks to the expectation of sameness.


The grid also makes a complex place instantly navigable. This isn't a trivial benefit. Cities like Berlin and London, historic agglomerations of villages, include vast nowhere stretches, and they sprawl in ways that discourage easy comprehension and walking. An epicenter of diversity, Manhattan by contrast invites long walks, because walkers can judge distances easily and always know where they are. The grid binds the island just as New Yorkers are bound by a shared identity.


That is, the grid gives physical form to a certain democratic, melting-pot idea — not a new concept, and probably not exactly what the planners had in mind, but worth restating. In the same way that tourists who come to Manhattan can easily grasp the layout and, as such, feel they immediately possess the city, outsiders who move here become New Yorkers simply by saying so. By contrast, an American can live for half a century in Rome or Hamburg or Copenhagen or Tokyo but never become Italian or German or Danish or Japanese. Anybody can become a New Yorker. The city, like its grid, exists to be adopted and made one's own.


Hilary Ballon, a professor of urban studies and architecture at New York University who organized the exhibition (running through April 15), adds that it even affects our daily behavior. “We cross at corners with the grid,” is her example. That's not quite the New York I know, but it's true that when we jaywalk or take shortcuts across plazas or stroll down Broadway, we are aware of violating the grid. The grid is the ego to our id.


And for the same reason we're conscious of the street wall, the regular line of building facades, so that when one building is set back and breaks that line, our equilibrium is disturbed. (See the erratic sidewalk of 57th Street.) Like the neighborhoods it circumscribes, the grid has its integrity.


“It would have been easier not to destabilize the landownership on the island,” Ms. Ballon concludes in her introduction to the show's catalog. “The commitment to prepare New York for the future sets an example for our times: do we have the capacity to address [climate change](http://topics.nytimes.com/top/news/science/topics/globalwarming/index.html?inline=nyt-classifier), build 21st-century infrastructure and promote sustainable growth?”


That's the question. Put another way, the grid was a leap for government and private enterprise united by faith in urban development. It was also proof of how adaptable citizens, and cities, can be. Generations of humane and progressive New Yorkers lifted it from the drawing table to greatness. An equitable and just city today depends on a vigilant populace keeping tabs on our planners and politicians.


More and more people want to live in cities now. New York remains a model.


Can it live up to the grid?


**Michael Kimmelman**


*This article was [originally published](http://www.nytimes.com/2012/01/03/arts/design/manhattan-street-grid-at-museum-of-city-of-new-york.html?_r=1&ref=michaelkimmelman) in the New York Times.*


#Manhattan_grid #urban_model
 ## Comments 
1. Dan Coulombe
1.11.12 / 11pm


What I like about the Manhattan Grid are the spaces created where it clashes with the older irregular street layouts; particularly the Astor Place/Cooper Union area. Several awkward triangular blocks and open spaces are created in this region, especially along Greenwich Ave.


Of course the interaction between Broadway (which I believe was an old Indian trail… a path of least resistance though the wilderness) and the indifferent objective grid has created some of most iconic spaces and buildings in the world. Times Square accidentally occurs when Broadway hits the grid at such an acute angle that it briefly opens it up, creating the odd bow tie space. I think something needs to be said about the aesthetic possibilities caused by the interaction between this spatial manifestation of the rational/empirical duality.
3. [vanderleun](http://americandigest.org)
1.12.12 / 4pm


Well, I'd like to read this but I have to say that the color scheme (medium grey on white) for the type makes it essentially too difficult to decode when the commentary is at any length. The links pop right out but everything else in the main body of the text just fades into the background. Too bad since the grid is something I have a passing interest in.
