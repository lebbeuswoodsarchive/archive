
---
title: LES VON LOSBERG  THIS HOUSE
date: 2012-01-09 00:00:00 
tags: 
    - architecture
    - house
    - poems
    - poetry
---

# LES VON LOSBERG: THIS HOUSE


Von Losberg's perspective is down-to-earth yet metaphysical. The following poems are from a forthcoming publication of his works.


**LW**


.


***from THIS HOUSE: REAL ESTATE***


*.*


the house clings to its


architecture the way


a drowning man clings to a


distant memory—beyond


all reason or the limits


of its tensile strength,


beyond the rudiments


of its machinery; the logic


of its fiction, nails


and screws, the principles


of balance and the figures


of its personal geometry


all serve no conscious


end, no matter what the


wind cries, what the


weather wreaks.


.


.**********


.


i am not a house.  these are not


my windows, not my doors, not my


ceilings, walls or floors.  nothing


that grows upon me grows upon me.


weather does not ride my spine,


does not cascade into my gutters.


no, not the walk, the drive strip,


terrace, port or shutters is


me or anything that i own


nor is the redgold sky-sheen that


reflects, nor are the settling creakings


or its wringing groans; the birds


do not nest in my nest; the squirrels


do not scrabble through my home.


nothing of it carries forward


into the abyss that transpires;


some thing else inspires, some thing


else, though hiding.


.


***********


.


**Foundation**


.


the foundation,


rigid figure


dug out of the ground


of its own desire,


guards an empty space,


though set upon defensible


bedrock.


all illusion


has its place, this one


under the house,


with earth and stone


and all the deeper,


all the real immutables.


even now there is a liquid warfare


going on, guerilla seepage


into the basement through concrete,


insidious as ideas wearing away the rock


of reason or the walls of disbelief.


the basement ceiling sweats and sags;


the floor puddles, unable to rest.


finally, a wound appears, a crack,


its lips turning to chalk, crumbling


wetly, decaying like flesh, water


starting like slow thin blood:


welling from stone, the patient


desire of nature will not end.


.


**********


.


Snow settles in about the eaves;


the house protests the white, the chill,


the onslaught of this season with so much undone,


so many windows still shut tight, so many rooms left


dark, so many flights unchecked.


the shutters slap at the wind.


shingles shudder though still nailed in place.


the house remembers the smell of fresh earth newly turned,


the subtle dust of new concrete, the pitch of raw pine,


paint, paste, plaster, sun and light.


it can't remember the day the air turned hard,


the day the wind grew teeth, the day the putty


and the paint began to line, to crack, to peel.


winter rings like a bell in its ears,


pealing away illusion.  there is nothing left to feel


but darkening of the sky and shortening of day.


somewhere in the house a door slams for the last time,


solid, ineluctable as night.


.


.***********


.


in the middle of the night


the house would ponder the


ineffable—if it could


for the gurgling in its boiler


hungry for coal, for the rattle


in its windows telling a tale


all too old the way loose


teeth tell it.


sideways slipping


the roofbeams sag; the gutters warp;


the weather wrings the house to its


foundation like a worn-out rag.


cars, people, clouds scud;


seasons whistle by.


all sense—


the pregnancy of meaning in


the luminescence of each twilight—


slips past, drifting on the wind—


skywriting—wispy glyphs


whipped steadily west.


and


memory, that could inform, lies


tucked into a  bed riddled with


dreams.


in the dawn, in the


twilight, stories go untold;


arguments linger, diaphanous


and drift into darkness; idea


migrates, avian in extreme,


beyond meaning, beyond


architecture, ignorant of design.


.


***********


.


**no house**


.


the house you see


is not the house


the house thinks it


projects; no, nor


the house dissected


when the house,


mired in leisure,


impassioned in despair,


wields the lancet of


language, deft and dreaded.


no, but some thing else


emerges that resembles


neither dream nor notion—


what it is—how to say


it—uncontrolled, unwitting


miss the mark.  the windows


do not see it, nor the doors


open onto what transpires.


the wainscoting, wallpaper,


semi-gloss trim conspire


skin-deep impression only.  at


the edges all is tidy, tacked


down, spruce, but at the


center is a hovel of straw,


a shack of sticks, and a


wind, like the breath


of a wolf, always howling.


.


**Les Von Losberg**


#architecture #house #poems #poetry
 ## Comments 
1. Gio
1.22.12 / 4pm


Really beautiful poems!  

I find the first one “Real Estate” precise in the representation of Architecture.


I once had a near death experience back in Brooklyn, while going to school.  

I can recall how images of my life past through me instantly while at the same time having the feeling of a near end.  

Images that define me…just memories combined with a feeling of sadness for the people who where going to miss me.  

What makes something truly an experience of” being”… are the memories and these appear to truly represent who we are and who have been. 


In the poem: ‘the house clings to its architecture the way a drowning man clings to a distant memory”  

That which enunciates it is always hidden.  

The tools that we use to create cannot represent the meaning of architecture….it is a if the more you do the less it becomes…but it is the only way to manifest it.
