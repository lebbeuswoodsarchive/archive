
---
title: QUESTIONING CATASTROPHE
date: 2012-01-23 00:00:00 
tags: 
    - earthquake
    - Honshu
    - Japan
    - tsunami
---

# QUESTIONING CATASTROPHE


[![](media/hq-14.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-14.jpg)


Enough time has passed for us to not feel like voyeurs of disaster, or guilty exploiters of human tragedy. Rather, we can look at these images with a more objective eye, seeing them as types rather than only as documents of a shocking human catastrophe. As types, they give us enough emotional distance to learn something that may stay with us and become useful in the future, rather than be overwhelmed, immobilized by emotion. Ultimately, tears only cloud our vision.


What exactly can we learn from these images of the Honshu quake and the tsunami that devastated the coast of Japan on [March 11, 2011](http://cires.colorado.edu/~bilham/Honshu2011/Honshu2011.html)?


“Many things” seems too easy an answer—yet it is true. Every branch of human knowledge is affected by what these images portray. As our concern here is the field of architecture, the first thing we need to acknowledge is that it is a field embracing and depending upon many other fields, from sociology to engineering, from law to art—the list is long and tangled. At the same time, architecture is not merely a summation of these many parts, but a synthesis of them, transcending any one to create a holistic idea of human existence.


The predominant thing we architects may learn from viewing and.thinking about these images is that we are helpless to prevent significant damage to buildings and towns caused by even relatively moderate natural upheavals. The 9.0 earthquake in the ocean floor some sixty miles off-coast and resulting tsunami that inundated the east coast of Honshu Island were severe by everyday standards, but nowhere near the intensity of the planet-shaping releases of energy that occasionally convulse the Earth's surface, extinguishing living species or changing global climates. Human beings and their communities are fragile because they are sustainable only within a narrow range of conditions and possibilities. It is the main task of architecture to maintain this range or to create it where it has not existed before. To some extent it is also architecture's responsibility to expand this range when people require it not only for survival but also to flourish within the demands of change brought on by catastrophic events such as earthquake and tsunami.


Here are some questions to consider, when looking at these photographs, if we working in the field of architecture want to learn anything from the Honshu disaster:


Much has been destroyed; has anything new been created?


If so, is it evident in the photographs; or is it something the photographs might help us to express?


To what extent is destruction necessary for creation?


Are creation and destruction two separate, opposing things, or are they the same thing, expressed in different ways?


Do the photographs convey beauty of any kind? Or, are they just ugly?


If conveying beauty, what does this tell us about the qualities that constitute beauty?


If conveying the ugly, what does this tell us about the qualities that constitute the ugly?


Do the photographs portray “Nature against the Human?”


If so, is Nature an enemy, to be defeated, conquered, and controlled by Humans?


If not, is Nature's destruction of what Humans have made serve any conceivable Natural or Human purpose?


What are Nature's purposes, as embodied in earthquakes and tsunami?


Can architects somehow design *for* earthquakes and tsunami, or only *against* them?


I invite the readers of this blog to ask questions relevant to architecture and design—in the Comments section below—that these photographs might suggest….


LW


.


[![](media/hq-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-2.jpg)


.


[![](media/hq-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-4.jpg)


.


[![](media/hq-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-6.jpg)


.


[![](media/hq-9.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-9.jpg)


.


[![](media/hq-13.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-13.jpg)


.


[![](media/hq-16.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-16.jpg)


.


[![](media/hq-17.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-17.jpg)


.


[![](media/hq-18.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-18.jpg)


.


[![](media/hq-20.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-20.jpg)


.


[![](media/hq-20b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-20b.jpg)


.


[![](media/hq-22.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-22.jpg)


.


[![](media/hq-24.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-24.jpg)


.


[![](media/hq-25.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-25.jpg)


.


[![](media/hq-26.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-26.jpg)


.


[![](media/hq-27.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-27.jpg)


.


[![](media/hq-28.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-28.jpg)


.


[![](media/hq-31.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-31.jpg)


.


[![](media/hq-32.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-32.jpg)


.


[![](media/hq-34.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-34.jpg)


.


[![](media/hq-33.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/hq-33.jpg)


#earthquake #Honshu #Japan #tsunami
 ## Comments 
1. [slothglut](http://karbonjournal.org)
1.24.12 / 12am


“The saline crystal and its red-bluishness,  

The milky sap and its sweetness,  

Various flowers and their fruits,  

The sun and the moon and their luminosity:  

These are neither separable nor inseparable.”  

—D.T. Suzuki


Might help to find the path of answering questions posed above. Many thanks as always!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.24.12 / 12am
	
	
	slothglut: if only we could live everyday with the wisdom, and resolve, of the poet. But we, and our institutions, fall into the trap of dualism and oppositions, setting the stage for eternal conflict, which provides for us only in the near term. We architects must think in the longer term, even in our engagement with the most immediate and urgent problems. That is why the questions I have posed are in the first place philosophical.
3. [straykatstrut](http://straykatstrut.wordpress.com)
1.24.12 / 1am


Reblogged this on [straykatstrut](http://straykatstrut.wordpress.com/2012/01/24/79/).
5. [Danlrene ©2011](http://danlrene.wordpress.com)
1.24.12 / 4am


or has our interference with nature created the tsunami's and earthquakes and other natural disasters? great blog
7. David Long
1.24.12 / 12pm


What would the preconditions be for habitation in order for a world like this to be lived in and created in this way?  

Does this happen already?
9. David Long
1.24.12 / 12pm


Correction: What would the preconditions be for a world like this to be lived in and created in this way?  

Does this not happen already?
11. [Questioning Catastrophe by Lebbeus Woods « CLOUD Disassembly RPI](http://rpi-clouddisassembly.transvercity.net/2012/01/24/questioning-catastrophe-by-lebbeus-woods/)
1.25.12 / 3am


[…] <https://lebbeuswoods.wordpress.com/2012/01/23/questioning-catastrophe/> […]
13. [Oliver](http://baufunk.blogspot.com)
1.26.12 / 7pm


Apologies for the shameless self-promotion plug, but I used these images and events for a project I finished a week ago. I'm a student of architecture who does these kind of speculative proposals outside of school, just for fun and for learning.


The project deals with the damaged Fukushima Daiichi nuclear power plant, combining a floating soil cleaning facility with a cemetery for the missing victims of the earthquake, built from scrap material left behind by the tsunami.


<http://users.student.lth.se/ar06op1/Downloads/Namazu/>


Would be nice to receive a comment.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.1.12 / 3pm
	
	
	Oliver: First of all, I applaud your attempt to employ design and architecture to address this very complex situation. Most architects are too timid or too afraid of criticism to even try. I haven't been able to study your proposals in detail and so cannot offer a specific critique. A general comment based on my overview is that your proposals deploy a conventional language of form and construction already ‘invalidated' by the forces of earthquake and tsunami. It seems to me that even pragmatism calls for the invention of a new typology of form—thinking of the “next time.”
15. [Deconstructing Reality | Gordon Matta-Clark « dpr-barcelona](http://dprbcn.wordpress.com/2012/01/26/gordon-matta-clark/)
1.26.12 / 10pm


[…] in the human capacity of transform reality into hyperrealism. In the same way that Lebbeus Woods wonders “To what extent is destruction necessary for creation?”, when looking at Matta-Clark […]
17. [Stratis Mort](http://arcsickocean.blogspot.com/)
2.2.12 / 1am


Architects must question themselves whether nature is something we can resist through engineering and technology or something we can approve and accept through architecture. To me, architecture until today seems ‘static'. The form sometimes ‘follows' nature in the sense of the design. But probably we must forget about ‘curvy' surfaces and parametric shapes. We must clearly think of how space can be adjusted to ‘receive' disaster, to be ready to change, to be ready to evolve through nature.  

With the most simple example, i would say people ‘invented' curtains. When too much light comes in our room, we can choose how much we let in. It's interactive. It alters through the acceptance of nature's vital and constant changes. If, through architecture and other mediums, we want to achieve more and more, we must first accept the fundamental rule of nature, as Heraclitos of Anc. Greece states: ‘everything flows'.  

On this, if it proves to be a potential architectural principal, there is the ‘ground' for theory, observation and understanding, not quick concretes and more glasses.  

To the very significant and interesting question on beauty and ugliness, in my perception, i clearly define beauty as a ‘temporary enthusiasm' and ugliness as ‘timeless interesting value' . And on architecture i see that everyday. Each generation in each period, ‘stigmatizes' the beautiful. In that way Parthenon's columns and Pantheon's domes are repeated again and again, almost inherited through time. That though specifies a temporary significance, which is then transformed to a banal cultural replica. I study in London and what i observe here is that people identify this city as ‘ugly', sometimes ‘terrible'. But this ugliness is the interesting, architectural aspect; the cultural wars in Brick Lane and the crime scene history of Marble Arch.  

Architecture must step back (particularly 4d-‘built' architecture) and recognize what we perceive as interesting facts in space. There is something else that performs in space more intense than walls and windows.  

Is it the planning of the street or how people protest in them?  

Is it the hotel on the beach or the formation of rocks because of the dynamic waves?  

Is it the luxurious living room or the live fireplace?  

Or is it the earthquake itself?
19. akaak
2.3.12 / 6pm


What is the true scale that humans exist at? We are rooted (understandably) in a scale familiar to the sizes of our bodies, but when looking at results of scales and accumulations this large, it's hard to comprehend how we truly fit. The design of the macro – and the micro – remain elusive, although our science constantly attempts to devoid them of all their secrets. This “middle” scale is of course the one we comfortable occupy, but the forces of macro and micro ultimately direct the fate of our existence.
21. [durganand balsavar](http://www.artesindia.com)
2.10.12 / 5pm


great blog ! In some ancient indegenous communities, existed a certain awareness of the nature of catastrophe which intuitively influenced design – bamboo dwellings on stilts in asia pacific, stone wall dwellings with wooden lintels and ties in earthquake zones of north india, circular bhoonga huts in Kutch that resisted the earthquake. In other instances communities have also recognised the elements of nature that protect dwellings from nature's own fury – sand dunes on the coast that protected settlements during tsunami in South india. With time over centuries, at times in the long absence of a natural calamity – the community and its architects / builders develop an amnesia to the memories of disaster – building callously driven by forms – divorced from the contextual forces, historic experiences and indegenous learnings transmuted over centuries from one generation to another. The architecture of the coconut tree by the coast provides clues to the design of the coastal settlement – that can sway and offer least resistance – with its ephemeral temporality. BUT that rarely happens and the architect, transposes the architecture of the OAK tree (a fetish fro permanence in a fragile ecosystem) – that escalates the degree of disaster. An earthquake, tsunami, or natural calamity for some reason, gain their significance only in the context of destruction of the human artefact – a devastating desert storm in a desolate uninhabitated zone is often of little relevance to us.
23. [Antoine](http://www.presquilerecords.com)
2.26.12 / 8am


Now rebuilding: <http://www.theatlantic.com/infocus/2012/02/japan-earthquake-before-and-after/100251/>
25. Moi
3.13.12 / 9pm


How much destruction does creation create? If we were ants, there would be catastrophes every single day, yet we would carry on.
27. [Questioning Catastrophe by Lebbeus Woods « CLOUD Reassembly RPI](http://rpi-cloudreassembly.transvercity.net/2012/01/24/questioning-catastrophe-by-lebbeus-woods/)
9.9.12 / 6pm


[…] <https://lebbeuswoods.wordpress.com/2012/01/23/questioning-catastrophe/> […]
