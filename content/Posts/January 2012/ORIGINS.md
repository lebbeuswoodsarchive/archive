
---
title: ORIGINS
date: 2012-01-02 00:00:00 
tags: 
    - AEDC
    - architecture
    - Colonel_Lebbeus_B__Woods
    - personal_history
---

# ORIGINS


[![](media/lbw-1b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-1b.jpg)


*(above) Site of the Arnold Engineering Development Center (AEDC) near Tullahoma, Tennessee, c. 1950, showing the Elk River Dam and Woods Reservoir, named in memory of Colonel Lebbeus B. Woods—my father—officer in command of its construction (portrait photo inset on the right).*


.


I am the son of a distinguished Air Force officer and bear his name. He died in 1953, at the age of fifty-two, from a rare form of blood cancer caused by his involvement with the development and testing of the atomic bomb, though his service record blandly states his cause of death simply as “a result of service.”


He was born to parents who were school teachers in the wilds of South Dakota, on a large Sioux reservation. After joining the U.S. Army in 1918, he took the highly competitive exams to enter [West Point](http://en.wikipedia.org/wiki/United_States_Military_Academy) and with only his equivalent of an eighth-grade education, got in. Graduating as an officer in 1925, he resigned his commission to become a civil engineer for the Pennsylvania Railroad, designing and directing the construction of railway bridges until the outbreak of the Second World War, in 1942.


He continued his engineering work in the Army, building airfields, bridges, and other necessary facilities in England and Europe as the Allies steadily advanced against the Axis forces. In 1944 he was assigned to the [Manhattan Project](http://en.wikipedia.org/wiki/Manhattan_Project) in Los Alamos, New Mexico, to construct the buildings and other structures needed by Robert Oppenheimer, Edward Teller, and other scientists there, to develop the atomic bomb. His first exposure to radiation was the bomb's test at the Trinity site; his second, in 1948, was at the Bikini Atoll in the Pacific. Five years later, he died a painful death—as had hundreds of thousands of others, both Americans and Japanese.


In that brief period, he accomplished a final mission: the building of the Arnold Engineering Development Center [(AEDC)](http://en.wikipedia.org/wiki/Arnold_Engineering_Development_Center), a sprawling experimental facility in southern Tennessee, that, since its completion in 1953, has been vital to the invention and development of air- and spacecraft and their jet or rocket propulsion systems not only for the military but also for NASA projects such as the Space Shuttle. A large manmade  lake, made to supply enormous quantities of water needed by the Center, but also famous for its ecologically protected woodlands and wildlife, is named in his memory.


I was born into and spent my early years during a time of war, first a hot one and then ‘cold,' but my childhood experiences unfolded—oddly enough—in a milieu of creative engineering, problem-solving, and construction. Patriotism was a given so there was no ideological contention at home. My parents were both FDR Democrats—he was also as good as given—so there was little political debate, either. What was important was getting the job done for good reasons. Up to age thirteen, I spent my free time reading, drawing, hanging out around jet aircraft and their pilots. I and a few other officer's chldren used to sneak down to the flight-line, watching jet fighters and bombers take off and land, then have cokes in the flight-line café where their pilots went for coffee, still dressed in their flight gear. Once, at the air-base in Ohio that was my father's last command,  I climbed some fences and wandered for hours in a vast field of derelict fighters and bombers, exploring them inside and out, until MPs showed up and took me into custody. It seems that some of these planes had been used in the Bikini atomic tests, and it took hours of medical exams, interrogation, and my father's high rank, before I was returned home to the officers' quarter of the base.


I also grew up around a certain kind of architecture, though I'm certain I didn't call it that or have much of an idea of what the term meant. I suppose that I would now call it functional architecture, because its uses dominated the genesis of its form; still, it had a self-conscious aesthetic and a design intent that included its visual qualities and the emotions they evoked. The building forms and their meanings are in a way romantic and fantastical—wind tunnels, transonic circuits, gas dynamics laboratories—-a kind of architecture for opening up and exploring new worlds. Even the more conventional industrial forms of steam plants and other support buildings reflect traces of the glow of a great adventure, experienced in this context of immensely consequential invention. All of it stayed with me, though was not to emerge—radically transformed—until many years later.


LW


[![](media/lbw-24a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-24a.jpg)


*General Carroll, Colonel Woods (second from the right) and their group inspecting the landscape of the AEDC.*


.


[![](media/lbw-30a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-30a.jpg)


*Colonel Woods with building contractors on one of the construction sites. A telling bit of trivia: In those days, a military officer's everyday uniform was clean, well pressed, and plain, and not so different from the everyday business clothes of civilians. Today, the everyday uniforms of officers anywhere are crumpled camouflaged  combat fatigues, worn as though they were ready to go into combat at any moment.*


.


[![](media/lbw-38b1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-38b1.jpg)


*The Transonic Circuit—a wind tunnel for testing air- and spacecraft in dense atmospheres at extremely high speeds.*


[![](media/lbw-34a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-34a.jpg)


[![](media/lbw-32a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-32a1.jpg)


.


[![](media/lbw-36a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-36a.jpg)


*The Gas Dynamics Facility is most heavily involved with developing jet and rocket propulsion systems.*


.


[![](media/lbw-37a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-37a.jpg)


.


[![](media/lbw-411.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-411.jpg)


*A newly developed jet engine being tested.*


.


[![](media/lbw-40-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-40-1.jpg)


*One of several shops devoted to making scale models of air- and spacecraft prototypes and components for testing.*


[![](media/lbw-38a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/lbw-38a.jpg)


.


Related posts:


<https://lebbeuswoods.wordpress.com/2008/11/13/big-bang/>


<https://lebbeuswoods.wordpress.com/2010/09/14/terrible-beauty-3-i-am-become-death/>


#AEDC #architecture #Colonel_Lebbeus_B__Woods #personal_history
 ## Comments 
1. wildoo
1.2.12 / 11pm


There is perhaps a poetic justice, as no doubt you have pondered, in-so-far as while your father's life was taken prematurely through his exposure to environmental conditions prevalent in his theatre of operations, that a reservoir, which is symbolic of the essence of life, bears his (your family) name. He was obviously a great role model. To someone having minimal familiarity with your work, these images go a long way to reveal what you draw ‘from' when you bring to life the many technically integrated, organically conceived, hybridized and tactile “architectural suggestions” that might populate the world to come. Thanks for sharing a bit of your dad's exemplary life and influence.
3. [Lebbeus Woods' Dad | CribcandyThe Best from Household and Interior Design Blogs Around the World, Every Day](http://cribcandy.com/home/picks/lebbeus-woods-dad/80398/)
1.3.12 / 10am


[…] 3rd, 2012 link to (permalink) A colonel who died as a result of developing the atomic bomb. Visit site » lebbeuswoods.wordpress.com comment […]
5. Pedro Esteban
1.3.12 / 3pm


I was as you, really close to the aesthetics of the extreme function( this post touchs me a lot) and I have never seen a most complete architecture, in fact to me architecture is not far from that “way of production”(a needed approach to architecture in the contemporary practice) because is incredible how engineers develop such a high developed aesthetic without want it, is “beauty” just from function, is to me a real pure “beauty”  

Is rare see in architects the strength of this spaces and forms; too much analysis of beauty makes beauty-without quotations.  

“Beautiful” story.
7. [m jesus huarte](http://www.territorio-de-arquitectura.blogspot.com)
1.4.12 / 1am


A beautiful story!


I think that we always work and project our desires in lifes  

thinking in “these person very close to us”.  

They showed us everything without showing us anything.  

They are very strong in our minds and are the reasons of our decisions.
9. Ben Dronsick
1.4.12 / 12pm


Riveting.
11. quadruspenseroso
1.5.12 / 2am


Dear L., I'm inspired to tweak my doings. Although they will never be so grand as your father's scale and sacrifice, I hope my son will see my tempered rubric, and after I'm gone, some day offer a sublime tribute like yours. I owe my father the same abbraccio.
13. [Stephen](http://skorbichartist.blogspot.com/)
1.5.12 / 9pm


Thank you for sharing this. We are reminded that we are indeed effected by what we learn as children.
15. [chris](http://www.metamechanics.com)
1.7.12 / 12am


Origins are always very insightful. The aside about roaming around derelict jet fighters is more telling than the aside intends to convey.


As one of the posters said above about “pure engineering” and unless I am mistaken your first degree was in engineering?


Great posts on futurism appear on this blog, great photos of a damn being built sent to you I believe by a former student, Delanda guest posts…in short – if any architects work exhibits the theory of the prophets of Deleuze in architecture (Delanda and Kwinter) your work does…without getting into this particular mode of thinking – break – which to most architects is mind boggling (kind of like Tschumis transcripts)..paradoxical to creators of structure…creativity does not reside in an categorical imperitavive…abstraction breaks the becoming…broke…I'd suggest a replacement for that phrase “aesthetisizing violence” with “Mr. Woods expresses often in his works ‘extreme existential engineering', a posibility in physical architecture that is centuries away but can ‘become' in in the virtual – whether internet or paper.” – qouting myself as the thought became a virtual statement, now a abstract concept to be riddled with myth by further review (its friday and I am studying…)


Just my opinion. I could back all that up with heaps of Delueze, Delanda, and Kwinter…but I have to study for those silly architectural exams
17. Caleb Crawford
1.9.12 / 2pm


Wonderful post. 


I have to wonder at the parallels to today, a time in which amazing design and engineering is happening, but also a time in which we face as grave an existential threat as that posed by the A and H bomb. The “milieu of creative engineering, problem-solving, and construction… What was important was getting the job done for good reasons” that you describe was an extension of the immediate existential threat of fascism. Our culture has not yet faced in any coherent manner the existential threat of climate change. It seems that the same misguided “fantasies” of nuclear retaliation and rebuilding on poisoned earth are at play here, or perhaps even worse, a complete denial of the potential consequences. As you indicated in your other post this week, “This understanding, fortunately, is what eventually happened, thanks to the educating efforts of many individuals who took it upon themselves to wake people up to the real dangers of nuclear weapons. The U.S. government deserves no credit in this regard and, in fact, spent more energy playing down the dangers.” The eternal return; Santayana; SOS (same old story). 


Your father's generation was not an anomaly. We have the potential to unleash such creative forces once again, and indeed we have an enormous pent up energy striving to take on the problem. 


The other side of the pressed uniform vs. the rumpled camos is the downplaying of class distinctions, that all are dressed for doing the work, that despite the rigid hierarchy, ideas can come from any member of a team . That relationship to authority (i.e. challenging it) was my generation, a quality I see much eroded in youth today. (just a bit of devil's advocacy here)
19. [WHY I BECAME AN ARCHITECT—Part 1 « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/02/06/why-i-became-an-architect-part-1/)
2.7.12 / 3am


[…] of jet bombers and fighters that enflamed my imagination—as I have already spoken of them here. Instead, I'll take up the story at age sixteen or seventeen when, some years after my father's […]
