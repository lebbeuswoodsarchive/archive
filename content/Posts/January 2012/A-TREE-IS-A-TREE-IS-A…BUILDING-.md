
---
title: A TREE IS A TREE IS A…BUILDING?
date: 2012-01-27 00:00:00 
tags: 
    - architectural_forms
    - architecture_and_nature
---

# A TREE IS A TREE IS A…BUILDING?


It is interesting to see the latest developments in neuroscience being used to justify an approach to design that has been around for a very long time. In short, this approach is to use natural forms as an inspiration for architectural forms. The author of [the following article](http://www.nytimes.com/2012/01/08/opinion/sunday/seeing-the-building-for-the-trees.html?_r=1&ref=opinion) argues that science informs us that our being inside our own bodies (and not only in our minds or, if you will, our brains), makes us especially receptive to the embodiments of other living things, such as trees, her prime example. In other words, we feel most comfortable, most at home, in an embodied environment, as opposed to an environment that we perceive only as an abstraction of ideas.


It is a very seductive argument in a time such as the present when people living in wholly artificial and unnatural cities, far from aboriginal landscapes, often feel overwhelmed by abstractions of every kind, from the dry statistics of global financial crises to the impassioned but still cerebral ideological debates that dominate our politics. Many of us yearn for experiences that take us out of the narrow human world and reconnect us with the less constrictive, richer and in a sense more hopeful world of Nature. The Green movements emerging today testify to this need, as do trends in architecture that emphasize sustainability and the pre-eminence of the organic. It is a mistake to consider them as strictly pragmatic.


[![](media/poelzig-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/poelzig-21.jpg)The last time this approach was taken up in full force was in the period during and immediately following the First World War. Lasting only a few years, it constituted a movement that was called by critics and later historians “Expressionism.” The natural forms that inspired architects such as Bruno and Max Taut, Hans Poelzig, the Luckhardt Brothers, Hans Scharoun, and others were not so much from the organic world reflecting today's concerns, but rather from the inorganic, mineral world of rocks and crystals that underlay the industrial revolution then in full swing. Still, the idea of a reconnection with a deeper level of Nature was—and remains—the driving impulse. *(left) The interior of the Grosses Schauspielhaus, Berlin, 1919, by Hans Poelzig. A design inspired by  stalactite formations in a mineral cave.*


My point here is not to dismiss these architectural movements and trends as merely passing fashions. To the contrary, they are critical counterpoints to dominant trends of our society that—carried too far—become more destructive than creative. Their short-lived nature indicates, however, the limits of trying to embody this need for reconnection only in architectural forms.


LW


[![](media/tods_2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/tods_2.jpg)


*(above) Tod's retail store, Tokyo, by Toyo Ito.*


January 7, 2012


**SEEING THE BUILDINGS FOR THE TREES**


**By Sarah Williams Goldhagen**


A REVOLUTION in cognitive neuroscience is changing the kinds of experiments that scientists conduct, the kinds of questions economists ask and, increasingly, the ways that architects, landscape architects and urban designers shape our built environment.


This revolution reveals that thought is less transparent to the thinker than it appears and that the mind is less rational than we believe and more associative than we know. Many of the associations we make emerge from the fact that we live inside bodies, in a concrete world, and we tend to think in metaphors grounded in that embodiment.


This metaphorical, embodied quality shapes how we relate to abstract concepts, emotions and human activity. Across cultures, “important” is big and “unimportant” is small, just as your caretakers were once much larger than you. Sometimes your head is “in the clouds.” You approach a task “step by step.”


Some architects are catching on to human cognition's embodied nature. A few are especially intrigued by metaphors that express bodily experience in the world.


Take the visual metaphor of a tree as shelter. Most people live around, use and look at trees. Children climb them. People gather under them. Nearly everyone at some point uses one to escape the sun.


Recently, architects have deployed tree metaphors in many different settings. At the [Kanagawa Institute of Technology](http://iwan.com/photo_Junya_Ishigami_Kanagawa_Institute_of_Technology.php) in Japan, Junya Ishigami created an elegant “forest” out of slender, white-enameled metal saplings that congregate in clusters and open into clearings of vocational work spaces. In Seville, Spain, a German architect, Jürgen Mayer H., gave definition and shade to the city's Plaza de la Encarnación with his [Metropol Parasol](http://www.jmayerh.de/19-0-Metropol-Parasol.html), a lilting, waffled construction of laminated timber.


Such projects follow earlier, very different tree-inspired buildings, like Toyo Ito's well-known [Tod's](http://www.arcspace.com/architects/ito/tod/tod.html), a retail store in Tokyo, and the [Mediathèque media library](http://www.archdaily.com/118627/ad-classics-sendai-mediatheque-toyo-ito/), an exhibition space and cinema in Sendai, Japan, which is so well supported by irregular, hollowed-out, sinuous “trunks” (housing elevators and staircases) that it survived the enormous earthquake last March.


Why should tree metaphors appeal to architects? Why should they be useful, even good, for people? In the Seville project, tree imagery helps construct a distinctive public place that offers shelter and areas to congregate. As under spreading trees, the boundaries defining these spaces are permeable; easy to enter and exit, they offer nature's spatial freedom yet help people to feel more firmly rooted where they are. And tree metaphors, deployed architecturally, simultaneously lament nature's absence and symbolically insert its presence.


Tree metaphors also refer to the experience of living in a body on earth. Trees are static, stable objects. Someone connected to a community is “rooted” there; a psychologically sturdy friend's feet are firmly “planted” on the ground. We use trees to describe human bodies and souls: the area from our neck to pelvis is our “trunk”; someone reliable is “solid as an oak”; someone exploring a new area of inquiry is “branching out.”


Buildings aren't nature, of course. Tree metaphors like the branching-out facade of Mr. Ito's Tod's surprise people. But because the surprise comes along with the implied reassurance of structural integrity (they're trees, after all), it prompts us to focus on the built environment, perhaps to reconsider its role in our everyday lives.


Architects may also like tree metaphors because a tree's overall structure is regular, while its fine-grained composition, its tangles of branches, are irregular, an arrangement conducive to the kind of design experimentation offered by new digital technologies.


But the design opportunities that tree metaphors present fail to explain their appearance in such a diverse range of buildings. Trees are familiar. Tree metaphors allow for an architectural inventiveness that stretches people without estranging them.


Trees are just one of the growing number of embodied metaphors used in contemporary architecture: Zaha Hadid builds [riverlike spaces](http://www.nytimes.com/slideshow/2011/07/06/arts/design/hadid.html?ref=zahahadid), while the Japanese firm Sanaa offers up a habitable mountainscape of a [student center](http://www.arcspace.com/architects/sejima_nishizawa/rolex/rolex.html) at the École Polytechnique Fédérale in Lausanne, Switzerland.


How many designers are clued in to the ongoing cognitive revolution and its potential for the built environment is unclear. But this collection of architects and projects herald more than just another stylistic or pyrotechnic, technology-driven trend. They point toward how the built environment could — and should — be radically reconceptualized around the fundamental workings of the human mind. We need, and are ever more in a position to create, a richer built environment, grounded in the way people actually experience the world around them.


[*Sarah Williams Goldhagen*](http://www.sarahwilliamsgoldhagen.com/)*, the architecture critic for The New Republic, is writing a book about how people experience the contemporary built environment.*


*Following are projects referenced in the article:*


[![](media/cog-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/cog-11.jpg)


*(above) Metropol Parasol, Spain, by architect Jürgen Mayer H.*


.


[![](media/kanagawa-institute-of-technology-glass-building-71.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/kanagawa-institute-of-technology-glass-building-71.jpg)


[![](media/05ishigami.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/05ishigami.jpg)


*(above) Kanagawa Institute of Technology, Japan, by Junya Ishigami and Associates.*


.


[![](media/sendai-interior-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/sendai-interior-4.jpg)


[![](media/sendai-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/sendai-1.jpg)


*(above) Mediatheque, Sendai, Japan, by Toyo Ito.*


.


[![](media/epfl_1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/epfl_1.jpg)


[![](media/epfl.jpg)](https://lebbeuswoods.files.wordpress.com/2012/01/epfl.jpg)


*(above) Ecole Polytechnique Fédérale de Lausanne, France, by Kazuyo Sejima and Ryue Nishizawa (SANAA).*


.


Related post on the LW blog: <https://lebbeuswoods.wordpress.com/2010/09/13/building-landscapes/>


#architectural_forms #architecture_and_nature
 ## Comments 
1. [Francisco Vasconcelos](http://csxlab.org)
1.28.12 / 2am


I believe it's … Phenomenology ^\_^
3. Caleb Crawford
1.30.12 / 1pm


This is an interesting thesis, and the relation to “nature deficit disorder” hadn't occurred to me until your post. Also recall that Art Nouveau and independents such as Gaudi occurred in the period before WWI, during some of the middle industrial revolution's greatest miseries. Couple with that the general interest in natural history, which took on an even greater focus with Darwin. There are the fantastic drawings of Ernst Haeckel, which inspired designers of his day. Today we have, in addition to biomorphic form (conventional construction inspired by natural form), biomimic design – design which responds to the performative qualities of natural systems, but may have no overt biomorphic form. Annie has a thesis that the biomorphic goes back even further and has been examining early American floral motifs…
5. [K.S](http://otherscapes.wordpress.com)
1.30.12 / 8pm


The classical columns – Corinthian, Doric, and Ionic, are abstractions of tree trunks – probably the first building materials mankind used. 


“And tree metaphors, deployed architecturally, simultaneously lament nature's absence and symbolically insert its presence.”


Where is this going to lead us? A whole world that has become giant symbol for a ghosted nature?
7. [metamechanics](http://metamechanics.wordpress.com)
1.31.12 / 2am


As Francisco notes – phenomenology.  

Lately reading a lot of Edmund Husserl again and trying to model and draw my dreams. The articles second paragraph hit the nail on the head for me regarding my architecture in dreams – associative. As I get all Gallileo in thinking (husserl reference) or perform rational mathematization on the dreams and thus strip the dreams of meaning – autoCAD, 3D modeling, numerical and categorical placement of form and space – I start to remember the sources for much of the architecture. The architecture is essentially an associative collage of the most phenomenol aspects of experienced or visually sensed architecuture I have witnessed AWAKE. Now the interesting thing to note is, some of the associations take a while to locate in my memory, as if I had completely forgotten the source.  

Which brings me to a question that leads to a point in reference to this article – how much of what we design is simply associative collaging of our memories of design and architecture?  

I would argue all of it, nothing is stand alone original, but can be original in assembly.  

The cultural cycle is a result of societys desire for one type of nostalgia over another. Abstract rational modernism is a nostalgia for rational thinking. What is most interesting and someone above notes this, is the extreme abstract rational modernism is now trying to approach the organic at the level of the organic complexity – or in other words, we are become more consciouss of our subconciouss – we are now closer to documenting the phenomenon in a Gallilean manner that we have ever been before….the rational known mind merging with the unknown mind.  

Good article, tanks.
9. Federico Lepre
2.3.12 / 7pm


The form is always derived from a real image. The reorganization and the study of this image are the major part in creating the design. For example: a tree. A tree as an image is not a tree as a living organism (and we might add that is a stretch to even give it connotations or connections with sustainable green movements), a tree is a form, which means a set of straight lines. When you take that as inspiration, even unconsciously, a tree reorganized these lines or simply play with these lines, in my mind creates a fusion between the most ‘intimate and romantic “Me”, among my knowledge, my thoughts, and in general my all.  

Although the initial shape can be the result was a tree that can be more varied, diverse and eclectic as “product”.  

I think that talk of “tree” as an image that creates a “comfort” that create more livable and “more familiar” spaces is a poor argument like the binomial “” important “is big and” unimportant “is small”. As a contrast image of the “factory” can be more familiar and more comforting. It all depends on how I manage this image.  

Fear of a city too abstract and alienating for people is stupidity as well as the belief that “a tree” and “nature itself” images are “good”.  

Thiat's because when we try to define “good” collective imagination that create “good” conditions of life or anything we choose to generalize people, to create an image of people and this is a fad wrong and too unrealistic. There are no spaces “good” by definition and spaces “bad”, there are “designed spaces”, and spaces that pretend to be courtly without too many bases with too many claims. The latter are usually the spaces created by architecture or sustainable reasoning too, simply because they generalize the “beauty” in the most technical aspects and less abstract. As an architect I have to create deep spaces and “Spaces” and a person must think and live in the space I occupy and live. We are complex and there isn't a collective image that can give us a general and satisfying “comfort”.
11. [Dominique Lamandé](http://lamandedominique.blogspot.com)
2.6.12 / 5am


this tendency of art to appuier on natural forms, is reccurent in creation. have you heard of D'Arcy Wentworth Thompson? Andre BLOCK? Frederick Kiesler?
13. [A Synthetic Architecture » Blog Archive » Commentary On A TREE IS A TREE IS A…BUILDING? « LEBBEUS WOODS](http://amcgoey.net/391)
2.6.12 / 6am


[…] A TREE IS A TREE IS A…BUILDING? « LEBBEUS WOODS […]
15. [Dominique Lamandé](http://lamandedominique.blogspot.com)
2.6.12 / 9pm


<http://lamandedominique.blogspot.com/p/art-architecture_06.html>
17. [A TREE IS A TREE IS A…BUILDING? « Things I grab, motley collection](http://thingsigrab.wordpress.com/2012/02/08/a-tree-is-a-tree-is-abuilding/)
2.8.12 / 12pm


[…] time. It short, this approach is to use natural forms as an inspiration for architectural forms.Via lebbeuswoods.wordpress.com Share this:TwitterFacebookTumblrLinkedInDiggLike this:LikeBe the first to like this […]
19. [A TREE IS A TREE IS A…BUILDING? | concerturbain](http://concerturbain.wordpress.com/2012/02/09/a-tree-is-a-tree-is-abuilding/)
2.9.12 / 9am


[…] time. It short, this approach is to use natural forms as an inspiration for architectural forms.Via lebbeuswoods.wordpress.com Évaluez ceci : Share […]
