
---
title: NOTEBOOK 01-3 (the last)
date: 2009-10-06 00:00:00
---

# NOTEBOOK 01-3 (the last)


2001 was a fateful year for many, and in different ways. Stanley Kubrick had chosen it as the year of rebirth and regeneration in his 1968 film “A Space Odyssey,” because it was the first year of the ‘new millennium,' according to those who consider Jesus to have been born in the Year One. In May of '01 I had a cardiac arrest, quadruple bypass heart surgery, and not only survived, but began a new phase of my life. On September 11 came the attack on the Pentagon and the World Trade Center, the destruction of its blandly mighty towers and the beginning of a new era in American life and world affairs—the beginning, I suppose, of  a slower-motion world war, number III. I traveled later that month to the fairy-tale mountain town of Taormina, Sicily, to lead a RIEA workshop on urbanism. When I returned to New York, I was teaching at The Cooper Union and made an installation “The Storm.” A notebook went with me and I recorded ideas and events, but the world had changed, as had I, and this notebook was to be the last.


![LWblog-NB01-3-1](media/LWblog-NB01-3-1.jpg)


![LWblog-NB01-3-2](media/LWblog-NB01-3-2.jpg)


![LWblog-NB01-3-9](media/LWblog-NB01-3-9.jpg)


![LWblog-NB01-3-9a](media/LWblog-NB01-3-9a.jpg)


![LWblog-NB01-3-10](media/LWblog-NB01-3-10.jpg)


![LWblog-NB01-3-3](media/LWblog-NB01-3-3.jpg)


The Storm installation, New York (2001):


![LWblog-NB01-3-Storm](media/LWblog-NB01-3-Storm.jpg)


![LWblog-NB01-3-4](media/LWblog-NB01-3-4.jpg)


![LWblog-NB01-3-8](media/LWblog-NB01-3-8.jpg)


![LWblog-NB01-3-5](media/LWblog-NB01-3-5.jpg)


![LWblog-NB01-3-7](media/LWblog-NB01-3-7.jpg)


![LWblog-NB01-3-7a](media/LWblog-NB01-3-7a.jpg)


![LWblog-NB01-3-6](media/LWblog-NB01-3-6.jpg)


The Fall installation, Paris (2002):


![LWblog-NB01-3-Fall](media/LWblog-NB01-3-Fall.jpg)


LW



 ## Comments 
1. aitraaz
10.7.09 / 10pm


The sketches bring to mind, at least for me, the final chapter of Deleuze's ‘Abecedarium,' the letter ‘Z' or ‘Zed.'


‘Zed' for ‘Zig Zag,' the movement of the fly, a movement of creation which Zen masters have always known, or rather the ‘nez,' nose (other zig zag) or the lightning bolt, the storm. ‘Z' is the movement:


“there's no Big Bang, there's the Zed which is, in fact, the Zen, the route of the fly…”


Universals (big bang) have never existed, only singularities: singularities in a chaos of potentials. Thus the problem of creation: bringing such disparate singularities into relation in the midst of chaos.


What Zen wisdom has always known: 


“There is always a obscure precursor that no one sees, and then the lightning bolt that illuminates, and there is the world…that's what thought should be…the grand Zed but also the wisdom of Zen…”





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.8.09 / 9pm
	
	
	aitraaz: I was unaware until now of the Deleuze Zed/zig-zag formulation. It seems right here, as with the drawings in this notebook, I left behind the representation of matter and began to explore the representation of energy—work on which I am still engaged. The goal is architecture, just as Deleuze's was philosophy. In both cases, the goals remain elusive, and the work incomplete
	
	
	
	
	
		1. Pedro Esteban Galindo Landeira
		10.11.09 / 3am
		
		
		Which concepts allow you think that is the representation of energy? Please this sounds interesting to me, if you could explain me better, thanks.
3. jeff
10.8.09 / 7pm


Thank you for this personal work. Being confronted with mortality seems to allow a renewed perception of ourselves and what we create. There is obviously a certain immedicacy in terms of time, but something else as well. I think it might be described as a willingness to speak of death in familiar terms. Perhaps this is just an example of the human condition, that we can't really know something until it is experienced.


What do you mean by, “and this notebook was to be the last”?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.8.09 / 9pm
	
	
	jeff: I agree with your comment about mortality—in my case, a close brush with death. I want to say, without sounding pretentious I hope, that all my work is personal—all of it has emerged from my experiences. This post is perhaps a more literal example.
	
	
	What I mean by ‘this notebook was to be the last” is that it was, indeed, the last I made. The notebooks were a project and with this one the project (see my comments on the earlier notebooks posts) was finished.
5. David
10.10.09 / 12am


Spherical aberration as an absolute composition is a powerful paradox of ambiguity, yet it becomes a refraction of unspoken matrix only perceived in a complete unity and harmony that interpenetrate the soul in a path of continue search for balance. Great work.. Thank you for sharing your gift.  

David
7. [davidbburns](http://davidbburns.wordpress.com)
10.19.09 / 12am


This is a banal question, but what are the dimensions of your sketchbook? I'm trying to scale them in my head.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.19.09 / 12am
	
	
	davidburns: 4-1/4 x 6 inches.
9. Paco Diego
11.9.09 / 10pm


Not to dabble in semantics, but when The Storm was presented at Cooper Union, you referred to it as a “construction” (I even purchased a poster of it for $25). 


Reading your awesome0 sketchbook and your captions on the images above, you clearly regard it as an “installation”…can you differentiate between the two words?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.10.09 / 12pm
	
	
	Paco Diego: “Installation” is an art-world term that I don't like for that reason—I want my “constructions” to be seen as architecture (or strongly related to it), and not as art. But I sometimes slip into using the more common term. I really shouldn't do that….
11. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] image: The Storm installation, New York (2001) work of Lebbeus Woods; images from <https://lebbeuswoods.wordpress.com> […]
13. olivier
12.8.10 / 8am


Hi, the drawings are simply beautiful…the expression and hand movement and all, it fascinates me. Like every stroke is meant to be. 


I have a question, what is this model of sketchbook?  

i've never seen other like this. I asking you that because I think that the fabric that covers it makes it even more affected by time and experiences. The stain suggests something.


Thank you very much in advance


-Olivier
15. [Darkness or Light « Phos + Skia](http://skiaphos.wordpress.com/2012/05/22/black-on-white/)
6.13.12 / 6pm


[…] LEBBEUS WOODS     Notebook    97-3   98-3   01-2   01-3 […]
