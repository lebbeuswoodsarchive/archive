
---
title: UTOPIA?
date: 2009-10-11 00:00:00
---

# UTOPIA?


![IdealSquare](media/IdealSquare.jpg)


The idea of utopia has all but vanished. The avant-garde architects of earlier generations rarely used the term—its meaning can cut both ways—but still proposed ‘ideal' urban designs that were, in effect, utopian. Now the avant-garde, such as it is, is focused on pragmatic matters, from innovative computer techniques of form-making to issues such as sustainability. Utopian ideas are conspicuously absent. Why is this so?


 The reasons, I think, are complexly interwoven. Foremost, the widely accepted feeling is that we have reached “the end of history” (Fukuyama) and the global triumph of capitalism and ‘liberal democracy.' While the former is manifestly not the case, it is true that the demise of socialism as a human ideal has left no credible alternative to capitalism's global dominance. All utopian projects reach not only for formal or technical improvements, but social ones, as well. So, in the current climate, the only possible utopias are those perfecting capitalism and its present, consumerist, forms of order. We can think of Rem Koolhaas as the visionary of consumerist utopias, celebrating its virtues and vices in equal measure. But we can also see shopping mall designers in nearly the same way, regardless or even because of their lack of design originality—very liberally democratic. In one sense, utopia has already been realized. Anyone can get a credit card, everyone can buy and be happy, at least until they max out their cards. So, where is the inspiration to envision ‘another' utopia? Certainly, the present leaders in the field of architecture have not found it.


 Then, there is the ‘green' movement. Who can argue with its premises? Our priority is no longer to improve human society but to save the planet from human society. Changes to be made to the social system are more remedial than systemic: reducing air pollution and carbon footprints, recycling, refitting, redesign, and the like. Capitalist enterprise, far from being curtailed, is encouraged through tax-incentives and government subsidies of new, green industries to expand its dominant role. Adaptivity is its keyword: anything can be turned to a profit. But who can argue with the goal, and since the very word socialism has become an insult, who would dare to? The green movement is important and necessary, but whether capitalism is really to be trusted with its fate remains to be seen. The lack of green utopias in a time increasingly obsessed with green issues may be due to capitalism's success and unchallenged dominance.


 This idea is certainly reinforced by the ubiquity of information. The instant accessibility from anywhere of information about anything seems in itself a utopian achievement. Information has been radically democratized and with it comes a belief that knowledge has, too. However, information is not knowledge (see the post Ars Brevis, Vita Longa) and indeed it takes knowledge not present in the information to put it to any use. There is a continual stream of new information, with the result of keeping its recipients continually off-balance—we never have enough and must continually return to the sellers to get more: internet sites that in one way or another are in the business of making money. Information is the ideal capitalist product. There is a cheap, inexhaustible supply of it and an insatiable market of consumers who believes it empowers them, and keep buying. How much closer to utopia can we get? We might say that capitalism is a utopia of self-satisfaction and restlessness. Who, then, needs a better society? Alternative utopias would be out-of-date as soon as they would be written or drawn. Ideals and idealism can only slow us down. Utopias can only get in the way.


 Then we come to architects themselves. Let us not consider the usual, even intelligent and talented practitioner. He and she have never, historically speaking, been interested in the hypothetical ‘what if?' as much as the down-to-earth ‘what now?' Instead, if we think about avant-garde architects who have some visible profile, we don't find work that envisions a social world widely improved by architecture. No utopias of the sort that dot the map of architectural history up through the post-Modern era of the 70s and 80s of the last century. Today, their aspirations seem to have retreated before the advance of capitalism and liberal democracy.


 Have we reached the end of utopia as well as the end of history?


 Let us listen to, and watch, the more ambitious and idealistic of the coming generation. Only they have the answer.


 LW


(top) Ideal Square of a City by Francesco di Giogio (often attributed to Piero della Francesca),  c. 1470


View of New Babylon by Constant, late 1950s:


![Constant1](media/Constant1.jpg)


City of Towers by Peter Cook, c. 1984:


![Cook-3](media/Cook-3.jpg)



 ## Comments 
1. Pedro Esteban Galindo Landeira
10.12.09 / 3am


But you only define utopia like process involving social aspects? Without social elements can exist the utopia?


What do you think about the form-making by computers? The computer is only a tool or the creation has died?
3. [Paulo Miyada](http://doistentamfalar.net/videoimagens)
10.12.09 / 6am


” Let us listen to, and watch, the more ambitious and idealistic of the coming generation. Only they have the answer.”


When and how will we meet them, then?  

How do you see competitions and prizes targeted at young people as opportunities for them to develop their projects (not only buildings, but texts, magazines, films)?


I see the new generation of architects and cultural producers as one too dependent on ‘project-oriented opportunities'.


Maybe this is a little off-topic, but i think that another obstacle to the development of contemporary utopias is the growing bureaucratization of the instances that legitimate critical and prognostical ideas.


Isn't utopia, nowadays, just another format that youngsters can attach to when they want to get some visibility and legitimate their projects?
5. [dpr-barcelona](http://www.dpr-barcelona.com)
10.12.09 / 10am


We found really interesting this topic about if there's still “new utopias” and try to focus in new generation's works. 


Recently we have published a post about the same topic [in Spanish] with a brief review about Constant, Archigram and other's work and then, make a new approach with some examples of new projects that we can include in the definition of “utopia”:


<http://arkinetblog.wordpress.com/2009/10/08/%C2%BFexisten-las-nuevas-utopias/>


– Have we reached the end of utopia as well as the end of history? | We think that utopias will never die, while some architects (at least 2 or 3) keep on dreaming with new ideas about cities and human beings.
7. Jack
10.12.09 / 7pm


It is ridiculous to say “the demise of socialism as a human ideal”. Socialism is alive and well and is the societal ideal for everyone on earth. The best societies on earth, i.e. northern European countries, are the richest, most peaceful and happiest countries on earth and they are very socialist, with free health care, free education, and a economic safety net for all. They also allow freedom of expression and encourage creativity and individual expression. It is ridiculous for an intelligent man like you to spout this propaganda of the right wing conservative liars.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.12.09 / 9pm
	
	
	Jack: I lament socialism's demise as a GLOBAL alternative to capitalism. Its success, in all the terms you mention, in a few small Scandinavian countries doesn't hearten me much. Germany and France have vestiges of it, but without major countries like China and Russia pressing its case, it is at best only a niche system, and offers no serious challenge to capitalism. I wish it were otherwise.
	3. Pedro Esteban Galindo Landeira
	10.13.09 / 5am
	
	
	freedom of expression and encourage creativity and individual expression. 
	
	
	AJAJAJAJAJJ sorry I can't say anything else, where do you live??? Individual expresion??? I'm a student and even I can express like I will like to, I need to go to internet to find somewhere to do it or at least read somethings I like.
9. [architectural ruminations](http://heres-todays-special@tumblr.com)
10.12.09 / 9pm


lw,  

your first two paragraphs are very similar to thoughts i had after our last class…i spent a good deal of time researching utopias and there is little written on them after the mid-1980's. i agree with your belief that rem is the “visionary of consumerist utopias” – this could be the topic of a whole discussion!
11. river
10.13.09 / 6am


You might like this discussion:
13. bdotq
10.13.09 / 11am


It seems you have a very particular definition of utopia that you are working with, one that seems to be hard linked to vintage definitions of socialism.


In terms of architechture and those professionals working in the field, I do think the concepts of an ideal world are still alive and well. The places that I really see it is in the areas of urban planning. The ones who are doing it right, are focused properly on the quality of life for all citizens of the city; Developing masterplans that make LIVEABLE cities.
15. [cygielski](http://cygielski.com/blog)
10.13.09 / 1pm


Socialism was founded on erroneous presumptions – that human beings can be made better by changing their environment. What we need is a system that will take into account human vices (like capitalism does), but will channel them into more positive pursuits (as the current corporatist flavor of capitalism most obviously does not). I see something of a possibility in the open source (“free as in freedom, not free as in free beer”) movement, which is beginning to spread to fields other than just software.
17. Jack
10.13.09 / 2pm


More ridiculousness: “I lament socialism's demise as a GLOBAL alternative to capitalism.” In fact, socialism is not in demise, it is spreading, only limited by those in power who prefer the selfish advantages of the status quo. Your comment that it isn't real until Russia and China do it has the tone of a spoiled child expecting instant gratification. Get a sense of history! Social change takes centuries. Socialism is the inevitable development of human enlightenment. All alternative systems lead to exploitation and environmental devastation. The only alternative to working for the common good is the perpetuation of selfishness and injustice.
19. [State Oil Company of Azerbaijan Republic | Edward Burtynsky « dpr-barcelona](http://dprbcn.wordpress.com/2009/10/13/state-oil-company-of-azerbaijan-republic-edward-burtynsky/)
10.13.09 / 5pm


[…] at this apocalyptic images the last word to think about is Ecology. Lebbeus Woods quotes in his last text: Foremost, the widely accepted feeling is that we have reached “the end of history” as Fukuyama […]
21. river
10.14.09 / 12am


My fiancee has recently returned from her MSW trip to Köln. There are people making free art in the streets there! But they are well cared for. They have art. They live in apartments mostly. Densely. Without disturbing the wilderness around them too much. If this is what socialism looks like, then I want it yesterday! Free as in Freedom! (not just beer).


The folks who will willingly grow your foodstuffs? They need their land back pronto!





	1. rob
	7.9.11 / 8am
	
	
	Why dont you move to a socialist country then?
23. W. Bradley
10.14.09 / 5am


A small aside to some of the commentary. I was struck by Jack's comment regarding what he sees as the health of Socialism in northern European countries and his snippy retort to – “Get a sense of history!” after coincidently reading Victor Davis Hanson's take on the Norwegian Nobel committee bestowing our President with the Peace Prize. Prof. Hanson, as a historian, brings a sense of history to Norway as he begins:


“Norway is a tiny country that was born lucky. It is weak and defenseless (and was quickly overrun in World War II [while neighboring, neutral Sweden sold the Third Reich 40% of its iron ore, that went for everything from Tiger tanks to kill Americans to the ovens at Auschwitz—with free shipping across the Baltic included as a favor]. In the late 1940s it would have been Finlandized during the Cold War, if not for American-led NATO. And the world's largest military is still pledged to its defense, in case any of the nations, to whose icons it bestows awards, some day decides to send terrorists or nukes its way.”


“Second, it sits on or near enough oil to allow what is otherwise a rather insignificant country to be the wealthiest per capita oil producer in the world, and enjoy the influence that many in the Gulf have grown accustomed to. Throw in minerals, natural gas, timber, and fish and the nation sits on a bonanza of natural wealth. No wonder there are philosophers who ponder how to dispense the largess and absenteeism is a national crisis (one receives almost ad infinitum the same cash whether “sick” at home or well on the job). The population of under 5 million is largely homogeneous (90% Nordic), and is thus stable, and both rich and safe beyond its wildest dreams. It does not border a Third World country; “difference” and the “other”—even with recent Islamic immigration—is still defined as speaking Swedish or Danish.”


“In other words, Norway has the leisure to be utopian, and cannot quite understand why other countries are not as liberal as it has proven.”


You can read Prof. Hanson's full post here: 


<http://pajamasmedia.com/victordavishanson/nobelitics/>
25. [city, battlesuit, archigram - mammoth // building nothing out of something](http://m.ammoth.us/blog/2009/10/city-battlesuit-archigram/)
10.14.09 / 3pm


[…] Woods's recent post on utopia isn't explicitly linked to this conversation, but Varnelis's comment on the […]
27. [Stephen](http://skorbichartist.blogspot.com/)
10.14.09 / 4pm


This discussion seems to be going off track a bit. The idea of utopia as a creation of the avant-guard has always been exciting. The reality is that utopian visions that have made landfall tend to be totalitarian and hermetic in nature. I am going to relate the idea of utopia to that of the idea city. The design of a city by one designer or firm does not work in reality, in my opinion.  

I think that there needs to be a good framework put in place that allows many creative people to add ideas. This is what makes a good city.  

Architects and artists should, however, continue to put forth ideas of utopia as these ideas can only add to the mix.  

What is the definition of utopia? Is it a place with no problems?  

I don't think that, as a society, that we ever want to get to a true utopia. It would probably be rather boring.  

I don't know if socialism is utopia. These days, I'm not even sure what socialism is.  

The fact is that there are too many people on the planet to manage themselves without a little organizational help. Better access to education and helthcare, freedom of expression could be utopia.  

We can all do better.
29. [cygielski](http://cygielski.com/blog)
10.14.09 / 7pm


Utopia DOES NOT require socialism. Utopia requires imagination.
31. djeak
10.15.09 / 8am


Lebbeus, I'm wondering what you think of this quote from a 2001 interview with the writer Bruce Sterling:  

“We're entering a different conception of history, in which Armageddons and  

Utopias are seen as simple-minded. Because they're the same thing: a bogus  

method to stop thinking about the passage of time. In a Utopia, history  

ends because everything's perfect; in an Apocalypse, history ends because  

everyone's dead. The problem here is not that we need pie in the sky or  

death-threats in order to feel awake. The problem is that the clock doesn't  

stop ticking just because we might find that intellectually convenient.”


In Sterling's estimation, Utopia and Oblivion are interchangeable concepts — polar extremes that hamper our thinking and prevent people from actually doing anything useful — and any declaration of history's end is mere arrogance and shortsightedness. But I imagine that the ideas that you are addressing here are less about capital-U Utopia as an eternal, “end-of-history” condition, and more about an exploration of alternatives to hierarchical or otherwise oppressive social orders. Or, perhaps between those two notions, a reconciliation of the needs of our pre-civilized, tribal (animal?) heritage with the possibilities and difficulties imposed by our civilization (and, of course, there are people who would argue that utopia ended with the advent of civilization, and could only return with its demise). Maybe “utopia” as a concept could be understood as a term not unlike, say, “rheumatism”, as it's similarly useful mostly as a very general descriptor of symptoms, and not in addressing any specific causes, goals, or outcomes. Maybe the catch-all grandiosity of the term needs to be taken down a notch or two in favor of something that people can actually get a handle on and use, rather than some ineffable ideal that's always just out of reach. In that sense, for example, we get concepts such as and [as specific alternatives to received social arrangements and economic activity; and](http://en.wikipedia.org/wiki/Parecon "participatory economics")  [as an alternative to ecological shortsightedness (and, implicitly, to capitalist growth-for-growth's sake).](http://en.wikipedia.org/wiki/Permaculture "permaculture")


The “lack of green utopias” then becomes “the lack of intentional, participatory permacultures” — more of a mouthful, less of a sense of awe and timelessness, and yet a clearer articulation of goals and desires (perhaps even the term “socialism” had somewhat similar origins?). Maybe, once these goals are reached, they appear to be “utopian” to someone who never thought through their implications. But once they become the new conditions of political and economic life, they give rise to their own new problems.


I'm aware that your work strives for something more poetic, and that you're here to open possibilities, to spur thinking and not prescribe it's direction. In that regard, what I'm talking about probably looks like a sort of base, utilitarian pragmatism. Maybe that's what it is. But I have to wonder how helpful it is to continue speaking of utopia. It's helpful at least as a byword for “what we want that isn't what we have”. But shouldn't we push a little farther than that? Am I misreading you entirely?
33. djeak
10.15.09 / 8am


My apologies for the linking botch — I'm not accustomed to XHTML. The last sentence of the 1st paragraph was meant to read “In that sense, for example, we get concepts such as “intentional communities” and “participatory economics” as specific alternatives to received social arrangements and economic activity; and “permaculture” as an alternative to ecological shortsightedness (and, implicitly, to capitalist growth-for-growth's sake).” 


(with links to Wikipedia entries on the words in quotes)
35. river
10.15.09 / 9am


Imagination!?  

Yes, Pronto or else, is how I feel too.  

I am no longer sorry that you are unprepared to hear of it.  

It's coming! in Spades now!


I'm going to relate the idea of the Garden to your house, in your neighborhood… however you define it. And see what happens? 


Heck! Maybe something will happen!
37. David J
10.19.09 / 10pm


I fully agree with stephen especially when he states that ‘a true utopia would be boring' through my own reading during Studies my biggest problem is that for a utopia to exist every person has to conform to the same vision.


The problem is every person has a different understanding of a situation and to what would be the solution to it, just look at this blog.


I don't think that a true ‘utopia' could ever exist but if there was a way, then maybe free expression could be the closest way to ever obtain this.
39. AWax
10.25.09 / 10pm


I agree with David, that no utopia can exist. It is part of the human condition to want more, and to define life equally based on misery as well as happiness. True utopia cannot be fabricated, it is not a stencil, it is a wild dystopian chaos balanced Utopian ideals.
41. Laura Blosser
10.30.09 / 9pm


The utopia needn't be homogenous, idyllic, or pure. The utopia must, quite simply, exist. 


It's about hope, about ownership — architects must possess the agency to envision the future.


I am most interested by the current lack of utopias. Why have we lost our ability to imagine? 


(Fear of incompleteness? Oppressed by the overflow of data? No clear “raison d'etre” of a utopic proposal — ie no product?)


[Perhaps, (& correct me if I'm wrong), there were fewer competitions & grants 50 years back, which gave architects more time to explore utopic visions ? The Italian recession of the 60s lead to the pursuits of Superstudio & Archizoom; maybe there will be a revival of utopias with the recession?]
43. thais
11.5.09 / 1am


I think it is really hard to experience Utopia, but nonetheless, it exists. Even more than we think.  

Utopia has always been accosiated with something unattainable, and therefor unachievable.  

Not many years ago, some “intelligent” people decided to make up an utopia. But only centuries after, we realise what sort of utopia that was.  

…Utopia is everywhere
45. David J
11.12.09 / 11pm


I still find it very difficult to envisage society today as a utopia,


i may be a cynic but my understanding of a utopia is of a society that functions correctly. in other words the Garden of Eden scenario.


the only way a utopia can exist, is if as individuals we give up some of our ‘freedoms'. but as can be seen throughout society there are too many people who will take advantage of weaker mmbers of society it is a trait that we as humans have and until this trait is irradicated then there can never be a utopia
47. [dokclab](http://dokclab.wordpress.com)
9.13.10 / 7am


I think Utopia is aspiration, and lack of utopia today is because no one believes, after the great disappointment of the 70-80-90, the possibility of achieving, even a remote possibility.  

We need faith, not religious, but we need to believe that there is the possibility of creating a utopia.  

After the death of ideologies (I believe that capitalism is not an ideology, but only to make money), utopia can only start from the bottom, where there is a chance for growth.  

Utopia is: “Possibility”.


(sorry for my English)
49. [Lost-InPlace](http://lost-inplace.blogspot.com/)
10.2.10 / 9pm


Is it possible, perhaps, that those considering a big picture, conventionally the notion of Utopia, have instead succumbed to cynacism and now hold the belief such a utopia is unobtainable after allowing for human error, (or corruption, or greed, or any number of other flaws…).  

We now have a very negative culture, those at the top profit from doom-mongering: environmental disaster, pandemics, terrorism, etc. and the focus is now on various ways in which our race will collapse on itself or how this can be countered (whether the threat is real or fictional is irrelevant).  

I am currently reading The Age of Absurdity by Michael Foley, he discuss the philosophy for a blissful (read utopian?) lifestyle and how the structure of the modern age and capatilism is actually counter-productive in our efforts to achieve this; Capitalism gains only if we have a constant need so any attempts to solve real problems would de-reail the system. Society now thrives from (and is fascinated by) a state of Dystopia.
51. [all but… « CUPtopia](http://cup2013.wordpress.com/2011/01/05/all-but/)
1.5.11 / 6pm


[…] UTOPIA? Ideal Square of a City by Francesco di Giogio (often attributed to Piero della Francesca), c. 1470 […]
53. [Bruno](http://web.mac.com/surjan)
1.10.11 / 10am


perhaps… the question (though the thesis supporting project remains… “what is Utopia?”) is not a matter of what or how or where or even when… utopia simply is and acts as the truest counter to the conventional nature of reality. In fact by the very ephemeral and unobtainable nature of Utopias, the idea survives and replicates gaining strength, a strength measured not by plausibility or tangible existence… rather measured against it. To whit, achieving utopia is not the end of the road; the endgame… pursuit, now that seems to be a sentiment worth living for. Utopia is a relay, not a race.
55. [Wormholes in Stockholm | Economy « dpr-barcelona](http://dprbcn.wordpress.com/2011/05/30/wormholes-in-stockholm/)
5.30.11 / 12pm


[…] des Simulacres | What if Capitalism has finally conquered our Utopias? [3] Lebbeus Woods on Utopia? [4] Architecture and utopia: design and capitalist development by Manfredo […]
57. [купон безлимитные тарифы](http://inwathefthem.land.ru)
11.12.11 / 10am


Thanks a bunch for sharing this with all folks you really know what you're talking about! Bookmarked. Kindly also discuss with my website =). We may have a link exchange contract between us
