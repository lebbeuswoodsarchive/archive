
---
title: CONSTANT VISION
date: 2009-10-19 00:00:00
---

# CONSTANT VISION


![Constant-1](media/Constant-1.jpg)


(above) Constant, c.1960


Some time ago, I was speaking with Peter Cook, the founder of Archigram, about the Situationists and, in particular, Constant Nieuwenhuys—the artist and visionary architect—and he told me a story. “In 1959 or '60, Mike Webb (a founding member of Archigram) and I attended a lecture given by Constant on his “New Babylon” project. We were just graduating from architecture school, but Mike leaned over to me during the lecture and whispered “we can do it better!” And so, a couple of years later, they did it at least differently, setting off a revolution in architecture that reverberates to the present day. What both Constant and Archigram did was imagine architecture as a leading instrument of social change, through the making of ideal or utopian architectural projects. The difference between them is that the projects and the ideals they expressed stand on opposite sides of a cultural divide. This takes a bit of explaining.


 First of all, the historical divide. The late 50s and early 60s was a period of global ferment and change. Eastern Europe and China were ruled by communist ideology. Western Europe and the Americas were ruled by capitalist ideology. Each was fearful of the other and the result was a ‘cold war' fought by propaganda and angst-raising demagoguery, as well are ‘contained' hot wars in smaller countries around the world.  Growing numbers of people on both sides of this ideological divide were growing restless with the methods of domination and its values. Some looked to the opposite ideology for salvation, some to personal rebellion, while most quietly endured. In the early 60s, the spreading discontent began spilling into the open. In the East, ‘counter-revolutionary' movements were brutally repressed. In the West, political and intellectual challenges to the status quo were harder for governments to contain, until massive public protests and uprisings in the later 60s were violently suppressed. So, at exactly the time that Cook and Webb attended the Constant lecture, the world was poised on a threshold of sweeping change.


 Then there was the technological divide. Up to the 60s, people living in cities—including those in government and other corporate institutions—functioned in analog modes: typewriters, ‘snailmail,' telegrams, telephones—even many early data-processors produced analog ‘punch-cards'. After 1960, digital computation began to take over, owing to technological advances such as the transistor and microchip. These enabled—among other things, such as credit-cards—the launching of geo-stationary satellites and instantaneous global communications. The contemporary world was born. Archigram, founded in 1964, quickly became a global phenomenon, in a way that New Babylon never did. Using Archigram ‘comics,' a traveling ‘opera' and other media-savvy tactics, they spread the word of a new ‘instant city,' alive with electronically propagated pop-culture, spontaneous events and ‘happenings', and the light, temporary architecture of eternally migrating, even globe-trotting urban dwellers. Constant, in his utopian New Babylon project, belonged to a seemingly settled and abruptly out-dated past of heavy, fixed structures and, in a sense, of traditionally Modernist architecture. These last remarks are in the main correct, but do in fact exaggerate Archigram's break with the past (including Constant's work) and certainly do not do Constant's project justice, particularly with regard to its social aspirations. But more on this in a moment.


 The last divide was architectural. The early 60s saw the end of the dominance of Modernist ideas about the beauty of the machine and the ethics of industrial technology, and the advent of Post-Modernist ideas focused on the virtues of the handily ambiguous term ‘culture,' emphasizing imagery over structure. Venturi's and Scott-Brown's anti-Modernist manifesto (“Complexity and Contradiction” ) appeared in 1965 and quickly became the new ideal. While it eschewed the work of Archigram, I do not believe that it was ignorant of it and was at least tangentially inspired by its pop-culture imagery. In any event, the days of ‘heavy metal' architecture were over, and those of architectural pastiche based on history and popular culture were in. Constant's New Babylon, along with the works of many architects who were the inheritors of Modernist ideals but had already extended them—Paul Rudolph and John Johansen among others—were abruptly ‘ignored.' Such is the nature of any ideology, especially a newly risen one, bent on securing its unrivalled dominance.


 New Babylon was inspired by and contributed to the work of the Situationists, a group of intellectuals, theorists and writers, as well as artists who were anything but Modernists in the classic capitalist mold. They were inspired by the irrational forms and practices of Dada and Surrealism, and were what we could call neo-Marxists, meaning inspired by Marx's vision of revolutionary socialism but seeking to use the capitalist system to achieve their ends. Guy Debord and others invented tactics such as *derive*, *psychogeographie*, and *detournment*, which seized upon, then subverted, capitalist notions in order to develop radical ways of living that were meant to culminate in revolution (Archigram first heard of these through Constant's lecture, no doubt). Constant joined the Situationists early on and became their architect, much the same as Antonio Sant'Elia had done with the Futurists, half a century before. The spaces of New Babylon were intended to be spaces of disorientation and of reorientation, from rational, functionalist society to one that is liberated and self-inventing. It was meant to replace capitalist exploitation of human labor and emotion with anarchist celebration of them.  Its architecture was to provide a complex armature on which could be woven endlessly new, unpredictably personal urban experiences, determined by ever-changing individual desires. In the end, however, the architecture of the New Babylon seemed to overwhelm such playful, radical spontaneity by its sheer weight and monumental scale.


 Is Constant's epic project of other than historical interest today? I believe it is. Aside from the visual strength and sometimes poetic nuances of his models, paintings and drawings, New Babylon raises many questions related to issues of contemporary interest. What role can architecture play in social and political change? What role should an architect take in determining the direction and character of change? How important is the design of space in the whole mix of human activity? Can design “change the world?” If so, who should control it? Constant and New Babylon can still inspire us—as they once did the young Peter Cook and Michael Webb—in spirit, if not in form.


LW


New Babylon: in the process of growth:


![NewBab-16](media/NewBab-16.jpg)


![NewBab-8](media/NewBab-8.jpg)


![NewBab-17](media/NewBab-17.jpg)


![NewBab-20](media/NewBab-20.jpg)


![NewBab-21](media/NewBab-21.jpg)


![NewBab-19](media/NewBab-19.jpg)


![NewBab-9](media/NewBab-9.jpg)


![NewBab-27](media/NewBab-27.jpg)


![NewBab-22](media/NewBab-22.jpg)


![NewBab-31a](media/NewBab-31a.jpg)


Relevant links:


<http://en.wikipedia.org/wiki/Situationist_International>


<http://members.chello.nl/j.seegers1/situationist/constant.html>



 ## Comments 
1. [Emily Marchesiello](http://www.stevenholl.com)
10.19.09 / 5pm


Great post, we're looking at it right now and gaining a special shot of energy.


–Steven, Andrew and Emily
3. [Constant Vision by L. Woods | Unit 13](http://unit13.ortlos.info/?p=175)
10.19.09 / 9pm


[…] link […]
5. [CONSTANT VISION « LEBBEUS WOODS | The World Matters](http://www.theworldmatters.org/2009/10/20/constant-vision-%c2%ab-lebbeus-woods/)
10.20.09 / 1am


[…] Published on Oct 20, 2009 at 2:00 am . Filled under: Arts & Culture | No Comments via <https://lebbeuswoods.wordpress.com/2009/10/19/constant-vision/> Bookmark and Share: sociallist\_c958f2db\_url = […]
7. river
10.21.09 / 6am


Constant Vision, hrrrng! No oldies. All new. We have no preconceptions about heading there, anymore. We are simply all related, somehow or another. We grow and cook our own best foods for each other's visits. We pull our finest wines from the vault.
9. [Фoto-inbox](http://foto-inbox.ru/?p=4263)
10.23.09 / 2am


[…] via <https://lebbeuswoods.wordpress.com/2009/10/19/constant-vision/> […]
11. diegopenalverdenis
10.27.09 / 12pm


Interesting post, I did not know Mr. Constant Nieuwenhuys.  

For me, this vision is ugly and claustrophobic, hope we don´t end up living in dark and omminious hives like this.  

I would like to think of cities as something a little more light and cheerfull. 🙂
13. [Arcosanti and Macro-Cosanti | Paolo Soleri « dpr-barcelona](http://dprbcn.wordpress.com/2009/10/29/arcosanti-and-macro-cosanti-paolo-soleri/)
10.29.09 / 8am


[…] One single wagon will already make a useful instrument, just like in Constant's New Babylon or Branzi's No-Stop-City, the idea of a never-ending city was here mixed up with the sense of […]
15. [organicmobb](http://organicmobb.wordpress.com)
12.29.09 / 5am


archigram to me inspires my own intuition. It must be put forth for it is me.
17. [PsyGeo from Z to A » Constant Nieuwenhuys : situationist architect](http://fromztoa.net/?p=1437)
12.29.09 / 12pm


[…] Nieuwenhuys is key to understanding what a Situationist cityscape would have looked like. This article by the architect Lebbeus Woods, discussing Nieuwenhuys' major city project in a neo-marxist […]
19. [blog.lhli.net](http://blog.lhli.net/2009/12/29/constant-vision-%c2%ab-lebbeus-woods/)
12.29.09 / 2pm


[…] <https://lebbeuswoods.wordpress.com/2009/10/19/constant-vision/> Dec 29, 2009 14:46 av lhli. ffffound Inga […]
21. [PAPER — THE BI BLOG](http://www.thebiblog.net/?p=2213)
4.4.10 / 8am


[…] New Babylon, Constant Nieuwenhuys, 1959-74 (source) […]
23. [Another City for Another Life: the unforeseen games of the city of the future | jeff watson](http://remotedevice.net/blog/another-city-for-another-life-the-unforeseen-games-of-the-city-of-the-future/)
5.26.10 / 11pm


[…] More on Constant Nieuwenhuys: Texts, Photos, and Paintings at notbored.org, profile at DADA and Radical Art, “Constant Vision,” by Lebbeus Woods […]
25. [Another City for Another Life: the unforeseen games of the city of the future « storyglot](http://storyglot.wordpress.com/2010/05/26/another-city-for-another-life-the-unforeseen-games-of-the-city-of-the-future/)
5.26.10 / 11pm


[…] More on Constant Nieuwenhuys: Texts, Photos, and Paintings at notbored.org, profile at DADA and Radical Art, “Constant Vision,” by Lebbeus Woods […]
27. [INSTANT CITIES « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/13/instant-cities-2/)
1.13.11 / 12pm


[…] models they were projecting a future of adventurous hedonism, a liberation of the sort projected by Constant, though emphasizing human creativity less than leisurely consumption. Their visions fulfilled the […]
29. [New Babylon: Constant Nieuwenhuys « J.A.N.U.B.](http://janub.wordpress.com/2011/02/09/new-babylon-constant-nieuwenhuys/)
2.9.11 / 12pm


[…] to read the full article: <https://lebbeuswoods.wordpress.com/2009/10/19/constant-vision/> […]
31. [Another City for Another Life: the unforeseen games of the city of the future | blog.remotedevice.net](http://blog.remotedevice.net/2010/05/26/another-city-for-another-life-the-unforeseen-games-of-the-city-of-the-future/)
6.6.11 / 6am


[…] More on Constant Nieuwenhuys: Texts, Photos, and Paintings at notbored.org, profile at DADA and Radical Art, “Constant Vision,” by Lebbeus Woods […]
33. [everything mobile | line\_of\_longitude](http://lineoflongitude.wordpress.com/2012/04/24/everything-mobile/)
4.24.12 / 7pm


[…] morning Yuma, Arizona manifest the visionary cities of Archigram, Situationist International New Babylon and Cedric Price.  Hot air balloons fill the sky moving East, most land in Exit 3′s […]
