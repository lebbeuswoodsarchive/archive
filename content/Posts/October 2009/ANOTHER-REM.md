
---
title: ANOTHER REM
date: 2009-10-24 00:00:00
---

# ANOTHER REM


The story goes like this. In 1983, then-new President of France Francois Mitterrand decreed a one-stage competition for the design of a new public park to be built on the large site of former slaughterhouses in Paris. It was intended to be the largest landscaped park in Paris and the first example of Mitterand's *Grands Projets,* and of his great ambitions to leave a lasting mark on the city. A jury of architects and landscape architects met, reviewed the entries and selected the design submitted by the Office for Metropolitan Architecture, led by Rem Koolhaas. This was announced in the Paris newspapers, and it looked as though Koolhaas would at last build a major project. Within a few weeks, however, it was abruptly announced that this would now be a two-stage competition, with the winner and the runner-up, Bernard Tschumi, required to develop their designs further and submit them to a new jury. In the Spring of 1984, it was announced that the design by Tschumi had been selected as the one to be realized, which it was several years later. All that remains of the OMA design is the large-scale model built for the second-stage submission. It has been said that Tschumi wisely did little to elaborate his first, very abstract, runner-up scheme, while OMA did too much. Certainly, its complexity appears expensive to build and, more damningly perhaps, to maintain.


 OMA's design shows us Rem Koolhaas at his most playfully original and inventive, yet at the same time most populist, provocative and, yes, conceptual. The scheme transforms the fifty-five hectares into a post-Modern labyrinth of contrasting layers in both horizontal and vertical dimensions. Existing exhibition buildings comprise one layer; connecting pathways and bridges another; street furniture, kiosks and other smaller structures another. The most important layer is the one comprised of forty-three parallel landscaping strips, each the same width, covering the whole site. Each strip contains different program activities, such as sports and games, but also different kinds and placements of trees, shrubs, and ways through them—a kind of botanical garden based not on scientific classifications but spontaneous explorations and, well, sheer enjoyment. Superimposed on one another, the layers comprise a theme-park of a new type, one unprogrammed and without stereotypes, perfect for Situationst *derives* and children's (of all ages) games. It is a park that would be difficult to experience the same way twice—repeat visits would each be different. For example, if we walk along the strips, we encounter a particular sequence of landscapes and experiences; if we cut across the strips an entirely different sequence of perceptions is discovered. We might call this park a proto- or supra-urban landscape, in that its experience evokes the qualities we would want a city to have, foremost among them the ingredients of our personally selected self-invention. It is very much a people's park, not because it caters to the lowest common denominators of expectations, but playfully challenges people to make of it what they can, each in their own way.


 This project reminds us that there was once a Rem Koolhaas quite different from the corporate starchitect we see today. His work in the 70s and early 80s was radical and innovative, but did not get built. Often he didn't seem to care—it was the ideas that mattered. However, his scheme for the Parc de la Villette begs to have been built and we can only regret that it never was.


 LW 


![REM-LaV-6](media/REM-LaV-6.jpg)


![REM-LaV-7](media/REM-LaV-7.jpg)


![REM-LaV-2](media/REM-LaV-2.jpg)


![REM-LaV-5](media/REM-LaV-5.jpg)


![REM-LaV-1](media/REM-LaV-1.jpg)


![REM-LaV-4](media/REM-LaV-4.jpg)


![REM-LaV-3](media/REM-LaV-3.jpg)



 ## Comments 
1. Laurence Turner
10.25.09 / 12am


Serendipity is a strange mistress, Leb. Just this morning I was going through old boxes of books and found an article on the La Villette project, and my mind wandered back to my early student days in Syracuse, and I recalled the story I heard at the time about why Rem lost and Bernard won. To paraphrase, Mitterand was a brilliant politician, but a bit of a lightweight intellectually, and he could understand Tschumi's 3 layered scheme, whereas OMA's 5 layers confused him. 


Best wishes to you, Leb!


Larry Turner
3. david
10.25.09 / 10pm


wow. I guess I am lightweight, intellectually.  

 I always “liked” Tschumi's design – but I would never say that I “understood” it.


I suppose 5 layers is more intellectual than 3 layers, since there are more layers, which sounds smarter!


to be honest, the schemes look somewhat similar except for Tschumi's fun red sculptures.
5. Pedro Esteban Galindo Landeira
10.26.09 / 5am


It is interesting to analyze that it had happened to the history of the architecture if this project had been constructed and that of Tschumi not.
7. Jos Bosman
10.26.09 / 10pm


Dear Lebbeus, your text on Rem's famous park design addresses the “twilight zone” between paper- and built architecture. You value the energy of vision at that time, as one that still today carries a challenge to become realized. From that point of view, and through your intense reading of the park design qualities, it may become clear that also in Rem's early built work there is a multi-layered challenge, which, even after it has been realized, keeps the characteristics of the “twilight zone”. Following your conclusion in relation to his paper work, also his early built work marks a utopian dimension within built practice, as drawn by Constant and the Constructivists earlier. I recognize this dreamlike quality especially in the realized twin housing tower complex in Groningen – at first glance, boring 60's or 70's modern architecture. But when being considered in their position within the city overlooking the canal (Eemskanaal), these towers form a complex that has a similar position as one of El Lissitzky's city gates in Moscow. In my inaugural lecture in Groningen, coming Thursday, I conclude with the image of a possible transformation of these housing towers into a Wolkenbügel: by placing a horizontal L-shaped lobby on top of them, from where to follow the straight line heading to the sea. It seems to me that in a next phase, the other Rem that you are conjuring up has – indeed – his chance to reemerge… in Groningen. Or in other words: the young Rem who started to build could not escape the law of the unfinished modern project, for which only the material of its founding myths is capable of revitalizing its practice “retroactively”. Best, Jos


Added on 6 November:  

My point was: The towers take in the position in a similar way as Lissitzky's Wolkenbügel for Moscow. Of course one needs to have the idea and ambition to see it like that (and I suppose Rem had such reading at the time). In fact: I am appointed as adviser for highrise in Groningen, so it is less abstract as how it sounds, in what I'm suggesting here… 





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.29.09 / 9pm
	
	
	Jos: Thank you for your comment, and I must say I find your faith in Rem very moving. Still, I find it unconvincing that the truly innovative Rem will reemerge in Groningen, with—as you describe—two boring modernist towers he designed in effect completed by you. Or, perhaps, I simply don't understand what you're saying. Is there a way for you to clarify?
9. [Lebbeus on Rem | no ideas but in things](http://www.quangtruong.net/?p=846)
10.27.09 / 6pm


[…] of the blogs I love getting a chance to read is Lebbeus Woods's. Today he posted a wonderful piece about Rem Koolhaas's  Parc de la Villette competition entry, accompanied by some wonderful […]
11. [Stephen Lauf](http://www.quondam.com)
10.27.09 / 9pm


Elia Zenghelis is also creditied as designer of the Parc de la Villette competition project.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.28.09 / 2pm
	
	
	Stephen Lauf: You are right, and your reference to this raises a very important point. Rem is a conceptualist who relies on other talented people to design his ideas. I don't believe I've ever seen a Koolhaas drawing or sketch (though they might exist—can anyone enlighten me on this?). I suspect his not being a designer per se is what gives his work a certain freshness of approach, more so earlier than later.
13. [Saurabh](http://www.urbanfloop.blogspot.com/)
10.30.09 / 7am


here is something I had written some time back:  

<http://urbanfloop.blogspot.com/2009/03/watchmen-and-architects-we-are-all.html>


ofcourse I agree with you, Yes it was another Rem…but it was also another time, when drawings as interventions seemed as powerful as the built…in the present context it difficult to be heard with all the ‘noise', so the only choice is to actually build the critique (eg. Rem's Dubai Renaissance)
15. [Lubbeus on Rem @ FreeRange](http://freerange.editkid.com/2009/11/06/lubbeus-on-rem/)
11.6.09 / 10pm


[…] Worth a quick read, I say! […]
17. [Cruz](http://www.wai-architecture.com)
11.8.09 / 4pm


O.M. Ungers said it in his interview with Rem and Hans Ulrich Orbist: “There is a great misunderstanding among architects. They think they are inventors and always need to be avant-garde. But you cannot permanently exist as an avant-garde. That is impossible.”  

I think that this statement draws a clear sketch of the condition of the “avant-garde” architect. On one hand they take advantage of their condition of being intellectual leaders and having control of the mass media to draw clients and armies of young architects willing to sacrifice for their gods, and on the other are confronted with the sad reality of having to face a market that outlasts a discipline as slow as architecture. Koolhaas is the vivid example of this. And even when his office is recycling the shapes of the original projects, they don't have the intellectual fuel that makes them what they were in the 70's and 80's.  

If you go as commercial as ‘K' did it's hard to avoid the detriment of the original ideas, unless of course you do like Corbu, and reinvent yourself one time after the other, even when you end up contradicting your strongest urban and architectural statements.  

Anyways, more or less related to this discussion, we are writing some research based on contemporary architecture, maybe Mr. Bosman and Mr. Woods find it interesting:


<http://waiarchitecture.blogspot.com/2009/10/what-about-understanding-contemporary.html>
19. [Jack Self](http://www.millenniumppl.blogspot.com)
11.9.09 / 10pm


On the subject of Rem, I saw [this](http://dornob.com/swimming-cities-floating-trash-or-modern-pirate-treasure/) that made me think of that floating island in Delirious.


Oh, and one of your old students – [Mas Yendo](http://masyendo.org/) – is exhibiting in [London](http://www.bartlett.ucl.ac.uk/architecture/events/lectures/lectures.htm#6), how do you feel about his work?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.10.09 / 12pm
	
	
	Jack Self: Mas Yendo is a very creative architect who has made an impressive, provocative group of projects over the years. Your comment prompts me to make a post on his work. Thank you.
21. [Landscape Architects – Latest Landscape Architects news – Landscape Architecture – October 2009 » AvaxDownload Ultimate …](http://ANNARBORLANDSCAPEARCHITECTS.INFO/landscape-architects-latest-landscape-architects-news-landscape-architecture-october-2009-avaxdownload-ultimate)
12.16.09 / 12pm


[…] ANOTHER REM « LEBBEUS WOODS […]
23. [Luca Guido](http://lucaguido.wordpress.com/2010/11/24/366/)
11.24.10 / 11pm


[…] Woods, nel suo interessante blog, ha pubblicato nell' Ottobre 2009 una breve riflessione su Rem Koolhaas. Rivolgendo la memoria […]
25. [Maintenance as a Key Sustainable Design Component | eatcology](http://www.eatcology.com/archives/103)
10.8.11 / 3pm


[…] from Rem Koolhaas's early 1980s Parc de la Villette competition entry (see Lebbeus Woods's write up for the background) was the maintenance regime- how were those stripes of planting and park going […]
