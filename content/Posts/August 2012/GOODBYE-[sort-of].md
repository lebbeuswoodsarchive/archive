
---
title: GOODBYE [sort of]
date: 2012-08-11 00:00:00
---

# GOODBYE [sort of]


The days of regular posting on the LW blog are over.


There are several reasons for this. For one thing, at my age and stage of life, with various health and other issues, my time and energy are limited. For another, I have begun writing a book that soaks up what time and energy I have for writing. It is not a compendium or collection of earlier work, but rather a new piece of work in itself, having to do with how World War Two shaped the architecture of the later 20th century. The ideas are fresh and the writing brand-new. Oh, and it's not a picture book. I've never done anything like it.


From time to time, I will post an article on the blog. I certainly don't expect readers to check into the blog regularly, as they have been able to do over the past four years. Readers who subscribe to the blog will get these occasional postings.


I must say that it has been a privilege to have communicated with so many bright and energetic readers. It has been a unique experience in my life that I will always value highly.


Thank you for all you have given.


LW



 ## Comments 
1. [kkoense](http://kkoense.wordpress.com)
8.11.12 / 11pm


Professor thank you everything. I continue to enjoy your work and look forward to this next endeavor. 


Enjoy. 


Ken
3. [gerri davis](http://gerridavis.wordpress.com)
8.12.12 / 12am


Thank you Lebbeus – I will miss your posts and await your book!
5. [straykatstrut](http://straykatstrut.wordpress.com)
8.12.12 / 12am


Thank you. I wish you well on your endeavor! ^\_^
7. wildoo
8.12.12 / 3am


I have come late to your blog but have enjoyed it thoroughly, sir as you have so very much to offer (at your state of life), and therefore I will be even more receptive of any word/thoughts you care to share. Best of luck with the book –I'm certain it will be a good read. . .  

Thank you for the stimulating thoughts and concepts. And the best of good health to you.  

Will Doolin
9. Pedro Esteban
8.12.12 / 4am


…  

I learned architecture here, just looking at the images sometimes getting a bit obsessed with them  

I could not understand the texts even I read them so many times! when I was 19  

What I enjoyed the most was that sensation of support I found in this blog when I was studying architecture in Cuba, where people could not understand that architecture is something very profound  

I say that I don't belong to any school but if I have to name one it is:  

<https://lebbeuswoods.wordpress.com/>


Thank you Prof Woods


this is the best school of architecture one student can have in this moment!! we can discuss a lot about this idea…
11. [danielamonaco1](http://gravatar.com/danielamonaco1)
8.12.12 / 6am


This blog it was for me a new way to drink to the best font of inspiration and knowledge. I hope to read soon your next book. Take care of yourself. Thank you Lebbeus, Daniela
13. [ekkehard rehfeld](http://gravatar.com/rieavico)
8.12.12 / 9am


from 1993 to 1999, from ‘reconstruction and resistance' to RIEAvico, never again did architecture make so much sense to me. except occasionally following your blog. i look forward to your book, lebbeus.  

ekkehard rehfeld
15. [citacionista](http://citacionista.wordpress.com)
8.12.12 / 10am


Reblogged this on [Citacionista](http://citacionista.wordpress.com/2012/08/12/178/) and commented:  

Um adeus virtual, que é um até já, e a promessa de “great things to come”. Lebbeus Woods é sempre uma fonte brilhante de inspiração e reflexão. O espaço (virtual e factual) não são, para mim, os mesmos desde que o descobri.  

Esperando pelo seu próximo “not a picture book”.
17. Lorenzo
8.12.12 / 11am


Thank you Professor Woods. I will miss reading your posts! Good luck on the book!


Lorenzo Bertolotto
19. [emmanuele pilia](http://www.piliaemmanuele.wordpress.com)
8.12.12 / 12pm


Mr. Woods, thank for your work, and for your blog.
21. [Michael Phillip Pearce](http://www.facebook.com/covenleader)
8.12.12 / 11pm


Best of luck LW and I look forward to your new book. This blog is a book in itself! mp
23. mbachic1
8.13.12 / 5am


i came across your blog while in architecture school and have continued to follow as i begin my architecture career. thank you for the ability to be inspired.
25. [Individuos opacos](http://gravatar.com/opacos)
8.13.12 / 8am


Thank you for your inspiring work.
27. [Individuos opacos](http://gravatar.com/opacos)
8.13.12 / 8am


Thank you for your inspiring work
29. Anthony Titus
8.13.12 / 12pm


Thank you for the years of much needed and desired inspiration. It is sad to see the regular postings come to a halt, but I take solace in the idea of ‘sort of'; as well as the release of the book. I believe the book will offer a unique and fresh perspective on a moment in history that still seems so unreal and elusive. Looking Forward//
31. Andres Souto
8.13.12 / 2pm


On the contraire. Thank you for making it possible to connect with a personality like yours in this way. You are an inspiration.
33. David Reid
8.13.12 / 3pm


Mr. Woods, As a student graduating in1990 from a midwestern university I found your images startling, consuming and inspirational. I still strive to understand them and infuse them in my thinking. Your writings and drawings have something honest about them that I find refreshing. The only pretense seems to be that you dare believe in the strength of an idea and that those ideas can be realized. It will be fantastic to glean from your future book and posts as often as you care to do so.
35. [Maurits](http://www.applied-architecture.com)
8.13.12 / 6pm


Thank you for the inspiring and thought provoking articles. 


I am looking forward to the book. One thought: might WWII have triggered our current obsession with tabula rasa developments?


All the best.
37. [İpek Ek](http://gravatar.com/fatmaipekek)
8.13.12 / 6pm


“Hergün bir yerden göçmek ne iyi [How good to move everyday from a place]  

Hergün bir yere konmak ne güzel [How nice to settle everyday down a place]  

Bulanmadan donmadan akmak ne hoş [How fine to flow without being frozen and blurred]  

Dünle beraber gitti cancağızım [My soul has gone with yesterday]  

Ne kadar söz varsa düne ait [All words belong to yesterday]  

Şimdi yeni şeyler söylemek lazım [Now is the time to say new things]” 


Mevlâna


Your aim always seems the same… I wish you great “new things”!
39. Arash Basirat
8.13.12 / 11pm


پروفسور وودز عزیز امیدوارم هرجا هستید موفق و پیروز باشید  

بی نهایت قدردان متانت و صبر و استقلال عمل و رای تان هستیم
41. Uri Wegman
8.14.12 / 1am


Lebbeus- Thank you for the incredible platform which you have offered us in the last couple of years. Its been a true source of inspiration and education – It will be badly missed but the energy will continue to permeate in the future. Looking forward to the book. Best of luck with it!
43. [R MacTague](http://www.rmt-a.com)
8.14.12 / 9am


Missed your lecture at the Bartlett as you had broken your leg!  

Thanks for all the inspiration and look forward to much much more, esp. the book.  

Take care.
45. Jean Paul Sara
8.14.12 / 10am


Thank you Professor Woods,


 Your Blog has given me the confidence to move forward in my studies and pursue my MA at European Graduate School. Will it be possible to study with you there in the coming few years?
47. [Tom Brooksbank](http://www.coworkscreative.co.uk)
8.14.12 / 5pm


Thank you so much for the inspiration and education you've given through this space.  

I hope we'll be able to continue reading and rereading the body of posts already here for some time yet.  

Keen anticipation for the coming book…
49. [metamechanics](http://www.metamechanics.com)
8.14.12 / 5pm


You mean I have to buy books and go to the library again for earnest intellectual reflections on architecture and related philosophy?  

thank you for all the posts and look forward to the book.  

– Chris
51. Kojiro No-se
8.15.12 / 11am


ありがとうございました、ウッズ先生。Thank you Professor Woods.
53. [Rand Pinson [Romulus]](http://www.lakeflatoporchhouse.com)
8.15.12 / 11pm


This has been a pleasure of mine every time I see a new posting and frequently forward posts and related articles to colleagues and other design enthusiasts. It was refreshing to read each time and i admired the format in which critical observations/comments/writings were shared with us. Its something I looked forward to. I will continue to subscribe in hopes of more posts and like all, look forward to the book.
55. [matthewdarmourpaul](http://matthewdarmourpaul.wordpress.com)
8.17.12 / 2am


Thanks so much! Good luck with the book.
57. david gersten
8.17.12 / 1pm


Dear Lebbeus,  

You are one of a rare breed of human birds, Big wings, big heart, flesh and blood and DEPTH, HUMAN DEPTH…. As you know, I deeply value our friendship. Beginners interested in beginnings….say what we mean and mean what we say…..always moving forward, non stop creative navigation, non stop questioning….non stop work….non stop challenging stupidity….Non Stop…..While I am not the most skilled on-line navigator, your blog was one of the places where i found shelter, the closest i ever got to feeling that this place (internet) could actually convey something of our human texture of our strength and frailty….this no doubt is a direct expression of the power of your voice, your thought….FULL FULL….I have never ‘posted?' or ‘commented?' on a blog, but having seen your Goodbye (sort of) I thought to mark the occasion in my memory by making my first comment….Celebration and Longing…Hope and Despair…..you have transformed lives Lebb and for that we are ALL so very much grateful…..See you soon Lebb, see you soon.  

Love,  

David Gersten
59. Mikael Pedersen
8.20.12 / 2pm


Dear Lebbeus  

thank you for your generous time and effort to share your thoughts over the last years. I will dearly miss your insightful comments and valuable reflections of current and (past) projects and the widened perspective you put your thoughts into. For me, following your blog, has been like sharing the experience of discussing architecture during morning espresso in Taormina before going into action in the studio, pumped with energy and inspiration. Best of luck and I look forward to your next project. Take care.


Mikael
61. Ravneson
8.21.12 / 4pm


Thank you and the best of luck with your continued practice!
63. DKZO
8.22.12 / 6pm


Professor Woods, You will be highly missed. You've been my homepage for a long time now. In my family we never say goodbye – so- So Long, Sort Of. And Pedro really could not have said it better. You are so much more. ❤ Looking forward to your future work always.
65. [General](http://www.general.la)
8.23.12 / 9am


Captain, My Dear Captain… may your enterprise be matched with the success of your potentia magnified by its influence.. the grandeur of time's placement of your works in the timeless history to which you now exist. We are your admirers as well as your blossoming seeds : this bloodline of inspiration to which you have represented now travels through the veins of our beating (he)artistry as the pure motivation that once captured our undying attention… you are the power that creativity decided to preside over the mastery of spirits representation… to materialize the geometric excellence of anti-nature as natural selection. Be with us, or be with us no longer…. the consequence is still the same: infinite gratitude and re:direction of the architecturaltruism towards more meaningful existence. Confluence, and fluid ambiance… as radical as reconstruction, may your next codex code us with coaxed culmination cultivating soul motivation to exist in more visual context… of a purely image-in-nation's complexion.. or a vision of wisdom's sensation as driven.. to be the invisible made visual: creation.
67. [samanthaff](http://marinatingelephants.wordpress.com)
8.24.12 / 2am


Professor Woods – you always have been and will be the ‘call' – ‘for architects to have ideas and on occasion new ones'. Your challenge is remarkable, necessary – thank you sincerely for giving of yourself to the world and I wish for you the best of all things. Warm regards, Samantha
69. CCKL
8.24.12 / 5pm


Woods not out of the woods yet.  

For you, from p.43 : 


Relax, Said the Lightman,  

Architecture's programmed a reprieve.  

You can check out any time you like,  

But you can never leave.
71. Jeff Brown
8.25.12 / 2am


…ready for a change….bored of this trench i have unconsciously dug..feel like i have been dead way too long……..time to wake up…….are we all already dead and just don't know it?….tired of commercials, politicians, bad conversation,…..anyone feel the same?  

I remember taking a rigid frame shovelhead that i just rebuilt up the Henry Hudson- cross bronx expressway(really rough road) to  

the saw mill parkway where I was passing all kinds of BMW's, mercedes, etc., when I felt like I finally broke through all the traffic, since there was no more cars around me…..when all of a the bike shut down. My license plate bracket which had my tail light on it had broken off. The bike had shorted out and I was forced off the road. I got off the bike and went for my tools in the bag under the frame and noticed they were all gone. I realized then that the reason there were no cars around me was that the tools were falling out after the cross bronx expressway and probably hitting the cars around me….which is probably why I felt alone on the sawmill…….hope it wasn't any of you. Any way, other bikers stopped and asked if i needed help. I asked if they could go back an retrieve my license plate. They did and came back with it wadded up in the broken taillight with a mercedes green paint on it. So, I pushed the bike to a bar in Elmsford, NY and ordered a pint of guinness. Someone there gave me a ride to a harley shop to get some fuses and a battery. …I got back to NYC.  

a month or so later,  

I get into yale (god knows how or why they gave a full scholarship) and I am in this class with a bunch of fancy sweater folks and I raise my hand to answer a question by the prof. He chooses someone dressed nicer (or comes from a wealthy family). I keep at it and on one occasion the prof. praises one paper he received…it was mine. Then,  

during a studio review after presenting my work, I was asked if I studied Architecture before I came to yale. I said yes at Cooper Union. I was told “Well, we don't do that kind of work here.”  

I suggested that since we are in an academic environment that the job of the critic is to educate through questioning and not direct that kind of response, instead offer comments to the work at hand… And that if you don't understand what is before you, ask more questions, in order to establish a dialogue, otherwise, it is you who does not want to engage in the work….that went no where….so, I took my work down …pissed off they picked up their chairs and moved on. Later, I was asked to have the work for the end of the year publication…..I declined.


….I guess it is time to get the work out. Jeff Brown
73. Jeff Brown
8.25.12 / 2am


oh, and by the way…. you had better write soon……we need you. (“no sleep til Brooklyn”)….you got that Otterbein?!
75. Jiri Boudnik
8.28.12 / 10am


Dear Lebbeus, I am a Cooper grad (Architecture 97) and have been living and working as an architect in Pilsen, Czech Republic, for the past 4 years. I have put together a conceptual architectural design competition for the second tower of a gothic cathedral of St. Bartholomew in Pilsen. One of my motivations was the 1980 publication of “late entries” for the Chicago Tribune tower, for which you designed your own tower. I would be happy if you (or some of your students) could participate in this competition as well. The first price is 2.000 Euro and there are not many chances to contemplate a second tower for a gothic cathedral these days. 


I understand that you are spending all your time on your book now, but consider this competition as a vacation from writing. 


Please see this site <http://www.zijinak.cz/dveveze/home> (dve veze means “two towers” in Czech) and if you have any question, please write. Thank you and good luck. Jiri Boudnik (AR97)


P.S. if any other architects or designers are interested in taking up the task of designing a second tower to a gothic cathedral, please write to me: [jiri.boudnik@gmail.com](mailto:jiri.boudnik@gmail.com)
77. [sarahhalford](http://gravatar.com/sarahhalford)
8.31.12 / 9am


It is a privilege to read your blog posts, after enjoying your formal published work so much. It's really been a treat to see the new ideas you're thinking about, thank you so much for taking the time to write all the posts. You continue to inspire, and I look forward to your next book.
79. [Zach Emmingham](http://architecture360.net/)
9.3.12 / 6pm


Dear Professor Lebbeus,  

I will always remember the brief summer studio that you taught our Cornell class (summer 2010) and I thank you for posting all of the student's work from our final review. By publishing our work on your site, you've helped our careers and I hope you can keep the site running. During the studio, you were able to inspire us to work together and produce great, original work. You did this not so much by your words, but by leading by example.  

I've recently moved out of NY and on to the next stage of life, but I hope to always be able to go back to your past and future writings and think about the creative possibilities of architecture.  

Good luck with your book.
81. [Cody Derra](http://codyderra.tumblr.com)
9.11.12 / 4am


Thank you sir.
83. [Lawrence Lek](http://www.lawrencelek.com)
9.17.12 / 1am


Thank you Professor Woods!
85. allan areano
9.21.12 / 2pm


Thank you professor Woods -no words can describe how grateful i am to you.  

Greetings from Honduras
87. [scott](http://gravatar.com/jokermtb)
9.27.12 / 3am


I find it ironic that I discover this blog, only to have it recede……Anyway – I look forward to your new book.
89. Lior Galili
9.30.12 / 3am


Dear Lebbeus,  

Thank you so much for your generosity. Your work is a precious encompass for me.
91. Oscar Reyes
10.9.12 / 12pm


hi Prof. Lebbeus, I am a profound admirer of your work and experimental thinking. Look forward to read the next book.
93. [kkoense](http://gravatar.com/kkoense)
10.30.12 / 5pm


I am at a loss. Your work and words have left an indelible mark upon my soul. You sir, will be missed beyond my capacity to express them here. Thank you for being a voice for those under siege.
95. AAscapes
10.30.12 / 9pm


We lost Prof. Woods, but his work will stay with us for ever!!!
97. [WM](http://www.asasku.blogspot.com)
10.31.12 / 3am


Goodbye Sir. You are truly an inspiration that shatters our limitation of Architecture itself.  

RIP.
99. FJ
11.3.12 / 10pm


Too Soon. Ciao Bello. 


Flavin
101. [Nico](http://microgeography.blogspot.com/)
11.5.12 / 6pm


Go in peace mr. woods. You've been an inspiration and a great challenge, I will continue to delight in struggling with your work and thoughts.


I hope some kind friend is able to keep this great archive of your thoughts and drawings online in perpetuity. It will be a great resource to us all.


I am also saddened that you did not live long enough to see your retrospective at SFMOMA completed. I do hope it will bring greater attention to your work.


NW
103. [Oliver](http://baufunk.blogspot.com)
11.18.12 / 6pm


I walked down Hyde Park Corner this day, thinking of something Lebbeus had said to me a few months ago: shouldn't we think of what comes ahead of us rather than what has been? Those words stuck with me, because they teach me what it means to imagine. Lebbeus was like that, he could inspire people with a single thought, regardless of whether that thought was written, said, drawn or built.


See you at the crossroads, Leb.
105. dkzo
4.17.13 / 10pm


i miss you lebbeus
107. [Francisco Vasconcelos](https://www.facebook.com/csxlab)
8.16.13 / 3pm


Miss this brilliant man.
109. BinMar Leto
5.9.14 / 3pm


Lebbeus Woods…Visionary Humanist and Visualist…we have yet to grasp the totality of his imaginations
