
---
title: DARKNESS ASCENDING
date: 2010-04-18 00:00:00
---

# DARKNESS ASCENDING


April 17: Over at [BLDGBLOG](http://bldgblog.blogspot.com/2010/04/continent-scale-weather-systems-made-of.html), Geoff Manaugh writes of the ash cloud spreading from the erupting Icelandic volcano across Europe and, undoubtedly beyond, in terms of a science-fiction scenario come true. He is certainly right about that, and it does not take much imagination to realize, as the volcano continues to erupt and the ash cloud continues to expand, that more of Europe, the Middle East, and northern Africa might be affected. Air travel will shut down, no one can say for how long, and with it the vital movement of people and goods. This is more than an inconvenience. After only three days it is already a disaster for travelers from all parts of the globe, their families, their businesses, and for airlines and the recovering European economy. Worse yet, it is another blow to human pride—following on the spate of recent earthquakes—and our sense of control, if not over nature, exactly, then at least over our own affairs. The ash cloud shows us how fragile our technological systems are, and therefore our present civilization, particularly when confronted by nature's altogether natural convulsions.


We cannot stop the volcano's eruption, or the resulting ash cloud, its expansion or devastating effects. We can only hope that it will all pass soon—which it may or may not—and we can get back to normal. Perhaps, this situation tells us, we should also be rethinking our idea of the normal. Who can say whether our increasing global dependency on technologies that are increasingly vulnerable to nature's inevitable transformations will not become the new normal. If that is to be the case, then we had better consider a radically new relationship to them.


(below) Graphics from the [New York Times article](http://www.nytimes.com/2010/04/18/world/europe/18ash.html?scp=5&sq=ash%20cloud&st=cse), April 17:


[![](media/ashcloud1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/ashcloud1.jpg)


updated April 18, and [London Times article](http://www.timesonline.co.uk/tol/travel/news/article7101162.ece), dated April 18:


[![](media/ashcloud2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/ashcloud2.jpg)


latest report on CNN: <http://www.cnn.com/2010/TRAVEL/04/18/volcano.ash.test.flights/index.html?hpt=T1>


[![](media/ashcloud-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/ashcloud-31.jpg)


Updated, April 19. What these maps are not showing is the ash cloud spread across the Atlantic or into Africa, the Middle East, and Central Asia:


[![](media/ashcloud-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/ashcloud-4.jpg)


Updated, April 20. The erupting volcano is more active today, adding fresh concentrations to the ash cloud.


[![](media/ashcloud-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/ashcloud-5.jpg)


What we are not hearing about in the mainstream media is the extent of fallout of ash and other particles from the apparently thinning cloud.


An article on the human and economic impact in Kenya: <http://www.nytimes.com/2010/04/20/world/africa/20kenya.html?hp>


LW



 ## Comments 
1. [seier+seier](http://www.flickr.com/photos/seier)
4.18.10 / 8am


the argument for high-speed trains no-one saw coming…
3. [Cold Icelandic Ash and Ahmadinejad's Hot Ash « korzacsol](http://kafee.wordpress.com/2010/04/19/cold-icelandic-ash-and-ahmadinejads-hot-ash/)
4.19.10 / 7pm


[…] Headlines  like Lebbeous Woods's ‘Ascending Darkness‘ addressing the ash crisis and alluding to the SF series Babylon, fit well into more biblical […]
5. Ajay Manthripragada
4.25.10 / 10pm


Perhaps worth noting is the sequential pairing of this entry with the last. Intending to see the entry on Goya entitled “Terrible Beauty,” an accidental click on “Darkness Ascending” led to these arresting images. The headings might be interchanged to equal, even pronounced, effect.
7. Steven Pitera
5.1.10 / 12am


When my brother and I were stranded in Prague on April 16th, we had planned to ride a train to Paris with a transfer in Offenburg. We had hoped to get a high speed train out of Paris into England (where my brother is studying). But the point of this was to say that during our very bumpy journey, we noticed a total lack of respect to the fact that people stranded had no connection to real-time communication and facts. Yes many people had iphones with internet connection but there was no attempt at every major train station to provide the people with an up to date fact list of the events unfolding.  

As an example, we arrived in Paris on April 18th and headed to Gare du Nord to purchase “Chunnel” train tickets. The train was booked till Thursday with only business class tickets worth up to 400 euros. No mention of the trains being booked on the websiteof the company running the trains. We then decided to book an intercity train to Calais to catch a ferry. With that we waited on a line to purchase the tickets and see what information the person working the ticket booth had. When we reached the front, she told us we could take a train to Boulogne-sur-Mer and catch a ferry run by the company Speedy Ferry to Dover Port. Excited we ran back to our hostel to check online and book a spot on the ferry. The company that ran the ferry lines ceased to exist as of 2008. We figured out that another ferry service ran that line and it was not accepting foot passengers.


It was very frustrating to see our connection to updated and real-time information just vanish overnight. At places like the train stations, a ticker announcing all failures and successes would have prevented many people from following rumors that lead to dead-ends.
