
---
title: PLAYING GOD
date: 2010-04-27 00:00:00
---

# PLAYING GOD


[![](media/taut-1a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/taut-1a1.jpg)


(above) Alpine architecture, by architect Bruno Taut, c. 1918


The Expressionist architect Bruno Taut envisioned, a century ago, the reconstruction of the Alps into a crystalline architecture that would bring the human world into closer harmony with the forces and processes of nature. His work on this project, while admired by a few, was widely derided and dismissed as foolish and absurd, at the very least an overestimation of the power of architects and of architecture to reshape the world for the better. Even worse, from today's perspective, reshaping mountain peaks into glittering, glass cities, is not only a travesty of nature, but also an ecological disaster. Still, we must admit that anytime an architect alters a natural site, or builds using materials taken from nature, he or she is ‘playing god,' that is, rearranging the natural order of things. Taut's sin—and my own, below—is only a difference of scale, not of substance, from what architects do every day. By dramatizing the human necessity to alter nature, such mega-projects confront us with an essence of the human condition.


[![](media/lwblog-end-2c1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-end-2c1.jpg)


(above) Cosmic reconstruction.


LW



 ## Comments 
1. [organicmobb](http://organicmobb.wordpress.com)
4.28.10 / 8am


I can see parallels with Taut's work and the growth of the suburb. Both cases have deal with the desire to use a natural system to benefit mankind.The suburb using a ‘natural planning' approach, that in which sets up a central armature and so forth, and Taut's desire to use the elevated natural groundplane of the mountain. Take Long Island's Levittown for instance or the planned housing communities just outside of Rome. Both of these suburban typologies were created post war and are now collapsing due to over crowding and the inability to cope with changing infrastructural demands. Seemingly perfect planning has now turned back on itself. The idea of man controlling ‘nature', or natural growth in the case of the planning of Levittown is one that we as architects need to and will continue to contend with. 


Great post Lebbeus, and I really like the title, spot on!


Dave
3. david
4.28.10 / 4pm


What a rich subject…I wonder how Taut's idea tied into his later life and work in Japan. The Japanese attitude towards landscape is similarly conflicted. Arguably, no culture literally idolizes nature more than the Japanese, and no culture has urbanized to the degree that the Japanese have…
5. [organicmobb](http://organicmobb.wordpress.com)
4.28.10 / 8pm


The japanese have influenced so many cultures. Purity comes to mind.
7. Nathan
5.2.10 / 11pm


If the human ability to alter nature makes us* god-like, then beavers, birds, ants, termites and just about any other species on earth is as equally god-like as humans are. Humans do not rearrange the natural order of things, we are the natural order of things. Everything we do is natural. To think we are separate in any form is, I believe, the cause of much of the destruction we see. Consider the word ‘nature.' It would be an interesting study to look at numerous indigenous groups all over the world who live in extremely close contact with ‘nature' and find out if they have a word as destructive as ‘nature.' ‘Nature' separates people from their environment. We consciously labeled it as ‘other,' when in fact it is inherent in absolutely every space we inhabit. The word ‘nature' is terribly artificial. If we must continue to use the word, and worse, the idea, then I think its appropriate at least reconsider what it is that we're actually doing, or should be doing. According to one persons opinion, “Architecture is love applied to nature” (I have lost the source of this quote).


* Humans or architects? In any event, the relationship between gods and architects is already too entrenched and is dangerous to continually propagate.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.5.10 / 1pm
	
	
	Nathan: You make your point eloquently, and I will grant it, up to a point. Certainly we humans are part of ‘nature' and within that broad framework everything we do is therefore ‘natural.' However, the distinction between what humans do and what other species do is useful, because at some point the quantitative becomes qualitative: beavers and termites do not have the capability of destroying other species, by, say, pollution or nuclear war, or altering the entire ecosystem of the planet or, perhaps, destroying it. Humans remain unique among other creatures, and must take unique responsibilities for the rest of ‘nature.' I don't think anyone would reasonably consider, for example, the human pollution of the earth's atmosphere as ‘natural.'
	
	
	
	
	
		1. Paul Anvar
		6.9.10 / 9pm
		
		
		Lebbeus thanks for the great post.  
		
		I think Nathan hits on some interesting points but I would also add that as is depicted in “Cosmic reconstruction”, Nature does not stop at the blue rim of our atmosphere. Given the insignificant impact radioactive bombs and carbon emissions have on interstellar nature I wouldn't worry too much about it. Besides if we blast our planet to oblivion it could be seen as a Darwinian move that the entire universe would benefit from. I look forward to crystal mountain tops!  
		
		Best regards,  
		
		P
9. Cory
6.2.10 / 5pm


I think that the idea of nature is linked to notions of balance, not as a state, but constantly changing. we exist in nature, and so are part of it, but the scope of our imbalance is far beyond any other species, and in the same way that our habitat is far beyond any. There is very little of the planet we haven't inhabited. In nature if a species gains too much advantage within its habitat its population grows because the species is doing well, however, if the species grows too much, it runs out of resource and the population plummets, or its area of habitat grows because it can find the resources it needs elsewhere. i think that this, too is true for humans, and we've run through most of the habitat and resourses on the entire planet. what we haven't done is figure out what the balance is.
