
---
title: SLUMS OF NEW YORK
date: 2010-04-08 00:00:00
---

# SLUMS OF NEW YORK


[![](media/riis-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-7.jpg)


*(above and below) Tenements on the Lower East Side of New York in the 1880s. Photos by Jacob Riis.*


Social housing, or what we in the United States call public housing, means government supported living spaces for those unable to pay free-market rates for them, and it is rooted in a humanistic idea of society. There are particular human rights that are not merely the privilege of the well-off and the well-connected, but are the basic rights of all people, by virtue of their being human. One of these is the right to a place within their community as secure as possible from harm.


The history of this idea goes back to human beginnings. It seems, from what paleontologists have discovered about the remotest human ancestors, that a distinguishing characteristic of humans, as opposed to other animals, is their impulse to care for each another. Most animal species isolate and abandon their wounded or weak members, leaving them as prey to predators who attack the weak first. Excavated pre-historic human skeletons showing evidence of injuries or wounds also show evidence of attempts to treat them, with the obvious intent of restoring an injured person's faculties and capacities.


Compassion—the feeling of another's suffering as though it were one's own—is not a merely sentimental add-on to the human psyche, but rather a part of basic survival instinct. Individuals survive only if their community survives, and the community survives only by the concerted effort of all its members. This is another way of saying that I will do well only if you do well. Some theorists have called this ‘enlightened self-interest,' and it works on both practical and metaphysical levels. As social creatures. we need each other emotionally, and also to assemble the diverse skills needed to perform the complex tasks that are distinctly human, such as the making of science, art, commerce and trade, farming and industrial production. If we are to succeed in these ventures, we must take care of our own. Not the least part of this caring is the securing of a physical place for each person within the communal structure—the landscape of the city—-that enables all to live with the dignity we need and deserve.


Jacob Riis was a immigrant in New York City, in the latter part of the 19th century, whose distress over the desperate living conditions endured by the poor took a pro-active form. Using the then-latest techniques, especially the invention of the ‘flash'—he photographed for the first time the dark and squalid interiors of New York's tenements, exposing to a wide public an appalling degradation of human life. The publication of these photos in newspapers and books in the 1890s, combined with a campaign of public lectures by him and a few colleagues, raised public consciousness of the inhumanity hidden from view at the heart of the prosperous city, and contributed eventually to the public housing movement, wherein the taxes paid by the well-off would support those struggling for survival at the outermost fringes of society.


The poor shown in Riis' photographs are not loafers and indigents, but working members of their community, providing essential—if menial—services, working to make better futures for their children, fulfilling the “American dream” that brought them to this fabled land of opportunity. Crowded together in small, dark rooms, where they slept, ate, shared communal bathrooms, and also did laundry for others, repaired clothing and household utensils, or simply found respite from hard manual labor, the people of the tenements evoke respect as much as pathos. They are the durable substance of an America just being born from waves of immigrants who have come to make a better way of life. The manner in which they dress shows their inherent self-respect, and their aspirations. Their faces betray not a hint of self-pity. They bear the exploitative rents imposed by their landlords with a calm reserve reflecting their faith in the future.


Public housing in America began during the Great Depression of the 1930s and really took off after World War II, when a super-prosperous America—the last-standing great industrial power—under the leadership of elected politicians, decided to make good on its promise. Following the design principles espoused by modernist architects such as Le Corbusier and Aldo Van Eyck, who placed their hope in enlightened government, tax-financed public housing ‘projects' were rapidly built in most American cities.


But then something happened. The super-prosperity ended and new enemies and threats to the community arose in the American psyche. The will to maintain existing public housing, not to mention improve it, subsided, and decay and public disillusionment set it. The root cause of America's slums—economic disparity between the rich and the poor, which had been addressed by successive governments from the 1930s through the 1960s—was increasingly ignored and, finally, exacerbated, under the triumphant banner of ‘liberal democracy' the ‘free market system.'  Today, public housing is barely mentioned. Schools of architecture do not study it because there is no longer the client—government—for its realization. The design and construction of new housing stock is left in the hands of real estate developers, who see no profit in building living spaces for those unable to pay market-rate rents.


Degrading living conditions still exist for very many in New York today, ironically behind the facades of the very public housing projects once considered an important part of the solution to the problem. It is not only that there is no contemporary Jacob Riis to expose them, but more so that there is no longer an America to be shocked into action by them.


LW


[![](media/riis-6copy.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-6copy.jpg)


[![](media/riis-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-3.jpg)


[![](media/riis-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-1.jpg)


[![](media/riis-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-9.jpg)


[![](media/riis-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-8.jpg)


[![](media/riis-10jpg.gif)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-10jpg.gif)


[![](media/riis-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/riis-5.jpg)


New York today: vestiges of the past:


[![](media/slumsny-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/slumsny-1a.jpg)


Public housing in New York today—the slums hidden in plain sight:


[![](media/pubhousing-1c1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/pubhousing-1c1.jpg)



 ## Comments 
1. [SLUMS OF NEW YORK « superbalanced](http://superbalanced.wordpress.com/2010/04/08/slums-of-new-york/)
4.8.10 / 11pm


[…] SLUMS OF NEW YORK via lebbeuswoods.wordpress.com […]
3. [Martin Taurer](http://livingroomcraftz@mac.com)
4.10.10 / 6pm


What is mentioned above as having happened to the American psyche post 1960 unfortunately spread like a disease across many western countries, probably fueled by a never ending American dream rolled out from Hollywood… neo-liberalism and free market antics of the last 2 decades of the 20th century destroyed public housing in many European counties as well (fringe exceptions such as Austria though still exist)…  

It is a sad assessment that there is no America anymore that can be shaken into action by Riis pictures – worse than that is that is that I expect the common western delusion of never ending affluence has eradicated the altruism that was the base of social movements (and social housing) between the 30-ies and 60-ies of the last centuries…  

I rather would like to hope that we can be able to re-learn humanitarian behavior
5. Linda Nochlin
6.3.10 / 9pm


I think Riis's photos are revealing and compassionate. But they are of their time. They still objectify poor people: we are not made to feel that they are active subjects: they are “other.” Not “us”. Riis's poor children are pathetic, and rightly so. But I think of Helen Leavitt's poor children: active, mischievous, living as best they can in the urban fabric of their times, giving a sense of potential activism for change by the poor themselves.
7. Rat Fink
9.2.10 / 7pm


In a couple of hundred years those images might seem decadent to NYC's poor. When you grow the way we do and devour at the levels we do things don't get better.
9. Tima
1.27.12 / 7am


Were not very far off from witnessing 21st century slums ourselves, just look at Detroit or New Orleans. 


FACT: Over 1 billion people worldwide live in total slums, thats 1/6 of the world population. I expect that number to drastically increase in the coming years, slums “redevelopment” is not able to catch up with migration to cities and increases in human population of the earth. (especially in “developing” countries)
