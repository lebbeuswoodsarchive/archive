
---
title: TERRIBLE BEAUTY
date: 2010-04-13 00:00:00
---

# TERRIBLE BEAUTY


[![](media/goya-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-2.jpg)


*(above) Title page from Goya's series of etchings entitled “Caprices.” The motto reads: “The dream of reason produces monsters.”*


Francisco Goya witnessed the horrors of Spain's war with France at he beginning of the 19th century and could not stay silent. Instead of creating propagandistic art, extolling glorious military heroism, he focused on the atrocities of the armies, committed against ordinary people. He knew that when soldiers get into a killing craze, they murder and rape indiscriminately, often just for the hell of it. If there were an Iraqi or Afghan Goya working today, he or she would not make journalistic photos of the slaughter of people who just happen to be there, but would draw and paint it, becoming selective, ‘aestheticizing' the atrocities, in order to elevate them to a serious level of reflection. The artist does not merely present us with raw material, which is always difficult to confront and understand—indeed, it is easier to dismiss it with only a shudder—but instead creates indelible images, which cannot be gotten out of the mind. For all the risks of making human depravity ‘look good,' the human conscience needs such images in order to burrow deeply into their meanings and implications. Jesus of Nazareth nailed to a wooden cross. Goya's nameless victims of torture and dismemberment. Contemplating these images, we can never fall into a simplistic idea of our human condition.


LW


*(below) Etchings from Goya's series “The Disasters of War:”*


[![](media/goya-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-3.jpg)


[![](media/goya-71.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-71.jpg)


[![](media/goya-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-11.jpg)


[![](media/goya-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-10.jpg)


[![](media/goya-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-8.jpg)


[![](media/goya-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-4.jpg)


[![](media/goya-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/03/goya-9.jpg)



 ## Comments 
1. [Art Does What Photography Does Not, Cannot? « Prison Photography](http://prisonphotography.wordpress.com/2010/04/12/art-does-what-photography-does-not-cannot/)
4.13.10 / 3am


[…] Woods posted some interesting thoughts today about Francisco Goya's meditations on […]
3. [Isil](http://fadoows.blogspot.com)
4.13.10 / 1pm


Sueño=Dream, not sleep.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.13.10 / 9pm
	
	
	Isil: Thank you! It makes a big difference—more ambiguous and provocative.
5. [Damir](http://Jonfonya.com)
4.14.10 / 7am


t's funny I don't know what he means by this caption. The dream of reason produces monsters. There are two interpratations both meaningful: one is that the dreamer  who dreams within reason will produce monsters of his dreams. The other, more contextual to goya's experience through the Spanish war, is the dreamers who dream of reason can produce monsters of themselves in waking life. Maybe the mind needs monsters or lack or reason to stay sane. Dreamers that follow no reason act out their fetishes and devil may care acts within dreams. the reasonable dreamers will seek those same fetishes in awake life, it is only natural that a balance occurs.
7. [Michael Phillip Pearce](http://www.carbon-vudu.us)
4.16.10 / 3am


Iraqi or Afghan is authentic, but could the artist[s] be of another setting that is just as passionate about the outcries? What about a contemporary like BANSKY, controversial, charismatic drawings, and pushes the envelope? The medium has changed, as well as the languages of how we communicate through art in the rawest urban sense—BANSKY has nailed it in our time. Is he a Goya, that is up to you. 


Iraqi or Afghan is authentic, but could the artist[s] be of another setting that is just as passionate about the outcries? What about a contemporary like BANSKY, controversial, charismatic drawings, and pushes the envelope? The medium has evolved, as well as the languages of how we communicate through art in the rawest urban sense. [refer to link below] Is he a contemporary Goya I wouldn't know, but he inspiring in a crude controversial way! Thank you for drawings and wonderful reminder of Goya and what he said.


[![](media/2317420354_8efa5ce810.jpg)](http://farm3.static.flickr.com/2018/2317420354_8efa5ce810.jpg)
9. [Michael Phillip Pearce](http://www.carbon-vudu.us)
4.16.10 / 6am


Well my above post is a great example of no posting during martini time–the later is the actual post. I guess it is like a sketch in pen, the permanence is embedded.
11. d pearson
4.20.10 / 2am


It's important to compare the differences in the emotional distance between Goya and the Spanish War or Bansky and global capitalism. Bansky responds to a media culture and thus operates further removed from the tragedies to which he speaks. But he installs them into the urban space of thousands effecting them socially. I believe Goya responded more intuitively on a human level to events closer to his own perceptual grasp.
