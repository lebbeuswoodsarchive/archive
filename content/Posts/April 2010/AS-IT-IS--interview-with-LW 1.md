
---
title: AS IT IS  interview with LW 1
date: 2010-04-21 00:00:00
---

# AS IT IS: interview with LW 1


[![](media/lwblog-asitis-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-asitis-1.jpg)


(above) *The Fall* installation at the Cartier Foundation for Contemporary Art, Paris.


**Interview by Sebastiano Olivotto**:


*In the Cartier Foundation exhibition called “Unknown Quantity,” you were invited by the philosopher Paul Virilio to create an installation, “The Fall.” What is the basic idea of this work?*


The exploration of a space of radical transformation. Paul Virilio and I, in our different ways, share an intense interest in the changes brought about by technological innovation, by cultural and social upheavals, by natural catastrophes like earthquake and the social and architectural responses to them. I see these extreme cases as the avant-garde of a coming normality, one that we must engage creatively now, inventing new languages, rules and methods, if we are to preserve what is essential to our humanity, that is, compassion, reason, independence of thought and action. Paul, in this exhibition, confronts ‘the accident' as an integral part of progress. He wants to face its terrors as a way of transcending them. I respect that. For my part, I want to face the consequences of destruction in order to employ them in the service of constructive aspirations. This means engaging the consequences of destruction—whether from war, earthquake, neglect or abandonment—head-on, neither attempting to erase nor memorialize them, but rather to use them as the starting points for a new sensibility, a new basis for creative action. “The Fall” uses a hypothesis, not an actuality, in this way.


*Paul Virilio is the creator of the “Desparition” theory, and you started your works investigating how objects emerge from chaos and turbulence. These theories seem to be opposing and related at the same time, in the sense that both affirm the transience of reality.*


I can't speak to the theory of Virilio you mention. I must admit that monumentality and permanence were givens in my education in architecture, even in the late modernist period which was in the backwaters of modernism's social and political and technological turbulence. I've come to believe, from that experience and a lot of reflection, that modernism was a failed movement because it did not deliver on its promises of facilitating social change through a new architecture. Instead, it was classicism dressed-up in new clothes. The essence of the modern condition—the contemporary condition—is transience. Things pass, they evolve, and quickly. There is hardly time to absorb a new mode, a new technique, a new place, a new situation, before it is made obsolete by something newer, which we must adapt to without having the time to do so. This kind of transience, which used to be called revolutionary and is now hardly noticed, demands of us an adaptiveness, a creativity that uses entirely different rules than history—classicism—can provide. Classicism and the monumental are still very much with us today. Bilbao is a classical monument. The architecture of transience, the architecture of our true modernity, has barely been imagined, let alone attempted in construction.


As a footnote, let me say that we must not equate the concept of transience with ‘flexibility.' Architecture should never be flexible. Rather it should be firmly and unconditionally what it is. Given the abstract nature of its shaping of spaces for living, they can and should be open for interpretation by different people, for whatever reasons they might have. This is a characteristic I emphasize. Also, architecture cannot present a ‘one-size-fits-all' set of conditions. Rather, it should be definite, precise, unequivocal, so that people can respond to it in whatever way they choose. Only by being singular, original, authentic can architecture offer to people spaces in which they are encouraged  to be the same.


[![](media/lwblog-asitis-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-asitis-2.jpg)


(above) The installation *Civilization* at the Martin Gropius Bau exhibition *7Hills: Images and Ideas of the 21st Century*, Berlin.


*We met in Berlin at the opening of the 7 Hills exhibition, one of the most important exhibitions in the history of that city. On that occasion you proposed a steel beam jungle, in which some artworks found their place. Now,  looking at “The Fall,” it seems to me that the jungle moved from the perimeter of the room to the center, to be the real protagonist of the space and the only interlocutor with the public.*


In the 7 Hills exhibition, I worked with Dr. Thomas Medicus, a historian and curator of the ‘hill' entitled “Civilization.” We both agreed that civilization could not be presented as the orderly progression most historians would have us believe it to be, but only as a paradoxical, non-linear accumulation that is redundant, excessive, mythological. What you describe as a “steel beam jungle,” I thought of as a Kabinett, a collection displaying the accumulated artifacts of random centuries. I also thought of it as a labyrinth, a complex circuit of spatial frames in which it is easy to become lost or confused. Confusion, in our Western tradition, is the first step in the process of learning. The artifacts of many epochs were juxtaposed, blurring comfortable timelines. Architecture here was neither rhetorical or didactic—it conveyed no ideology—but was instead heuristic: its structure liberated unexpected associations between the artifacts of civilization, making multiple interpretations of its lineage possible.


The installation in Paris had quite a different ambition, even though the means of creating space had something in common with the installation in Berlin. There were no artifacts, for one, but the biggest difference was a higher degree of abstraction in Paris, a more purely tectonic realization. This brought it closer to an architecture that I consider a constructed idea of itself.


*Was your aim, in these two installations, to show that architecture is no longer in the service of art, but only in the service of architecture itself, as the most comprehensive form of art?*


I never thought architecture was in the service of art. Architecture serves ideas, hopefully ideas big enough to embrace many aspects of life. Architecture's comprehensiveness, though, is not to been seen directly. It's not a collage of various disciplines or forms of knowledge, as post-modernism would have it. Instead, it's synthetic, abstract. And by this I don't mean it is minimalist, a reduction. It must create a complexity that offers highly individual interpretations and uses. Programmatically speaking, it can only do this if it is, in the first place, ‘about' itself.



*This attitude asserts, tacitly or not, that the real art is to be created in the relationship between  space and the spectator's body movement. Even the Venice Biennale of Architecture and the one of Art seem, in the last editions, to be the same.*



I would state it differently. Architecture creates a field of potentials, defined by spatial limits, and also by its own imbedded methodology, within which people may choose to act, or not. Traditional architecture tries to choreograph people's movements, even their thoughts and feelings. The architecture I envision is more anarchic. For some years I called it freespace, free of predetermined purposes and meanings. The difficulty of occupying such spaces confronts the crisis of contemporary existence, namely the necessity to invent one's self and meaning in the face of world-destroying changes.


**to be continued**




 ## Comments 
1. [Oliver](http://dronelab.net)
4.25.10 / 8pm


*And, if you believe in the laws of the conservation of matter and energy, even the new is only the old in another form.*  

I like this thought, which, again, only repeats the obvious, but in a way that it opens ones eyes to things forgotten. I often forget about the things that are in front of me all the time.
3. Sam
4.26.10 / 9pm


A lot today's buildings are outside of space and time, and don't agree with history, rather it lives within it. Call me paranoid, but we live in an unreal world filled fast paced industries and phony technological advances that forces us to become mental slaves taking whatever this post-capitalist society provides. It seems like the only real world that can be seen is through Lebbeus's work. 


Cultures change over time and differ across space. Yet it is being destroyed because history is not evolving organically with independent elements outside of history. The world's facticity needs to be emphasized more so than pure reason.
5. [organicmobb](http://organicmobb.wordpress.com)
4.29.10 / 8pm


Sam you need to read DeLanda's Thousand Years of Nonlinear history…
7. dana
12.27.10 / 8pm


LW 


I read somewhere that you find teaching your drawing process to be pointless. That the person needs to find the originality in themselves and admire the works of others. However, I also read somewhere that you came to a school and taught your process in a period of 3 days or so. For the sake of pretending that it will have an impact on someone – that could result in something substantial… could you possibly give us an idea of your drawing process? You are so good at giving us a look into your head… what about that hand of yours?
