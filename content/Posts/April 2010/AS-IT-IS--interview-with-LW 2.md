
---
title: AS IT IS  interview with LW 2
date: 2010-04-22 00:00:00
---

# AS IT IS: interview with LW 2


[![](media/lwblog-asitis-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-asitis-3.jpg)


(above) The Fall installation, Cartier Foundation for Contemporary Art, Paris.


**Interview by Sebastiano Olivotto:**


*Is the result of the metaphorical or actual “fall” an enfeeblement of the traditional arts or instead the evolution of architecture as an art in itself rather than an extension of technology?*


Architecture must find its own way. To call it an art confuses the issues that are crucial to its formulation today, or I should say its reformulation. As an architect, I can learn something from everything, including the traditional arts and various technologies, high and low, but this knowledge cannot in itself inspire the making of a new architecture.



*One of your early works was about the [“Einstein tomb.”](https://lebbeuswoods.wordpress.com/2009/09/27/the-vagrant-light-of-stars/)* *A flying architecture, freedom from gravity and the relativity of time and space were the main themes of that work. With The Fall installation the theme is gravity and present reality.*



The way you put it brings to mind the fate of Icarus! And it's true that my ideas have come closer to earth over the past twenty years. The Einstein project came at a moment of what I would call philosophical idealism, when I was looking to science and mythology for ideas that could inform architecture. Since then, my skepticism and existentialism reasserted themselves. The Aerial Paris project of some ago shows what an existentialist architecture that flies might be like.



*Your work has also evolved from considering architecture as a volume comprised of architectural elements, to architecture as a void between vectors or lines. It seems to me that your composition has matured in an unusual and maybe inverse trajectory, in which you obtain space no more through addition but through subtraction.*



Yes and no. Yes, my architectural means have changed. No, I still consider architecture as a construction made up of parts.


*It also seems that with this installation in Paris you wanted to create a space that is the direct translation of two-dimensional drawings. If we consider the tube you used in this installation as a metaphor of the drawings' lines, we can affirm that your idea is to use architecture to represent the drawings and not the contrary, as it is usually done.*


Wouldn't it be wonderful if we could draw actual space, and not only its representation! In a way, that's exactly what we—I and my collaborator on this project, Alexis Rochas—did in Paris. We created a spontaneous architecture, one that worked directly with existing conditions. It is idiosyncratic and so complex that could never be built again in the same way. Its coherence, justifying my claim that it is in fact a system of order, comes from the design not of the space itself, but of the elements—aluminum grids and tubes—the language by which the grids are made and the tubes are bent, and, not least, the rules governing their placement. Spontaneity only works within clear limitations. Like the improvisations in Baroque music and jazz.


 *This seems to be very innovative and to suggest that architecture needs be freed of its stereotypes and of elements like “walls,” etc.*


Not all architecture. Just this architecture, which is experimental, trying out new ways and means. And, of course, it is not ‘architecture freed of itself,' just of certain historically certified notions, such as typology, forms of pre-conception that sponsor the already-known and the too-often rehearsed.


 *Doesn't such an architecture run the risk of becoming formalistic, that is, socially and culturally sterile?*


Yes, it's a risk. When you take architecture away from the narratives of history, from the signs and symbols giving social existence agreed-upon meanings, you are putting a big responsibility on individuals to give meaning by the actions. The architect makes the first move by investing space with his or her– understanding of the conditions and demands of reality. As I see it, these today call for a high degree of individual initiative, because social life has become so fluid that it can no longer be symbolized in static form.


(below) Reconstruction of war-damaged apartment buildings, Sarajevo:


[![](media/lwblog-asitis-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-asitis-9.jpg)


*You have investigated complex urban realities, such as those in Sarajevo and Havana, borderline and peripheral situations. You affirm that there is a constant relation between periphery and center. Is what happens on the periphery a consequence of a central phenomena, or vice versa?* 


*Huge question. I'll try to focus it on the understanding I came to after visiting the places you mention, and its impact on my ideas about architecture. We are conditioned to think of the center of a culture, a capital city, for example, dominating the periphery of its sphere of influence. The center radiates and the periphery absorbs its energy, and is transformed by it. This model works well so long as the center is the place of innovation in the culture, that it is the source of growth and change. Often it is. But, in recent years, centers have become culturally—socially and politically—more stagnant, taking on the roles of museums of culture, places where the past is more important than the future. Perhaps this is the result of tourism and its vast economy, or the general cyclical evolution of things. The periphery—towns, countries, cultures—still in many ways dependent on a center, have experienced sudden, often violent and dramatic changes in ways the more controlled centers have not. Communications technologies such as television and the internet, made global through satellite networks, have elevated the importance of these peripheral events, because they can be seen everywhere. As a result, peripheries become centers.*


The events occurring in places like Sarajevo—and now Baghdad—are terrible and challenging to the status quo maintained in the big centers. They change the way we see the world. As a result the concepts of center and periphery no longer have fixed meanings, but are part of a fluid condition that is unpredictable and in a sense uncontrollable. Big centers like the USA, trying to hold onto its past and the power it has had, can only confront this new  condition with a violence greater, it hopes, than those of its enemies. In doing so, it is trying to force the peripheries back into their former, less influential role. This is a policy doomed to fail. The balance has already shifted, due to technological change and its consequences, past the point of no return.


Thinking of architecture in relation to this phenomenon, it is surprising to me that the building type of greatest interest today is the museum. It is as though architects are turning their backs on the vital questions in order to live in a fantasy world of new shapes that cover up stagnant, outmoded ideas, thereby perpetuating them. Obviously, the old centers are where there is money to build. That's where the clients are, and they have room for architecture only when it advertises a status quo. In Paris, my client was a philosopher, not a board of trustees, and a philosopher whose ideas take fully into account the new dynamics of center and periphery, of accident and design. That's why I was able to experiment as I did.


 *You were a journalist during the Sarajevo siege. How did the experience of observing  this crisis change your way of  thinking about architecture?*


In the winter of 1993, Sarajevo was surrounded and under murderous attack by Bosnian Serb forces. The only way to get through their lines was to fly into the airport on UN relief flights. The only way for me to get on one of those flights from Ancona in Italy was to be, or pretend to be, a journalist. My real purpose in going was to meet with colleagues I knew in Sarajevo, to bring moral support from outside, and to begin discussions about rebuilding the city, when that time finally came. I took with me forty copies of the pamphlet “War and Architecture,” which presented my strategies for the reconstruction of war-damaged buildings and the reasoning behind them. Also, I went to arrange a workshop for students and architects in Sarajevo that would bring creative architects there to deal with the difficult questions that would eventually have to be dealt with. That workshop happened the following March, with Thom Mayne, Ekkehard Rehfeld, and myself attending.


My experiences there fortified a conviction that architecture must address seemingly intractable problems like these, even though there are no clients per se, and no money. First of all, because buildings were damaged or lost, and this directly impacts people's lives. It's not hypothetical. They have to be repaired, rebuilt. Secondly, the attack on Sarajevo was an attack on civilization. The aggressors wanted to destroy its urbanity, its tolerance, its complexity, its cosmopolitan spirit. So, it was an attack on the idea of the city, and therefore on cities everywhere. It was an attack on London, Buenos Aires, New York. If we, as architects, ignored  it, we would be ill-equipped to deal with the same sort of attack when it fell upon our city.


(below) SCAR construction, Sarajevo:


[![](media/lwblog-asitis-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-asitis-7.jpg)


*In some of the works related to war and the irreversible damage it causes, you used the Scar and Scab concepts. Is this architecture as surgery?*


No, I don't think so. It is just architecture doing what it is supposed to do—dealing with human conditions and problems. Trying to formulate space with the potential to enable new ways of thinking and living, those resulting from the trauma of war.


 *They also look like temporary structures. In your drawings, though, the materials look aged.  Is that meant to suggest a permanence to the work, or does it suggest that these architectures are the result of an assemblage and reinterpretation of ready-made objects, even fragments and waste of the war itself?*


Both. The permanent is only an extended form of the temporary. And, if you believe in the laws of the conservation of matter and energy, even the new is only the old in another form. It is of crucial interest exactly what this ‘other' form is.


 *In a time of global crisis like the present, what is the role of art?  What is the value of architecture? Is it destined to disappear, leaving the scene to the standard and the real estate value, or do you think that creativity can stimulate the beginning of a new movement of capital toward a new economic welfare for many?*



I wish I could believe that architecture will be instrumental in making this kind of reform, but I don't. Money and power will serve themselves and have very little use at the present time for the kinds of ideas we've been discussing. But exactly for that reason, exactly because the global situation is so turbulent and uncertain, it's important that some architects devote themselves to ideas they believe make architecture responsive to the highest aspirations, their own and those of others as they understand them to be. I see little hope for the moment that this kind of work will make any significant impact on the way most people think and work, but it will keep the ideas alive, and also the hope.


**end of interview**



 ## Comments 
1. [Oliver](http://dronelab.net)
4.25.10 / 8pm


*And, if you believe in the laws of the conservation of matter and energy, even the new is only the old in another form.*  

I like this thought, which, again, only repeats the obvious, but in a way that it opens ones eyes to things forgotten. I often forget about the things that are in front of me all the time.
3. Sam
4.26.10 / 9pm


A lot today's buildings are outside of space and time, and don't agree with history, rather it lives within it. Call me paranoid, but we live in an unreal world filled fast paced industries and phony technological advances that forces us to become mental slaves taking whatever this post-capitalist society provides. It seems like the only real world that can be seen is through Lebbeus's work. 


Cultures change over time and differ across space. Yet it is being destroyed because history is not evolving organically with independent elements outside of history. The world's facticity needs to be emphasized more so than pure reason.
5. [organicmobb](http://organicmobb.wordpress.com)
4.29.10 / 8pm


Sam you need to read DeLanda's Thousand Years of Nonlinear history…
7. dana
12.27.10 / 8pm


LW 


I read somewhere that you find teaching your drawing process to be pointless. That the person needs to find the originality in themselves and admire the works of others. However, I also read somewhere that you came to a school and taught your process in a period of 3 days or so. For the sake of pretending that it will have an impact on someone – that could result in something substantial… could you possibly give us an idea of your drawing process? You are so good at giving us a look into your head… what about that hand of yours?
