
---
title: EXUBERANT SPACE
date: 2011-09-13 00:00:00 
tags: 
    - architecture
    - Filip_Dujardin
    - photomontage
---

# EXUBERANT SPACE


[![](media/fic-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/fic-2.jpg)


*(Note: The photomontages shown here have been widely published on the internet for some time, but never with commentary that addresses issues*I mention below* important to contemporary discourse .)*


What at first glance seem like absurdist montages of buildings by photographer [Filip Dujardin](http://www.filipdujardin.be/), good for amusement only, actually raise some important questions. It doesn't take long to realize that these images are actually design ‘drawings,' made with photographs. making architectural proposals that invite us to think seriously about them.


The most obvious question that arises is about the buildings' unusual forms. What, might we imagine, is the purpose of the buildings that demands their highly sculptural forms, assembled from what are clearly parts of ordinary buildings? A clue is found in Modernist architecture of the last century, when materials and structural systems were invented that had the capability of effectively floating and flying buildings, free of the gravity that for centuries had bound them so closely to the earth. Cantilevers and long-spans were not only a conquest of natural forces but a liberation of the human spirit from a servitude to them, putting human beings—through advanced technology—more fully on their own and more responsible for their own well-being. In this way, Modern architecture had far-reaching ethical, even spiritual, consequences, but that wasn't the only reason structurally daring buildings were proposed and sometimes built—it was also for the sheer, playful fun of it and an exuberant enjoyment of space.


I don't imagine Dujardin's designs will have any weighty consequences. For one thing, they don't exemplify any new concepts. Rather, they are themselves consequences of principles that Modernism introduced a century ago. They do, however, breathe some new life into those old ideas that many have pronounced dead. Looking at some of Steven Holl's later projects, such as the [Horizontal Skyscraper](https://lebbeuswoods.wordpress.com/2011/06/28/steven-holls-horizontal-skyscraper/), and the [Edge of the City](https://lebbeuswoods.wordpress.com/2008/12/11/visionary-architecture/) project for Phoenix—as well as Dujardin's designs—it is clear that they are very much alive.


So enjoy. Guilt-free.


LW


[![](media/fic-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/fic-5.jpg)


.


[![](media/fic-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/fic-4.jpg)


.


[![](media/fic-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/fic-7.jpg)


#architecture #Filip_Dujardin #photomontage
 ## Comments 
1. Pedro Esteban
9.14.11 / 12am


this text puts so much light…  

I said: I can't believe LW will post about this… but thanks, at least some see further…


the first paragraph is really important today…thanks for that clue…. 


Interesting the fact that you do not publish the images of Holl in the same post.
3. [‘Ficitions'. Neues, oder zumindest neue Erkenntnis im Alten « Durchmessungen](http://durchmessungen.wordpress.com/2011/09/21/ficitions-neues-oder-zumindest-neue-erkenntnis-im-alten/)
9.21.11 / 10am


[…] wenn diese Entwürfe, wie Lebbeus Woods schreibt, auf den ersten Blick vielleicht lediglich zur Unterhaltung dienen könnten, würfen sie doch […]
