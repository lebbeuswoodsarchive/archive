
---
title: HEROES OF A REVOLUTION  Gordon Pask
date: 2011-09-28 00:00:00 
tags: 
    - architecture
    - cybernetics
    - epistemology
    - Gordon_Pask
---

# HEROES OF A REVOLUTION: Gordon Pask


[![](media/pask-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/pask-1a.jpg)


This is the first in a series.


*The revolution was cybernetics, a transdisciplinary field of research and theory that, in the 60s, 70s, and 80s, changed the way we think about both knowledge and information, and how they are communicated between individuals and social groups. It enabled the advent of ‘the Information Age,' and paved the way for the use of the computer in every aspect of daily life, including the Internet.*


*While the direct impact of cybernetics on architecture has so far been limited to concepts of ‘virtual reality,' ‘intelligent buildings,' and ‘simulacra,' its indirect impact has been enormous in the use of the computer as a tool of architectural design, representation, communication, and education.*


*During the period of cybernetics' development, the Architectural Association in London was at the center of the effort to apply its ideas and methods to architecture, primarily in the efforts of Peter Cook, the founder of Archigram. Gordon Pask was very much involved in discussions at the AA, along with Cook, Dennis Crompton, Michael Webb, and Cedric Price, (all under the direction of Alvin Boyarsky), about how architecture could become one of the ‘trans'—‘across'—disciplines embraced by cybernetics. The photo of Pask above is from the cover of one of many magazines published at the AA during this period of intense intellectual and artistic ferment.*


*I met Gordon Pask on several occasions, most memorably when I was giving a lecture at the AA on Einstein's ideas of space and time, when he abruptly came to the lectern and finished my lecture—with corrections, of course!*


*LW*


*Below is a sketch of Gordon Pask, made by Paul Pangaro, after Pask's death.*


**Gordon Pask**


*[I wrote this obituary at the request of the Guardian Newspaper in London, which they published on **16 April 1996*** *under their title, “Dandy of Cybernetics.” It seems to have been published nearly verbatim, with some capitalisations and spellings changed; this text is my original….* ***Paul Pangaro****]*


Gordon Pask, who has died aged 67, spent his life developing an elegant theory of learning that stands without peer. His achievement was to establish a unifying framework that subsumes the subjectivity of human experience and the objectivity of scientific tradition. Sponsored by governments and industries on both sides of the Atlantic, his life-long research spanned biological computing, artificial intelligence, cognitive science, logic, linguistics, psychology, and artificial life. His was an original approach to age-old questions of how the human organism learns from its environment and relates to others through language.


Andrew Gordon Speedie-Pask was born in Derby in 1928, son of a partner in Pask, Cornish and Smart, a wholesale fruit business in Covent Garden. The biographies from Pask's six book jackets mention Liverpool Technical College, Cambridge University, University of London, the Open University; but one has the feeling that these were simply locations, and his many advanced degrees simply souvenirs, of work that was entirely of his own control and creation.


He placed himself squarely in the tradition of cybernetics, while at the same time charging ahead in a direction that was wholly new. Cybernetics was named in the 1940s as the discipline concerned with information, feedback, identity and purpose. These concerns were independent of whether the system in question was an animal or machine, individual or population. This domain suited Pask, not the least because it was not mainstream. Standing out was what he wanted.


When I first saw him, at one of the many academic research labs around the world where he played the role of consultant-as-catalyst, surely he stood out. He was dressed as always, an Edwardian dandy in double-breasted jacket and bow-tie and cape. He was slight of build, but the power of his mind made him huge. His courtly manner softened the intimidation of his probing questions and his fierce interest in precision and speed.


When Pask built his machines and his theory, his philosophical view was at odds with artificial intelligence, which arose from the seeds of cybernetics but presumes that knowledge is a commodity to pluck from the environment and stick in a cubbyhole. Pask's learning environments, whether for entertainment or touch-typing or statistics, viewed the human as part of a resonance that looped from the human, through the environment or apparatus, back through the human and around again. For Pask, that is the interaction by which we understand each other when we speak or dance together. He specified how this works in detail in his many publications on Conversation Theory.


Pask's criticisms of artificial intelligence were publicly polite but probing. His private view was that it was impoverished and could not achieve its goal of reproducing intelligence. He had himself reproduced intelligent behaviour with electro-mechanical machines soldered by his own hand in the 1950s. By realising that intelligence resides in interaction, not inside a head or box, his path was clear. To those who didn't understand his philosophical stance, the value of his work was invisible.


In his lifetime he received substantial recognition. Among cyberneticians he is acknowledged as one of the all-time greats. Conversation Theory has provided cybernetics its prescriptive power for modelling learning and agreement. Outside the field, Pask was known for the intensity and scope of his lectures; audience comprehension was more elusive. I often heard listeners say that 10% of his talk was understandable and, if the other 90% was as good, than this guy was really something.


Even more dense for the uninitiated was his prose, where a passion for completeness and comprehensiveness made entry difficult. This softened in later years, when his presentations were also more accessible, and he acquired a following in the social sciences.


Pask's competitors sometimes resented his habit of incorporating their theories into his own. Though he usually quoted them with full attribution, it was the determination with which he played the “my theory subsumes your theory” game that disturbed them. From those who adopted his ideas, it is hard to know the extent of his influence. The card catalogs of many libraries list his books, which somehow are missing from the shelves.


Pask was capable of great kindness and sometimes utter disregard for the individual. His theory shows how conflict is a source of cognitive energy and thereby a means for moving a system forward more rapidly. He seemed willing to foster conflict around him, even if it drove him and others further than physiology would prefer. His touch-typing tutor pushed the learner harder and harder, to the point where the rate of learning is greatest but also closest to the brink of system collapse. His students and collaborators were vastly changed by knowing him; some needed time to recover.


While living so much in his (and others') heads, Pask had extraordinary sight and hearing and physical coordination. I can still feel the adrenaline as his passenger in an Austin Mini. He followed the car ahead at a constant, harrowingly-close distance that was precisely maintained the entire route from Richmond to London, Pask double-clutching all the way. His one-on-one conversations had a similar focus and commitment.


From the intensity with which he lived, perhaps his own body suffered the most. Waking him for his evening dinner guests, after his long nights of work and short days of sleep, entailed a delicate balance of firmness and compassion. While waiting I could review the fruits of his night's work — perhaps a new song lyric or research paper. Before the jacket and bow-tie and cape could go on, here was this mad and brilliant creature, all sinew, rising to dazzle and demand of us, and of himself, once more.


Andrew Gordon Speedie-Pask, cybernetician, producer for the stage and lyricist, born June 28, 1928; died March 29, 1996. He is survived by his wife, Elizabeth, daughters Amanda and Hermione, and grandson, Nicholas.


#architecture #cybernetics #epistemology #Gordon_Pask
 ## Comments 
1. joe schmoe
10.6.11 / 1pm


If you can run for Maria Nordh's position if it's an electable position, etc…or join the board that determines how the code is interpreted and approved, that would be the next step I would think…not form and space making in the traditional architectural academic manner.


My experience in NYC has proven that codes, zoning, etc… enforcement, interpretation are completely social, political and economical. Although the legal language is extremely straightforward, the clients push their professionals (architect and engineers) to find innovative ways of interpreting such language which usually later warrants a memo update by the building department further clarifying the language often denouncing the innovative reading of the code, etc…putting the clients often in awkard half built positions requiring a reconsideration by the building department….seriously – if the slab and structure is up are you really going to ask someone to tear it down based on a sentence writing on paper?
3. [Per-Johan Dahl](http://gravatar.com/pjdahl)
10.24.11 / 1pm


The problem is not so much about the physical structure but about the normalization and control of spatial perception. Nordh's reaction summons up the history of building and planning regulations. Introduced in the early twentieth-century to obstruct some health crisis, building and planning regulations have ever since been used frequently for other purposes than protecting public good. This is particularly true today when new codes are instantly introduced in Europe and the U.S. to, in the name of sustainability, mask the implementation of historically looking architecture and developer oriented city building.
