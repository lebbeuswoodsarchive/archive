
---
title: SEPTEMBER 11, 2001-2011 (complete)
date: 2011-09-08 00:00:00 
tags: 
    - 9/11
    - architecture
    - reconstruction
    - Sarajevo
    - terrorism
    - World_Trade_Center
---

# SEPTEMBER 11, 2001-2011 (complete)


[![](media/sara-twintowers-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/sara-twintowers-1.jpg)


*(above) The burning Unis twin towers, Sarajevo, 1992. Photo by Ivan Puljic.*


**WE SAW IT COMING**


For anyone who saw the burning twin towers in Sarajevo, in the summer of 1992, which were attacked by terrorists bent on undermining the morale of the people of that cosmopolitan city, the attacks on the twin towers of the World Trade Center in New York, nine years later, with the same goals in mind, came as no great surprise. The fall of the Iron Curtain and the end of the Cold War had produced a new type of global struggle based not on vast armies clashing in the field, but on small-scale insurgencies attacking the centers of their enemies' power, disrupting them, and thereby undermining their self-confidence and ability to dominate others. This new type of warfare was called terrorism. Its main weapon is creating fear in the enemy, both government and ordinary citizens, leading not to armistices, treaties, and other official instruments of reconciliation between legally recognized states, but to de facto victories, in which the insurgents hope to win economic or political concessions that strengthen them in their own domain or globally, in the sense that they are ever more feared and hence ever more powerful and influential.


One significant new feature of this new type of conflict is that opposing sides are not drawn along socio-political lines—one communist and one capitalist—as in the Cold War rivalry between two superpowers, but rather along religious ones. This is a throwback to the Middle Ages, and not Modern at all, except in terms of weaponry and techniques of command and control. The conflict now is primarily between Christians and Muslims. The attack on Sarajevo was carried out by a Christian insurgency against a Muslim majority. The attack on the World Trade Center in New York was carried out by a Muslim insurgency against a Christian majority. Both had the goal of degrading a way of life. Both attacks were attacks on the idea of the city itself.


On March 5, 1994, a conference was convened in the Great Hall of The Cooper Union in New York City. It was titled: *SARAJEVO/NEW YORK: The City Under Siege*. Participating were many of the most astute observers and analysts of the ongoing attack on Sarajevo and its consequences for cities everywhere. The conference's aim was to make New Yorkers aware of the urgent needs of Sarajevans but also to the possibility that “it could happen here.” How many people left at the end of that day with the idea that New York City could be attacked I cannot say. But some of us saw it coming and many—if they understood at all what was happening an ocean away—should have.



[![](media/sar-ny-flyer-1abw.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/sar-ny-flyer-1abw.jpg)


**A GREAT MIND IS A TERRIBLE THING TO WASTE**


Fierce debates swirled around the World Trade Center site after its towers fell, focusing on how, or even if, the site should be rebuilt. Some of the most brilliant and creative architects of our time entered the debate, often with specific design proposals, any number of which—if one had been used as a point of departure—could have turned tragedy into triumph, by radically reforming the devastated site into a new kind of public urban space. The design scheme finally selected, by Daniel Libeskind—an architect of great renown for his evocative conceptual designs and the Jewish Museum in Berlin—was the least innovative of all, and the least demanding of politicians and developers, accounting, no doubt, for its digestibility. Amounting to nothing more than a conventional grouping of conventional skyscrapers, dominated by a so-called “Liberty Tower” 1,776 feet tall, it was meant to symbolize an American resilience in some way that was never entirely clear. A reminder of the Declaration of Independence of the United States from the British monarchy on July 4, 1776 seems oddly detached from the Al Qaeda attacks, the reasons for them, or any serious vision of what might be the best response—but it was hailed at the time by many as a brilliant idea. In any event, even Libeskind's uninspiring proposal was eventually hijacked by the politicians and developers, who handed it over to David Childs of Skidmore, Owings and Merrill (SOM), and he oversaw its further descent into an indifferent conventionality.


The chance to create a great new work of architecture and public space—the best memorial to human courage and resilience in the face of terrible loss—had itself been lost.


**NEW YORK, NEW YORK**


It is often said that New York is a center of global culture. The only culture that it is truly the center of is the culture of buying and selling, and of someone, usually a wealthy elite, making money in the process. In New York and, increasingly elsewhere, the culture of profit is the one all other subcultures revolve around. Of course, art and music and literature and dance all flourish in New York, because there is the possibility to ‘buy and sell' them in one form or another—they make money. Without that money the cultures of beauty and inspiration would simply wither away, or so we are told. What we are not told by the corporations and individual patrons who assure us that capitalism is the great sponsor of the arts, is that it is only the arts that fit into the mainstream that are bought and sold. Indeed, the mainstream can tolerate and even requires a certain amount of naughty avant-garde art works that shock, criticize, provoke and attack corporate assumptions, in order to claim legitimacy in historical terms. But, there are precise limits to what can be done and promoted for profit, and they are tightly drawn. Any serious, non-ironic invocation of socialism will be ignored by the marketplace. Or any serious invocation of actual revolution, under the aegis of any ideology. Or any serious invocation of a future that does not include capitalism as its centerpiece.  The list is long, but little discussed in public—that, too, is avoided by artists and critics who adopted long ago a self-censoring caution, lest they be considered terrorist sympathizers or otherwise unfit for the marketplace.


New York is a city with few innovative contemporary buildings. Famous names are trotted in for the occasional extra-marketable building, but none are invited to make ambitious urban proposals that could affect the future of the city and the way most people actually live. Most notably absent are world-famous American architects like Eric Owen Moss and Steven Holl, who lives in the city. Thom Mayne and his firm Morphosis have built a radical building for the Cooper Union, a progressive university that insists its students attend for free. The rest of the city is the product of real estate speculation, of profit and loss—the model that cities around the world seem eager to follow.


The message sent by New York, a focus of global attention on this September 11, is simple: *Business as usual.*


[![](media/wtc-lw-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/wtc-lw-1.jpg)


***For publication of this article in another venue, and with more images, go to:***


**[http://www.domusweb.it/en](http://www.domusweb.it/en/architecture/we-saw-it-coming/)**


LW



#9/11 #architecture #reconstruction #Sarajevo #terrorism #World_Trade_Center
 ## Comments 
1. Graham
9.9.11 / 9pm


Lebbeus, 


Tremendous article . . . and yes . . . it is as it always has been Business as Usual. On the day after the publication of your article in DOMUS/WordPress, Democracy Now lead with the following story:


<http://www.democracynow.org/2011/9/8/a_fateful_day_9_11_also>  

“On the anniversary of the 2001 attacks on the World Trade Center and the Pentagon, we look back at several national and international events linked to that day” The report spans incidents in Haiti, Guatemala, India, and includes an interview with Ariel Dorfman, served as a cultural adviser to Salvador Allende, who details his death at the hands of a US backed coup on Sept. 11, 1973, 


And today with this:  

<http://www.democracynow.org/2011/9/9/rebuilt_ground_zero_billed_as_national>  

“While the 10th anniversary has made international headlines, little attention has been paid to some controversial aspects of the rebuilding at Ground Zero. At a time when President Obama is launching a massive jobs initiative, key parts of the construction project were outsourced overseas. A Chinese glass company won the contract to manufacture the special blast-resistant glass base of 1 World Trade Center. Some 250 tons of stainless steel from Germany will also be imported for the project. So far, just two tenants have agreed to move in to the 105-floor building, the massive skyscraper formerly known as the Freedom Tower.”


. . . and:  

<http://www.democracynow.org/2011/9/9/as_study_links_9_11_debris>  

“According to a new article by ProPublica, recently uncovered documents reveal that federal officials in Washington and New York went further than was previously known to downplay concerns about health risks, and misrepresented or concealed information that might have protected thousands of people from the contaminated air at Ground Zero.” With the intention being to get Lower Manhattan back to Business as Usual.


– Graham
3. [dan lenander uhrenholt](http://www.danlenander.com)
9.12.11 / 5am


Hey Lebbeus .  

Great Article. Also the anniversary of the Allende assasination . Saw the documentary and the bulletholes in the Parliament walls when I was in Chile 2 years ago.  

Keep up the writing. All the best  

Dan
5. [WAR AND ARCHITECTURE: Three Principles « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/12/15/war-and-architecture-three-principles/)
12.16.11 / 12am


[…] I am revisiting the work I did some fifteen years ago for an unhappy reason. Originally intended to address the destruction of buildings in Sarajevo, Bosnia—which I and many others hoped would prove to be an isolated catastrophe—it has instead turned out to be only the beginning of a new trend resulting from globalization, a proliferation of  regional, often insurgent-driven wars that have resulted in the piece-by-piece destruction of cities and the killing of their inhabitants that characterized the torturous three-year attack on Sarajevo. […]
7. Velton
12.16.11 / 4am


Where are we today with terrior.Do we have it in set. Velton watkins
9. [Arash Basirat](http://etoood.com/Profile.aspx?MyKey=334)
8.14.12 / 12am


این مقاله به فارسی برگردانده شده است نسخه برگردانده شده ی ان را می توانیم در ادرس زیر مشاهده فرمایید  

<http://etoood.com/Security/Article/Article.aspx?ArtKey=528>
