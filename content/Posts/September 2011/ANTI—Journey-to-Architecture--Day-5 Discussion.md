
---
title: ANTI—Journey to Architecture  Day 5 Discussion
date: 2011-09-22 00:00:00
---

# ANTI—Journey to Architecture: Day 5 Discussion


[![](media/lat-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-13.jpg)


[![](media/lat-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-2.jpg)


*(above) Images from a videotape by Lebbeus Woods.*


**Transcribed audiotape Raimund Abraham and Lebbeus Woods discussion in a monk's cell at LA TOURETTE**


**October 25, 2007**


LW: Raimund has gone to the car to get his cigars. I'm standing on a balcony looking back into the cell and out over this large, sloping green meadow bordered by large trees and thick woods. I'm facing the sun directly; it's pouring into the cell. The mood here is totally different than when we were talking earlier. Raimund's idea is to present arguments—this is the dialectic style. My approach is not so rigorous and I'm not so sure I'm very good at creating arguments. I think one question in my mind is about the idea of built architecture, and about the way we know built architecture too far away to visit, or inaccessible to us, which is through some mediation. This is the first time either Raimund or I have been here been here. How does being here change our understanding of the architecture? I'm not sure that it does. Being here is potent experience in itself, but I don't think it tells me much I didn't already know from photos and drawings. I had the same feelings at Ronchamp….


LW: (playing back the above to Raimund with his cigar in hand) I'm not sure this is something you want to talk about.


RA:  You say what you have to say and I'll say that what puzzles me a little bit is that you say that you're not very good at arguments. Arguments are the foundation for a dialogue. Or, the dialectic. If you are curious, you ask a question. A good question leads to another question. I do not like giving answers. I do not know enough to give answers. But I am very good at questioning. And I'm curious and the rigor comes from my respect for the language. When I write, I'm in total pain. Every time I have to write a new text, I have the feeling like I've never written one because of my respect for literature.


I'm coming back to your question about experiencing architecture. I think that I brought this up earlier. There are two are dimensions, basic mental dimensions, one is visual and one is tactile. With a photograph, you touch a picture, without touching the concrete, the tile, without touching them with your eyes, the actual materials. I don't need to come here to understand the spaces, the plans. I don't need that, but what is the overwhelming difference is the presence of physical space—that's like communicating, and you see there are different kinds of communication. I hate email, because email is impersonal. I can touch a letter. When I get it, I know immediately from whom it is and there is a presence of the person who did it. When something is removed from touch it has no reality for me. Email has no reality whatsoever. When you write with a typewriter….Acconci said once in a very beautiful little lecture that when you touch a letter (on the typewriter) the mechanics of the finger imprinting the letter on a white piece of paper—the white piece of paper is fragile—it might be and will be violated with the first letter you put on. In order to erase this letter you either have to destroy the paper or use the white out. But in a computer text you only have to push one button and the text is gone. Simply gone. I am fearful when I draw, when I write, because I have too much respect for the medium. When I make a drawing, I labor. It's not easy. I cannot rely, there was an interesting, some months ago, Ronald Mason, you know, we know each other quite well. And he was, he accomplished what other men have never accomplished and never will be accomplished. It means he climbed all the eight thousand meter peaks of the Himalaya, most of them without oxygen. He was the first one to climb Mt. Everest without oxygen. So, he was in an interview and they were trying to praise him for his accomplishment.  He said, “How does that help me now. You see, I can't do that anymore. Now I have to look for a new challenge, the old challenge has been met. The desert now, I'll explore the desert.” And then the interviewer said, but the desert is removed from the mountains. And he said, no no no, it's the same, it's just crumbled mountains.


LW: (laughing).


RA: The presence of the person makes a difference. I mean, there are different realities. So, I have a absolute limit on the telephone. After five minutes, I am nervous. It doesn't matter if I'm talking to my lover, my mother—it doesn't matter. I can't go beyond a certain limit because I miss the presence of the person. No, sitting with or hugging someone are distinct realities, which have to do with increasing the tactility of the experience. So for me, coming here, it has nothing to do with what I knew about La Tourette. Nothing.  Photographs conveyed a great deal: this is the idea, it is something that Le Corbusier thought abut. The experience of this building was completely different yesterday when there was no sun than today with the sun. I mean the mood of a rainy day or an overcast day was somehow enforcing the decay of the building. Today I felt it much less and it became also much less important. Maybe its courage on Le Corbusier's part that he said I don't care about the decay—the spirit of the space of the architecture is so strong that the details don't matter. And in a way he's right.


LW: OK. Let me go a little bit further. The tactility aspect—the present society is moving further and further away from that, just generally.


RA:  I never really cared about society.


LW: I do. I can't isolate myself from the idea of the world around me. I live in New York City. If I lived in nature I would probably think about the trees and the animals. There aren't any trees and animals, Just people.


RA:  What? There are some trees and animals. Pigeons come to my windowsill….


LW: Well, you say you don't care about society and never cared about society which is your choice. Something that you don't care about, or don't have the curiosity about, as you would say, is an idea I can't separate myself from when I am concerned about architecture. If you don't care about society, then let's not discuss it.


RA: We can discuss anything you want to discuss.


LW: OK. Then let's talk about the computer….


RA: Lets talk about society.


LW: Society….


RA: I would like to talk about society. I said I don't care a great deal about society, but it means I care about humanity. You see, I care about the human condition.  I think society is invention of sociologists, it means nothing. Ultimately, humanity consists of individuals. The interesting thing about the American Constitution in its most radical interpretation is not protecting the collective.


LW: No….


RA: It is protecting the individual.


LW: Against the collective?


RA: It is the only constitution that is against the collective. When Goldwater was running for president, he declared that social security is unconstitutional. I'm talking about the radical condition of so-called society. There is a moral dimension. Capitalism has become a cult, so it has a religious dimension which means it cannot be profaned anymore. The sacred has been incorporated into the capitalist system.


Ultimately, all that doesn't touch me. Ultimately, I'm alone. Everybody is alone. The society idea is just a crutch that makes you feel like you're a member of a community. Community is different. If you are part of a family, that is a social structure, part of so-called society, but the relationships are personal, and ultimately everybody protects it's individuality.


I feel very much alone when I work. Also, it is a time now, where our work, and all independent work, is becoming rare. I have never given up my independence never. I have never looked to a client. I have never solicited a commission, I have never solicited anything. So that means that I protected my independence. So up to now, I was building a couple of building because I was lucky. Maybe one or two.  Mainly because I was given one by a friend. So, but what I'm saying is that this for me it is very important to detach myself from the collective. I cannot think collectively. We are completely alone. When you draw, you are completely alone.


LW: I agree….


RA: You are completely alone. We are privileged that we have chosen this sort of existence, which allows us to be by ourselves. I mean, we rely on money–of course we do or we would starve. We need the basic support, so we can pay our bills. But ultimately, our independence in that sense, of course, is relative.


LW: Yeah. I feel very privileged to be able to do work that I want to do.


RA:  Yes and we were talking about how you and I said that maybe it is a little pretentious ,but I don't think so. In architecture, we are almost the last two standing.


LW: .Our generation…


RA: In the battle we had maybe fifteen years ago. We had…


LW: Colleagues, comrades.


RA: Yes, comrades, in the battle. And by the year, they get less and less and less because they succumb to success, so-called success.  And give up their independence and that is the price they pay.


LW: But that makes me very suspicious. I know there are a number of people that got their start doing some radical architecture. They were talented, they were able to create amazing images, but that's just a path to getting to, to playing into success…


RA: I'm not that suspicious about this. When it happened, they were very authentic in searching for a new way to, for a new…


LW: For the original….


RA: For applying the imagination or going to, or as I always say to students, going to the limits of the imagination. So I told them, that is what the imagination is. So I hope there would be more courageous individuals who will take the risk. Did you ever read anything by Bachman, one of the most prolific Austrian writers? She committed suicide in the sixties. She wrote a book called “The Welder.” The welder after work always would go to the pub with his friends and then go home—a certain routine. And one afternoon there were none of his friends there and he was alone and he was sitting down having a beer and he felt something under the table and he went for it and it was a book, and he had never read a book. So he started reading this book and then another book. And then another book. He became so obsessed with reading books that he lost his job and he lost his family. And at the end he jumped off the bridge. So what I was talking about—the story is a demonstration of how dangerous it can be to imagine.


LW: We're both teachers so it's something that we do and something that we take seriously. And you're right, we just try to encourage people to be independent and to think independently and to aim for some kind of independent life. And I agree that's more important than trying to express something like “the spirit of the times,” or anything of the sort. I'm not interested in doing that, I've never done that. At the same time I have curiosity.  So, lets bring the computer into the story at just this point. It's part of society, an invention that has grown out of a kind of collective enterprise of science and technology and capitalism sponsored by specific social conditions, and it's having a tremendous impact. My curiosity is, what is the potential. Is there any potential in this new technology, other than producing bland corporate buildings?


RA: The potential is always there. If we talk about technology alone—I mean architecture without being confronted with a program is not architecture. There is no pure form in architecture. I do not believe there can be. I think any form must be challenged—this means that neither the form rules the function nor the function rules the form. It is created through a confrontation of these almost irreconcilable conditions. It is a co-existence, but you cannot circumvent a program. The same in literature—you cannot circumvent content—you have to confront it. If you are able to write *Finnegan's Wake*, it is a confrontation with content, with knowledge of literature, with everything, only with a new way of using language that hadn't existed before. The same words are used, but challenging grammar, spelling. When you are talking about architecture you always confront the idea of ‘house.' Even when I design an imaginary house, I confront myself with the idea of habitation. Wherever my fantasy leads me, the  the idea of habitation must be there. So, this means first that I have to challenge the whole program of habitation before I even think about what formal condition can apply.


LW: But the program of habitation is not necessarily in pragmatic terms like, here's the bathroom.


RA: Hey. Maybe it is.


LW: Maybe not.


RA: In the *House Without Rooms*, there is a toilet, and it is monumentalized. In a house without rooms it will have a completely different meaning than in a regular house, when it's in its own room. You cannot say that I'll make a house where you cannot shit. Then you would have to make an outhouse.


LW: As soon as you say you can't do something, it kind of inspires me to make it possible to make a house where you can't…


RA: Yes, you can shit on the floor.


LW: Or out the window or something else.


RA: Well, however you resolve it, you cannot ignore it. You can make a room with out light but not because you forgot the window, but only because you didn't want the window. This is a critical point.


LW: Back to the computer. It is, so they say, just another tool, like a pencil.


RA: Yes, it's a tool but in a different way. Since it has no tactile dimension, you have use with different expectations. If I use a computer, I use it like a surveyor uses his surveying instrument. For example, to store information. Or, to transmit information. I can measure things. If I have a very complex geometry, it is a very useful instrument. It is helping me with the building that I'm designing now (the *Musicians' House*), in locating coordinate points in the cylinder. Working with a computer—with an assistant!—for many, many years, I know exactly what a computer can and cannot do. And it cannot think.


LW: No, but neither can a pencil. A pencil can't think.  You have to guide it.


RA:  Many of the younger architects today let the computer think.


LW: It's the software that ‘thinks,' which means the software engineer, the person who designed the software, who thinks. He, or she, is a kind of silent design partner.


RA: Yeah…think.


LW: Yeah, but that's not the ultimate limit of the computer, just a lazy way of using it.


RA: I would be interested in the computer if computers could think. I mean to a certain extent they can, say, in chess. In chess where they are maybe not a hundred percent predictable tools.


LW: You know I'm surprised you say that because I would think you would feel the opposite. The computers that think are the ones that produce these awful buildings. These are softwares, Maia, Rhino. You hit a button and it generates these things. That's not what you mean by thinking.


RA: No, that's not thinking. It's reproducing.


LW: It's something that the computer comes up with on its own….


RA: Yes, I know, but it's based on an already existing….


LW: Software program.


RA: I'm talking bout thinking that is unpredictable. A computer cannot be unpredictable. It's always predictable.


LW: Only the person using the computer can be unpredictable.


RA: I'm talking about artificial intelligence. I would be interested in artificial intelligence where the computers would be my enemy.


LW: Your adversary, like chess.


RA: My adversary. Some really good players have lost. And chess is still almost infinite. Thinking is infinite.


LW: I just want to leave the possibility—I think we would be fools if we didn't—that the brave new world that we're entering of simulation and non-tactile experience has the possibility of producing an architecture with humanity, with imagination. And that it has an actually leading to building, just not in a way that we're used to doing or thinking of it.


RA: You see, I'm not a sociologist and neither are you. So I think that since we are here sitting across from each other what we haven't touched upon at all is our work. Our own work. That it is something that you know more about than society. There are people more brilliant than you and I who have investigated the condition of society, the implications of the computer and all that. What I try to avoid is to engage in platitudes, that is, already known knowledge. That you can read in books. What is unique is what you do and what I do. And what you and I do is worlds apart. So what connects us is the spirit of our work, but not the specifics of our work.


LW: All right. Let's move to our work, what drives it.


RA: We both work within the discipline of architecture and we have already touched on its limits—I don't think it's unlimited. Also, I believe, it's based in origins. Every discipline has origins. Another aspect of architecture is that it has to be inhabitable. There must be something latent that triggers your desire to inhabit it. If it is removed from that desire, it does not interest me. Then there is something else that I believe, that architecture always had the most elemental function of providing solitude. In the complexity of utilitarian life—ultimately cities are described by utilitarian forces—when you walk into a church, or a temple, you feel like you've entered the world of solitude. I don't believe it is the role of architecture to reflect upon the disorder, the earthquakes, explosions, all that they explain—I don't believe architecture should celebrate that, because it is happening anyhow.


LW: That's exactly what I've been working on, as you know. But you think architecture should celebrate solitude?


RA: I would say so.


LW: How very strange, that idea. I think architecture should celebrate people's connections with each other, and those are always fraught with difficulties, confrontations, as you would say, even crises of various kinds. Many of my projects have addressed these most difficult conditions—I don't think celebrate is the right word. Architects generally avoid these kinds of conditions. I've wanted to engage them, because architecture is always implicated. In war and its aftermath—reconstruction—in earthquakes, its falling buildings that kill people….


RA: Ok, ok—but  let's talk about our specific projects, because they have a different kind of evolution. The work that you did for Sarajevo was formally closer to the work that I'm doing than what you're doing now. And you are talking about installations as ‘vanishing architecture.' All my installations have been an engagement with the body or the absence of the body. The physical engagement. The first one I did was a black room. Just black, with a few telephone books. There was a camera recording it every fifteen minutes and at the end of twenty-four hours the telephone books had been destroyed. It was simply a place of solitude. Anybody could enter and leave whenever they wanted for twenty-four hours…


LW: And tearing up telephone books is what solitude did to them?


RA: …and then the next two installations were the *Zero-Zones* and the *Hyperspace*. I combined electronic devices in total space, where your presence triggers these devices and they produce sound, or cold, or whatever. The point is, it was a body that entered that space became part of it. The physical presence was not a visitor,  not an inhabitant, but an active ingredient of the space. An activator.


LW: Let's go on with the idea of habitation. For me, habitation can't—or maybe shouldn't—always be defined in advance. That's different from most architects, who try to anticipate what people will do in a space they've designed, and the only way they can do that is rely on what people already do, their habits. So, we know how to live in a normal house with assigned, pre-determined ways to think and act in different rooms—kitchen, bedroom, bathroom. If we want to design a new house, we shouldn't bring the old, habitual ways of living—the new house should provoke new ways to inhabit space. In the Berlin Free Zone project, inside the existing buildings in the Mitte, I proposed new kinds of structures and terrains that you physically can't inhabit in any conventional way, because the new social situation demanded new ways of thinking, and the ways we think and live day-to-day are very related….


RA: Yes, but….


LW: Let me finish please. In places where change creates a crisis, there's a strong desire to return things to normal, by restoring things to the way they were—but it's not always possible, or desirable, because the crisis has changed conditions, relationships. In those cases it's best to to create something new, including architecture. The challenge I've taken up is how can architecture—the design of space—aid or become instrumental in innovating, inventing, new ways of inhabiting space.


RA: OK, but on the other hand, there are different kinds of habitation, I cannot physically inhabit a painting, but when I inhabit painting mentally, I'm physically passive. So, when I talk about habitation, I anticipate how I would inhabit that space, as new, as radical as the space might be. But one has to feel the latency of the absence of the inhabitant—that's basically it, you see? I can inhabit a Pollock only mentally, but not architecture. Now, I'm asking you, your latest drawings for example, they're made of lines, no? I mean for me….


LW: Think of the lines as boundaries, edges defining spaces.


———————————————————————————-


RA: I'd like to talk about one of my favorite Sarajevo projects you've made—the houses with the….


LW: Curving beams….


RA: Which have a tension cable that, if they were relieved, the house would be flying. That project has all dimensions of architecture—structure, kinetics, habitation. That is something that I respect. But your latest work—if I cannot enter it anymore—is a problem for me.


LW: I understand. There is a big difference between what I'm doing now and that project. I have to say that I'm following my intuition, following ideas where they lead me. Certainly what I did in Sarajevo was very particular to the place and time. When you mentioned earthquake, you had obviously seen the models and drawings I did related to earthquakes—my attempt also to visualize an architecture shaped, in a constructive way, by them.


RA: May I interrupt you?


LW: Yes, ok.


RA: I think the real earthquake it's more authentic than simulating an earthquake.


LW: I didn't simulate an earthquake.


RA: Oh yes, it was simulated.


LW: No.


RA: Oh, of course, come on! I know the images you showed in the lecture you gave at SCI-Arc. It was recognizable as a spatial formation caused by an earthquake.


LW: Right, but the earthquake doesn't make ruins but a new type of architecture created by unpredictable forces that are released by an earthquake. It's an architecture that uses those forces instead of resisting them. That was the whole premise.  And it allows those forces to create shifts within a tectonic language. In my drawings and models everything is tectonic—it is a world of architecture, a new world.


RA: But those unpredictable forces also happen in this world. I believe that architecture's function is to provide an escape from those forces and not a celebration of them. Ultimately, what is deconstructivist is the celebration of those forces. But you cannot have a fear of the right angle, of straight lines.


LW: Well, the right angle is weak in an earthquake. The right angle is good for resisting vertical forces, like gravity—a column and a beam at a right angle is beautiful. But as soon as you have lateral forces, which are most of what an earthquake releases, the right angle fails, unless to is braced, reinforced.


RA: So, the right angle works.


LW: Yes, it can be made to work better. But do we want to be only defensive? Couldn't we begin to think how to work with natural forces and not only against them? Especially in seismic zones where earthquakes happen frequently? In the earthquake projects, I've visualized what an architecture of these regions might be—a kind of indigenous architecture. There're not a celebration of destruction, but a way of living with nature, rather than against it. As an architect, I'm interested in taking what life gives, a lot of which is unpredictable and difficult, and making something positive out of that, rather than trying to impose an idealized abstraction.


RA: But if I were to imagine that I would be exposed all day long to spaces which would make me aware of the complexity of life, of complexity of forces, of what destruction an earthquake can cause, I would rather run, run, run until I find a meadow like that one (pointing outside the cell) where I could be free of all that. I strongly believe that architecture has to provide a world where you can think.  And not where you are challenged by the forces  you fight all your life. I fight forces.


LW: Architecture is supposed to create a refuge?


RA: Yes, exactly, a refuge, absolutely. How a refuge might look is something else. I have no preconceptions.


LW: Architecture, for me, has to create the space of confrontation. You've spoken of this yourself. Architecture has to formulate the ground for inventing new ways of living, because change has made the old ways obsolete….


RA: But now, you see, that is the power of the place we are. First of all it's in the landscape. You feel it immediately. La Tourette is completely in the right spot. It creates a landscape. It is in equilibrium with the landscape. You see anything you do in architecture is the violation of the site, like when I described the first letter touching the paper. You're dealing with the site like a cosmological condition that is exposed to the stars, sky, nature—and then you intervene with this building. The role of architecture to create a new equilibrium. The only justification for kilingl animals is for food, and to cook them well.


——————


LW: This summer, Aleksandra and Victoria and I were traveling and we ended up in Milan for two days, and we went to the Sforza castle, where there is a fine museum. And they have the last pietà by Michelangelo—in the last space in the museum. This sculpture depicts the virgin standing, holding up a slumping figure of Christ. They're both facing forward and she's holding him up from behind and he's terribly thin and emaciated. It's a very moving scene—mother and son. The most interesting part, though, is the way Michelangelo had made Christ's right arm. His right arm in a kind of twisted position where the virgin is lifting and he carved it making a beautifully finished arm and hand—he was almost 90 years old when he did this, chiseling hard Carrara marble—but then he changes his mind. He obviously thought that the arm was too far forward so he carved another arm behind this one—and then he died. The pietà it was unfinished—but there is an arm is suspended in space and it's the most uncanny thing because it's not about Christ moving or anything like that—it's about the transformation of Michelangelo's thinking, and the kind of courage he had to make that decision, after all the work he'd already put in.


RA: Michelangelo never inspired me. Never. It's true that history inspires me. I'm a contemporary architect. When I look at a Palladio building, even the most beautiful ones, I say, sure, this is great. But when I first saw the Villa Savoye, my heart beat faster. As it did when I arrived at the building. This is something we talked about, the contemporary and history…


LW:  The Michelangelo is contemporary for us.


RA: I was in Sardinia and we found this water temple. It was one of the most reduced, minimal architectural structures I've ever seen. There was a triangle in the ground, it was maybe 15 feet on a side, and in the triangle there was a stair going down, and the stairs became a triangle and the walls followed that triangle so it was a very complex and inverted pyramid going down. And then on the outside of the triangle it was a hole on the ground and the hole was a light source for the water, because it was a water temple for the water which was at the end of the stair. So you couldn't photograph this.


LW: No, I'd say.


RA: There was no way to photograph it. Well, for example the Pantheon is another of a certain few, because those historic buildings, we're talking about building in architecture, which have that connection to the origin, to digging a hole. If you imagine, you can invent light by having a dark room and puncturing a hole in it. A dark room. I think that when we talk about program.


LW: You use the word ‘invent' here and I like that…


RA: No, but I mean, when you think about the window, that's just a name.


LW: It's a habitual way of thinking.


RA: But that's very, very important. When you think of a window, you can make the newest window, and  it's still a traditional window. But when you think about the program of the window…. for the hospital in Venice Le Corbusier created a new space, because when you are actually lying in bed you are looking up. So now we take program of a hospital, a modern hospital and there are overwhelming forces of technology but what remains exactly the same is the patient lying in bed, in pain, with hope, despair, maybe surviving, maybe dying. If this condition is the program of the hospital, then you make a new hospital, a modern hospital.


————————————————————————————-


LW: You're conceptualizing on an ontological level, from a philosophical position. You don't reinvent architecture yourself. You…


RA: What do you mean we don't reinvent architecture?


LW: You don't reinvent it every time you sit down.  Because a certain amount of knowledge is already a part of your being, and you bring it to the activity of drawing, of design.


RA: I think that one has to measure. And I think one would be lying if youwould not be aware that you strive for something original. And what does original mean?


LW: Exactly.


RA: It comes from origins. So that means, you always challenge the origins of the language that you're speaking. For example, take music. In the indigenous condition, you take an instrument and start playing. That means you rely on your ear. The moment you compose music, that comes very close to the translation of architecture, of an idea into a line, the first line you draw. When you compose you make a note. For me this is the most magic. The moment that something is drawn, is at that moment in sound. You see, Charles Ives never heard one of his symphonies performed. So can you imagine the complex symphonies with so many instruments and he is able to anticipate these sounds while he draws little dots. Just as it is in architecture—the drawing is the anticipation of the building.


LW: I think the drawing aspect of architecture is something that begins with the desire to be original—but let's not to go back to the original.


RA:  You don't go back to remain there, you go back to depart from there. It is always a departure, always a departure.


LW: But we are points of origin ourselves.


RA: But we are already, how do I say it, diluted with knowledge, with experience, habits so we are not true points of origin. I'm talking about very simple, absolutely simple things—a line imposed over the landscape—digging a hole. They don't limit you formally to anything, but they are the origins….In geometry, the ideal condition isn't even drawn—it's written.


LW: Euclid's theorems.


RA: All theorems in geometry are written, not drawn. You cannot draw a tangent—it is impossible. It is physically impossible. But, you can say that a straight line touches a curve at one point, even though you can never draw it. When you draw it, the  knowledge of it's impossibility hits you. That is an origin.


LW: First of all, knowledge is not something that can be transmitted by any means. We have to create knowledge, all the time. So, what you just said is something you just created. Yes, Euclid existed and wrote his theorems, but they are just words, not even ideas, until you give and I conceive of them. If no one ever thought of them, they'd cease to exist as knowledge.


RA: It's funny that you should say that because I thought that knowledge is the only thing that can be transmitted.


LW: No, it cannot.


RA: What is transmitted?


LW: Certain evidence that someone once created knowledge—texts, drawings, music, architecture. scripts—but they're nothing until you or I or someone else takes them up and invents them again.


RA: I'm not talking about imagined—I'm talking about transmitability. We couldn't talk to each other if we couldn't recognize the grammar of the English language. That's knowledge. It's what we learn. What you cannot learn is creativity, imagination.


LW: English grammar and geometry theorems are textbook stuff—empty until we fill them with—exactly—creativity and imagination. It isn't knowledge until we do that, just potential.


————————————————-


LW: Lets shift a bit into the question of cities. New York City, where we live, where the streets, the people, the life, the whole physical construction of the city is so complex.


RA: Physically.


LW: It's complex on so many levels. Wouldn't you agree that New York is not just physically complex? That it's also complex as an idea, as an expression of a manifestation of lots of ideas. It's an accumulation, not just a single decisive work, therefore complex.


RA: Yes, of course, any organism of that sort is, but it's more of a physical complexity that one of spirit.


LW: I'm always amazed when I look at the city when I look around and think my god, my god, what is this place, what is here that I don't even imagine—the lives of people.


RA: I now the city best from my studio. The city has changed dramatically in the 40 years that I've lived there. Dramatically. When I arrived—I will never forget this—I lived on 1St and 23rd Street in some friend's loft. It was a Sunday. I went on the street, 23rd Street, and it was covered with garage. The entire street.


LW: Probably a garbage strike.


RA: No there was no garbage strike. There was not one trash can in the whole of New York City. Not one trash can. And I was coming from Europe and it was such a feeling of liberation. The anarchistic aspect of New York—that's what I felt. When it was dangerous and dirty, it was completely different from what it is now, when it is safe and rich.


LW: I don't think it's so safe.


RA: Really?


LW: Well it's safer.


RA: Oh, come on! When I used to go home at night I always used to walk and look back to see who was behind me.


LW: That's not what I was alluding to. Isn't there a profound sense of mystery in the human condition in the city. Just the mystery of it—the life, death and the density.


RA: So, all you can do is to recognize the mystery.


LW: Why can't that become part of your origins of architecture, too?


RA: I think that when one works one isolates oneself form everything. Totally solitary. And that influences…


LW: Of course, there are certain moments….


RA: Cezanne left Paris when it was thriving. He went out into the countryside, rented a room and painted a mountain 300 times. He didn't go out there to invent modern painting. He invented it, but just going out there away from the city. So, there is absolutely no ideal place to work. I was attracted to New York from the first time I saw it. I feel comfortable there.


L W: But it doesn't inspire you?


RA: Of course it inspires me, I only live in a place that inspires me. When Vienna stopped inspiring me I left. New York is still inspiring me but I don't want to analyze what inspires me. That mystery you are talking about must remain a mystery. I made a contribution to it with my building in midtown. I've proved that a single building can radiate a spirit that changes the mood, or creates a different mental place. The most amazing thing that I didn't even contemplate was how the smallness of that building overpowers all the larger buildings around it. But there is a view from above, from a sky scrapper across and you see that building rising among huge, gigantic buildings and it was much more powerful than any of those. And they were indigenous buildings made in the mystery of the city. So it's actually you know, just the opposite: a totally solitary, precise building proved to be stronger than the quantitatively larger, mysterious parts of the city.


LW: The mostly anonymous parts.


RA: I'm lucky I succeeded. There are other examples—the Guggenheim—buildings that were complete, because they were in the dimension of the mind. They weren't governed by real estate,  or by all the other forces that make the city. The city was made from pure speculation. The first skyscrapers were made by wealthy landowners who bought strategic lots and ten blocks all around them to prevent anybody building the same size skyscraper too close. It was pure speculation. The first one who really made a space of spirit was Olmsted. It's almost unthinkable that today, with the power of real estate speculation, that any mayor would approve a Central Park. But it wasn't part of the indigenous, the anonymous forces shaping the city—it was a highly intellectual, courageous, visionary idea.


LW: Yes—let's take the Guggenheim as an example….


RA: Let's not get bogged down by examples.


LW: Let's not get bogged down at all. The power of the city is expressed in buildings, in their collective density, in the compressed spaces between. We can debate whether those buildings are architecture or not. Still, taken together, they are very moving. They don't express the kind of solitary, self-conscious philosophical idea you're talking about, but they do express an incredible spirit of the human, in a way greater than what one mind could conceive or create. I can't believe that architecture is only the singular, great buildings….


RA: No, Lebbeus, that's not at all what I said.


LW: No?


RA: No—I just wanted to make a distinction between being fully conscious in your actions and simply participating in the mystery you discussed by just make another part, just being absorbed in that mystery.


LW: Many architects are….


RA; I'm talking about the conscious process of making architecture versus the indigenous process of making building. That's all. You should not get too trapped in examples. We should talk about philosophical arguments instead of examples. Analogies are very dangerous—you can use them to prove anything you want. This versus that—and this is absolutely seductive and absolutely rejectable as a philosophical, a critical argument, because you cannot dissect a comparison.


What has interested me is to be in a building that is literally sacred. Now there's always the cliché that architecture's ultimate aim is to profane. That means to separate the sacred from the profane. This was the way in the beginning of the church to separate the two worlds—the profane world of the everyday and the sacred world of the church. But I think, in architecture, it is just the opposite. You cannot profane architecture because I think architecture becomes architecture when the utility—part of the profane world— incorporates the sacred. I'm talking about the secular sacred, not the religious sacred.  So, this means when you go beyond utility, you reach a level of sacredness. It is a fusion, a dialectical condition between two worlds that has to take place when it becomes architecture. So, in La Tourette, there is a complex of utility and the sacred. We have seen details that have caused the building's decay, which is part of it's utility. The construction struggles against entropy. But, if you want to retain the sacredness, you have to be aware of the utility. See, that's the crucial element of building—translating from the ideal, which is the drawing, to something that is built.


——————————————————


RA: When I build—no matter how big the building is—I build it like I would a chair with my table saw. Except I don't have as much control over it. But still, I have to exercise as much control as I can. I can see half an inch, I can see it. It doesn't mean you have to have a certain obsession, because it's a translation. Also,when you build you have to anticipate how materials will be over time. That was one of the great accomplishments of the Greeks and Romans. If you read Vitruvius, it reads like an adventure story for materials. At that time his knowledge, you had to be a geologist to be an architect, a scientist with the knowledge of how materials behave. It was all incredibly simplified. But when a concrete truck comes in to my building site in Germany, I'll take a look at the concrete and if the concrete is not perfect than I say why is that discoloration in the concrete? Maybe they didn't wash the truck's mixer out, so I say wash it out. That's why I'm so impressed by the neglect of this attitude at La Tourette. It's neglect. I don't know if  Corbusier wanted that lousy condition of the concrete, which gives the concrete a certain power. In comparison, the concrete that I absolutely despise is that of Ando, which is concrete that is impeccable with no fingerprint. Concrete has to be alive. But what I'm saying is that this is most directly in the hands of the guys who performed the work.  

LW: You have to assume that Corbusier wanted it that way.


RA: I'm not so sure.


LW: Or, he just couldn't make it to the site when they were pouring.


RA:  It's possible. Maybe he had a lousy contractor.


LW: If he did have a lousy contractor, it would have been all the more reason that he would have been at the site for the pours.


RA: He had a scientific mind. That he didn't care what the workers did it would surprise me. I mean, even the formwork was all funky.



 RA: I reject the word conversation.



LW: I think to converse is to speak together.


RA: But we don't speak with. We speak against.  

LW: You think that's what dialogue is, speaking against?


RA: Oh, absolutely.


LW: Speaking against the other person?


RA:  No, not the person—the person doesn't interest me. You don't interest me at all when we talk. Only what your argument is. I'm listening to every word and I'm trying to respond as precisely as I can. What meaning it has, what other thoughts it triggers. I'm only interested when I speak to somebody if it triggers my imagination to  have another thought. I can never defend a position only because I have worked to arrive at it. If there is a new thought, that challenges it, that's more interesting, than I would give it up immediately. I only knew one person in my entire life who also had that capacity: Arakawa. He could take off like a rocket, because none of us would defend our positions—-we were just eager for the new possibilities that dialogue triggered. A conversation is exchanging opinions, but in an intellectual discussion of architecture, I would never rely only on opinions.  

—————————————


RA: I like to continue with the argument we had in the car about food. You said that my standards for food are higher than yours.


LW: Apparently.


RA: And I said, it's not my desire, it's the food's desire, and this translates to architecture. Each material has its limits. The limits aren't known, but the limits are there. The limits of the material have to be discovered, which means that the material sets the limits, not me. If I don't understand the limits of the material, I cannot build. That's what Kahn's comment on the brick[[1]](#_ftn1) means. He asked the brick. The aim is to push to the limits so you can find them. You always have to recognize a new possibility at those limits, and that's where something becomes new.


LW: Just like the gothic cathedral with the stone and the technique of vaulting. They pushed them higher and higher. In Notre Dame in Paris they went to 112 feet, and in Beauvais they went to 190 feet and the vaulting collapsed. They fpund the limit.


RA: Yes, but the only the structural limit. When you build, each material has different dimensions. One is surface, one is depth. One is structure. Structure may be the most obvious one if something breaks. But there is also another dimension which is maybe just as important, which is whenthe material looses it's essential qualities because it is misused.


LW: A primitive mason will use stone at the lower end of the materials limits by just piling it up. With a higher degree of craft, the master stone mason will be able to use it at its upper limist.


RA: But there is another limit. The formal limit. That means when you elevate it to the level of architecture. The mason then becomes an architect. So when this happens, it is at the threshold between architecture and building.


LW: Yes.  With stone it was…


RA: With anything. It doesn't mater. You see we are now in a time, where it seems that the new range of technologically advanced materials make what we can do almost unlimited. Which is true formally—you can do whatever you want with the computer. But I'm interested for example in ancient materials like wood, brick, even mud—to make a new mud building in our time is more challenging than using new materials. There was at the beginning of the heroic period of modern architecture the sudden possibility that there were new material that would result in new formal conditions. You had the cantilever, which was impossible before. From that moment on the cantilever existed, so now, if your aim is only to extend the cantilever to the maximum, it doesn't mean anything. It was stronger at the moment when it was invented. It's like the Brooklyn Bridge—the first suspension bridge. But because you had the dialectical condition of the brick, the stone, the compression of the supports, and the new material—steel—this dialectical collision made the Brooklyn Bridge stronger than any modern version. That's my position.


LW: This more of a dialectical position than it is technological.


RA: But that's what it meant to go back to the food, to the materiality. Inn any discipline, if you want to invent something new you have to go always to the origins of things. Always. In the written language it's etymology—you have to know where a word comes form, because its use is habitual—you don't think about it, and it loses it's original….


LW: So you think.


RA: You cannot find the limits of a written word if you don't know the moment when it was first uttered or inscribed.


LW: That sounds good, but how can you actually know that? I would rather rely on intuition. But in architecture, one doesn't go back every time to the origins.


RA: Always. Always. I'm always aware of origins. That's why I could say I'm not consciously influenced by any historic moment of architecture. Of course, the knowledge is more in the stomach than in the brain. This means that I'm not consciously aware. Whatever influences one can be recognized afterwards. That's not what happens when I work. But what I'm always aware of are the logical conditions of architecture. That means it's not the building, it's not the construction. It's a hole in the ground. When you violate the horizon. That is the crucial moment. Actually even before that, maybe before geometry came into the picture. It was first the making of a mound from making a hole. When geometry became the grammar of architecture, than it was the surveyor who was the first architect. The surveyor, who put the ideal line over the amorphous landscape. Measured the landscape. That is what for me, is the geometrical origin of architecture. Digging a hole and making a mound is physical. Because the actual dimensions are crucial. The physiological space and the geometric space. They are completely divided from each other. Because one is an invention of the mind and the other is inherent in nature. You grew up with touch and smell—the limits of your perceptions. When the visual and the tactile collide, is where you have architectural space.


LW: Let me just say I find all of that very appealing, and the part about origins very romantic. You probably didn't know that one of my first published books was titled “Origins.”


RA: No.


LW: But the origins I was concerned with were more mythical than what you describe, which are really pre-historic, even pre-writing, even pre-language. A lot of my early projects were inspired by mythologies of change, of transformation. The Four Cities[[3]](#_ftn3)—earth, air, fire, and water—each has an architecture of transformation, according to the essential material of each city, and the relationship with it of its inhabitants….


RA: Too much society.


LW: Just one of our big differences.


———————————————————


LW: We've talked about how architecture can exist in a drawing.


RA: Of course.


LW: Because it's conceived. One doesn't have to actually build every building.


RA: No.


LW: The architecture is in the drawing—it has to be. If there's no architecture in the drawing, there's not going to be any architecture in the built building.


RA: I've claimed that as long as I've taught. That architecture doesn't have to be built. Because any discipline is first an idea. You have to have an idea within the language you are speaking. The idea can be manifested, but it has to be translated. And idea is dimensionless. You can have the greatest idea in the world, and if you're not able to draw it, not able to write it, not able to compose it…


LW: Than you don't have it.


RA: Then it's meaningless. No, but the idea is still there.


LW: I'm not so sure. What could be an idea that can't be said, that can't.…  

RA: An idea is that if a child looks at a chair and thinks it's a horse—if the child engages with it, puts it upside down…. But the idea—you can dream. That is what dreaming is all about, daydreaming. You decide, you create your own reality. But this reality can vanish at any moment unless you decide to try to make it physical. That means to write architecture, to draw architecture, to make models.


LW: I can't have an idea divorced from the act of doing it. I don't daydream architecture, I don't think architecture until I start drawing.


RA: I wouldn't touch a pencil if I didn't have an idea, I would be doodling. Then of course…


LW: I don't doodle, looking for an idea. Drawing and the idea come together….


RA: Wait, let me finish. When I labor in drawing, I'm fully aware of what pencil I use, what paper I use—but for me, an architectural drawing has to anticipate the built. Other wise it's a picture.


LW: Well, I agree with that. The idea that's drawn is ultimately about the built.


——————————————————


LW: You mentioned at Ronchamp that you know the context is important, so the landscape around the chapel was originally a barren hilltop and there was no reception center and no flower pots and not so many trees planted to soften it. So, with your Musikerhaus your music house, what's to prevent it from suffering the fate of Ronchamp?


RA: It will not. It will absolutely not.


LW: But that's out of your control, right?


RA: Yes. Let me tell you something. After I die, I couldn't care less what happens. In Austria I built my first house by myself in 1962— before I went to America—and worked with a friend of mine who was a photographer, and he died some years ago and I don't remember who inherited the house. Now I get this phone call from some students who want to create some movement, because they heard it was going to be sold and new owners want to tear it down, or mutilate it; they want to protect it. I said to them that I'm really touched, really touched, but ultimately I could not care less. Of course, I would prefer that they tear it down rather than mutilate it.


LW: I see.


RA: Because I'm still alive, I still can judge it. But deep down I don't care.


LW: I think you talked about memory earlier.


RA: Yes, but I did that house before I really knew shit about architecture.


LW: But there is something there, in the house.


RA: I know, but when I say built architecture, it was already before the time when I drew my drawings, and it was still a rather conventional house. Maybe in the context of Austrian architecture it's an important house. I don't care. I only measure it within my work. I don't evaluate my buildings in terms of historical significance. They have to meet my criteria of criticism, that's all. You have to be very honest about that.


——————————————————————————-


RA: So, an installation is an installation. It lasts as long as the exhibition is and then it's gone, a kind of vanishing architecture.


LW: I don't like the term ‘vanishing architecture.' It's a dismissive term.


RA: No, no.


LW: There is architecture for the ages, like the Acropolis, and then there's this vanishing stuff. I think the real vanishing stuff is simply construction.


RA: Temporary.


LW: Temporary is fine.


RA: Temporary is pragmatic.


LW: Well, I'm an American, I'm a pragmatist.  I would like a more precise term for installations. ‘Vanishing' is more like ‘visionary.' How would you like to be called a ‘visionary architect?'


RA: We have both been called visionary architects.


LW: I know. And we didn't like it.


RA: And we didn't mind it. either. Have you ever protested to be called a visionary?


LW: Yes, I have.


RA: In my first participation in a show at the Museum of Modern Art was called ‘architectural fantasies,' which I hate. That I absolutely hate.


LW: Fantasy, visionary—I don't see much difference.


RA: No—visionary is far better than fantasy.


LW: Really.


RA: Because it's not fantasy, you see.


LW: I consider myself to be an architect and I make designs and occasionally I might build something in some form, or not. The building part is not up to me.


RA: But Lebbeus, visionary isn't a derogatory term. A fantasist is.


LW: Hmmm….


RA: Because when you make a drawing, or I make a drawing, that is not a fantasy at all—that is reality.


(end)


*Below are images of several projects by RA and LW referenced in the discussion, taken from the layout of the ANTI book:*


[![](media/highhouses.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/highhouses.jpg)


*High Houses* in Sarajevo, by LW.


[![](media/earthquake.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/earthquake.jpg)


*Earthquake Architecture*, by LW.


[![](media/ra-inst-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/ra-inst-2.jpg)


*Telephone Book* installation, by RA.


[![](media/ra-inst-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/ra-inst-3.jpg)


*Zero Zones* installation, by RA.


[![](media/ra-houses.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/ra-houses.jpg)


*House without Rooms,* (on the right),by RA.


[![](media/mhaus-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/mhaus-1.jpg)


*Musicians' House*, by RA.


[![](media/mhaus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/mhaus-2.jpg)


*Musicians' House,* by RA.



 ## Comments 
1. [Salvatore D'Agostino](http://wilfingarchitettura.blogspot.com/)
9.22.11 / 4pm


Lebbeus,  

with a real example of dialogue (crisis) architects.  

Regards,  

Salvatore D'Agostino
3. euphemus
9.23.11 / 9am


This is beautiful, beautiful, beautiful.
5. Michael Cranfill
9.24.11 / 5pm


LEB, I have had the fortune of staying the night here on several occasions, both when I was much younger and now. I spoke often with one of the monks who was there during the construction. The monks were very involved in the actual construction work and some insights came from our conversations. For example the joint lines in the parapet at the top of the large sanctuary appear as cracks but indeed were layed out in a particular rhythm by Corb and placed as wedges so that the joints would crack “naturally”


I am including several comments by Corbusier for your readers who may be unaware of Corbs wrightings as he was prolific in that regard. Your discussion in light of this, has a great bearing on the work at hand…. while it may not appear at first glance .Best regards.


 Le Corbusier: Nothing is transmissible but thought, 1965  

 In 1950 I had a feeling that a page of life was being turned over: the end of a world, both immanent and imminent…., a great page in human history, the life of the men before the machine age , the life that the latter has shattered, ground up, pulverized. Example in the USA, in New York a city of 15 million inhabitants, the horror of an affluent society without aim or reason. Real-estate is sold on a plan basis, a block at a time…There is excavating in all directions….without pity, without playfulness. In Chandigarh: possible contact with the essential delights of the Hindu philosophy: stars, all of nature, sacred animals, birds, monkeys, and cows and in the village, the children, the adults and the still active old people, the pond and the mangroves, everything has an absolute presence and smiles, everything is miserably poor but well proportioned…


The Open Hand is a symbol of peace and reconciliation ….harmony is possible among men. We must cease preparing for war….we must invent, decree projects of peace. Money is nothing but a means. There is God and the Devil– the forces confronting us– There is still time to choose to equip ourselves rather than to arm ourselves. Open Hand, open to receive the wealth of the world to distribute it to the peoples of the world, ought to be the symbol of our age.


Life can be born in our plans. We must rediscover man. We must rediscover the straight line wedding the axis of fundamental laws: biology, nature, cosmos. Inflexible line, straight like the line of the sea. The trained Man too, inflexible line ought to be an instrument for measuring things, capable of serving as a level, as a datum line in the midst of flux and mobility. That is his social role. Morality: not to give a damn for worldly honors, not to reach for cash, …to act only in accord with ones own conscience  

 I have been inspired by one single preoccupation, imperatively so: to introduce to the home the sense of the sacred: to make a temple of the family. I have devoted fifty years to housing. I have brought the temple to the family, to the domestic hearth. I have reestablished the conditions of nature in the life of man.  

—————————————————————-  

“Realizing how much our world was convulsed by the birth pains of the machine age, it seemed to me that to achieve Harmony ought to be the only goal. Nature , man, the cosmos: these are the given elements, these are the forces facing each other.”  

———————————————————-  

“The work of nature which proceeds from inside outwards, uniting, in three dimensions, all the diversity, all the different intentions in perfect Harmony…..a profound primordial function animating even the lowest cell of organic life”  

“in nature, the smallest cell determines the validity, the health of the whole….by restructuring the city, the cell of the social body….the family would be restored to its previous health”


The Monastery of Ema  

“A radiant vision, a modern city dating from the fifteenth century….the noblest silhouette in the landscape, an uninterrupted crown of monks cells: each cell has a view on the plain, and opens on a lower level on an entirely closed garden. I thought I had never seen such a happy interpretation of dwelling  

——————————————–  

As with Rousseau the alienation of things and beings can be measured in terms of a distance from the point of origin. At the outset of “The Radiant City” Le Corbusier writes:  

“I am attracted to the natural order of things, I don't like parties, I look for the Primitive Man” my primary aim is to “preserve the innocents of the landscape”  

—————————————-  

“I struggle between, on the one hand rationalism, strongly imbued in me by my active life…and on the other hand, the Innate, intuitive idea of supreme being, which is revealed to me at every step by the contemplation of nature”


“Concrete is a material of the same rank as stone, wood or baked earth , the experience is of importance. It seems to be really possible to consider concrete as a reconstructed stone
7. Michael Cranfill
9.24.11 / 9pm


There were numerous spelling errors in my most recent post to your site. I got caught up in the moment. The funniest such error was writing and ‘wrighting'. The quotes from Corbusier ‘s ‘ Radiant City' and ‘ Poem of the Right Angle' came from some research I was doing on the topic of the organic. Best
9. Gio
9.26.11 / 4am


Wow..What an interesting argument between fellow friends and Architects.


I liked the topics about Origins, Solitude, Society and Visionary Architecture.


Raimund's answer about Origins and the creation of the dark room through openings that shed light, is poetic and direct to the point.  

Architectural programs will see new beginnings that way making them original. Now I understand.  

Rightfully loved the concept by Raimund about being present : A building is a building and if after so many years somebody wants to destroy or mutilate your building ; let it be. It already existed. Like a drawing that only lasts until you say “it is done” and move to something else. It served its purpose.


Architecture without solitude is devoid of its essence and the road of The Visionary is solitary indeed.  

I also dislike the term Visionary : To have a vision, many have visions. Martin Luther King had one, and Jacob many.  

So the name should be about “One that searches for originality….origins…closer to the first form, the first sound, the first word, the first line that measure, the first sensation and as a result the first image……the root of architecture”.


I believe there will always be those who practice the root of architecture.


Some just take long to mature into it.  

I bet there are some as I write this who have struggled with the consequences for this form of architecture:  

The easy wait out, the just to pay the bills.  

Many who labor and battle with the process of creation, with the mystery as you both called it ; the new, and work the paper until becomes saturated with unrecognizable lines loosing inspiration by the minute against history, fashion or mediocrity. Few care. The stakes set to measure are really high almost of untouchable peaks.  

The battle of educating the masses, clients and society goes out the window when capitalism and state only open doors to those who from the beginning have something and by doing this the doors to mediocrity remain open.  

Many have lost it all, stability and family for this solitude which ironically depends of the interaction with others in order to become a reality.  

There is a cruel awareness, it needs to be seen and put out there to vindicate itself otherwise, becomes a beautiful image in the universe waiting to become. But as difficult as it sounds, this road must be taken in order to save ourselves from not falling into the hands of the mediocre mirage image of our present day society.  

 I am glad we have got to meet Raimund and continue to know his works through you. I am as well glad that you continue to inform us about others like yourself who share their thoughts and work for the benefit of our society.


I enjoyed it a lot.  

Thanks for sharing your trip with us.


Gio





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.27.11 / 8pm
	
	
	Gio: your comments are quite lucid and poetic in themselves. I'm sure Raimund would enjoy them, as do I. Glad you're here to care and respond from your own depth of experience and understanding.
11. Christopher Lauriat
9.28.11 / 1pm


Lebbeus,


If you haven't seen this already, it might be of interest to you and your readers.


<http://www.dezeen.com/2011/09/26/today-at-dezeen-platform-thomas-hudson/>


Chris
13. Grullone
9.28.11 / 1pm


What a post! I have reread it now and enjoy much of it, so many great moments. As a whole, though, some patterns caught my attention. 


I was surprised by the polemical attitude of RA, or, maybe, the insistence of maintaining it at times when perhaps the conversation could have gone further, but was held back. It's as if the attitude required in making experimental architecture, the search for origins, originality, etc., brings with it an inevitable disdain for….well, for everything else. Unconsciously or not. I say this from my experience wanting to do the same thing, ahem, while in school, and the way I conversed then was pretty similar to the way RA seemed to act in parts of this conversation.  

So, I suppose that what is even more surprising was the more open stance of LW, more vulnerable to criticism but at the same time more likely to be listened to. To me it felt that LW was speaking from experience and RA was constructing arguments based more on the mechanics of the situation. Maybe I am wrong, I was not there and don't know either of them. But I wonder, Could it have gone any other way? and would it have been better or worse? What is it about the act of conversation that brings this dynamic out? Because i think that it is a product of a moment more so than of the people discussing. A conversation, rather than a comparison of texts, brings out these odd and complex by-products, and they interest me.
15. [metamechanics](http://metamechanics.wordpress.com)
10.6.11 / 2am


lebbeus – thanks for putting this in print on virtual paper…


I fully agree with RA on his position regarding architecture design process: idea, draw, goal is to be built, battle with the material/realit and done, and if i'm over it, who cares, tear it down. if you don't have an idea, why are you drawing? and if the drawing isn't architecture, what are you doing? 


but this position and approach inevitably leads to what I call ‘old man' brain. RA clearly has it in this essay and the solution is always ‘solitude'…this is why many men fish, hunt, become monks, etc….in solitude we can think it all out and be satisfied. the older you get the better this looks. ultimately you become a ludite and deny anything the ‘kids' are doing, because they just don't get it. (i've decided eventually if i have a firm in 30-40 years they can fire me when I sound like an old architect)


lebbeus your approach seems to let you keep pace and remain involved socially.
17. BinMar Leto
10.9.11 / 2pm


Lebbeus, thank you for transcribing the dialects between you and Raimund Abraham at the hallowed monk cell  

of Corbu's La Tourette! I've been re-reading the exchanges between both of you…Raimund in his absolutism  

and your empathy with society. 


After your discussions at La Tourette was there a gourmet meal with a good French wine afterwards  

that had the same essence or the very least, offered levity after the intensity generated in that empty  

monks cell which you both inhabited intellectually…
19. [Бетон с доставкой](http://beton-dostavka.com.ua)
10.14.11 / 4am


[стоимость](http://beton-dostavka.com.ua)
