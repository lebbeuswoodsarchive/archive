
---
title: SHANDOR HASSAN’S SHACK
date: 2011-09-17 00:00:00 
tags: 
    - art_installations
    - exhibitions
    - found_objects
    - shacks
---

# SHANDOR HASSAN'S SHACK


[![](media/dr-text-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-text-21.jpg).


[![](media/dr-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-2.jpg)


.


[![](media/dr-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-3.jpg)


.


[![](media/dr-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-1.jpg)


.


[![](media/dr-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-4.jpg)


.


[![](media/dr-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-5.jpg)


.


[![](media/dr-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-6.jpg)


.


[![](media/dr-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-7.jpg)


.


[![](media/dr-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-8.jpg)


.


[![](media/dr-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-10.jpg)


.


[![](media/dr-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-12.jpg)


.


[![](media/dr-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-13.jpg)


.


[![](media/dr-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/dr-11.jpg)


**Shandor Hassan**


#art_installations #exhibitions #found_objects #shacks
 ## Comments 
1. [Michael Phillip Pearce](http://www.facebook.com/pearce.mp)
9.17.11 / 10pm


I like what you are doing! The roof is the most interesting exterior element, and I wonder if you changed it, would something arise and be more interesting? You have a lot of great objects up there that I think could be used on the exterior–only if you had a porch. This is just a thought next time if you found some twine or equal to secure your roof, weave it like a basket or fishing net. Great work/post thanks for sharing! Thought provoking.
3. tom
9.20.11 / 8am


I like the difference between this and most found object art works which merely case the work in amber by making it art. In this case you get immersed in it, in which many complex feelings come up.
5. Roobarb
10.3.11 / 6pm


Nice piece – I remember a version of it from the show in Chinese Characters.
7. shandor hassan
1.19.17 / 3pm


sad drunk man bun lives with mom.
