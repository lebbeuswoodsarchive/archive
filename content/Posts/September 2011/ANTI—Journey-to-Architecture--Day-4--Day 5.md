
---
title: ANTI—Journey to Architecture  Day 4, Day 5
date: 2011-09-21 00:00:00 
tags: 
    - architecture
    - journeys
    - La_Tourette
    - Raimund_Abraham
---

# ANTI—Journey to Architecture: Day 4, Day 5


**DAY 4—October 24**


Half the day was spent back-tracking by taxi to the Lyon airport—where we saw from a distance the usual soaring terminal building by Calatrava— getting a new rental car, then driving back through Lyon and finally to L'Abresle and La Tourette. It was a gray, chilly afternoon when we drove up the wooded gravel road to the high ground next to the building's entrance. The entrance was, of course, closed and locked. Inquiring below in a tourist shop, we learned that the monks were gone and the monastery had been closed for over a year. Only scheduled tours were being admitted, usually busloads of tourists who were led in groups through the darkened interiors, as through an ancient ruin of some long-lost significance, then bussed away, leaving only silence. Undaunted, we spent several hours walking on our separate tracks around the building, Raimund with his camera, me with my camcorder, exploring and documenting what we could. The building was like a decaying fortress on a lush green hilltop, defending itself and its cloistered inhabitants against the world. We were each awestruck by its sheer physical presence, and I could not help but wonder how far it was from the crisp white, optimistic buildings of the architect's younger years. It struck me, as I walked through the sloping meadows around it, with their views of a distant valley overshadowed by the building's presence, that this was an old man's architecture—but only an old man who had once been young and hopeful and utopian. It was like the last chapter of a novel that begins with high hopes and aspirations and ends with a particular kind of resignation—courageous and forceful, and not merely ambitious, but heroic. One cannot help but think of the last, too-far swim into the sea that ended Le Corbusier's life. Resignation and strength.


When we met at the car, it was already late afternoon. We realized we would have to find a hotel for the night, gather our thoughts and impressions, then return tomorrow, and somehow get into the building. L'Abresle held nothing for us, so we drove west, rather than return to Lyon, ending up in a larger town, Roanne, where we found a congenial tourist hotel—with a restaurant that was still open. Over so-so food and wine—about which Raimund complained throughout—we exchanged our impressions about the building we'd not only finally seen but had come to talk in.


*Photos by Raimund Abraham, unless otherwise noted:*


*Our hotel in Roanne:*


[![](media/ronne-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/ronne-1.jpg)


**DAY 5—October 25**


The drive to and from Roanne gave us sixty kilometers of time each way to talk and think. The landscape had none of the manicured pastoral beauty of the one around Besançon, but was fraught with billboards, gas stations, and all the cheap commercial development that as an American I was accustomed to seeing. It was a curious feature of these hours in the car that Raimund and I did not engage in the chit-chat about our personal lives that people normally use to pass the time. Rather, we were either talking about some aspect of our jouney, or La Tourette, or architects we knew or had known, or were silent. It was not that we didn't like each other's company, or were total strangers in personal matters, but that our focus on this ‘mission' was so intense that it would allow for little else. Even our talk about food and wine took on philosophical dimensions.


We rose quite early in order to arrive at La Tourrette when we imagined the tourist office would open, so we could conspire to gain entrance to the monastery. And so we did. The two lovely young women in the office were so charmed by Raimund (I'm sure) that they gave him the keys to the kingdom, or at least to the building. We would have it to ourselves for the whole day. Amazing.


It was a sunny day, the kind that Le Curbusier no doubt designed his building for—crisp shadows and sharp corners on the building's geometries, its richly textured concrete surfaces perfectly etched. His dictum, “Architecture is the correct, masterly, and magnificent play of volumes, assembled under light,” was brought perfectly to life. We wandered first around the building, which is very much an alien object in the still green autumn landscape, retracing our steps of the previous, gloomier day, taking in and documenting what we later said was an entirely different building. Then we went inside.


Wandering through the deserted, chilly corridors, following by memory the building's plans, we walked first to the cells, which lined—like hotel rooms—one side of surprisingly wide, sunlit corridors, then went on to the refectory, with its Xenakis modulated windows overlooking the distant valleys, then to the church, a huge box of a space barely, mysteriously lit by minimal clerestory windows at the high roof edge and the three famous light tubes over a side chapel. It was austere in the extreme, very monastic, with no concession to comfort or the softening effects of religious iconography of any kind, except for an unadorned iron cross that seemed more like a farm implement, or an instrument of some kind, than a sacred symbol. In this space, more than any other, I was reminded that Le Corbusier was not a Catholic, nor even a practicing Christian, which did not matter to his patron, Father Couturier, who considered the best of modern art as inherently sacred—a radical position vindicated, I felt that day, by the awe-inspiring severity of this church—a pure act of architecture.


Finally, we returned to the monks' cells, selected one with a table and two chairs, and settled in to talk. Our first session lasted for two hours; then we left for lunch in L'Abresle, and returned for a second session that lasted until the late afternoon. The particulars of our discussions can be found in the edited transcripts of tapes we made. For my part, I felt the ‘pressure to perform' rather overwhelming, especially in this great, haunted building. Here we were, finally, at our destination, with only a few hours, rather than days, as we had originally planned, to accomplish our mission of arguing our differences as architects. Raimund proved to be more adept than I at dealing with what I felt was the staginess of it, which in itself says something about our differences. Still, as the transcripts testify, we managed to open some critical questions that we both agreed at the day's end we would pursue when we returned to New York. Driving away from La Tourette, knowing that we would not see it again, at least on this trip, but also, perhaps, ever, reduced us both to silence on what seemed the interminable drive back to our hotel in Roanne.


*“I did not come here to take pictures of La Tourette,” Raimund declared when we finally arrived. But he could not help himself. The visual power of the architecture, outside and in won out over his fear of making redundant images of one of the most photographed—and photogenic—buildings of our time. As these photos show, he underestimated his abilities, as each bears the unique stamp of his sensibility. All the while, I was making videos, which will have to wait a while to be seen:*


[![](media/lat-14.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-14.jpg)


.


[![](media/lat-18.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-18.jpg)


.


[![](media/lat-15.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-15.jpg)


.


[![](media/lat-16.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-16.jpg)


.


[![](media/lat-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-11.jpg)


*(below) The bell-tower  of the church:*


[![](media/lat-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-12.jpg)


*The oddly obscure entrance to the Monastery of La Tourette:*


[![](media/lat-17.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-17.jpg)


*An interior corridor:*


[![](media/lat-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-10.jpg)


*The interior of the church:*


[![](media/lat-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-9.jpg)


.


[![](media/lat-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-6.jpg)


*Views into the church's side chapels:*


[![](media/lat-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-8.jpg)


.


[![](media/lat-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-7.jpg)


*One of the many cells, where the monks studied, prayed, and slept:*


[![](media/lat-20.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-20.jpg)


*Raimund Abraham and LW talking in one of the cells. Taken from the video by LW:*


[![](media/lat-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/09/lat-1.jpg)


*.*


*The post immediately following this one is comprised of excerpts from a transcript of the audio-taped discussion between RA and LW, made in the cell at La Tourette.*


LW


#architecture #journeys #La_Tourette #Raimund_Abraham
 ## Comments 
1. BinMar Leto
9.22.11 / 12am


Lebbeus, Thank you for your posts! Your ANTI-journey to Architecture with Raimund  

is a discourse in itself! Looking forward to the conclusion, yet I am certain it is on-going  

and should not end…
3. Gio
9.25.11 / 3am


Mr. Woods, Thank you again for your post.


Really good pictures. Would you be sharing the videos too? 


Gio
