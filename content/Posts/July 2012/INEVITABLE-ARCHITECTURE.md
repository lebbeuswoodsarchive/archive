
---
title: INEVITABLE ARCHITECTURE
date: 2012-07-09 00:00:00
---

# INEVITABLE ARCHITECTURE


How important is it—if at all—for architects to consider the ultimate decay of the buildings they design?


After all, it is the new building that realizes best their ideas, hopes, aspirations, and the prospect of its being diminished over time amounts to their diminishment, as well. Most architects dislike the idea of buildings' decay and work hard to avoid it by the careful selection of  materials, systems, and methods of assembly that will withstand the forces of nature continually attacking them, chiefly those of weather. Nevertheless, there is a tendency to decay inherent in materials and systems themselves—an entropy—that no amount of care in design or maintenance can overcome. Buildings will inevitably decay, and there is nothing architects or those charged with a building's upkeep can do about it. So, what is an architect to think or do about it?


The most common thing is to forget about it. Or, to put it in psychological terms, to deny it, much as we put out of our thoughts our own inevitable decay and extinction. We tend to proceed in life as though we will live forever, thereby remaining optimistic enough to believe what we do has some enduring value and meaning. Without this capacity for denial, most would become paralyzed by despair. If architects did not believe their designs had some enduring qualities. it would be difficult to believe in what they do. So, even the designer of temporary architectural installations believes they will endure through various forms of documentation—photos, film, even reconstructions—and thus finds sanctuary in denial.


There is, of course, another, less common and more difficult way, and that is to embrace or at least accept decay from the start.


I personally find the Romantic fascination with ruins problematic. From Capar David Friedrich to Albert Speer (Hitler's architect and town-planner). the evocative power of ruins has worked to produce powerful emotions, often for ideological—religious and political—purposes, making the motives exploitative, at the least. As a marketing device, nostalgic emotions of loss can sell paintings and politicians and their policies, but do little to advance knowledge.


Still, there is a tougher, more critical edge to the acceptance of the decay of buildings  and their inevitable ruin that places architecture in a unique position to inform our understanding of the human condition and enhance its experience. Chiefly, this is to include in design a degree of complexity, even of contradiction embodied in the simultaneous processes of growth and decay in our buildings, that heightens and intensifies our humanity. Thankfully, there is no stagy, contrived method to accomplish this in architecture. Each architect must find their own, unique way.


LW


*The following photographs are by [Cristopher Payne](http://www.chrispaynephoto.com/), from his series North Brother Island:*


[![](media/cp-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-1.jpg)


.


[![](media/cp-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-2.jpg)


.


[![](media/cp-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-3.jpg)


.


[![](media/cp-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-4.jpg)


.


[![](media/cp-10.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-10.jpg)


.


[![](media/cp-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-6.jpg)


.


[![](media/cp-7.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-7.jpg)


.


[![](media/cp-8.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-8.jpg)


.


[![](media/cp-9.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-9.jpg)


.


[![](media/cp-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/07/cp-5.jpg)



 ## Comments 
1. aurora
7.9.12 / 11pm


Mr. Woods, would you be willing to provide me with an example of the type of design you would create that would embody the contradiction of simultaneous processes of growth and decay in our buildings. I share your point of view on this topic, however desire to further understand. I am still new to the architectural world, or rather thought process.


Kind regards,


A.
3. Alicia Breck
7.10.12 / 3am


“The reality and reliability of the human world rests primarily on the fact that we are surrounded by things more permanent than the activity by which they are produced.” – Hannah Arendt.


I wonder: if buildings decay, either by the passage of time, natural disasters, or man-made destruction, is this shift in state reflected in the relationship people have with structures? Does a loss of the built environment necessitate a loss of identity, culture, community… even memory? 


Personally, I have romanticized old, decaying buildings as tribute to “what was” – as if the building held a page marker for another chapter in book. And yet, that is precisely its beauty: that the chapters must be taken together to form the book in the first place. Perhaps in the face of “inevitable architecture”, designing the decay of a building should be considered? Or perhaps its integration into another form, as part of the [future] built environment? Would doing so connect past associations with the building (identity, culture, etc.) with future adaptations? I would like to think buildings do not always have to be new to be useful, and that they can shift states and still continue… 


AB
5. [Francisco Vasconcelos](http://csxlab.org)
7.10.12 / 1pm


embodied memories.
7. [Stratis Mort](http://arcsickocean.blogspot.com/)
7.11.12 / 5pm


as a reply to aurora: (in case it could be helpful to develop more thoughts on it)


look at this blog <http://speakingruins.tumblr.com/>:  

(extract)  

”Buildings in Motion


The Sagrada Familia, in Barcelona is a living ruin.10 In Gaudi's cathedral we see the close alignment of ruin and construction. Having been in construction/ruin for over a century the time passed has allowed a re-imagination of the Gaudi's original image with the technological, engineering advancements of the past decades accelerating the implementation of the project. Throughout its construction the ruin is constantly in a state of becoming, forever made and re-imagined. Due to be completed in 2026, it is somehow disappointing to imagine it finished.”
9. [The Fascination of Extreme Environments. The Inevitability of Disrupting Natural Ecology? « dpr-barcelona](http://dprbcn.wordpress.com/2012/07/12/extreme-environments/)
7.12.12 / 10am


[…] as a building material is more than remarkable, because it follows what Lebbeus Woods call “inevitable architecture“, embracing from the start the future decay of the project, wich being part of nature moves […]
11. Bob
7.13.12 / 9am


“Radical Reconstruction”?
13. [INSPIRE-SE: Arquitetura [3] «](http://arquitetogeek.com/2012/07/13/inspire-se-arquitetura-3/)
7.13.12 / 10am


[…] 02. Planejando a decadência e morte do prédio projetado. […]
15. [Dario Nunez-Ameni](http://atelierdna.com)
7.14.12 / 12am


Professor Woods: 


Yes, nature is dangerous. After all, we live by geological narcosis and the world is under no obligation to live up to our expectations of it. How tortured I felt laying wall paint over my kids' early scribbles, or the annoying but nonetheless wonderful graffito on our home's facade– Just waiting for Banksy I tell myself. I suppose these are some of the complex and contradictory human conditions you're talking about. We find comfort in the familiar, the known, yet the words ‘decay' and ‘ruin' are problematic precisely because they evoke a longing for an ideal state, one that arguably doesn't last any longer than the instant a building is first inhabited or, more radically, first imagined. To expect this–or any–ideal state to exist in perpetuity is perhaps the original delusion of architecture.
17. Jean Paul Sara
7.15.12 / 8am


It is not possible for the design process to exclude the design of decay. To marry actual building materials with the virtual principles of design and engineering is precisely to already anticipate the arrangements, and rearrangements, of a structure or its parts during the processes of decay. Science can narrate this history with ideal precision. The mere onset of an idea is, too, already a confrontation between the architect and the reality within the virtue of the design: At once both architect and structure become realized by the affects of this confrontation. The unlimited possible junctions and forks in the design process make the question “how to apply an idea” somewhat dangerous and as much or more an ethical question as technical. The demand upon the architect here is a true grinders task, an artists' task, one that highlights the transformation of architecture into activism and “becoming architect” into “becoming philosopher.” 


No doubt architects deny this confrontation to push through ideas to get projects in motion. I think here we may begin defining the root causes of uninspired decay as at least twofold. In one respect it is the responsibility of the designing architect yet it is unsure what the outcome of efforts to affect a decaying process will be. In another it is that of the architect who inherits a decayed structure although, again, one can not know if they are doing right while they think are doing right. Nevertheless, the question “what do we do with our decayed buildings” belongs in the hands of the architect affected by confrontations with reality within the virtue of design as much as the question “how do we apply an idea?” To secure this arrangement the business of designing buildings must equally become the business of designing architects as such.
19. [Bernardo BRUNO](http://bernynavigator1.blogspot.com)
7.16.12 / 11pm


<https://plus.google.com/u/0/107606399238705180004/posts>
21. [metamechanics](http://metamechanics.wordpress.com)
7.19.12 / 12am


Lebbeus, assuming I can get clearance from client, I will post a bunch of survey photos of an abandoned and quickly deteriorating complex of buildings (12) somewhere and link her. I think I mentioned in passing on a blog post here last year.


The deterioration at this complex is pretty practical in all its causes:


– someone (municpality of some sort) couldn't fund the function that had occupied the complex  

– for a few years the place was maintained to a certain degree with utilities still on and a team of security guards  

– eventually the security was dwindled down to one ex marine with a gun (if you met him still good security) like he said “if you aint afraid of death what is there to be afraid of.”  

– eventually utilities were suspended.  

– junkies and homeless started stealing the copper plumbing piping and brass on sprinkler heads.  

– contractors started stealing mechanical equipment, aluminum windows and doors, and even pre-cast concrete coping stones.  

– the removal of windows allowed weather to penetrate directly causing peeling of paint, water damage, moss, and even vegetation growth on interior  

– the removal of coping stones allowed water to penetrate the masonry walls further accelerating the deterioration of the facades  

– sprinkler mains burst due to freezing and the basement in a few buildings were flooded enough that a diving suit would be needed to survey.  

– elevators stuck as is  

– homeless people moved into the space, random mattresses and toothbrushes throughout spaces, usually in dark basements  

– travertine stone even being removed from old lobby  

– elevators stuck as is  

– in the movable folding stacks for books welding equipment from a recent contractor pillaging site was still on site and ready for use


If I can at least post photos that tell the story above about the site anonymously I will.


One of the coolest survey I ever did, really was more of an adventure.


I am also pretty sure the bricks alone at this complex if purchased new were equal to the cost of the complex.





	1. [metamechanics](http://www.metamechanics.com)
	7.29.12 / 2pm
	
	
	the photos of the space i was speaking of
	
	
	<http://metamechanics.com/blog/?p=439>
23. [Clinton Cole](http://www.cplusc.com.au)
7.19.12 / 7am


It is easy to be distracted by images of buildings in their decayed state from causes other than nature and design or material failure. I too find the romantic fascination with ruins problematic.  

Whilst the effects of nature may lead to decay the more likely cause of these buildings leading to such outcomes relates more to the decline of certain industries, broken hereditary lines and dilution of royal blood lines, erosion of religious or political beliefs, fracturing of a society through war, large scale disease or starvation, and decline in the traditional use of buildings and space (the decay of the formal dining room over the past century and the decay of the study over the past two decades are good examples of spatial decay).  

Also consider the growth in the number family breakdowns, the modern regard by financial institutions of buildings as pure property value as opposed to cultural or architectural value, growth in the complexity of property ownership (successors to title, contested wills etc) which can ultimately lead to legal stalemates spanning generations where the only outcome is the ultimate decay of the buildings and property being contested.  

I do not only see the quaint romanticism of buildings in decay. There is almost always a sadness, a loss, a tragedy, reasons associated with defeat, greed, disregard and even disrespect in the value of human creation when buildings are left to fall into decay.  

Whilst on an expedition to South America Joern Utzon famously phoned his engineer and said with great excitement that “the Opera House will be beautiful when it is a ruin”. This is the only Architect I know with the vision you are raising awareness of in your discussion.  

Perhaps rather than considering how their building s might decay Architects should be more focussed on how their buildings will be disassembled or demolished for re-use.





	1. [metamechanics](http://metamechanics.wordpress.com)
	7.21.12 / 3am
	
	
	Speaking of post+modern…as if cole and woods were with us at the wendy upenn ps1 event tonight…the pomo discussion happened at the something “pot” under the ELevated subway after a long discussion with a museum employee about the dirt and scaffold above that is a piet mondrian painting throughout the ps1…
	
	
	I think the utzon bit above covers the beginning of pomo. He is already imagening the decay of a building prior to its existence…a post reflection upon itself.
	
	
	Coles bit on the decay of the dining room and study and more importantly the commidification of the home point to a clear pomo condition. We are long past living, we are living now at least one reflection removed. We can't live without thinking about the possible editorial someone may write about our basic living, everyone is doing this, not just Salvador Dali.
	
	
	We have already entered pomo of pomo believe it or not…as a speaker said tonight at the wendy upenn event – if you are actually listening…who would right? We were there for the free beer and sending loved ones photos of where we were because an affiliation of an affiliation made us important.
	
	
	I didn't wear my name tag, did you?
	
	
	I think both coles and I's response to mr woods points out facts beyond the modern architect, facts most architects can't influence, the social reality of powers beyond the plan…
