
---
title: ARCHITECTURES OF CLOSING AND OPENING
date: 2011-05-24 00:00:00 
tags: 
    - architecture
    - design_methodology
    - urban_design
---

# ARCHITECTURES OF CLOSING AND OPENING


According to Aristotle's either-or Logic (A=A; A≠B), we can say that there are two kinds of buildings in the world: those that reach a conclusion or an end, and those that open up new possibilities or a beginning. This is an important distinction when evaluating any work of architecture, because it establishes the criteria by which we can judge a building's failure or success.


An example of the latter type of building would be Thom Mayne's recently completed Giant Corporate Campus in Shanghai, about which I will write extensively in an upcoming post, but will show an image or two here. What makes it a beginning is simply this: it demonstrates a way of thinking and a lucid method of design that can yet be applied to many projects and in many different ways. An example of the former type of building—the conclusion or ending—is Zaha Hadid's Spittelau Viaduct Housing in Vienna. What this means is the complex of buildings intended as social housing sums up an idea and way of working, offering little for the design of future buildings.


The architecture of Zaha Hadid tends to aim for a category of conclusion called ‘the masterpiece.' Each building is a summation of a particular thought, a zenith of imagination and talent, leaving those who try to learn something from her buildings with nowhere to go—she has said it all. The 19th century music critic Eduard Hanslick once wrote of the operas of Richard Wagner, who claimed they were the “Music of the Future,” that “they are all superlatives, and superlatives have no future.” The best, the finest, the most exciting, the most original—what, indeed, can follow them along the same line? Nothing. One can only create a new line altogether, taking nothing from the one already conquered.


So it is with Zaha's Spittelau housing. She has taken the conventional blocks of social housing seen everywhere in Austria and Germany—rationalist white masses with ribbon and punched windows—twisted and cut them at acute angles to create interesting shapes and spaces between. The result is a visually stunning and novel architectural ensemble that is instantly recognizable as her design. We can appreciate and admire, or criticize and detest, but it all ends there. Coming away, we can imagine nearly infinite variations of twisting and cutting the conventional, but little else. She has, with a masterstroke, summed up the thought and concluded the idea. Anywhere we might take it will only be an echo or an imitation.


We certainly need such masterstrokes in the world, and rather urgently, as we survey the dreary constructed landscapes engulfing the planet. Yet we know that they are not nearly enough to sufficiently assert our humanity against the rising tide of mediocrity and worse. We need architects who give us beginnings and not endings, and whose buildings not only inspire, but also give us the tools for thinking and working to go forward.


LW


*The following are images of the Spittelau Viaduct Social Housing project, Vienna, by Zaha Hadid Architects:*


*.*


*[![](media/zha-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-11.jpg)*


*[![](media/zha-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-2.jpg)*


*[![](media/zha-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-3.jpg)*


*[![](media/zha-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-4.jpg)*


*[![](media/zha-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-5.jpg)*


*[![](media/zha-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/zha-6.jpg)*


*.*


*The following are photographs of the Giant Corporate Campus, Shanghai, by Morphosis Architects:*


*.*


*[![](media/g-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-1.jpg)*


*[![](media/g-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-2.jpg)*


*[![](media/maynecampus-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/maynecampus-1.jpg)*


*[![](media/g-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-3.jpg)*


*[![](media/g-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-4.jpg)*


*[![](media/g-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-6.jpg)*


*[![](media/g-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-7.jpg)*


*[![](media/maynecampus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/maynecampus-2.jpg)*


*[![](media/g-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/g-11.jpg)*


*The cover of the book by Morphosis Architects, “Combinatory Urbanism: the Complex Behavior of Collective Form,” putting forward a new methodology of designing urban architecture, with many examples from the firm's practice, including the Giant Corporate Campus:*


*[![](media/mayne-book-cover.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/mayne-book-cover.jpg)*


#architecture #design_methodology #urban_design
 ## Comments 
1. Daniel Jacobs
5.26.11 / 5pm


In reading your paragraphs about Zaha's “Masterpiece” modus of design I became frustrated by the overwhelming praise being poured upon the building, until I reached the “or criticize and detest” phrase. I don't mean to say that I detest the posted projects, what I want to understand is why we (“we” being the academic and professional world of architecture and design) cannot see beyond the broad ideas that arise from the deft maneuvers of Zaha's brush or the vast complexities that must surely exist in Thom Mayne's mind. I just think that depth of discussion of architecture and design should be about detail and tested use. A statement referring to a piece of architecture creating “interesting shapes and spaces between” should make us all cringe because all I see in the photos are the dead and empty zones of picture perfect design purity that we all know that a building should never be. I want to see how Zaha's ideas translate to the place a worker who lives in this building sleeps at night and how a child plays around the angled columns. The way we represent idealized architecture today is immature and I argue that we should be getting beyond our own captivation at the beauty of the image. The idyllic grassy slopes atop the Thom Mayne project pique my interest but don't answer any of my questions (and I look forward to your post about the Thom Mayne building potentially to answer them), the cavernous spaces within are alien enough to cause me to look twice but there is no indication of their use, or purpose. I am merely curious as to why the answers to those questions are deemed less important than the overall strokes of a project and the implications they have on design, instead of the implications the project has on people.
3. [chris teeter](http://www.metamechanics.com)
5.27.11 / 12am


Is this a beginning of the end post? or the end of the beginning…as neither of concepts surely matter any more (beginning and ending)


To reference an old post of yours where I asked who were more of less the zeitgeist futurists of architecture these days… and your later Zaha post (blogging against the touched up empty photos) in which I believe you answered my question – zaha is the perfect virtual built….here is a perfect concept of pure form thought built – voila.


Morphosis – took me a good decade out of undergrad to even be attracted or interested in the work. Perhaps some persuasive Delanda lectures got me there.


To your point on opening and closing – I think its easier to argue that Zaha's masterpieces open up the public's eye to architecture, it gets them to the first stage in our education – developing a concept. You know how hard it is to explain concept to people who crunch numbers? 


Morphosis in my opinion can best be appreciated if you are in the craft of developing concept in conflict with reality (physical, politcial, economical. Etc…), typical architectural practice for most of us. You get your moments here and there, but you don't conclude them. A friend told me they never had one big masterpiece in their 20 years experience, it was usually getting something built here and there, etc…in this sense you can really appreciate morphosis…now if you have not been educated in the craft Morphosis seems like some random bullshit that you are supposed to get if you want to be cool or something. 


In grad school we went out to LA and visited Morphosis office (Thom graced us for a few minutes). The girl giving us a tour of the office showed us a project in China and told us the client would usually ask “how does Thom feal about? Is he happy?”


morphosis is a designers designer. I think like my immediate uneducated reaction to morphosis my freshman year was just plain confusion…its too much for those who can't first grasp complete master pieces.


Listening to a lot of iannis xennakis these days and reading his stuff, between him, alvo noto + ryoti (spelling?), pan sonic, luc ferrari, stockhausen, etc…I don't think if I spent time trying to write electronic music would I even have the faintest idea what they are doing…and still really don't.


Looking forward to your morphosis post.


– Chris
5. tom
5.29.11 / 4am


Coming from a sub-novice- I find the comparison interesting because of the superficial similarities. One of the differences I see, is in the lines of the buildings, maybe just meaning on an intuitive level. The lines of Hadid seem orchestrated in such a way to give the impression that all the perspectives of her architecture will coalesce as one grand spatial theme.  

The lines of Mayne seem to be coming from and going different places, coalescing in the present space of the inhabitant and not some distant conceptual space.  

I always liked the analogy that Robert Harbison made between Bernini and Borromini and I think a similar analogy could be made for Hadid and Mayne (or a number of othe architects).
7. Gio
6.7.11 / 7pm


Great Post! Mr. Woods.


You are right, the two architects are masters at their craft.


Zaha Hadid has closed it up to completion. There is nothing else to advance nor gain from it.


I thought for a moment that maybe the programs, both very different, allowed for one to be a closed architecture and the other to be an open architecture, but on close inspection of Morphosis's corporate campus in Shangai,  

I can see that one can actually apply many of the forms, spaces and solutions to a housing project.  

Now, I will go even further and propose that this project in Shangai has many if not more of the functions and needs desired in an urban housing project. To be more exact, it has a close resemblance to Le Corbusier's Unite d' Habitation, full of open spaces for recreation and sports, open areas for socialization, open gardens…etc…a place where you live and work…..a good example of open architecture in the sense of this post.


Interesting to me, this brings me to the idea of “Identity” in the present state of Architecture.  

I believe that our present state of architecture is in a process of new discoveries triggered by the advances in science and technology. This has allow for the end of the accustomed forms of identity for the program.


At first glance looking at these two projects, I can recognize the housing project for the forms here used to symbolize the program.  

On glancing at the Shangai project, I feel it could be a High school, Art center, Museum or an University.  

(a better example of the current state of architecture)


Somehow there is not longer a separation between the program and the plan.


To me the challenge rest on our abilities to discern the new forms of this era…..all of the examples are needed to better understand where we are going.


Thanks for creating this space where anyone can post.


Gio
