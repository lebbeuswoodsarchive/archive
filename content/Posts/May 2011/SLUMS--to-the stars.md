
---
title: SLUMS  to the stars
date: 2011-05-01 00:00:00
---

# SLUMS: to the stars


## ***Breaking News—-LEB'S ALL-NIGHT CAFE HAS [CLOSED](http://lebsallnightcafe.wordpress.com/2011/05/06/closed/).***


***. . . . . . . . . .***


[![](media/tod-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-2.jpg)


[![](media/tod-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-3.jpg)


A lot of [attention](http://www.domusweb.it/en/architecture/the-tower-of-david/) is being paid to squatters taking over an unfinished skyscraper in Caracas, Venezuela, and rightly so. It is both a morality tale from which we can draw many harsh lessons about our contemporary global society, and a prophesy of the future. Surely, with vacant land running out in the vast cities that migrants from rural areas are now flocking to by the millions and in every part of the world, it is inevitable that slums will occupy abandoned, unfinished or unleased skyscrapers. The harshest lesson of this phenomenon is that slums are radically out of the control of governments and private institutions that we have no choice but to look to for treating the existence and the effects of this human scourge. Without some measure of control or at least slowing the spread of poverty and slums, it is conceivable that in the next few decades they will begin to overwhelm organized society and eventually push it to the brink of a new Dark Age. In such an age, it will not only be public services that will begin to collapse from overuse, much of it illegal, but social systems of every kind, from education to art, as the financial burden of paying for the problems created by a vast population that pays no taxes will make many of civilized society's essential activities unsustainable. This is a grim and frightening prospect indeed and one that is already beginning to happen. The takeover of the Caracas skyscraper is not just an oddity. Rather, it is a first drop of rain in a coming storm caused when the global financial system falters, even a little, and a financier-developer must abandon a project for lack of funds. A crack has opened up and it is immediately filled by people desperate to find shelter and establish a community, people who have every right to them but have been excluded from those provided by established society. These people have no choice but look for cracks in the social edifice and, when they find them, move quickly and decisively. As mainstream society falters more under the increasing weight of the impoverished and excluded, more cracks will appear and be filled. It will be an incremental process, but inexorable. Unless, that is, the powers-that-be in our present society begin to address the root causes of poverty.


Another lesson to be learned from the Caracas story is that all those who have been hoping for a social revolution to emerge from the dispossessed squatters and slum-dwellers—the poor—had better think again. What these people want is not a new, egalitarian society founded on ideals of social justice, but only what most others already have—a consumer society with all the bells and whistles and toys. Clearly, the Caracas tower is not a breeding ground for radical social change. It is an unintended parody of the society that created the squatters' dire situation to begin with.


LW


.


[![](media/tod-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-4.jpg)


[![](media/tod-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-1.jpg)


[![](media/tod-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-6.jpg)


[![](media/tod-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-5.jpg)


[![](media/tod-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-7.jpg)


[![](media/tod-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tod-8.jpg)



 ## Comments 
1. [nick](http://nickaxel.net)
5.1.11 / 8pm


it seems that you're forecasting the collapse of traditional notions and experiences of “civilization”. is this idea, and its reified forms, not the ‘greatest' western historical construct of all time? maybe only once this happens, or, only when the informal becomes the norm, we can find another, better, less alienated, way of being.
3. JRO
5.3.11 / 2am


Funny how the bulletin board refers to the squatters as “propietarios de espacio” (lit. space proprietors or space ‘owners').


I also find the clause discussing sale of real “property” (not without permission of the ‘coordinators') as evidence to the fact that what the squatters really want is simply the goods and services other members of the greater community have. No matter how they get it and how they rationalize their actions; thievery by another name.
5. Nimai
5.3.11 / 7pm


Hi Leb, very interesting article! Your comments regarding the collapse of aspects of conventional ‘organized society' in conjunction with the financial downturn and eventual depression seem to strike a chord with north american nostalgics determined that america will return to its golden days of white picket fences and suburban sprawl in the deep south. I think Obama is dreaming, oil is running out, it aint gonna happen and the consequences of the collapse of economic systems based on crude oil and the rise of population densities in major cities is an area that needs to be addressed by the powers-at-be BEFORE the problem hits its peak as opposed to when the acute effects are felt
7. itsme
5.4.11 / 12am


The roots of this issue can be summed up in one very simple law under which all organisms exist, be they a colony of fire ants or a city of people: All species populate up to and slightly in excess of the available food supply. The old boys who wrote the bible understood this. Doesn't the good book say something to the effect that the poor will always be with us? Course it's likely the biblical authors were students of Plato who was first to make written observation relating food supply to population. That is why for the past 10,000 years no matter how much food we grow and ship around the world to the starving, there are always more hungry people this year then there were the year before. In the end we never do feed the poor and starving. All we do is strip the natural world of its beauty and diversity in a futile attempt that was doomed by biological imperative from the outset.





	1. Nimai
	5.4.11 / 5pm
	
	
	Just a quick query as to some actual evidence of who has previously postulated this ‘law' that we can never have enough food for the population? I think any knowledgeable historian will tell you that there have been civilizations in the past such as the ancient Vedic civilization that existed in India for thousands of years where no one in the kingdom would go hungry. I couldn't disagree more with this ‘law', the present global predicament of scarcity, economic collapse, food shortages etc. is caused by unbridled consumerism, human greed and an economy built upon and based around dwindling oil reserves. I think the parody Woods mentions in the last paragraph of his post is an observation of human consciousness rather than an affirmation of a law that we never have enough i.e. that the rich corporate fat cats and the struggling beggars and squatters really have the same exact inherent greed in their mindset and that at the end of the day nobody in society regardless of their bank balance is striving for transcendent values and social systems that surpass the need to eat, sleep, mate and defend like animals.
	
	
	If people in society really care about the environment, really want to end food scarcity and poverty than they should start accepting the truths that were too inconvenient for Mr. Gore himself:  
	
	<http://www.taipeitimes.com/News/editorials/archives/2009/05/18/2003443851>
	
	
	
	
	
		1. itsme
		5.8.11 / 8am
		
		
		Read Daniel Quinn, read Plato. It's not a big secret. And if you have to reference some vague mythical culture to try and dispute what I said, maybe your position just doesn't hold water. Don't mean to offend you but if you look at real documented history, you will see that I am right. And if you still disagree then I take solace that you've been presented with the truth, perhaps at some time in the future when you are ready, you will hear what I have said. It will be a tough day for you as it was for me, but the beginning of knowledge is always the hardest part of the journey. Good luck to you and thank you for caring enough to take the time to respond.
9. Jean-Paul Sara
5.4.11 / 8am


Prof. Woods (and commentaries),


I agree. Those concerned with architecture's role in societal alienation, as Manuel DeLanda might put it, should be focused not on seemingly alienating buildings but instead on what it means to experience alienation within those buildings and how then reactive forces might generate, organize and multiply within that backdrop, and perhaps materialize for the architect the dystopic blueprints they never dared to draw up. It's amazing how even an innocent architectural project can turn septic if only poverty levels and activity goes unchecked.
11. [Guilherme Cartaxo](http://enfado.org)
5.4.11 / 9am


Reminds me the story of the “Grande Hotel da Beira” in Mozambique: <http://www.buala.org/en/cidade/we-the-ones-from-the-grande-hotel-da-beira>
13. adamx
5.5.11 / 7am


Most people are just trying to live their own lives, not start revolutions, and you can't blame them for that. 


As for the “slum” here, it seems admirably well run. Perhaps rather than looking at it as some sort of problematic space full of problematic people, the city should help make it more livable (put in plumbing, etc) and perhaps use some of that space as schools for the kids living there.


This is not in any way a parody of life or an impending dark age, as far as I can see. It only becomes that when society refuses to provide the services needed.
15. Toni
5.6.11 / 9pm


Mr. Wood's I will gladly accept your invitation from the previous post and promise to write something substantial within the next few weeks. Meanwhile, I agree with adamx completely on this issue. This is the way John Turner thought about slums when he started working in Latin American cities in the 70's. In many cases, where these informal areas have been established for over 40 years, John Turner's hypothesis that these places were a ‘step up' economically for people who had very little, is almost unquestionably proven.  

The case of Latin America is very interesting as many urbanists and urban historians are beginning to see the area as a model for the poorer countries of the global south in terms of self-help projects that have considerably improved entire cities.  

There is one main question that arises from your general discussion on ‘slums.'  

What is your definition of ‘slum'? Just an informally settled area– or a place that is inadequately serviced?  

The difference between the two is very important since it seems like the ‘conditions' of these places are what you rally mostly against, but sometimes, issues of ownership slip into your posts. Once the complex element of ownership becomes part of the discussion, you are more along the way to understanding this multi-layered issue and possibly making a strong argument.  

This is only one of the many things that makes up this issue. This leads me to the response about the thought that your general ideas were shallow.  

In one of the comments to the previous post, someone asked if one must know about global economics and other social issues in order to fully grasp the idea of slums and you responded that we must. Not only do we need an awareness of the global conditions, but we must also understand the layers of context under which slums happen. The first and most important of these layers is the city itself. Without the cities, informal areas as we know them wouldn't exist. Not only the city form (which often informs where the slums happen) but also the city's political, social and economic structures and their histories.  

We must also consider regional histories that often– in countries of the global south– include a period of colonization, independence and republic construction. (I will write about this more in depth at a later time).  

In essence, this issue is not merely about philosophical issues of why wealth is not distributed evenly, that is just one aspect and in my opinion, one of the least helpful in understanding these places in our contemporary context– maybe it is only a way of explaining ‘the informal' the grand scheme of history.  

It is also not just about ‘the rich and the poor,' rather about the dynamic relationships between the two that have led us to our contemporary urban situation.  

I applaud your commitment to discussing these issues in your blog as a ‘public' form of discourse since I truly believe that “the more we know the better we will act”– and this is definitely an opportunity to share ideas and knowledge.
17. Toni
5.6.11 / 9pm


By the way… I think it is truly amazing what they have done with this building.  

If the developer went broke, why shouldn't people that are committed to taking care of the place own it and have proper city services–rather than having a bank ‘own' it?
19. Jean-Paul Sara
5.7.11 / 12pm


Toni, I hear you.


 Depending on given “regional histories that often– in countries of the global south– include a period of colonization, independence and republic construction,” a particular lead up to such as the unfinished Caracas structure could very well cause the “residentially disabled” community to embrace it with the very best that could be hoped for because it is truly a step up. I would have to see it myself to comment further in this way but if it's really working out then that's great. And, yes, schools if possible.


However, perhaps it is in the very least cautious to consider how organization and optimism usually get worked over in these environments, over time, by new and evolving reactive forces contingent with poverty in periods of colonization and independence you refer to. The Caracus story is that of the unique character of its people in this situation in history, and not that of what typically occurs in these spaces, under these conditions. Let's just hope it holds up.





	1. Toni
	5.8.11 / 4am
	
	
	Jean-Paul  
	
	When I mentioned the importance of the intricate histories of these countries, I meant it not as the cause for the conditions that created abandoned buildings but rather the environments where the formal has left gaps where ‘the informal' has been able to happen (socially, economically, institutionally,etc).  
	
	Also, the whole idea of my post was to bring to light the fact that this is not the exception to ‘informal' owners improving their economic conditions through taking over empty property. There are tons of examples throughout Latin America of how many of these informal places have been upgraded and are currently decent neighborhoods. This is the reason many Latin American cities can serve as an example for poorer areas of the South.  
	
	It is also impossible to generalize about informal areas. Their conditions are as diverse as the cities they are a part of. Again, the reason I mentioned it is important to understand their unique conditions– because no two are alike, and many have actually come a long way since they were first established.  
	
	There is at least a 40, maybe 50-year history of informal settlements, so the idea that they can be improved and offer people a way out of poverty has, in fact, been proven. (John Turner)  
	
	This is not to say that many are not in terrible, inadequate conditions– either way we cannot generalize.
21. Jean-Paul Sara
5.11.11 / 10am


Toni,


I have no doubt informal areas can be upgraded into decent neighborhoods. I imagine it could happen anywhere with the right civil apparatuses in place. I seriously hope not to generalize. I am reading informal areas themselves, together with their optimism and organization, as active, self-organizing systems that are permitted to happen, as you put it, and that are specifically opposed to reactive, planned systems of organization which tend to dominate.


As much as the Caracus structure we are speaking about is “out there,” a prop, an accidental backdrop people live within and around, I would argue it is much more an imagined space, an internalized and internal space, and it is this internal space that is materializing itself in gaps left in (formal) forms of (formal) reality where they can possibly dominate a planned onset, thrive, and evolve.


This internal space occurs in the unconscious, namely the active and reactive drives we have in motion that we might not know are in motion: The Things We Don't Know That We Know. Negative drives are not characteristic to poverty nor are active ones to wealth. However, I think I may have communicated that when I mention forces contingent with colonization and independence. Again, I read these two as diametrically opposed: colonization as reactive / negative and independence as creative / active, with respect to the drives that at once invent, and function as a result of, their reality.


Why then do I caution? Perhaps I shouldn't. I am not sure it is even possible but what I believe I'm seeking is a reading of this short 40-50 year history of informal areas from the vantage point of the drives materializing them. These, in my view, are “the conditions [that] are as diverse as the cities they are a part of.”
23. Jean-Paul Sara
5.11.11 / 4pm


In the third paragraph of my last post there is no “is” between “This internal space” and the word “occurs.” Also, I forgot the closing quotation mark on the final sentence that is a reference.
25. [Scraper Sitting « JAMES THOMAS](http://jmtresearch.wordpress.com/2011/05/19/scraper-sitting/)
5.19.11 / 3pm


[…] From: Lebbeus Woods […]
27. zaire sais
5.20.11 / 9pm


excellente article lebbeus. not only are ur drawings and concepts pretty evolutionary,but ur politcs and outlook concerning late stage capital are totally on point. it think this developement in global social systems is a desirous thing. real world human needs should trump the “donald trumps of the world,and their speculative investments. i hope this spreads to new york city,tokyo,etc. good stuff.
29. [site: tower of david | nomadic property](http://benleavitt.wordpress.com/2012/06/22/site-tower-of-david/)
6.22.12 / 3am


[…] reference: Lebbeus Woods Share this:TwitterFacebookLike this:LikeBe the first to like this. This entry was posted in […]
