
---
title: FOUR IDEAL HOUSES  2011
date: 2011-05-17 00:00:00 
tags: 
    - architectural_education
    - architecture
    - ideal_houses
---

# FOUR IDEAL HOUSES: 2011


## *Breaking News……*Some word about Ai Weiwel: [THIS CANNOT PASS (updated May 18, 2011)](https://lebbeuswoods.wordpress.com/2011/04/03/7669/?preview=true&preview_id=7669&preview_nonce=0f47692d9b)


[![](media/2-all-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/2-all-1.jpg)


*(above) The Four Ideal Houses construction models as seen at the Final Review, in the 3rd Floor Lobby, Foundation Building of The irwin S. Chanin School of Architecture of The Cooper Union, on May 5, 2011.*


***THE COOPER UNION School of Architecture, ARCH111-S11—ARCHITECTONICS; Professors Lebbeus Woods, Anthony Titus, Uri Wegman, Aida Miron; Professor Anthony Vidler, Dean. SEMESTER PROGRAM:***


***[FOUR IDEAL HOUSES](https://lebbeuswoods.wordpress.com/2011/01/26/four-ideal-houses-first-year-studio-2/)***


***Project****: This semester we will focus on the design of****four Houses****, which we term ‘ideal' because each occupies a different elemental volume—****cube, cylinder, cone, and pyramid****—and each embodies a program of habitation based on a different natural element—****earth, air, fire, and water****. Furthermore, the****inhabitants*** *of each House are assumed to be ‘ideal,' in the sense that they embody, for our purposes, only certain universal human characteristics, such as physical size, capabilities of movement, perception of their environment, and interaction with it. The****site*** *of each of the four Houses will also be ideal, meaning sloped or flat, horizontal or vertical, and will disregard any inherently idiosyncratic features. In the design of each House, equal emphasis will be placed on the* ***interior and exterior*** *of its volume. In taking this approach, we realize that these ideal types exist only as ideas, yet find these ideas useful in the laboratory of the design studio as a means of****understanding the fundamental tectonic elements of architecture.***


***Historical background****: There is considerable historical precedent for our project. We find ideal architecture—of exactly the sort we are engaging—in examples from Vitruvius, through Alberti and Da Vinci, Ledoux, Semper, Taut and Le Corbusier, Archigram, up to the present in ideal projects by Hadid and Holl. These and other architects have found it important to define their personal principles of design, as well as to set a standard against which to measure their more reality-based work. Ideal architecture has been essential to defining building typologies, which serve the purpose of bringing a level of order to the diversity of urban landscapes. Each student is encouraged to look up and into historical examples, the better to understand the broader context of our work this semester.*


**The Teams for the conception, design and construction of the *FOUR IDEAL HOUSES:***


**Cylinder-Air:** Angelo John Alonzo, Kyra Cuevas, Mathew Maiello, Arta Perezic, Janine Wang, Johae Song. **Cube-Earth:** Henry Barrett, Cory Hall, Ashley Katz, Phong Nguyen, Aisa Osako, Nicholas Pacula, Andrea Recalde, Christopher Stewart, Dakota Linkel. **Pyramid-Water:** Evan Burgess, Kohl Lewis, Iyatunde Majekodunmi, Gisel Orizondo, Jaime Pabon, Ian Smith, Kristy Chiu. **Cone-Fire:** Dustin Atlas, Febe Chong, Franco Cuttica, Omari Douglin, Mabel Jiang, Kyung Lee, Binhan Li.


The final review of the Four Ideal Houses consisted of the oral presentations by each team of their House in the form of a large-scale construction model (showing scale-giving details of construction) and enlargements of construction drawings for each house, which were deemed by the faculty as the most authentic visual records of the processes of thinking and making.


*(below) Four Ideal Houses, drawings and models.*


*Cylinder/Air House:*


[![](media/cyl-sk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cyl-sk-3.jpg)


*Cone/Fire House:*


[![](media/cone-sk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cone-sk-3.jpg)


*Cone/Fire House:*


[![](media/cone-sk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cone-sk-2.jpg)


*Cone/Fire House:*


[![](media/cone-sk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cone-sk-1.jpg)


*(f. to b.) Cone/Fire House, Cylinder/Air House:*


[![](media/4-conecyl-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/4-conecyl-1.jpg)


*Pyramid/Water House:*


[![](media/pyr-sk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/pyr-sk-2.jpg)


*Pyramid/Water House:*


[![](media/pyr-sk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/pyr-sk-1.jpg)


*Pyramid/Water House:*


[![](media/pyr-sk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/pyr-sk-3.jpg)


*Pyramid/Water House:*


[![](media/1-pyr-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/1-pyr-1.jpg)


*Cylinder/Air House:*


[![](media/cyl-sk-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cyl-sk-4.jpg)


*Cylinder/Air House:*


[![](media/cyl-sk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cyl-sk-1.jpg)


*Cylinder/Air House:*


[![](media/5-cyl-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/5-cyl-1.jpg)


*Cylinder/Air House:*


[![](media/6-cyl-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/6-cyl-2.jpg)


*(f. to b.) Cylinder/Air House, Pyramid/Water House:*


[![](media/7-cylpyr-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/7-cylpyr-1.jpg)


*Cuber/Earth House:*


[![](media/cube-sk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cube-sk-1.jpg)


*Cube/Earth House:*


[![](media/cube-sk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cube-sk-2.jpg)


*Cube/Earth House:*


[![](media/cube-sk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cube-sk-3.jpg)


*Cube/Earth House:*


[![](media/cube-sk-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cube-sk-4.jpg)


*Cube/Earth House:*


[![](media/8-cube-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/8-cube-1.jpg).


*(f., l. to r.) Cube/Earth House, Cone/Fire House (not fully seen); (b. l. to r.) Pyramid/Water House, Cylinder/Air House:*


[![](media/3-all-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/3-all-2.jpg)


*Discussion of “Four Ideal Houses” at the review. From left to right: Uri Wegman (ist Year CU faculty), Dorit Aviv (CU grad, guest), Sulan Kolotan (guest, Pratt), Anthony Titus (1st Year CU faculty), Eunjeong Seong (half-visible behind Anthony, guest RPI), LW (back to camera, Ist Year CU faculty), Steven Holl (guest, Columbia GSAPP). Aida Miron (1st Year CU faculty, not seen):*


*[![](media/classic-don-maestro.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/classic-don-maestro.jpg)*


LW


#architectural_education #architecture #ideal_houses
 ## Comments 
1. Jemuel Joseph
5.17.11 / 9pm


The specificity of the form and spatial objective is an interesting way to introduce first year students to the practice of architecture. Other times, one may approach this by first generalizing these basic elements… as well, basic elements. What i'm interested in is how the group dynamics of a first year studio results in the very tuned and rigorous solutions shown above, and how the specific team members contribute to this process of thinking and ideas. I do realize that each was assigned an element dealing with their houses however, it still remains unclear to me how they relate to themselves to amalgamate (if you will) into the beautiful projects show above.
3. [Four Ideal Houses – Lebbeus Woods](http://rchitectur.com/archives/299)
5.24.11 / 2pm


[…] Lebbeus Woods posts about his semester as a critic at Cooper Union for first year students. […]
5. aida
5.24.11 / 7pm


In the studio of the architecture school an oceanic event is taking place: Fragments of conversations thought dim “diamond” projections of overlapping temporalities,  

spatialities and memories, shifting scales and drawings continue ongoing dialogues with blown up sketches, and what redefines architectural “models.” Here we are confronted by the full scale construction of the 4 Ideal Houses, where strict geometries: interact with 4 elements: Earth, water, fire and air, in what  

a visiting artist called “a real coup!” The “existential beauty” (L. Woods) of the houses next to the sketches on the wall, stand as witnesses to the limitless process and transformations of thought. Through strict parameters, the students where able to engage language and tectonics, re-framing the possibility of habitation through construction and scale. In the backdrop of the space fragments of conversations: “Eisenman would be jealous of this cube house.” “This is in the spirit of the Tatlin Tower,” “Kahn's dialogue with the brick meets wax.” “Luminous material in First year.” 


From Holderlin to Heidegger's: “poetically man dwells..” this studio's work made me think that poetically Beings create, here as a group of students, in an architecture school, as a political act.
7. [MANIFESTO: The Reality of Ideals « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/06/06/manifesto-the-reality-of-ideals/)
6.6.11 / 4pm


[…] following has emerged from the innovative First Year program and work made this past semester. It has been posted on the wall of The Cooper Union End of Year Show and is […]
