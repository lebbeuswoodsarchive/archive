
---
title: DRAWINGS, STORIES 3
date: 2011-05-10 00:00:00 
tags: 
    - architectural_drawing
    - architecture
    - story-telling
---

# DRAWINGS, STORIES 3


[![](media/cella-6-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/cella-6-1a.jpg)


**.**


## **DRAWINGS AND STORIES**


**.**


*The following is the second in a series of three posts presenting drawings and stories behind them by LW.*


The cognitive scientist explained it to me this way. “For a few thousand years, architecture had been the conscious communication of a few important ideas within a particular human community. Chief among these ideas was *stability*. People considered their existence as fragile within a natural world basically hostile to them. In order to survive and, beyond that, to flourish on the planet, people had to struggle against the effects of nature, one of which was their own inevitable mortality that most often came about as a result of ‘natural causes.' The cognitive processes that produced these ideas have their roots in the emergence of the human brain and its own self-reflectivity millions of years earlier than the invention of architecture and indeed of human society and the very idea of individuality. The neural structure of the human brain evolved differently from that of the animal brain, becoming larger and more complex. The brain's complexity led to the invention of diverse defenses against natural forces and their effects, one of which was architecture. Architecture—the conception of enclosed spaces for living—made possible a balance between human fragility and natural hostility, a stability that encouraged and assured the establishment of both human society and individuality.


The structure of the human brain is the key to understanding the evolution of the human mode of existence. It is crucial, as we learned in the 20th century, that the human brain is ‘two-tiered.' It is, like all brains, comprised of neural nets that function according to principles of electromagnetism—neurons processing electrons. The nets are biological computers computing what we broadly call ‘thoughts.' The human brain on the first tier computes ‘rules' of behavior, and, on the second tier ‘rules of the rules' that enable us to change our behavior as our changing circumstances require, making us ‘adaptable,' our supremely and particularly human trait. This means that we can change ourselves as well as change our environment, say, through making buildings.


When the severe environmental crisis struck our planet at the end of the last century, it became urgently necessary to change not only our behavior, but also the rules by which we govern our behavior. In short, we had to engage in second-tier, or second-order, thinking.  It was only by doing this that we as individuals and as a global community were able to break out of our old, dysfunctional ways of thinking and living and invent, for example, new modes of constructing buildings. It was equally important that we also change our ways of living in them. Indeed, it would make no sense to build new kinds of buildings if we were unable to adapt ourselves to them. This had to happen on the level of individuals and not only the societal level, and within a generation or two at most.


Was it coincidence that the new way of building mirrored, in effect, the neural structure of the human brain by creating a continually regenerating network form that resembles the structures of both matter and energy? Or, could it be a sign that attitudes towards nature are less defensive than they were and more conciliatory? Debate continues on these questions up to the present moment, no doubt because the process of invention left little time for philosophical considerations. However, it seems certain that changes to the neural structure of the brain will result in a few generations. Our idea of what is human will necessarily evolve.”


[![](media/tower-12a-sq-3-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/tower-12a-sq-3-2.jpg)


LW


*Coming next: What the social scientist said.*


#architectural_drawing #architecture #story-telling
 ## Comments 
1. Kevin H.
5.11.11 / 12pm


Mr. Woods,  

Perhaps I am just interpreting it this way, but your recent posts seem to be touching on a subject that I myself have been struggling with recently. That is, when is something complete? Or more importantly, Should something ever be “complete”


The human DNA system is an ever evolving network which promotes situational response and evolution over static predictability. It is because of this that we see changes and advancements in both physical attributes as well as immunology resistances, etc.


I have begun to question architecture in the same way. Certainly, habitation requires a physical environment to be defined but shouldn't situational need and evolution demand a response that can allow said environment to change to suit the need? Like evolution could we use what we have and continue to add on to/ reject existing structures in a way that is perpetually suiting short term need while allowing for long term advancement?


I feel the static acceptance of style or architectural movements is limiting the speed at which architectural evolution could actually be occurring.


I encourage anyone who has read this far to let me know your thoughts. Perhaps I am way off. If you feel so Let me know, but more importantly, let me know why.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.11.11 / 2pm
	
	
	Kevin H.: Hopefully, many thousands of years from now, human beings—or creatures like us in essential ways—will still exist on this planet. What they will be like will emerge from what we do today. Necessarily they will have evolved in response to many environmental factors, including buildings that interface more sympathetically than they do at present with the wider nature of the planet. This means that the brain's hard-wiring will evolve n its interactions with architecture. This evolution will be slow, if left to random feedback. However, it might be accelerated through technological interventions including, but not limited to bioengineering in the form of what might be called directed feedback. The story the cognitive scientist tells suggests the latter. The construction of architecture will become more instrumental than ever before, in shaping human evolution.
	
	
	And you've answered your own question: nothing is ever complete, except in death.
3. [Jeffry Burchard](http://architectureev.blogspot.com)
5.15.11 / 4pm


Just finished watching (again) a TED talk by Sean Carroll discussing the recent (1998) scientific discovery that the universe is expanding at an accelerating rate. This is relevant to the topic at hand because it suggests strongly that at some point in the far distant future it will be literally impossible for humans to exist in any form that is recognizably similar to the form we take today. I hold the opinion that human action is at its' basic level a means of self-preservation, but when the available length, in units of time, of that preservation is moderated by an irreversible universal velocity the purpose of a species self-preservation is compromised. Does this render any exertion of adaptation, either forced or spontaneous, a rather dubious, ill-informed, or wasteful task? That question is obviously not easily answered, though I don't think it would be human to admit purposelessness.  

Just a prod to my own brain…
5. Caleb Crawford
5.16.11 / 2pm


The influence and evolution of the mind in relation to our architecture is a particularly appropriate discussion. Your story here has provoked a vision of towers that rise up in relation to our thoughts, much like in Lem's Solaris. There are amazing advances in prostheses happening, where thought moves robotic limbs. Why not shape our architecture? One of our great gifts as architects is to raise up cities and palaces from graphite and wood pulp. 


The other provocation elicited is to dwell upon our current politico/economic condition. We seem to be in what I can only describe as a manic depressive condition of extreme elation followed by severe and debilitating depression. The tragedy is that while our elation has resulted in some very fine works of architecture, for the most part it is marked by poor quality ephemera. Unlike a brilliant spectacle which lives on in our memories, we will be saddled with this detritus for many years. In some ways I long for the attempt at permanence, which is a way of celebrating our time on earth through the exuberance of our environment. If you aren't familiar with it, check out this clip of Orson Wells <http://www.youtube.com/watch?v=VGPPUY40Y7k>
7. Gio
5.23.11 / 4am


I just have a comment Mr. Woods I think this post to me is cleverly trying to get to the source of the meaning of the word Architecture.


In that frame of thought, I was wondering when and where in our evolution of cognitive abilities, the solution to an environmental situation, “Shelter” became “Architecture” something of the senses, of the mind, something spiritual.


Not long ago this way of thinking began to see the roof and the supporting structure as planes that interact with the surrounding conditions (outside-inside) calling it “space” but what is here interesting to me is that those same planes are becoming transparent networks of fields that shape not just the outside environment but ourselves as well.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.23.11 / 12pm
	
	
	Gio: Thanks. Your interpretation is very fine and provocative.
