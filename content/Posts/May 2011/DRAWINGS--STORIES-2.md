
---
title: DRAWINGS, STORIES 2
date: 2011-05-08 00:00:00 
tags: 
    - architectural_drawing
    - architecture
    - story-telling
---

# DRAWINGS, STORIES 2


[![](media/lwblog-drwgs-stor-2-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/lwblog-drwgs-stor-2-1.jpg)


**.**


**DRAWINGS AND STORIES**


**.**


*The following is the first in a series of three posts presenting drawings and stories behind them by LW.*


*.*


The engineer explained it to me this way. “When we fully understood, at the end of the century, that building solid buildings was inherently non-sustainable, due to the laws of thermodynamics considered independently of factors like the quality of materials, details of joining them together and any other aspect of design, the push was on to find an entirely new way of constructing buildings. This at first sounded to everyone like something impossible—what about all those thousands of years of architectural history and culture?—you can't just push them aside and install some ‘new way' in their place. But the fact is we didn't have any choice. The planet was becoming unlivable at a much faster rate than anyone had predicted or imagined was possible. That was the time when the ‘exponents' in ecological science were discovered. Climate change, air and water pollution, melting of the ice caps and rising of the sea levels, as well as ozone layer depletion that exposed all living things to dangerous levels of cosmic-ray bombardment, the destruction of agriculture, were not linear processes as had long been thought, taking centuries to become critical, but accelerating exponential ones understood finally as the result of feedback mechanisms operating deep within (remember the Chamovsky Effect?) interdependent ecosystems. Anyway, the search for new ways of doing many things, including the construction of buildings, was put on an emergency basis. And naturally—or luckily, depending on the level of your faith in human ingenuity—the new ways were found.


It is true that the existence of human beings, especially, eleven billions of them, are not essential to the wellbeing of the planet. Indeed, quite the opposite, when we own up to the human causes of the planet's life-threatening problems. Still, people made up their minds to stay on and conduct their lives pretty much as they always had, with some serious behavior modifications. A new way of constructing buildings, for one, was discovered, or invented within a few years and, within a few more years thereafter, had all but replaced the long held traditions of making solid buildings.


The key was not in the discovery of some entirely new principle, but rather in the radical reinterpretation of a long known one: the equivalency of matter and energy. What Albert  Einstein had formulated so precisely in the early 20th century. the Science Consortiums in the early part of this century reformulated in terms of Free-energy Forms. The most important of these for constructing buildings was not the Field, but the Vectoral, based on a kind of ‘string theory' of electromagnetism that binds primarily visible light particles in a continuously regenerative way. A building built of these Vectorals in effect recreates itself nano-second to nano-second, working with and against other particles (and their respective forces, such as gravitons and gravity) as the Law of Continuity requires. In short, a building's enclosing surfaces are constructed of light, an infinitely renewable resource that is *simultaneously* matter and energy. As it turned out, the technology for constructing in this new way was fairly simple, though it developed rather slowly from massive, unsustainable machines to the compact ones used today, which are made using much the same methods of construction as they enable.”


.


*Coming next: What the cognitive scientist said.*



[![](media/lwblog-drwgs-stor-2-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/05/lwblog-drwgs-stor-2-4.jpg)


LW


 


#architectural_drawing #architecture #story-telling
 ## Comments 
1. Caleb Crawford
5.9.11 / 2pm


Is this based on the newly discovered magnetic properties of light? I am familiar with the potential application in terms of energy production. Unlike the photovoltaic effect, which happens on the surface, the magnetic effect happens in depth – a vector. Our human experience of energy gives us the impression of mass, volume. I am trying to imagine what your insight means for architecture. 


I am curious about the blue tower. That has suspended within it a number of rectilinear shapes which I interpret as volumes. Those graphically read as field. 


As you are probably aware, there's a lot of discussion about mass vs. lightness in ecological building. Mass has advantageous properties in the regulation of energy flows. American lightweight construction has a lot of comfort problems that are solved by throwing energy at them. Banham described two mindsets towards environmental management – the structure minded and the power oriented. One manages shelter through structure, the other throws energy at the issue. What you propose is a meeting of the two? 


Nice story.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.9.11 / 5pm
	
	
	Caleb: Thanks for your insight. Hang on until all three posts are made for an”answer” to your question.
3. Gio
5.11.11 / 3am


Really Interesting!


Mr. Woods are you reading from a book or did you really had this conversation with an engineer or scientist?  

 It sounds very possible indeed to build thinking like this.  

I need to star reading.


Looking forward for the next one.


Gio
