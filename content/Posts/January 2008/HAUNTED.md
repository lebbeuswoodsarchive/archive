
---
title: HAUNTED
date: 2008-01-13 00:00:00
---

# HAUNTED


[![Photo by Shandor Hassan](media/Photo_by_Shandor_Hassan.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/7.jpg "Photo by Shandor Hassan")[![Photo by Shandor Hassan](media/Photo_by_Shandor_Hassan-1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/6.jpg "Photo by Shandor Hassan")[![Photo by Shandor Hassan](media/Photo_by_Shandor_Hassan-1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/5.jpg "Photo by Shandor Hassan")[![Photo by Shandor Hassan](media/Photo_by_Shandor_Hassan-1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/4.jpg "Photo by Shandor Hassan")[![Photo by Shandor Hassan](media/Photo_by_Shandor_Hassan-1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/3.jpg "Photo by Shandor Hassan")


Shandor Hassan's photographs are of places that seem haunted. The feeling comes not only because we see the places at night, and devoid of people, but from a different kind of emptiness, one that is haunted by a non-human presence, or rather, by the ghost of something more vague, more abstract. This ghost is not at rest, as the stillness of the images suggests. We sense its uneasy presence, even if we do not think or speak of it.


These are not places we like to be. Yet they are here, ostensibly for us. The elements that comprise them are, if not exactly friendly, then at least familiar. They were made for us to use and appreciate. They are intended to welcome us, yet they do not. We enter these places reluctantly, only from necessity. Then we leave as quickly as possible. 


But what, exactly, haunts them? I believe it is the ghost of American modernism.


It is the ghost of a once-upon-a-time promise of a better life for everyone, a promise that never delivered. The convenience stores sell junk food that makes us fat. The service station dispenses endless fuel for our gas-guzzlers poisoning the atmosphere. The franchise restaurant is everywhere but belongs nowhere. The pawn shop may be easy, but it reminds us of our, and others', desperation. The promise haunts us and its ghost lingers at the edges of night, dreamlike and restless.


Then we come to the little illuminated house. How cheerful it is! But the ghost is there, too, mocking our optimism and good cheer.


[![shandor1lr.jpg](media/shandor1lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/shandor1lr.jpg "shandor1lr.jpg")


LW



 ## Comments 
1. [Ark](http://the-arkitekt.livejournal.com)
1.14.08 / 3pm


Most of these immediately bring to mind what was offered, not only by modernism but “the future of the road”. Automobile cultures threat to what I consider to be ‘real' communities shows itself here.


The roadside restaurant open 24 hours a day for late night drivers. The motor hotel or ‘Motel' for the tired traveler. The vast parking lot of the pawn shop. The service station to maintain your vehicle. All of these and of course the ‘drive-thru' for those that don't even want to get out of there vehicle, let alone stop.


Places to move through but never to truly stop at. Permeable and passive. So many of them over and over in a sprawl landscape, a simulacrum of a simulacrum so much so to the point that they hardly exist.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.14.08 / 4pm


Astute observation, Ark. 


Of course, the automobile was a prime instrument of American modernism. “On the road,” is its mantra, along with “the American dream.” 


Dream, indeed.
5. Spencer
1.15.08 / 9pm


I am perplexed by these photos. On one hand, as a place intended for the public they certainly do as you suggest. They leave a sense of emptiness, loneliness, and vacancy that is well played up by the photographs. It is through the wonderful eye of this photographer that brings this to light. On the other hand, if we think deeper they do succeed in their intention of progressing the American modern ideal. If we tint the lens to capitalism there are people who have (I assume) profited from these businesses. To others, being those people who have suffered to the notion of capitalism, these are the places they have to congregate, socialize, and recreate. These places would seem to these people, although ironic to us, their places of choice, maybe out of habit, history, or just because there isn't any other place to go.  

While there is a lot to say about the mundanely of these spaces there is equally a lot of significance. And, although it would not be a place I choose to be or design it still has some beauty to it.  

It's very fitting that you, Lebbeus, choose to post this right after the article on The Ugly. Clearly there is something to be said here about contrasting directions that The Ugly can take. In this case, The Ugly is accepted in spite of its limitations but still succeeds in exposing something profound. Does that make it beautiful?
7. [lebbeuswoods](http://www.lebbeuswoods.net)
1.15.08 / 11pm


Spencer: you raise (at least) two issues. The latter concerns the aesthetic. The photographer has indeed created beauty of a particular, modernist kind. Spare, stripped of non-essentials, almost minimalist. This is reinforced by the photos' haunting, existentialist feel of alienation. It is an aesthetic well assimilated into our culture by now, so it does not shock us. We all know it well. It's almost reassuring, at least to the ‘intelligensia.' And that's who contributes to this blog.


This brings us to the second issue: class. Social class, which, in our capitalist society, means economic class. There is, in my post, an expression of my professional educated class attitudes. As with you. When we speak of ‘us' and ‘them,' it's about class distinctions. Your comment about “habit, history, or just because there isn't any other place to go,” is quite correct. But I contend that the ghost haunts us all, only some more self-consciously than others.


I must confess that I am deeply invested in the idea of the truly new (therefore the Greenbergian ugly), because I think our present social conditions and privileged perspectives need to change.
9. Stephen Mohn
8.8.08 / 7pm


These photos bespeak the ghost of a promise that was granted. A promise made by fathers who saw death and destruction during world war II. They promised the next generation of children that polio and other diseases would not harm them, that they would not starve, and that they would be educated. These promises were kept and if there were a lack of promise it's the subsequent generations who screwed things up.
