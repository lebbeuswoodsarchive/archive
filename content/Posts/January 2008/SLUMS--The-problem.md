
---
title: SLUMS  The problem
date: 2008-01-18 00:00:00
---

# SLUMS: The problem


[![](media/slum-mumbai1a.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/slum-mumbai1a.jpg)


(above) Slum in Mumbai, India.


There are some problems that seem beyond solution. This is because the causes of the problems are either not known, not well understood, or are so paradoxical and contradictory, so hopelessly intertwined with one another that they cannot be effectively identified and addressed. The problem of slums [some prefer the term ‘squatter communities'] is one of these seemingly insoluble problems. They are a global problem and a growing one, as exponential population expansion in many countries forces a disproportionate number of people into increasingly untenable living conditions.


Most slums exist in countries struggling to emerge from colonial exploitation, economic isolation, political anarchy, sectarian violence, and a host of other conditions that do not effect more developed countries, or not so drastically. Poverty is the cause of slums—people do not have money, and little prospect of getting any. Thus they don't have adequate food, drinking water, medical care, education, or any way to escape their poverty by moving away or up. They are trapped in poverty, more or less without hope. But what is the cause of their poverty? The answer is brutally simple: an unequal distribution of wealth and the resources it can buy and control. Why is a country's wealth unevenly distributed? Again, painfully simple: because the people who have been able to get the money and resources want to hang on to them, and to get more. Their justification for doing this, even in the face of the visible horrors of poverty and the human suffering it causes, is that those who have the wealth and resources are best able to manage them well. If they were turned over to the poor, they would be squandered and wasted, because the poor have no experience at managing them. The stability of society itself would be threatened. Further justification lies in the presumption that if the owners of the wealth and resources are allowed to do their good job of managing, the poor will benefit, too, because the whole community will prosper. This is sometimes called the Trickle Down Effect.


The problem with these justifications is that they are much too optimistic, particularly for the emerging communities and countries where the most terrible poverty is rampant. The poor are in desperate condition and cannot wait for bits of wealth to filter down to them from the upper socio-economic strata, even if that were to happen. Tragically, the upper strata in the societies most afflicted by poverty and the slums it creates are most likely to be comprised of corrupt and rapacious managers of the wealth, whether in the form of political leaders or private entrepreneurs. Presiding over a politically disempowered and disenfranchised populace, the managers have no one to hold them accountable. Cycles of coups, civil wars, and revolutions usually replace one set of self-enriching despots with another, and the state of the poor is unaffected or made worse. One would hope for the enlightened despot to come along who would enforce true reforms that would improve the lot of the poor, but this has not happened and no one, especially the poor, can count on it. Meanwhile, their lives, played out among the most abject and dehumanizing conditions, goes on. Somehow, and in spite of everything.


Human beings are resourceful. Adaptiveness is the essential human quality, enabled by self-conscious intelligence. Where other animals can live only within a relatively narrow, biologically determined range of conditions, humans can modify either the conditions or themselves to such an extent that they can live at the extremes. Extreme heat, extreme cold, extreme poverty. People adapt and use their ingenuity and inventiveness to survive, but also to find meaning and purpose, and whatever degree of pleasure, even happiness, that humans may know. Living in the slums, which means living without many beneficial, even necessary, things, but also with so many threatening, even dangerous, things, is a great test of human ingenuity, and of the human spirit, which means nothing less than finding, or creating, a degree of satisfaction in being human. In the slums, people's ability to modify the living conditions is minimal, because they do not have the resources to do so. A few pieces of tin, scrap lumber, cardboard make a house. Clothing and food are scavenged from the refuse of others with more. Health care is homeopathic, and life expectancy is short. Education is in the home, but more often in the dirt paths that pass for streets in the slums. Childhood is truncated; children have to do something useful for the family's survival, as soon as they are able, or—orphaned at any early age, or cast out because they are too expensive to keep—fend entirely for themselves. Slum dwellers have no choice but concentrate on modifying themselves: adjusting their expectations from life to a minimum; surviving on a minimum of material means; learning how to deal emotionally with daily deprivations that would crush the pride and sense of self-worth of those accustomed to having even a modicum of material comfort and security. In the face of these conditions of existence, their resourcefulness is crucial. People with steady jobs and incomes, who are assured of having enough money to go to school, to the doctor or clinic; who can save some money, buy enough food and clothing to last a while; who can plan for the future; all too often coast along without thinking very much or having to fall back on their resourcefulness. But there is no coasting for the slum dweller. Everything is now, today, and each day is a new struggle for survival. The gains made yesterday were maybe enough, but they were consumed yesterday. Nothing carries over, except the needs.


Slum dwellers share something with people caught in a war zone, where the infrastructure of society has been interrupted or destroyed. They have to scrounge and improvise, just to have the basics pf shelter, food, heat. To survive, they have to be inventive. But the people in the war zone can look forward to the end of war, the restoration of society and its services. The slum dwellers have no such prospect. For them the war, its brutalities and atmosphere of cruelty and indifference to human life, never ends.


It is easy enough for people who do not live in the slums and who are nestled more or less comfortably in their lives to shudder at the unhappy fate of their fellow creatures, while at the same time feeling relieved that it is not their fate. Their security seems assured by their ties to the institutions, and persons, managing the wealth and resources. Their roles in the grand scheme might be small, but they fulfill them earnestly, and steadily, and surely they are necessary to the ‘system,' so long as they are loyal and useful. Or so they believe, or must believe. Actually, they—the middling servants of the great system—are mere fodder and entirely dispensable. Tomorrow, they could get their pink slips and be out of work, for reasons completely independent of their loyal servitude. Corporate mergers. Downsizing. Outsourcing. Accounting corrections. Computer errors. Their being cast into the streets, however, is largely metaphorical. Even unemployed, they are still part of the system. Someone will pick them up. They have experience, education, they are certified, conditioned, too valuable to be thrown away. Or so they hope.


The slum dwellers have not been thrown away either, because they have never been part of the system. A relative few manage to find paid work in factories, or as day laborers, but most fend for themselves, the system's illegitimate children, its orphans. They scavenge in city dumps, living in one way or another on the waste that others, better off than they, produce. And they do this ingeniously. Like the can collectors in New York City, where the state levied a bounty on a few recyclable materials, slum dwellers work hard to collect from the dumps recyclable materials like plastics, fabrics, rubber and even metals like iron from construction refuse and aluminum from domestic discards, selling them to scrap companies, who in turn sell them by the ton to major recyclers. From there the materials are returned in semi-raw form to factories and the cycles of consumption that constitute the global economy (see below). The slum dwellers, the scavengers and pickers, are part of the big system, but not officially, in the sense that they find a recognized and rewarded place. They get no benefits or perks from the companies that benefit from their labor, but get only what they can earn daily from the crumbs that fall off the big table.


From a safe distance, it is tempting to demonize, or romanticize, slum dwellers. On the demon side, they are parasites, unclean, unwanted, unhealthy, attached to the body of organized society. On the romantic side, they are outsiders, struggling subversively within the system, surviving by their wits and stubbornness, masters of that indispensable human quality, ingenuity. Each view is an extreme of the reality, and each serves the purposes of different interest groups occupying higher social strata. Consequently, both views in effect accept the existence, and persistence, of slums.


Of course, only the most rabid ideologues would openly admit such a thing. “We've always had the poor, and always will!” proclaim those on the political far right. “The poor will rise up, and revolutionize the whole society!” proclaim those on the political far left. Sad to say, the far right position is more widely accepted. The idea of radicalizing the poor is a dream or a fantasy of people who do not actually share their desperate conditions. They, the slum dwellers, have no time for political idealism—they are too busy trying, day to day, to survive.


There is much that is admirable in the way that slum dwellers struggle against overwhelming adversity, but admiration must be tempered by the realization that they do not struggle because they choose to, out of principle, or in the service of high social or political ideals, but because of their desperation at the brutal limits of survival. It is a mistake—and a grave disservice to them—to imagine that their ingenuity, resourcefulness, and capacities for self-organization can in any way serve as models for our present global society. To believe so would be to endorse the dog-eat-dog ethics that rule their lives and, all too often, those occupying society's more economically advantaged classes. To believe so would be to endorse the most cynical and degraded vision of the human future imaginable, a throw-back to the barbarous 19th century perversion of believing in ideas such as ‘the survival of the fittest' and ‘the nobility of poverty,' which justified the blatant exploitation of many by a few.


The only thing we can learn from slums today is that they cannot be tolerated in any form, or under any circumstances; that poverty, their most terrible feature, must, as rapidly as possible, be alleviated; that the wealth and resources of any community—which prominently includes its human resources—cannot be controlled for the benefit of an elite, under whatever name or ideology it goes; that the survival of the emergent, global society depends on its reformation of institutions—public and private—presently managing society's material and cultural wealth; and that reform must come not by violence from the lower social strata, but from enlightened leadership from the higher, if not the highest, strata of the social and economic structure.


[to be continued]


LW


(below) Slum dwellers scavenging in a computer dump in Accra, Ghana:


[![](media/slum-accra-ghana-1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/slum-accra-ghana-1.jpg)


See and read more in the New York Times article:


<http://www.nytimes.com/slideshow/2010/08/04/magazine/20100815-dump.html>


For more about the topics discussed above see TWO FOOTNOTES TO DANTE, a post on this blog:


<https://lebbeuswoods.wordpress.com/2010/08/19/two-footnotes-to-dante/>


Additional images and thoughts about slums:


<https://lebbeuswoods.wordpress.com/2010/10/12/knots-the-architecture-of-problems/>



 ## Comments 
1. Godofredo
1.21.08 / 9am


I fully agree with your words.  

Do you think architecture can play any important part in this fight?





	1. anil kumar.n
	8.7.10 / 10am
	
	
	yes, i think that in india most of the people in slum areas. mainly out of 100 members there are 75 peoples lived in india .so we have to improve our rules and regulations and mainly we have to improve our bad personal attitude about our slum peoples.
	
	
	
	
	
		1. anil kumar.n
		8.7.10 / 10am
		
		
		my suggession to our india central government is that , you have to improve rules and regulations on slums …………..
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.21.08 / 1pm


Godofredo: Yes. I think architecture has an important role to play that architects have yet to begin. There will be at least two more posts on this, which will be posted here very soon.
5. [Gregory Hurcomb](http://gregoryhurcomb.com)
1.23.08 / 3am


It is intriguing to consider the history of slums, which I am not fully aware of, but when was the condition of the slum first actualized; it would have to be sometime during or after the Industrial Revolution and be tied to divisions of labor, trade, commerce, the new economics of development and industrialization. Today as developing nations try to catch up at a breakneck pace with the global economic powers I can't help but think the slum is not something easily transformed or averted. The powerful seem unlikely to divest themselves of their spoils. How then to turn the tide of the conquerors so to speak? Perhaps architects can bridge the gap between neighborhoods and communities for better conduits and communication exchanges through differential spatial interactions – allow for better understanding and communication to occur.
7. [Rene Peralta](http://www.generica.blogspot.com)
1.23.08 / 4am


I believe your perspective is correct, and I applaud you for saying that there is no use for romantic rhetoric of our slums or squatter communities. Yet, In my experience in the city of Tijuana, a city where 50% of housing has an illegal origin, squatter areas have developed into consolidated communities in a lapse of 30 years or more. In the last 15 years the city has been wanting to solve the problem with private developer housing subsidized by the govt and created vast extensions of serialized housing units of 300 sf ( and some times 270) that have until now become our own version of “projects” housing. Therefore, due to its manufacturing industry and low unemployment rate, Tijuana's slum have been able to move ahead, at a slow rate, but have persevered in some cases as better planning projects in contrast to what architects have tried to draft.
9. Michael Wagner
1.23.08 / 10pm


A friend of mine just told me about this blog, and I will be checking back often. This was a great read, and I've been a huge fan of your work for some years now, even to the point of attempting to incorporate some of your ideas into my Master's thesis (though, admittedly, I don't think I did them much justice at the time). 


I've become especially interested in the possibility of hyper-development to the point of collapse, I.E. intensifying density and consumption to such a point that it implodes, and new patterns of growth can emerge that are wholly different from established norms.


Anyway, just wanted to drop by and show my appreciation for your work.
11. [Ana Maria Leon](http://www.flickr.com/photos/anamarialeon/)
1.25.08 / 1am


Following Rene's comments, a similar situation is taking place in Guayaquil. The city has developed land for serialized housing units, and the programs seem to be moderately succesful. At the same time, the city is also gradually providing the older, more established slums with basic infraestructure. Most slums here follow a consolidation process, in which they slowly replace construction materials [from cana guadua to cement blocks]. A big part of the problem is that slums are usually located in the land nobody wants [here, sea estuaries are filled with garbage and then turned into invadable land]. Both planned and unplanned growth is still mostly horizontal.


Looking forward to the next installments.
13. [Mark Primack](http://www.markprimack.com)
1.26.08 / 5am


You can read about the birth of slums in ‘Rebels against the Future. The Luddites and their war on the Industrial Revolution', by K. Sale.  

 I'm an architect in a small coastal town that, in its determined elitism, denied housing to even blue collar workers. I slogged through the ignominy of a city council election campaign to push through zoning ordinances allowing the conversion of suburban garages to studio apartments. People saw their aging parents, maturing children, au pairs or friends as tenants, or calculated the ‘unit' as an eventual retirement subsidy, and so embraced the concept. But I saw it as both an efficient densification of population and a quietly cumulative redistribution of the wealth of housing.  

 Every neighborhood in suburban America could ‘grow' by perhaps 35%- all small affordable units- without paving one additional inch of earth. No bureacracy, no pork barrel, no ‘clearances', just mom-and-pop urbanism.  

 A trained limner (though no candle to Lebbeus), I drew on nothing but nerve to materialize more housing than I may ever imagine. Might your entry be asking what architects can accomplish when they put their pencils down?
15. Spencer
7.7.08 / 3pm


I'm wondering out loud to those posters that live in the USA. Have you noticed in your city any of the symptoms of slums? 


Here in Seattle we are experiencing a few small encampments in our parks, particularly those parks that are not for recreational use but exist because they are steep slopes or next to highways. Our city government is sweeping them out regularly per the veiled request of neighborhood property owners.


Clearly what we are seeing is, at least Seattle and maybe the USA catching up to the rest of the world regarding poverty and slums. If your city is experiencing this too, what solutions and or positions have you seen your city take on slums and poverty? Are you seeing a rise or decline in poverty? How involved do you feel the public is in creating solutions?
17. [Architecture and Apathy | Nightly Built](http://nightlybuilt.org/?p=975)
2.13.09 / 8am


[…] Woods (LW) is one of the few exceptions. In three posts on his blog he gives an introduction to the problem of the slums, and he gives one […]
19. Andrew E
2.23.09 / 3am


there are slums in Boston. Triple deckers and rowhouses along blue hill ave, dorchester ave, morton st, etc. It's the gutter of New England
21. [Architecture and Apathy « Open Source City](http://opensourcecity.wordpress.com/2009/02/24/architecture-and-apathy/)
2.24.09 / 7pm


[…] Woods (LW) is one of the few exceptions. In three posts on his blog he gives an introduction to the problem of the slums, and he gives one […]
23. jenkins
9.24.09 / 2pm


mr. woods..i had you on a crit for a glass studio many years ago..sincerely, that was a real highlight even though you were lukewarm on my project.


i have been working on ‘slum' upgrading for quite some time now..i would like to share some lessons on the subject.


1 the upgrading program must not be disruptive..you can not move people around like cattle and their livelihood is usually dependent on location. 


2 the ‘slumdwellers' must be treated like clients..their input and acceptance is essential on many levels.


3 this can be done affordably but it can not be done on the cheap nor should it be. community involvement minimizes waste.


4 commercial activity within the settlement MUST be respected and accomodated.


5 people who live on a dollar a day can be remarkable at managing their money…there is more cash in these settlements than you think. codi and sdi (see below) have demonstrated the power of savings cooperatives.


i refer you all to these worthy organizations…if i was still young and unattached, i know where i would be.


[Click to access CODI%20in%20thailand.pdf](http://www.codi.or.th/downloads/english/Paper/CODI%20in%20thailand.pdf)




<http://www.codi.or.th/>


<http://www.sdinet.org/>


Samsouk and Jockin are fantastic people!





	1. jes
	2.9.10 / 9pm
	
	
	jenkins,  
	
	I am just starting a master's thesis on the subject of slums. I would love to e-mail you with a couple questions if this is a topic you're still interested in. Or anyone else that is involved in this issue. Let me know. Thanks! [jesking1@gmail.com](mailto:jesking1@gmail.com)
25. [Saksittua 15.7.2010 « Ajatuksia ensimmäisestä maailmasta](http://metsamies.com/blog/2010/07/saksittua-15-7-2010-2/)
7.30.10 / 8pm


[…] SLUMS: The problem « LEBBEUS WOODS – People with steady jobs and incomes, who can save some money, buy enough food and clothing to last a while; who can plan for the future; all too often coast along without thinking very much or having to fall back on their resourcefulness. But there is no coasting for the slum dweller. Everything is now, today, and each day is a new struggle for survival. The gains made yesterday were maybe enough, but they were consumed yesterday. Nothing carries over, except the needs. Slum dwellers share something with people caught in a war zone, where the infrastructure of society has been interrupted or destroyed. They have to scrounge and improvise, just to have the basics pf shelter, food, heat. To survive, they have to be inventive. But the people in the war zone can look forward to the end of war, the restoration of society and its services. The slum dwellers have no such prospect. For them the war, its brutalities and atmosphere of cruelty and indifference to human life, never ends. Kerro ystäville: […]
27. [TWO FOOTNOTES TO DANTE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/19/two-footnotes-to-dante/)
8.19.10 / 12pm


[…] LW comments, see SLUMS: the problem, a previous post on this […]
29. [Matias](http://www.airoots.org)
10.14.10 / 9pm


Dear Mr Woods,


What would you think of a text about “suburbs” which treats them as one big homogeneous entity? You probably think that the author should hop in his car and drive through some suburbs. First in his own city, then in other cities and then in other countries. At the end of the trip the author would have seen so many different types of suburbs, each time with their own dynamics and contexts that he would not be able to write about them in the same way. 


This is the way I feel about your text. You are making grand statements about a reality that affects 1 or 2 billion people in the world and in the process erase all differences and throw everything in the dumpster. 


If you use the same word to describe a neighbourhood in formation like Dharavi and a computer dump in Accra, whatever you say about them will be shallow. 


This type of broad amalgamation and labeling opens the way to all kinds of man-made urban disasters, starting with redevelopment projects which are typically based on the belief that by providing property rights and modern-looking amenities you solve the “problem” of the slum, but the in the process the local economy is destroyed, in spite of the fact that it was providing more for the residents that the government ever would.


Matias Echanove, an admirer of your work





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.15.10 / 1am
	
	
	Matias: I take your objection to generalizing about local communities and economies, but they are dwarfed by the inhumanity of the conditions in which they exist. Let me ask you a question. Would you write about issues of human rights only in relation to specific local cultures around the world? For example, is your position on whether people ought to be persecuted for their beliefs, arbitrarily imprisoned, tortured and even executed dependent on local attitudes, customs, or types of government? Or, do you believe that human beings everywhere have certain ‘inalienable rights' to be free of such violations of their dignity, well-being, and need to survive? If you believe that such basic human rights are relative (the first case), then you belong to a very large group of people who hold that such issues are local matters and that “you should keep your nose out of other people's business.” If, however, you believe that they are absolute (the second case) you belong to a small group of people who are willing to argue and struggle for other people's rights and not only your own. It depends on your point of view and it's your choice. I've made mine.
31. [Pressroom](http://pressroomelective.wordpress.com/2010/10/29/153/)
11.6.10 / 9am


[…] – Woods, L. (2008), Slums: the problem, internetlink: <https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/> […]
33. [SLUMS: a too-long story « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/29/slums-a-too-long-story/)
4.29.11 / 2pm


[…] on, we must try to understand the dynamics driving the creation of slums. If we think of slums as a problem that needs a solution, we must state the problem with more clarity than we have so far. We can […]
35. [COLLASUS: Office for Collaborative Sustainability](http://collasus.com/textresearch/indi)
5.15.11 / 6pm


[…] Cecil Balmond – Informal The Endless City – Ricky Burdett Lebbeus Woods – part 1: The Problem part 2: What to Do? part 3: One […]
37. Shila Abdula
7.6.11 / 2pm


I'm a student of architecture in India and as was asked before in the discussion I still would want to know how architecture could in any way change or affect the living conditions of the people who live in slums. Because as we know urban planners are the ones who are mostly in charge of these projects and they do a good job. So what difference would it make if an architect intervenes in a slum planning project?
39. [Ezinne Lily](http://www.facebook.com/ezinnel)
11.17.11 / 5am


i would like to be really clarified on the role of the urban planner in slum management.
