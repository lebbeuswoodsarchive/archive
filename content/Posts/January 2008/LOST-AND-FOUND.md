
---
title: LOST AND FOUND
date: 2008-01-21 00:00:00
---

# LOST AND FOUND


[![ld73-1lr.jpg](media/ld73-1lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-1lr.jpg "ld73-1lr.jpg")[![ld73-2lr.jpg](media/ld73-2lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-2lr.jpg "ld73-2lr.jpg")  

[![ld73-3lr.jpg](media/ld73-3lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-3lr.jpg "ld73-3lr.jpg")[![ld73-4lr.jpg](media/ld73-4lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-4lr.jpg "ld73-4lr.jpg")  

[![ld73-5lr.jpg](media/ld73-5lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-5lr.jpg "ld73-5lr.jpg")[![ld73-6lr.jpg](media/ld73-6lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-6lr.jpg "ld73-6lr.jpg")


I want to be careful of not straining the indulgence of visitors to the LW blog. With this post I might be pushing my luck. Still, I thought it might be of interest to people who know my work to see a few drawings of mine that I came across in an old plan file–certainly the first drawings I made in which I tried to give visual, even tectonic, form to philosophical concepts. They were made in 1973.


The paired, dialogical drawings seem most alive to me. In them, the main ideational threads–conflict and transformation–that run through my work up to the present, are visible. As is my excited embrace of contradictory modes and forms. This is seen in the interplay of the organic and the geometic, the fantastical and the mathematical, but also, less conventionally, in their subordination to both the premeditation and spontaneity of drawing. Architects make designs. Their designs have to embody—or at least allude to—the paradoxical nature of the human condition, and of our personal experiences. These were early attempts that, it seems to me, still resonate with a degree of clarity.


In these same drawings, some nascent formal experimentation appears, the beginnings of a personal language and grammar of formation that has been developed by me over the years since then, in numerous projects and studies. What is interesting—and a little frightening—is that the basic forms, and ideas, was there from the beginning.


The two more identifiably architectural drawings are less interesting today. The ideas, while well enough drawn, are more conventional, more related to architectural developments of their time. So be it. We have to learn. And also, we must never forget, it takes a long time to understand architecture, let alone create it.


[![ld73-8lr2.jpg](media/ld73-8lr2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-8lr2.jpg "ld73-8lr2.jpg")[![ld73-7lr.jpg](media/ld73-7lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-7lr.jpg "ld73-7lr.jpg")


LW


ps. The series of which these drawings are a part was intended to be a ‘treatise on architecture,' but was never published. Naturally, it also included many pages of dialogical text, such as the one posted here….


[![ld73-9lr.jpg](media/ld73-9lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ld73-9lr.jpg "ld73-9lr.jpg")



 ## Comments 
1. aaronnajera
1.23.08 / 12am


I like the way you approach the paradox. In a way, your drawings present a very ordered world, but just beneath the surface, organic forms seem to appear of within making it truly chaotic. But you seem to be saying, chaos is the natural order of things. And chaos is order itself too.
