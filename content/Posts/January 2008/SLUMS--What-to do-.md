
---
title: SLUMS  What to do?
date: 2008-01-28 00:00:00
---

# SLUMS: What to do?


[![luandaslum1.jpg](media/luandaslum1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/luandaslum1.jpg "luandaslum1.jpg")


It must be said at the outset that no enlightened political leaders in any part of the world can legitimately believe in the practice of what is called ‘slum clearance,' which refers to the demolition of slums and the displacement of their inhabitants without a thought about where they can go. This is not to say that the brutal practice of bulldozing slums and driving out their inhabitants with armies of police is not being carried on—it is. Recent examples in Africa and Latin America only testify to the persistence of despotic political leaders in places where people have little voice in public affairs. Elsewhere it is well recognized that such an approach simply relocates the problem at a high human cost, postponing the day when it must be dealt with more humanely, and on a more enduring basis.


Secondly, it must be said that the idea of ‘urban renewal,' which is a less blatantly brutal but still violent approach to the elimination of slums, simply does not work. The practice of demolishing slums and then imposing large-scale housing projects has generally failed, for the reason that slums do have social structures, however misunderstood they may be by those of the higher socio-economic strata from which come the urban planning professionals and bureaucrats who design the renewal projects. It has been shown by many tragic examples that simply replacing slums with planners' ideas of what people should be living in destroys much of human value that can never be replaced, and causes untold human misery. Slums are inhabited by human beings, many of whom, even at the desperate edge of survival, have invested themselves in their families and communities, and want a better life for themselves and their children. Not unlike many others who are at the lower end of the economic chain, they need help in coping with their circumstances, help that comes from those who control the wealth and resources.


The burning question is: exactly how—in practical terms—is an enlightenment of the ruling, or at least the managerial, classes to come about? What are the best possible scenarios?


At the top of the list: the increased availability of information will make politicians and business leaders aware of the human catastrophe of slums, and this will mobilize them to improve the slum dwellers' living conditions. In short, government and corporations will make the elimination of poverty a high priority.


This is a most unlikely scenario. The availability of information has done little to mobilize leaders in the past, from stopping the Holocaust to the genocides in Bosnia and Rwanda, the famines in Africa and Asia, the ‘death squads' in Latin American countries, and many other human tragedies that could have been stopped by the intervention of political leaders. Knowledge of slums is today widely disseminated in the print and electronic media. Leaders give occasional lip service to this problem, but little else.


Another possibility: elected officials and business leaders will recognize that the vast, interconnected webbing of the global economy cannot carry permanently the burden—financial, political, moral—of burgeoning slums. As a result, government and corporations will find more effective ways of employing the slum's under-utilized human resources.


This is a somewhat more likely scenario, given the right conditions. The costs of slums, like those of a deteriorating environment, are often hidden because they are purposely overlooked, but they are enormous, and cumulative. Slums are increasing in many urban areas already the most afflicted by them, and so is the economic drain they cause. This drain comes from the costs of ‘containing' slums, which includes the costs of policing at least the perimeters where they abrade with more acceptable urban areas; the costs of dealing with humanitarian crises caused by outbreaks of contagious diseases that might spread into the wider urban population; and of water pollution from untreated sewage, including human waste, being dumped into rivers and streams that must be shared by all; the costs of lost city services, such as potable water and electricity, that are appropriated by slum dwellers without paying for them; the costs of keeping order when unrest or mass violence occasionally breaks out in the slums for whatever reason; the costs—often indirect—of maintaining a large population of illiterate and uneducated human beings, who nevertheless require not only food and shelter, but also intangibles like personal dignity and social justice, which must be ‘paid for' by somebody, usually elsewhere, in the social network; likewise, the costs—psychological and moral—of having to live with slums, costs paid for by the other social strata in the society afflicted by them. Slums drain a society's resources, and are a form of entropy that threatens, in the long run, the society's survival.


Finally, if the perspective is altered to a purely capitalist one, slums can be seen as an unused pool of human potential—that is, of cheap labor—that could be employed in the global economic system. Businesses, supported by government trade policies, have recognized for many years the advantages of cheaper ‘offshore' labor in the making of many consumer products. As nations such as China and India and Indonesia develop their domestic economies and expand their global influence, the demand for cheap offshore labor will dramatically increase, even as the present ‘outsources' dwindle. New sources of skilled, semi-skilled, and unskilled labor will have to be found—-or created. With the same sort of investment made in training workers in the garment and other consumer products industries in Southeast Asia, present-day slum dwellers could take a first step up in the economic chain. The main impediment to this happening is that government and business would have to cooperate in a coordinated way, and, so far, neither social sector has shown any real interest in doing this.


However, the idea of turning millions of people who have been held down in abject poverty into millions exploited in subsistence-wage sweat shops and factories is far from an ideal solution to the problem of slums. It might be an economic step up, and a point of entry into the game of capitalism, but it amounts to a type forced labor, where the slum dwellers would have little choice but to accept it, considering the alternative of continued abjection and destitution.


An intriguing hypothesis—advanced by a number of people—emerges: what if the slums could be improved from the inside, rather than from without? Or, to put it another way, what if the interventions coming from without were aimed at empowering slum dwellers to find—-or invent, using their ingenuity to adapt—-the ways to transform their own conditions? After all, they understand these conditions better than anyone, where they work for them and where they do not. If the slum dwellers have admirable ingenuity in surviving under the most terrible of conditions, why should this same ingenuity not be the key to transforming slums and eventually eliminating them?


The biggest task would be addressing the problem of changing the terrible physical conditions of slums. How might the vast pool of human energy embodied in the people who live in slums be liberated to engage the physical transformation of their place of living–their habitat? Answering this question will take much more than political good will, and more than the commitment of money by public and private institutions to such a project, even in substantial amounts. It will require new ideas about how to effect real changes in conditions, and from within.


This is where architects come in.


[To be continued]


LW



 ## Comments 
1. forkfingers
1.28.08 / 8pm


I just found this blog and am excited to read more. I've always loved your concepts and propositions and I'm interested to see what you propose for this. I only wonder what architects can possibly do. Create cheap slum housing from shipping containers? The way I see it even if architects were to create better living conditions, it wouldn't remain as so. With better living conditions would come an ease for the population to propagate and over exceed whatever living capacity is designed for them with or without proper financial means to do so. You said it yourself, “Humans are resourceful” and given any living conditions, humans will find food, shelter, and sex. With slum living conditions as they are, most of the children that result die of malnutrition or disease. Ease those living conditions and those children will propagate more children to the brink of those living conditions.


Now, this does sound pessimistic, and I do agree that the genocidal dictators have created horrific conditions that should have been addressed by other worldly rulers. But looking at the general slum condition of zero financial means is just a look at the edge of humanity's sustainability bubble. We're living beyond our means and these are the people pushed to the outer rings of it.


The only way I see ending the existence of slums in the world would be to limit child birth worldwide, and frankly, the world wouldn't stand for that (other than in China, where it's forced). The balancing of wealth from the prosperous to the destitute exists only in the utopias created by the most ideological socialists. It all works in theory, but even in reasonable applications (again, as in China) there exists poverty and poor living conditions.


I'm not really sure where I'm going with this other than that the concept that there may not be anything we can do about it. “Slums” have existed in various forms for as long as recorded history, not just the industrial era. The pursuit of balancing the wealth is a noble one and may ease things on a slight scale. Think about it from time to time, toss your $2 in the UNICEF bin if it heals your conscience, adopt a Tanzanian. I'm not sure.
3. [shandor hassan](http://www.shandorhassan.com)
1.29.08 / 3am


Shortly after a mass eviction of residents from the artist loft building located at 111 First Street in Jersey City, NJ where an artist community had been for some 20 years, I found myself in eastern Turkey at the base of the Caucus Mountains. I had been asked to document a music and arts-culture festival, for an NGO based in Istanbul. It was an event that brought people and performances from the surrounding countries of Georgia, Armenia, Arzabajan. During my time there I explored the slums, or the “gypsy” neighborhoods, and witnessed the daily efforts of survival, and the way that people developed/cultivate place and home with limited resources, as you outlined in your first essay. During my stay in Kars I became aware of a larger scale “project” of the city which was to renovate/restore various historic buildings, from the Ottoman and the Russian periods, which included the plan to create hotels and museums, a new environment that would bring a renewed interest into the historic area, a seemingly great plan of “urban” and cultural renewal, and an exciting restoration project for the professionals and the students that worked there, but perhaps not so great for the people that lived there.


Evidently a group called Global Heritage Foundation based in Stanford California along with the Mayor of the city of Kars, were leading the plans for the development of this part of town; where the gypsy communities were. It seems a lot of other people are interested in this project as well. It became apparent when I was at a dinner of several hundred people that there was a lot of money being represented, some from the World Bank, others from a Norway business group, the United Nations and some from tourism developers from Istanbul, and businessmen from New York City; in fact it was a big networking party, all in attendance seemed to be in either development/tourism, finance, or politics, none of the residence of the area to be developed were present, as far as I know. What could possibly be so important that brought this group together? Merely a historic renovation? 


 It seems that this kind of dinner is taking place everywhere from Kars to Las Vegas, and beyond. Most of us are not invited to this dinner table. And while this development project is on the surface very interesting, with the rebuilding of an old Hamam (Turkish bath house), and other historic architectural structures, there is a much greater movement going beneath the surface. I think about places closer to my home, where I personally have been effected by development plans, and its “progress”, and what kinds of meetings, dinner parties, champagne toasts, that property owners, developers, investors, politicians, architects, and even Starchitects are having, about the future plans of Brooklyn or Jersey City, and whose future are they toasting to? It seems there is an unspoken model at work, that includes “important” representatives from various positions in society, with various official titles and expertise, making decisions about other peoples lives. 


What is the role of the architect? You ask, again. Do you suggest that the architect could be a kind of doctor, or healer? At least here you are making the basis of a diagnosis.  

I would say in some instances that the architect could direct and educate those that have the power to finance or influence: the economic sources. Ultimately all of this is pointing to the survival of all human life, but also maybe it is a new definition of the word architect. Perhaps there is a way that social integration can be influenced by architectural practice; in its totality, but will the people in power listen? Or wait until they themselves are submerged in the fecal matter, and then realize they were a bit to greedy to take the time to listen. Most people are blind (perhaps for good reason) to the reality that there is something wrong with the whole “structure”, the rotten foundation. What can be done? I am of the mind that slum dwellers and the lower strata of society should, or have to be empowered to develop. Yet as the resources that are below and above the surface, like water, become more valuable, and fear and power take hold and continue to control peoples lives it seems there will be some serious confrontation to contend with.
5. Nathan Bishop
1.29.08 / 5am


Slums to be used as holding-pens of cheap labour? This leads into a topic which you have not yet mentioned: the geographic factor. Every single slum, be they in the same city, country or continent, differ in conditions and needs. I have had the privilege of visiting favelas in Rio De Janeiro. Here the problem is made even more complicated by the criminal-element (drug-gangs) and physical geography. Speaking to capitalism, favela inhabitants earn on average $150 – $200 a month and many are able to find jobs in the city. This is not comparable to Kibera in Nairobi. Nor does it work in the opposite sense: Mogadishu, the largest city (not a slum) in Somalia has suffered from the absence of a functioning government for more than a decade and its people are subject to the same lack of government services as slums, but not necessarily inadequate and informal housing. 


The complication of the geographic factor makes be-all, end-all solutions impossible (be they slum-clearance or slum-recognition). Rio De Janeiro has had some success with its Favela-Barrio project (slum-neighbourhood), despite the presence of gang violence. Hernando De Soto suggested in his book “The Mystery of Capital: Why Capitalism Triumphs in the West and Fails Everywhere Else,” that if slums were to be legally recognized on a world-scale, the value of these buildings would be in excess of $9.3 trillion. Unfortunately this does not solve the problem as it only provides a one time influx of cash which ignores self-sufficiency (unless slum dwellers realise that they could get loans to start a business using their home as equity), and it is only of assistance to home/building owners, not renters.


It is a very depressing problem because it appears that almost nothing works. I do however love the second sentence in your last paragraph. It sounds very uplifting.


Forkfingers, I do not agree with your idea that those in slums are pushed to the outer ring of sustainability. In fact, I find there is so much to learn from slum dwellers in terms of sustainable living and architecture. Many favelas in Rio De Janeiro utilize storm water collection to create access to potable water. It appears that the very definition of a slum requires its inhabitants to make more with less.
7. Godofredo
1.29.08 / 12pm


Dear “forkfingers”, do you really believe in anything you've just written?


Dear Lebbeus, how would you empower slum dwellers and at the same time prevent them from immediately escaping the slums to go and live in another part of town? And in such a way the slum itself would remain the same…  

How do you prevent the slum dwelling community from simply evading the slums once they have means to do so? And consequently, do you end up addressing the people or the slum itself? Improving the slum from the inside for it to become something else?  

In a way, trying to convince the guys in the slums that they are living in a radical experiment of architectural creativity seems a bit imposing… The slum dwellers should have the same architectural rights that everyone else does – even if that implies the right to live in shopping-mall city. If you are a slum dweller the quest is for integration and not for a special status that perpetuates the feeling of alienation and inferiority.  

Also, if one simply improves the slum (structures/services) without addressing its origins then immediately more people will head into the slum, and new peripheral slums will develop…  

So it seems to me that the architects should focus on the already formed slums yes, but primarily on strategies for preventing slums ever to become.  

This would imply ability for architecture/urbanism to be ready at hand, prepared to house thousands of people, prepared for war, for political/territorial transformations.  

Would such architecture necessarily be nomadic and ephemeral? Maybe.  

Would it imply strong political will and specific mechanisms? Definitely.  

In the end I think the best way to combat slums is to address the problem of how the new megacities of the world are being developed.  

Just look at Dubai and wonder if any star architect has ever designed housing for the millions anonymous workers.
9. forkfingers
1.29.08 / 2pm


“Also, if one simply improves the slum (structures/services) without addressing its origins then immediately more people will head into the slum, and new peripheral slums will develop…”


this was exactly my point. and do you really believe that you can build enough housing for every person in the world to live comfortably? and for it to never push capacity?
11. Godofredo
1.29.08 / 2pm


I'm quite sure that if you don't adress the problems facing architecture you won't face the probability of faillure – which for some people might be very confortable.  

But to give up because you can't bother to be inventive and don't have the will to imagine, is I believe, against everything architecture should stand for.  

Apparenlty you are only interested in solving those problems for which you know the answers in advance.
13. Godofredo
1.29.08 / 3pm


Also, it was precisely such “laissez faire” liberalism and hypocritical contempt for workers rights that produced slums in the first place.  

One could see the history of architecture as a gigantic series of failed attempts to achieve impossible goals. That's something to be proud of.  

Or would you prefer to stick to the amazing complexities of designing luxury flats in palm-tree-shaped islands?
15. [lebbeuswoods](http://www.lebbeuswoods.net)
1.29.08 / 4pm


Godofredo: You make a new and important point. We must change the ways slums are growing, avoiding as much as possible the expansion of existing slums. To do this, however, we must understand the mechanisms that created slums to begin with and keep them growing. The primary mechanism is the migration of poor people from the country to the city in search of a better, more economically rewarding way of living. Once moved, they become trapped in the deeper poverty of slums. You seem to suggest that they be given an alternative to moving into urban slums. A very intriguing thought.


Your sentence “…this implies an ability for architecture/urbanism to be ready at hand, prepared to house thousands of people, prepared for war, for political/territorial transformations,” conjures visions of a future landscape of nomadic communities, self-sustaining and politically independent, or of the dispersion of cities as centralized human centers. Two references jump to mind. First is the Roman military camp that was built every night as Caesar's legions marched across Gaul–abandoned, their grids became the sites of future towns. The other is the ‘tent cities' organized in Zaire to accept Rwandan refugees in the mid-90s. Neither can be a model for the future, but are, oddly enough, precedents. They both ‘worked,' as what Hakim Bey calls ‘temporary autonomous zones.” Certainly, architects and others need to visualize much better possibilities.


At the same time, we cannot abandon existing slums. In an upcoming post, I intend to address the need for architects to have ideas about them and ways these ideas might be implemented.
17. forkfingers
1.29.08 / 4pm


I admit I am not imaginitive enough to solve the world's poverty problem. I've worked on this problem in the past will continue to do so in the future, but I won't beat myself up over it when at the end of the day I recognize that I haven't solved the world's slum problem. And I would be entirely open to an idea that seems fitting enough to do it, but I have yet to see something so worthwhile. Sure, creating cheap housing may give a number of people homes and will make them happy, but they will most definitely overcrowd them given time. And although this is just a temporary fix and doesn't solve the problem, it does leave them better off than before. So sure, let's raise money and get imaginitive and design the best and cheapest housing we can with that money. And maybe if we do enough of it, all these people will have homes. It's been done, and it's going to be done.


You're not bringing anything new to the table, you're merely slamming me for playing devil's advocate. What I prefer to design or what I am interested in answering is not the topic of discussion here. The design of quick, cheap, disaster relief housing has been a hot topic for years now. These sorts of design competitions were all over when Katrina hit New Orleans, or that tsunami hit the South Pacific the year before. And a number of starchitects did indeed submit entries.


Simply because I recognize these goals as “impossible” does not mean I don't attempt them; although I don't necessarily see these attempts as something to be proud of. It's just work. They're just ideas. I'll be proud when something good comes of them. I am not proud of failed attempts.
19. forkfingers
1.29.08 / 4pm


The idea of the anticipatory architecture is very interesting indeed, but it still does not solve the problem of food or money or labor. Perhaps if communities, like the Roman grid camps (a good example I quite like) that are self-sustaining complete with agriculture and community labor were created, we could solve most of these problems all at once. These people move to the cities to seek money or opportunity, but perhaps they instead should flock to communities set up for this anticipation where there is work ready for them to work for each other. This would be similar to many of the self-sustaining “hippie” communes you can find around California.
21. Godofredo
1.29.08 / 4pm


Lebbeus, I meant that people very easily abodon the slums once they are able to do so. Not into urban slums, but into the consolidated parts of town (with better infrastructures and services).  

This natural and legitimate desire to move away from such places prevents the slums from generating an economically diversified community.  

So another question might be which mechanisms can contribute to stabilize slums (I would have the problem in another direction though…).  

An issue raised by Mike Davis is that the agressive capitalism and social exploitation (provoking the unsustained agricultural collapse in the origin of most of the slums) becomes even more agressive and more unjust inside the slums themselves. So I'm not sure if the kind of inventivness you refer to will produce anything better.  

The slums are foremost a political problem of a new social organization, of dealing with deterritorialized communities.  

But they aren't simply a problem of the slums themselves but also of the anachronistic modern towns.  

Where is the “polis” if half the population has to camp outside?  

Cities have to become able of incorporating extreme populational mutations. Western cities are nice and clean. But they serve and represent only a minority.
23. [lebbeuswoods](http://www.lebbeuswoods.net)
1.29.08 / 10pm


A number of really important ideas are expressed in today's posts. I am a slow thinker and must reflect on them a while, but want very much to comment in response, because I think there is a chance that we can make some progress here. It will take me a day or so….


Right off, I want to thank Shandor Hassan for his story about Kars, in Turkey. As he says, it hits home to many of us living in ‘nice and clean' Western cities, because it exposes the mechanics of capitalist ‘development,' which always aims for the profit of a few. Yes, I believe architects can be healers, where the wounds of conflict are treated with respect. But also, I believe that architects need to instigate conflict, where it is necessary to initiate change, maybe in those places and times where the wounds are covered over, and ignored.


Clearly, ‘what to do?' arouses a lot of passion, and thought, and highly articulate comments. Let's keep the discussion alive….
25. [lebbeuswoods](http://www.lebbeuswoods.net)
1.30.08 / 2pm


Nathan Bishop: Your economics viewpoint is valuable here. More or less obviously, capitalism—as presently conceived and practiced—is at the core of the poverty problem, hence of slums. I am curious what you think of capitalists like Muhammad Yunus (Nobel peace prize 2006) and the Grameen Bank in Bangladesh (<http://almaz.com>), with its innovative ‘microcredit' policy specifically created for the poor. Do you think this might be an answer for all the diverse slums you describe?
27. Nathan Bishop
1.31.08 / 3am


I am aware of micro-credit, but my knowledge of it is limited. I am not an economist. That essay was the result of observations I've made travelling to third world countries (although it is very much an issue in the western world too). There are two concerns for micro-credit. First, most slum inhabitants rely on the informal economy as a means for being. In the most recent issue of Harvard Design Magazine, an article entitled “A Billion Slum Dwellers and Counting,” the author discusses a book by Mike Davis:  

—  

Second, Davis finds that informal economies, which provide most of the new jobs for urban migrants, are under increasing strain. Employment in the informal sector is not generated primarily through new initiatives but by dividing existing jobs and incomes; in stagnant or shrinking economies, more people are competing for diminishing amounts of work. Moreover, informal activity is not a guaranteed path into the formal economy, and it places a particular burden on women and children, since it lacks the protections and benefits of formal employment. Then too, Davis argues that increasing competition in the informal sector dissolves networks of self-help and begets racial or ethno-religious violence. 


Davis sees the informal sector becoming increasingly desperate, preyed upon by militias and charismatic religious groups. In a bleak epilogue, he laments the absence of any “official scenario for reincorporating this vast mass of surplus labor into the mainstream of the world economy.”  

—  

Thus it is what people do with their micro-credit which becomes of interest. Obviously they will try to turn it into a profitable enterprise, but whether this is in the informal or formal economy is questionable. Hernando De Soto's book, mentioned in my first post, points out that bureaucracy prevents start-up, formal businesses in third world countries. 


The second issue with micro-credit is distance. In many cases, slums are pushed to the exurbs. If a slum dweller did obtain micro-credit to open their own business, and did it legally, they could only service those in the slums. A formal business that only serves those with little to no money really does not stand a chance at becoming profitable.
29. Godofredo
1.31.08 / 10am


In this discussion one can also not forget the extensive land speculation and increase in land prices that have pushed old and poor people to the periphery. 


But I have a broader question: is it possible to separate slums from the specific mechanics of the capitalist system?  

Isn't ‘microcredit' in the end a method of profitable integration and control which (useful as it may be in some cases) steps aside from addressing the real issue of social and human rights? The right to have decent housing, services, healthcare etc..?  

What about state responsibilities?
31. forkfingers
1.31.08 / 2pm


To be free from the capitalist system and receive housing and healthcare, the government would have to set up free land, free housing, and free taxes, for a new autonomous community that would not likely contribute anything to said government's economy. I can't see a reason any government would invest that kind of money. State responsibilities and human rights are nice and fine, but unless the outcome of whatever system set up for them results in some sort of contribution back to the capitalist system that initially supported them, no one would agree to it.


The idea of the labor camp (for lack of a better phrase) that creates work and stimulates the economy while at the same time supporting the slums is a realistic one. I know this sort of thing exists throughout Africa, but from what I understand, most often the men leave the slums to work at these factory towns and leave the women with numerous children and AIDS and usually neglect to send any of their earnings back. Now, that sort of unethical action is not exactly the responsibility of those who set up the system, but if these town integrated men and women in a positive, community suporting atmosphere, we might see it evolving into more self-support (which is the ultimate goal).
33. Godofredo
1.31.08 / 4pm


The slums are a direct consequence of the capitalist system and we should never forget that.  

Its not a matter of begging for money and asking for help, but of fighting for one's rights…
35. [lebbeuswoods](http://www.lebbeuswoods.net)
1.31.08 / 11pm


Colleagues: Seems that—like it or not—we're stuck with capitalism. 


What we need to do is find a way around capitalism, or a way to use it against itself. 


I believe the way to do this is to work at the periphery of its interests and influence, where we can work with even radical new ideas, because ‘no one is watching.' I do not believe that capitalists or capitalist institutions (including governments) will help us. We (assuming we're not capitalists-at-heart) have to begin by joining together and pooling our resources. Judging from the comments so far, I'd say these are intellectual—both analytical and inventive.


Let's not forget for a moment that ideas are powerful. Especially good ideas–ideas whose time has come.


So, let us see if we have any good ideas.


Counting on your critical engagement, and criticism, I would like to advance one idea. If we think that the slums should be reformed from within, then maybe we begin with their physical reformation. Maybe their physical reformation could be an ‘internal industry,' that somehow generates value, within a slum community. It doesn't have to be ‘monetary' value, but it could be in the form of barter and trading within the community itself. No doubt this spirit of cooperation already goes on in slums. What if it were formalized, given community-wide form? If it were, then what would its physical production (energy, shelter, sanitation) look like? 


Architects, if they turn their talents to it, could give this idea shape, material, form—and momentum.
37. Vick
4.7.08 / 2am


Wow!


truly mindblowing have been expressed here. just found this blog and found your views and ideas, mr.lebbeus as fascinating. having studied and being extremely interested in unitary urbanism, i think your works are a prelude of what to come. MVRDV's Metacity, Constant's New Babylon, and now your works, truly inspire me! am currently doing a project about improving low cost housing and the usual sprawl arrangements. thinking about voids and its possibility of spaces in the middle of a low cost apartment…
39. Vick
4.8.08 / 9am


i think collectivist solutions do inspire the best methods and strategies of improving the current scenario of slums.  

looking on an urbanist scale, only an entirely collectivist solution would be able to mend this problem once and for all. 


i do think that the barter trade amongst communities could turn out to be a good idea, however, there are many restrictions and limitations surrounding the physical form of a building. this brings us again to changing the norms and notions of architects. it has to start from here.
41. Maurice Omollo
4.14.08 / 10am


I read the Slums: what to do? and I immediately got interested. There are many facts in the contribution but one would really wonder whether the needs of the slum dwellers will ever be considered in the improvement of their welfare! I consider the case of Nairobi's slums, whose populations are the majority in the city and indeed the area to go for in elective positions for the politicians; who also determine their fate! The question is ‘who would like them to disappear when these provide them with votes?' The destitute state of most of the slum dwellers suggest how they are bribed to vote.


The direction of letting the slum residents determine their priorities is what may be considered a ‘participatory approach' to development. However, their participation will only make sense if their immediate need – which is known (finance – to cater for their basic needs), is accessible. How then is one to provide the slumdwellers with this? It is here assumed the slums have been considered as areas for human habitation not to be bulldozed as has always been the case. The answer may be found in ‘affirmative action' for the slumdwellers which would require a multi-faceted approach. 


Omollo
43. [Architecture and Apathy « Open Source City](http://opensourcecity.wordpress.com/2009/02/24/architecture-and-apathy/)
2.24.09 / 7pm


[…] Woods (LW) is one of the few exceptions. In three posts on his blog he gives an introduction to the problem of the slums, and he gives one idea. He […]
45. Luis
11.20.10 / 7pm


Slums are the responsibility of the nation that has them. Whether they are a humane society or not is their problem. Slums are direct consequence of lack of social responsibility. Slums are not a result of Capitalism, go to La Habana, Cuba and see a communist slum. The whole city is just a slum! The way to eradicate the slums is through self enterprise with a government oversight. Residents become the builders of the new community. However the real slum is in the mind of the slum's residents. They have no skills and are a parasite to society. cheap labor? Yes, but unskilled! Slums, are the source of organized crime, social ignorance, communism, and the cry of having what others have strive hard to obtain and they are not willing to strive; They must be eradicated!
47. [COLLASUS: Office for Collaborative Sustainability](http://collasus.com/textresearch/indi)
5.15.11 / 6pm


[…] Balmond – Informal The Endless City – Ricky Burdett Lebbeus Woods – part 1: The Problem part 2: What to Do? part 3: One […]
