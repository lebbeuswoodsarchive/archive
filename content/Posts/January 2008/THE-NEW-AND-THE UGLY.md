
---
title: THE NEW AND THE UGLY
date: 2008-01-08 00:00:00
---

# THE NEW AND THE UGLY


(This is the third in a series of posts on the relationship between aesthetics and ethics.]


[![Jackson Pollock, painting “No. 27,” 1950](media/Jackson_Pollock,_painting_“No._27,”_1950.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/pollock1.jpg "Jackson Pollock, painting “No. 27,” 1950")[![“Full Fathom Five,” 1947, by Jackson Pollock](media/“Full_Fathom_Five,”_1947,_by_Jackson_Pollock.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/cc25a.jpg "“Full Fathom Five,” 1947, by Jackson Pollock")


The art critic Clement Greenberg once wrote that the new is always ugly. This is because it confronts us with experiences and ideas that we haven't encountered before and don't understand, or, at least, are not accustomed to. It follows that, because we live in a society, and an urban landscape, driven by the new, we are in for substantial, even perpetual, ugliness. His concern was the aesthetic, but also the ethical. He wrote in a post-WWII period not only of rapid expansion of American cities and the social landscape they create, but also of Existentialism, which made ugliness—if it was ‘authentic,' that is, if it emerged from the inner nature of a thing—a virtue. Prettiness was conventional, easily acceptable, and, in a time of rapid change, an ethical crime against truth. Prettiness was used as a cosmetic by advertisers and other commercial—and political—interests to disguise the difficult, even tragic, struggles that social, economic and technological changes were forcing upon people and their ways of thinking and living. Prettiness was used by the powers-that-be not to make the new more digestible, but to disguise its deeper implications and ethical imperatives. It was a way of saying, ‘Don't worry, everything is normal–just go on as you always have.' In other words, “Just let us keep running the world as we always have.' 


Occasionally artists manage to create the truly ugly. The painter Jackson Pollock, whose drip paintings were prime examples of Greenbergian newness, hence ugliness, was driven by ideas of the ‘automatic,' which was a term for the uncontrollable forces of both the psyche and of society. Mega-scaled urban and suburban growth had made central control impossible, much the same as mass production and mass media had a generation before. The technological machinery was no longer guided by a single person or elite group; rather, the elite, to gain commercial advantage, had broken big machines down into ever smaller, more salable ones, relinquishing control of the changes they wrought to ever more dispersed and more ubiquitous ‘consumers.' Pollock's tangled paint drippings and swirls were at once an assertion of one person's independence from the norms and a poignant expression of his surrender to the unpredictable, the vicissitudes of modern existence. Their violation of the aesthetic norms of beauty were at once exciting and challenging. They existed at an edge between order and chaos, balancing traditional notions of composition with inchoate scrawls and scribbles. Their ugliness indicated a way forward. We could live with uncertainty without abandoning all noble traditions.


If we follow through on Greenberg's thought, we realize that eventually things we didn't understand become familiar: yesterday's ‘ugly' becomes today's ‘pretty.' It's telling that Pollock's paintings, even though they've become icons of modern art, are still considered ugly by many people–a sign of their enduring newness. 


Question: have architects managed to create the truly new and enduringly ugly? If we look at Le Corbusier's chapel at Ronchamp, I think we can agree he succeeded in doing so here. Gehry's Bilbao Guggenheim, which we might see as a play on Ronchamp's formal themes, seems glittering and pretty in comparison, and won universal acceptance immediately, always the acid test. Ronchamp is a modernist icon, but is too strange in form and idea to have been, by now, more than superficially assimilated. It is still new, still ugly.


LW


[![Le Corbusier's Chapel at Ronchamp, France](media/Le_Corbusier's_Chapel_at_Ronchamp,_France.jpg)](https://lebbeuswoods.files.wordpress.com/2008/01/ineff-corb1-copy-a.jpg "Le Corbusier's Chapel at Ronchamp, France")



 ## Comments 
1. Vlad
1.9.08 / 5am


One good example of architecture turned into something truly ugly, was demonstrated at 2005 P.S.1 installation by Hernan Diaz Alonso. As far as Ronchamp, I don't think it can be considered ugly because it is not such a strange building after all. It resembles so many things as Charles Jenks pointed out – it has this enigmatic quality. It is perhaps the first iconic building – so strange, so easily recognizable and yet so familiar thought images seen somewhere before, but not quite – a hat, a boat…. Almost any metaphor will fit in so beautifully.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.9.08 / 7pm


Vlad, to clarify a point: just because the new is ugly, it does not follow that everything ugly is new.
5. David Bowman
1.9.08 / 9pm


Thanks for the thought-provoking post… but I wish you would have taken it further. Maybe you will, in future posts on ethics and aesthetics. But I want to know what you think: where are we now? Pollock and Le Corbusier are long dead, and as you mentioned in another post, we keep rehashing the same key figures over and over in our histories of art and architecture, which pushes the discussion down the same worn-out paths. 


Even so, I can't really conceptualize this idea without thinking about Modernism. The public consensus on Modern (i.e. Modernist) architecture in the early 1900s was that it was ugly. People hated the sight of a building shorn of its overt symbolism; gabled roofs, window shutters, ornamental door frames. Those things helped people make sense of the world. Those details created reassurance and comfort. They implied a connection to the past (and that's why we see them recurring, disingenuously, in subdivisions around the world). Modernists exposed the building for what it was, creating a naked box. The box itself was not revolutionary at all. Many cultures built boxes — sometimes highly ornamented, sometimes not. But now that you've got a naked box… what do you do with it? Well, there's no way to take it further. The Modern box was an attempt at taking off the Emperor's clothes. A way of demystifying something. It was an end, and not a way forward. And, as with many other things, naked doesn't always mean pretty. So we had Postmodernists trying to put the clothes back on. But it was too late, nobody could get that image out of their minds. The naked truth about our buildings had been revealed. We were drawing a boundary around ourselves, screening out nature and fencing in space, and we still can't think of any other way to do it.


I think this is one explanation for the failure of much contemporary architecture to show us anything truly new. It is an attempt to create mystery, rather than demystify, because the events of the last century have been so successful at killing off most of our superstition and mystery about the world. The tortured geometries that you find in some contemporary projects should be taken quite literally as signs of a tortured creator. Only rarely does an architect realize how to solve the same old problem in a new way. This struggle has created some really diverse work that is innovative on an aesthetic level, if nothing else. But it makes sense that the more answers to a question there are, the more poorly-defined the problem is. Many contemporary architects are struggling to find the way forward, to find that hidden door. But what if the door just doesn't exist? What if we've finally left the building, and stepped into the void?


I think that's exactly what's happened. Instead of a big movement (or a few big movements) that people could learn to appreciate, architecture has fractured into a hundred subcultures, and within those subcultures, you have individuals who are basically working as autonomously as artists. That's great in some ways; it's created some amazing buildings (or at least, some amazing renderings). But if this is truly the case, then I'm not sure if “new” and “ugly” will ever be divorced. We're going to be uncertain about the future from now on, because the recent past has proved to be so unreliable. The work of certain architects (and their as-of-yet unknown antecedents) will always look “new” and “ugly” because there will be no primary school of thought that can be relied upon to interpret the work, and therefore, we will not be able to habituate to it on a cultural or even personal level. We'll be forever stuck in this era of “massive change” (nod to Bruce Mau) where nothing sits still long enough to get a good look at it. Is something like OMA's Seattle Public Library really a harbinger of new ideas — a new ugliness that will become accepted in time? What if it's just ugly because it's ugly? Having visited it in the not-too-distant past, I still can't make up my mind.


And there's a flipside to this as well. Some people love projects like the Seattle Public Library precisely because the work looks new. In fact, I would say that OMA has gone to great measures to make sure that everything looks new and different. To fans of OMA, old is automatically ugly and tainted. New looks cool, sexy, and different. Modernism is their grandfather's architecture. These people are desperate for new meanings, new forms, new messiahs who will find that hidden door and show us the way forward. So the battle lines have been drawn, and now we have something that looks like progress. Champions of the new, progenitors of the future, versus the stalwart defenders of nostalgia — or champions of tradition, scale, and urban coherence versus out-of-control globalized placelessness. Whichever way you want to look at it, it's just the illusion of progress. Modernists were also obsessed with the idea of a cultural tabula rasa. The difference between Modernism and today is that Modernism was optimistic and wrong; today, we're nihilistic and (probably) right.


So it seems (to reference another post of yours) that Fukuyama was probably right about the end of history, more right than he could have ever known. Would he have been able to predict the success of the internet, the hybridization of high-culture/low-culture/no-culture that is developing as a result of globalization and advanced communications technology? Is there truly nothing new? Or maybe everything at the end of history is perpetually new, an orgiastic mess of forms and materials and colors, except we get the feeling that we've seen bits and pieces of it all before. Standing in any major city these days, it certainly seems so. It's like we live in a never-ending remix. A miasma of architectural styles, automobiles of every shape and color, multi-lingual signage, people from all corners of the world, conflicting consumer messages blinking on enormous digital screens. We've conquered distance and time with our technology. Sunset is no longer the end of the day. Going home from work doesn't mean you're finished working. Getting married doesn't mean that it's forever. Whatever it is that we've all created, it's terribly efficient at deposing the old gods. The only meaning you can glean from this scenario is that there is no inherent meaning. But maybe this is a good thing. Maybe the the end of history will finally free architecture, and other similarly confused disciplines, from the burden of responding to (or creating) The Next Big Thing. As someone who is relatively new to the field, I think it's an exciting time, a time of immense freedom.
7. Godofredo
1.10.08 / 2pm


Let us try a different direction of tought:  

New or ugly are allways exterior interpretations of any creative event. Both refer to a territory of signification where something will be judged as “new” or “ugly”. Therefore these are qualifications of a creative event based on its exterior and not on its interior dynamics. Of course its exterior feeds its interior and so forth in a circuit of reciprocal causality..  

Nonetheless, they are representions and as such tell us more of the way we see creation than of creation itself. This “relativity” is not a post-modern issue per se, but one might say that it is due to the territorial superimpositions characteristic of a globalized society that such contradictions become more apparent.  

Also, the end of history is not the end of the past, but the result of understanding that we can only look at the past from the point of view of the present. History is therefore a representational construction, the result of a specific cartography and not an absolut given. Same thing for ugly and new.


Thinking in these terms prevents us from addressing the true problems at stake:


Should architecture be considered as pure creation (a singular creation of new territories of signification) or should architecture be concerned with the public/audience to which it will adress itself to?  

Is architecture meant to serve human beings or humanity as a whole?


I suppose the answer is both.  

Both symbolic and functional, architecture crosses the divide between creation and pragmatism. And each building will inevitable tell a different story.
9. [lebbeuswoods](http://www.lebbeuswoods.net)
1.10.08 / 3pm


David: your comment takes off in provocative directions. I won't respond point for point, because I largely agree with what you've said. I particularly agree with your thought about the ways modernism demystified architecture, leaving little to be said about architectural form. There is really nothing new to say about form for its own sake. And, yes, we should stop waiting for some profound new revelation about form–it's not going to happen, because it is no longer the major issue in architecture.


It seems to me that new forms are necessary today only to enable new ways of thinking and of living. In other words, we don't need a new form of the same old museum typology, but only if we have an entirely new idea of ‘museum.' The same goes for other building types. It is possible to refine—sometimes dramatically—well-known forms, as Thom Mayne has done in the San Francisco Federal Office Building, or, as Steven Holl has done in the Nelson-Atkins Museum. But new forms can only emerge from new conditions. Often these conditions are ‘ugly'—varied forms of violent or difficult change affecting individuals or entire social groups. 


The Next Big Thing in architecture is already underway, and that is how design can deal with the un-designed, such as squatter settlements, or the rebuilding of cities damaged by war and natural disasters. I have been working on these conditions since the early 90s. I'm eagerly looking forward to Koolhaas' book “Lagos: How it Works,” which will be out in March and to see if it addresses the future transformation of the vast Lagos slums.


I like your skepticism and share it. When it comes to the freedom that you mention in an upbeat way at the end of your comment, I will only caution that it exists only for those who have suffered loss—the more loss, the more freedom. It's not a happy state, but one, to use Sartre's term, to which an increasing number of us are ‘condemned.'
11. [lebbeuswoods](http://www.lebbeuswoods.net)
1.10.08 / 3pm


Godofredo: Good to hear from you. I know you dislike simple terms like ‘new' and ‘ugly,' and they do need a lot of interpretation to have much meaning. Still, they refer to the aesthetic aspect of architecture—both the way a thing looks and what it does—which has profound implications for both the human and for humanity. It is my contention that the aesthetic has been detached from the ethical for the political convenience of an elite who controls a large share of the world's resources, human and material. The aesthetic has been reduced to matters of style and form, deceptively so. Until we architects reconnect aesthetical issues with ethical ones, architecture will remain an empty game of form-making for its own sake.
13. Godofredo
1.10.08 / 7pm


I fully agree and like to hear you criticizing the detachment of ethics and aesthetics. But for the same reasons I would rather have architects speaking in terms other than beautiful or ugly.  

Not only such terms have too many connotations, but also they propose an understanding of architecture as something frozen in time, something that Is.  

Wouldn't it be more interesting to speak of architecture as something in permanent transformation, as something that becomes?  

I have always seen it as part of a movement and for that reason have always attempted an aesthetics of impacts, intensities, aggressions, aggregations, expressions etc.  

I believe that if architecture attempts to produce or express something it is about that something that architects should speak and not about what the building might be.
15. Godofredo
1.10.08 / 8pm


Other question: how do you prevent the aesthetics of the “ugly” from simply ignoring the conditions that produced such ugliness?  

If Lagos “may well be the most radical urbanism extant today”Koolhaas (I don't fully disagree) is the neo-liberalism that produced it the most radical architect ever?  

One can't forget Mike Davis when he says “praising the praxis of the poor became a smokescreen for reneging upon historic state commitments to relieve poverty and homelessness”.  

How do we do it then?
17. [lebbeuswoods](http://www.lebbeuswoods.net)
1.10.08 / 9pm


I agree with Mike Davis, fully. 


On your other point, any thing is, paradoxically, always two different things at the same time: what it is and what it is becoming. If this were not so (according to Zeno, in his famous Paradox)) the arrow would never move. Anyway, it is philosophically legitimate to name a thing, even though–at the very instant of naming–it is no longer the same thing.


But let's not split hairs over this. It seems we are in basic agreement. The real question is: how does the paradox of being/becoming impact our conception and design of architecture?
19. Hamish Buchanan
4.27.08 / 8am


Interesting how you oppose ugly with pretty (a dismissively feminine term), rather than beautiful. Also, how you resort to Greenberg, whose status has been seriously re-evaluated over the past two or three decades, and Pollock, who died in 1955. In the visual arts, there was a serious critical re-engagement with the question of beauty beginning back in the mid-1990s. Interesting that the architectural world remains so oblivious to it.
21. [lebbeuswoods](http://www.lebbeuswoods.net)
4.27.08 / 5pm


Hamish Buchanan: Would you please enlighten us with some reading references to the “critical re-engagement with the question of beauty” that you refer to?
