
---
title: THE PROTO-URBAN CONDITION
date: 2008-02-29 00:00:00
---

# THE PROTO-URBAN CONDITION


[![The given, abstract/analogous site](media/The_given,_abstract!analogous_site.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/lwblog-pc-given1.jpg "The given, abstract/analogous site")


[![](media/lwblog-protocond1a3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-protocond1a3.jpg)[![](media/lwblog-protocond2a2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-protocond2a2.jpg)[![](media/lwblog-protocond3a.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-protocond3a.jpg)


 


 


With cities on the rise—many existing ones expanding rapidly, and others, once mere towns, growing into full-fledged urban centers—the need to understand the urban condition becomes ever more urgent. While we would hope that local culture would be a major factor in the design and planning of particular cities in quite different parts of the world, we have to acknowledge that there are more universal factors operating globally today, having to do with international economics and development. Architects are being asked to design buildings far from their home cities and cultures and they inevitably transplant what they already know. What concerns me here is, what do they already know? The case for studying proto-urban conditions—-conditions shared by cities anywhere—-is made in the hope that what architects already know can be deepened to the level of principle. If such a deepening could occur, local variations on prototypical conditions would become more possible, and the present era of typological impositions of one culture upon others would gradually be brought to a close.


The rectilinear street grid, creating straight, continuous spaces lined with building walls, is a proto-urban condition explored by fourth year students at the Cooper Union School of Architecture, under the direction of a team of four faculty, in the Spring semester of 2007. Rather than assign a site in the grids of, say, Manhattan, Barcelona, or Beijing, any of which would be heavily loaded with historical and cultural factors requiring extensive analysis, the given site was an abstracted version of a street space resulting from a rectilinear grid. This allowed us to immediately begin to develop new strategies for inserting programs of use that could inform not only this analogous site, but Manhattan, Barcelona, Beijing, and other cities, as well.


The commentary by Professor Kevin Bone sums up the procedure of the studio: “The studio set out to investigate the ideal of multiple architectural works woven together across an urban landscape and to look at the community of projects from various perspectives. Works were expected to operate in fields of non-specific boundaries. Where the limits of one individual architectural proposal began and another ended was not absolutely defined, allowing for new combinations of urban architectural interactions. To accomplish this, students worked in various groups on various stages of the investigations. Initially students were asked to pair up and develop architectural propositions that communicated with each other. This architectural conversation could be from one side of the wall to the other, through the wall or at discrete positions along the wall, as long as the projects somehow addressed other proposals. As the studio evolved these works (in some cases) fused together into a single hybrid proposition. The pair ups then assembled in larger groups containing several pairs of students. These larger groups looked at all of the proposals of the class and attempted to place them in areas of operation within the site. These groups each produced a kind of spatial/zoning diagram, an architectural master plan for the work of the studio. The best ideas from each of these four “planning” groups were incorporated into a general organizing structure. All students worked on a single idealized urban site and all projects existed simultaneously with each other. The assembly of works became an autonomous architectural work, with accidents and non-intentional spaces provoking further architectural response. The proto-urban problem began to build its own context and its own history. The class and studio began to build its own community and negotiate its own rules for various architectural actions.”


As can be seen in the master model, which was developed at eighth-inch scale, the rigor of the methodology combined with the freedom of the students to design for programs of their choosing resulted in a varied, complexly interwoven, highly articulated architectural landscape. Densely layered spatially and programmatically, it demonstrates a principle often considered paradoxical, that—given carefully considered circumstances—the interests of a community and its creative individuals can reinforce one another. The making of architecture is the key.


LW


[![students and faculty review the master model](media/students_and_faculty_review_the_master_model.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/lwblog-pureview1.jpg "students and faculty review the master model")


[The PROTO-URBAN CONDITION–Faculty and Students](https://lebbeuswoods.files.wordpress.com/2008/02/arch141s07-classlist.pdf "The PROTO-URBAN CONDITION–Faculty and Students")



 ## Comments 
1. [nina b](http://ninamariebarbuto.blogspot.com/)
3.1.08 / 3am


The proto-urban condition is system design. The rule set for the city machine, this project is ideally is what web 2.0 accomplishes as to date. Though it is not site specific, it allows for complete specificity through implication of the in habitants. DIY town. Recently we had the undergraduate thesis students presented for mid terms. One of the projects, by Adam Bandler is a juxtaposition of Baghdad and Los Angeles, focusing and criticizing insurgency. He combined the cities and made a new one, one with gentrification as imposed system and the other with the occupancy of the green zone. His goal is to design the device for the space defining factors to be implicated by typically destructive means. I know I am continuing on about this but during his review he was critized for making something “not real.” Your blog entry Lebbeus speaks and supports his thesis. I wish you were here for the reviews. There is a new geopolitical landscape that has a consciousness dying to be explored, the “modernity at large”. Having a studio focusing on the proto-urban brings a platform for this investigation. Thank you. I wish I could have participated also.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
3.1.08 / 7pm


nina b: when you say system design—given your examples—I believe you are referring to the ‘soft science' approach to creative analysis, which was developed in cybernetics, systems theory, and cognitive science beginning in the 50s. More or less overlooked in recent years, it is an approach that counts on analog models, rather than virtual ones. Rumor has it that the analog may be coming back, now that we are seeing the limitations of a purely digital/virtual approach.
5. [nina b](http://ninamariebarbuto.blogspot.com/)
3.3.08 / 10am


I concur. Analog (and one could even say nature) is making a come back. We have all these tools, scripts, and robots. Now its time to start implementing them against living bodies, not second life.
7. [nina b](http://ninamariebarbuto.blogspot.com/)
3.3.08 / 10am


<http://ninamariebarbuto.blogspot.com/>


<http://inhabitableorganism.blogspot.com/>


When ever you have time, here are two blogs that I have been working on that deal with those idea. Please check them out.  

Thank you!
9. spencer
3.4.08 / 5pm


For clarification, is Web 2.0 considered an analog technology?  

If so, I would assume most people would not immediatily recognize it as such.  

My friends and I discuss technology often. Recently we were talking about WiFi and what it would look like to the naked eye if it could be seen. We imagined constant streaming bubles surrounding a WiFi hub with occasional bursts or pulses of strong use. This brings up the notion of technological transition from cable and wire to broadcast and wireless. This is a clear change in form of data transmission (travel).  

It seems the derivative of the city grid is movement and how a traveler/information gets from point A to point B. With current technology (foot, auto, phone, email) that is a straight line.  

Lebbeus, considering the thread of most recent topics you've posted it I wonder if you are looking for that next change in city form. What were the results of this studio? What constants were uncovered? What glimmers of change were appearant? Did any student approach the project from the point of view of the slum? How did politics (governement and mob rule) enter and inform the project?
