
---
title: SLUMS  One idea
date: 2008-02-09 00:00:00
---

# SLUMS: One idea


[![Morphing capsule](media/Morphing_capsule.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/capsule2abloglr.jpg "Morphing capsule")[![Kit of parts capsule](media/Kit_of_parts_capsule.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/capsule1abloglr.jpg "Kit of parts capsule")  

[![Argentine slum, with first effects of inserted capsules](media/Argentine_slum,_with_first_effects_of_inserted_capsules.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/slumrecon1blog.jpg "Argentine slum, with first effects of inserted capsules")


Architects, as I said in a recent post, are idea people. We have concepts and make designs that embody or implement them. We present them as clearly and openly as possible, and can only hope that others will find them useful to their ends, and build them. When it comes to the reformation of slums, we are a long way from having designs and are very much at the beginning of the having ideas stage. It is in that spirit that I make this post. I count on the tough criticism of colleagues.


If we brainstorm a little, we can imagine, as one solution, a capsule–a material package–that can be inserted into a slum and immediately, with its use by a person living there, begins to transform that person's living conditions. What it does first of all is establish a demarcated space that is secure from without, clean and radiant with natural and artificial light within. The sheltered space provides distinct areas for sleeping, gathering, hygiene and sanitation. It provides space for cooking, with adequate ventilation, and for work—whatever type of work the person and his family or friends do for their own benefit, economic or otherwise. The capsule is capable of configuring itself, or being configured, according to different patterns of work and living. It does not produce a module, or standardized unit. If we think of it more like a nutrient, rather than a product, we understand that the results it creates are adapted in unique ways to particular persons or families, because it nurtures whatever is strong in a given situation, rather than imposing a uniform result based on a pre-conceived judgment of what is best. In this sense (letting our imaginations play freely), we can conceive of the capsule as something that organically enhances the living and working conditions of people who use it, beginning as something standardized—like a pill we take when sick—that morphs into a restorative source of energy, metabolizing into material form, enabling health, enhancing the capabilities of those who use it, whatever they may be.


If such a capsule could be designed by leading thinkers, engineers, architects; if it could be produced by the best technicians using advanced technological facilities; if it could be financed by institutions in the private and public sectors with vision of the vast benefits for everyone it would create; then it would seem that such a project could actually be accomplished. If a large number of these capsules were to be inserted into a slum, the result over time would be growth of a healthier living environment, from within. Because the transformation would be incremental, both in time and scale, it is conceivable that an organic form of community could literally grow from the exchange and cooperation of people now inhabiting slums as they reform their living environments.


It is easy to imagine objections to such a ‘magic bullet,' as the cure for syphilis was called in the 19th century. From the side of the slum dwellers, it might seem an unwelcome intrusion from outside, just another quick fix imposed by the economically advantaged on the desperately poor, serving the interests of the rich by transforming the slum according to their well-intentioned but—to the slum dweller–necessarily opposed values. It is especially important, then, that the transformative capsule enables the slum-dwellers to achieve their goals, serving their values, and does not reduce them to subjects of its designers' and makers' will. Inevitably, the values, prejudices, perspectives and aspirations of the designers and makers will be imbedded in the capsule and what it does. Therefore the slum-dwellers should, in the first place, have the right of refusal. Also, they must have the right to modify the capsule and its effects as they see fit. It cannot be a locked system, capable of producing only a predetermined outcome. The implication of these freedoms is that the capsule, whatever its capabilities, could be used to work against the intentions of its designers and makers. Because the effects of the capsule would be powerfully transformative, its possession would involve risk for all the groups, and individuals, involved. Because the sponsoring institutions in the economically enabled sector have the most to lose, it is more likely that resistance to the creation of such a capsule would come not from slum dwellers, but from these sectors. For those who believe in such a project, it is imperative that leaders in the economically powerful sectors be convinced that creation of the capsule is a risk worth taking.


Even without institutional support and sanction, the conceptualization and design of the capsule can and should begin immediately. There is nothing to lose by moving in this direction. And moving independently is probably the only effective way to proceed, because issues of politics and economics, from institutional viewpoints, will slow down or frustrate the process of having ideas and developing them, as it often does in much less complex projects. What is needed now is free thinking by the most creative minds, in order to innovate on the level required by the ‘unsolvable' problem of transforming slums on a local, let alone global, scale. Because the problem is urgently in need of solution, the effort should be thought of in the same way as that to find a cure—or at least a treatment—for a virulent disease that is already affecting millions and spreading rapidly.


There is really no time to waste.


LW



 ## Comments 
1. [mark](http://greenovision.com)
2.10.08 / 2pm


This way of looking at small rather than large scale change occuring over time(but lets get started now!) is important. So often with slum removal and renovation we look to the political realm for solutions. As You stated “from institutional viewpoints, will slow down or frustrate the process of having ideas and developing them” the clock keeps ticking and nothing gets done. I believe in grassroots solutions because in smallness there is evidence of transformation…small and in palettable increments. I have noticed that when a new building is built at first some may not like it, some might not even see it, and others will love it, but after time it becomes part of a neighborhood, people get used to it. It is easier for most people to adapt to small changes rather than colosal ones. In this subtle rearrangement of “place” over time inhabitants will feel less threatened, more likely to feel less institutionalized, and in time these “capsuls” become everyday or normal. So often large scale projects have little forewarning, they often are overly engineered, and often do not adapt well to local ways, neighborhoods, and in effect alienate people and place.  

The capsule method could break down expense to a achievable scale where contributers could actually see something happening, changing, and one capsule at a time.  

Prehapes the biggest battle that this method faces is the possible sprawlingness of single capsuls. I notice in the Argentine photo the vast sprawlingness of the slum. This is an issue. Humans are using up gound, and environments in this spread out fashion… like the suburban sprawl that has consumed so much of our planet. I would advocate an approach that clusters and overlaps such capsuls, and shares spaces and emenities.  

The american consumer home where every household contains every appliance to be solely used by only those that live there has proved to be a waste of resources. I suggest “sharing” capsuls. I am not suggesting breaking down privacy competely but where possible to save on resources.
3. Josh V
2.11.08 / 2pm


mark, your point on how small changes are more easily accepted than large ones is good. These things may seem like alien invasions to their territory, but at such a small scale, they would likely not be so afraid of them, nor angry towards them. These spaces could be (and most definitely should be) very adaptable by these people. They may seem strange at first, but inviting to change.


The point of overlapping them I think may congest too much in one location. The sprawlingness of the slum means these objects should be few and far between, while evenly spread throughout. I think invading them in space that is already occupied by housing may aggravate some (and quite possibly use these spaces for living rather than the intended public service). Maybe if they were projected above the slums, or the river, in space that is not yet occupied, less toes would be stepped on. And the separation from the space that is so obviously allocated for living may accent that it is not intended for living, but something else.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.11.08 / 5pm


mark: I think the clustering idea is very good, in the sense that there do indeed need to be shared spaces, both for practical reasons and community building. I'm not sure how to approach the ‘sprawl' problem you rightly mention, without thinning-out (displacing) the dense population, or ‘going vertical,' neither of which seem like good approaches. We should work on that…..
7. Keith
2.14.08 / 6pm


I too agree with the idea of having, “common,” areas. This helps with the obvious problems such as costs and resources, and it also helps people to interact. The one problem I see with the common spaces is that so many times these areas loose the, “warmth,” that one may find in their own personal kitchen, bath, etc.  

Take for instance the relocation of millions of people in China for the Three Gorges Dam project. In this instance, entire villages were uprooted and moved to a new location. Then, to save in cost and resources, living units were built with shared common areas such as kitchens and bathing areas. To the financially well of peoples, this was a good idea. Yes, they are making these people move from their family homes, but they would provide many modern conveniences such as electricity and running water for a people that never had it before.  

This looks good and well intended on paper. In reality, once occupied, these were the coldest and dreariest looking places that the inhabitants hated. They had actually lost the sense of community they once had and many wished they could go back to how it was.  

Another issue, one that has been pointed out earlier in this discussion, is on alowing the inhabitants to take control of how these capsules evolve. This is a great idea. Referring back to the Three Gorges Dam project, all the new housing looked exactly alike. There was no difference between one house to the next. We as architects should know that it is the diversity of many building types that helps create a place. If everything looks alike, then the personal identity of a village, town, city, state, country is lost. If what is built in Argentina looks just like what is built in Colombia or China, then what gives each area its sense of identity?  

So, we must come up with a way to adress these issues. We must try to come up with solutions that do not strip away what makes each individual community a community. These areas may be a slum to some, but it is home to others.
9. [mark](http://greenovision.com)
2.15.08 / 2pm


Keith I so agree with you about the individual and how they make a dwelling “theirs”. It was Martin Heidegger who said in order to dwell one must be engaged with the making of place. It is this personalization of spaces that gives color/identity to neighborhoods. It seems so often that architects and designers often impose their own aesthetic, as if they know better. What we must keep in mind is we are not living the slum dwellers life. How can we really know how their everyday life is either uplifted or flattened by our impositions? “Taste culture” I believe it has been called before.  

I believe this is the starting point to designing the capsule. I suggest the capsule is to provide the essential human needs. Yet, the capsule should be free to be individualized wherever possible. Individualization could occur with the skin of the capsule, or spaces being designed to be adaptable to dwellers needs allowing it to be changed/modified.  

Exteriority of the capsule has much to do with feeling like home, ie.cladding is often a locally available resource, It has context to place. I see the capsule as providing the necessary stability and healthy environment but not to determine the ultimate aesthetic of a place (often such endeavors equate to simulacrum, a fakeness we can detect). The capsule would be the medium for local color and individualization. The bones of these dwellings should still offer uniqueness much like the bones in the human face, behind the skin the skeletal structure has expression, it determines much about how the skin will look once stretched over the frame.
11. christian
2.16.08 / 7pm


While reading the comments I recognized that there is an certain view on- or opinion about- what such a slum is. That opinion seems not fully right or maybe goes not far enough. In opposite I think that the inhabitants of a slum (a generalization I do not find appropriate but needed here) are NOT the helpless poor lonely people. I agree that they have to suffer a lot of disadvantages and often violence but thats only one side of the medal. Let me explain: Who build the dwellings, the basic infrastructure and the social community in a slum. The people who era living there. Do they by nature are less educated as people living not in slums. No. There are recently created slums in the Middle East or in Africa where refugees are piled up and excluded from the rest of the society. There are Gyspy slums in the East European Countries where they actually took a cultural step back and went out of the concrete slab buildings. 


By building a slum those people take an effort that is a huge civil achievement. They take over land and create a space. Space that they need and that is created by the power and ability of those humans in their lifetime. This space has a very specific scale. And I am tending to say that there is a relation between the education and the size of the space that they are creating. I know for sure that there is a Palestinian Refugee camp/Slum/Ghetto that has up to three stories high build structures. So my question here is : What can be improved with giving the slum inhabitants more ways to change their dwelling or to give it individual expression? The cultural value of expression through architecture comes long time after the division of working task in a community. In my opinion slums are not on that cultural level – or maybe a slum is in fact a – parallel to our social logic – existing environment. The russian village is an interesting example of such. It until today is a unity in itself with a strong affection to the zar. But even industrialization, communism and so on did not destroyed that basic parameter. The development of ownership in production tools and private property did in fact almost not happen. There are relations to slums that are noticeable. 


Back to the basic features a city has to provide that humans can live there. Clean water, medical supplies,garbage collection and so on. There is no doubt that the simplest imaginable way of providing those things would improve the quality of life by a substantial amount. But I don't understand the logic of providing for free what  

got developed by learning and doing wrong in cities over the centuries. Is that the act of humanity that is appropriate? ‘As was mentioned before those intrusions are in fact an almost occupying act of the CITY next to it. Who else would provide that. AND who would actually do the particular job ?? People get told or decide to do so. Those people are(I am guessing) mostly members of the middle class . The “Buerger” who were strong enough to create institutions like a majors house. Give me an example of a similar important institution that is created today? 


The changes or improvements are aiming not only at the build structures itself, but foremost they aim at the slum inhabitant. I think that is wrong. That capsule can't have the same stereotype of what is right or wrong that we have. The rich, old, self standing, dressed up woman that I see on the street in cabs and museums is the opposite type of being human to the slum guys. So where we are heading ? To make my point clear. Those humans are tougher than all of us and please do not treat them like little SimCity Monkeys that get a garden planted to become happier. The deserve something way better than that. Thank you for reading the text.
13. christian
2.17.08 / 9pm


This movie just won the best movie award at the Berlinale  

Filmfestival in Germany. Watch the trailer about brazilian  

slums. (Personally it is to armystyle for me)
15. Zach V
2.19.08 / 3pm


This all sounds incredibly dangerous. Slums are not problems for architecture. They're problems of city planning. Dealing with the effects of concentrated poverty requires spreading people out, for instance, by requiring all neighborhoods to have low-income housing. It requires stopping the spread of sprawl and battling gentrification. You're suggesting that the poor be isolated even more. I understand the need to provide people with safe spaces–and it's no coincidence that areas of high poverty have few parks and many sewage treatment facilities–but if we're putting resources into the problem, we might as well deal with the larger issue instead of continuing the status quo for poverty: out of sight, out of mind.
17. [spencer](http://www.eworks.org)
2.19.08 / 5pm


Zach, It's true that architecture isn't the only solution but is part of the solution. You are right that good social policy, be it in the form of city governemment, NGOs, non-profits, etc. are the main deliveriers of help to the needy. LW's concept is a good start to this dialoge. Spaces are needed to deliver needed services to people living in slums. His idea is not a “silver bullet” but a component that allows for a larger system to work. It is similar to the Housing First program that many cities in the USA are attempting to use to reduce and help thousands of homeless people. In essence these are buildings (architecture) inserted into the community that provide neccessary services and a perminate place for people to live. In this case people are relocated, but they are moved to a place that gives them, treatement, training and nourishment they need to survive. 


If we took a close look at the slums, encampements and treatment of the homeless throughout our world we would find that all countries are in violation of many human rights. I would love to see LW's concept put into action because we will only know if it works if we see it in use.
19. christian
2.19.08 / 8pm


Hey ZACH, I don't understand what you are saying. “Dealing with the effects of concentrated poverty requires spreading people out, for instance, by requiring all neighborhoods to have low-income housing. It requires stopping the spread of sprawl and battling gentrification. You're suggesting that the poor be isolated even more.” – Of course understand your words and your content, but what do you mean by that? You are suggesting a dissolving process of the slum into the structure of the adjacent city. So you transfer community people to apartment people. The example of Germany and Austria for instance shows where this process leads to. There is no single city with a slum. And it is good that almost all people can have a decent life here. But there is also a weak family structure in those countries and also the care of the government for the people is an extensive part of the tasks that needs to be done continuously. So all good? A very different community that I experienced where the Brooklyn area where mostly orthodox jews life. They have their own shops, doctors, newspapers and so on. They provided a relatively good life for them. That for me is one of the most interesting community formations that I have seen. Very strong.  

Back to Germany – Berlin is the largest Turkish city outside of Turkey. A community that has itself encapsulated and works also by it's own rules. They have a lot social housings in those particular parts of the city. There is a invisible border that creates a huge economical imbalance. But for a good part of time they were the cheap labor for the germans. And they still are in a weak economical and educational position. 


Slum people are not homeless people. But their community is shaped by the strong forces that are put on them from the outside. They found an answer to the fact of being unwanted. They defend them self from intrusions and their crimes is the exploitation of the city next to it. In a way the slum is the second half of the medal. Isn't it weird that in Lagos or Sao Paulo there are areas that are almost the same like in European cities. High class areas. So called. Like a copy. And they in fact are. Is there a slum in Havana? Maybe? I guess not, because the government would not allow that and the moment of growth of Havanna is not now, there are not much people relocating there. The slums in a way pay for the adjacent city to be western. The cheap labour that is needed to become wealthy is directly around the corner. Singapore is super rich but right next to it on the other side of the border cheap workers do there job. So what do you think would be the right way to approach this dual relationship. I think one approach could be to make the slum an independent city by itself. Majors house or what so ever, a name and an understanding of the border. The violence and the drugs are the biggest problems for such a process. But the are everywhere a problem. 


If one looks at poor small cities in arabia, then it almost looks like they are like slums. I just got that thought that maybe the slum is only there because of the western opponent. Thank you for reading.
21. [spencer](http://www.eworks.org)
2.19.08 / 9pm


Christian, I think Zach is suggesting to mix the population, which in some cases may be a good idea. The important point to keep in mind here is the one that you make: People and situations are different and need different solutions. Lebbeus is recognizing this by creating open spaces where uses can be filled by the direct needs of those people decided in part by those people. In Seattle, we have a very different situation that is being handles in a similar manner (as I stated above).  

So, I am not sure a Slum has been defined specifically by Lebbeus' project. It might be best not to define it too rigidly so that a broader discussion can be made and we can each have our own situation tested and we can learn from each other's results.  

Can you also tell us (non-spanish speaking) more about the movie?
23. christian
2.20.08 / 9pm


Hallo spencer, first i absolutely understood what zach is aiming at. But I am trying to ask if there isn't a different way out of the situation. At the end it is all about the question of freedom. Most of the time freedom for the generations afterwards. My personal freedom here in europe was achieved by my grandfathers. Or maybe the grand-grandfathers. But however it is this freedom is a hard thing to achieve. Or sometimes you get harmed for fighting for it. See the killed Jounalist Anna Politkovskaya in Russia who did that. She tried to help people who get scrued by government, the judges and the military. They lost there sons and fathers and brothers. To a great extend I life in a dual self recognition of someone who is fully aware of the tragedies of man that happen everyday and also of the impossibility of helping them. (one side thought .. there is this anonymus artist basky who got so well known that galleries want to buy his work. He refuses and instead posts highres images on his website for everyone to print by himself – could that be a capsule ? A value in money and in culture that could be a present for a slum ?) You see I am thinking. But I disagree with your saying “t might be best not to define it too rigidly so that a broader discussion can be made…” I absolutely think that a straight clear and as rigid as possible definition is needed. Not for what a slum is, but what precisely your thinking is. And so far I am most skeptical about broader discussions. Thank you for reading.  

PS: <http://german.imdb.com/title/tt0861739/plotsummary>
25. Spencer
2.21.08 / 3am


Christian,  

I think we are saying the similar things. It is a long process out of poverty (to freedom?). Stability may take generations to attain and it's not impossible. It takes commitment from everyone.  

Could you clarify a few things you said because I am assuming english is not your first language and some of what you said is confusing.


“But I am trying to ask if there isn't a different way out of the situation.” What Zach said is one of many ways some people can get out of poverty. I was confused by your earlier comment because you seemed to disagree with him. Do you now agree with him?  

In my previous post I commended Lebbeus on his idea because it creates spaces which can be used to best serve what these people need. His solution stays flexible to allow for different activities and programs (solutions) to happen.


” To a great extend I life (live?) in a dual self recognition of (1) someone who is fully aware of the tragedies of man that happen everyday and also of (2) the impossibility of helping them.”  

Are you saying you feel it is impossible to help people out of poverty? If you do feel it is impossible to help them, why is do you feel this way?


The artist situation you refered to, is this a way to raise money for people living in poverty? If so, what would you do with that money? How does money help them? I am not disputing that money will help, becuase it will, I am curious as to how you feel it can.


In my experience working with people in poverty rigid thinking leads to a singular solution that not everyone can fit into. Each person living in poverty has a different situation (culture, history, etc.). Each city has a different situation in regards to people living in poverty (slums, encampments, people living on the streets, etc). Each governernment will help out in different ways (socialist, democracy, facist, etc.). No two are the same but many are similar. Which means no one solution may work for each situation but a flexable process to a solution can be reformed to fit each situation. This is why I am in part excited by Lebeuss' idea and feel that it is a good approach.  

Like I said before I know of another approach being used in USA cities that is similar to Lebbeus'. But, because Slums do not occure as much and to the same size as Non-USA cities the approach is somewhat different. Instead of inserting space into a slum, buildings are constructed closer to the services that the people living in them will need. Other services that are needed but not closely available will be provided inside these new buildings as well. In Seattle we also have communities where people live in tents. They are more nomadic and move from place to place every 3 months. Even though they live in the same city as the other people living in poverty their situation is different enough that they need a different solution. It might be similar to the first or it may need to be vastly different.


It could be that you are saying the same thing I am.  

” I absolutely think that a straight clear and as rigid as possible definition is needed. Not for what a slum is, but what precisely your thinking is”  

I, too, am not saying that a slum needs to be defined. Where we may differ is in defining the solution. I think you are saying that a clear solution must be defined. If that is true then we do differ in how we think about solutions.
27. Zach V
2.22.08 / 3am


Christian and Spencer, my post probably wasn't clear enough–of course there are many ways to attack the problem, I'm no city planner, and I think that immediate change in slums like LW suggests could be great. But it also could be a way of continuing the status quo. There is no silver bullet as you say, and cities in Europe are not the same as in America. But it's pretty clear that the projects in America are a failure, yet we keep building them! People do not want to deal with poverty/minorities, so they separate themselves from it physically, which in turn gives them no reason to deal with it, and so on…. 


We definitely need to brainstorm different ideas, but the most important thing is to actually talk to people who live in slums and not design their lives for them. Is is often hard to separate politics from achitecture, but I think there is a utopian strain in a lot of architectural theory that's something like: “Wouldn't all our problems be solved if…” Unfortunately, thinking like this (communist or free-trade) has had disastrous consequences when broadly applied without dialog with actual people. There is no panacea, we need to treat each situation uniquely. Site specificity, people!


(Nonetheless, we still need inventive ideas to deal with this singular problems! Invent, experiment, invent, experiment!)
29. elan
2.23.08 / 9pm


Spencer, Christian, Zach:  

Concerning the questions you ask to one another: about mixing the low-income or slum people seems to me to be a question parallel whether to renovate or reconstruct. So, I ask “do you think slum people have their own culture that has any value for us?” And the point that they still DID build the little they have… they must certainly have their own stories, own myths…) I just thought of Werewere Liking, a writer, actress, builder of arts and creative communities, called KiYi Village in Ivory Coast (1980s-present). I dont know much about the context, but it is a type of social cultural creative power capsule. An internal mythical poetry process kind of Art that creates and remembers.  

I would be cautious about this ´up by the bootstraps notion´ (especially if we provide the boots). In my view, ‘Getting out of poverty' is a modern fallacy invented by charity groups to prevent the need to ‘get poverty itself out'. Just give it a nudge and move it around! People have to deal with the reality that poverty is constructed and sustained by our cities and industries. I think Christian´s observation about this double is right on, in that the society into which to mix poor people still needs its cheap labor (which is almost always outside of the market center) until everything is run by solar and wind power and we start farming. Division is the global policy of the day, take Manhattan as an example, the city is selling the public housing to corporations to sell to rich people as renovated duplexes- this idea of mixing just doesnt seem to be happening.  

So my constructive thoughts: On the flip side to (what i consider) the positive notion of having an internally motivated and calibrated capsule, I am proposing a second internal approach, one that wonders how we can generate an urge for ´us rich ones´to care about these places and to actually go there? Dont they have anything positive? Would it be useful to have the desire to become embedded within their tissue? What would it mean for us to make that ´pulled down by the bootstraps´ leap? Just to be there for a purely human reason. (too dangerous too scary?)  

Could we build through the UN or UNESCO, a network of Free Spaces For The Integration of Those Who Seek Political Asylum Throughout The World- perhaps coupled with housing for poets artists? An epicenter of western-eastern (upper) culture that has defected by its own will and in search of a new home, ironically found in the midst of a slum? If that were to happen I imagine there would be an internal mutual reaction and I think a creative one that has to do with a pan-global existential reality. People who seek asylum always want to go to the good places, but is there a way to use that as a catalyst for these slum areas? I suppose this idea is a type of urban planning that considers taking the notion of a civic program but at the scale of a row of shacks in order to provoke a multicultural dialogue about place and being alien.
31. [Brook](http://brookmeier.com/blog/)
3.4.08 / 2pm


I live in Kolkata and daily drive through a slum on my way to the office (and I just bought a fish from the Aquarium outlet in the slum). Slums here are not hidden from sight (like in Delhi) and exist adjacent to other neighborhoods regardless of income level. 


In talking to some people here about the slums, one concern seemed evident. If you make the slums better there are two possible outcomes: they will attract more people or they will rent out their space to make money and create a new, lower class slum. 


One thing that seems important before or in tandem with any changes to the current slums, is a change to the villages. Which in India seem to be slums with no opportunity. If people want to create a life for their family, they have to move to the City (often leaving the family behind). For each person who leaves the slum and can somehow move up the ladder, 10 people are waiting to take their spot. Maybe capsules should be in the Village as well.
33. [lebbeuswoods](http://www.lebbeuswoods.net)
3.4.08 / 4pm


Brook: You are certainly correct in saying that rural poverty feeds the urban slums. It's the same in Nigeria, Brasil, all around the Mediterranean, everywhere. The rural question is a tougher one than slums themselves, because it is even more pervasive and less concentrated. It has to be dealt with, as you say, in tandem with the urban settlements.
35. Vick
4.12.08 / 12am


I've always believed in architects changing the common perceptions and notions of slums. In truth, architects have in their power, unbelievable power to change the scape of the world, but only if they dared to break out of the confined perceptions of the world. 


The problems associated with slums are actually much more deeprooted than what the others think. It is a deep problem, even to the extent of psychological factors. Not an easy task to solve it, but its possible.


I've always believed in improving living conditions. If you have done that in a low cost housing or slums, chances are you have solved a step to the problem. Talk to the people and mingle in. Approach them and live with them, and then you will understand the true gravity of the situation. Go live in a slum.
37. Camilo
4.14.08 / 2pm


It is a very interesting discussion going here. In my view, in Lebbeus second post, where the idea of empowering the people, the slum dwellers, was mentioned at the end of the essay, is where I really think possible (innovative) solutions lead by architects are, -if architecture is to be understood as a transdiciplinary practice, making use of tools that go beyond those given to us by the knowledge of a particular (tectonic) set of technical tools- can really be found.


External , top-down solutions are systematically tried, and partially solve some of the problems, (roads get paved, property titles issued, sewage and electricity arrives, schools and libraries all late and scarce, but sooner or later get there) and in most cases are really producing cities that are far from ideal, as is still the case of Bogotá, for instance.


To build a collective image.  

If an image of the desired future is “built”, a sort of navigation chart which should be collectively drawn, both by local activists and slum dwellers, together with specialists such as architects, putting together individual desires and ambitions with pragmatic and specialized knowledge, then the stimulation of collective imaginaries could start to act, and then probably some major changes could possibly occur pushed by the will to pursue that image generated through a participatory process.


Yes, it is major task, no doubt about it.


In the mean time, the United States and the European Union could start by removing their internal agricultural subsidies to enable the so much praised free trade becoming closer to fair trade, after all slums in spite of being located in certain parts of the world are not just a local problem, but a “glocal” one requiring solutions that are beyond any national border.
39. Maria
5.4.08 / 10am


Slums are organizations of emergency, every morning when I take my coffee; I look out side the kitchen window and a sea of small little boxes appear in front of me. A built topography, intricate and genius to my eyes. Full of life and smells, full of character and identity.  

Without any knowledge of engineering, only empiric survival skills, people inhabit out of necessity the outskirts of the Albarregas River in Merida Venezuela.  

Unlike Brook example, these slums are fortresses, island within the city.  

Isolation is a strong and destructive force in where the lack of belonging is somehow overwriting any sort of outside solutions as responsive as this may try to be.  

Working with the communities I have realized that the only tools we designers have to mediate with such strong and rapid force is, tools it self. The main issue with slums, as I see it, as I have lived it, is commodity, as contradictory as it may look. It's a matter of mentality.  

Archimedes principle, is the only truly solution I have witness. Self organization shouldn't be stopped yet emphasized from within.
41. [Mick](http://www.geocities.com/scotty83ie)
10.22.08 / 4pm


“Do no harm” – Mary Anderson  

I fear that the insertion of a capsule will only reinforce the existing negative aspects of a squatter settlement; these settlements are unique and individual creations and as such are due unique attention. We must understand that even a perceived improvement of a squat may have negative outcomes, we must understand the dynamic of these environments and the role they provide in this world- for the do provide a role, the provide cheap shelter and low cost living for many people (1 in 3 by 2040) which allows for people to survive on that small profit margin from menial jobs / home industries etc. the truth is that well intentioned slum Improvements may result in speculative development, increased cost of living and the gentrification in particular of central city slums; thus forcing inhabitants even further to the fringes of society. This is not a quick fix; it will take time, lots of energy, thousands of architects and billions of people
43. [Maurits](http://nightlybuilt.org/?cat=18)
1.31.09 / 10pm


I agree with Camilo and Maria. 


Let uss assume the slum dwellers already live in capsules, but insufficient ones. The materials they used were acquired from garbage dumps or, yes, Do It Yourself stores. These DIY stores can be the agents of change. They need to sell components that are cheap and sustainable and that assemble just as easily as Ikea furniture. These components need to be as engineered and sophisticated as Lebbeus suggests. A green roof system for example that buffers and filters rainwater. That cools the house below when the water evaporates as the sun shines. That filters the city air from dust and moderates the urban climate by preventing the ‘heat island' effect. And that provides farmland. 


But look at the larger scale: we also have to think about the viability of the slum. The slum has to be architecturally sustainable. It is known that the Peruvian slum dwellers adopt a street grid that is comparable to that of the surrounding city. You see in time that multi storey buildings appear on the corners of the streets, and that the slum slowly transforms into a mature neighborhood.


And at a community level: in Brazil NGO community centers are being built with Internet connections that serve as educational hubs to eradicate ignorance. Many examples, one driver: empowerment. By increasing the independence and self sufficiency of slum dwellers, we decrease the lack of perspective that poverty is. Give them the tools and they will know what to do. Make their dwellings a vehicle for independence. Connect them to the grid. Teach them what their rights are and before you know it mainstream society will start to listen. It is already happening.
45. [Architecture and Apathy | Nightly Built](http://nightlybuilt.org/?p=975)
2.15.09 / 12pm


[…] Woods (LW) is one of the few exceptions. In three posts on his blog he gives an introduction to the problem of the slums, and he gives one idea. He […]
47. [the best architecture of the decade - mammoth // building nothing out of something](http://m.ammoth.us/blog/2010/01/the-best-architecture-of-the-decade/)
1.25.10 / 6pm


[…] Woods describes as offering architecture as “the rules of the game”, or, the thinking he described behind a “capsule” which could offer architectural aid to people living in slums: From […]
49. [the best architecture of the decade « Vews](http://www.atelierv.com/vews/?p=545)
1.30.10 / 12am


[…] Lebbeus Woods describes as offering architecture as “the rules of the game”, or, the thinking he described behind a “capsule” which could offer architectural aid to people living in slums: From the side […]
51. [QUINTA MONROY by Elemental « RADDblog](http://raddblog.wordpress.com/2010/01/30/quinta-monroy-by-elemental/)
1.30.10 / 9am


[…] Lebbeus Woods describes as offering architecture as “the rules of the game”, or, the thinking he describedbehind a “capsule” which could offer architectural aid to people living in slums: From the side […]
53. [COLLASUS: Office for Collaborative Sustainability](http://collasus.com/textresearch/indi)
5.15.11 / 6pm


[…] References Mike Davis – Planet of Slums  http://download1081.mediafire.com/y6upk23xl89g/uk2ynhmwmzd/Davis+M+-+Planey+of+Slums.pdf <http://www.urbaninform.net/> Cecil Balmond – Informal The Endless City – Ricky Burdett Lebbeus Woods – part 1: The Problem part 2: What to Do? part 3: One idea […]
