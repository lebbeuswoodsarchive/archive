
---
title: COMMON GROUND
date: 2008-02-24 00:00:00
---

# COMMON GROUND


[![nysw8lr.jpg](media/nysw8lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/nysw8lr.jpg "nysw8lr.jpg")[![nysw3lr.jpg](media/nysw3lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/nysw3lr.jpg "nysw3lr.jpg")[![nysw7lr.jpg](media/nysw7lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/nysw7lr.jpg "nysw7lr.jpg")[![nysw14lr.jpg](media/nysw14lr.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/nysw14lr.jpg "nysw14lr.jpg")


The sidewalks of New York are extraordinary, if overlooked human documents. Found on them—as though on the tablets of a lost civilization—are accumulated inscriptions comprising a chronicle of the city's existential complexity. There are the marks and scratches of the ceaseless movement of people and vehicles; the stains of spills large and small; the ruptures and cracks of destruction and repairs; the often jarring juxtaposition of diverse materials; both fresh and long-faded symbols indicating this and that; and the mysterious black spots that are everywhere, which I have thought were ‘the souls of the departed' that somehow sank into the gritty surfaces, but which my wife assures me are only the marks of chewing gum carelessly thrown away. The task of deciphering the stories imbedded in even a square meter of any sidewalk in the city would be overwhelming, particularly if we consider that a story they might reveal could read like this:


The light gray is the bottom layer, infused here and there with sandy tan and gravelly blue speckles, and even some dull red flecks that were part of the original matrix. Then comes an almost transparent layer of black soot, deposited unevenly across the entire surface, probably ten years ago when the building across the street almost burned down. Then comes a layer….


Or, like this:


The Con Edison surveyor sprayed-painted a mark indicating where the water main is, six feet below the surface. A woman in soft-soled black shoes stopped abruptly just here, dropping a bottle of ketchup she'd gone out to buy, while dinner was on the stove (she was in such a hurry that the ketchup was unbagged), unaware that the water main was slowly leaking. A drunken man stumbled and….


Not exactly compelling stuff. No beginnings, middles, or endings. No climaxes or conclusions (unless you consider the faded bloodstains of a fatal shooting still visible, but which only an experienced forensic investigator would recognize as such), just the on-and-on of everyday life. Yet we could fill books, even libraries with such stories. Imagine what future anthropologists could make of them, if only the sidewalks would survive a few thousand years—but they won't. Like the daily lives they portray, they slip unnoticed past our consciousness and beyond the reach of memory. During the next street-digging project, the sidewalks will be broken up and carted in unrecognizable chunks to a landfill, even as new sidewalks are being laid down.


Without the stories, the sidewalks fall prey to gratuitous aestheticizing. They are visually very seductive, if we are not bound by classical standards of beauty. The layering, the diversity, the clashes and unexpected harmonies of textures, colors, tones, lines, dribbles and dots, cracks and joints. They combine to form a vast mini-terrain that is almost entirely of human invention, but not intention. Fusing accident and design, they form a visual field that is unique in part and whole, and inimitable. It can never be repeated or reproduced. Yet sidewalks can awaken the imagination of the visual artist who is easily intoxicated by a depth of possibility that seems, in its contradictions, inexhaustible. Just like the city, at once timeless and ephemeral, monumental and immemorial, they are close—very close—to the heart of things.


LW



 ## Comments 
1. elan
2.24.08 / 10pm


it is a shame that the buildings dont look more like the sidewalks… this terrain of accident is such a beautiful idea. it makes me think of a pre-renaissance (pre-perspective) casual manner of following the land, and not trying to control too much. i think of baroque and cubism as ruptures of the strict and rigid patterns being imposed. this notion of reading into the sidewalk, the chaotic orders of many knowable but untraceable histories is perhaps a way to think about the making of architecture that remains in a visual and conceptual state of perpetual happening.
3. elan
2.24.08 / 10pm


i should say rather, ´ít is a shame the buildings dont have the same substance of feeling more like these sidewalks´
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.25.08 / 12am


elan: Your reading of text, images, and the actuality is inspiring, opening hopeful doors to the future of architecture.
7. [fairdkun](http://lomohomes.com/fairdkun)
2.26.08 / 4am


finally, someone with a common sense in seeing environment where he lives in. not blinded by mere aesthetical reasoning.  

a lot of people claim to be, but so few, according to my experience, actually have the courage to really be courageous in this regard.
9. [Mark Primack](http://www.markprimack.com)
2.26.08 / 5am


Lebbeus, Thank you. That quality that you describe in streets and sidewalks is of course what I found so captivating in your own work. That you could portray not just surfaces but whole accretions of structures as though they had evolved through the ‘ceaseless movement of people'- that is what endeared me to your drawings long before I ever bothered to investigate their conceptual underpinnings. It wasn't that your architecture was ‘timeless', but that it absorbed time. I wonder and worry at your daring to distance yourself from such a preciously rare quality in the purer abstractions of your more recent work, even as I continue to be moved by your writing.
11. markw
2.26.08 / 7pm


There was a spot on Avenue B that I would pass everyday and imagine the accumulated events of the Lower East Side happening on… stood on by squatters watching Guliani's tanks blasting away at their building across the street, slept on by escaped mental patients with IV tubes in their arms, died on by accidentally-electrocuted dogs, walked over by John Coltrane or Mick Jagger.
13. spencer
2.26.08 / 8pm


There's a japanese aesthic, Wabi Sabi, that this reminds me of. It recognizes wear and tear as a method of its own beautification.


<http://en.wikipedia.org/wiki/Wabi-sabi>


Not to be flippant, but have you ever noticed spit on the sidewalk? It takes on quite an alien quality. We all recognize it but I could never describe the form it takes on. It is a pure relationship between trajectory, gravity, surface, and viscosity. No two a like. Each puddle its own. Both revolting and revealing. A true non-aesthetic.
15. [lebbeuswoods](http://www.lebbeuswoods.net)
2.27.08 / 12am


Mark Primack: I think you've got it right, and certainly many people feel the same. The earlier work you refer to stands on its own. I took it as far as I could and moved on. Still, I do believe the more recent, more abstract (and less appealing!) work carries forward—in time, at least—many of the transformational ideas. I follow my best instincts in these matters. We'll see where it goes.
17. [Deema](http://www.deemaland.blogspot.com)
3.2.08 / 9pm


I really do appreciate your expression on the “dominant” stories in the city, yes, i do believe they're overly dominating that we are no longer have the vision to “realize”. especially in a sidewalk context which acquires motion.


on another sense, your article reminded me by the wall perpendicular to Castelvecchio Museum, Verona. it reflects a vivid understanding of design and accident. which makes the building produced by both visual history and an architect.


But, a second thought makes me think about design and accident theory. if we apply those two terms on a time line, don't you think design is seeming to have the accidental quality in terms of its sudden effect compared to the slow change of existing layers -in which you described as accidents-, if believing it is said from the “environmental” side rather than the “intentional” side of the story?
