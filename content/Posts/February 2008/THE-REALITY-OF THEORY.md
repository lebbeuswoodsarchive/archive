
---
title: THE REALITY OF THEORY
date: 2008-02-06 00:00:00
---

# THE REALITY OF THEORY


[![Elektroprivreda Building, Sarajevo under siege, 1992](media/Elektroprivreda_Building,_Sarajevo_under_siege,_1992.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-1992blog.jpg "Elektroprivreda Building, Sarajevo under siege, 1992")  

[![Damaged Elektroprivreda Building, Sarajevo under siege, 1993](media/Damaged_Elektroprivreda_Building,_Sarajevo_under_siege,_1993.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-1993blog.jpg "Damaged Elektroprivreda Building, Sarajevo under siege, 1993")[![Reconstruction design by Lebbeus Woods, 1994](media/Reconstruction_design_by_Lebbeus_Woods,_1994.jpg)[![Reconstruction design by Lebbeus Woods, 1994, computer rendering by Carlos Fueyo, 2008](media/Reconstruction_design_by_Lebbeus_Woods,_1994,_computer_rendering_by_Carlos_Fueyo,_2008.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-1blog.jpg "Reconstruction design by Lebbeus Woods, 1994, computer rendering by Carlos Fueyo, 2008")  

[![Actual reconstruction, Ivan Straus, Architect, 2005](media/Actual_reconstruction,_Ivan_Straus,_Architect,_2005.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-2ablog.jpg "Actual reconstruction, Ivan Straus, Architect, 2005")](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-lw1994blog.jpg "Reconstruction design by Lebbeus Woods, 1994")


The question of how theory relates to practice has come up several times related to previous posts. The Electrical Management (Elektroprivreda) Building in Sarajevo, Bosnia, gives us a good chance to consider the issue.


First, a brief (as possible) history. Sarajevo is the capital city of Bosnia and Herzegovina, which in 1991 was one of the six republics incorporated into Yugoslavia, an independent Socialist Federation founded in 1945. With the end of the Cold War in 1990, Yugoslavia began to break up and the republics became independent countries in their own right. In the case of Bosnia, this was accompanied by massive violence, and a war raged on its soil from 1992 to 1995, which the so-called Dayton Accords ended. Tens of thousands were killed and the worst genocide in Europe since the Holocaust was carried out by Bosnian Serbs against Bosnian Muslims. The city of Sarajevo was under blockade and military siege from 1992 to 1995, during which time it had no normal supplies of food or water, no electricity, gas, or heat, and no telephone links. It was almost completely cut off from the outside world.


The UN flew in canned food and basic medical supplies. Journalists were allowed to fly in and out on UN relief flights. A few cultural figures—prominently Susan Sontag—became ‘journalists' and came to Sarajevo during its darkest hours to give moral support, stage theater performances, and other gestures to encourage people generally, but particularly Sarajevo's world-class artists and intellectuals. The city was dark and cold and under constant artillery and sniper fire.


The Bosnian Serb army surrounding Sarajevo had in mind to humiliate the people in the city, to punish the city for its cosmopolitan character and traditions. As much as anything, the siege was a terrorist act, a war on diversity and urbanity, an attack on the very idea of city. Thankfully—because of the strong spirit of its people—the terrorists ultimately failed.


In November of 1993, I went to Sarajevo—as a ‘journalist'—at the invitation of Haris Pasovic, head of the Sarajevo International Film and Theater Festival, who was aware of my work from a lecture I have given in Sarajevo two years earlier. I was accompanied by another architect, Ekkehard Rehfeld, who fully participated in all conversations and events. I brought with me forty freshly printed copies of “War and Architecture” (Pamphlet Architecture 15) and a roll of photocopy enlargements to make an exhibition. My goal, put simply, was to help architects there begin thinking about the role architecture would play both during and after the siege. I was able to see first-hand what the people were enduring and many damaged buildings.


The Electrical Management Building, along with the Post Office, Parliament, National Library, mosques and churches were symbolic of the civic life of the city, and therefore were especially targeted by the besieging Bosnian Serb army. I met the architect of the building, Ivan Straus, one of the most respected architects in Yugoslavia, who was also very supportive of my presence and ideas. It was he, during a later visit, who asked me to design a reconstruction of the Electrical Management Building.


In this and other reconstruction projects I proposed for Sarajevo, my theory was clear: the siege, and the four-year-long war, changed everything. Socialism was out, and an uncertain privatization was in. The city was losing its ethnic diversity, as Serbs, Croats, Slovenes, and anyone who could left and, for a while, before the city was completely surrounded, Muslim refugees poured in. The infrastructure of utilities and services was severely damaged, with no idea of where the money would come from to repair them, or when that would even be possible. Buildings vital to the social and economic functioning of the city were damaged and unusable without extensive reconstruction, but, again, how this would be financed was unclear. More than all this, the people of the city had suffered years of deprivation, terror, and uncertainty, and many would be transformed by it. How, I asked, could architecture play any positive role in all of this?


My answer was that architecture, as a social and primarily constructive act, could heal the wounds, by creating entirely new types of space in the city. These would be what I had called ‘freespaces,' spaces without predetermined programs of use, but whose strong forms demanded the invention of new programs corresponding to the new, post-war conditions. I had hypothesized that “90% of the damaged buildings would be restored to their normal pre-war forms and uses, as most people want to return to their old ways of living….but 10% should be freespaces, for those who did not want to go back, but forward.” The freespaces would be the crucibles for the creation of new thinking and social-political forms, small and large. I believed then–and still do–that the cities and their people who have suffered the most difficult transitions in the contemporary world, in Sarajevo and elsewhere, have something important to teach us, who live comfortably in the illusion that we are immune to the demands radical changes of many kinds will impose on us, too.


The design for the reconstruction of the Electrical Management Building is a case study in the application of this theory. Most of the building would be restored to accommodate corporate offices of the known kind. However, in the space that had been literally blasted off by artillery fire, would be constructed a freespace, to be inhabited by those who, in the reinvention of ways to inhabit space, would open the way to the future.


Architects are idea people. We have concepts and make designs that embody or implement them. We present them as clearly and openly as possible, and can only hope that others will find them useful to their ends, and build them. In the case of the Electrical Management Building in Sarajevo, my theory and design were not used. This does not mean that they are wrong, or a failure. Nor does it mean that those who elected to ignore them were wrong to do so. More challenges await us, and the ideas may yet become useful, in ways that I could never conceive.


LW


[![Lebbeus Woods exhibition in the destroyed Olympic Museum, Sarajevo under siege, November 1993](media/Lebbeus_Woods_exhibition_in_the_destroyed_Olympic_Museum,_Sarajevo_under_siege,_November_1993.jpg)](https://lebbeuswoods.files.wordpress.com/2008/02/saraepb-olymp1993.jpg "Lebbeus Woods exhibition in the destroyed Olympic Museum, Sarajevo under siege, November 1993")



 ## Comments 
1. Josh V
2.6.08 / 5pm


These “freespaces” you're creating are quite spiritual in their introspective manner. Wherever you've designed an architectural bandage, there doesn't appear to be any glass or exterior penetrations. Within that space, the viewer cannot look out, only within to the spot they occupy to remember what happened there specifically and how they feel about that. Do you have any sketches or models showing what does happen inside?


I would love to see something like this built just to see what people would do with it, how they would occupy it, or how they act within it. Would it just be another monument dedicated to war that tourists walk by and take pictures of? or would it be a lunch-break room for the occupants of those offices? or perhaps a space for art installations and reactions throughout the city. I usually don't like the idea of leaving a space's use up to the public, but in this case I think the space is of too much cultural importance for them to ignore.


What was done? Did they just rebuild it as it was?
3. [lebbeuswoods](http://www.lebbeuswoods.net)
2.7.08 / 12am


Josh: I like your ‘introspective' interpretation of the freespace ‘injection.' Also your willingness to play freely with the possibilities of the interior space. The lunch-break room and the space for installations are equally potential, and not mutually exclusive. You are definitely one of the people I would wish to inhabit the spaces, and make them your own.


The blue glass building is what was rebuilt.
5. Elan
2.7.08 / 3pm


The Electrical Management Building project in Sarajevo has many similarities to a building in Prague. While it wasn't damaged in a war, it has an identity that is completely aligned with violent act, and with failure, as well. The building is the administrative office tower of the Czech Radio (built under the communist regime) and never finished. It is the tallest building in Prague – 27 stories and it is on a hill- and for 25 years or so it has been vacant, for many years recently, not even with a facade. Similarly with the reconstruction of the Electrical Management Building, the solution seems to be to cover it with shining glass. This building is part of a much larger complex of administrative, high rise buildings imposed at the center of a quickly built residential community of the 60's. In the late 90's, the city of Prague owned all this administrative land and they asked planners to respond to the site. Many of these ideas brought a range of scales and a diversity of spaces to the area, to contrast with the looming imposition of the tower, and the sense of emptiness in the public space. In the end, the city sold the land in fragments to individual developers.  

The concept of unifying and strengthening the neighborhood of 60,000, and building relevant architecture, was put aside in favor of a completely market-based program. The ‘Masterplan' was introduced and was promoted for political purposes, using a language that spoke positively about the future, while in reality it was a cover for reducing, replacing or eliminating civic programs; i.e. no bus station, no town hall, no auditorium or forum, no more farmer's market, no new roads, no sports area, but a new shopping center, new hotel, new luxury condominiums. The public space became marginalized to the role of becoming a ‘leftover, not-the-building' space.  

The new ‘manager' of the site asked ‘International (st)architect' Richard Meier to create this new Masterplan. While boasting a district center, and a place in Prague's history, its focus did not look beyond the boundaries of the site. The architecture claims to be ‘a counterpoint to Prague Castle' and ‘a 21st century public space', while in fact it is a collection of hermetic objects designed for a few nouveau-riche, that have no relation- in the worst way- and desire no relation, to the surroundings where they are set. This project, however, is still not definitely going to be realized- it is being built slowly and in parts. It has had to be redesigned a number of times due to opposition by the local government, by UNESCO, and by other preservation groups. Currently the mall is rising out of the ground, and the Radio Tower is complete with powerfully shining glass.  

It seems that the majority of people resent this part of Prague- for its ugliness- and want it to be rebuilt. Unfortunately, while the current proposal is not exactly admired, in general it is becoming accepted. A combination of architects who do not want to fall out of favor with decision-makers, and a general public who want to see change, critically or not, is the reason. The imagination of the place, and the memory of its impact-for decades passed- is slowly being filled in, though ironically, in a way that keeps much and many left out.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
2.7.08 / 4pm


Elan: Your thorough description of the situation in Prague illuminates what is happening throughout the formerly Socialist world in Eastern Europe. Privatization has taken over. There are big profits to be made, at least by an entreprenurial few, from the sale of government owned properties to developers. They bring a glittering, glossy kind of ‘Western' architecture, as a symbol of the East catching up with the West. The conservatives in these ‘new' countries want to fight this kind of cheap/expensive import, but don't have much of a chance. One reason is that they merely want to preserve the past and have no alternative to moving forward. 


It seems to me a great tragedy that these new countries have rushed so heedlessly into Western/American-style consumer capitalism. There was a chance here to forge a ‘third way” that would set a model for the whole world, and provide a true alternative to what we already know. Architecture would necessarily be a vital part of this alternative.
9. aitraaz
2.8.08 / 9am


Many interesting points here: without wanting to veer too far off topic, i would contest the notion that these countries “rushed” into Western style consumer capitalism of their own free will. More accurately they were “shocked” into western style capitalism and into a (nearly) global hegemony of ‘unregulated' (savage) capitalism. This unregulated capitalism continues to spread with particular violent force to this day (middle east).


Fukuyama's preposterous declaration of the end of history with the victory of economic liberalism over all other systems came at a crucial point in the growth of unregulated capitalism: the fall of the Soviet state. Russia together with many of its satellite states and others under its influence were immediately cannibalized by international financial institutions (IMF, World Bank) through the pretext of subsidies and loans in order to rescue these countries from inflationary and deficit problems. In turn, massive and violent privatizations were made in astonishingly short periods of time leading to radical polarizations in wealth distribution where an absolute minority of wealthy citizens (beneficiaries of privatization) operate in unfettered markets above the vast majority of their poor populations. In Russia for example (consider the oil/gas/energy oligarchy), such a vast and violent re-distribution of wealth was undergone that until recently the average working wage was far lower than it was under the Soviet system.


This unregulated capitalism was brought upon these countries in a tried and tested form; it was the same blueprint used on South America from the 70's to the 90's, Poland, China, Indonesia and so on. It continues today in the middle east, where countries not belonging to the global market or mass privatizations must be colonized for surplus. Unregulated capitalism also begins to unfold upon itself in western countries (New Orleans as an example) under the guise of ‘disaster capitalism,' wherein existing urban fabrics and social fields are restructured (using the same shock methodology, eg. order out of chaos), re-privatized and redistributed in more profitable ways. As Deleuze and Guattari once ironically stated, capitalism (in itself a revolution force) would shoot itself straight to mars if conditions demanded it.


In a certain sense, this is the field in which architecture currently operates. Fukuyama, a key feature in the neoconservative movement and contributor to the Reagan doctrine (and more importantly Friedman economics) openly supported the monetist concept that an unfettered capitalist based market will balance itself, in direct opposition to the Keynesian concept that capitalism must be regulated. 


It is important that we understand that this concept is fundamentally flawed, and that the ‘natural balance' an unhindered capitalist system seeks is solely one of profit. Thus, in recent years, for example, the sustainable energy market has been significantly reduced and replaced by a much more lucrative ‘disaster management' market (eg. anti-terrorism, anti-disaster etc) for the simple reason that it leads to greater profits. Such as market will seek ever greater surplus vectors through the increasing need for greater disaster.


Architecture thus operates in the wake of this capitalist vector, offering its services to the clients of the new unregulated capitalist word (the few, the wealthy) building primarily for the privileged luxury market (luxury hotels which represent an enormous boom in the construction market, massive museums and entertainment complexes, elite retail and consumer structures (PRADA for one), enormous speculative housing complexes and developments and more. 


Perhaps “third ways” are emerging, currently in South America for example, where tradionally politically left oriented populations are finally making their voices heard after decades of devastation under violent privatizations schemes. International money borrowing is being soundly rejected by many of these governments (thus reducing the necessity of the continuing privatization of government industry) and a move to more fair and socially driven economic programs in slowly and humbly making its way back. Perhaps within these emerging ‘fissures' (brought about by populations) a third way may emerge…
11. [lebbeuswoods](http://www.lebbeuswoods.net)
2.8.08 / 3pm


aitraaz: I deeply appreciate your calm and clear analysis of what is happening in Eastern Europe and many emerging societies around the world. Your distinction between unregulated capitalism and a regulated capitalism hits the mark, and has immediate political implications, such as, will the U.S. elections this year just give us more of the same neocon ‘free market' rapaciousness? Or, will it at least shift the momentum in a new, better direction, towards regulation, for starters?


As architects, I don't think we can wait for the political climate to change (or not), before we act. We have to think of ways to deal with the problems of the situation and hope they will be useful in creating change, so that the principles and techniques will be ready when the time of their usefulness comes. In a sense, we have to be part of the advance guard of a third way….
13. [Mark Primack](http://www.markprimack.com)
2.9.08 / 7am


Lebbeus,  

Not being an academic, I equate ‘freespace' with ‘freetime'. And I can't help but think of Bernard Tschumi's ‘follies' for La Villette as a regulated consumer-targeted version of your idealistic ‘freespace'. Both require a leisure class, whether born of affluence, tourism, distraction, disaster, displacement or simple unemployment. All result in ‘freetime'.  

Sarajevo reminds me of the Macedonian Romani singer Esma Redzepova, who declared that “music is the luxury of the poor.” That is, freespace free of architecture.  

 I believe we are headed for a form of ‘freespace', but will it prove to be the ‘tavern' where revolution is fomented and regenerated, or the ‘salon' where the profits of disaster management are regularly celebrated?  

Thank you.
15. Godofredo
2.9.08 / 4pm


Its curious that you find a healing potential within architecture. Immediately Joseph Beuys comes to my mind with his concepts of warmth theory and social sculpture.  

Or even, in a Deleuzian way, it's like an attempt of a clinical architecture.  

Anyway I agree with much of what you wrote, but I would say that architecture´s task is not so much to heal (at least by the usual standards of healing) but to inflict new wounds.  

Not so much about parasites in the leftovers of capitalized space but war-machines producing even more fissures on the social tissue. A sort of slum-invasion.
17. [lebbeuswoods](http://www.lebbeuswoods.net)
2.9.08 / 5pm


Godofredo: Bit by bit, the depth of your radicalism emerges! Well, I support it, and have said much the same. However, I want to make sure that the destruction we do (architecture always destroys, along with its construction) is precisely measured, and is far outweighed by the good we do. That's the moral choice facing any of us in our lives and work and it's a tough one, because there's no assurance that the good we intend will in fact outweigh the bad we do. It's a personal judgment call we face every day. Also another reason other- and self-criticism are so important. We must be as judicious and circumspect as we can, then act as decisively as possible.


My hope for architecture is that it will become more decisive in confronting the really tough problems, which means that architects will stop waiting for orders from above, and fulfill the demands of the social agency inherent in the very idea of controlling (designing) space. Seems that, in this regard, there are not many—as yet—willing to do so. But that can change. And I believe it will.
19. [lebbeuswoods](http://www.lebbeuswoods.net)
2.9.08 / 6pm


Mark Primack: I find your idea of “freetime” a rather strange one. All of our time is, at root, freetime. The fact that we choose to make committments of our time to others (family, work, institutions) only speaks to our willingness to give up our freedom for “good” or “necessary” reasons. And that's a choice. It's not essential to the human condition, as many (bums, drifters, artists, Zarathustras) have shown.


In the same sense, all space is [already] freespace. We can interpret, and use it, as we will. The fact that we follow the ‘programs of use' designated by those who paid for its design and construction, only speaks to our choices to do so. And that's all right, most of the time. But it's not essential to the nature of designed and constructed space.


The only innovation I added to ([already existing] freespace was to make it difficult to occupy. The difficulty impels us to invent new ideas of occupation, habitation, living that somehow correspond to our confrontation with new conditions of existence—-personal and social.


And, yes, freespace is risky. Just like freedom. It can be used, and abused, with equal ease or difficulty….
21. [Mark Primack](http://www.markprimack.com)
2.10.08 / 1am


As a long time admirer of your work, I'm hoping that this discussion and others here, on slums and Sarajevo, are taking place on a conceptual planet inhabited by people who still labor to live.  

 My ‘idea' of ‘freetime' may be unromantic, tedious, compromised or simply uninteresting, but it is not ‘strange'. Nor are bums, drifters, artists, Zarathustras essential to the human condition, though they all claim to be. That conceit is their most striking commonality. They do not put food (labor intensive organic?) on our tables, shirts (pesticided third world 100% cotton?) on our backs, or roofs ($500 a square foot on your coast and mine) over our heads.  

 I have friends who raised money, went to Kosovo and distributed firewood in the Rom community there. Warm bodies and hot meals- we may not appreciate that aspect of architecture until it's gone for us too. Is it wrong for me to ask what you think you brought to the people of Sarajevo? You went there as a ‘journalist'. Well, journalists come, make their deadlines, headlines and then leave to make their careers.  

 In your writings I often think I hear James Agee, and I look for Walker Evans in your images. But I can only associate your response to my comments with ‘famous men' sitting in safe – often tenured- positions.  

 Some may enjoy your work for the distance they measure between your worlds and theirs. I am inspired by how proximate it all seems to the real lives of the disassociated and dislocated, today's working people who have no neighborhood, no commons, not village, no refuge and no freetime. Those people, the people of Sarajevo and San Francisco, don't need more ‘difficulty', from you or anybody else.
23. [lebbeuswoods](http://www.lebbeuswoods.net)
2.10.08 / 2pm


Mark Primack: In Sarajevo, during the period I describe, warm bodies and hot meals were hard to come by, still it was telling that people were glad that I and others came with only ideas for the future, or art and theater, or a workshop. It's hard to understand, I know, but in desperate circumstances people need to engage their minds and spirits in something hopeful that reconnects them to the wider human world.
25. [Mark Primack](http://www.markprimack.com)
2.11.08 / 1am


Lebbeus, right you are. Just ask Frank Capra. He told us, during trying times, that “It's a Wonderful Life, “Heaven Can Wait”, and that we could perfect democracy as soon as “Mr. Smith (Obama?) Goes to Washington.” I witnessed the engagement you speak of when my city suffered an earthquake in 1989 that leveled our downtown. My engagement as an architect eventually led me to elected office.  

 And Capra reminds me of the Hoovervilles of the Great Depression. Here's something of what Wikepedia has to say on that subject, some aspects of which might illuminate your discussion of slums.  

“These settlements were often formed in unpleasant neighborhoods or desolate areas and consisted of dozens or hundreds of shacks and tents that were temporary residences of those left unemployed and homeless by the Depression. People slept in anything from open piano crates to the ground. Authorities did not officially recognize these Hoovervilles and occasionally removed the occupants for technically trespassing on private lands, but they were frequently tolerated out of necessity. In New York a street was dedicated to children's games, the games were provided as an idea of Herbert Hoover. This worked to boost the morale of the nation.  

Some of the men who were made to live in these conditions possessed building skills and were able to build their houses out of stone. Most people, however, resorted to building their residences out of box wood, cardboard, and any scraps of metal they could find. Some individuals even lived in water mains.  

Most of these unemployed residents of the Hoovervilles begged for food from those who had housing during this era. Several other terms came into use during this era, such as “Hoover blanket” (old newspaper used as blanketing) and “Hoover flag” (an empty pocket turned inside out). “Hoover leather” was cardboard used to line a shoe with the sole worn through. A “Hoover wagon” was a car with horses tied to it because the owner could not afford gasoline. “
27. [lebbeuswoods](http://www.lebbeuswoods.net)
2.11.08 / 1pm


Mark Primack: Thanks for the Capra reference. It's perfectly apt. As for the Hoovervilles, it's hard for people today to believe that they were populated by white-collar (an old-fashioned term) office workers, as well as blue-collar factory workers. Such was the collapse of the free-market system that people who never thought they'd lose everything and become homeless did.  

In Sarajevo, Susan Sontag staged—at the height of the siege—a version of Beckett's “Waiting for Godot.” For the Sarajevans, with their profoundly ironic and skeptical sense of humor, Beckett was the equivalent of Capra for optimistic Americans. Accordingly, “Godot” was a great success at lifting spirits!
29. [Mark Primack](http://www.markprimack.com)
2.14.08 / 4am


Lebbeus, I'm familiar with the films of Emil Kusturica (Black Cat White Cat, Underground, Arizona Dream, Time of the Gypsies) and I have many Balkan friends (mostly émigré musicians), So I've noticed that our modern inclination to act out life as though it were a TV or a movie role is informed in Eastern Europe by magic realism and the Cold and Balkan war theatres of the absurd. I can't help but think that the audience in Sarajevo was responding appreciatively to the celebrity of Sontag as much as resonating to Beckett's end-of-WWII catharsis. But can you stretch that moment into a movement, into a culture that sustains an architectural effort? I watch my Balkan friends settle into cars, home ownership, day jobs and consumer goods to beat the band. Can peace-time Sarajevo be far behind?  

I think perhaps the Gypsies- those who deprive themselves of both the security and the anonymity of assimilation- offer the greatest prospect for the sustained ‘freespace' of your researches. After all, they own all and none of this world. I'll be listening to my daughter singing in romanes with a brass band in Williamsburg this Sunday night. I'll scout it out for you. Regards.
31. Zach V
2.19.08 / 3pm


I just want to make sure that the freespace we're talking about is not construed as a place of infinite possibility. Freedom is always limited–in this example, by history and structured space. There are certain things you're free to do in the agora. Certain things in a park. But there are many things you cannot do. This is due both to the design of the space as well as the peoples and practices that are brought together there. If you want to create spaces that open up new possibilities, they have to allow experimental movements and ways of thinking, but this can only happen in particular ways. You have to be attuned to the history of the spaces and the habits of the peoples you're working with. Perhaps a big, empty space would offer liberating potential; perhaps something very confining in certain ways would be more freeing. It depends on what kind of liberation we want. Is this about thinking? Physical activity? Market interactions? Social formations? All of these are incredibly site-specific questions that make it more of a question of “highly-determined-experimental space” rather than “freespace.”
33. [blog » Blog Archive » The architectural “freespaces” of Lebbeus Woods](http://derekwisdom.com/blog/?p=76)
2.1.09 / 11pm


[…] here. Filed under: Everything […]
35. [Electroprivreda | reconstruction and “freespace” designed by Lebbeus Woods « dpr-barcelona](http://dprbcn.wordpress.com/2009/11/23/electroprivreda-reconstruction-and-freespace-designed-by-lebbeus-woods/)
11.23.09 / 8am


[…] for the reconstruction of the Electrical Management Building is a case study in the application of this theory. Most of the building would be restored to accommodate corporate offices of the known kind. […]
37. [notes on the aesthetics of decay « luctor et emergo](http://aureliomadrid.wordpress.com/2010/06/29/notes-on-the-aesthetics-of-decay/)
7.14.10 / 7pm


[…] [99] …see Woods, Lebbeus ‘The Reality of Theory' [https://lebbeuswoods.wordpress.com/2008/02/06/the-reality-of-theory/](https://lebbeuswoods.wordpress.com/2008/02/06/the-reality-of-theory/  ) […]
39. […notes on the aesthetics of decay « luctor et emergo](http://aureliomadrid.wordpress.com/2010/08/15/notes-on-the-aesthetics-of-decay-2/)
8.15.10 / 4pm


[…] [99] …see Woods, Lebbeus ‘The Reality of Theory' <https://lebbeuswoods.wordpress.com/2008/02/06/the-reality-of-theory/> […]
