
---
title: HIGH HOUSES
date: 2010-02-23 00:00:00
---

# HIGH HOUSES


[![](media/lwblog-high-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-11.jpg)


The High Houses are proposed as part of the reconstruction of Sarajevo after the siege of the city that lasted from 1992 though late 1995. Their site is the badly damaged “old tobacco factory” in the *Marijn dvor* section near the city center.


The concept of the project is simple. The houses rise up high into the airspace once occupied by falling mortar and artillery shells fired by the city's besiegers in the surrounding mountains. By occupying the airspace, the High Houses reclaim it for the people of the city. Balancing on scavenged steel beams welded end-to-end, they are spaces of a new beginning for Sarajevo, one that challenges—in physical terms—the city's past and present, aiming at a future uniquely Sarajevan. Stabilized by steel cables anchored to the site, the houses, poised like catapults, fulfill the paradoxical desire to fly and at the same time be rooted in their place of origin.


These houses are not for everyone. Indeed, probably only a few could master their challenges. Yet each mastery would manifest a spirit of courage and inventive skill in the name of all who must reinvent a city transformed by destruction.


[![](media/lwblog-high-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-3.jpg)


[![](media/lwblog-high-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-21.jpg)


[![](media/lwblog-high-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-10.jpg)


[![](media/lwblog-high-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-7.jpg)


[![](media/lwblog-high-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-5.jpg)


[![](media/lwblog-high-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-9.jpg)


[![](media/lwblog-high-61.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-high-61.jpg)


LW



 ## Comments 
1. [Michael Phillip Pearce](http://www.carbon-vudu.us)
2.23.10 / 1am


Excellent concepts and they still hold true to this day. The drawings set major precedents–“authenticity”. 


To take this concept and develop a mock structure somewhere in the glades off the Gulf Coast would be a living experiment that could push an envelope to the “edge”. In fact, I would love to see an abandoned oil platform as the host to such an event…some of these contrived kinetics built off of it, perhaps in a movie–for it seems entertainment is the only moving force in America. 


Great work LW!
3. Mike
2.23.10 / 5am


Do it.
5. [Miller Taylor](http://www.middle-grey.com/images/)
2.23.10 / 2pm


Excellent post. High Houses for the few, to inspire the many. The imagery alone is noteworthy, but combined with the themes of trial, challenge, and transformation they take on a beautiful and classic narrative. 


Keep up the inspiring work.
7. [Chris](http://www.chris-hamby.com)
2.23.10 / 3pm


I keep seeing City 17 from Half Life. 


Beautiful concepts!
9. wm
2.24.10 / 4am


i wish it was built.
11. [Lebbeus Woods – Real live drawings « RESET ARCHITECTURE | reset studios blog](http://resetstudios.wordpress.com/2010/02/25/lebbeus-woods-real-live-drawings/)
2.25.10 / 9pm


[…] <https://lebbeuswoods.wordpress.com/2010/02/23/high-houses/> […]
13. [Stephen Korbich](http://www.ffadesign.com)
2.26.10 / 5pm


The imagery is fantastic as usual. The ideas spark stories in my head about the people who would make such a place. The expense and difficulty of constructing such precarious and delicate structures would be such an extravagance in a post catastrophic world that these High Houses would probably be symbols of power or religion or some other profound purpose.  

I continue to be intrigued by the use of what seems to be found materials in your concepts. Not just any found materials though but repurposed military scraps from war machines of all kinds.  

There is certainly inspiration for those of us (is it all of us now) who are looking to reuse old buildings and materials.  

I would love to see or hear the story of the community of survivors who build in a destroyed world and how and why they do it.
15. Mr Black
3.5.10 / 10am


amazing installation….. they could be putted in the forest too around the city
17. [Anthony Matters](http://www.heterarchy.co.uk)
3.8.10 / 6pm


I remember seeing this work as a student and being completely mesmerised by it – in fact it was a key inspiration for me to pursue the career that I now have.


What is truly amazing is that these could have been produced yesterday, they are as innovative and exciting now as they were then.


Lebbeus is a legend who has inspired an entire generation of creative adventurers.
19. [High Houses | AMNP](http://architecture.myninjaplease.com/?p=7472)
3.9.10 / 9am


[…] noticed these images by Lebbeus Woods of his proposed High Houses popping up in a couple places on the interwebs yesterday and figured ‘why not on AMNP, […]
21. [HIGH HOUSES by LEBBEUS WOODS | Architecture Lab](http://architecturelab.net/2010/03/10/high-houses-by-lebbeus-woods/)
3.10.10 / 11am


[…] inventive skill in the name of all who must reinvent a city transformed by destruction.” by LEBBEUS WOODS […]
23. shreyank
3.11.10 / 6am


I don't know… seems to me like Mr. woods work fit better in a art direction for ‘Blade runner' or matrix. I really need to study it in depth to understand it… as of now I really can't relate o understand it.
25. [High Houses – Lebbeus Woods « [re]visioni](http://revisioni.wordpress.com/2010/03/11/high-houses-lebbeus-woods/)
3.11.10 / 10pm


[…] Testo e immagini da: <https://lebbeuswoods.wordpress.com/> […]
27. [High Houses « Gluey Porch Treatments](http://mjfchance.wordpress.com/2010/03/11/high-houses/)
3.11.10 / 11pm


[…] March 11, 2010 · Leave a Comment You gotta check out these incredible designs by American architect Lebbeus Woods.  You can see all the images on his blog here. […]
29. [High Houses | Icon\_ology](http://iconology.therndm.com/archive/high-houses/1023)
3.13.10 / 12am


[…] WordPressAMNP […]
31. [Craters as other form of architecture [Alles Ist Architektur] « dpr-barcelona](http://dprbcn.wordpress.com/2010/03/15/craters-as-other-form-of-architecture-alles-ist-architektur/)
3.16.10 / 9pm


[…] Hanging gardens of Barcelona by T?F. Another project that fits perfect in this proposal are the high houses designed by Lebbeus Woods, that can be rised up all over the crater's hole. Not so long ago, […]
33. MadProphet
3.23.10 / 7pm


Unfortunately, this is going to be built on the site:


[![](https://i0.wp.com/www.sa-c.info/updateImg/sarajevo_city_center_a93b623736_SLIKA2.jpg)](http://www.sa-c.info/updateImg/sarajevo_city_center_a93b623736_SLIKA2.jpg)


A hotel and a gigantic mall run according to sharia laws.


Thank You, Lebbeus, for everything You have done for my city. I hate to say that most of it was probably in vain
35. [crabe.ru » HIGH HOUSES « LEBBEUS WOODS](http://crabe.ru/?p=3617)
3.30.10 / 8pm


[…] via <https://lebbeuswoods.wordpress.com/2010/02/23/high-houses/> […]
37. [Existing Visual » HIGH HOUSES « LEBBEUS WOODS](http://www.existingvisual.com/2010/03/30/high-houses-%c2%ab-lebbeus-woods/)
3.31.10 / 1am


[…] via <https://lebbeuswoods.wordpress.com/2010/02/23/high-houses/> […]
39. [Inhabiting Pits and Craters in Los Angeles « dpr-barcelona](http://dprbcn.wordpress.com/2010/05/26/inhabiting-pits-and-craters-in-los-angeles/)
5.26.10 / 11am


[…] We think that these kind of projects should be done or at least, dreamed. As the Braga Stadium in Portugal, which was placed in a specific location in order to avoid building a dam along the water's edge in the valley, there will be a kind of poetic when reviewing the Irwindale zone and found out that the Peck Road Quarry is full of utopic structures that became part of its new nature, as the crane rooms designed by Antonas or the Lebbeusian High Houses: […]
41. [High Houses | steparmorer](http://steparmorer.com/2010/09/13/high-houses/)
9.13.10 / 7pm


[…] land have been the goals of political and artistic movements for centuries, but these concepts by Lebbeus Woods portray a level of hope through reclamation that borders on the fantastical. In his […]
43. [Jo](http://www.stumbleupon.com/stumbler/MYRTADotson)
4.1.11 / 12am


Keep up the wonderful piece of work, I read few blog posts on this website and I think that your web site is very interesting and contains bands of good info !
45. [Concepts of High Houses – FutureTechture](http://www.futuretechture.com/2012/03/concepts-of-high-houses/)
3.25.12 / 6pm


[…] The concept of the project is simple. The houses rise up high into the airspace once occupied by falling mortar and artillery shells fired by the city's besiegers in the surrounding mountains. By occupying the airspace, the High Houses reclaim it for the people of the city. Balancing on scavenged steel beams welded end-to-end, they are spaces of a new beginning for Sarajevo, one that challenges—in physical terms—the city's past and present, aiming at a future uniquely Sarajevan. Stabilized by steel cables anchored to the site, the houses, poised like catapults, fulfill the paradoxical desire to fly and at the same time be rooted in their place of origin”. (source: LEBBEUS WOODS) […]
