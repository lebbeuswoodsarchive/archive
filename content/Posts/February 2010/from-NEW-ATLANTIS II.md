
---
title: from NEW ATLANTIS II
date: 2010-02-03 00:00:00
---

# from NEW ATLANTIS II


“Organizing elements are hidden,


too obvious to notice again.


Scattering rules each apply once.


The structures of spaces interact,


constant engines of chance.”


[![](media/lwblog-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-3.jpg)


“A realm of spaces between


demi-worlds of difference


neither confirms the parts,


nor constitutes a totality,


violent or extreme.”


[![](media/lwblog-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-21.jpg)


“We have deep towers,


standing on earth's inner side,


wherein wars rage against


states of gravity's decay,


at the scale of one.”


[![](media/lwblog-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-11.jpg)


“We have morphological events,


wherein the secrets of change


become visible and plain,


returning to us the pure


Mystery of play.”


[![](media/lwblog-43.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-43.jpg)


LW



 ## Comments 
1. Mark Keller
2.3.10 / 8pm


I imagine these cities of thought climbing similarly from both the inner surface of the skull and of Earth's crust. By relating artifice to geomorphology you seem to insinuate a new cave mysticism, one rapidly and perpetually reconfiguring toward utility. Although at first blush the dream is one of manifest destiny and infinite expansion, I now see it yields to gravity if nothing else. Like the mind, collective human endeavor does have expiring ‘form-making energies' over significant scales of time. Further, if this cavity is driven by forces of both fission and collision, these tensile skins may become massive, not just voluminous. In short, I struggle to grasp the disjunction between geologic solidity and this new metallic emptiness.
3. [Yefim Freidine](http://fimafr.ru)
2.5.10 / 2pm


looks like pavel filonov's paintings 🙂





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.5.10 / 6pm
	
	
	Yefim Freidine: Thank you for the reference. [Filonov](http://www.google.com/search?client=safari&rls=en&q=pavel+filonov&ie=UTF-8&oe=UTF-8) was certainly a great painter.
5. [Chris Teeter](http://www.metamechanics.com)
2.6.10 / 12am


wow!
7. [Kenneth Howe Jones](http://www.poulsbohemian.com)
2.11.10 / 5pm


Leb: “In spaces of relentless struggle, titanium shards break glass and stone into human form.” I paraphrase your words from 35 years ago.
