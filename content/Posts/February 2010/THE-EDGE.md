
---
title: THE EDGE
date: 2010-02-21 00:00:00
---

# THE EDGE


[![](media/edge-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/edge-2.jpg)


(above) Philippe Petit walks on a wire between the Twin Towers in August, 1974.


At the edge, we perform at our peak, our best. We have no choice, really. Anything less and we fall off the edge, plunging into the unknown. The edge is a limit, in the first place of our knowledge. We have to push ourselves to get to it. The closer we come to the edge, the more we have to use the knowledge we have. At the edge only the hard-core knowledge is useful. All the frills and redundancies, the posturings and pretensions, simply get in the way and in fact will doom us to failure. At the edge it is only the essential and the authentic that count.


Architects rarely work anywhere near the edge. They usually operate well within the boundaries of what they comfortably know and what others know, too. We expect architecture to be stable and sure, indeed with just enough frills and pretensioms to make it a little, but not too, different from what we have known perhaps many times before. In this way, architecture can be reassuring and at the same time interesting. The designed spaces we inhabit, architects and their clients believe, should not push us to the edge, but instead keep us in a comfort zone, where we can live out our lives as fully as possible.


But wait. There is a contradiction here. If our goal is to live as fully as possible, to perform at our peak, our best, to use our most crucial knowledge and to shed all the superficialities that only distract us, then architecture that keeps us in the comfort zone, never testing our knowledge or skills or our will to excel, would seem to be somehow inadequate. The design of spaces we live in should challenge us to be imaginative, inventive, intrepid.


But wait again. Maybe most people prefer comfort to living fully, at the edge. Maybe we can live vicariously to the full—through movies, art, sports—that is, by watching others living at the edge, exercising their knowledge and skills to the limit. Throw in the occasional thrill ride at the theme park, or the once-in-a -lifetime car crash, or the inevitable death in the family, and that will do very nicely, thank you. Architecture, in this case, should give us refuge, sanctuary, protection from the extremes, placing us somewhere in a secure existential ‘middle.'


In today's world, living in the middle has become, for many people, difficult. Living at the edge because of war, natural disasters, loss of homes, jobs, identities, they do not have the advantage of being actors, artists, or athletes trained to push themselves to the limits of their knowledge and skill, and perform at their best on the edge. Only those used to living in poverty, including slum-dwellers, learn somehow to live there, but their daily struggles for survival yield an abject victory at best. For the rest, the indignities of loss mount as they wait to be rescued. We do not often hear the stories of those for whom rescue never comes.


Philippe Petit's walk on a wire between the Twin Towers of the World Trade Center, in August of 1974, tells us a lot about architecture and the edge. He and his team, who illegally penetrated the buildings' security systems and rigged the wire, conceived the two towers as anchor points, stable and sure. Architecture, we believe, endures. Our lives continually moving within and around it are fleeting, ephemeral. It is a very great, but also instructive, irony that, in this case, the architecture did not endure. The towers were brought down by illegal ‘interventions' different from Petit's only in their intent to do harm, and to prove the instability of architecture. Both proved the vulnerability of presumably secure systems—especially the social ones symbolized by architecture—and shifted the focus of public perception and debate to what might be called ‘the endurance of ephemerality' in contemporary worlds driven so often to the edge.


LW


More on Philippe Petit's August 1974 wire-walk:


<http://en.wikipedia.org/wiki/Philippe_Petit>


<http://en.wikipedia.org/wiki/Man_on_Wire>



 ## Comments 
1. lin y.
2.22.10 / 7am


Essentially, I believe most of us have thantophobia, or rather, the fear of death… Architects especially. It is this pursuit for security and longevity that drives one to design conventionally because well, history has proven them to work. And because of this fear of obsolescence, architects are not motivated to do something vastly out of the world. Even the architects we have come to associate with the terms “wild” and “insane” are doing things on a very superficial manner. There are hardly any remarkable construction or social breakthroughs that I can recall. We dwell now as we did before. 


Is that a good thing? Honestly, I don't know. I like the feeling of starting design, knowing that it won't get built or if it did, it will be torn down sooner or later. Architecture does not persist, rather it invents. But then, one gets weighed down by responsibilities… social, political and fiscal…
3. [Dave Irwin](http://organicmobility.com)
2.22.10 / 7pm


Lin, I think that architects do get weighed down by responsibilities such as the ones you have listed . I believe that this is within the built environment due to amount of time and momentum necessary to launch a new architectural typology within society. Time is something that will never move as quick as we might hope in architecture.


 Looking at other disciplines such as sculpture or painting one can begin to see a movement or change in the creative world much quicker. If you look at the Futurist Movement in Milan during the early 20th century small sketches by the young Sant' Elia are the strongest notions toward the architectural goals held dear by this small group. Not to mention the energy within paintings done by other members. It was clear that a new way of thought was culminating in this small group visa the mediums of painting and drawing. 


I feel that in order for a true movement or shift to take place within architecture now, there needs to be a strong creative underpinning in other disciplines that complement the approach to a built work. Perhaps this speaks to the importance of a multidisciplinary emphasis in an architectural education. 


So do architects have a “fear” or stigma attached to the death of their work, yes but I believe that this comes with great reason. Work by sculpture Andy Goldsworthy is a great example. His sculptures are designed to be dismantled by nature whether it be the change in tide or a strong gust of wind. Though his work will not; in their individual states, stand the test of centuries, his process will. This is what we should strive for, a force behind ones work that is timeless by principle even if not in built form. 


Great post Lebbeus, thank you.
5. [Leopold Lambert](http://www.leopoldlambert.com)
3.3.10 / 1pm


Dear Mr. Woods,  

I found your article about the edge extremely interesting in what it was triggering in my mind. I thus wrote something about it on my own blog :  

<http://boiteaoutils.blogspot.com/2010/03/unwall-edge-lebbeus-woods-philippe.html>  

I hope that you won't mind. If there is any issue of credits or something, please feel free to tell me.  

Thank you for your blog that takes the risk of daily enunciation which is not an easy thing.  

Have a good day  

Leopold Lambert
7. [# (UN)WALL /// The Edge – Lebbeus Woods / Philippe Petit | The Funambulist](http://thefunambulist.net/2010/12/23/unwall-the-edge-lebbeus-woods-philippe-petit/)
12.23.10 / 2am


[…] Woods recently wrote a text called The Edge about the necessity of architects to work as tight rope walkers instead of working “within […]
9. [Same place, many years before 9/11 : socks-studio](http://socks-studio.com/2011/09/11/same-place-many-years-before-911/)
9.11.11 / 8am


[…] Philippe Petit‘s 1974 high wire performance between the two towers of the World Trade Center inspired Lebbeus Woods, for his article The Edge: […]
