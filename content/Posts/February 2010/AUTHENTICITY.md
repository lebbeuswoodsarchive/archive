
---
title: AUTHENTICITY
date: 2010-02-06 00:00:00
---

# AUTHENTICITY


On its face, the concept of authenticity is a simple one. A thing or a person is authentic if they emerge from a source of originality. The source might be a unique way of making, in the case of a thing; or it might be from a unique nature of being, in the case of a person. Because things are made by persons– individuals who employ in some manner the ways of making—the character of things and persons are often closely related. The point is that in any case the source of the authentic will be comprised of unique properties and qualities, and not merely those imitated from another source or adopted for effect.


We are all well familiar with the inauthentic, with things that are imitations of something else, and with people who pretend to be what they are not. There are fake Picassos—forgeries of Picasso paintings—just as there are fake Picassos in another sense—persons who pretend to be an original painter by imitating his paintings, sometimes without being aware of doing so. In the case of Giorgio de Chirico, who late in his life imitated his own earlier paintings, there are fake de Chiricos by de Chirico himself. This would also be the case with an architect who makes design sketches for a building he or she has already fully designed. It does not matter what the motive is (often it is to fill out a publication or an exhibition), such sketches are inauthentic, fake, because they were not made in the original struggle to formulate an idea, but rather after the struggle is over.


I realize that by now the ostensibly simple concept of authenticity is beginning to get a bit murky. Is the fake Picasso authentically fake? Is a posturing bore authentically posturing or boring?  I suppose so, in a vernacular sort of way. But the idea of the authentically inauthentic is not useful because it reduces to a game of words dependent on the absence of a ‘source of originality.' The source of a fake is the faker, who by definition is not original. Without originality, authenticity does not exist.


In the present age of artistic appropriation, remixes, and what Walter Benjamin called “mechanical reproduction,” the concept of authenticity is pushed close to the edge of extinction. Indeed, Benjamin argued for the virtue of the inauthentic, art that was mass produced by industrial means and wholly without originality. Prime examples would be mechanically copied prints of paintings, movies, recorded music, as well as the digital images and performances that today flood the internet. In a literal sense these things are only copies of other, original things that they may strongly resemble (depending on the skillfulness of the copy), but which lack authenticity—that belongs only to the thing copied. However, Benjamin argued for the shattering of art's “aura” inspiring awe and reverence for its originality, in order that art could truly belong to the masses of people and no longer to intellectual and social elites who owned it, controlled it, and used its authenticity to claim and to exercise political power. Benjamin's reasoned objection to the concept of authenticity has greatly influenced intellectuals, theoreticians and academics right up to the present. But there is more to the story.


Benjamin's viewpoint was radically socialist or communist. He hoped for the advent of an egalitarian society where the people would control the means of production and each would benefit accordingly. In such a society, everyone would have equal access to art, that is, the art made common by mass production—it would be woven into the everyday fabric of living. The Bauhaus had a similar idea and goal. However, Benjamin, and the Bauhaus founder, Walter Gropius, overestimated the ability of socialism and communism to create such a society, but also underestimated the adaptability of capitalism—its ability to co-opt any idea, including radical egalitarianism, for its own purposes. What Benjamin foresaw without knowing it, and inadvertently contributed to bringing about, was a capitalist society of mass culture that we call consumer society, where everyone who plays the capitalist game has access to products of every kind, including art. The public museum of today is a retail store of art for consumption by the masses of consumers, as much as movie theaters, television, and the internet. The irony is, of course, that elites remain in power, still brandishing their control of original art, not least through the museums (“look but don't touch”). Capitalism has succeeded in creating radical egalitarianism of its particular kind and at the same time reinforced the power of ruling elites.


Architecture has become more popular today than ever before. Its popularity does not come from the ways it improves the everyday lives of most people—as modernists like Gropius once hoped it would—but rather because of the ‘brand names' now associated with its status as a consumer product. In New York City, for example, long the bastion of anonymous, bluntly commercial building ‘boxes,' designer buildings are sprouting everywhere. Tourism, grown to international proportions, has turned the streets of the city into a public museum in which are displayed the works of the most renowned architects. While the masses cannot afford to live or shop in them, their facades—shaped with signature originality—are free for anyone in the streets to see and regard as reverently as they choose, or with whatever awe their brands demand.


Authenticity survives today in a vast landscape of the inauthentic, one largely drained of originality by mass production, mass media, and mass marketing. Authenticity has, as never before, become the luxury of the few.


LW


[![](media/bilbao1a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/bilbao1a1.jpg)


A related post, dealing with the form of inauthenticity called “bad faith”:


<https://lebbeuswoods.wordpress.com/2008/11/23/bad-faith/>



 ## Comments 
1. Kiel
2.7.10 / 9am


On that topic — how did you become involved with the Arthur C. Clarke book illustrations (The Sentinel)? Always struck me as an uncharacteristically daring decision on the publisher's part. And where does illustration lie in terms of authenticity? In your mind.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.7.10 / 4pm
	
	
	Kiel: In the early 80s, a book packager (the creative person who puts a book together and then sells it to a publisher, who simply prints, advertises and distributes it) who was also a good friend of mine—Byron Preiss—asked me to illustrate a special edition of Clarke's Sentinel and other stories. At the time, I was making a living illustrating other architects' building designs (renderings), and the project appealed to me. I made the drawings based only on my interpretations of the stories—no one told me what to draw. In fact, Clarke, who was living in Sri Lanka, objected to one or two of my illustrations, but Byron stood by me and they were published as I drew them. Altogether it was a good experience, but—like my short stint in Hollywood—I decided it wasn't for me—just too far away from architecture. I did two other projects for Byron—one of Zelazny stories and one of Bradbury stories—but that was it. He was a client who deeply respected my work and never interfered with it.
	
	
	As for your larger question about illustration, I see no authenticity problem on the creative end, under the circumstances I experienced. However, the ‘mechanical reproduction' of the illustrations diminishes their authenticity to the degree that the book emerges from an anonymous, non-original, source.
3. Kiel
2.8.10 / 1am


Thank you, Lebbeus. Was completely unaware of your Bradbury & Zelazny works — excited at the prospect of discovering them. 


What are your feelings on the work coming out of the world's top architecture schools? I'm a student at CCA (formerly CCAC), where you've talked, and I worry often about compromises arising in my work to satisfy some instructor's will (whether real or imagined) — which seems to be a blind fixation on compound geometry for compound geometry's sake. Then, I regularly discover that nearly all of my heroes and inspirations had at least a “contentious relationship” with school, if they didn't drop out entirely. It's caused me to question my choice.





	1. Liza
	2.10.10 / 10am
	
	
	First of all – THANK-YOU SO much Mr. Woods for being such a brilliant writer! I have had to wade through TOO much architectural theory that obscures its simplistic premises within a morass of mind-numbing mumbo-jumbo. It is a pleasure to read architectural theory that gives me a visceral thrill as thoroughly as an architectural work can. All art should do this – give someone a visceral thrill.
	
	
	The following comments turned out to be more of a blog, only because your post has so resonated with my life-long struggle with authenticity, that has only become currently compounded with my pursuit of architectural instruction. Much of what lies below is a response to Kiel's experience, in the context of that search for authenticity –
	
	
	Ironic that I should be drawn to reading this post, and the following comments, only to see that I too, struggled with EXACTLY the same dilemma with CCA's MArch 1 program last semester. But I have to say that CCA is only doing what the majority of the”BEST” architecture programs are doing – hyping up the the digital “making” and design process which at this point is leading to, as Kiel said, creating compound geometric architectural skins, ad nauseum, because that is what computers do so well and so quickly. Just plug in an algorithm and some parameters and click Render and you have – insta-building. With this sort of trend, designers sans the mental and physical design skills developed via the manual process, will be outsourced overseas in NO time at all. 
	
	
	But that is another matter altogether. I am not a ludite. I love computers as tools, but I definitely see their limitations. And I definitely see limitations in the thinking processes of those who INSIST that ALL successful designs or processes thereof must follow logically each step of an equation like a geometric proof. I don't believe that is true design, to figure out a “system” and let it just run on … I believe that design needs to be more interactive and arbitrarily specific … that is specific to the arbitrariness of life.
	
	
	And this is where I am delighted with”Authenticity” – and it had me wondering, before reading your comment …. how exactly CAN a student of architecture develop their own authentic expression of talent when they are literally forced to THINK a certain way in order to design “successfully” ? And in thinking and designing the same way – how authentic can we possibly be, especially if we are all pumping out our designs using primarily the computer? I think this is the question that multitudes of creative students before us in all types of fields have struggled with. But for me, in order to be authentic, I need freedom of thought and creation. And this is what graduate school should be – offer you the tools and instruction to pursue one's own muse …is it this anywhere?
	
	
	Me, I had to drop out of CCA after a semester. A very arduous decision-making process preceded this. And it still is difficult not being “safely” ensconced in an institutional setting whose duty it is to convince all of its students of its academic validity.
	
	
	I am now pursuing a more eclectic instruction serving my own authenticity, until I once again pursue a Master's degree. This time in Interior Architecture – a field specialization that is too new (academically) and eclectic(professionally) to have reached dogmatic proportions in the realm of academic instruction. I hope, fingers crossed.
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		2.10.10 / 2pm
		
		
		Liza: I hear you—and am sorry to say I have heard before many similar accounts of disillusionment with contemporary architecture schools and for exactly the same reasons. The computer is a great tool but is definitely being misused as a shortcut to getting supposedly great looking student projects, which in fact are meaningless exercises that all look alike anyway. This approach is promoted for the most part by lazy or incompetent teachers of design who believe, at best, that what the profession of architecture needs is aesthetic technicians. This approach is accepted by the administrators of schools because it is a trend that so many schools are following and which keeps the tuition money rolling in. This is not the place for me to expand on this criticism, so perhaps I need to write another in my Architecture School 101, 102, 201, 202, 301, 302, and 401 series of posts. In any event, I have deep sympathy for your predicament and only hope that it does not drive you out of the field of architecture. We are much in need of intelligent, deeply thoughtful and reflective people who can raise its standards.
		
		
		As for writing, I always strive for clarity of language as a way to get to clarity of thinking, in the first place my own. The use of architectural or other forms of theoretical jargon is like the misuse of the computer in design—it's a quick way to get supposedly great sounding results that are actually meaningless and all alike anyway. It is written by academics for other academics, and not to clarify thinking about concepts or communicate ideas to those authentic souls who actually practice architecture.
5. [Simon's World » Blog Archive » Lebbeus Woods on authenticity](http://cygielski.com/blog/2010/02/07/lebbeus-woods-on-authenticity/)
2.8.10 / 2am


[…] <https://lebbeuswoods.wordpress.com/2010/02/06/authenticity/> […]
7. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
2.8.10 / 8am


@Kiel – I understand your delusion with school all too well. In that regard I always remember the words of the famous comedian writer Mark Twain, which incidentally I posted a couple days ago on my twitter account:


“Never let your schooling interfere with your education”


@Lebbeus – It is an interesting way you define authenticity and link it to reproduction and, as I interpret it, copyright.


Authenticity is primarily a case of True or False, but by linking the authentic to a conceptual value of not only balance between interior and exterior, substance and image (facade), but also process > product you are able to establish the relation between the first and originality.


I also agree with the following reasoning of Capitalism as an adaptable system that uses (usurps) the democratization of culture (as a product of process) to further establish itself and the elites. This cultural oligarchy of “art” creating elites then collect from the masses for the consumption of such content ironically leading to the subversion of the term itself.


What I disagree upon is, to some extent, the conclusion you derive from the pressupositions. Based on the concept of inauthenticuty as a result of reproduction, then what this oligarchy offers is not authenctic.


Can we then still call it culture?


I would risk stating the issue as the success of the, so called, elites in mystifying the Inauthentic content they provide as Authentic. Therefore creating a whole consumer market based on lies and false products (which they even deem fit to charge us for multiple times).


Thoughts?
9. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
2.8.10 / 8am


@ kiel – LOL! I meant “disillusion” not “delusion”… sorry about that 😉
11. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
2.8.10 / 8am


@Lebbeus – Also, I would be very interested in perusing those illustrations you created for those books/stories. Can you point me in the right direction?
13. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2010/02/06/authenticity/)
2.8.10 / 9am


**Social comments and analytics for this post…**


This post was mentioned on Twitter by ethel\_baraona: AUTHENTICITY by Lebbeus Woods | <http://tinyurl.com/ykw33nn>…
15. Mark Keller
2.8.10 / 9pm


Nuno Raphael:


I think we can fault both the elite mystics and their transfixed masses. The issue of the original content being authentic or not to me is somewhat moot, as the real problem resides in the stratification and hierarchical dissemination of artifice, leading to our concept of branding. 


That which is idolized and elite may be authentic, but that which is authentic is not always elite. If more people became creators of these brand items (buildings, clothes, etc) rather than consumers, the hierarchy of the inauthentic would perhaps be disrupted.





	1. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
	2.9.10 / 9am
	
	
	Well certainly, no doubt the masses should convert from a consumer paradigm towards one of participation, this is to mean not only take advantage of existing content by also create it for others.
	
	
	The internet is actually encouraging this by enabling the meekest of users to access the tools needed for development.
	
	
	What I meant is that by controlling a something that is made and labelling it as “Quality Content” (and then charging for it) the elites passively hinder the masses from realizing they too can be content creators (even outside of a return-based economy).
17. [chris teeter](http://www.metamechanics.com)
2.9.10 / 1am


Kiel stick with it. Both in undergrad and grad I had been threatened to be expelled from a studio or two, mainly due to my lack of respect for my profs version of architecture. I have also been more or less fired for lack of respect. 


One day when I teach studio I will let students discover themselves, and when I'm a chair of a program my school will infuriate the rest of schools with overwhelming openess to students agendas confronted by real world problems. One day of course, assuming no one in power reads this post and intends on denying…


But then again Kiel I piss people off and keep on truckin'…its your version and its a mainly free country.


Lebbeus you could replace art with religion and have quite the same scenario with some slight differences.


What does it mean to authentically know god? Moreover is your god authentic? If everyone believes in god is it the true god? Or if you are the complete outsider are you the one true believer in the one authentic god?


I'd like to hear your version on authentic religion assuming religion can be authentic? (If art is I don't see why religion couldn't be authentic)





	1. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
	2.9.10 / 9am
	
	
	Your goals seem perfectly just.
	
	
	However, the fact that you seem to respond to your teachers with conflict and disrespect makes you no better than them.
	
	
	My point, by quoting Mark Twain, was exactly to play by their rules (maybe with some tentative conceptual defiant models occasionally) while doing academic works (because they do not matter anyway) and once you get your say THEN try to change how things are perceived.
	
	
	One can never complain of disrespect if he answers with disrespect as well.
	3. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.10.10 / 5pm
	
	
	chris teeter: Friedrich Nietzsche said that “there was only one true Christian, and he died on the cross.” 
	
	
	I'm an agnostic and a bit of an existentialist (existence precedes essence), which in this case means that I stay away from religion—indeed, from all metaphysics—except when its concepts have some demonstrable bearing on historical or contemporary thought, such as I mention in my [Earthquake (again)!](https://lebbeuswoods.wordpress.com/2010/01/13/earthquake-again/) post. Your questions are important, but I'm really not able to address them.
19. [Chris Teeter](http://www.metamechanics.com)
2.9.10 / 5pm


what's disrespect though is really the question? should you intellectually and very formally tell your professor his version of architecture is bunk, or just get straight to the point? (former being playing by there rules)


authentic disrespect anyone?
21. [Kenneth Howe Jones](http://www.poulsbohemian.com)
2.10.10 / 2am


Leb: I'm curious. Just what was it that got you thinking specifically about authenticity?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.10.10 / 4pm
	
	
	Ken: As you may remember from the long ago days of Environmental Theater, I've always felt that the issues of ethics and those of form are tightly intertwined, really inseparable. The concept of authenticity is clearly one that addresses their relationship. I suppose the prevailing trends in architecture today, which free form from any sort of social or even personal commitment (see my comment to Liza here), prompts my thinking about authenticity. It is a concept very much under attack by theorists today. All the more reason to examine it.
23. [Kenneth Howe Jones](http://www.poulsbohemian.com)
2.10.10 / 9pm


Leb: Ah yes. Definitely one of your most eloquent premises. You've had me restling with that one for decades now. Thanks for the context. Keep up the good fight.


Regarding other thoughts expressed here, remember Nietzsche also said “One must still have chaos in oneself to be able to give birth to a dancing star.”
25. Kiel
2.11.10 / 12am


Liza, when abouts was your CCA adventure? Any teachers stand out? 


I've tried to clarify the problem in my head: good design is not impossible, but made extraordinarily difficult by abundance. Put another way, an economy of materials and “options” gives focus — therefore a greater investment of thought — to design. As the cyber-arsenal makes just about everything instantly available at a keystroke, we tend to indulge everything, rather than orchestrating space with the poetry of restraint granted innately by, say, a pencil and paper. 


Think of the time when studios were like this:


[![](https://i0.wp.com/www.som.com/resources/content/5/0/4/3/7/8/6/3/images/002_21605698.jpg)](http://www.som.com/resources/content/5/0/4/3/7/8/6/3/images/002_21605698.jpg)


Nothing like it today.


Lebbeus, do any architects of the past occupy your definition of authentic? I really upset my teacher last semester by choosing Mario Botta as a “precedent”. He tried to conceal his disdain, and failed.
27. [Oliver](http://dronelab.net)
2.11.10 / 6pm


Imitation can be a valuable tool in acquiring the necessary skills for future original artwork. True, you can do both – improving technique by putting unique ideas into form – but that is more than one man can chew, especially at young age and in a fast-paced environment where ideas are not allowed to ripe. Both are present at the generic architecture school of today.


Before you can be a Picasso you must learn to draw. Picasso himself started off by copying renaissance artists. I understand that this is slightly different from your thoughts on the matter but one should not disqualify the potential of inauthenticity. Skill doesn't come easy. It takes years of labor before you have something of a foundation – a tool, language, call it what you want – on which you can live out your truly authentic self.
29. [chris teeter](http://www.metamechanics.com)
2.12.10 / 12am


Liza intertior architecure is about 75 percent of what we architects/designers do in NY. Unlike architecure, interior architecture requires a visceral understanding of lighting and materials. Granted minimalism can be nice, minimalism often is the proud and famous “architects” cop out of really commiting time to developing a great space. See too nice of an interior masks the ever important abstract concept based on whatever mumbo jumbo.


I actually teach 3dsMax and because as noted above I lack respect I can do say: Gehry in 15 minutes, Zaha in 10, SOM in 5…what I mean by that in a very cynical manner is much of what you see today as hot architecture is simply someone finding a way to exploit a new function/technique in a software, as noted above quickly.


Don't get me wrong, Gehry developed his designs without a computer, Zaha was painting her renderings before computers, and SOM that's just good solid engineering…the point is and I wish all who use the computer understand this-  

Its a tool to express your design and perhaps develop your design, it isn't design. Don't let technique drag your design down or dictate it.


I can't draw very well, I lack the patience to make a pencil do what I envision, but like I said I can do Gehry in 15 minutes…


Hopefully at this point in architecture schools, now that students are essentially computer programmers, we have exhausted our obsession with design by tool and we get back to actually designing. And I believe the 70s exhausted the intellectual concept of architecture.


Interior architecture is a start, seriosly.
31. Kiel
2.12.10 / 1am


I can draw very well, which is perhaps my problem. You couldn't, for example, do Lebbeus in 15 minutes.
33. Kiel
2.12.10 / 1am


I can't agree that the 70's exhausted “the intellectual concept” of architecture — how many numbers can be found between one and zero? There's always the genius-thought waiting, patiently, for articulation. I'm struck that we still haven't fulfilled Rohe's 1921 Friedrichstrasse goal — a single, unmullioned skin of glass. Maybe nanotechnology will enable it.


[![](https://i0.wp.com/farm3.static.flickr.com/2703/4347776830_083f572c1a_o.png)](http://farm3.static.flickr.com/2703/4347776830_083f572c1a_o.png)
35. adamwiseman
2.12.10 / 4pm


Very interesting post, Lebbeus, as per usual. 


I have to offer commiserating words to the students whose dissolution with the architecture academy has lead them to pursue other fields, or has frozen them in a state of indecision about their future, but from a slightly different perspective: I taught (part-time) studio at the University of Kentucky for two years, until last year when new personnel hired by our dean in his sophomore year, forced myself and three other instructors out. Our replacements were people who had experience working in starchitecture firms and/or with portfolios heavy in “theoretical” digital works. Besides losing a job I enjoyed, what distresses me the most in regards to academic authenticity, and our pursuit as architects, is the bland and blatant globalization of design (exacerbated by the lack of both freedom and rigor that most digital studios fall into), and the lack of humanist issues in teaching. This shift, however belated it was in reaching the University of Kentucky, was inevitable–when the College of Design was vetting this dean, the opening slide at his first presentation, owing a little too much to Rem, read: GLOBALIZATION. 


The emptying out of phenomenological tectonics and subsequently, the primordial joy of tactility felt from making by hand, is unfortunately tied to the squandering of enthusiastic young minds for the purpose of creating lackluster architecture infected by the techniques and propaganda of global big business. Students are being persuaded to create images that could very easily be distributed to investors, but without the understanding of space, texture, light and material. All too often, any intellectual content is absent. I believe that it is important for schools to not lose the understanding (and directional focus) that architecture's making is poetically tied to existence, not the idolization of technique to satisfy the agendas of people or organizations that have only their own interests at hand. 


So, to the students who have not given up yet, I say, keep tinkering–use as many different methods of making you can to strengthen an idea and put it out there. The worst thing we can allow to occur is the extinguishing of poetic inspiration because an instructor feels it is not “sexy” or “fashionable” enough.
37. martin
2.12.10 / 5pm


while it pains me to leap onto the back of an overloaded comment board….


there is a fascinating article in the nytimes about a young german woman who wrote a book that she considers a sort of “mash-up” of other peoples work. its been very well regarded and criticized immensely. I wonder, Mr. Woods, what you think about fair use and authenticity in a case like this? 


<http://www.nytimes.com/2010/02/12/world/europe/12germany.html?hp>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.13.10 / 2pm
	
	
	martin: I haven't read the novels mentioned in the NYT article, but they belong to a category of creations I only mention in the post. This is the literary equivalent of what in visual art is considered ‘collage.' It has long been accepted that, for example, Max Ernst's collages made entirely from cut-up 19th century book illustrations, are original art—and I agree that they are. Ernst created something new in meaning from something old and ‘found', yet relied on the meaning of the older work for the new meaning, which was a form of irony that resonates with modern sensibility and notions of truth. In other words, this form of borrowing by one artist from another is authentic only within narrow limits. If it is done to achieve the same or similar meaning, then it is simply stealing.
	
	
	Fair use applies to using another person's copyrighted work, without their permission, for educational or research purposes only. If it's used to make money, then it's illegal and a matter for the courts.
	
	
	
	
	
		1. [Nuno Raphael Relvão](http://twitter.com/UnfoldedOrigami)
		2.13.10 / 4pm
		
		
		@Lebbeus
		
		
		In that regard, and I know I am not saying anything new, the quality of Authenticity is in the process, not in the finished product. Do you agree?
39. [chris teeter](http://www.metamechanics.com)
2.13.10 / 3am


Kiel I actually respect lebbeus, and I have thought about recreating his renderings, speaking of inauthentic…I will tell you it would take considerable more effort and time to get it down to a science and even then imitating hand with a computer has its limits… 


Someone mentioned imitating as a good way of learning, well since the computer is a mathematical machine of objective meaningless facts and actions, and extremely flexible in logic, (a mirror of the pure rational mind), recreating steps for creating product, whether art or spreadsheets, is an easy thing to do if you can breakdown the piece you are imitating ontologically from a first person perspective. What imitating other's works means existentially is getting into their stream of conscioussness, an unguarded conscioussness, otherwise if someone knew you were trying to imitate them they would rattle you with riddles, which of course given your ability to read social actions in a mathematical objective meaningless string of facts and action may be easy for you… 


I still prefer guns ‘n roses “knocking on heavens doors” over clapton or dylan…sometimes the inauthentic imitation is better than the real thing…


And frankly in a world of analog creation, the physical world, all actions are authentic. Only in a world of intellectual virtual ideology, digital?!?, does the concept of inauthentic begin to become meaningful.


And this is why this particular blog post is a magnet to readers with responses and questions on truth of the existential situation of say a univeristy of kentucky and academia and a career and most importantly architecure.


While the 70s architects were intellectuallizing our parents were getting psychedelic..


I think psychedelic would be more fun, personally. 


Technical construction dwgs meats fairies with magic dush, flashing flash dancing on its way to yellow brick pavers supports the tin man tuning the AC roof unit…my opinion go with a W24x55 in that case.
41. [Randy Nishimura](http://www.sworegonarchitect.blogspot.com)
2.14.10 / 7am


Lebbeus: I too have pondered the concept of authenticity as it applies to the creation of architecture (<http://sworegonarchitect.blogspot.com/2008/05/authenticity.html>) but from a slightly different perspective than yours. Rather than view authenticity as it applies to originality or the unique way of making a thing, my understanding of authenticity is derived from existentialist philosophy and the imperative to be true to one's own nature and beliefs. Fundamentally, it may not be possible for architecture to be both truly authentic and socially or culturally responsive at the same time because architects operate at the whim of forces external to the process of creation.
43. [Cruz](http://www.wai-architecture.com)
2.14.10 / 7pm


On the one hand I could agree with a not so optimistic view of authenticity in which a selected few control the real authenticities, but on the other, I think that this society is experiencing a massive shift in the domain of the authentic. And just like we are experiencing with the economy, every time the “selected few” have less control over how things develop in a world less constrained by the usually controlling entities. 


The tools that have become available with technology have created a sort of distorted apotheosis of the society that Constant was looking for in his New Babylon; a society in which everybody could freely manifest its creativity.  

With all this rendering softwares like Photoshop—in the case of designers, and “freedom propaganda tools” like facebook, youtube, myspace, the public domain has gained a spotlight I doubt existed before. And even when architecture is still an elitist game for the powerful and the well-educated, maybe it's a matter of years when the wave of freedom of media takes over and challenges the establishment. 


For now the only thing that seems to be protecting the “high arts” from this phenomenon is a protectionism that gives more value to the experience and pedigree than to the actual production of innovative ideas.
45. [The End of the Classical: Is ‘Re-' the new ‘New' « Rahul Sen: Future-Sense](http://www.future-sense.net/2010/03/06/the-end-of-the-classical-is-re-the-new-new/)
3.6.10 / 9am


[…] article titled ‘Authenticity' from the blog of Lebbeus […]
47. [Gary Winship](http://www.reconstruct-art.com)
9.9.10 / 10pm


Without originality, authenticity does not exist.  

As an artist I am constantly asking myself what is new by questioning the authenticity of originality.
