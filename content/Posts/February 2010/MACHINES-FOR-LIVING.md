
---
title: MACHINES FOR LIVING
date: 2010-02-27 00:00:00
---

# MACHINES FOR LIVING


[![](media/lwblog-yendo-41.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-41.jpg)


*(above) a Living Unit, designed by Masahiko (Mas) Yendo.*


Mas Yendo thinks of machines and the industrial age that spawned them not so much skeptically, as epically. Machines get old. They break down, are discarded, and usually end up in trash dumps. Epochs get old, too. They burn out and the ideas that drove them are pushed aside by new ideas and ideals. Still, the epic lives of things and ideas are more complicated, in that they never die but are merely transformed from one phase-state into another. Some theorists of history, such as Giambattista Vico, Oswald Spengler, and W. B. Yeats, have claimed that its cycles follow ever-recurring patterns over great spans of time. In the realm of material things, even the most conservative scientists would agree with the “law of the conservation of matter and energy,” which states that matter can never be destroyed, but only converted to other material forms or to energy, then, eventually, back again—the balance of matter and energy in the universe remains constant. For Yendo, all of this comes down to a simply stated fact: machines must be recycled. His projects envision a re-use of industrial hardware and a reconfiguration of its purposes.


However, it must be said that he is no cheerful, upbeat environmentalist. The need for recycling in his world is not an option but a necessity. The people who inhabit it are struggling for survival, pushed to the edge of extinction by political or ecological or economic disaster. Their houses, or ‘living units,' are re-assemblages of machines and other industrial products and are intended to provide refuge and sanctuary from hostile environments. They are somewhere between space capsules and ad hoc favela houses, hybrids of high and low, in any event an architecture of extremes.


Extremes, unfortunately, tend to isolate people from one another. Social systems do not work well at extremes and people end up having to fend for themselves. I experienced this in Sarajevo during the siege. Without water, heat, electricity, telephones, food, and without effective government to supply these vital things, people spent their days scavenging wood, candles, bits of black-market canned goods, and carrying containers of water long distances from a few sources—often under the threat of shelling and snipers—strictly for themselves and their families. Yet, even in the darkest days of that extreme, people had a strong sense of community—they were all suffering the same deprivations and knew it. Yendo's living units take us a step further, to an entrenchment of isolation. No sense of community remains. Or almost none. The skills required to make the living units are surely those of more than one person. In Yendo's world, some cooperation between people survives.


And, in fact, when we get to the “Hole in Water” project, cooperation is necessarily institutionalized, in order to save Sagami Bay from deadly pollution, either residual or continuing. These floating structures are inhabited machines designed to accrete the sea-life that will naturally consume pollutants—a process materialized beyond solo survival. A community has reformed, post-catastrophe. It has learned its lesson.


Yendo's vision is a dark one, to be sure, but not hopeless or ultimately pessimistic. In extreme circumstances, people call on their reserves of resourcefulness and inventiveness not only to pull themselves through but also to put a uniquely human stamp on their place and time. As long as this goes on, there is hope for civilization.


LW


“UL-9205,” self-sustaining Urban Living Unit:


[![](media/lwblog-yendo-83.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-83.jpg)


[![](media/lwblog-yendo-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-9.jpg)


[![](media/lwblog-yendo-9b-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-9b-21.jpg)


[![](media/lwblog-yendo-71.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-71.jpg)


“UL-9304,” self-sustaining Urban Living Unit, with solar panel:


[![](media/lwblog-yendo-12b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-12b.jpg)


[![](media/lwblog-yendo-51.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-51.jpg)


“B1-9004, Hole in Water,” artificial reef structure:


Sagami Bay, Japan, with artificial reef structures:


[![](media/lwblog-yendo-11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-11a.jpg)


[![](media/lwblog-yendo-62.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-62.jpg)


Habitats, of which Yendo has said, “What I speculate in my work is that new technologies will play an important part in how architects embrace new ideas. The advancement of seemingly unrelated sciences such as biochemical engineering, coupled with the growing consciousness of environmental issues, furthered by the development of mechanical design, must serve to inspire creativity”:


[![](media/lwblog-yendo-23.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-23.jpg)


[![](media/lwblog-yendo-32.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-32.jpg)


[![](media/lwblog-yendo-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwblog-yendo-11.jpg)


A monograph on the work of Mas Yendo:


<http://www.amazon.com/Ironic-Diversion-RIEAeuropa-Book-Yendo/dp/3211834923/ref=sr_1_1?ie=UTF8&s=books&qid=1267284494&sr=8-1>



 ## Comments 
1. Kiel
2.28.10 / 3am


Wonderful, more than vaguely reminiscent of Kow Yokoyama.
3. Pedro Esteban Galindo Landeira
3.1.10 / 5am


An excellent work. A great perspective for today's life.


I see this like the constructivism whitout revolutionary aims. Or like an apocaliptyc view of them. 


Where I can find some images from the constructivism on the web? Really good images. Thanks.
5. [spencer](http://graynol.blogspot.com/)
3.8.10 / 3pm


Wonderful drawings. I agree that the thread of constructivism is apparent. Thank you for sharing.
7. Pedro Esteban Galindo Landeira
3.14.10 / 5pm


It's true that those structures look like the end of everything, but in some way they look encouraging, those machines are like the persistence of the humankind.
9. Pedro Esteban Galindo Landeira
3.14.10 / 5pm


Go to a war, wow lebbeus this is amazing, how wonderfull experience, that is work at the extreme, I know your project for Sarajevo, I suppose your work has change a lot after that experience.


Please do yo have some book translate to spanish? Please if you have give me the link to buy it.
11. [Intrepid Engine » Blog Archive » Machines For Living](http://intrepidengine.com/?p=270)
3.15.10 / 10am


[…] feel compelled to post this image without too much comment. Those who wish to learn more can go to Lebbeus Woods blog post, but from what I can gather it's a design for a living habitat for homeless people made from […]
13. [MACHINES FOR LIVING – D-503](http://d-503.com/2011/02/01/machines-for-living/)
3.17.11 / 9am


[…] <https://lebbeuswoods.wordpress.com/2010/02/27/machines-for-living/> […]
15. [Mas Yendo: Machines for Living in The End of Times. : socks-studio](http://socks-studio.com/2012/08/19/mas-yendo-machines-for-living-in-the-end-of-times/)
8.19.12 / 10am


[…] Lebbeus Woods var addedComment = function(response) { //console.log('fbComments: Caught added comment'); […]
