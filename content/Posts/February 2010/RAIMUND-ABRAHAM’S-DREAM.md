
---
title: RAIMUND ABRAHAM’S DREAM
date: 2010-02-14 00:00:00
---

# RAIMUND ABRAHAM'S DREAM


[![](media/radream1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream1.jpg)


HOUSE FOR EUCLID, 1983:


[![](media/radream2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream2.jpg)


Corner detail:


[![](media/radream31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream31.jpg)


Projection of fragments:


[![](media/radream41.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream41.jpg)


The Cube, model:


[![](media/radream51.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream51.jpg)


Kinetic transformation, model:


[![](media/radream61.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream61.jpg)


THE LAST HOUSE, 1984:


[![](media/radream7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/radream7.jpg)


**RAIMUND ABRAHAM**


(The following are answers by LW to questions asked by a journalist about the works of Raimund Abraham)


*How long have you known Mr. Abraham (or his work) and in what capacity?*


Rather belatedly, I became aware of his work in the late seventies, when I saw his drawings in an important exhibition at Leo Castelli's in New York. I was struck by their density and mysterious quality that seemed to speak of another idea of architecture than any I had seen. He had a widespread reputation for being difficult and uncompromising. Everyone spoke of him in hushed tones, very seriously and respectfully. I didn't meet him until the late eighties, when John Hejduk invited me to teach a design studio at the Cooper Union. Raimund and I didn't get along so well in the beginning. He and Hejduk were the most important teachers in the school. As a teacher, I looked, listened, and learned by their examples how to teach in my own way. Introducing Raimund at a lecture some years later, I called him the “teacher of the teachers,” which he certainly was, though I'm sure that was not his goal. He took teaching seriously because it engaged ideas in terms of architecture. In reviews of student work, he was spontaneous, articulate, and above all creative. His guiding principle was that the critic had to participate in the creative task of the student's project by interpreting the drawings, models, texts. It was not good enough to sit safely back and pepper the student with questions, which is what most critics did. Anyway, we eventually became friends, and I came to know his basic warmth and generosity. These qualities coexist in a paradoxical balance with the often more public stubbornness and irascibility.



*What do you find most interesting about Mr. Abraham's “conceptual” work and his drawings? What can other architects learn from it? How can the unbuilt, drawn work contribute to the built realm of architecture?*


I'm curious why you choose to put the word conceptual in quotes, as though his entire oeuvre were not conceptual.


First of all, his works—drawn and built—are always visually compelling. Regarding his exploratory drawings, one is immediately struck by their sensuality, their tactility, their originality. They are the very opposite of drawings that attempt to be objective or coolly professional. There are something highly personal, which is very unusual for the drawings of an architect, and this makes them a little frightening. At the same time, they address ideas that allude to the universal, in his use of archetypes—square, cube, circle, sphere, point, line, and plane–for example. The interplay between these supposed extremes creates an inner tension, an existential, dialectical, ultimately tectonic, that is, a constructed, idea of place, time, the world.


Such drawings (and many he has made are in this category) cannot be directly translated into buildings, nor, I imagine, are they intended to. They are not prescriptive and illustrative of some next step, but formulations of principles, grammar, methods of thinking and working. They have much to teach in an explicit way, as I think they have taught architects like Ando, but are impossible to imitate. They have the best kind of influence in that they challenge other architects to find their own integrity, while at the same time showing that this can be achieved in architectural terms.


*Are the architectural drawings by Mr. Abraham art or architecture or a hybrid of the two?*



This is a misleading question. It takes us into either-or debates which really have nothing to do with his work, drawn or otherwise. In my view his drawings are essentially philosophical, in that they struggle with questions of existence and its meaning. What makes them architecture—or, I should say, Abraham's architecture–is that they create clear relationships between abstract, tectonic space and form and human experiences and conditions that comprise our existence. As he has said on many occasions, “architecture must confront a program,” which I take to mean a program for inhabiting particular spaces and their contexts.



*Can you see traces of Mr. Abraham's drawn work in the Austrian Cultural Forum building in midtown? If so, has he translated the drawings into something built and does the translation work, in your opinion? If the building is very different from the conceptual work, how is it different and why do you think it's different?*



More than traces, but not so much in the way you imply with your questions. In looking at this building, I'm not interested in how bits and pieces of his exploratory drawings did or did not get built. With other architects, whose drawings are prescriptive of what is to come, that is always a crucial question. With this building and this architect, the question is, did he apply his principles in the design of the building? For Raimund, I believe, a drawing that examines ideas in the ways I've mentioned is its own world. It does not need to be justified or legitimated by any further action. Designing a large and complex building in a congested urban site is a different task, one, however, that will succeed in the architect's terms only if it embodies the highest standards of his thought and work, which will necessarily be embodied differently than in the drawings. It is to Raimund's great credit that he understands the difference between the two very difficult tasks. To get his design concept built, he had to fight for it over many years, overcoming incredible political resistance to its originality. He fought not in the petulant way of an architect who is not getting what he wants, but in a wise and steady way, in a sense with great patience and perseverance. He understood the process, accepted its rules of engagement and within their limitations worked with the same vision, toughness, and personal commitment that he has brought to his drawings. The building as it stands is a great building, a work of architecture, and one of the very few in this vast landscape of generic buildings. It exhibits all the qualities of his best drawings, including that visually compelling, mysterious and rather frightening quality of the personal confronting the universal.


[AUSTRIAN CULTURAL FORUM](http://en.wikipedia.org/wiki/Austrian_Cultural_Forum_New_York), Manhattan. Design competition won, 1992; building completed 2002:


[![](media/lwb-ra-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwb-ra-7a.jpg)


[![](media/lwb-ra-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/02/lwb-ra-8a.jpg)



 ## Comments 
1. chaldea
2.15.10 / 12am


quite harsh on the interviewer.
3. [Chris Teeter](http://www.metamechanics.com)
2.17.10 / 2am


so at 50 he was remembering his dreams as such, I think M.C. Escher did that as well.


Raimund Abraham's “House with two Horizon's” and Prescott Cohen's “Torus” house have alwas seemed very similar to me….perhaps the unbelievable geometry.
5. Chest Rockwell
2.26.10 / 3am


I've always liked Abraham's platonic and tectonic sensibility for architecture…a complete 180 over Lebbeus' confounding elaborateness. Like he once told me at a critique:


“Complexity is seduction…creativity is reduction”.


What is Abraham up to these days? I hope he's not spending his time sunbathing in Cuba.


Btw, do you think 12 Monkeys holds a candle to La Jetee?
7. aes
2.26.10 / 5pm


i've always gotten a kick out of the Jim Stirling cameo. to me it's a perfect metaphor for much of Abraham's work– what at first appears to be conceptually (and often literally) monolithic, is at closer inspection embedded with hidden counterpoints, subversions, and ironies. (in this sense i might characterize his work as neither complexity nor reduction, but compaction.)
9. [Portrait of the Artist as the Books He's Loved | HTMLGIANT](http://htmlgiant.com/word-spaces/portrait-of-the-artist-as-the-books-hes-loved/)
10.12.11 / 12am


[…] A random tumblr had posted an image of his which lead back to Lebbeus Woods's blog, which had a post on Abraham (it's worth noting that Woods is a brilliant architect himself). Another quick & […]
11. Pedro Esteban
10.20.11 / 3am


el edificio-dibujo/ the drawing-building


Is enter into paper…
13. [metamechanics](http://gravatar.com/metamechanics)
1.18.12 / 3am


oh i do remember this, the last image of the last house is one of Abrahams drawings i like to show rendering students.
