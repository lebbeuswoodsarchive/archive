
---
title: ARCHITECTURE SCHOOL 101
date: 2009-01-28 00:00:00 
tags: 
    - architecture
    - education
    - school
---

# ARCHITECTURE SCHOOL 101



A school is—before all else—a faculty.


 It is obvious that without a faculty, a school could not exist, for there would be no one to teach the students who come to a school. Also, the better students, those who are most eager to learn, most ambitious for themselves, and most demanding, those, in short, with the most potential for becoming good architects, select a school partly because of its faculty—they understand well the dynamics of learning.


It follows that, without a good faculty, a good school cannot exist. A mediocre faculty can only create a mediocre school, never a good one, regardless of how much potential its students have. Only a great faculty can produce a great school, and it does so by helping students realize their full potential.


There are two aspects of a great faculty (let us put aside mediocre and say that good is fine, but why not calibrate higher?): they are very effective teachers, and they have active peer relationships. The latter refers to the exchanges they have with other teachers and critics within their school and to their creative activities outside the school, in the big, wide world of ideas and work. Peers demand of each other, first of all, a high level of dedication to architecture, meaning a high level of seriousness. A peer is an equal. No one who is serious (even playfully so) wants to waste time with anyone who is not. There is always a certain amount of competitiveness among peers, and not just for position. The true competition is for achievement: as teachers and as architects. Creative rivalry and intellectual disputation are good, even noble, forms of competition, and are to be encouraged, and appreciated.


However, creative achievement does not necessarily make an architect an effective teacher. Teaching requires several qualities operating in parallel.


The first is *having something to teac**h*. An architect, or anyone else, who wants to instruct young people should feel strongly about what they know and have an equally strong desire to communicate it with others, particularly aspiring architects.


The second is *a commitment of time and energy* to teaching. Dipping in and out of a studio or seminar in distracted bits of time stolen from a busy career is no commitment. Teaching cannot simply be a line-item on one's CV. A teacher must spend “quality time' with students, that is, being personally, fully engaged in the time—of whatever duration—he or she is with students.


 Third, a teacher must understand *the difference between training and education*. The term ‘training architects' is an oxymoron. The trans-disciplinary nature of architecture, the depth and diversity of knowledge it requires, as well the complexity of integrating this knowledge into a broad understanding that can be called upon at any moment to design a building or project, goes far beyond what anyone can be trained to do. Still, some teachers try to train students, using all the finesse of training dogs. Even those who disclaim rote learning and ‘copy me' methods can carry vestiges of attitude that amount to the same. A good test is whether the students' work in a design studio is diverse and individual, or is similar or even looks like the personal work—the ‘design style'—of the teacher. The best teachers preside over the flourishing of individuals and their ideas, and the resulting diversity. Diversity is the essence of *education.*


Schools of architecture must require of students that they pursue in some depth a broad range of subjects. This is because architecture is the most comprehensive field of knowledge one can enter. It engages the whole of society, and must be informed by a society's knowledge, practices and values. Philosophy comes first, as it provides a framework for ordering all the diverse bits and pieces. Then come the social sciences, literature and poetry, and art. These studies happen together with architecture and engineering courses, and, ideally, coalesce in the design studio. It is the task of the studio teacher to set up projects and programs that enable this coalescence—far from easy. To accomplish it, a teacher must have the requisite knowledge himself or herself, and an almost uncanny ability to state in plain language a problem, lay out a methodical series of scheduled steps leading to an articulated and attainable goal. It is up to the teacher to make sure the intended work is actually accomplished within the given time. There is nothing more discouraging and dispiriting than work left unfinished.


Not least in importance is the study of history. Knowledge of the histories of the many communities we share today in global society, as well as the history of architecture, towns, and cities, is crucial. Goethe said, *“The best part of history is that it inspires us.”* He was right. When we see what people have been able to achieve in the past, we realize that we can do the same, in our own inevitably different terms. Without a strong sense of this *spirit* of history, an architect can only drift with the currents of the moment. It is the responsibility of studio teachers to make this clear.


 


Students are the other half of any school's story.


Without good students, a good school cannot exist. However, it is much easier to find good students than good faculty. It is far easier to find great students than great faculty. As Raimund Abraham once said, “There are no bad students.” What he meant was that young people who aspire to become architects and have gone through an admissions and selection process have demonstrated in advance a potential that should be respected. If students try and yet do not do really good work, it is, with few exceptions, due to the failure of their teachers. In contrast, many architects who become and remain teachers do so for reasons other than their potential as teachers. There are many—competent professionals—who should never be allowed any contact with young, eager students bristling with talent and ambition. Bad teachers, especially those who imagine themselves as good, do irreparable damage. They kill the spirit.



This does not mean that outstanding architects cannot emerge from mediocre schools—they can, and some have. But their being outstanding is more the result of their own drive to learn and develop, *in spite of* the mediocrity around them in school. They are, in effect, self-taught. However, even the most self-determined students need some help along the way: the encounter with a rare teacher who stirs their imaginations, ignites their passions about an idea, or sets an example by the teacher's own knowledge, integrity, and dedication. These are the qualities that describe the entire faculties of great schools.


This brings us to the other half of any school's story. Yes, there are three halves. The third is a school's administration, its dean and department chairs….





 *(to be continued)*


LW



#architecture #education #school
 ## Comments 
1. earlstvincent
1.28.09 / 8pm


Looking forward to the next part. Great post.
3. paulo miyada
1.29.09 / 11am


hello. well, maybe it will appear in the next part, but I would ask you about the ideological position of teachers and school's administration. Because the way the opinion and position about the professional role and the ‘architectural ethics' (if we can call it so) appear and are discussed during someone's studies may be decisive for their professional and civic position. Of course this can always be discussed under philosophy and social studies, but it goes beyond this moments, and is present also in studios. I think a great faculty is one that makes this discussion always clear and present to be discussed, whatever their opinions are.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
1.29.09 / 4pm


paulo miyada: Ethics is a crucial topic, which is why I have spent so much time and space on this blog discussing it. In schools, the teachers should address this topic frequently. If they fail to do so, it is the responsibility of the students to raise their questions within the whole class.
7. peridotlogism
2.2.09 / 6pm


LW – what is the goal of a ‘great' school of design education?
9. [lebbeuswoods](http://www.lebbeuswoods.net)
2.2.09 / 10pm


peridotlogism: The answer to your question will appear in a forthcoming post, (probably) entitled Architecture School 201. To give you a broad answer, I would say a great school of architecture is one that educates architects to go out into the world and do all they can to change it in ways they think it should be changed—and gives them the tools to do it.
11. martin
2.2.09 / 10pm


how serendipitous. i just finished applying to graduate school this afternoon.


what you are describing here, which seems fairly accurate, is nearly impossible to pin down. it can be incredibly difficult for an applicant to understand the full breadth of a school, short of spending some significant time with/at/around/inside the program in question. advice from graduates or current students, no matter how detailed, doesn't always cover all the bases of the quality of the school, reviews always seem superficial, information on the web is too vague, and *current* course descriptions are elusive at best.


in my experience in undergraduate school (penn state) i realized that the true dynamics and quality of the program weren't revealed to me until well into my 4th year.


i guess my question is: how can an applicant truly evaluate a school's quality as you have described it?


by the way, for which school are you currently working (if any) ? i believe your name has come up on at least 3 faculty lists that i have seen..
13. [nina barbuto](http://ninamariebarbuto.blogspot.com)
2.3.09 / 9am


thank you Lebbeus. I completely agree. I went on to grad school with the sole purpose of one day teaching architecture. I am looking for some place now where I can truly bring excitement and exploration back to the studio. Architecture school and teachers shouldn't be about their CV, as we now how it is now with teachers jet setting in and out and forgetting their students names let alone projects. What recommendations do you have for people like me who want to teach as much as learn from students? and ps. do you know of any one hiring now?
15. peridotlogism
2.3.09 / 6pm


I'll respond further on that post then. Thanks!
17. Spencer
2.5.09 / 8am


Lebbeus,


If, ““There are no bad students.” What he meant was that young people who aspire to become architects and have gone through an admissions and selection process have demonstrated in advance a potential that should be respected,” is true and there are good and great students everywhere then I have to wonder if it is really the school that makes the faculty great? After all, teachers are going to find these students at every school. So, it seems reasonable to say that it is the schools resources and reputation that attracts the teacher. I mean, honestly, most faculty members want to be teaching at the most reputable school with the most assets? After all, Harvard offers more to a teacher than Ball State just like Ball State offers more than the University of Idaho (seems reasonable, right?). Correct me if I am wrong, but I've also never heard of a professor interviewing students to measure their goodness or greatness when selecting a school to teach at. I suspect they, like the student, choose the school more by reputation and resources (being different for both of course).


There are some exceptions to this. About 10 years ago I was inspired when I heard that both Catherine Ingraham and Jennifer Bloomer, both accomplished educators, were teaching at the Iowa State University. Both had taught at more reputable schools but for some reason, along with a several other teachers of similar quality, decided to travel out to the center of our country to a less know school of architecture. I'd like to think they decided to do this to reach those students who wouldn't normally be exposed to the higher level of instruction.


So, I can't help but wonder where this leaves the less reputable schools with the less serious peers and more importantly the good and great students who attend those schools. 


It's easy to say they supplement their education with determination and the inspiration of a “Lone Ranger.” In my experience it is even more rare to see such a thing happen. When I look around at the built environment I am reminded there are many more uninspired architects in the world than inspired as I realize that an architect designed that strip mall, that an architect put a stamp on the designs for a new Shuck's Autoparts in my neighborhood and that thoughtless two story addition to a less than glamorous house next door to my friend's house was likely to be designed by someone who went to architecture school. 


In my opinion, the education system short changes the majority students resulting in many terrible buildings that we will have to live to see built and then torn down. It's a waste of our resources that we can not afford to waste. I'm certainly not saying that all buildings have to be great but that we could stand to have less of the uninspired ones. I think a large portion of them could be avoided if more teachers like yourself took bold steps to reach those good and great students who do not have the luxury and fortunate to attend the more reputable schools.


I do look forward with anticipation to the continuation of this thread of ideas and possibly some resolution to my discontent.
19. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.18.10 / 4pm


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
21. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.18.10 / 11pm


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
23. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] school on his blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
25. [Lebbeus Woods on Architecture School | Architecture EV](http://architectureev.wordpress.com/2011/01/02/lebbeus-woods-on-architecture-school/)
1.2.11 / 6pm


[…] Architecture School 101 Architecture School 102 Architecture School 201 Architecture School 202 Architecture School 301 Architecture School 302 […]
27. stefanus hadi nugroho kurniawan
4.18.11 / 1pm


i'll wait the next pahse
29. Pedro Esteban
5.27.11 / 8am


ok, let's read the other post
31. [Students are the other half of any school's story | a Walk in the Parq](http://euvgparq.wordpress.com/2012/01/02/students-are-the-other-half-of-any-schools-story/)
1.2.12 / 12am


[…] propósito do Balanço de Semestre que agora finda, obrigatório (mesmo) ler o texto Architecture School, da autoria de Lebbeus Woods. Gostar disto:GostoBe the first to like this . Esta entrada foi […]
