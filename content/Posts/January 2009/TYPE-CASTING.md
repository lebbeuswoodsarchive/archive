
---
title: TYPE CASTING
date: 2009-01-16 00:00:00 
tags: 
    - architecture
    - innovation
    - typology
---

# TYPE CASTING



![Grosse Gruppe)](media/Grosse_Gruppe).jpg)")


*Above: Kurt Schwitters' Merzbau, a new typology of the boundaries of living space.*


In various posts over the past year or so, I have stated outright or alluded to the need for architects to envision new types of constructed, inhabited space.


 What I refer to are not just recastings of already known building types, such as many innovative contemporary designs do—prominent examples being the Bilbao Guggenheim museum, and the CCTV building in Shanghai. Spectacular though they are, they must be recognized as monuments to existing knowledge, a gloss on existing ways of living and thinking. One can admire the remarkable abilities of their architects, while at the same time recognizing that they contribute nothing new to the typologies of museum or of office buildings, let alone to the concepts of preserving and publicly presenting artifacts, or creating space for collective work. But, we might ask, why should they?


Why should architects concern themselves with conceiving new types of spaces? Is that not the prerogative of their clients, who represent the needs of society at a particular time?


The answer to the last question is ‘not always.' Clients do, indeed, represent the needs of certain financially empowered social elites that, understandably, want to serve their own interests. The wealthy and empowered believe that, by doing so, they serve the whole society, and they are right, at least in the case of museums. However, this does not mean that a museum must always be a big building with galleries, where things are lined up for display. Could there be other spatial solutions that re-contextualize works of art? That is a question of architecture and the invention of a new typology.


Throughout my working life, I have been most concerned with the changes happening in the world and how they impact architecture. Or, to put it differently, how architecture might impact the changes, and for the better. Early on, I developed the attitude that architecture is not a passive field, but an active one. Architecture should not merely follow, but lead in the conception of space for human use. Architecture should not merely express change, but should participate in creating it. Architects have special responsibility, because of the enormous impact architecture has on the lives of others. Ultimately, in a democratic society, it is up those others to decide whether architects' designs and, most especially, their innovations are useful and valuable. Architects need not worry that by making proposals, however unconventional, they are imposing them on others. Architects, thankfully, simply don't have that power. We are idea people—but we are, therefore, obliged to actually have ideas, and on occasion new ones, and to give them material form.


The changing needs of the majority of people who actually comprise society and who, in supposedly democratic political systems, count as much as elites, rely on the advocacy of perceptive and inventive architects, when the elites' attention is focused elsewhere. The Modernist movement in architecture was commissioned, as it were, by architects, who saw the need for new types of housing, offices, factories, even cities, when those in government and business failed to do so.


New types of inhabited space with any value are not invented arbitrarily, for the sake of novelty or mere interest. Rather, they are made necessary by changing needs for living, brought about by technological, cultural, political and other changes that impact the lives of people of every social description. Architects should do their best to understand these changes and propose new types of spaces, when and where they believe conditions demand.


By way of giving some examples, I offer a partial list of new types of structures and spaces introduced in my projects over the past twenty years of tumultuous change in the world. A thorough survey will reveal many others, proposed by architects throughout the history of architecture.


**Living-laboratories**


Spaces and structures performing as instruments for experimental living and working. *Examples:* the *inverted* (hanging) *towers* in Underground Berlin; the *Solohouse.*


**Aero-living-laboratories**


Spaces and structures for living and working experimentally in the fluid terrain of the sky. *Examples:* the levitating *aerial ‘houses'* and *communities* of Aerial Paris.


**Freespaces**


Spaces and structures for living and working that are 1) free of pre-assigned purpose or meaning; and 2) difficult to inhabit; intended for those willing to invent ways to inhabit them. *Examples:* the *hidden freespaces* of the Berlin Free Zone project; the *bridging, leaning, and suspended freespace structures* in the streets of the Zagreb Free Zone; *urban wall freespaces* of the Habana Vieja project. 


 **Injections, Scabs, Scars**


Spaces and structures creating three stages of the reconstruction of war-damaged buildings. *Examples*: numerous projects for Sarajevo, Bosnia.


**High Houses**


Spaces and structures built on vertical beams stabilized by cables in tension, occupying the air space above a site, while still being physically connected to it. *Example:* Sarajevo *High Houses.*


**Wall Cities**


Urban-scaled, improvised ‘megastructures' built up incrementally, using tectonic and spatial fragments. *Examples: defensive wall* for the Bosnia Free State; *Quake City* of the San Francisco Bay Area projects.


**Meta-Institutes**


Structures presenting spatial conundrums, the solving of which contributes to a re-institution of an institution (e.g., of government). *Examples: Parliament Building* *reconstruction*, Sarajevo; *Meta-Institute project*, Havana.


**Horizon Houses**


Structures that change their relationship to the horizon and, hence, to gravity, altering the nature and potential of the spaces they contain. *Examples:* the *Wheel House, Block House, and Star Houses.*


***Buoyant Buildings***


Spaces and structures for living on and under the water, forming communities that reconfigure themselves (self-organize) according to fluid-dynamics of the sea and shifting social arrangements. *Examples:* the *Iceberg structures* of the San Diego project. 


**Terrain**


Artificial landscapes reforming natural ones, according to the influence and impact of both human and natural forces of change. *Examples:* the *terraforms* of the DMZ (Korea) project, the Terrain and the Utopx projects; the *tilting beach* of the Havana Malecon project.


**Earthquake Houses**


Structures and spaces in seismically active regions that incorporate the forces released by earthquakes to transform them and the ways of living, working, and thinking they support. *Examples*: the *shards, slip, wave,* and *fault* houses of the San Francisco Bay Area projects.


 **Vectorspaces**


**Structures and spaces formed by tectonic lines and their groupings embodying physical, emotional, and intellectual energy. *Examples: installations* of The Fall project in Paris; the System Wien project in Vienna.**


LW


![webbdrive-in1](media/webbdrive-in1.jpg)


*Above: Michael Webb's**drive-in house, a new typology fusing house and automobile.*


 



#architecture #innovation #typology
 ## Comments 
1. Marc K
1.18.09 / 3am


It is fear. It is fear that takes us and fear that separates us. We see worlds in which the arms of our walls reach and touch us, grab us, and take us to uncomfortable, frightening places. We see fissures and breaks, experience the singe of barren light through harsh apertures, and the coldness of the steel against our backs. We seem so scared we cannot look; we cannot listen. It seems that the future holds for us no bounds, no return.


Still, we look outside and still the sun shines. And we still find peace in disillusion. Take the reigns of the past and rear forward. The manner in which we see our walls will take our phenomenological selves among spaces Hejduk could not even imagine.


I find no peace in these relations; I see only the long, drawn stare of fright amongst the eyes of my brothers. And in such inventions and dreams to they see sullen spaces; spaces of other worlds, fit not for them. But it is so contrary! How these men may look at themselves and comprehend their superlative being, to only find their true place among those too fearful to take the reigns themselves…
3. mchart929
1.19.09 / 12am


It seems to be that there is almost a similarity between the philosophy of today's contemporary architects and that of the New Formalist movement — both cases involve architects employing the newest fabrication techniques of their times* to apply ornamentation to existing building types.


*In the case of current architecture, the new fabrication techniques would be the use of computer software to create the new shapes present on contemporary buildings such as Gehry's.
5. Art
1.19.09 / 8pm


You are correct in stating that by serving their own interest the wealthy ruling elite serve society. But who does society serve? Society is only concerned with itself and nothing else. Society takes power from the individual and gives it to the elite. Since our society believes that money is how value is measured, new typologies cannot emerge unless they are congruent with this belief. I would ask whether architects can become leaders and generate new beliefs about value. I think if they did, they could not be considered “architects” because by definition architects have a certain responsibility to society. To generate new beliefs about value would be to directly challenge society itself. We must begin by asking ourselves if we believe society is a actually something worth preserving. I think its completely within an architects right to impose their ideas on others. What kind of morality allows the self serving ruling elite this right but not architects? Do architects have so much of their identity invested in the idea of society that they are afraid to challenge it? When will architects see themselves as leaders instead of servants?
7. David
1.23.09 / 6pm


@Art:  

“Society is only concerned with itself and nothing else.”


What should society serve, other than itself? What could it? What single idea can you even name, except for those that are part of society?


“our society believes that money is how value is measured”


Not quite right. “Money” is the name that we give to something that allows us to trade our goods and labor in an abstract way. If we decided that “love” or “widgets” or “respect” or “bing-bong” was what determined value, we would no doubt see some ugly behavior surrounding those things, too.


“I think its completely within an architects right to impose their ideas on others. What kind of morality allows the self serving ruling elite this right but not architects?”


I think you are mistaking “rights” for “power.” Anyone has the so-called right to impose an idea on another. It is when that person has the ability to actually do something about that imposition that conflict occurs. Once architects actually have the power to impose their ideas on others, they would then be, by definition, “ruling elite.”


Why not just say: “I wish the ruling elite had better, more benevolent ideas that coincided with my own.”


Lastly:  

“Do architects have so much of their identity invested in the idea of society that they are afraid to challenge it?”


Ummm…yes. An architect without society (whatever you define it as) is like a puppetmaster without fingers. Unless you are an architect who builds your own buldings. And uses them. And cleans them, etc, etc, etc
9. Art
1.25.09 / 8pm


David:


Your comments were very enlightening and helped me to realize that the purpose or society and the definitions of money, power and rights are practically meaningless. I do however think it is more accurate to think of society as a collective identity or belief system instead of just “people” in general. Wishing that the ruling elite had better, more benevolent ideas that coincide with my own is simply a waste of time. But I love the idea of architects attaining the power to impose their ideas on society and becoming part of the ruling elite instead of just being an accessory. What would need to happen for architects to attain this power? I firmly believe that architects should strive to see themselves as leaders and assume that role in society. Is there any reason why architects should not?
