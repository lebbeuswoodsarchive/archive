
---
title: SOTIRIOS KOTOULAS  seeing space
date: 2009-01-22 00:00:00 
tags: 
    - architecture
    - arctic
    - basic_research
    - Sotirios_Kotoulas
---

# SOTIRIOS KOTOULAS: seeing space



It is rare to encounter basic research in the field of architecture, but the publication of this project by Sotirios Kotoulas gives us a chance to do so. By its nature, basic research opens up new and unfamiliar domains that address the foundations of our knowledge. Architects, absorbed as they are in contemporary problems of design, devote little time to questioning the assumptions underlying their work. What is space? How do we know it? What constitutes its reality, its physical fabric? What material forces, lying beyond the realm of the visual, shape the physicality of space and human comprehension of it?


Our present world is greatly impacted by the invisible, and most extensively by the focus of Kotoulas' research: electromagnetic forces. Computers and the internet, satellites and cell phones, indeed, all of the electronic instrumentation of our globally interconnected civilization increasingly relies upon for cohesion, engage the range of the electromagnetic spectrum of which visible light is but a sliver. In a palpable sense, we already inhabit electromagnetic spaces and are part of their constituency, as the term ‘cyberspace' attempts to acknowledge. The reason we do not know where cyberspace is, or when we are in it, or how it looks and feels when we are is because our conceptual and perceptual faculties are stuck in older ideas of space. We are hemmed in by our present assumptions and by our inability to visualize, and thus physically experience, space we cannot measure by means we already know. Kotoulas aims to change this by bringing the invisible into the realm of the visible. Without losing his sense of awe, or reducing the immeasurable, he accomplishes his mission by traveling to polar regions of the far north, to the geographical edges, if not the metaphysical limits, of our present civilization, where the visual dimension of our experience is distorted by extreme conditions. He recognizes this as a chance to not only extend our knowledge of ourselves and what we can create, but to add something new to the apparatus of our understanding. His is a polar expedition of the mind, and the territories—philosophical and material—his rigorous and imaginative explorations reveal are claimed by him, quite appropriately, in the name of architecture.


LW


![spout71](media/spout71.jpg)


![spout62](media/spout62.jpg)


![spout82](media/spout82.jpg)


![spout2a1](media/spout2a1.jpg)


![spout3](media/spout3.jpg)


![spout1](media/spout1.jpg)


 


![spout41](media/spout41.jpg)


![spout51](media/spout51.jpg)


**Sotirios Kotoulas** is an architect, born in Winnipeg, Canada, who is currently working on several new projects, including his PhD. in architectural theory at McGill University, in Montreal.


A book, entitled *Space Out*, authored by him, was published by the Research Institute for Experimental Architecture (RIEA) and Springer-Verlag/Wien, in 2005. The above images and text were excerpted from the book:



<http://www.amazon.com/s/ref=nb_ss_gw?url=flatten%3D1%26search-alias%3Daps&field-keywords=Sotirios+Kotoulas&x=0&y=0>


#architecture #arctic #basic_research #Sotirios_Kotoulas
 ## Comments 
1. [workspace » Blog Archive » Seeing Space](http://www.johndan.com/workspace/?p=728)
1.23.09 / 5am


[…] Woods points out the importance of Sotirios Kotoulas' work on visualizing electromagnetic forces in …. Kotoulas is finishing his PhD at McGill and the author of Space Out (from which the images above […]
3. David
1.23.09 / 7pm


I find Mr. Kotoulas' drawings beautiful. They made me think that the confusion of cyberspace with “regular” space is an accident of vocabulary. 


Asking “where” cyberspace is is sort of like asking “when” the stitch in time is. Incoherent question.


Part of the problem is that “space” is a term that we incorrectly assign physical attributes to when it is in fact a mental phenomenon. While we can ask whether there exists an ideal basketball that all actual basketballs endeavor to resemble, there is no such analagous relationship with space. All space is ideal space.


The “space” in cyberspace is collapsed. We imagine that physical space fills the void between mass. In cyberspace, there is no such articulated void. Everything is equally spaced. Everything depends on how you got there.


The lack of “ideal space” is a curiosity of our time (for liberal intellectuals like myself and most of the readers of this blog, I'd imagine). Most ancient religions (and new ones) believe that some space is more ideal than others. Thus, the Forbidden City, the Mormon temple, the Western Wall. The modern thinking person finds these ideas variously beautiful, quaint, and nostalgic, but does not truly believe them.


But – is there hierarchy in cyberspace?  

In physical space, the relationship is individual->ideal or group -> ideal.  

In cyberspace the relationship is individual -> individual or individual -> group.


Has the collapsing of barriers to communications become a new egalitarian religion for atheists?


These are interesting:


[More atheists on Internet than in “real life?”](http://boards.ign.com/teh_vestibule/b5296/176198938/r176198983/)


[Internet converting people to atheism](http://friendlyatheist.com/2009/01/15/would-you-be-an-atheist-without-the-internet/)
5. sos
1.29.09 / 5am


David,  

Space is mentally and physically experienced, it is both quantitative and qualitative; one can simultaneously inhabit measurable space and immeasurable space in present time. The embodied experience finds unity in the paradox. Electromagnetic phenomena are not proportionally dispersed fixed frequencies at fixed locations, they fluctuate, and their intensity and location can stimulate the mind and the body to inhabit and perceive other spaces with radically different qualities. Hierarchy will always exist when you have favorites.
7. martin
2.3.09 / 6pm


if an object gives specificity to the space around it, or since space and objects are mutually defining, what happens when there is no object? is there space? is there nothing? isn't ‘nothing' space in some sense? just empty space? i suppose the inclusion of a perceiver of space makes it space because the perceiver is an object. do attempts at defining space only lead to teleological arguments? how does one escape this?


i found this interesting visualization of the internet:  

[![](media/Internet_map_1024.jpg)](http://upload.wikimedia.org/wikipedia/commons/d/d2/Internet_map_1024.jpg)


interesting how it loosely resembles a galaxy.
9. [lebbeuswoods](http://www.lebbeuswoods.net)
2.3.09 / 8pm


martin: in the spaces Kotoulas has studied, the objects are there—electromagnetic waves, with precise physical properties—we just can't see them. Also, my post omits an extremely important dimension of his work that adds a human dimension: hallucination. This is very much related to the invisible physical presences. For more on that, you will have to refer to his book! It's all there….
11. [A Synthetic Architecture](http://amcgoey.net/general/seeing-space/)
2.18.09 / 12pm


**SEEING SPACE …**


SEEING SPACE « LEBBEUS WOODS – Lebbeus Woods presents a series of drawings by Sotirios Kotoulas who's research explores making visible the spaces of the electromagnetic spectrum. While his drawings are fascinating in their angular complexi…
13. [PRETTY DAM PURE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/10/25/pretty-dam-pure/)
10.25.10 / 3am


[…] Kotoulas is an architect who makes exploratory projects and currently teaches architectural and urban design and theory in the school of architecture of […]
