
---
title: re  THE SYSTEM
date: 2009-01-20 00:00:00 
tags: 
    - architect
    - copyrights
    - legal_system
---

# re: THE SYSTEM



In response to a recent post, some discussion has taken place about ‘the system,' referring to the prevailing social order—a matrix of accepted customs, values, practices, and laws—which governs our lives. This post is not going to argue its merits and demerits, or whether there might not be a better system, but rather to make available one small example—from my personal experience—of how the present system works. While there are obviously many larger and more important problems in the world urgently in need of attention, this example nevertheless affirms that the system, at its best, does empower solitary individuals, even when they challenge wealthy and powerful corporations. Even more, it recognizes the rights of artists—and architects—to defend against the exploitation of their works without their consent. Not only are there cracks and crevices within which the seeds for transforming the system can take root, but also ‘laws on the books' that give each of us power, if we are willing to use them.


The following is a matter of public record. Judge Miriam Cedarbaum's opinion has become part of Federal Case Law in the United States, and *Woods vs. Universal City Studios, et. al.,* is used as a precedent in support of all lawsuits involving artists, architects and designers who believe they have a legitimate claim of copyright infringement.


 LW


![lwblog-the-system-dwg1](media/lwblog-the-system-dwg1.jpg) 


 1146  


LEXSEE 920 f.supp 62 


**LEBBEUS WOODS, Plaintiff, -against- UNIVERSAL CITY STUDIOS, INC., ATLAS ENTERTAINMENT, INC., TERRY GILLIAM and JEFFREY BEECROFT, Defendants.**  


**96 Civ. 1516 (MGC)**  


**UNITED STATES DISTRICT COURT FOR THE SOUTHERN DISTRICT OF NEW YORK**  


   ***920 F. Supp. 62;******1996 U.S. Dist. LEXIS 3848;******38 U.S.P.Q.2D (BNA) 1790*** 


**March 29, 1996, Dated**  


**April 1, 1996, FILED**          


**COUNSEL:**


[**1]  APPEARANCES:  


MOSES & SINGER, New York, NY, By: **Peter** **Herbert**, Esq., Philippe Zimmerman, Esq. 


LAW OFFICES OF JAMES P. TIERNEY, Santa Monica, CA, By: James P. Tierney, Esq., **Peter** J. **Anderson**, Esq., Attorneys for Plaintiff.  


VLADECK, WALDMAN, ELIAS & ENGELHARD, New York, NY, By: Anne C. Vladeck, Esq., LEOPOLD, PETRICH & SMITH, Los Angeles, CA, By: Louis P. Petrich, Esq., Gary M. Grossenbacher, Esq., Attorneys for Defendant Universal City Studios, Inc.   


**JUDGES:**


MIRIAM GOLDMAN CEDARBAUM, United States District Judge  


**OPINIONBY:**


MIRIAM GOLDMAN CEDARBAUM  


**OPINION:** 


    [*63]  OPINION 


**CEDARBAUM, J.**


Lebbeus Woods sues Universal City Studios, Inc. for infringement of his copyright in a drawing. Woods has moved for a preliminary injunction enjoining Universal from distributing, exhibiting, performing or copying those portions of the motion picture entitled 12 Monkeys which reproduce his copyrighted drawing, or any portion of it. For the reasons that follow, Woods' motion is granted.


   Background


In 1987, Woods created with graphite pencil a detailed drawing, entitled “Neomechanical Tower (Upper) Chamber,” which depicted a chamber with a high ceiling, a chair mounted on a wall and a [**2]  sphere suspended in front of the chair. (Woods Aff. P 7 & Exh. 1.) The wall and floor of the chamber are comprised of large rectangles with visible joints forming a grid pattern. (Id., Exh. 1.) The chair, whose back, seat, front and footrest are each comprised of rectangles, is attached to a vertical rail on one wall. (Id.) The sphere is supported by a metal-frame armature and held aloft directly in front of the chair at face level. (Id.) Cables loop beneath the chair and the sphere. (Id.) This version of “(Upper) Chamber” appeared in a catalog entitled Lebbeus Woods/Centricity, published in Germany in 1987. (Id. P 7.) In 1991, Woods colored his black and white drawing of “(Upper) Chamber,” and this version was included in a collection of Woods' illustrations entitled Lebbeus Woods/The New City, published in the United States in 1992. (Id. PP 8-9.)


In late December 1995, Universal released 12 Monkeys. At the start of the movie, the main character is brought into a room where he is told to sit in a chair which is attached to a vertical rail on a wall. The chair slides up the rail to a horizontal ledge on the wall so that the chair is several yards [**3]  above the ground. A sphere supported by a metal-frame armature descending from above is suspended directly in front of the main character.  [*64]  On three occasions, the main character returns to this chair. (**Anderson** Aff., Exh. 14-15.)


In early January 1996, two of Woods' colleagues told him that they believed 12 Monkeys was using his work. (Woods Aff. P 10.) On January 18, 1996, Woods saw the movie. (Id. P 12.) On January 24, Woods, through his **attorney**, notified Universal of his claim. (**Anderson** Aff. P 4.)


   Discussion


To obtain a preliminary injunction, Woods must demonstrate: (1) irreparable harm and (2) either a likelihood of success on the merits of his claim, or sufficiently serious questions going to the merits to make them a fair ground for litigation and a balance of hardships tipping decidedly in his favor.  *Polymer Technology Corp. v. Mimran, 37 F.3d 74, 77-78 (2d Cir. 1994).* 


I. Likelihood of Success on the Merits


To establish infringement of a copyright, a plaintiff must show both ownership of a copyright and that the defendant copied the protected material without authorization. *Rogers v. Koons, 960 F.2d 301, 306* (2d Cir.), cert. denied, *506* [**4] *U.S. 934, 121 L. Ed. 2d 278, 113 S. Ct. 365 (1992).*


   A. Registration


Section 411(a) of the Copyright Act, *17 U.S.C. Â§  411*(a) (1988), requires a plaintiff to register a copyright claim before bringing an action for infringement. In 1992, the color version of “(Upper) Chamber” appeared in The New City, a collection of illustrations by Woods. (Woods Aff. P 9 & Exh. 5.) Universal argues that the copyright registration for this book, filed in August 1992, only covers the selection and arrangement of Woods' illustrations, not the earlier published illustrations themselves. However, the certificate of copyright registration for The New City lists Lebbeus Woods as the author of the illustrations contained in the book.


Moreover, where the owner of the copyright for a collective work also owns the copyrights for its constituent parts, registration of the collective work satisfies the requirements of Section 411(a) for purposes of bringing an action for infringement of any of the constituent parts. See *Greenwich Film Prod. v. DRG Records, 833 F. Supp. 248, 252 (S.D.N.Y. 1993);* *Computer Assoc. Int'l v. Altai, Inc., 775 F. Supp. 544, 556-57 (E.D.N.Y. 1991),* aff'd in part,  [**5]   vacated in part on other grounds, *982 F.2d 693 (1992).* Wood's submissions show that he owns the copyright in the earlier published “(Upper) Chamber.” (Woods Aff., Exh. 2.) Thus, the copyright registration for The New City satisfies the requirements of Section 411(a) for purposes of the present suit.


In any event, Woods also represented at oral argument that he has applied for separate registration of the 1987 and 1991 works, and expects a certificate of registration from the copyright office within the next few days.


   B. Copying


Universal cannot seriously contend that “(Upper) Chamber” was not copied during the filming of 12 Monkeys. Terry Gilliam, the director, admits that in preparing the design of 12 Monkeys, he reviewed a copy of a book that included “(Upper) Chamber.” (Gilliam Aff. P 10.) Gilliam and Charles Roven, the producer, discussed the drawing with Jeffrey Beecroft, the production designer. (Id.)


A comparison of “(Upper) Chamber” and footage from 12 Monkeys demonstrates that the movie has copied Woods' drawing in striking detail. For example, in both 12 Monkeys and “(Upper) Chamber,” the wall and floor are composed of large rectangles with [**6]  visible joints forming a grid. The wall in 12 Monkeys has the same worn texture as the wall in “(Upper) Chamber,” including places where the surface layer of the wall has fallen away. In both 12 Monkeys and “(Upper) Chamber” there is a horizontal shelf and apron near the top of the vertical rail to which the chair is attached. Both the chair in the movie and the chair in Woods' drawing are comprised of four rectangular planes, and have armrests with diagonal supports comprised of two parallel strips separated by a narrow space. Both chairs have the same pattern of horizontal and vertical etching on the upper part of the chair back. The  [*65]  spheres in the movie and in “(Upper) Chamber” are both suspended in front of the chair from a metal framework and have a similar surface design.


Universal argues that the infringement is de minimis because the infringing footage in 12 Monkeys amounts to less than five minutes in a movie 130 minutes long. Whether an infringement is de minimis is determined by the amount taken without authorization from the infringed work, and not by the characteristics of the infringing work. As discussed above, 12 Monkeys copies substantial portions [**7]  of Woods' drawing. 


II. Irreparable Harm


Normally, when a copyright is infringed, irreparable harm is presumed.  *Fisher-Price, Inc. v. Well-Made Toy Mfg., 25 F.3d 119, 124 (2d Cir. 1994).* This is only a presumption, however, and it vanishes if the copyright holder unreasonably delays prosecuting his infringement claim. Id. Universal argues that 12 Monkeys had been in release for twenty-nine days by the time Woods made his claim, and that this constitutes unreasonable delay. There is no evidence that Woods knew or should have known of the infringement until early January 1996. On January 24, Woods notified Universal of his claim. (**Anderson** Aff. P 4.) Three weeks does not constitute unreasonable delay. 


III. Discretion


While an injunction is not the automatic consequence of infringement and equitable considerations are always germane to the determination of whether an injunction is appropriate, *New Era Publications Int'l v. Henry Holt, Co., 884 F.2d 659, 661 (2d Cir. 1989),* cert. denied, *493 U.S. 1094, 107 L. Ed. 2d 1071, 110 S. Ct. 1168 (1990),* in the vast majority of cases, an injunction is justified “because most infringements are simple piracy.”  [**8]  *Campbell v. Acuff-Rose Music, Inc., 127 L. Ed. 2d 500, 515 n.10, 114 S. Ct. 1164* (citing Leval, Toward a Fair Use Standard, *103 Harv. L. Rev. 1105, 1132 (1990)).*


Universal has not demonstrated that this is a case of “special circumstances” justifying an award of damages or a continuing royalty instead of an injunction, or that “great public injury” would result from an injunction. Cf.  *Abend v. MCA, Inc., 863 F.2d 1465, 1479 (9th Cir. 1988),* aff'd sub nom.  *Stewart v. Abend, 495 U.S. 207, 109 L. Ed. 2d 184, 110 S. Ct. 1750 (1990).* Universal argues that it will suffer considerable financial loss if a preliminary injunction is granted. Copyright infringement can be expensive. The Copyright Law does not condone a practice of “infringe now, pay later.” Copyright notification and registration put potential infringers on notice that they must seek permission to copy a copyrighted work or risk the consequences.


Universal also argues that there are First Amendment and public interest considerations that favor the continued distribution of an unredacted version of 12 Monkeys. However, Universal has failed to specify the First Amendment or public interest considerations that [**9]  argue against an injunction. Universal does not argue that its commercial, science-fiction movie constitutes criticism, comment, news reporting, teaching, scholarship or research, and thus, rightly does not contend that this infringement of “(Upper) Chamber” falls within the fair use doctrine. See *17 U.S.C. Â§  107* (1988 & Supp. 1993).


   Conclusion


Because Woods has established the prerequisites for a preliminary injunction, and because Universal has failed to demonstrate that this is a case of “special circumstances” justifying only an award of damages and not an injunction, Woods' motion for a preliminary injunction is granted. The injunction will not take effect until Woods submits to the Court the separate copyright registration certificates for the 1987 and 1991 versions of “(Upper) Chamber.” Woods will submit a proposed injunction in accordance with this opinion. 


SO ORDERED. 


Dated: New York, New York


   March 29, 1996


   MIRIAM GOLDMAN CEDARBAUM


   United States District Judge



#architect #copyrights #legal_system
 ## Comments 
1. [Adam Smith](http://www.thisishatch.com)
1.21.09 / 11am


I remember hearing about this in arch. school. In this instance, I'm sure you were wronged and the film makers “ripped you off”, but generally I'm very inspired by the open source/creative commons approach and even more radical approaches like the early pirate party. 


I'm concerned by the parallels between the corporatization of public space and the expansion of intellectual ownership like the patenting of plain ol' corn and the TMing of Happy Birthday. It makes me wonder if intellectual property really is the same thing as physical property. 


Just curious, would you ever consider a creative commons license for your some of your work? 


ps. not comparing your case with those other dubious applications of IP, just trying to find the poles…
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.21.09 / 2pm


Adam Smith: Intellectual property is easier to steal than physical property. The legal penalties for doing so are far less severe.


I think intellectual property is necessary as long as personal property is sanctioned by law. When we arrive at the utopian point of a truly egalitarian society, all personal property will cease to exist and no one will miss it. However, we're a long way from that point in the evolution of our present society.


Even under existing international copyright laws, the doctrine of ‘fair use' (mentioned in the judge's opinion) allows the relatively free use of copyrighted intellectual property, if it aims to improve the human condition through scholarship, research, education. But it forbids use without the copyright-holders' permission in order to financially benefit only a few. This seems just to me. Even idealistic.


Finally—what is a creative commons license?
5. Fine Line
1.22.09 / 6am


<http://creativecommons.org/>  

Share, Remix, Reuse — Legally  

Creative Commons provides free tools that let authors, scientists, artists, and educators easily mark their creative work with the freedoms they want it to carry. You can use CC to change your copyright terms from “All Rights Reserved” to “Some Rights Reserved.”
7. peridotlogism
1.23.09 / 3pm


“When we arrive at the utopian point…”


I don't imagine ‘we will' – I might, you may, someone else might a little bit, but all of those points could be lost at a moments notice.
9. [Labbeus Woods « baskonusbirakdinle aka pushtalkreleaselisten](http://baskonusbirakdinle.wordpress.com/2009/10/09/28/)
10.9.09 / 2am


[…] of a  human like robot. Another sibling, 12 monkeys (1995) has copied one his piece and he won a copyright trail but then he let them use the scene, the interrogation cell with the chair with the […]
