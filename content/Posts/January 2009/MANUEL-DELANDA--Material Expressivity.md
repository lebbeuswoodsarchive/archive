
---
title: MANUEL DELANDA  Material Expressivity
date: 2009-01-05 00:00:00
---

# MANUEL DELANDA: Material Expressivity


*Another in the series of articles by philosopher Manuel DeLanda.*


**MATERIAL EXPRESSIVITY**











It is a well known fact that a beam of white light is composed of many pure-color components, or wavelengths. In addition, like the pitches of musical sounds, the different color components have their own rate of vibration, or frequency. These two characteristics allow both light and sound to produce distinctive effects on animal and human brains, effects that may be used for expressive purposes by territorial animals and human artists. But possession of a nervous system is not necessary to make expressive use of color or sound. Even humble atoms can interact with light, or other forms of electromagnetic energy, in a way that literally expresses their identity. Atoms in a gas, for example, if energetically excited, will emit light with an arrangement of bright parallel lines, each line corresponding to a single frequency and positioned relative to one another according to their wavelength. Each atomic species, hydrogen, oxygen, carbon, and so on, has its own characteristic pattern of lines, its own distinctive “ﬁngerprint” as it were. And much like ﬁngerprints may be used to determine the identity of humans, these line patterns can be used by spectroscopists to determine the chemical identity of a given material. Born in the nineteenth century, the science of spectroscopy has become quite complex, using a variety of methods and devices to extract ﬁngerprints from materials, but it ultimately relies on the capacities of atoms themselves to produce expressive patterns, through emission, absorption, or other processes. 


These expressive patterns are what scientists call “information.” This term does not refer to the semantic information that we may get from, say, newspapers, but to linguistically meaningless physical patterns. That physical information has nothing to do with semantic content is demonstrated by the fact that information theory was developed during World War II to deal with problems of communicating encrypted military messages, that is, messages in which the linguistic form and content were hidden. Physical information pervades the world and it is through its continuous production that matter may be said to express itself. Material expressivity, on the other hand, crossed an important threshold when it ceased to be a mere ﬁngerprint and became functional in the form of the genetic code: groups of three nucleotides, the chemical components of genes, came to correspond in a more or less unique way to a single amino acid, the component parts of proteins. Using this, correspondence genes can express themselves through the proteins for which they code. This implies that expression has gone beyond the production of information to include its active storage and processing. And this, in turn, implies that when populations of information-storing molecules replicate themselves, and when this replication is biased in one or another direction by the interactions of proteins with each other and with their environment, the expressive capacities of material entities may evolve and expand in a multiplicity of novel ways. Like atoms, living organisms can express their identity by the emission of patterns, chemical patterns for example. But unlike atoms, this expression has functional consequences since it allows the recognition of an organism's identity by members of the same species, a recognition that is crucial for genetic replication. 


Another important threshold in the history of material expressivity was crossed with the emergence of territorial animals. A simple illustration of the radical transformation this involved may be the conversion of urine and feces, up to then a material byproduct of food chains, into territorial markers expressing not just a species identity but the possession of resources by an individual organism. Sound, color, and behavioral displays involving posture, movement, rhythm and silhouette, are examples of expressive territorial markers, sometimes used in competitions between males, other times in courtship rituals aimed at females. A particularly interesting example is that of bowerbirds. The term “bower” means a pleasant shady place under a tree, but in this case it means a stage or arena built by the bird to perform its displays. Bowerbirds not only clear a small area from leaves and other distractions, but also construct and decorate elaborate structures out of grasses, reeds, and sticks, with an architecture that varies from one species to the next. Of the eighteenth species of bowerbirds some wear bright colors in their bodies, such as the intense blue coloration of the male satin bowerbird, others have lost their color but compensate for it by increasing the number of colored decorations. Indeed, there is a strong inverse correlation, noticed by Darwin, between the expressivity of the bird's body and the complexity of the bower itself: the less inherited expressive resources a species has the better architects its member organisms must be. It is almost as if material expressivity itself migrated from the surface of the bird's body to the external construction and its decorations. 


Darwin also noticed an inverse correlation between the degree of ornamentation of a bird's body and the complexity of its song, and believed that birds were the most aesthetic of all animals. Given that the structure of bird songs is only partly inherited (birds must learn to sing) and that the power to attract females depends in part on the richness of a male's musical repertoire, each individual bird must create new combinations of musical motifs to distinguish himself from other birds of the same species. If ﬁngerprints become markers or signatures with the emergence of territoriality, with bird song signature becomes an individual style. Indeed, a great twentieth century classical composer, Oliver Messiaen, some of whose compositions were directly inspired by bird songs, went as far as calling blackbirds, nightingales, and other bird species that must develop individual styles, “artists”. Thus, the history of material expressivity may be summarized by the sequence ﬁngerprint-signature-style. Finally, we must add the expressive possibilities of bringing many of these non-human artists together. A good illustration of these possibilities is the so-called “dawn chorus” and event taking place usually in the spring and involving many bird species simultaneously. Before sunrise, one or two blackbirds begin the chorus and are then gradually joined by a series of other birds (robin, song trush, wren, great tit, chafﬁnch) until a symphony of sound is reached by midmorning. 


Matter expresses itself in many ways, from the simple emission of physical information to the deliberate use of melody and rhythm. The universe itself may be viewed as a grand symphony of material expressivity. The early human artists who tapped into this expressive reservoir for their cave paintings, body tattoos, and ritual ceremonies, far from introducing artistry into the world were simply adding one more voice to an ongoing material chorus. 


**Manuel DeLanda**



 


 


 


 


 


 


 




 ## Comments 
1. john m
1.6.09 / 1am


If extractive action is taken as attainment of simple identity, do we achieve knowledge of nightingales by wounding them to hear them sing? That is to say that there has to be identity beyond (or in the absence of) a yeilding to demands for expression.
3. [Art and power: the emancipatory potential of contemporary art « Nima Maleki: Politics and Critical Thought](http://positivity.wordpress.com/2009/05/22/art-and-power-the-emancipatory-potential-of-contemporary-art/)
5.22.09 / 2pm


[…] questions (whatever those are…), but rather – to borrow DeLanda's thoughts on material expressivity and affordability – to inspire a critical and creative environment that affords the reader a […]
5. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] pp. 152-53 M. DeLanda, ‘Material Expressivity'in Domus, No 893, June 2006, pp. 122-23 <https://lebbeuswoods.wordpress.com/2009/01/05/manuel-delanda-matters-4/> M. DeLanda, ‘Smart Materials'in Domus, No 894, July 2006, pp. 122-23 […]
