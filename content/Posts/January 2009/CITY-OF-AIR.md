
---
title: CITY OF AIR
date: 2009-01-03 00:00:00
---

# CITY OF AIR



As my journey was nearing its end, and I was preparing to return to my own part of the world, the theory I devised to explain what I had seen began to assume a greater importance than the architecture. There were two reasons. First, the theory did what all good theories must do: it predicted something I had not yet discovered—this was the existence of a fourth city. Second, I found scant physical evidence of it. So, I spent my days making diagrams and conceptual drawings of various kinds, laying out the structure of my theory, and thinking about where I should explore next.


The answer, as it turned out, was fairly simple. Because the theory was based on an idea of natural cycles, such as that of the seasons, and the day, but also of the birth, maturation, death, and rebirth of everything from animals to stars, and, yes, the unseen transformations of energy to matter and back again, in infinite variations—and because the four-part cycle, as described in ancient philosophy and science, of earth, fire, water, and air, seemed so relevant to the cities I had seen, it was clear that the missing city I sought, and that would complete the epic cycle, was a City of Air.


So far, I had not found one. Was it a city of the wind? Was it a city woven into the others, comprised of differing temperatures, densities, aromas, even colors of air? Was it an invisible city, one that had “melted into air?” I did not think so. All of these ideas and expectations were too convoluted, and too intertwined with what I had already experienced and presumed to know. What I was searching for would offer new revelations, just as the other cities had. Finally, I reasoned that the city of the air was simply…*in* the air, in the domain where we do not expect to find a city at all.


Still, looking to the sky, I saw no structures, and was deeply skeptical of doing so. Perhaps there were a few airplanes, though I had not noticed any until now. If there was a community of people living in the sky, how would they stay up there, defeating gravity, as it were? If they were simply rushing through the sky, hurrying to get from one terrestrial destination to the next, it would not constitute living in the sky, actually inhabiting the vast, open spaces just above the earth, engaging their particular qualities and constituting an “aerial” way of life. What, I wondered, were these qualities? I longed to go into the sky, and live there for a while; to slowly roam the clouds and the changing spaces between them; to be far above the earth and all its landscapes and teeming life, and to see the stars with new intensity and clarity. But I could not.


Going back through my drawings, I discovered one or two in which floating structures appeared. While I thought of them at the time as mere appendages to the structures that were the focus of my interest, mainly because they seemed too massive and heavy ever to be airborne on their own, I now realized they were structures of a different order, one belonging to the terrestrial skies. Combining imagination and reason, I made a number of elaborated drawings of how aerial ‘buildings' might appear, some supported by lighter-than-air gases, others—giving up that conceit, because of the improbable buoyancy ratios—rising in all their mass and weight, to unseen heights. Had this community discovered new laws of nature? Or, had they gained new command over known forces. enabling them to levitate heavier-than-air structures without noisy engines and propulsion systems? This I was never able to learn.


Leaving the strange world I had explored, I experienced an epiphany of sorts. It occurred to me that the diagrams I had made of the cyclical theory were more truly forms of the aerial community than their levitating habitats, whatever their form and material, whatever the technology that enabled them to fly. The community of the City of Air was divorced from the earth and its material concerns. Existing in the chilly heights, its thoughts and passions turned to the invention of abstract philosophical systems. Its inhabitants were the true overseers of the human toil below—-masters of all, and of none.


LW



![fc47](media/fc47.jpg)


![fc431](media/fc431.jpg)


![fc461](media/fc461.jpg)


![fc45](media/fc45.jpg)


![fc30a](media/fc30a.jpg)


![fc30b1](media/fc30b1.jpg)


![fc30c1](media/fc30c1.jpg)


![fc41](media/fc41.jpg)


![fc40](media/fc40.jpg)


![fc25](media/fc25.jpg)



 ## Comments 
1. miran
1.4.09 / 1pm


An astonishing journey indeed. By being documented both trough drawing and word, it will hopefully remain perpetually experienced, and more important, discussed, thought about and built upon by different exploring and curious pairs of eyes, eyes not being an arbitrary metaphor for a human in this case. 


It is not because eyes are supposed to be (or not) prevailing perceptive instrument, but a more general issue interests me here, and it is symmetry, both in terms of human, (animal, plant) anatomy, and as a pure idea. Eyes, being positioned the way they are, only confirm and illustrate the existence of symmetry in those terms.


Clearly, in this World of four Cities, an interaction between human factor and nature (if we will still hold on to this dichotomy) is seen via opposition of known human world to this one, or to say, the differences and similarities. Studying the structures built in this World of four Cities, I see them mostly being conditioned by natural laws, thus responding in terrain-like, liquid-like, movement-like forms (to put it very bluntly), and at the same tame, coexisting but very different, ideal forms appear – a straight line, a plane, a sphere, a circle. 


Now what I notice here is this structural phenomenon of symmetry, or to put it more precise, the inception (or recapituliziation) of symmetry of ideal forms in means of say, a plane cutting a sphere in half, or a material curve emphasizing the specific circumference , or a set of openings, or in most cases, a complex combination of such elements. This is something abstractly, idealistically human, where a set of conditions defining that parts of architecture are graspable trough Euclidian geometry, symbolic language, or at least so it seems? 


What could this “inception” of symmetry represent, or be a product of? What political or cultural phenomenon (or a stream of conditions, for that matter) from within society thus from within “buildings” themselves, leads to these “started symmetries”, when a dynamic equilibrium has once been discovered and incorporated into life? 


Could it be a warn sign of dangers of an idea taken too literally, and threats of a system lead by a single thread of thought (say, adding quantity to a size coordinate) without a possibility nor intention to change? 


The rigid symmetry means static balance, but symmetrical division of cells leads to life, and life is nothing but movement. The eyes might be symmetrical, but each of them sees different picture, together adding another dimension to both of them, at the center section plane. Could this be one of the lessons we should learn from this journey?
3. [Miller Taylor](http://www.flickr.com/photos/millertaylor/)
1.5.09 / 3pm


A very fitting and beautiful end to your journeys through this strange land, inhabited by those, who like us, seem destined to divorce the ground to survive, no matter how cyclical their earthly systems. 


The entire series was very evoking, a pleasure to read and view.
5. [Alex Bowles](http://www.otofog.com)
1.7.09 / 9am


Very elegant conclusion. Bravo.
7. Mark Jackson
9.19.10 / 1am


Interesting to encounter this having looked at Irigaray's The Forgetting of Air. As well as Heidegger and Fink's Heraclitus Seminar that encounters the breath of fire
9. Nathaniel Schlundt
12.1.10 / 5pm


“The community of the City of Air was divorced from the earth and its material concerns. Existing in the chilly heights, its thoughts and passions turned to the invention of abstract philosophical systems. Its inhabitants were the true overseers of the human toil below—-masters of all, and of none.”


There is no escape from human toil…


[![City of Air Transformation](media/City_of_Air_Transformation.jpg)](http://www.flickr.com/photos/56563328@N06/5223701697/)
