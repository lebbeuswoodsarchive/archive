
---
title: MANUEL DELANDA  Opportunities and Risks
date: 2009-01-30 00:00:00 
tags: 
    - behavior
    - environment
    - materiality
---

# MANUEL DELANDA: Opportunities and Risks



*Another in the ongoing series of articles on materiality by philosopher Manuel DeLanda.*


How do animals perceive their material environments? This question is intimately related to another one: what opportunities for action are supplied to an animal by its environment? The complementarity of the two questions points to the fact that when it comes to animal perception it is the interaction of organic bodies and the materiality of their surroundings that matters. A cluttered space, for example, supplies a walking animal with the possibility of locomotion in only some directions, those exhibiting openings or passages, but not in others, while an open, uncluttered space offers it the possibility of locomotion in all directions. To use a different example, the edge of a cliff presents a walking animal with a risk, the risk of falling, and the sharp edges of the rocks below, the risk of piercing its ﬂesh. In both of these examples it was important to specify that the animal in question is a walking one, because a space full of obstacles or the edge of a cliff do not constrain ﬂying animals in the same way. Only to an animal lacking the capacity to ﬂy would those opportunities and risks seem signiﬁcant, and the fact that they are indeed perceptually relevant to the animal will be displayed in the way it accommodates its behavior: moving to avoid collisions or keeping a safe distance from the dangerous edge. 


We could say that an animal perceives not the properties of its material environment, but the potential for action that those properties supply it with: a piece of ground is perceived not as horizontal, ﬂat, and rigid, but as affording the opportunity to walk. Conceptually, the distinction that we need here is that between the properties of an object and its capacities: a knife may possess the property of being sharp and this may give it the capacity to cut, but the latter can only be exercised with respect to another object that has the capacity of being cut. In other words, unlike properties that an object either has or does not have, capacities are relational: a capacity to affect always goes with a capacity to be affected. This is why a given distribution of opportunities and risks depends both on an environment's materiality as well as on the behavioral capacities of an animal. And whether an animal will pay attention to a given environmental feature, that is, ﬁnd that feature worthy of perception, will depend as much on its own abilities as on the objective properties of the feature. 


A given environment contains a variety of material components in different states: gas, liquid, solid. The ﬁrst two tend to offer animals a medium to move through, to ﬂy, or to swim. Air and water are also media that transmit signals: light moves through them, chemical substances are carried by them, and waves form in them. This supplies an animal with information about its surroundings, but only if it has evolved the capacity to be affected by those signals, that is, the ability to see, smell, or hear. Solid objects, on the other hand, present an animal with opaque surfaces, but it is at these surfaces that light rays are bounced, chemical substances emitted, and vibrations passed on to the surrounding medium. In other words, it is mostly surfaces that an animal perceives. In addition, it is mainly the layout of these surfaces that supplies it with opportunities and risks. A good example of this is the capacity of cavities to serve as shelter: a layout of rigid surfaces facing inward, like a hole on the side of a mountain, supplies an animal with a place to hide, either to escape from a predator or, on the contrary, to conceal its presence from its unsuspecting prey. But again, the cavity becomes a shelter, and it is perceived as such, not only because of its physical form and capacity to resist penetration, but also because it is a participant in a predator-prey drama. In other words, it is because animals supply one another with opportunities and risks – a predator affords danger to its prey while the prey affords nutrition to the predator – that surface layouts become signiﬁcant to them as perceptual entities. 


Many animals are also capable of manipulating surface layouts changing the distributions of opportunities and risks in their favor. Examples of this abound: spider webs, ant and termite mounds, rodent burrows, bird nests, beaver dams. Producing these new layouts involve a variety of activities, from excavating and carving, to piling up, gluing together, molding, rolling and folding, and even weaving and sewing. The materials used may be secreted by the animals themselves, like spider silk or honeybees wax, or collected from the surroundings, like the wood pulp used in wasp nests, or the grass and twigs used in bird nests. Through an evolutionary process that has meshed the activities of animal bodies to the properties of materials, the resulting surface layouts display the right capacities: a spider web is able to absorb the kinetic energy of ﬂying prey, then glue it and retain it; the branches and cut down trees of a beaver dam are not only capable of blocking a water current to create a deep pond, but their arrangement also possesses air openings to afford ventilation and climate control; the subterranean architecture of ant nests provides the right connectivity between vertical or inclined shafts and horizontal chambers to permit movement, storage, and protection. 


As part of predator-prey dynamics these artifactual surface layouts can act as traps or shelters, but they can also participate in processes of animal cooperation and communication. When insects build a nest, for example, they communicate with one another through the changes they make to their environment—physical deformation as well as impregnation with hormones capable of affecting the animals themselves—their behavior becoming coordinated as they modify the way solid surfaces around them affect them and are affected by them. This indirect communication (called “stigmergy”) allows the insects to construct a nest without any one of them having an internalized representation of it.Builders of robots, particularly in the ﬁeld of behavioral robotics, are learning from the way animals use the changes they make in their surroundings to store information. As they say, why build robots capable of creating internal models of the world when the world is its own best model?


**Manuel DeLanda** 



#behavior #environment #materiality
 ## Comments 
1. [good riddance, architects « p o s t e t h o t i c](http://postethotic.wordpress.com/2009/01/31/good-riddance-architects/)
1.31.09 / 11am


[…] 01.31.2009 “When insects build a nest, for example, they communicate with one another through the changes they make to their environment—physical deformation as well as impregnation with hormones capable of affecting the animals themselves—their behavior becoming coordinated as they modify the way solid surfaces around them affect them and are affected by them. This indirect communication (called “stigmergy”) allows the insects to construct a nest without any one of them having an internalized representation of it.” — Manuel DeLanda […]
3. jeff
2.12.09 / 9pm


can someone please tell me where this article came from?
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.12.09 / 10pm


jeff: This and the other DeLanda articles on this blog were originally published (about two or so years ago) in the Italian design magazine DOMUS, in his monthly column, “Matter Matters.” They are republished here with his permission.
7. [Justin Boland](http://www.brainsturbator.com)
12.22.09 / 7pm


Thank you for making this available outside the pages of DOMUS. De Landa is a treasure.
9. [Opportunities and Risks | Baierle & Co.](http://baierle.wordpress.com/2010/07/26/opportunities-and-risks/)
7.26.10 / 1pm


[…] Read original post at Lebbeus Woods […]
11. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] ²M. De Landa, “Matter Matters – a series from Domus Magazine”, published in Domus Magazine from n. 884 to 901 between 2005 and 2007: M. DeLanda, ‘Building with Bone and Muscle'in Domus, No 884, September 2005, pp. 208-09 <https://lebbeuswoods.wordpress.com/2008/12/03/manuel-delanda-matters-1/> M. DeLanda, ‘One Dimension Lower'in Domus, No 886, November 2005, pp. 136-37 <https://lebbeuswoods.wordpress.com/2008/12/23/manuel-delanda-matters-3/> M. DeLanda, ‘The Importance of Imperfections'in Domus, No 888, January 2006, pp. 136-37 M. DeLanda, ‘Events Producing Events'in Domus, No 889, February 2006, pp. 100-01 <https://lebbeuswoods.wordpress.com/2008/12/13/manuel-delanda-matters-2/> M. DeLanda, ‘Evolvable Materials'in Domus, No 890, March 2006, pp. 164-65 M. DeLanda, ‘Extensive and Intensive'in Domus, No 892, May 2006, pp. 152-53 M. DeLanda, ‘Material Expressivity'in Domus, No 893, June 2006, pp. 122-23 <https://lebbeuswoods.wordpress.com/2009/01/05/manuel-delanda-matters-4/> M. DeLanda, ‘Smart Materials'in Domus, No 894, July 2006, pp. 122-23 <https://lebbeuswoods.wordpress.com/2009/02/27/manuel-delanda-smart-materials/> M. DeLanda, ‘Crucial Eccentricities'in Domus, No 895, September 2006, pp. 262-63 M. DeLanda, ‘Matter Singing in Unison'in Domus, No 896, October 2006, pp. 286-87 M. DeLanda, ‘High Intensity Environments'in Domus, No 897, November 2006, pp. 148-49 M. DeLanda, ‘The Foam and the Sponge'in Domus, No 899, January 2007, pp. 140-41 M. DeLanda, ‘Opportunities and Risks'in Domus, No 901, March 2007, pp. 192-93 <https://lebbeuswoods.wordpress.com/2009/01/30/manuel-delanda-opportunities-and-risks/> […]
