
---
title: PURE FORMS
date: 2009-01-24 00:00:00 
tags: 
    - architecture
    - forms
    - philosophy
    - politics
    - religion
---

# PURE FORMS




Living in a time of intense experimentation with architectural forms can induce a hunger of eye and mind for more basic forms. It is not just that we can tire of novelty and complexity, but also that we can yearn for a clarity of meaning not easily found in them. This does not refer to a nostalgia for the stripped-down shapes of early Modernism, but rather to purer geometric volumes that have appeared in architecture over the ages, the meaning of which is not in doubt and does not need to be interpreted.


What me might call ‘pure forms' are some of the most hard-won achievements of the human mind, in philosophy and mathematics. We may rightly speak of Euclid, in his books comprising *The Elements*, and his Greek precursors as the inventors of the pure volumes—cubes, cones, cylinders, pyramids—because we still follow the mathematical rules they established, but we also recognize that, long before, the Egyptians, Assyrians and Persians understood them and were attracted, for religious as well as practical reasons, to their eternal, immutable nature—indeed their irreducibility.


Pure forms have served different religions, philosophies, and political systems, but always those that fulfill our yearning for clarity in the existential complexity and uncertainty of our experiences—and that is their downside. They are instruments of fundamentalisms of every kind, reducing existence and its meanings to a presumed set of basics and, perhaps, leaving little space for differences. 



Some examples, in the order they appear in the images below:


**TOWERS, Tel-Aviv, Israel**


On a rare day when fog from the sea covered the city, only the tops of several geometrically pure towers were visible from the air, seemingly disembodied from the city below, appearing like apparitions in a dream, or an entranced meditation on the mystic geometries of the Kabala.


 **THE KAABA****, Mecca, Saudi Arabia**


Surrounded on the day of this photo by more than a million pilgrims and worshippers, this holiest of sites in Islam appears to be a perfect cube, even though it is not quite. It is interesting to realize that Islam and Judaism share the Bible's Old Testament, and take most seriously its prohibitions, in Mosaic Law, against ‘graven images.' Both religions reject representational iconography, turning instead to geometry for their sacred symbolism—the Kaaba (meaning ‘cube') being a central example. In the Islamic world, over the centuries, the advancement of geometry and mathematics was enormous and, with the Islamic conquests of Spain and incursions into Europe, were instrumental to the development of Western art and science during and after the Renaissance.


**CAPITOL BUILDINGS****, Dhaka, Bangladesh (1962-74), Louis I. Kahn, architect**


The center of government was commissioned by a military dictatorship when the country, largely Islamic, was called East Pakistan, and completed in the new country of Bangladesh. The American architect returned pure forms to the region where their meaning resonated most deeply. This is not a democratic or egalitarian architecture, but quite the opposite: an architecure provoking not free debate and discourse, but a sense of awe and reverential silence.


**MUSIKERHAUS, Dusseldorf, Germany (1998-), Raimund Abraham, architect**


Pure forms signify an ideal order underlying both the music and community of four musicians living and working in this house. For Pythagoras, who formalized the pure tonal frequencies and their relationships, music was a precise expression of the structure of the cosmos. For the architect of this building, pure geometry becomes more human when infused with the unpredictability of musical performance and improvisation. [For more, refer to the post “Musikerhaus” on this blog.]


**FUNERARY MONUMENT, Utopian site (c.1780), Étienne-Louis Boullée, architect**


The Neo-Classical architecture of Boullée stripped-away the superfluous excesses of Royal pomp, and was very much in tune with the spirit and goals of the French Revolution. The leaders of the Revolution believed they were beginning a brave new, above all rational, society. They reset the calendar to the Year One, established ten months instead of the traditional, irrational, twelve, and a uniform Metric system of measurement. Plain, pure volumes very much spoke to their hopes for the establishment of a universal social order. The Revolution self-destructed—consumed by its internal machinations—before any of Boullée's ideal architecture could be realized.


**GREAT PYRAMID OF KHUFU, Giza, Egypt (****2580-2560 BCE), Unknown architect**


The right pyramid [the angle at which a line drawn from the center of the base to the apex is ninety degrees, a right angle] is simply the most stable of the most common regular volumes. This Pharaoh's tomb was meant to last forever, and this is no doubt the main reason this pyramidal form was chosen. Originally covered in polished white granite or marble that formed smooth sides, and capped with a gleaming golden tip, its endurance seemed assured. However, thieves stole the marble and the gold, and after only four thousand or so years, the pyramid is crumbling a bit. Recalling Shelley's “Ozymandias,” we might imagine that the eternality of a pure form exists in its idea, not its construction.


**MONUMENT TO THE THIRD INTERNATIONAL,****Petrograd, USSR (1919), Vladimir Tatlin, architect**


**Much like the French Revolution before it, the Russian Revolution hoped to establish an entirely new form of society, based, however, on the centralization of power and the authority of the Communist Party, which would lead people to freedom and an egalitarian utopia, eventually. Two years after the Revolution, the architect designed this building to both commemorate and house the Comintern, the central authority in the new system. It was to be the tallest building in the world, and comprised of four pure forms—a cube for the mass meetings, a pyramid for administrative offices, a cylinder for communications, including propaganda, and a hemisphere for radio and telegraph equipment, each rotating at different rates—wrapped and supported by a complexly rising and curving structure aiming, it seems, at the stars. This latter form turned out to be a pure form, too—the double helix of DNA, the elemental structure of all living things, not discovered until 1952. Before any of its ideal architecture could be realized, this revolution, too, self-destructed, leaving a tough cinder that endured long after the revolutionary fires had burned out.**


LW 


![lwblog-ta-fog2](media/lwblog-ta-fog2.jpg)


![lwblog-kaaba](media/lwblog-kaaba.jpg)


![lwblog-kahna](media/lwblog-kahna.jpg)


![ramhaus5](media/ramhaus5.jpg)


![lwblog-boullee1](media/lwblog-boullee1.jpg)


![lwblog-giza](media/lwblog-giza.jpg)


![lwblog-tatlin21](media/lwblog-tatlin21.jpg)


#architecture #forms #philosophy #politics #religion
 ## Comments 
1. earlstvincent
1.28.09 / 8pm


Looking forward to the next part. Great post.
3. paulo miyada
1.29.09 / 11am


hello. well, maybe it will appear in the next part, but I would ask you about the ideological position of teachers and school's administration. Because the way the opinion and position about the professional role and the ‘architectural ethics' (if we can call it so) appear and are discussed during someone's studies may be decisive for their professional and civic position. Of course this can always be discussed under philosophy and social studies, but it goes beyond this moments, and is present also in studios. I think a great faculty is one that makes this discussion always clear and present to be discussed, whatever their opinions are.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
1.29.09 / 4pm


paulo miyada: Ethics is a crucial topic, which is why I have spent so much time and space on this blog discussing it. In schools, the teachers should address this topic frequently. If they fail to do so, it is the responsibility of the students to raise their questions within the whole class.
7. peridotlogism
2.2.09 / 6pm


LW – what is the goal of a ‘great' school of design education?
9. [lebbeuswoods](http://www.lebbeuswoods.net)
2.2.09 / 10pm


peridotlogism: The answer to your question will appear in a forthcoming post, (probably) entitled Architecture School 201. To give you a broad answer, I would say a great school of architecture is one that educates architects to go out into the world and do all they can to change it in ways they think it should be changed—and gives them the tools to do it.
11. martin
2.2.09 / 10pm


how serendipitous. i just finished applying to graduate school this afternoon.


what you are describing here, which seems fairly accurate, is nearly impossible to pin down. it can be incredibly difficult for an applicant to understand the full breadth of a school, short of spending some significant time with/at/around/inside the program in question. advice from graduates or current students, no matter how detailed, doesn't always cover all the bases of the quality of the school, reviews always seem superficial, information on the web is too vague, and *current* course descriptions are elusive at best.


in my experience in undergraduate school (penn state) i realized that the true dynamics and quality of the program weren't revealed to me until well into my 4th year.


i guess my question is: how can an applicant truly evaluate a school's quality as you have described it?


by the way, for which school are you currently working (if any) ? i believe your name has come up on at least 3 faculty lists that i have seen..
13. [nina barbuto](http://ninamariebarbuto.blogspot.com)
2.3.09 / 9am


thank you Lebbeus. I completely agree. I went on to grad school with the sole purpose of one day teaching architecture. I am looking for some place now where I can truly bring excitement and exploration back to the studio. Architecture school and teachers shouldn't be about their CV, as we now how it is now with teachers jet setting in and out and forgetting their students names let alone projects. What recommendations do you have for people like me who want to teach as much as learn from students? and ps. do you know of any one hiring now?
15. peridotlogism
2.3.09 / 6pm


I'll respond further on that post then. Thanks!
17. Spencer
2.5.09 / 8am


Lebbeus,


If, ““There are no bad students.” What he meant was that young people who aspire to become architects and have gone through an admissions and selection process have demonstrated in advance a potential that should be respected,” is true and there are good and great students everywhere then I have to wonder if it is really the school that makes the faculty great? After all, teachers are going to find these students at every school. So, it seems reasonable to say that it is the schools resources and reputation that attracts the teacher. I mean, honestly, most faculty members want to be teaching at the most reputable school with the most assets? After all, Harvard offers more to a teacher than Ball State just like Ball State offers more than the University of Idaho (seems reasonable, right?). Correct me if I am wrong, but I've also never heard of a professor interviewing students to measure their goodness or greatness when selecting a school to teach at. I suspect they, like the student, choose the school more by reputation and resources (being different for both of course).


There are some exceptions to this. About 10 years ago I was inspired when I heard that both Catherine Ingraham and Jennifer Bloomer, both accomplished educators, were teaching at the Iowa State University. Both had taught at more reputable schools but for some reason, along with a several other teachers of similar quality, decided to travel out to the center of our country to a less know school of architecture. I'd like to think they decided to do this to reach those students who wouldn't normally be exposed to the higher level of instruction.


So, I can't help but wonder where this leaves the less reputable schools with the less serious peers and more importantly the good and great students who attend those schools. 


It's easy to say they supplement their education with determination and the inspiration of a “Lone Ranger.” In my experience it is even more rare to see such a thing happen. When I look around at the built environment I am reminded there are many more uninspired architects in the world than inspired as I realize that an architect designed that strip mall, that an architect put a stamp on the designs for a new Shuck's Autoparts in my neighborhood and that thoughtless two story addition to a less than glamorous house next door to my friend's house was likely to be designed by someone who went to architecture school. 


In my opinion, the education system short changes the majority students resulting in many terrible buildings that we will have to live to see built and then torn down. It's a waste of our resources that we can not afford to waste. I'm certainly not saying that all buildings have to be great but that we could stand to have less of the uninspired ones. I think a large portion of them could be avoided if more teachers like yourself took bold steps to reach those good and great students who do not have the luxury and fortunate to attend the more reputable schools.


I do look forward with anticipation to the continuation of this thread of ideas and possibly some resolution to my discontent.
19. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.18.10 / 4pm


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
21. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.18.10 / 11pm


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
23. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] school on his blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
25. [Lebbeus Woods on Architecture School | Architecture EV](http://architectureev.wordpress.com/2011/01/02/lebbeus-woods-on-architecture-school/)
1.2.11 / 6pm


[…] Architecture School 101 Architecture School 102 Architecture School 201 Architecture School 202 Architecture School 301 Architecture School 302 […]
27. stefanus hadi nugroho kurniawan
4.18.11 / 1pm


i'll wait the next pahse
29. Pedro Esteban
5.27.11 / 8am


ok, let's read the other post
31. [Students are the other half of any school's story | a Walk in the Parq](http://euvgparq.wordpress.com/2012/01/02/students-are-the-other-half-of-any-schools-story/)
1.2.12 / 12am


[…] propósito do Balanço de Semestre que agora finda, obrigatório (mesmo) ler o texto Architecture School, da autoria de Lebbeus Woods. Gostar disto:GostoBe the first to like this . Esta entrada foi […]
