
---
title: SAME DIFFERENCE
date: 2009-01-13 00:00:00
---

# SAME DIFFERENCE



One of the main problems in the present age of globalism and individualism is how to reconcile—and sustain—their paradoxical coexistence.


The disappearance of distinct local cultures as a result of the global expansion of consumerism and its brand-names, franchises, and the mass-marketing of look-alike products shows no signs of slowing down. Sooner or later, we might imagine, every human settlement will look more or less the same. Already, the newer urban centers where the population has grown recently and rapidly are difficult to distinguish architecturally, as the same building types, same technologies, and even the same globe-trotting architects, are defining their skylines. In the streets, local languages and customs still survive in differing degrees, at least for now.


Parallel with this aesthetic and cultural development has been an increase in the value of human identity. Throughout the world, people are concerned with affirming their individual identities in a public way. The rapid emergence of internet sites where any person can post personal information available to everyone is a prominent example. This can be seen as a reaction to the homogenization created by consumerism, yet it is also part of a broad political change in the present society that emphasizes human rights to a historically unprecedented degree. Perhaps, as some have pointed out, it is consumerism itself, both in its marketing propaganda and the ubiquity of credit-card buying, that has fostered the demand for publicly asserted personal identity, but it is also the growing belief that each person has a right to have a say in forming and directing everything from the election of political leaders and the policies of government to public opinion about anything. The internet and news media are jammed with opinion polls and personal blogs of every description, and while their actual impact is unclear, it is certain this is a trend that shows no signs of slowing down.


The conflicts between or, paradoxically, the merging of these two seemingly contradictory movements in culture, politics, and society is one of the epic human projects presently unfolding. Skeptics claim that the widespread public push for personal identity is the dying gasp of individuality in the flood of consumerist conformity engulfing the world. Optimists claim that their coexistence is the first stage in the emergence of an entirely new and more complex, even more subtle, form of human society, a step up in human evolution.


In the realm of aesthetics—which is the flipside of ethics—it is clear that new definitions and distinctions must be made. The age of the singular and original object standing out from an aesthetically anonymous background (the Medieval model) is ending. Appropriation, the death of the author, and most especially of the genius, “mechanical reproduction”—not only in product and print, but also in virtual and electronic form—signal new aesthetic techniques, and also new aesthetic values and criteria of judgment. The emphasis is shifting to the background—the field—which is no longer anonymous and uniform, but alive with the unique, the singular, mass-produced and not. In such a time, designers, artists, and architects must rethink and redefine, in visual terms, the global field condition. They must learn to see, and enable, variations in the field—the aesthetically and socially complex field—and for that task what they most need is the capability to perceive, and conceptualize, the differences in radical similarities.


LW



![cwn11](media/cwn11.jpg)


![cwn2](media/cwn2.jpg)


![cwn3](media/cwn3.jpg)


![cwn4](media/cwn4.jpg)


![cwn5](media/cwn5.jpg)



 ## Comments 
1. [Alex Bowles](http://www.otofog.net)
1.13.09 / 10pm


From the perspective of somebody who works in the media trades, I can say that this shift is (a) absolutly real, and (b) profoundly disconcerting. 


If you extend the concept of architecture to encompass the infrastructure formed around capitol-intensive production tools and distribution platforms – all used to proliferate analog signals – then the shift to a peer-to-peer network capable of reducing all signals to a single, digital denominator is an almost complete inversion. 


No wonder the role of the audience – as a self-aware medium in their own right – is evolving in the direction LW outlines here.
3. Marc K
1.14.09 / 6am


One question popping immediately into my mind unfortunately finds itself directed at you. Architects that have reached such a level of discernment from their peers tend to control where such a globalization of ideas may flow, being that their work harnesses the values of people both emotionally and aesthetically. How then, does a person as yourself speak to your individuality, when today it is so closely examined and closely mimicked (with inevitable failure) by so many? How does a man in your current position [individually] respond to such global treatment?
5. Nikola Gradinski
1.14.09 / 8pm


The era of all encompassing globalism, as it advances is certainly displacing the individual, in terms of the person, the idea, objects, etc. I find this post very optimistic especially in regard to the free use of the term ‘human rights', one that can be added to a previous post on ‘dead words', given its public flogging in forms such as the mass media, where it is used to condemn and to justify, among other propaganda uses. The ‘increasing value of human identity' is a trend that must surely be only occurring in the minds of free thinking individualists as the (controlled) world is certainly becoming increasingly less tolerant of the idea of the ‘individual'. The proliferation of the expression of free individual thought, in the form of blogs etc., appears to be a dissident movement in the face of the mass mind control apparatus that is the mass media. It may also be argued that this ‘virtual' freedom of expression may form an outlet for public frustration, while actual freedom slowly disappears. This becomes an escapist fantasy world, which ultimately results in another type of prison, or at the very least a controlled environment in which a certain prescribed degree of unpredictable activity is allowed to occur, producing a disarming cathartic effect on its participants, absolving them (at least in their minds) from taking any form of meaningful action in the real world. Simultaneously, the pursuit of shall we say, ‘otherness', is being aggressively stamped out by the fast evolving corporate prescribed 21st century concept of ‘identity', rendering it not only undesirable, but utterly obsolete. Therefore one can be an individual and exercise what they see as ‘personal choice' in a wide ranging plethora of ‘interests' as long as it falls within the boundaries of the afore mentioned prescriptions of the system. Even more disturbingly, this over saturation of the environment with ‘choices' or more precisely, ‘no choices' coupled with mass media indoctrination has produced the ultimate passive worker citizen, one who has descended into utter apathy. 


The implications for architects, specifically, seem somewhat dire, at least in the traditional sense. This (d)evolution has rendered the entire profession impotent and reduced it to a hyper-specialized service mechanism, providing each individual within it an ever more myopic view of the construct loosely termed as ‘reality', save for a privileged few, perhaps. We seem to have no choice left other than to reject the current system and look towards constructing a new reality, and take the first steps to true evolution.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
1.14.09 / 11pm


Nicola Gradinski: You lay out the case for a pessimistic view of the current situation very clearly and succinctly. I cannot disagree with what you say, yet at the same time am looking for a way forward other than “reject the current system.” The current ‘system' is all we have to work with, unless you—like Marx and Lenin—have a better one in mind. I don't. However, we can think about how to emphasize the strengths of the present system, even against the momentum of many others that emphasizes the weaknesses. 


You articulate the weaknesses very well. I would hope we can articulate the strengths equally well. For me, these include the mandate each of us has to take responsibility for our common conditions, and to change those we believe should be changed—one by one, day by day—with fierceness of imagination and dedication to what it is we do best. We can, in short, transform the ‘system' from within. If this is not true, then mere resistance is futile. I'm not willing to accept that.
9. peridotlogism
1.15.09 / 6pm


LW – responding to your last paragraph, especially the second sentence.


The subtext of this passage suggests an appreciation of power-to-change in the ‘civilized' systems of the collective. Basically, your position is about the responsibilities of others.


Freedom and comfort tend to exist with an inverse relationship. Our lives, societies and systems function that very unconscious assertion polished and made an idol everyday by the steps we keep taking (and not taking) to remain comfortable and critical. 


The current system is not all we have – the current system is what we allow ourselves. Real revolution would so far removed from the familiar and comfortable (and one must ask what is it that seeks comfort vs freedom) that it is terrifying to even sense that reality. I am a cynic and I share Nicola's position – and optimistic and pleasant revolution is only virtual.
11. [rob](http://eatingbark.covblogs.com)
1.16.09 / 8pm


Nikola, peridot — 


I suspect you are being at once too pessimistic and not pessimistic enough (or cynical, if you prefer that term).


I am guessing (and I could be projecting this onto you, so correct me if I am wrong) that you are hoping that there will be (or can be) a “true evolution”, at some point in the future, which would move the state of human existence from an unacceptable (current) state to an acceptable (if not fully perfected) state. 


I would suggest that this is unlikely to ever happen; the (real) revolution has not occured because the revolution cannot and will not ever occur. My evidence that this is true would be somewhat tautological: it is unlikely that such a revolution will occur because such a revolution has not occurred, and vice versa. This is because human existence, being finite, is fundamentally imperfect (i.e. is not perfectable). This would be where I think you are being insufficiently pessimistic.


If — perhaps only if — this is correct, then it seems likely that you are also being too pessimistic, that Lebbeus is correct — the current ‘system' (a rather reductionistic simplification given the diversity and breadth of current human existence — I probably prefer ‘reality', as ‘system' implies a degree of consistency that I don't think you can ascribe to what is happening) is what we have to work with. We should then be satisfied with making individual changes (in the sense of “one at a time”, not in the sense of excluding collective action), particularly so long as those changes are made with “fierceness of imagination”. This is where I think your insufficient (universal) pessimism produces excessive (localized) pessimism.
13. [lebbeuswoods](http://www.lebbeuswoods.net)
1.16.09 / 9pm


rob: it's the best when a discussion occurs between those who comment, and so intelligently. I hope Nikola and Peridotlogism have read your comment and are able to respond….
15. [Kush Patel](http://www.thought-folio.blogspot.com/)
1.18.09 / 10pm


LW – Thank you for such a profound and thoughtful piece. I am fascinated with your argument around the paradoxical nature of our current times and the need for architects to perceive and conceive differences in increasingly but apparently similar global surrounds. In my research, I am drawn toward works of critical social theorists and a few spatial-theorists who along similar lines have attempted to reexamine the kind of spaces that current tensions produce. As philosopher Henri Lefebvre (1974,1991 tr.) himself argues that a *praxis* is what is needed to see through the mystifications of abstract capitalist space. Only then can we consciously and deliberately create new spaces.


Perhaps, the concern then should not be whether or not the local will survive the homogenizing global or for what length of time. For I would argue, that the local and the global will themselves cease to be fixed categories and instead become part of a plural, inclusive and an ever transforming continuum – a “body without organs” (Deleuze & Guattari) at once formed and continuously forming. 


These are truly interesting times.
17. Nikola Gradinski
1.19.09 / 5pm


rob: I agree with your point about the ‘revolution' as you put it, and that it will never occur, but not with your explanation. Waiting for some ‘deus ex machina' to solve all problems will mean waiting a very long time, eternity perhaps, while others shape the world into ‘states of humanity', that may become more or less appealing to ‘society' as they continue to float like a leaf on the stream, powerless. 


The ‘state of humanity' should not be the driving concern here as it is essentially an abstract and unquantifiable value, one that is neither ‘acceptable' nor ‘unacceptable'. It is precisely the ‘hope of future events' to come, or ‘fear of impending doom', that removes us not only as architects, but as human beings from the now, the only moment, to use that misnomer loosely, that exists. Essentially it is the state of the individual relative to itself that is important, therefore a change in oneself begins to shape the whole. It is here that we must effect this change, a change of the most subtle and exact nature. This inward perspective responds harmonically to the failings of the outward system as outlined previously, and to other phenomena such as the death of the ‘genius' relative to his peers, the singular object relative to its background etc. The system can be rejected therefore by working on oneself, the evolution is happening now, here, we only have to begin participating in it. 


This ‘architecture' of the individual, begins to form the ever increasing intricate organic fibers of the flux that shapes the world. The resulting diversity within the flux will begin to erode the mass consensus state – one which is slow, solid, unbalanced, reluctant to change, controlled, and therefore altogether unnatural, which can be, for the purposes of my previous post, the definition of ‘system'. 


We must go beyond the infinite, and embrace the ever evolving cyclical change that we must rise to re-invent for ourselves every day.
19. [stefan](http://www.stefansaal.com)
1.23.09 / 2am


lebbeus, a couple of points…


first, i'd like to note how well your stuff works with the blog format, the scrolling down of the drawings and so forth. it puts me in mind of a chinese ink landscape, or a sort of vertical scroll…the vertiginous cities…you could almost create some long, continuous things just for this format…


secondly, as i maneuver around the web, i am often struck by how much is available, how many creative voices are out here, how quickly i can dart from place to place, looking things up, checking in, seeing individual things, exploring, it's like standing at the world's magazine rack…


btw, i'm not sure artists “must” do anything, but stay true to themselves…
21. peridotlogism
1.23.09 / 3pm


“This is where I think your insufficient (universal) pessimism produces excessive (localized) pessimism.”


Consider me revolved ; )
23. [rob](http://eatingbark.covblogs.com)
1.23.09 / 8pm


Nikola —


I would, with some qualification, agree that “the state of humanity” is, as you say, “an abstract and unquanitifiable value” (my qualification would be that I prefer “oversimplification” as a description of what is wrong with the term, because that allows for the possibility that it is occasionally useful to simplify a matter as well as acknowledging the danger inherent in doing so). 


I would ask you to consider, though, that you are doing the same thing (which you reject in the case of “the state of humanity”) when you speak of a “system” or a “mass consensus state” — oversimplifying. Reality contains a vast multiplicity of systems, which overlap and interact with one another in so many places and ways that it is very hard for me to see how speaking of the whole as a single entity is useful, other than as an expression of a sense of dissatisfaction with the world (which might be, as I suggested above, derived from insufficient (universal) pessimisim — much (local) pessimism is derived from an (unrealistic) expectation that we possess the ability to perfect reality, or at the very least, to do away with most of the things that trouble us). While from one perspective (a perspective that I might suggest is unrealistic) a multiplicity of systems might appear to be part of “the system”, talking about them as one entity elides very real and significant differences between, say (as a convenient example), the governing system of the US under President Obama and under President Bush, who, from one perspective, are both beholden to various assumptions embedded in the American political process (American exceptionalism and an excessively easy acquiesence to consumerist culture might be a nice pair of pernicious examples) but must also been seen from a perspective which admits that they have serious differences in governing philosophy and aims.


I think the language of ‘system' reveals a particular way of thinking that employs oversimplification as a means to the rationalization of reality. It is certainly possible to be engaged in a process of rationalization even while calling for flux and diversity — the opposition of “system” and “flux/diversity” is an oversimplification of the relationship between two kinds of things, which in reality exhibit a much more complicated relationship (as, for instance, in the post “re: THE SYSTEM”, where Lebbeus gives us one example of a system acting to preserve the individual, which is where you seem to be exclusively locating flux/diversity). This seems to me to be the same form of rationalization common in the late 19th century/early 20th — Hegel, Marx, etc. — which posits ‘oppositions' as the best way of understanding reality.
25. [rob](http://eatingbark.covblogs.com)
1.23.09 / 9pm


At the beginning of the second paragraph in the previous comment, “that you are” should read “that you might be”.
27. peridotlogism
1.26.09 / 7pm


Rob, sugar, where is your blog? I am up for that read. Blog me towards a truer understanding of reality!
29. [rob](http://eatingbark.covblogs.com)
1.26.09 / 9pm


peridot (why do I feel like peridot is your first name and logism your last?) — I can't promise my blog will get you a truer understanding of reality or anything, but you're welcome to read it: <http://eatingbark.covblogs.com>. At times its had a fair bit of architecture-ish stuff, at other times not. At some point in the near future, the architectural stuff will find a new home at [mammoth](http://m.ammoth.us). And then eatingbark will finally be fully dedicated to posting soccer/music videos and excerpting mcsweeney's articles.
31. [rob](http://eatingbark.covblogs.com)
1.27.09 / 4pm


peridotlogism — I can't promise my blog will get you a truer understanding of reality or anything, but you're welcome to read it: <http://eatingbark.covblogs.com>. At times its had a fair percentage of the posts devoted to architecture and landscape, at other times not. At some point in the near future, the architectural stuff will find a new home at <http://m.ammoth.us>. And then eatingbark will finally be fully dedicated to posting soccer/music videos and excerpting mcsweeney's articles.
33. egrec
1.30.09 / 9pm


LW et al,


Not long after reading this post, I came across an essay (linked below) by the technological thinker Kevin Kelly. Though not directly related to the specific issues raised in this discussion (meaning there may be a better post to comment on with this), I feel it is relevant (in a general, theoretical sense) to the idea of difference; of the physical and cosmic nature of individuality itself. This passage in particular:


“We see the slope all around us in many ways. Because of entropy, fast moving things slow down, order fizzles into chaos, and it costs something for any type of difference or individuality to remain unique. Each difference – whether of speed, structure, or behavior – becomes less different very quickly because every action leaks energy down the tilt. Difference is not free. It has to be maintained against the grain.”


the link to the piece is here if anyone wants to read it:  

<http://www.kk.org/thetechnium/archives/2009/01/the_cosmic_gene.php>
35. [Noticias de Arquitectura](http://www.noticiasdearquitectura.blogspot.com)
2.13.09 / 9pm


It seems that the actual conflict between the art and science is taken us, “artists”, designers, architects, painters ando so on, to believe that the principal cause of this “paradoxical coexistence” is the natural evolution of the new technologies.


I think it's wrong !!!


I think the principal problem nowadays is the emerging crises on the creation of new concepts that is a process that we think, in a wrong way, depends on new technologies, computers, media, images, etc…


Today, more than before, we need isolation from the reality to understand the new needs and perheps we can find a way to develop some new concepts.


Global is and only the information, not art.


Art or the act of, will be always started by one individual, and not globally.


Sorry about my english, i hope you understand what i say.


thank you for this article.
37. [THE DREAMS THAT STUFF IS MADE OF « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/04/the-dreams-that-stuff-is-made-of/)
1.4.11 / 1am


[…] support social justice in any form. These drawings aspire to finding, in spatial terms, what I have elsewhere called ‘the differences in radical similarities.' Too grandiose? No doubt. But such are the […]
