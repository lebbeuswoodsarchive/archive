
---
title: SLOW MANIFESTO
date: 2009-01-07 00:00:00
---

# SLOW MANIFESTO



The new cities demand an architecture that rises from and sinks back into fluidity, into the turbulence of a continually changing matrix of conditions, into an eternal, ceaseless flux—-architecture drawing its sinews from webbings of shifting forces, from patterns of unpredictable movements, from abrupt changes of mind, alterations of position, spontaneous disintegrations and syntheses—-architecture resisting change, even as it flows from it, struggling to crystallize and become eternal, even as it is broken and scattered—-architecture seeking nobility of presence, yet possessed of the knowledge that only the incomplete can claim nobility in a world of the gratuitous, the packaged, the promoted, the already sold—-architecture seeking persistence in a world of the eternally perishing, itself giving way to the necessity of its moment—-architecture writhing, twisted, rising, and pinioned to the uncertain moment, but not martyred, or sentimental, or pathetic, the coldness of its surfaces resisting all comfort—-architecture that moves, slowly or quickly, delicately or violently, resisting the false assurance of stability—-architecture that comforts, but only those who ask for no comfort—-architecture of gypsies, who are driven from place to place, because they have no home—-architecture of circuses, transient and unknown, but for the day and night of their departure—-architecture of migrants, fleeing the advent of night's bitter hunger—-architecture of a philosophy of interference, the forms of which are infinitely varied, a vocabulary of words spoken only once, then forgotten—-architecture bending and bending more, in continual struggle against gravity, against time, against, against, against—-barbaric architecture, rough and insolent in its vitality and pride—-sinuous architecture, winding endlessly and through a scaffolding of reasons—-architecture caught in sudden light, then broken in a continuum of darkness—-architecture embracing the sudden shifts of its too-delicate forms, therefore indifferent to its own destruction—-architecture that destroys, but only with the coldness of profound respect—-neglected architecture, insisting that its own beauty is deeper yet—-abandoned architecture, not waiting to be filled, but serene in its transcendence—-architecture that transmits the feel of movements and shifts, resonating with every force applied to it, because it both resists and gives way—-architecture that moves, the better to gain its poise—-architecture that insults politicians, because they cannot claim it as their own—-architecture whose forms and spaces are the causes of rebellions, against them, against the world that brought them into being—-architecture drawn as though it were already built—architecture built as though it had never been drawn—


LW




 ## Comments 
1. egrec
1.8.09 / 8am


LW,


It would be great if, in this retrospective mode of late, you could also reproduce the Glossary from *Anarchitecture: Architecture is a Political Act.*


I recognize much of the above manifesto from published work, so perhaps you are already considering this…
3. [lebbeuswoods](http://www.lebbeuswoods.net)
1.8.09 / 1pm


egrec: you must have the Pamphlet Architecture 15: War and Architecture. It's the only publication (that I recall) of this manifesto. I posted it in the belief/hope that it is still as fresh as it was when I wrote it.


Also, I'll look into your specific request.


There will be more retrospection, as some still interesting work is out of print, or, like the Four Cities, never properly published at all. Of course, the posted texts for this project were written only now.
5. Hamish Buchanan
1.8.09 / 10pm


“architecture that comforts, but only those who ask for no comfort”


How fucking, self-aggrandizingly moralistic: as if to ask for basic human consideration or decency should condemn one to hell? Who are you to judge?
7. [lebbeuswoods](http://www.lebbeuswoods.net)
1.8.09 / 10pm


Hamish Buchanan: Nowhere in the manifesto does it say those who ask for comfort should not receive it. They (we) just won't get it from the architecture evoked by the manifesto.
9. egrec
1.9.09 / 8am


LW,


A version of it appears at the end of an essay in *Radical Reconstruction*, which I assume was taken from the *War and Architecture* pamphlet.
11. Penelope
1.10.09 / 8pm


Dear Lebbeus, 


In your manifesto you refer to concepts and ideas such as the ‘incomplete form', the uncertain moment, the unknown, the philosophy of interference, ‘architecture built as though it had never been drawn'. These are some of the ideas that distinguish your visionary cities (your manifesto) from modernist's visionary cities, which did not leave much space for the unknown and the unpredictable. Shouldn't a manifesto be open to ideas yet not thought in order to be really rebelling? 


Penelope


p.s. The four cities posts are amazing (it would be great to publish them!).
13. egrec
1.11.09 / 9am


(I second Penelope in that I would gladly pay for a paper edition of the four cities material)
15. [Alex Bowles](http://www.otofog.com)
1.11.09 / 5pm


Third. Production on the scale of *The New City* would be especially grand. 


As an aside, I had to pay $75 for a fairly well worn copy of the same, and that was after putting in a ‘let me know if one of these ever comes up for sale' request on Amazon's used book finder. It was out there for more than six months before the dog-eared copy I (very) happily bought finally hit the market. 


Seems like the Cities of Earth, Fire, Water, and Air aren't the only ones in demand.
17. peridotlogism
1.13.09 / 5pm


This manifesto is an act of totality seeking fruition. Perhaps all manifestos are. It doesn't seem as if architecture is really that important – The four cities are really Four Peoples. To talk of this in terms of ‘the human factor' is to insert an opaque projection which keeps things just as they are, embellishing a pleasure in the virtual.
19. rui
1.21.11 / 4pm


dear Lebbeus,


i read this manifesto every day and i am inspired by it. this architecture reminds me of the urban fabric of the shanty towns of lagos, where i grew up. I have come to look again at the magic of what i once did not want to see
21. gareth lloyd
2.19.11 / 3pm


Like !
23. Eli K.
11.21.11 / 9pm


much like the words of Martin Heidegger, architecture that results in a struggle perpetuates our way of being in the world, an exisetnce that is always violent, always a strife, always trying to change one another. It is this type of architecture that stays open to its inhabitants and allows us to dwell with in it. Architecture that is designed for no one, but supplies everyone, accidental in nature, that is to say not formed by coincidence but rather exposing what is hidden as Verillio describes it. Like a glass falling of a table, revealing its fragile nature, this collision of elements reveals our nature as beings and lets us simply be. I have enjoyed tour words very much, and not for the first time.
