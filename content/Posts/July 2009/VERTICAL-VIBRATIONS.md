
---
title: VERTICAL VIBRATIONS
date: 2009-07-05 00:00:00
---

# VERTICAL VIBRATIONS


Under our current social and economic regime, we are accustomed to thinking of towers as iconic objects. Tall buildings stand alone, even when they are crowded together in denser urban centers, a bit like people at a party. Each is individual, with a unique profile and countenance, yet uncannily similar to others. Their similarities allow them to be members of a group, even a community, providing a common basis for conversation between them. Their differences do so also—as long as they are not too pronounced—and allow each of them to claim a unique identity. This combination of uniqueness and similarity suits the corporate, institutional reality they are meant to symbolize: the clients who commission them want to be distinct, so we can identify—and buy, in one way or another—what they are selling, from products to services to ideas and ideologies. Yet they want to be seen as compatible with the familiar cultural norms. Every architect who designs such towers knows very well this story of a delicate balance between the novel and the acceptable.


However, this may not be the only tower game in town. Actually, we would hope it is not, because it offers a narrow range of options, as we can see from urban centers emerging around the world. One reason is that tall buildings are limited in their general form not only by property lines and accompanying air rights, but also by structural requirements. Tall buildings are enormously heavy and their weight must be supported by unusually solid foundations. Their relatively extreme heights subject them not only to immense vertical forces of compression, but to tensile forces from bending in response to lateral wind pressures and the occasional earthquake. Their center of gravity is along a generally vertical line perpendicular to the surface of the earth—too much leaning or tilting and they simply break and fall. New technologies and materials may extend the range of vertical eccentricity they can sustain, but only by a few degrees. The basic tower envelope is fixed, a given that any architect must live with.


 Perhaps, then, if we want to escape from the tower shape game (presently the only game in town) it is time to think about towers differently: not as iconic forms covering a predetermined structural frame; nor as symbols of institutional identity and the social authority that comes with the financial capability to commission and own tall buildings; but rather as tightly bound bundles of physical, intellectual, and emotional energy. This is not the same as thinking like a structural engineer, who is only obliged to consider quantifiable physical forces acting within the tower in relation to the earth, but in a more broadly conceptual way, taking into account the meanings and character of tower spaces, how they might be used in a changing world, and why. If we leave aside the symbolism of a tower's overall shape and focus instead on the complex of forces active within it, we might discover the tower's fuller potential as a domain of thought, action, and aspiration.


 


![LWb-VERT-1](media/LWb-VERT-1.jpg)


![LWblog-VERT-2](media/LWblog-VERT-2.jpg)


![LWblog-VERT-3](media/LWblog-VERT-3.jpg)


![LWblog-VERT-4](media/LWblog-VERT-4.jpg)


![LWblog-VERT-5](media/LWblog-VERT-5.jpg)


![LWblog-VERT-7](media/LWblog-VERT-7.jpg)


![LWblog-VERT-6](media/LWblog-VERT-6.jpg)


LW


Also, refer to the post [TOWERSPACE](https://lebbeuswoods.wordpress.com/2008/05/19/tower-space/) on this blog.



 ## Comments 
1. sueb
7.6.09 / 6pm


What does the lines and colors mean, and why are they placed the way they are? I like your work and I would like to understand this better. 


Also, if I don't understand, would the general public understand and feel this thought, action, and aspiration that you are trying to convey?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.7.09 / 11am
	
	
	sueb and egrec: the lines are what I imagine to be the vectors of various energies active around a vertical space that is becoming a tower. The divisions of space are in formation, but also in continual flux. The overall shape of the tower (not drawn in these) will not be decided in advance, but will emerge—and re-emerge—over time. Another version of this type of tower is the proposal for the [World Center](https://lebbeuswoods.wordpress.com/2009/05/11/locus-of-memory/).
	
	
	As for the public, let us not underestimate people's powers of imagination, if they are inspired by an idea.
3. egrec
7.7.09 / 5am


sueb: The drawings seem to have been produced by marking over a cross-section or elevation of a tower according to apparent or perceived locii of “physical, intellectual, and emotional energy”, no?
5. egrec
7.16.09 / 2am


LW: thank you.
