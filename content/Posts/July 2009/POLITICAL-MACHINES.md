
---
title: POLITICAL MACHINES
date: 2009-07-23 00:00:00
---

# POLITICAL MACHINES


Cell phones are political machines *par excellance*. This has been shown several times in recent years but nowhere better than the demonstrations against the government in Tehran over the past weeks. Twitter and instant messaging as well as texting were used to organize the resistance and also to communicate its existence to the news media around the world. Still, people had to physically put themselves on the streets of the capital, claiming its public spaces as their own. The virtual world was still subordinate to the physical one, and actual space remained the prime contested territory, as the repressive, violent responses of riot police and the Basij militia demonstrated. If that is so, then we must concede that architecture—the design of physical space—still has a role to play in human affairs, even—or especially—those having to do with politics.


 We are well acquainted with the monumental architecture of official power, the large and expensive buildings that demonstrate the wealth of private corporations, arts institutions, and stable governments. But what about the architecture of resistance to established authority? What about the architecture of rapid political change? Such architecture cannot be expensive, because those who need it are not sponsored by banks and mortgage companies. Anyway, there isn't the time for the usual building process. Political architecture of this kind must be improvised, spontaneous.


 Politics is, at its best, a mechanism for people to change their lives for the better. This means empowering those who have been disempowered by prevailing institutions. The architects of such architecture can only provide concepts of designs for spaces enabling political change, as well as models for structures that serve as its mechanisms. However, they cannot expect that these models will be followed to the letter. Rather, they serve as inspirations and guides for those who will actually invent an architecture of change, from the materials and situation at hand, be they educated as architects or not. Better, I believe, that they are the best of educated architects, so they can bring the scope of their knowledge to bear on the task, and their instinct for the poetic in human experience.


*Freespace structures (leaning, bridging, suspended) inserted temporarily into the streets of Zagreb, Croatia, during the political crisis of 1991. Serving as communications centers and personal spaces, they become active instruments—machines—of change:*


*![LWblog-POLIT-7a](media/LWblog-POLIT-7a.jpg)*


![LWblog-POLIT-1a](media/LWblog-POLIT-1a.jpg)


![LWblog-POLIT-9a](media/LWblog-POLIT-9a.jpg)


![LWblog-POLIT-2a](media/LWblog-POLIT-2a.jpg)


![LWblog-POLIT-8](media/LWblog-POLIT-8.jpg)


![LWblog-POLIT-5a](media/LWblog-POLIT-5a.jpg)


![LWblog-POLIT-6a](media/LWblog-POLIT-6a.jpg)


![LWblog-POLIT-10a](media/LWblog-POLIT-10a.jpg)


![LWblog-POLIT-4a](media/LWblog-POLIT-4a.jpg)


LW



 ## Comments 
1. david
7.23.09 / 4pm


Those sure look expensive





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.23.09 / 5pm
	
	
	david: Labor-intensive.
3. martin
7.23.09 / 8pm


labor-of-love-intensive?


this makes me think of the state of “christiania” in copenhagen. It claims to be an autonomous state within denmark that is not officially recognized. they build their own structures and have their own economy, but have been in a constant battle with the administration and its arms (the police) since they first squatted there in 1971.


naturally, their buildings are not as visually compelling as your drawings, but they have a similar quality.


<http://en.wikipedia.org/wiki/Freetown_Christiania>
5. [what he said « p o s t e t h o t i c](http://postethotic.wordpress.com/2009/07/23/what-he-said/)
7.23.09 / 9pm


[…] building process. Political architecture of this kind must be improvised, spontaneous. — Lebbeus Woods Posted by postethotic Filed in 1 Leave a Comment […]
7. [wm](http://www.asasku.blogspot.com)
7.24.09 / 5am


I really wished that these ideas becomes a reality: a message and critics towards the worlds social-political conditions would be something refreshing today.


I'm expecting such complexity through Gehry's works but to my surprise when after watching most of video presentation on his work.. He basically sounded like he can just do whatever he like without no apparent reason or message.


Perhaps I'm too idealistic, but with such power through architecture, design have a bigger role and responsibility to the society other than a glossy space provider.
9. [Daily Digest for July 25th](http://howtoimplode.com/?p=100)
7.25.09 / 5pm


[…] Shared POLITICAL MACHINES […]
11. [Quadrus Penseroso](http://quadruspenseroso.wordpress.com/)
7.26.09 / 1am


The Kent State Commons Shootings, a Tiananmen Square Tank and a Tehran Basij Bullet cannot crush or kill your Angry-Ass Momma.


Our taxi driver, an enterprising young man driving his unsanctioned vehicle, cautiously approached the traffic circle which unofficially doubled as a town square and market. We brushed along side a pedestrian incident with only inches to spare. “Women on dis-Island are not to be mess'd wit mon – respect” he said with a brief leap of smiling maturity. 


Arms locked with her opponent, an enraged woman was delivering swift kicks with shocking intensity – in direction of her man's groin. 


By arching his upper body forward; arms outstretched, the skinny fellow wearing a neatly pressed shirt and pants barely managed to clear the arc of her ferocious hoofing – desperately clasping her hands to form a rigid apex. The old Rude Boy must have understood enduring public humiliation was a necessary punishment. 


Her diaphragm propelled obscenities at decibel levels rivaling concert PA equipment – threatening to seismically shift the buttressed affair into busy vehicular traffic on one side or into the murky waterway on the other.


Whether the Gothic arch between them was one of beaming triumph or agonizing pain really depends on where Smart People believe the Theory of Relativity actually came from.


Was she an obese, marginalized, indignant citizen disrupting the colonial version of peace, will authorities respond to 911 calls placed by “witnesses” – citizen journalists Twittering their amusement, uploading a grainy clip on YouTube — OR —- is she a real-life Towering Full Figure of Dignity in massive skin tight denims, her plenty filled blouse, gloriously bejeweled aura, dispensing ethical justice for all to see, hear and even touch, on a hapless under-employed philandering lumpen she foolishly dated and trusted? 


Did the public audience give rise to her unruly confidence, silently approving? Were the playful and informal spatial characteristic of the gathering place intoxicating? Onlookers appeared reluctant to intervene and were perhaps captivated; her embodied energy undoubtedly capable of stopping a military convoy. One can imagine the standstill: a senseless block of frozen, sterile time where the streets have no beginning or end … finally a brave soldier relents; others eventually disembarking their mighty machines, to enjoy laughter, Red Stripe, a good spliff, jerked chicken and endless arrangements of 4/4 time low tempo beats filling the air.


For Mainland theatre, it was a flash point: an interpretation of Verdi's Anvil Chorus for Il Trovatore – expressing the moment of comedic chaos. 


Once on Common Ground, the Autocrat and his machinery is no match for a vengeful and burning heart …
13. [InfoBore 20 « ubiwar . conflict in n dimensions](http://ubiwar.com/2009/07/27/infobore-20/)
7.27.09 / 7pm


[…] Political Machines – Lebbeus Woods […]
15. damir
8.1.09 / 6am


I must protest, although these words sound great: political, change, spontaneity, resistance, insurgent architecture… what physically are these drawn structures prescribing as change? clutter? more and more?  

It seems to me, the only space that required for such a change (if any) is lack there of more objects.  

Why is the first thought always to add something rather then subtract from it? Personally, i think the political game is and has been running the faucet cold. And instead of shutting the tap we add, build, prescribe, hot in hopes we change the temperature.  

Shutting the water every now and again seems more appropriate of a solution.
17. Kamyar Fanaei
8.8.09 / 7am


I can't help reacting to this text (post). The point which is forgotten here is all communication systems – infrastructures – are under intense control of government in Iran. They can easily – maybe just by clicking a button – shut down the short message service or reduce the internet widthband. A better look at recent events in Iran and precise following the real news may make your ideas more accurate.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.8.09 / 1pm
	
	
	Kamyar Fanaei: Thank you for this important clarification. I would point out that electronic messages and images from people in Tehran were still reaching the Western press, even at the height of the demonstrations and the government crackdown. I'm not sure how this was done. It seems obvious that there are ways to get around the censorship—there always are.
	
	
	
	
	
		1. Kamyar Fanaei
		8.17.09 / 10am
		
		
		Yeah, you are right. Although the Facebook.com (our main tool for communication in these days) is blocked in Iran, but thanks to Ultrasurf and Freegate we are able to visit filtered websites.  
		
		And I'm thinking about an architecture of change which encourage people for demanding better conditions.
19. Pedro Esteban
12.31.10 / 12pm


I'll put some you know where.  

In Cuba will do a new kind of internet, I don't know exactly how is the subject(better) but that's the idea.


Please can you speak about the technic of the first image. I don't care about your technics, for me that isn't matter, but I love architecture without photoshop perfect image.
21. [Lebbeus Woods – Political Machines « WorldTransformsItself](http://worldtransformsitself.wordpress.com/2011/03/18/lebbeus-woods-political-machines/)
3.18.11 / 2am


[…] *original text can be found at https://lebbeuswoods.wordpress.com/2009/07/23/political-machines/ […]
