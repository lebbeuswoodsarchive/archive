
---
title: STEVEN HOLL IN SHENZHEN
date: 2009-07-10 00:00:00
---

# STEVEN HOLL IN SHENZHEN


As he did with his *Linked Hybrid* towers in Beijing, Steven Holl—in his Shenzhen *Horizontal Skyscraper*—shows that he is the master of the large-scale, multi-use building. These projects transcend their generic description and are effectively new building types, as their metaphoric titles proclaim, in sharp and challenging contrast to the usual developer typologies. In Shenzhen, the large buildings hover above what is to be a lush tropical garden, freeing—to a remarkable degree—the ground-plane for public use. This concern for making open space for people to enjoy is a primary goal of Holl's urban designs, and sets them apart. The architectural forms are straightforward, even modest by today's flashy standards, but their presence on the landscape is extraordinary. In Shenzhen, it took some special engineering to make the long buildings span between a small number of vertical cores, creating a new type of public space, but Holl's team achieved this, and within the developer's budget. Now nearing completion, this project should become a model for others, not to be literally copied, but one that shows the decisive effect imagination and innovation can have. In a world where the number of large-scale commercial projects combining living and working spaces is certain to increase, and steadily, it offers inspiration, and hope that architecture can make a difference after all.


LW


![SH-Shen-3](media/SH-Shen-3.jpg)


![Site Plan_clean_BW](media/Site_Plan_clean_BW.jpg)


![Ground Floor Plan CLEAN](media/Ground_Floor_Plan_CLEAN.jpg)


![SH-Shen1](media/SH-Shen1.jpg)


![SH-Shen-2](media/SH-Shen-2.jpg)


![Sections and Elevations](media/Sections_and_Elevations.jpg)


![SH-Shen-4](media/SH-Shen-4.jpg)


The landscaping plan projects a lush tropical, public garden:


![080201 PLANT_INDEX.pdf](media/080201_PLANT_INDEX.pdf.jpg)


 


Credits (partial): Shenzhen Vanke Real Estate Company (client). **Vanke Center**, Shenzhen, China. **Steven Holl Architects**: Steven Holl, Li Hu (design); Li Hu (partner in charge). Associate Architects and Engineers: **CCDI**.



 ## Comments 
1. [PEJA](http://www.piliaemmanuele.wordpress.com)
7.16.09 / 10am


Steven Holl really is a great architect. I think his architecture may be accompanied to Jean Nouvel's Architecture: both work with perception, both work with modern composition of space, without falling into clichés of this way…
3. [Millennium People](http://millenniumppl.blogspot.com)
7.19.09 / 10pm


I don't think Jean Nouvel is completely comparible with Holl – the description above reminds me of his Quai Branly, yet this building really has none of its grace. I hardly see how this building is ‘making a difference', there is no overt social agenda that wasn't developed 50 years ago. No one still believes that freeing up the ground plane equals the best public usage, anymore than they believe the Modernist idea that any green space is positive green space. It strikes me as a commercial project, an office money-maker. I challenge anyone to defend that ‘these projects transcend their generic description and are effectively new building types.'
5. [STEVEN HOLL'S HORIZONTAL SKYSCRAPER « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/06/28/steven-holls-horizontal-skyscraper/)
6.28.11 / 7pm


[…] following commentary is an expanded version of one that appeared in a previous post. It attempts to give a summary historical background for the Horizontal Skyscraper […]
