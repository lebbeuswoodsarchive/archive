
---
title: COMMON GROUND  the seminar work
date: 2009-07-14 00:00:00
---

# COMMON GROUND: the seminar work


A selection of works by participants in the Cornell University [COMMON GROUND Seminar](https://lebbeuswoods.wordpress.com/2009/06/17/common-ground-the-seminar/):


 


JARUWAN THAVATKIATTISAK. The underside and “breathing holes”:


![Jaruwan-3](media/Jaruwan-3.jpg)


![Jaruwan-5](media/Jaruwan-5.jpg)


![Jaruwan-4](media/Jaruwan-4.jpg)


![Jaruwan-1](media/Jaruwan-1.jpg)


 


LANDON ROBINSON: “We control you”:


![Landon-1](media/Landon-1.jpg)


 


![Landon-2](media/Landon-2.jpg)


CHRISTOPHER LAURIAT. Small to large boundaries:


![Lauriat1](media/Lauriat1.jpg)


![Lauriat2](media/Lauriat2.jpg)


 


MITCHELL PRIDE. Seeing through to “the other side”:


![Mitchell-1](media/Mitchell-1.jpg)


![Mitchell-2](media/Mitchell-2.jpg)


 


THEA VON GELDERN. “She will be gone”:


![Thea-1](media/Thea-1.jpg)


![Thea-2](media/Thea-2.jpg)


 


JOSHUA NASON. “Facing” the common ground of the seminar space:


![Joshua5](media/Joshua5.jpg)


![Joshua6a](media/Joshua6a.jpg)


 


 


![Joshua-1a](media/Joshua-1a.jpg)


 


More work will be added as digital files become available.


Views of the COMMON GROUND EXHIBITION and DICUSSION:


![Review3](media/Review3.jpg)


![Mitchell3](media/Mitchell3.jpg)


![Review4](media/Review4.jpg)


![Review5](media/Review5.jpg)


![Review2](media/Review2.jpg)


![Review6](media/Review6.jpg)


**Common Ground Exhibition, Cornell-NYC, June 24, 2009**. Participants: Bogeng Chen, Grace Kim, Christopher Lauriat, Tien Ling, Joshua Nason, Manasi Pandey, Mitchell Pride, Landon Robinson, Jaruwan Thavatkiattisak, Thea von Geldern, Zhiqinag Wang, Siyuan Zhang, Yingdi Zhang, Milena Zindovic. Guest discussants: Miodrag Mitrasinovic (Parsons The New School for Design); Shannon Mattern (Department of Media Studies and Film, The New School); Anthony Titus (The Cooper Union). Faculty: Lebbeus Woods (The Cooper Union), Christoph a. Kumpusch (Cornell), Mark Morris (Cornell), Coordinator, M2 Program.



 ## Comments 
1. Christopher Lauriat
7.15.09 / 3pm


Lebbeus, thank you for posting the work. The combination of creative freedom and a tight focus generated a diversity and depth of investigation that was incredibly productive.


There seems to be a dialogue between Cooper and Cornell starting to coalesce into an intentional, fruitful exchange of ideas. I hope that this trend continues and even moves beyond just the two schools. Students and teachers (and administrators) will benefit from understanding the strengths and shortcomings of our institutions of learning. A participation in the greater community of architectural scholars can only enrich the discipline and challenge individuals to transcend a particular school of thought.
3. Joshua Nason
7.18.09 / 4am


As I review the work, once again, from the seminar I am impressed with the ideas that were made manifest in such a short time. A true diversity of thought is evident in the variety of work as well as the discourse that has been generated through the different explored processes, the exhibition and now the publishing of what occurred in a mere flash of time. To me, this speaks to the strength of working at a hastened pace.


In these moments as designers, we must rely on our instincts and learn to trust in ourselves but also learn to edit and focus very quickly. Through this, a cohesion is formed between rapid iterations and a requisite precision of criticism and refinement, which are then activated through rather cerebral modes of thought and effort. For me, it is a microcosm of layered creative processes that force one to deepen their intuition and engage their work in a very corporeal sense. It can be in many ways tactical but is in every way tactile.


Is it possible to always work like this? I hope so…
5. Diego Peñalver
7.20.09 / 8pm


Interesting works, visually attractive, but I wonder what was the seminar about, and the excibit..maybe a little introduction will help me understand.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.21.09 / 12pm
	
	
	Diego: I think the post titled COMMON GROUND: the seminar, gives the introduction you are looking for.
7. Diego Peñalver
7.21.09 / 6pm


Thanks Lebb,  

I missed that…will take a look.
