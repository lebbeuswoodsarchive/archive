
---
title: SLUMS  Misery (!) and Hope (?)
date: 2011-12-30 00:00:00 
tags: 
    - Jim_Yardley
    - Mumbai
    - slums
---

# SLUMS: Misery (!) and Hope (?)


[![](media/mum-41.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-41.jpg)


*Note: This article was originally published in the New York Times on December 28, 2011.*


##  **In One Slum, Misery, Work, Politics and Hope**


**By Jim Yardley**


MUMBAI, India — At the edge of India's greatest slum, Shaikh Mobin's decrepit shanty is cleaved like a wedding cake, four layers high and sliced down the middle. The missing half has been demolished. What remains appears ready for demolition, too, with temporary walls and a rickety corrugated roof.


Yet inside, carpenters are assembling furniture on the ground floor. One floor up, men are busily cutting and stitching blue jeans. Upstairs from them, workers are crouched over sewing machines, making blouses. And at the top, still more workers are fashioning men's suits and wedding apparel. One crumbling shanty. Four businesses.


In the labyrinthine slum known as Dharavi are 60,000 structures, many of them shanties, and as many as one million people living and working on a triangle of land barely two-thirds the size of Central Park in Manhattan. Dharavi is one of the world's most infamous slums, a cliché of Indian misery. It is also a churning hive of workshops with an annual economic output estimated to be $600 million to more than $1 billion.


“This is a parallel economy,” said Mr. Mobin, whose family is involved in several businesses in Dharavi. “In most developed countries, there is only one economy. But in India, there are two.”


India is a rising economic power, even as huge portions of its economy operate in the shadows. Its “formal” economy consists of businesses that pay taxes, adhere to labor regulations and burnish the country's global image. India's “informal” economy is everything else: the hundreds of millions of shopkeepers, farmers, construction workers, taxi drivers, street vendors, rag pickers, tailors, repairmen, middlemen, black marketeers and more.


This divide exists in other developing countries, but it is a chasm in India: experts estimate that the informal sector is responsible for the overwhelming majority of India's annual economic growth and as much as 90 percent of all employment. The informal economy exists largely outside government oversight and, in the case of slums like Dharavi, without government help or encouragement.


For years, India's government has tried with mixed success to increase industrial output by developing special economic zones to lure major manufacturers. Dharavi, by contrast, could be called a self-created special economic zone for the poor. It is a visual eyesore, a symbol of raw inequality that epitomizes the failure of policy makers to accommodate the millions of rural migrants searching for opportunity in Indian cities. It also underscores the determination of those migrants to come anyway.


“Economic opportunity in India still lies, to a large extent, in urban areas,” said Eswar Prasad, a leading economist. “The problem is that government hasn't provided easy channels to be employed in the formal sector. So the informal sector is where the activity lies.”


Dharavi is Dickens and Horatio Alger and Upton Sinclair. It is ingrained in the Indian imagination, depicted in books or Bollywood movies, as well as in the Oscar-winning hit “Slumdog Millionaire.” Dharavi has been examined in a Harvard Business School case study and dissected by urban planners from Europe to Japan. Yet merely trying to define Dharavi is contested.


“Maybe to anyone who has not seen Dharavi, Dharavi is a slum, a huge slum,” said Gautam Chatterjee, the principal secretary overseeing the Housing Ministry in Maharashtra State. “But I have also looked at Dharavi as a city within a city, an informal city.”


It is an informal city as layered as Mr. Mobin's sheared building — and as fragile. Plans to raze and redevelop Dharavi into a “normal” neighborhood have stirred a debate about what would be gained but also about what might be lost by trying to control and regulate Dharavi. Every layer of Dharavi, when exposed, reveals something far more complicated, and organic, than the concept of a slum as merely a warehouse for the poor.


One slum. Four layers. Four realities.


On the ground floor is misery.


One floor up is work.


Another floor up is politics.


And at the top is hope.


“Dharavi,” said Hariram Tanwar, 64, a local businessman, “is a mini-India.”


[![](media/mum-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-1.jpg)


[![](media/mum-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-2.jpg)


**Misery**


The streets smell of sewage and sweets. There are not enough toilets. There is not enough water. There is not enough space. Laborers sleep in sheds known as pongal houses, six men, maybe eight, packed into a single, tiny room — multiplied by many tiny rooms. Hygiene is terrible. Diarrhea and malaria are common. Tuberculosis floats in the air, spread by coughing or spitting. Dharavi, like the epic slums of Karachi, Pakistan, or Rio de Janeiro, is often categorized as a problem still unsolved, an emblem of inequity pressing against Mumbai, India's richest and most glamorous city. A walk through Dharavi is a journey through a dank maze of ever-narrowing passages until the shanties press together so tightly that daylight barely reaches the footpaths below, as if the slum were a great urban [rain forest](http://topics.nytimes.com/top/news/science/topics/forests_and_forestry/rain_forests/index.html?inline=nyt-classifier), covered by a canopy of smoke and sheet metal.


Traffic bleats. Flies and mosquitoes settle on roadside carts of fruit and atop the hides of wandering goats. Ten families share a single water tap, with water flowing through the pipes for less than three hours every day, enough time for everyone to fill a cistern or two. Toilets are communal, with a charge of 3 cents to defecate. Sewage flows through narrow, open channels, slow-moving streams of green water and garbage.


At the slum's periphery, Sion Hospital treats 3,000 patients every day, many from Dharavi, often children who are malnourished or have asthma or diarrhea. Premature tooth decay is so widespread in children that doctors call them dental cripples.


“People who come to Dharavi or other slum areas — their priority is not health,” said Dr. Pallavi Shelke, who works in Dharavi. “Their priority is earning.”


And that is what is perhaps most surprising about the misery of Dharavi: people come voluntarily. They have for decades. Dharavi once was known for gangs and violence, but today Dharavi is about work. Tempers sometimes flare, fights break out, but the police say the crime rate is actually quite low, even lower than in wealthier, less densely populated areas of the city. An outsider can walk through the slum and never feel threatened.


Misery is everywhere, as in miserable conditions, as in hardship. But people here do not speak of being miserable. People speak about trying to get ahead.


**Work**


The order was for 2,700 briefcases, custom-made gifts for a large bank to distribute during the Hindu holiday of Diwali. The bank contacted a supplier, which contacted a leather-goods store, which sent the order to a manufacturer. Had the order been placed in China, it probably would have landed in one of the huge coastal factories that employ thousands of rural migrants and have made China a manufacturing powerhouse.


In India, the order landed in the Dharavi workshop of Mohammed Asif. Mr. Asif's work force consists of 22 men, who sit cross-legged beside mounds of soft, black leather, an informal assembly line, except that the factory floor is a cramped room doubling as a dormitory: the workers sleep above, in a loft. The briefcases were due in two weeks.


“They work hard,” Mr. Asif said. “They work from 8 in the morning until 11 at night because the more they do, the more they will earn to send back to their families. They come here to earn.”


Unlike China, India does not have colossal manufacturing districts because India has chosen not to follow the East Asian development model of building a modern economy by starting with low-skill manufacturing. If China's authoritarian leaders have deliberately steered the country's surplus rural work force into urban factories, Indian leaders have done little to promote job opportunities in cities for rural migrants. In fact, right-wing political parties in Mumbai have led sometimes-violent campaigns against migrants.


Yet India's rural migrants, desperate to escape poverty, flock to the cities anyway. Dharavi is an industrial gnat compared with China's manufacturing heartland — and the working conditions in the slum are almost certainly worse than those in major Chinese factories — but Dharavi does seem to share China's can-do spirit. Almost everything imaginable is made in Dharavi, much of it for sale in India, yet much of it exported around the world.


Today, Dharavi is as much a case study in industrial evolution as a slum. Before the 1980s, Dharavi had tanneries that dumped their effluent into the surrounding marshlands. Laborers came from southern India, especially the state of Tamil Nadu, many of them Muslims or lower-caste Hindus, fleeing drought, starvation or caste discrimination. Once Tamil Nadu's economy strengthened, migrants began arriving from poverty-stricken states in central India.


Later, the tanneries were closed down for environmental reasons, moving south to the city of Chennai, or to other slums elsewhere. Yet Dharavi had a skilled labor force, as well as cheap costs for workshops and workers, and informal networks between suppliers, middlemen and workshops. So Dharavi's leather trade moved up the value chain, as small workshops used raw leather processed elsewhere to make handbags for some of the priciest stores in India.


During this same period, Dharavi's migration waves became a torrent, as people streamed out of Bihar and Uttar Pradesh, the teeming, backward northern states now at the locus of rural Indian poverty.


“After 1990, [immigration](http://topics.nytimes.com/top/reference/timestopics/subjects/i/immigration_and_refugees/index.html?inline=nyt-classifier) was tremendous,” said Ramachandra Korde, a longtime civic activist commonly known around Dharavi as Bhau, or brother. “It used to be that 100 to 300 to 400 people came to Dharavi every day. Just to earn bread and butter.”


Leatherwork is now a major industry in Dharavi, but only one. Small garment factories have proliferated throughout the slum, making children's clothes or women's dresses for the Indian market or export abroad. According to a 2007 study sponsored by the United States Agency for International Development, Dharavi has at least 500 large garment workshops (defined as having 50 or more sewing machines) and about 3,000 smaller ones. Then there are the 5,000 leather shops. Then there are the food processors that make snacks for the rest of India.


And then still more: printmakers, embroiderers and, most of all, the vast recycling operations that sort, clean and reprocess much of India's discarded plastic.


“We are cleaning the dirt of the country,” said Fareed Siddiqui, the general secretary of the Dharavi Businessmen's Welfare Association.


Mr. Asif, the leather shop owner, is a typical member of Dharavi's entrepreneur class.


Now 35, he arrived at the slum in 1988, leaving his village in Bihar after hearing about Dharavi from another family. He jumped on a train to Mumbai. He was 12.


“Someone from my village used to live here,” he said. “We were poor and had nothing.”


Mr. Asif began as an apprentice in a leather shop, learning how to use the heavy cutting scissors, then the sewing machines that stitch the seams on leather goods, until he finally opened his own shop. As a poor migrant, Mr. Asif could never have arranged the loans and workspace if Dharavi were part of the organized economy; he rents his workshop from the owner of the leather-goods store, who got the order from the supplier for the briefcases for the bank.


Today, nearly all of Mr. Asif's workers are also from Bihar, one of the myriad personal networks that help direct migrants out of the villages. Mohammad Wazair earns roughly 6,000 rupees a month, or about $120, as a laborer in Mr. Asif's workshop. He sends about half home every month to support his wife and two children. He is illiterate, but he is now paying for his children to attend a modest private school in their village. He visits them twice a year.


“In the village, what options do we have?” he asked. “We can either work in the fields or drive a rickshaw. What is the future in that? Here, I can learn a skill and earn money. At least my children will get an education.”


[![](media/mum-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-5.jpg)


[![](media/mum-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-6.jpg)


**Politics**


“Now the place is gold,” said Mr. Mobin, the businessman.


He is sitting on the top floor of his building, surrounded by men's suits in the apparel shop. His family began in the leather business in the 1970s and has since moved into plastic recycling, garments and real estate. Slum property might not seem like a good investment, but Dharavi is now one of the most valuable pieces of real estate in Mumbai. Which is a problem, as Mr. Mobin sees it.


“People from all over the city, and the politicians, are making hue and cry that Dharavi must be developed,” he said. “But they are not developing it for the people of Dharavi. They will provide office buildings and shopping for the richer class.”


As Mumbai came to symbolize India's expanding economy — and the country's expanding inequality — Dharavi began attracting wider attention. Mumbai grew as Dharavi grew. If the slum once sat on the periphery, it now is a scar in the middle of what is a peninsular, land-starved city — an eyesore and embarrassment, if also a harbinger of a broader problem.


Today, more than eight million people live in Mumbai's slums, according to some estimates, a huge figure that accounts for more than half the city's population. Many people live in slums because they cannot afford to live anywhere else, and government efforts to build affordable housing have been woefully inadequate. But many newer slums are also microversions of Dharavi's informal economy. Some newer migrants even come to Dharavi to learn new skills, as if Dharavi were a slum franchising operation.


“Dharavi is becoming their steppingstone,” said Vineet Joglekar, a civic leader here. “They learn jobs, and then they go to some other slum and set up there.”


Dharavi still exists on the margins. Few businesses pay taxes. Few residents have formal title to their land. Political parties court the slum for votes and have slowly delivered things taken for granted elsewhere: some toilets, water spigots.


But the main political response to Dharavi's unorthodox success has been to try to raze it. India's political class discovered Dharavi in the 1980s, when any migrant who jabbed four posts into an empty patch of dirt could claim a homestead. Land was scarce, and some people began dumping stones or refuse to fill the marshes at the edge of the Arabian Sea.


Rajiv Gandhi, then India's prime minister, saw the teeming slum and earmarked one billion rupees, or about $20 million, for a program to build affordable, hygienic housing for Dharavi's poor. Local officials siphoned off some of the money for other municipal projects while also building some tenements that today are badly decayed. The proliferation of shanties continued.


Three decades later, the basic impulse set in motion by Mr. Gandhi — that Dharavi should be redeveloped and somehow standardized — still prevails. But the incentives have changed. Dharavi's land is now worth hundreds of millions of dollars. Private developers do not see a slum but a piece of property convenient to the airport, surrounded by train stations and adjacent to a sleek office park.


A sweeping plan approved in 2006 would provide free apartments and commercial space to many Dharavi residents while allowing private investors to develop additional space for sale at market rates. Many Dharavi civic and business leaders endorsed the plan, even as critics denounced the proposal as a giveaway to rich developers.


For now, the project remains largely stalled, embroiled in bureaucratic infighting, even as a different, existential debate is under way about the potential risks of redeveloping Dharavi and shredding the informal networks that bind it together.


“They are talking about redeveloping Dharavi,” said Mohammad Khurshid Sheikh, who owns a leather shop. “But if they do, the whole chain may break down. These businesses can work because Dharavi attracts labor. People can work here and sleep in the workshop. If there is redevelopment, they will not get that room so cheap. They will not come back here.”


Matias Echanove, an architect and urban planner, has long argued that Dharavi should not be dismissed as merely a slum, since it operates as a contained residential and commercial city. He said razing Dharavi, or even completely redeveloping it, would only push residents into other slums.


“They are going to create actual, real slums,” he said. “Nobody is saying Dharavi is a paradise. But we need to understand the dynamics, so that when there is an intervention by the government, it doesn't destroy what is there.”


[![](media/mum-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-7.jpg)


[![](media/mum-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-8.jpg)


[![](media/mum-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-9.jpg)


**Hope**


Sylva Vanita Baskar was born in Dharavi. She is now 39, already a widow. Her husband lost his vigor and then his life to tuberculosis. She borrowed money to pay for his care, and now she rents her spare room to four laborers for an extra $40 a month. She lives in a room with her four children. Two sons sleep in a makeshift bed. She and her two youngest children sleep on straw mats on the stone floor.


“They do everything together,” she said, explaining how her children endure such tight quarters. “They fight together. They study together.”


The computer sits on a small table beside the bed, protected, purchased for $354 from savings, even though the family has no Internet connection. The oldest son stores his work on a pen drive and prints it somewhere else. Ms. Baskar, a seamstress, spends five months' worth of her income, almost $400, to send three of her children to private schools. Her daughter wants to be a flight attendant. Her youngest son, a mechanical engineer.


“My daughter is getting a better education, and she will get a better job,” Ms. Baskar said. “The children's lives should be better. Whatever hardships we face are fine.”


Education is hope in Dharavi. On a recent afternoon outside St. Anthony's, a parochial school in the slum, Hindu mothers in saris waited for their children beside Muslim mothers in [burqas](http://topics.nytimes.com/top/reference/timestopics/subjects/m/muslim_veiling/index.html?inline=nyt-classifier). The parents were not concerned about the crucifix on the wall; they wanted their children to learn English, the language considered to be a ticket out of the slums in India.


Once, many parents in Dharavi sent their children to work, not to school, and [child labor](http://topics.nytimes.com/top/reference/timestopics/subjects/c/child_labor/index.html?inline=nyt-classifier) remains a problem in some workshops. Dharavi's children have always endured a stigma. When parents tried to send their sons and daughters outside the slum for schooling, the Dharavi students often received a bitter greeting.


“Sometimes, the teacher would not accept our children, or would treat them with contempt,” said Mohammad Hashim, 64. “Sometimes, they would say, ‘Why are you Dharavi children over here?' ”


Mr. Hashim responded by opening his own school, tailored for Muslim children, offering a state-approved secular education. He initially offered the curriculum in Urdu but not a single parent enrolled a child. He switched to English, and now his classrooms are overflowing with Muslim students.


Discrimination is still common toward Dharavi. Residents complain that they are routinely rejected for credit cards if they list a Dharavi address. Private banks are reluctant to make loans to businessmen in Dharavi or to open branches. Part of this stigma is as much about social structure as about living in the slum itself.


“They all belong to the untouchables caste,” said Mr. Korde, the longtime social activist, “or are Muslims.”


But money talks in Mumbai, and Dharavi now has money, even millionaires, mixed in with its misery and poverty. Mohammad Mustaqueem, 57, arrived as a 13-year-old boy. He slept outside, in one of the narrow alleyways, and remembers being showered with garbage as people tossed it out in the morning. Today, Mr. Mustaqueem has 300 employees in 12 different garment workshops in Dharavi, with an annual turnover of about $2.5 million a year. He owns property in Dharavi worth $20 million.


“When I came here, I was empty-handed,” he said. “Now I have everything.”


Dharavi's fingerprints continue to be found across Mumbai's economy and beyond, even if few people realize it. Mr. Asif, the leather shop owner, made leather folders used to deliver dinner checks at the city's most famous hotel, the Taj Mahal Palace. The tasty snacks found in Mumbai's finest confectionaries? Made in Dharavi. The exquisite leather handbags sold in expensive shops? Often made in Dharavi.


“There are hundreds of Dharavis flourishing in the city,” boasted Mr. Mobin, the businessman. “Every slum has its businesses. Every kind of business is there in the slums.”


But surely, Mr. Mobin is asked, there are things not made in Dharavi. Surely not airplanes, for example.


“But we recycle waste for the airlines,” he answered proudly. “Cups and food containers.”


*Hari Kumar contributed reporting.*


[![](media/mum-31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/mum-31.jpg)


#Jim_Yardley #Mumbai #slums
 ## Comments 
1. [SLUMS: Misery (!) and Hope (?) « LEBBEUS WOODS « shareitorstopit](http://shareitorstopit.wordpress.com/2011/12/30/slums-misery-and-hope-lebbeus-woods/)
12.30.11 / 4pm


[…] SLUMS: Misery (!) and Hope (?) « LEBBEUS WOODS. Share this:TwitterFacebookLike this:LikeBe the first to like this post. ← Previous post […]
3. zale
1.4.12 / 4pm


“It is a visual eyesore, a symbol of raw inequality that epitomizes the failure of policy makers to accommodate the millions of rural migrants searching for opportunity in Indian cities”
5. Prasad
1.12.12 / 8am


Its the same story retold several times. Does it feel good to see change there in Dharavi.. or does it by reading a new review about this place from a very new person.
7. elan fessler
6.7.12 / 12pm


<http://www.artinfo.com/news/story/807260/learning-from-lagos-contemporary-architects-harvest-the-slums-for-design-inspiration>
