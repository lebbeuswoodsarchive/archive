
---
title: MICHAEL SORKIN  Sidewalks of New York 2
date: 2011-12-27 00:00:00 
tags: 
    - Michael_Sorkin
    - New_York
    - public_space
    - sidewalks
---

# MICHAEL SORKIN: Sidewalks of New York 2


[![](media/letters_ny_into_01.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/letters_ny_into_01.jpg)


A day or so after the posting of his “sidewalk manifesto” (as I, not he, calls it) Michael Sorkin sent along the images shown below, requesting that I simply add them to the original post containing only his text. I felt uncomfortable doing this, and will try to explain why.


The text attracted me because it immediately sparked my imagination. The words alone offered many possible interpretations. It is this range of design implications that validate the concepts' place in the idea of a better, more egalitarian New York, making them worthy of being written into law.


However, when I received the renderings of the specific design proposals of the Michael Sorkin Studio that implement the text's concepts, the potential of the text to serve egalitarian or democratic purposes was diminished. Indeed, appearing next to these design images, the text can be read as only an argument for these particular designs, and not as broad statements of principle.


For me, the text “manifesto” and the designs are both worthy of our attention and critical evaluation. The only solution to presenting them without compromise to either text or designs, is to post the design images separately, allowing the blog's readers to relate them as they choose.


LW


[![](media/amsterdam-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/amsterdam-1a.jpg)


*(above) Amsterdam Avenue.*


[![](media/147thharlem-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/147thharlem-1a.jpg)


(above) 147th Street, Harlem.


#Michael_Sorkin #New_York #public_space #sidewalks
 ## Comments 
1. [Jim Meredith](http://www.archizoo.com)
12.27.11 / 7pm


The manifesto seemed to propose micro-local choice and promised texture, granularity, diversity and choice rather than central and authoritative planning. The renderings, however, seem to erase architectural differentiation, individual and neighborhood identity, evolution and diversity of culture, and the patina of time and use, and overlay the city with a single-value imperialism – the triumph of commercially-dominated (whose bubble-wrapped greenhouses are those that are more plentiful than people?) urban agriculture.
3. Aaron
12.27.11 / 7pm


Yeesh…  

The renderings are almost offensive (particularly 147th street) where a nice farming couple seemingly from vermont are placed in a very sanitized future green space. It's a nice concept that smacks of gentrification when illustrated as it has been.
5. [vanderleun](http://americandigest.org)
12.27.11 / 10pm


Two different handfuls of the same eco-horseshit.
7. [MICHAEL SORKIN: Sidewalks of New York 2 « LEBBEUS WOODS « shareitorstopit](http://shareitorstopit.wordpress.com/2011/12/28/michael-sorkin-sidewalks-of-new-york-2-lebbeus-woods/)
12.28.11 / 4pm


[…] MICHAEL SORKIN: Sidewalks of New York 2 « LEBBEUS WOODS. Share this:TwitterFacebookLike this:LikeBe the first to like this post. ← Previous post […]
9. [Ethan Kent (@ebkent)](http://twitter.com/ebkent)
12.29.11 / 3am


I agree. I loved the manifesto and saw it as strong and inspiring. These images though embody the misdirection and ineffectual approach of the design disciplines and how they are precluding successful urbanism with greenwashed, design-led solutions. We are calling this “eco brutalism”, and it has has become the dominate culture of design, and the only way to win competitions. It is directly undermining urbanism, environmentalism, and what cities and architects can do to adress environmentalism. It is also vastly limiting the role of, and demand for, “good design” in the public realm.


These equally radical photosims from our NYC Streets Renaissance Campaign (<http://www.pps.org/projects/new-york-city-streets-renaissance/>) were actually used by NYC DOT back in 2006 and led directly to the creation of the current policy and public plaza program. Some of them came out of community processes as a reflection of what that community would like to become. Many of these and others we did as part of the campaign have or are in the process of being implemented.  

<http://www.streetfilms.org/public-space-transformations/>


Some more recent ones done for the upper west side including a similar stretch of Amsterdam Ave to the one above:  

<http://www.streetsblog.org/2007/11/06/envisioning-an-upper-west-side-streets-renaissance/>


We need a much more radical vision for our streets and advocates behind it. The NYC DOT has led one wave implementing the vision of communities with the support of advocates, but the dominance of traffic and allocation of space to cars has still not been fundamentally challenged. The next push has to come from the advocacy community again.
11. Michael Sorkin
12.29.11 / 4pm


I am abashed at the hostility, misreading, and lack of solidarity in these posts. The manifesto was sent to Leb as part of a conversation we were having about OWS and he asked if he could post it. Later, he requested images and I sent the two controversial photoshop gems along. They were done some time ago as part of a larger project – New York City (Steady) State – that is investigating the limits of urban self-sufficiency. Our research looks the technical, morphological, social, and economic implications of urban autonomy, a thought experiment that attempts to press the limits of autarky and egalitarianism in the context of New York. The two images – of sites near where I teach – are part of the volume of the study dealing with food, which examines potentials for production at a variety of scales throughout the city. Within these images can be observed small greenhouses, cisterns, windmills, permeable paving, organoponicos a la Havana, vertical farms, façade growing systems, and other off-the-shelf elements of urban agricultural infrastructure. We see these in a family relationship with the fire-escapes, hydrants, street lights, water tanks, light and air legislation, and other distributed elements that shape the character of the picturesque tenement block that Leb has posted. The idea that these elements are necessarily the interventions of ConAgra or Archer Daniels Midland is thoughtless and misses the point about the expanded nature of the public realm under a regime of local sustainability. Minimal attention to these images should also reveal (Yo! Kents!) that the major portion of the street has been taken out of the automotive realm and returned to more authentically public uses: mass transit, pedestrianism, the production of food, recreation, etc.


Our study is, in fact, an inquiry. One thing we have learned is that vertical agriculture, despite its charms, is massively inefficient. As part of our investigation, we have designed a system in which the city does, indeed, grow all its food within its boundaries (using vertical farms as a major element) but analysis reveals that this can only be done with insane energy inputs: we estimate 25 power plants of Indian Point scale would be necessary to achieve such “autonomy”. This perverse outcome has led us to look for a variety of sweet spots that bring agriculture to individuals and neighborhoods but in which there is a more defensible balance between costs and social benefits. Indeed, the point of the exercise – underway now for more than half a decade – is precisely to look beyond green-wash at the actual forms and numbers that would be required to radically transform the city in the direction of greater self-reliance and greater responsibility for the consequences of our own respiration. In an era of dilatory and ineffectual nation states and hegemonic transnationals, this project is frankly political. I believe that the city is the logical increment of organization for democratic resistance to the disenfranchising homogenizations of multinational culture and the massive infringements of our rights and possibilities so succinctly critiqued by OWS


I would suggest that those of you who were taken with the manifesto might try to draw (out) some of its implications for actual sites in the city. I'm sure Leb would be happy to post them here. We need an expanded repertoire of possibilities in the face of dire times for our bodies, our body politic, and our urban tissue – not internecine sniping. Let a thousand blocks blossom!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.29.11 / 5pm
	
	
	Michael: I would indeed be glad to publish designs implementing the crucial aspects of your manifesto, as I already have published yours. But I must caution that all images are ambiguous—even those made with precision of representation. What we think they mean might be interpreted quite differently by others. It's not easy, but we must find ways to employ the ambiguity in service of our ideas. I learned this from the reception of my early drawings for the reconstruction of war-damaged buildings and towns, which were often greeted with “hostility” and “misreading.” I was abashed at first, but eventually realized that my ways of representing—visually and verbally—had to change in order to communicate my ideas—which I would not change—better to others.
	3. Peter Willis
	12.30.11 / 7pm
	
	
	This project wouldn't be such a bad idea if Manhattan was a Communist island with a population of 250,000 vegetarians. Apparently, exponential population growth is not something he understands. Our sewer, right of way, utility easements and subway system will be replaced with soil to plant cabbage. Irrigation system? What's that? 
	
	
	I liked this project better when it was called Broadacre City. At least FLW understood the importance of pragmatism.
	
	
	Building fenestration will be obsolete as foliage meshes will be given priority. Who needs light and air in their abode's anyways? Minorities will not exist. 
	
	
	Lack of solidarity? Are you really this naive? Architects don't congratulate or cover each other's arses the way doctors do (just go to any arch. school critique). Internecine and public sniping is part of the collective culture. If you can't stand the heat, stay out of the kitchen…or better yet, go write another naive book in your Ivory Tower without any regards to actual feasibility.
	
	
	
	
	
		1. Peter Willis
		12.30.11 / 7pm
		
		
		Leb,
		
		
		The hostility and misreadings of your earlier work has nothing to do with you, but understand that people are more likely to believe what they see with their eyes and not what they hear with their ears. As architects, we must prioritize the importance that visual impact has over whatever we preach textually or verbally.
13. Michael Sorkin
12.29.11 / 7pm


Point taken. Creation is a patient search! Ditto representation. As your work so abundantly reveals, there can indeed be ambiguity in precision and vice versa. Such ambiguity may be useful as a goad and a challenge but not if it obfuscates real principles. Real criticism should have the power to change ideas. Yours does.
15. Lulu Jackson
12.29.11 / 7pm


I love it when architects that have no sense of agricultural feasibility pull naive ideas out of their arse. Its akin to a child saying “I want to fly to Jupiter when I grow up”. All a parent can respond is “drink your warm milk before going to bed”.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.29.11 / 8pm
	
	
	Lulu Jackson: Do you have children? If you do, I hope that the next time they bring up the Jupiter idea, you won't patronize them but say, “if you really, really want to and work really, really hard, you will.” And they might!
17. Icreeight
12.30.11 / 1am


Conceptually intriguing, aesthetically challenging. There is so much beauty in the first (before) photo with the facades and fire escapes. One can only imagine the dynamism as the shadows shift daily, creating a sculptural play on the building faces and the awnings on the sidewalks. It would be a shame to lose this. The concept is admirable, however the esthetic is fuzzy and literally feels fuzzy. And, for me personally, it doesn't feel welcoming. A garden is only beautiful at times, even if it is diligently maintained. Where are the brown, dying or dormant plants in the images? Sadly they will exist. Think of planter boxes initially embraced and often ultimately ignored. Conceptually intriguing though.
19. [metamechanics](http://metamechanics.wordpress.com)
12.31.11 / 1am


not sure if I should open with “well well” or i have had at least 10 beers by 8:30pm?!? equally impressive right?  

anyway…


1 – cutting edge theoretical architectural theory on a regular basis ends up on Mr. Woods blog? anyone disagree?  

2 – Mr. Sorkin has been around for a long time working on his theories, educating, etc…..correct?  

3 – so if Mr. Sorkin does the municipalities a favor and writes in buliding code fashion a manifesto all about the future of argricultural information age (Toffler did not predict), he would be cutting to the chase and expressing psuedo-futurism-about-to-happen


 psuedo-futurism-about-to-happen – the Kardashians can't even keep up and if you had an idea close or slightly critical of Sorkin, now is the time to post it and be critical, because surely Mr. Sorkin was just goofing off one day when came up with this (sarcasm, see Sorkin's above posts and works).


4 – I plant and have planted corn since I was in junior high and let me tell you corn will grow anywhere, like a weed. none of those images are naive architects planting shit….give me a fire escape and I'll give you a corn field. 


5 – if Sorkin wasn't so famous the comments and anaylsis would be kinder…. I swear architects don't get out much…hey look at me I look like a Moma poster wearing all black.


6 – before we get to Jupiter we will colonize the moon and Mars and it will look like this – <http://metamechanics.wordpress.com/2011/07/09/future-architecture-on-mars/> 


plug plug.
21. Peter Willis
12.31.11 / 6am


I think part of the critique to Mr. Sorkin's manifesto is the evidence that there is a strong disconnect between academia and the “real world”. Architecture isn't the only profession where this exists. Just look at all the so-called economic experts at the start of the current recession spouting Keynesian economics, and look where we may be headed next. 


What's fantastic about Mr. Wood's blog is that it is an open forum where criticism and ideas can be exchanged. Even if it had not been Mr. Sorkin that proposed this manifesto, the level of harsh criticism isn't just limited to hazing students at school juries and architects at town hall meetings. It took only one year to build the Empire State Building, but ten years to just finalize a design on the WTC. The role of the architect has been greatly undermined because schools of thought parade a professor's personal ideologies and ephemeral trends over what the student may or may not be educated in in the long term. Did Mr. Sorkin even consult agriculturalists, day laborers or urban planners(practicing ones!) prior to proposing this?


Rodney Dangerfield's satire of this disconnect comes to mind:
23. D. Buonfrisco
1.6.12 / 12am


@peter willis: You're awfully disrespectful and grossly ignorant. Notice that nobody has responded to, let alone acknowledged, your comments. These two venerable men have not done so because they're wise enough not to waste their time. I guess im not… The internet is full of people like you who make wanton statements based on arrogantly ignorant assumption – which can all but ruin the quality and productiveness of online forums. This blog is one of the few places where you can consistently find discussion that is critical, productive, and with benevolent purpose. 


Please don't be that guy, its a bad look.
25. steve leitch
1.6.12 / 11pm


what mr sorkin is working on here is perhaps the greatest real architectural challenge that you folks will have to face…better get your heads out of the sand. How cool will NYC be when the oil runs out and your food stops rolling in every day on those big trucks? urban agricuture is either going to happen, or big cities as we know them are going to become very uncivilized places. 


but what your saying is, “drill baby drill” so that we can keep on doing things the way we always have, oh yeah, and fire escapes are more beautiful than plants…wow! very progressive.
27. [John Young](http://cryptome.org)
1.8.12 / 12am


Inspired by the architectural genius of the firebug in LA setting fire to toxic and hazardous vehicles offensively dominating streets, riversides and landscapes, could clear the way for Sorkin's intervention on behalf of uncanned humans barred from au naturel jay-walking across cities and landscapes. LA and Freewayed middle America shows what's coming from unchallenged vehicular, ship, aircraft and spacecraft addiction, their coddling with inhuman parking facilities, airports, docks, launch pads and grotequely, verily High-Lineish camouflaged, pedestrianisations which yet again presume vehicles must stay close by their contemptuous owners vulgarly valorized by photographic glossing with BMW-sponsored architectural roadshows which omit vile fumes and noise and bloated machine threats. 


Sorkin's “manifesto” is almot too kind to those of us cowed into accepting monstrous vehicles as a nesessary evil of architectural design, bombarded incessantly with TV promotions of automobile freedom from the hunkered office treadmill to CAD submissively to the tyranny of the property line. Not a burned wreck nor body part in these Guggenheim exhibits.


But no matter, the most important architectural question how to destroy the the ridiculous new World Trade Center, or entomb emptily as the Washington Monument as an example of what never again subject inhabitants to for economic predation and iconic Manhattan-skyline fetishism. 


I dream of an unlicensed liberated uncorked populace expanding crosswalks packing Bics and hurling Molotovs from overpasses at the enemy invaders.
