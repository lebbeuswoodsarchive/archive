
---
title: WHY COOPER UNION MATTERS
date: 2011-12-12 00:00:00 
tags: 
    - free_education
    - The_Cooper_Union
---

# WHY COOPER UNION MATTERS





*The following is the best overview of the crisis at The Cooper Union—including its renowned School of Architecture—that I have read so far. I pass it along to the readers of this blog everywhere because, as its author says, the outcome of the crisis will have meaning for all of us.*


LW


# Why Cooper Union Matters


by Litia Perta



On a clear night in early November, hundreds of people filed into the Great Hall at Cooper Union. By 7:00, the auditorium's 900 seats were full and hundreds of people crammed into standing room at the back. The event was not open to the public and security guards in the lobby were checking everyone for some form of Cooper ID. The current student body is counted at 918, so it only took a quick glance around to see that the event had drawn far more than just current students. Both faculty and alumnae had also come out in great numbers for the emergency meeting that had been called with Cooper Union's Chairperson of the Board of Trustees, Mark Epstein, and his much quieter fellow Board member Richard Lincer.


At issue was the recently leaked information that the Board of Trustees was considering charging tuition to Cooper students—a move that many believe would radically undermine the philosophy that is at the institution's core. Financial newspapers and business journals have reported widely in the last few years on the safety of Cooper's endowment and on the wisdom of many of its investment strategies, and so the news that the school carried a deficit of over eight million dollars during the summer of this year sent shockwaves throughout the community. When, only some months later, that deficit was recalculated and announced to be over 16 million, it sent people reeling. The late October leak that the Board seemed to have decided that converting Cooper Union to a tuition-based institution may be the only way to keep the school solvent was met with bewilderment by students and faculty members alike, who demanded to know what, exactly, was going on.


[![](media/perta-web1.jpg)](http://brooklynrail.org/article_image/image/8914/perta-web1.jpg "Vintage postcard of Cooper Union, circa 1917.")Vintage postcard of Cooper Union, circa 1917.
While financial transparency was unfortunately not one of the results of the November meeting, what did become clear was a paradigmatic divide between the representatives of the Cooper Board and the people who actually comprise the institution. Early on and then repeatedly throughout the meeting, Mr. Epstein assured those seated in the hall that current Cooper students would not be asked to pay tuition. This position was met initially with a kind of dumbfounded silence and then each time it was repeated, grumblings grew louder. It seemed difficult for Epstein to imagine that self-interest had not motivated the attendance of those gathered before him. Indeed, it seemed plain to all but Epstein that a collective purpose had stirred this group together.


Cooper Union is an all-scholarship institution. That means that students who get in (an exceedingly and increasingly difficult task) do not pay tuition for their education. In the current educational climate where astronomical tuition and routine hikes are the norm, Cooper's policy is both unusual and unique. Something very particular happens inside an all-scholarship classroom that simply does not happen anywhere else. Because every student is guaranteed a full scholarship, the playing field is leveled—there is no price on anyone's head—which creates a palpable bond between students that enhances learning. But something perhaps even more critical happens in this kind of environment. Because not a single Cooper student has taken out loans to pay impossibly high tuition fees, conversations and explorations can move beyond the instrumentalized notion of education that is so prevalent elsewhere.


It may be a rapidly receding notion, but the students at Cooper are engaged in nothing less than the pursuit of knowledge and thinking for their own sake. The philosophy that courses through the institution is that education is a higher good, one that enriches the individual and, in so doing, enriches the human community. In this framework, education has its own value—and this is what makes Cooper Union radical and worth saving, perhaps even worth imitating: It is operating on a fundamentally different idea of what education is, and what it can be. So unfamiliar has this notion become, so fully has it been absented from current educational discourse, that it now rings of privilege or luxury—some kind of Enlightenment-era credo available only to the patriarchal elite. But when did it become okay to think that if an idea or a theory does not have an immediate, measurable, quantifiable economic use value it is a privilege to learn about it? When did such complacency develop around this kind of argument, enabling it to become the silent and seemingly obvious norm?


Since the 1980s, universities have responded to the pressures of economy by increasingly commercializing themselves, selling their educations as a product. That education has faltered as a result of this is evident all around us. The discourse has become one of investment: Exorbitant loans are justified on the grounds of the value of the product they purport to put out—namely, students that generate income (which then, in theory, enables them to pay off their educational debt). This model keeps education squarely in the place of an instrument within a distinctly capitalist frame, and it has far-reaching consequences. It has already shaped the way schools prioritize disciplinary weight inside the curriculum so that humanities and arts budgets have dwindled to almost nothing. It has limited the nature of discussions in the classroom and the priorities of the students so that it has become commonplace for students to demand higher grades for mediocre work, because of an over-concern with their own marketability once they pass through the institution's walls. The difference, at Cooper Union, is that because education is regarded as a higher good and not exclusively as a marketable product, the learning process is able to move outside of a solely capitalist frame. An all-scholarship school makes other kinds of thought possible because of the freedom it allows for thinking and learning to roam outside of mere supply and demand, investment and product.


American higher education now justifies itself on economic grounds: that we are producing the workforce for a global world. Such rhetoric has defined its function and has become its purpose. When was the last time debates around education articulated the virtues of higher thought, scholarship, a liberal education in the sense of one that frees the mind? When was the last time we explored the impact an idea, a novel, or a work of art can have on a person and on what they might decide to do with it? The danger of losing Cooper Union to the privatized, tuition-based educational model is not simply that we would lose one of the last bastions of non-instrumentalized education. The danger lies also in the fact that it would be like jumping out of the frying pan and into the fire, as even tuition-based institutions are faltering everywhere. And we are only now beginning to conceive of the economic impact that six billion dollars of collective student loan debt will have for generations to come. At a time when conventional structures are failing all around us, this seems like the moment to re-invent, re-imagine, re-conceive of what education is, and so what a school like Cooper might be—perhaps even how other schools can follow its model. Instead, the current Board of Trustees threatens to revert to examples that arguably don't work. At the end of the long meeting with Chairperson Mark Epstein, Professor Sohnya Sayres, a humanities professor who has been at Cooper for two decades, spoke eloquently about how concerning it was to her that at no point had any Board member been able to articulate the value of what Cooper is, what it does, and what it stands for. Her point was both philosophical and financial: “If you can't defend it, you can't get people to donate to it!”


Sayres's point returns to the particular financial problem Cooper Union finds itself in, and the possible ways it might pull itself out. Both the new president, Jamshed Bharucha, and the Board of Trustees repeatedly talk about needing to generate more revenue in order to sustain the school. Indeed, while tuition seems to be the only solution they have seriously engaged, Bharucha has responded to community pressure by appointing a “revenue task force” to investigate other options, because tuition comes with its requisite problems. There are students of wealthy families at Cooper Union who could conceivably afford to pay tuition fees but as soon as the institution ceases being need-blind, it would find itself in the same strange boat as so many other schools that tend to have two admissions lists: those for the students they actually want and those for the students who can afford it. Whether or not Cooper could maintain the excellence that has been its reputation with such a divide is questionable.


Perhaps more concerning is that if Cooper entered into the tangle that would inevitably ensue by trying to charge tuition, it stands to jeopardize the peculiar tax status it now enjoys. In 1902, Cooper Union acquired the land that the Chrysler building now stands on. Each year, the owners of the building come up with property tax that would usually be paid to the city of New York but, in a strange series of contested court proceedings stretching back to 1931, that property tax gets paid directly to Cooper Union. This tax equivalency status is one of the institution's major sources of revenue. While President Bharucha has dismissed the idea that charging tuition could undermine this agreement, the precedent is one that has been historically difficult to defend—and it seems to hinge on the argument that the institution is of direct benefit to the city. In many ways, Peter Cooper's intention to provide an education specifically for underserved communities—for the working classes and poor women—is a mandate to which it would be wise to recommit. This is the link that allows a private school to benefit from public tax money. While it is easy to decry the agreement as inequitable, and while attempts have been made to tear it down (notably by the city's few Republican mayors), it represents a rare and important commitment to education—a commitment the institution would do well to match by pledging to more faithfully represent the demographic constituents of the city. Whether or not charging tuition would jeopardize this status is up for debate. What is certain is that there can be no guarantee that charging tuition would pull the school out of its troubles. And once tuition is charged, the school is no longer itself anyway.


In all of these discussions, the emphasis tends to be on the need to generate more revenue. What goes unmentioned or strategically obfuscated are the institution's itemized expenditures. How much does it really cost to educate someone? Of the commonplace tuition fees, how much goes to what are known as “administrative costs” and how much is direct instructional expense? It seems that in the turn towards privatized education, a Wall Street mentality has slipped into the mix: If you deliver a product and you do it well, you get a bonus, and that bonus—that administrative cost—is shouldered by the students you supposedly need more tuition from in order to educate.


The annual operating budget at Cooper Union has been quoted at 61 million dollars per year. While numbers have been bandied about that show total expenses, they are slippery at best and a clear picture of how much it costs to run the institution has yet to be released. Some information is available in the public tax record that Cooper, along with any other institution of higher learning, is required to file with the state. And these documents reflect a very disturbing trend. In the last 10 years, administrative costs at Cooper have doubled. Payments to the officers of the institution (the president and the various administrative and academic deans) have also doubled in a decade. This does not seem to have any direct relationship to an increase in the duties of these officers, nor does it seem to have a logical relationship to the number of students for which each officer is responsible. For example, at Cooper Union, the architecture school has the least number of students and the engineering school makes up the largest population. The assumption would be that the managerial work of the respective deans of these schools would be proportional to the number of students, and their compensation would reflect the same. But somehow, the dean of the engineering school is paid the least of the three schools' deans, and the architecture school dean is paid the most.


Similarly, while full-time faculty salaries have gone up only 2.5 percent per year, as is regulated by their union, salaries of the administrators have increased at much higher rates. In 2009, then President George Campbell's total compensation package was $668,473—which included a cash bonus (for what, exactly, it is unclear) in the amount of $175,000. Staggered by these numbers, I looked at many other schools to see if their administrators were getting comparable packages—schools that, unlike Cooper, depend on tuition revenue for significant percentages of their budgets. I looked at schools like Vassar (a school that stopped being need-blind in 1997 and currently charges upwards of fifty thousand dollars per year to its 2400 students) and found that George Campbell's compensation packages were well above average. He was also the only president of a college that I could find who received a cash bonus.


I have had a somewhat illustrious career as a second-class faculty member—i.e., an adjunct professor—teaching at some of the country's most esteemed institutions: Wesleyan University, Bard College, U.C. Berkeley, and Cooper Union among them. I was hired to teach at Cooper in 2006 when I was a doctoral candidate and my semester's fee for 14 weeks was $4,500. Sometime later, after completing my Ph.D. and spending a year as a post-doctoral fellow at the Center for the Humanities at Wesleyan University, I returned to Cooper Union. It was the fall of 2009 and I was re-hired by the college's dean of the humanities at the exact same rate I was paid in 2006. I was told at the time that the budget could not accommodate any fee increase for having received my Ph.D., nor could it afford paying any increase for the standard annual rates of inflation. That same year, the dean who hired me received a compensation package valued at $239,724.


Seventy percent of Cooper Union's classes are taught by people like me: non-tenured faculty. For the most part, we have no job security, no health insurance, and no hope for a salary increase; moreover, we are generally considered expendable in that, if we won't work under those conditions, the school can easily find someone who will. I find it troubling that the institution now justifies the need for more revenue without making public its detailed expenses. The vast disparity between the value Cooper assigns to my work as a teacher and to a dean's work as an administrator makes questionable the idea that it costs a lot to educate someone. Exactly where is the money going?


That there is a need to re-evaluate our educational priorities seems clear. Something that has gone unmentioned during the discussions around charging tuition at Cooper is the fact that student loan debt is the only kind of debt that can never be forgiven. Even a declaration of bankruptcy will not absolve you of it. I recently attended a meeting of the Arts & Labor working group that has grown out of the Occupy Wall Street movement. An older artist spoke to us about no longer being able to afford her student loan fees (fees for a loan she took out in 1968). The government has begun to take her loan payments out of her monthly disability and Social Security checks—a story that impacted many of us gathered there who easily saw ourselves in similar straits.


To teach at Cooper Union is, and always has been, an honor—not because of what I get paid or any accolades I may receive, but because of the sheer wonder of what happens inside a Cooper classroom. Of all the many places I have taught, it is the only place where the openness of thought, the eagerness around intellectual exploration, the transformative nature of a liberal—a liberating—education is both palpable and electric. If the institution is now in trouble, let all that are a part of it see the numbers so that everyone might participate in a solution. Now is the time to re-think and re-structure, to move toward a future model: one that values instruction, one that honors educational costs without inflating them, one that chooses to protect future student generations from the kind of debt peonage that is everywhere. A question worth asking is whether the current Board of Trustees is up for this kind of re-imagining. Is it wise to entrust those who got the school into this predicament with the task of getting it out?


What is needed now is a vision: a way of seeing long down the line to a time when perhaps there will be many all-scholarship schools, when the value of a free education is once again understood, proclaimed, protected. This debate does not only affect the community of Cooper Union. If you believe that all people should have the chance to broaden their minds, if you hope to engage in higher education, if you have children you want to send to college, if you struggle under the weight of student loan debt—then this is your fight, too.


**LITIA PERTA**


*This article [originally appeared](http://brooklynrail.org/2011/12/local/why-cooper-union-matters) in THE BROOKLYN RAIL, Critical Perspectives on Arts, Politics, and Culture, December 11, 2011-January 12, 2012 issue.*











#free_education #The_Cooper_Union
 ## Comments 
1. Ryan J. Simons
12.12.11 / 4pm


As someone who is currently amassing a mountain of student loan debt, the news of Cooper Union's possible charging of tuition was a bit disheartening to hear. I've always viewed Cooper as this tiny–in size, not prominence- beacon of hope within the current landscape of American higher education. I am extremely skeptical about the notion of “sponsored studios” within a school of architecture and feel many schools have become merely sponsored by student tuition & think providing them with a “skill set” such as an adeptness in CAD programs or an internship in a global corporate office is a big enough “return on their investment.” A few of my peers have an extremely base joke in which we refer to certain students as “Payroll”. While it sounds immature & dismissive, there is a sense that certain students were accepted based on their–or their parents- financial position and don't bring much to the table in terms of curiosity or creative energy. This sets up an immediate lack of community when such a divisive attitude is well-known & accepted throughout the education system enlarge. In conjunction with this article, I'd also like to pass along a brief post from Adbusters concerning the corporate sponsorship–perhaps “takeover” is a more relevant term- at the University of North Carolina: <http://bit.ly/rTYDH3>. I'm sure not everyone at the university felt is was okay for such an action, but I wonder how many people openly objected to this idea. How many people will it take for Cooper Union to reject the idea of tuition? At this stage in the proceedings, I'm sure there are plenty more questions than answers.
3. [Richard](http://www.commercialarchitects.info)
12.12.11 / 5pm


If there were no such thing as student loans or health insurance there would still be doctors and universities but both would be affordable.
5. [K. Bradley](http://theformover.com)
12.12.11 / 5pm


Fantastic article. The only part that sort of lost me was the assumption that the heads of each department should be compensated on the number of students under them. The head of the “renowned School of Architecture” deserves to be paid the most because that program is the most successful – it takes a lot of work to keep an architecture program engaged in the architecture community, including the fostering of a relevant faculty, Cooper Union is synonymous with the best of the best architecture students.





	1. Joe Viola
	12.13.11 / 5pm
	
	
	…as it is for Engineering. Each should be compensated the same – or perhaps it should be made in proportion to the contributions of their namesakes Chanin and Nerkin or their alumni. Why do some architects feel the need to consider themselves more relevant than anyone else?
7. [damir](http://www.vudesings.com)
12.12.11 / 6pm


‘Administrative' costs/salaries have segregated the group that is responsible for the decisions of the institution from its staff and students. The consequence of such segregation can be seen in any financial aid offices across the country. The infrastructure in place to discuss your 50k+ tuition is presented with only one solution: loans. This astonishing amount of money per year for some students discourages most from engaging in the discussion of money at all.  

I think this issue is a discussion of action or pessimism. As a graduate of RISD, the architecture school was in the process of appointing a new department head for the following year. Once the announcement was made to the students and adjunct faculty it was presented in a way that did not leave room for debate. The decision was made as it always had been. The students disagreed with the decision. They envisioned what the future of the school would hold for study abroad programs and future hired faculty under the appointed head and protested. They signed petitions, rallied the first year students and demanded the ability to vote as to who should be appointed from the current faculty. This was done without disrespect to any candidate. The architecture school heard their voices and responded with a vote which appointed the department head the students preferred.  

“We are threatened, the future is going to be more awful.” Can be read as a pessimistic cry or a call to action. It took graduating class, not affected by the appointment to stand up and protect what they have enjoyed for future classes. Challenge the future we have all seen too well and don't take for granted the benefit you currently have.
9. channa newman
12.14.11 / 12pm


This move to tuition needs to be resisted. Any plans on how to form meaningful resistance ?  

Cooper Union should be a model for all American institutions of high learning. As a professor and critic of the corporate state of American education I am not surprised that even Cooper Union falls into the hands of technocrats. Shame on them. This is the capitalists' reflex to a challenge that would demand of the self-serving “leaders” to consider sacrificing their own indecent private wealth — for a change.


As a mother of a Cooper Union graduate and a steady – albeit modest – I am disappointed. I make no contribution to anything but the public good .


Also, Cooper Union was to figure in my book on corporate-run education in America; it was to be cited as the example to follow. Who would have thought? (I go by the only rational approach: education should be as free as the air we breathe. Here goes the business culture privatizing the air!)


Perhaps it is not too late to block this anti-democratic move?  

I will be happy to join in.


Channa Newman, PhD  

Pittsburgh, Pa





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.14.11 / 1pm
	
	
	channa newman: You've got it right and your support matters. There are several initiatives by Cooper students, faculty, and alumni. For details, you might start with this student-run website: <http://freeasairandwater.net/>
11. [Kevin Slavin](http://www.facebook.com/kevinslavin)
12.17.11 / 5am


I would encourage any and all of you to pledge funds — not to the Cooper Union that moves towards tuition, but to the Cooper Union of the last hundred years. I'm a proud (and horrified) alum, and adjunct professor there now. Site is up at: <http://www.freecooperunion.com>
13. Caleb Crawford
12.19.11 / 2pm


Excellent piece. While not exhaustively researched into the abuses of the academic-industrial complex, it highlights the perverse activities at one institution. Particularly egregious is the plight of the adjunct scholar, without benefits, insurance or security. Schools in major cities such as New York can fill their teaching ranks sometimes to 90% with adjuncts. I don't really buy the argument that universities have to compete with the private sector, which is why it has to pay top dollar to its administrators. It seems to me that if adjuncts city-wide staged a general strike with specific demands and similar exposures of administrative malfeasance, it might improve our lot. 


As for tuition at Cooper, I understand the ideal which was shared and shed by CUNY a couple of decades ago. Still, at $5000/year, CUNY is next to free as institutions of higher learning go. The difficulty of getting into Cooper is an impediment to its serving those at most need. The entry requirements typically privilege those with sophisticated training generally available to the upper middle class and wealthy. While I agree with the slippery slope of the two classes of potential student (those who can pay and those who cannot), with the problems of executive compensation, and the general corporate form of contemporary eduction, I also have to question who Cooper is educating.
15. Arsalan Rafique
12.19.11 / 10pm


I happen to be in a dilemma that is brought upon us by the mere entrapment of the economic cultures pertaining educational “price” as, being a citizen of a war stricken country and an architecture graduate, I can just imagine (while being helplessly horrified) the consequences of putting a price up on any education that is “out of reach” for any body who might have the intellectual capacity to enter into the colleges of substantial educational breadth. I witness all doors closing that may have allowed us underprivileged natives of the world to reach out and learn (and maybe teach too) through an exchange of ideas, and yet some hope was kindled when one went through prospects like Cooper Union's. The air we breathe has conditions with it, nothing is without a price. It does not make a difference if the administration or the current economic falterings were to be blamed, because the facts are more gruesome than to be solved by pointing the issue out, and the reality is even more tragic. The uneven, unfair nature of the system that enables exchange(s) of knowledge is against those who do not have the pockets large enough to feed the family and to simultaneously pay for the education that is ideologically supposed to be independent of it. We do not care who is to blame, but we worry about the intellectual lag this world will eventually be left with it. 


Mr. Woods has been an invisible guiding light for us few here in Pakistan – as we can relate to his texts and his work, and in turn are inspired by it. He has also been, and I'm sad to say this, the carrier of this news that is a blow to all with hopes of becoming as great as he is.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.20.11 / 1am
	
	
	Arsalan Rafique: The things you say need to be said where many can hear, though I must protest you do me too much honor. I'm replying to you simply to say that you should not give up hope for yourself and others who share your dilemma, because I and many colleagues here consider it OUR dilemma. The struggle is far from over and I truly believe justice will prevail. Your support is crucial, and I am grateful for it.
	
	
	
	
	
		1. Sally Bowles
		12.24.11 / 6pm
		
		
		Lebb, 
		
		
		You're right in stating that if CU begins charging tuition it will become a third tier architecture school and join the ranks of Pratt, RISD, etc. The school's no-tuition policy is what made it special in the hearts of those gifted students that chose to come. If lucre plays into the picture, why wouldn't a gifted mind choose MIT, Cornell (or any big namy Ivy) over Cooper?
17. Sally Bowles
12.24.11 / 6pm


Lets just call a spade a spade here. Someone's gotta pay for the new academic building, and it aint going to be faculty and staff.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.25.11 / 12pm
	
	
	The traditional method is by a combination of borrowing, selling assets from the endowment, and raising donations from the wealthy, usually alumni. In schools that charge tuition, it might mean a small increase in tuition. As Cooper does not charge tuition, the prospect of beginning to charge it was brought up by those Members of the Board of Trustees who do not understand the mission of the school or want to change it.I and many others believe this is a bad idea. The overall budget shortfall must be made up by other means.
19. zale
1.4.12 / 3pm


I'm glad I got them on the phone to hear them say they weren't charging this year… right before they hung up on me, but stilllll 🙂
