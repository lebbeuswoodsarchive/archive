
---
title: FELLINI’S DREAMWORK
date: 2011-12-19 00:00:00 
tags: 
    - drawing
    - dreams
    - Federico_Fellini
    - psychoanalysis
---

# FELLINI'S DREAMWORK


[Sigmund Freud](http://www.google.com/search?client=safari&rls=en&q=sigmund+freud&ie=UTF-8&oe=UTF-8) considered dreams “the royal road to the unconscious,” a glimpse into a world of mind we inhabit without knowing it, except when we dream. Even then, we do not know it as we do the world we are conscious of, because this world—the unconscious—is revealed to our conscious minds only in fragments that often seem irrational and incoherent. Freud, however, believed they made a great deal of sense, if only they could be ‘translated' in terms of our conscious experiences. Not only that, but understanding the sense they made was vital to our mental health.


This is hardly the place to explain Freud's theories or argue for or against their validity. A great many books doing just that have been written in the hundred or so years since the publication of *The Interpretation of Dreams,* a founding text for the field of *psychoanalysis.* It must do for now to know that over this past century Freud's theories have been applied with success to aid in the treatment of various forms of mental illness—including everyday stress, anxiety, and depression brought on by the demands of modern living. Of equal importance, perhaps, is that these theories have entered our culture in ways that enlighten our understanding of what it means to be human, influencing both our critical and creative capacities in art and science.


I feel fairly certain that Federico Fellini, one of greatest film-makers in the history of cinema, which just happens to span the same century as psychoanalysis, knew all about its theories and even spent time on the psychoanalytic couch. Regardless of whether he did or not, he did spend a great amount of his time recording his dreams, in exactly the way Freud would have appreciated: noting every detail he could remember in words and drawings. The good Doctor would have had a field day with them!


If you can read Italian, there are Fellini's notes in the samples below from a recent publication entitled *[Federico Fellini: The Book of Dreams.](http://www.amazon.com/Federico-Fellini-Book-Dreams/dp/0847831353/ref=sr_1_1?ie=UTF8&qid=1324324603&sr=8-1)* If, like me, you cannot, you can refer to English translations in the book's appendix, or, have your own field day with appreciating the drawings, both for their startling originality and freedom of technique, and as points of departure for making your own psycho-analysis of Fellini and his films. For example: what part of the human anatomy does he seem most obsessed with? And why? That, as they say, is only the beginning.


LW


[![](media/ff-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-1.jpg)


.


[![](media/ff-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-5.jpg)


.


[![](media/ff-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-21.jpg)


.


[![](media/ff-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-4.jpg)


.


[![](media/ff-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-3.jpg)


.


[![](media/ff-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-6.jpg)


.


[![](media/ff-71.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-71.jpg)


.


[![](media/ff-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-9a.jpg)


.


[![](media/ff-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-10.jpg)


.


[![](media/ff-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/ff-11.jpg)


.


#drawing #dreams #Federico_Fellini #psychoanalysis
 ## Comments 
1. [rafaeloshouldbe](http://gravatar.com/rafaeloshouldbe)
12.19.11 / 10pm


thank you so much, Fellini is one of those path I have to go away



corre all'indietro (running back)


Fellini(and others Italians -registi-) for me is like La Jetée for you


Have you seen this?  

<http://en.wikipedia.org/wiki/Satyricon_(film)>
3. [metamechanics](http://metamechanics.wordpress.com)
12.21.11 / 4am


interesting stuff and in time with many of my recent philosophical interests (i encompass psychology within philosophy, psychology is merely a small sub-category to existence)…take a pill or read a book or find a religion, etc…unfortunately most the world blames something else for their lack of control of existence and character….all together another topic.


I belive M.C. Escher's ideas were results of waking from dreams and pursuing them?


The tricky thing I find is jotting down the dream in time before it is only remembered emotionally, and fading quickly. Sometimes reading and nodding off the dream logic solves problems unbelievably out of the box, fast, and overwhelmingly correct, but fully grasping it in time conscioussly is tough. I's like a professor giving a badass lecture (Manual Delanda for example), you have an epiphany, you forget the cause of it and you want him to repeat what he just said for the last hour, but he can't, he's lecturing to everyone and re-living that hour doesn't guarentee same epiphany…many things inform that epiphany – the notes on your paper, your location in the auditorium, the person sitting in front of you, etc…


my formal architectural design education has essentially guided me to avoid the stuff i come up with in dreams – good luck justify that shit in a jury..”so i was having this dream and here's the floor plan”….i've finally decided to pursue the recreation via my 3d modeling skills, may take decades but it's worth i think, not for psycholocal reasons, just to understand how much we already know. i reference the movie “Limitless”, i'd argue based on the Red Bull overdoes phenomeone. by the way the vitamins in Red Bull (one of the B's) enhances the dream state, occasionally i drop some vitamin B before going to sleep (FDA approved, ha).


thanks for the inspiring post lebbeus. and regarding your previous post – I agree… that pamphlet and radical reconstruction are another great source for thinking, like mold breaking.
5. Davis
12.24.11 / 3am


For better remembering: when you wake, be relaxed and keep your eyes closed. You are not trying remember. You are trying to keep a fluid mind as the mind is in dream. Follow out odd wonderings in your head as they may be leading you to memory. Sometimes I lie in bed for five minutes before something comes back to me from my dreams. Patience is key, and also developing a fluid mind that carries into waking: that you cannot grasp the dream is essential. They say (and my experience supports) that the more you record your dreams the better you become at remembering.





	1. [metamechanics](http://metamechanics.wordpress.com)
	12.27.11 / 1am
	
	
	hey just read this. it sounds like what you may be suggesting is what i'd call meditating, relaxing enough to enter dream state but still be awake? 
	
	
	“that you cannot grasp the dream is essential.” did you mean “can grasp”? otherwise need more clarifcation
	
	
	not making this up to fit the conversation, but last night i did try a form of “patience” in my dreams. typically when i become aware of the dream surroundings I analyze the space as any human would rationally and concentrate on some specific aspect, like a zoom lens, and usually what happens is a quick story change or it falls apart and i awake, etc…last night I relaxe a bit and watched it like a movie, the architecture and environments were as perfect in detail as reality, people were a little fuzzy sometimes due to probably trying to recognize foreign faces, etc…the environments were fantastic by all means but fluidly ran into each other, short version here:
	
	
	leaving a stadium and getting on a one way train to NYC. in the dream I knew the front of the train would be less populated so I chose to go towards the front, but not intentionally plan the end, hence the end appeared eventually. at the stop we all got out and climbed treated wood stairs like on a deck up to an overpass over water just like the meadowlands (NJ). when i started thinking about the strength of the 60 feet high one run 4′ wide stairs, the stairs lost the railing, treads fell off and I began to fall. in the distance was an abandoned building and instead of worrying about falling I just watched as the dream took me to the building….and it goes on…when I thought about emergency egress of the stairs the dream did the weird storyline skip, etc…so I relaxed.
	
	
	davis thanks for the response.
	
	
	
	
	
		1. Davis
		12.27.11 / 5am
		
		
		Yes; I think my in-run (pre-flight checks and all) to a nap or sleep is like meditating. (I like to think “surrender” and imagine I do so in fluid)  
		
		“That you cannot grasp the dream is essential” was in effort to guide you, and by your response it seems you practice something of how I meant. I was trying to speak to the situation in which you cant remember much, but a sliver. To clutch that sliver demanding more and upsetting yourself in not knowing would be how the dream memory cannot be grasped. Though that is for me. So far.  
		
		I am interested in your quick dream work–incorporating relaxation. My dreams seem devoid of questioning, and so tension, and thus relaxation too. Perhaps that is the nature of my dream, and it is not till I surface that I find tension.  
		
		Good musing with you
	3. [metamechanics](http://metamechanics.wordpress.com)
	1.14.12 / 4pm
	
	
	Davis, just so you know…had a vivid architectural dream this morning. did your recommended wake state re-cap, currently sketching the floor plans and timeline, jotting down the names, and given my ability to do 3D computer renderings, over the next few months will render a few of the photographic memories. also managed to reference real world scenarios from which most likely the scenes evolved…will blog it once i get some time, in the waking state calling this building/infrastructure – “the motley hamilton estate”….
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		1.15.12 / 7pm
		
		
		metamechanics: Does “will blog it” mean that you have a blog? Or, will you submit it to the LW blog? Given your consistently intense engagement with this blog, I would be more than glad to see it here.
		3. [metamechanics](http://metamechanics.wordpress.com)
		1.16.12 / 3am
		
		
		I have a blog but you could post, its going to take a few months though I think (work, kids, etc..). CAD'ing the plans won't take and story line is quick, but want to match the photos 100%. not sure releasing the dream prior to the images makes sense. i'm pretty sure all dream memories are pseudo-memories of reality and hence i've managed to nearly link all dream photo memories back to real locations which further helps me locate and scale “the motley hamilton estate”. it's mid-town, but Soho buildings around 1850. i'll keep you posted on this page. thanks.
		5. Pedro Esteban
		1.16.12 / 3am
		
		
		Just a suggestion.  
		
		Please can you publish the sketches? Is not more easy just that way? I prefer to see the sketches indeed.  
		
		The conversation between you have been quite interesting, thank you.
		7. [lebbeuswoods](http://www.lebbeuswoods.net)
		1.16.12 / 2pm
		
		
		metamechanics: Check out the architectural dream of Raimund Abraham described by him on this blog: <https://lebbeuswoods.wordpress.com/2010/02/14/raimund-abrahams-dream/>
		9. [metamechanics](http://gravatar.com/metamechanics)
		1.18.12 / 3am
		
		
		lebbeus i forgot my manners, i'd be absolutely honored. i vaguely remember reading something about Raimund and dreams, but I might have that confused with M.C. Escher, so def. going to read it, thanks for link.
		
		
		my sketches suck, they really do, i don't have patience to make the hand do what the mind sees.
		11. [lebbeuswoods](http://www.lebbeuswoods.net)
		1.18.12 / 10pm
		
		
		metamechanics: I'd like to see the sketches anyway. I (and, I suspect, Pedro Esteban also) don't altogether accept your self-criticism involving patience. Go to lebbeuswoods.net, enter Contact, and we'll connect via email. If you'd like to,,,
		13. [metamechanics](http://gravatar.com/metamechanics)
		2.6.12 / 4am
		
		
		hey man replied via your website. and no my patience doesn't exist for the pencil….although once after my wash U architecuhtre camp experience i produced a drawing of a Fayette, Mo building for some school calender that was sketched on site interpreting brick mortar and grout correctly in pencil an paper…random side note, reminded me of the time i totally botche a scoring oppurtunity for a bro in high school, i told the girl he was trying to get into her pants, i assumed she just understood…either way my buddy knocked me in the head a few times down the street from same sketched house…
		15. Davis
		9.3.12 / 6am
		
		
		MetaMech,  
		
		Just coming back to this thread, months along, and pleased to see how it grew. Usually there is architecture in my dreams, and often I can identify the architecture as based on an architecture I know in waking life. the rooms are not always the same, though the shapes are. pretty fun. i record my dreams in illustration, and have some fun trying to draw architecture–described in one image–known in multiple perspectives as the dreamer often knows more than one view, and/or there is a developing perspective as the dream is narrative.
		
		
		and good to see your dream activity so vibrant around the time of our chat.  
		
		salut,  
		
		D
7. Sally Bowles
12.24.11 / 7pm


I never understood why academians continue to cream themselves over Fellini's voyage into dreams thru his films. Its like anything Italian is automatically considered haute-couture. Luis Bunuel did a better job of this in his films and work.
9. Mark Bates
12.28.11 / 6am


Lina Wertmuller's Seven Beauties, is a contender.
11. [Tragic Lost » LW on Fellini's Dreamwork](http://tragiclost.org/wordpress/?p=128)
1.2.12 / 5pm


[…] Read here […]
13. zale
1.4.12 / 3pm


There is this book I found on the Barnes & Noble shelves…


It's a book that has pictures of architect libraries. I can remember an instructor telling us, “Got this for five cents, keep your books with you”… A bookstore somewhere in New York sells cheap. Not in Barnes & Noble… this book tells us something about how architects don't read. Which of course, is ironic (esp in a book about the libraries of architects)


Anyway. I get it. And these images strike me. LW, I'm sure you own at least one moleskin. Moleskin itself put out a book called “the hand of the architect” sketches of contemporary architects. I met this boy at a bar for the first time a few months back, I told him about the book. He, who just had graduated from some L.A. Architecture school immediately purchased the book from amazon, at say, 1am. And I smiled.


This here reminds me not of italian artists, but of german artists. So be it. It's me. The color work especially. Or should I say the pressure. The contrast of a dark idea(?) with light workings feels timid. I can see more in the fire. But, perhaps the exposure of dreams is hard enough in itself. ❤
