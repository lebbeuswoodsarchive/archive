
---
title: WAR AND ARCHITECTURE  The Sarajevo window
date: 2011-12-02 00:00:00 
tags: 
    - damaged_buildings
    - reconstruction
    - Sarajevo
---

# WAR AND ARCHITECTURE: The Sarajevo window


[![](media/window-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-4.jpg)


*(above) Prototypical wall and window repair for Sarajevo, Bosnia, c. 1994, view from inside. Concept by LW, design and construction by architect Paul Anvar.*


.


Some twenty years go, I wrote:


 *Architecture and war are not incompatible. Architecture is war. War is architecture.**I am at war with my time, with history, with all authority that resides in fixed and frightened forms. I am one of millions who do not fit in, who have no home, no family, no doctrine, no firm place to call my own, no known beginning or end, no “sacred and primordial site.” I declare war on all icons and finalities, on all histories that would chain me with my own falseness, my own pitiful fears. I know only moments, and lifetimes that are as moments, and forms that appear with infinite strength, then “melt into air.” I am an architect, a constructor of worlds, a sensualist who worships the flesh, the melody, a silhouette against the darkening sky. I cannot know your name. Nor you can know mine. Tomorrow, we begin together the construction of a city.*


This manifesto was read aloud on the steps of the burned-out Olympic Museumin Sarajevo on November 26, 1993, in full view of Serbian snipers and artillery gunners. Happily, no fire rained down on the assembled audience, of which I among many others was included. Coming to the last line, one of the two gifted actor-readers objected “Why wait until tomorrow?” Typical Sarajevan humor, candor, and bravado in the face of overwhelming odds.


Over the two decades since this manifesto was written, I have had much time to consider the words I wrote and what I meant by them.


At that time, I was responding to an urgent situation in Sarajevo, Bosnia—a city under a sustained terrorist attack that, in the West, was considered a siege, as though it were part of a normal war, which it was not. Snipers had turned streets into lethal shooting galleries and artillery gunners had turned ordinary buildings where people worked and lived into incendiary death traps. It was clear that architecture was part of the problem—the killing of thousands of innocent men, woman, and children—and I felt strongly that as long as the attacks continued (it turned out to be for more than three years) architecture also had to be part of the solution.


Without the help of architects, people had built temporary walls as shields against snipers and thrown up all sorts of improvised repairs to their homes and workplaces. I reasoned that these makeshift structures, though more or less effective for their purposes, created a degraded environment, which was exactly the goal of the terrorists. To survive, and to frustrate the enemies of their refined culture, people need a sense of order in their world, one that is consciously created, or designed. Sarajevans nobly showed this need by the way they dressed, in spite of the lack of water, heat, or lighting, somehow always in clean, pressed clothing, the women elegantly coifed and made up, incongruously strolling in the parts of the city center that were screened from snipers if not from mortars and cannons in the hills above, like players from an Alain Resnais film. Inspired by this and a dash of Michelangelo's designs for the fortifications of Florence, I set out to consider how to repair damaged houses and offices in ways that embodied the *elan* of their inhabitants, as well as kept out the rain, snow, and cold. These were extremely modest designs, made from scavenged metal, wood, and even cardboard.


One principle I adopted in the beginning was that such found materials would be reshaped, piece by piece. More than anything, I wanted this small-scale architecture to avoid becoming ‘junk sculpture,' or a collage of detritus. Intention is important, even at the smallest scale, and the intention in Sarajevo was to consciously reshape its world, turning ruins and battered remnants into a new kind of architecture, a uniquely *Sarajevan* architecture, and something of which the city's people could be proud. The goal was also to establish some basic rules of reconstruction, keeping in mind the enormous task of rebuilding the damaged city that would begin when the terrorists were defeated and people could turn their energy to building the new city I had forecast in my manifesto, and a new way of civic life.


*To be continued.*


.


[![](media/window-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-21.jpg)


.


.


[![](media/window-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-6.jpg)


.


[![](media/window-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-7.jpg)


.


[![](media/window-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-9.jpg)


.


[![](media/window-22.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-22.jpg)


.


[![](media/window-24.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/window-24.jpg)


LW


#damaged_buildings #reconstruction #Sarajevo
 ## Comments 
1. [emmanuele](http://www.piliaemmanuele.wordpress.com)
12.2.11 / 1pm


Perhaps I am simple, but “war and architecture” has always moved me.
3. [Gio](http://gravatar.com/gb427)
12.6.11 / 4am


Mr. Woods are those concepts all window treatments viewed from the inside?


It does feel like a fortress as if the one living inside these spaces where in constant fear of looking to the outside  

(which it makes sense since they are in Sarajevo). But there is also another more interesting underlying thought that it is truly shaped and represented in the architecture, the relationship of war and architecture. I have never been in a war so I can only relate to that through movies and books and in all of those examples , including your own, the reference to the horizon line becomes blurry in the chaos of the scenes.  

Planes and lines become broken and twisted having no recognizable form or beginning and the light reveals these edges with no earthly reference to compare it. 


These volumes and spaces make total sense for these conditions. There is a need for their existence.


Gio





	1. Paul Anvar
	12.7.11 / 7am
	
	
	Gio  
	
	Thank you for your comments. They helped to recall my memories of a time when even a young student and his professor could have a positive impact on a terrible time in a beautiful place. 
	
	
	 I would say yes…..first and foremost there was fear. As Lebbeus writes, fear to walk in the light of day as well as fear to see what the light would reveal in the streets the next morning.   
	
	However, the geometry of the window prototype in our minds was a result of trying to find a way to solve the problem of keeping people from freezing in the winter. At the time Sarajevo was under constant shelling which left many buildings without glass and thus heat. However, through Lebbeus's contacts we were informed that U.N relief provisions were being dropped that were contained in cardboard boxes. Given that cardboard was the only construction material to provide protection from the cold and or snipers. We built accordingly.  
	
	 For me the beauty in the geometry  of these cardboard forms is much like one would see when looking upon an abandoned steel mill. Perhaps form does follow function after all.  
	
	Cheers
5. [» War and Architecture: The Sarajevo window [Woods & Anvar] cane brandy](http://www.canebrandy.com/15/2011/12/war-and-architecture-the-sarajevo-window-woods-anvar/)
12.11.11 / 11am


[…] to a war-torn Sarajevo using spare UN cardboard by Paul Anvar and Lebbeus Woods. Lebbeus' article here is worth a […]
7. [bosevan](http://gravatar.com/boseva)
12.11.11 / 11am


Yes, I have the feeling that every building project need also destruction project (un,fortunately I have been trained also how to destroy buildings)  

If these projects are only Camouflage, they have future temporal life, if not, they need more clear explanation for existence
9. [WAR AND ARCHITECTURE: Three Principles « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/12/15/war-and-architecture-three-principles/)
12.15.11 / 12am


[…] This post is a continuation.  […]
11. [BEYOND MEMORY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/03/22/beyond-memory/)
3.22.12 / 12pm


[…] is what I had in mind in the War and Architecture projects of the 1990s, which addressed war-damaged buildings and their reconstruction. The SCAR […]
13. [FCJMesh-003 : On Networked Utopias and Speculative Futures 3/3 | The Fibreculture Journal: Mesh](http://mesh.fibreculturejournal.org/fcjmesh-003-on-networked-utopias-and-speculative-futures-33/)
9.20.12 / 6am


[…] worlds in which social, political and personal transformation might be achieved” reminded me of Lebbeus Woods' exploration of some of these themes in his proposal for a kind of ‘liminal' architecture […]
