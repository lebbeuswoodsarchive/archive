
---
title: MICHAEL SORKIN  Sidewalks of New York
date: 2011-12-25 00:00:00 
tags: 
    - New_York
    - sidewalks
    - urban_theory
    - urbanism
---

# MICHAEL SORKIN: Sidewalks of New York


[![](media/sork-side-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/sork-side-1.jpg)


For more than thirty years, [Michael Sorkin](http://en.wikipedia.org/wiki/Michael_Sorkin) has been a strong, constructively critical voice in the field of architecture. His many books and articles have opened our eyes and minds to the social and political issues lurking behind the often too-fancy facades designed by the famous for the rich and powerful. He blithely refuses to be seduced. At the same time, he has been the stalwart champion of a more egalitarian urbanism, in his writings, his urban planning practice, and his teaching. His book *[Local Code….](http://www.amazon.com/Local-Code-Constitution-City-Latitude/dp/1878271792/ref=sr_1_1?ie=UTF8&qid=1324855901&sr=8-1)* is a landmark in all these venues.


The following piece, which he has generously given for publication on this blog, challenges us both in its content and its form. The concepts require us to throw off our complacency and seriously consider what we mean by the term “public space.” And, rather than entertain us with an impassioned polemic, he presents these concepts as a document ready to be written into law. Just think how much time he is saving all those wrangling politicians on the City Council! But it takes a bit of chewing to digest.


LW


***The Sidewalks of New York***


1.     The Streets belong to the people!


2.     So do the Sidewalks.


3.     A minimum of 50% of the Street space of New York City shall be taken out of the realm of high-speed and mechanical locomotion and assigned the status of Sidewalk.


4.     This minimum shall apply on a Block by Block basis.


5.     The entirety of a given Street may be transferred to the status of Sidewalk with the consent of 75% of the membership of the Block Committee.


6.     A Block Committee shall be comprised of all of those of voting age whose primary work-place or residence is accessed from a given Block.


7.     All New York City Sidewalks, including these additions, shall revert to ownership by the City of New York, which shall assume primary responsibility for their maintenance.  Notwithstanding this obligation, the right to control the disposition of uses on each Block shall be shared by the Block Committee and the City of New York, subject to the over-riding general rights of Passage and Assembly.


8.     A Block shall be understood to be the space from corner to corner defined by a single Street, not a square block, and shall encompass the Sidewalks on both sides of the Street.  Each square block shall be understood as including portions of four different Blocks.


9.     Block Corners, the junctions of Blocks, shall be assigned to one of the impinging Blocks such that each Block shall control two out of the four Corners it engages.


10.  Such assignment shall be random.


11.  The consolidation of Blocks for purposes of the administration by the Block Committees of elements of the blocks that exceed that space of a single Block shall be permitted as long as the consolidation is of Blocks that are contiguous.


12.  In no case may this consolidation be permitted to exceed four contiguous Blocks.


13.  All uses on the Sidewalk shall be public or accessible to the public.


14.  Neither the Right of Passage along the Block nor the Right of Assembly within the Block shall be fundamentally infringed or impaired.


15.  No Assigned Public Use (APU) shall impede walking or standing rest within the area of the designated minimum Territory Of Passage (TOP).


16.  The use of Sidewalks, other than for Passage or Assembly, including loitering and standing rest, shall be determined by Block Committees which may assign rights to their use other than for Public Passage or Assembly.  Such subsidiary public rights shall be assigned on a rotating basis.


17.  In no case may more than 5% of the area of any Block be occupied by a use that requires direct payment by the public to access its benefit.


18.  Fees from the assignment of public rights shall profit the Block from which they are derived except in the case of High-Income Blocks.


19.  A High-Income Block shall be understood to be a Block on which revenue from fees shall exceed by more than 50% the median fee collected from all Blocks, city-wide.


20.  25% of the revenues from High-Income Blocks shall be tithed to the Block Bank.


21.  The Block Bank, the directors of which shall be composed of representatives from the Block Committees, shall make Block Grants for improvements to Blocks that do not qualify as High-Income Blocks.


22.  Permitted uses shall include sitting, the playing of games and miscellaneous other recreational activities, gardening and agricultural activities, the storage of bicycles, the capture of rainwater, the care of children, the management of waste, the planting of trees, public toilets, and the sale of books, journals, newspapers, and snacks.


23.  The area of any Block necessary for access to the New York City Transit system, including both street-level and underground operations, shall be designated a *corpus seperatum* and its maintanence shall be the responsibility of the Transit Authority.


24.  Uses of sidewalks shall be classified as either Grandfather or Sunset uses.


25.  Grandfather uses are to be permanent.  Sunset uses are subject to annual review by Block Committees.


26.  Grandfather uses shall include Minimum Passage and Street Trees.


27.  Minimum Passage shall be a lateral dimension between ten feet and half the width of the expanded Sidewalk, whichever is greater, and shall be harmonized with the dimensions of contiguous Sidewalks.  These dimensions shall be established by Department of City Planning with the advice and consent of the Block Committees.


28.  Street Trees shall be planted such that they shall, within five years of their planting, provide adequate shade over the full area of the Block during the months of summer.


29.  The location and species of these trees shall be established by the Department of City Planning with the advise and consent of the Block Committees.


30.  Sleeping on sidewalks shall only be permitted by permission of the Block Committees on application no less than one day in advance of bedtime.


.


**Michael Sorkin**


#New_York #sidewalks #urban_theory #urbanism
 ## Comments 
1. [MICHAEL SORKIN: Sidewalks of New York « LEBBEUS WOODS « shareitorstopit](http://shareitorstopit.wordpress.com/2011/12/26/michael-sorkin-sidewalks-of-new-york-lebbeus-woods/)
12.26.11 / 11am


[…] MICHAEL SORKIN: Sidewalks of New York « LEBBEUS WOODS. Share this:TwitterFacebookLike this:LikeBe the first to like this post. ← Previous post […]
3. [cem özgüner - istanbul](http://abakus2000.blogspot)
12.26.11 / 9pm


Hi , Michael ,  

Great to hear from you , as usual supporting the peoples' rights on that issue…  

Cem-İstanbul  

PS:THree years past your last visit to İstanbul-Beykoz-Riva…(A parcel by the river :))still there ; naked :))
5. Patrick
12.27.11 / 12am


“30. Sleeping on sidewalks shall only be permitted by permission of the Block Committees on application no less than one day in advance of bedtime.”


Should be:


“30. Sleeping on sidewalks is permitted.”
7. [Evan Bray](http://savemoheganlake.com)
12.30.11 / 5pm


The right of assembly is already defined by the rules of the city of new york, right? Street trees are governed by Parks Dept., not City Planning. City Planning, through zoning, would have a say in permitted uses, but that seems to be opening a can of worms. The last thing we need is government defining permissible uses in the public sphere. So long as they don't run up against any municipal laws (e.g. no camping), it is understood to be permitted. Very interesting manifesto; I think it could be honed to be a more practical proposal, but conceptually is very strong.
9. [Miroirs de la ville #3 Psychogéographie ! Poétique de l'exploration urbaine « Urbain, trop urbain](http://www.urbain-trop-urbain.fr/miroirs-de-la-ville-3-psychogeographie-poetique-de-l%e2%80%99exploration-urbaine/)
1.24.12 / 7am


[…] contredit l'autorité. Sur cet énoncé simple, la rue devient l'objet des mouvements pour les libertés civiques. Tandis qu'en Europe — s'il fallait entretenir une distinction coûte que coûte —, la […]
11. [MICHAEL SORKIN: Sidewalks of New York | concerturbain](http://concerturbain.wordpress.com/2012/02/09/michael-sorkin-sidewalks-of-new-york/)
2.9.12 / 9am


[…] planning practice, and his teaching. His book Local Code…. is a landmark in all these venues.Via lebbeuswoods.wordpress.com Évaluez ceci : Share […]
13. mel
5.29.12 / 3am


hi there, for all those knowledgeable peeps out there,  

just a couple of queries as follow when i have read his book on local code.


1)Why does Sorkin create new terms for elements of the city? Why does he set out this set of regulations?  

2)How would a building code like that outlined by Sorkin be established in real life? How equitable would it be?
