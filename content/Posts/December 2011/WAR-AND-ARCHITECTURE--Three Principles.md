
---
title: WAR AND ARCHITECTURE  Three Principles
date: 2011-12-15 00:00:00
---

# WAR AND ARCHITECTURE: Three Principles


***Note to the readers:** I wish to apologize for what must seem a blatant self-promotion in this post, but it is not possible to separate the personal from the conceptual, because the two stories are here so fully intertwined. As I said in an earlier post, the ideas developed in this work have such currency in the present that, I believe, it is a necessary risk to take. I can only ask for the readers' generous forbearance.*


*LW*


[![](media/lwblog-wa-cover.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-cover.jpg)


*(above) The front cover of* War and Architecture*, an issue in the Pamphlet Architecture series that I took with me to Sarajevo in late November, 1993, when the city was under attack. I must thank Clare Jacobson, its editor, and Kevin Lippert, its publisher, who worked hard to ensure that I would have it on the date of my departure for Sarajevo. The text is in English and Croatian, thanks to Aleksandra Wagner, who made the translation of my text in English to what was then still called Serbo-Croatian.*


*This post is [a continuation](https://lebbeuswoods.wordpress.com/2011/12/02/war-and-architecture-the-sarajevo-window/).*


.


I am revisiting the work I did some fifteen years ago for an unhappy reason. Originally intended to address the destruction of buildings in Sarajevo, Bosnia—which I and many others hoped would prove to be an isolated catastrophe—it has instead turned out to be only the beginning of a new trend resulting from globalization, a proliferation of  regional, often insurgent-driven wars that have resulted in the piece-by-piece destruction of cities and the killing of their inhabitants that characterized [the torturous three-year attack on Sarajevo](https://lebbeuswoods.wordpress.com/2011/09/08/september-11-2001-2011/).


In going over what I wrote about this work at the time—in 1993—I find it inadequate in its explanation of what inspired the designs, drawings, and models and what I hoped to achieve by making them. No wonder, I say in hindsight, that they were accused of “aestheticizing violence,” and merely being exploitative of a tragic human condition. I failed to put the work in the broader human context that it needed to be understood as proposals for architecture serving rational and needed purposes. I hope to correct—to the extent I can here—this failure.


Because of my work concerned with the Sarajevo crisis long ago, people have often asked what I was working on for Baghdad, or Kabul, or Tripoli, or a growing list of cities that have shared its fate. My answer is always the same: *nothing.* While each is different, the destruction they have suffered is so similar to that suffered by Sarajevo that the principles I established there apply as well to the more recent catastrophes. This is a crucial point. My “war and architecture” work was not aimed at proposing the reconstruction of particular buildings—that should be the work of local architects—but at deriving guiding principles. The specific buildings I addressed with my designs were meant more as demonstrations of how these principles might work in particular cases, rather than as actual building proposals. Again, I strongly believe that reconstructions should be designed by local architects, who understand the local conditions far, far better than I ever could. I did and still do feel, equally strongly, that I and other ‘conceptualists' can make a contribution to reconstruction on the level of principle, because we can more readily have a broader view, not having directly suffered the trauma of our city's destruction and its lingering emotional and intellectual effects.


So, to the principles.


Before attempting to address the reconstruction prospects forced upon us by the destruction of Sarajevo, I studied the history of modern cities attacked in the Second World War. There is a massive literature on this heart-wrenching but crucial moment in human history. However, there is a small literature on the rebuilding of the damaged cities—many of which were severely damaged—and even less about the actual concepts that guided their reconstruction. From my studies, I can see only two guiding principles shared by the majority of post-war reconstruction projects.


The **First Principle: *Restore what has been lost to its pre-war condition.***The idea is to restore ‘normalcy,' where the normal is the way of living lost as a result of the war. The idea considers the war as only an interruption of an ongoing flow of the normal.


The **Second Principle: *Demolish the damaged and destroyed buildings and build something entirely new****.* This ‘new' could be something radically different from what existed before, or only an updated version of the lost pre-war normal. Its application is very expensive financially, at the least.


Both of these concepts reflect the desires of most city inhabitants to ‘get back to normal,' and forget the trauma they suffered as a result of the violence and destruction. Yet, both concepts ignore the effects of the war and destruction on the people who suffered through them, not only the personal psychological effects, but also those forcing changes to people's social, political, and economic relationships. Before the war, Sarajevo was the capital of Bosnia and Herzegovina, one of the states in the Socialist Federal Republic of Yugoslavia. After the war, it was the capital of an independent country and no longer Socialist. The impact of this change alone on people's everyday lives has been enormous, and particularly so in the ways they perceive each other and themselves. In this sense, it is not possible to get back to normal. The pre-war normal no longer exists, having been irrevocably destroyed, Still, this does not mean that many—even most—people will not desire to do so. In such a society, wise leaders are needed to persuade people that something new must be created—a new normal that modifies or in some ways replaces the lost one, and further, that it can only be created with their consent and creative participation. In effect, a new principle of reconstruction needs to be established.


We'll call it the **Third Principle: *The post-war city must create the new from the damaged old.*** Many of the buildings in the war-damaged city are relatively salvageable, and because the finances of individuals and remaining institutions have been depleted by war and its privations, that salvageable building stock must be used to build the ‘new' city. And because the new ways of living will not be the same as the old, the reconstruction of old buildings must enable new ways and ideas of living. The familiar old must be transformed, by conscious intention and design, into the unfamiliar new.


It is worth mentioning that the most needed buildings are the so-called ordinary ones—apartments and office buildings, primarily. Symbolic structures, such as churches, synagogues, mosques and those buildings of historical significance that are key to the cultural memory of the city and its people, must also be salvaged and repaired. With these latter buildings, the First Principle—restoration to the pre-war state—is almost always justified, whatever the cost, which is always high. However, the application of this principle to ordinary buildings makes no sense, because there is nothing especially memorable to restore. To the contrary, the apartment and office blocks that survive destruction must provide the day-to-day spaces for the new ways of living to be enabled by their ‘radical reconstruction.'


The projects for Sarajevo that demonstrate exactly what is meant by this term, accompanied by extended captions, are presented below. I think it is possible, and just, to project the Third Principle into the reconstruction of today's war-damaged cities.


[![](media/lwblog-wa-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-1.jpg)


(above) A typical residential block, badly damaged in places, reconstructed with new types of spaces for residents' use. The principle here is that reconstruction integrates people's experiences of the destruction into needed social changes, as well as architectural ones.


[![](media/lwblog-wa-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-2.jpg)


*(above and below) Typical residential blocks, damaged and reconstructed as described above. It is important to remember that most of such ordinary buildings are damaged only in part and can be salvaged by reconstruction for the post-war city and its new ways of living.*


[![](media/lwblog-wa-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-4.jpg)


*(below) The UNIS twin office towers, attacked in 1992, and burned. The buildings' structural and floor systems survived and were suitable for ‘radical reconstruction.' The new types of office space will be used in ways that will be unique to the post war conditions.*


[![](media/lwblog-wa-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-10.jpg)


*(below left) The burning Electrical Management Building, and (right) the badly damaged, but salvageable Parliament of Bosnia and Herzegovina:*


[![](media/lwblog-wa-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-8.jpg)


*(below) The purpose of the New Parliament is not simply to replace the old, Socialist parliament, but—in the first place—to study and debate what a post-war Bosnian parliament should be and do. New types of spaces woven into the surviving Cartesian structural frame, create a dialectic between timeless and timebound, a network of the unknown that inspires both dialogue and innovation:*


[![](media/lwblog-wa-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-7.jpg)


[![](media/lwblog-wa-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-12.jpg)


*(below) Like the “[Sarajevo window](https://lebbeuswoods.wordpress.com/2011/12/02/war-and-architecture-the-sarajevo-window/),” the scavenged construction materials are carefully reshaped and reconfigured, then fitted together with a high level of craft—a technique appropriate to the New Parliament's methods and goals.*


[![](media/lwblog-wa-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-6.jpg)


.


[![](media/lwblog-wa-cov-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/12/lwblog-wa-cov-2.jpg)


*The book* Radical Reconstruction, *for all its textual deficiencies, does present demonstrations of the Third Principle, stated above, in Sarajevo; Havana, Cuba; and San Francisco, USA.*


.


**LW**



 ## Comments 
1. [Alex Bowles](http://www.alexbowles.com)
12.15.11 / 8pm


I've printed this post and inserted it in my (cherished) copy of ‘Radical Reconstruction'. It really is the perfect addendum. Thank you.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.16.11 / 12am
	
	
	Alex Bowles: I'm very glad the post accomplished its purpose!
3. AAscapes
12.17.11 / 4am


Hello Professor Woods,


I really appreciate this post! I own a copy of the Pamphlet Architecture 15 “war and architecture” and many others in the Pamphlet Architecture series as well as your “Radical Reconstruction”. I used both of these as my precedents in my architecture thesis (December 2010) including in my final presentation—I read the following statements:


“Architecture must learn to transform the violence, even as violence knows how to transform the architecture”  

and  

“The scare is a mark of pride, and of honor, both for what has been lost and what has been gained.


My thesis was about war/death (body becomes layer of topography) and second part was about border issue.


I have attempted to contact you in the past. I would love to send you a copy of my thesis monograph (160 pages) if you accept that.


Thank you again for this post!


Alan  

<http://www.aascapes.com>
5. Sally Bowles
12.24.11 / 6pm


Lebb,


Just what is the purpose of decorating damaged building structures with scrap airplane parts? Is it to make a sculptural statement about war? 


As a professional and practicing architect that creates real buildings and deals with real conditions (sans the comfy cushion of academia), it amazes at the level of naiveity that gets peddled in architecture schools for such high professorship salaries. Is it any wonder that architects don't get any respect relative to their professional peers? Do me a favor, just call what you peddle art, and call it a day. 


Labels are necessary to distinguish the quixotic from the pragmatic.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.25.11 / 12pm
	
	
	If you ONLY had “airplane parts,” scrap wood, metal and plastic, even cardboard, could you make architecture?
	
	
	Also, I'm really interested in what you—“a professional and practicing architect”—consider to be “pragmatic” architecture. Examples?
7. [Michael Phillip Pearce](http://www.facebook.com/pearce.mp)
12.30.11 / 10pm


Apologize” & “failure”? This blog and your [LW] large body of work would not have influenced so many if that were such the case. I, like many, are grateful for this information, this exploration into the obvious solution for design build ops that architects and developers are so afraid of. Some of the best spaces are those utilizing the 3rd principle of merging the present with the past, so that they may co-exist together making rich layers of information–wisdom. 


But, more importantly it reduces the vast overhead of the cost for the “new”, and most of the new really isn't so new anyway. Radical Reconstruction is radical recycling, reclamation, and just plain responsible. That is a lot of R's.


Sally what you say is very disrespectful, “Do me a favor, just call what you peddle art, and call it a day”. Radical Reconstruction is a serious way to build, rebuild and create a new way for communities to rapidly repair and heal. Granted LW makes it, art as do all the successful architects and designers, but it isn't peddling art. 


Haiti should have used all the rubble and made gabion building blocks as a major part of their RR-kit of parts. They could have utilized the essence of Rome and created a community—radically. Wire baskets, forklifts and a vision to restore community. 


The biggest challenge I see here is who is in charge of the conceptual vision and can they handle utilizing principle #3? 


Thank again LW for all the posts.
9. [Lebbeus Woods at Sub-Studio Design Blog](http://blog.sub-studio.com/2012/01/lebbeus-woods/)
1.5.12 / 2pm


[…] LW blog Posted by Sean Tagged: architecture « Thomas Doyle – Surface to […]
11. [Radical Destruction: Lebbeus Woods « simdilikkaldi](http://simdilikkaldi.wordpress.com/2012/03/19/radical-destruction-lebbeus-woods/)
3.19.12 / 12pm


[…] Radical Destruction: Lebbeus Woods Share this:TwitterFacebookLike this:LikeBe the first to like this post. […]
13. [Engaging with the city's past « occursus](http://occursus.wordpress.com/2012/04/03/engaging-with-the-citys-past/)
4.3.12 / 9am


[…] its past. We've discussed post-traumatic urbanism through the lens of Lebbeus Woods' War and Architecture and have also begun to think through our engagement with (or perhaps literal and/or conceptual […]
15. [07 Visionary Building 1 « new vs old](http://newold12.wordpress.com/2012/07/06/07-visionary-building-1/)
7.6.12 / 8pm


[…] Lebbeus Woods WAR AND ARCHITECTURE: Three Principles Raimund Abraham ABRAHAM'S [UN]BUILT (with a text by Diane Lewis) #gallery-85-2 { margin: auto; } #gallery-85-2 .gallery-item { float: left; margin-top: 10px; text-align: center; width: 33%; } #gallery-85-2 img { border: 2px solid #cfcfcf; } #gallery-85-2 .gallery-caption { margin-left: 0; } […]
17. [What's in a frame. | a furn](http://architecturefurniture.wordpress.com/2012/08/10/whats-in-a-frame/)
8.10.12 / 4pm


[…] War & Architecture: Three Principles by: Lebbeus Woods […]
