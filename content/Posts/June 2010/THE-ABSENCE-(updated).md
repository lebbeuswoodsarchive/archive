
---
title: THE ABSENCE (updated)
date: 2010-06-15 00:00:00
---

# THE ABSENCE (updated)


[![](media/mak-raprog-1a-lr.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/mak-raprog-1a-lr.jpg)


[![](media/mak-raprog-3a-lr.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/mak-raprog-3a-lr.jpg)


A conference in Vienna this past weekend celebrated the continuing presence of Raimund Abraham, one of the most difficult architects to celebrate in any conventional sense. Taking place in the echoing, vast, main exhibition hall at the Museum fur angewandte Kunst (MAK), and organized by the master art impresario Peter Noever—the MAK's long-time director—the three-hour event was packed with people who came to see and hear a handful of international architects and artists who had trekked to Vienna for this occasion. No doubt people were curious to hear what Thom Mayne, Peter Eisenman, Wolf Prix (the sole ‘local'), Eric Owen Moss, the historian Kenneth Frampton, Vito Acconci, Jonas Mekas, and myself, would have to say about our fallen comrade—in his absence. Abraham, an Austrian by birth who had renounced his citizenship in protest against the election of a far-right party a few years ago, and had become an American citizen, remains a profoundly controversial figure in Vienna, not least for his uncompromising views on architecture. Everyone knew that in his presence such a conference would be a contentious affair, with Abraham challenging every hint of conventional thinking, or simply walking out.


Indeed, in his absence, this was not to be a forum for controversial ideas, but rather an occasion for anecdotes about the man, his life and ways. Not surprisingly, many of the more pungent stories had to do with Abraham walking out—of reviews of student work, of state occasions, of lectures, even of baseball games to which he had been invited and given expensive seats. The implication was not so much about his disapproval, but his refusal to (as Dylan Thomas once wrote) “suffer fools gladly.” His departures were usually abrupt and unannounced, leaving those who remained to wait and wonder. Perhaps we could call it ‘time to think?' Of course it was rude. Of course, it was unacceptable. Of course, people never forgot it and continue to speak about it, though by now—in his absence—with a curious kind of reverence. Who else would have dared? Who else would have been so contemptuous of the consequences? But then, who else would have flouted architectural trends with so little concern for people's opinions? Who else would have gone his own way so heedless of any criticism? All was part of the same self-determined fabric of thought and action, consistent through and through. How are we to come to terms with this man and his works, now that he is dead? At this event, at least some of us knew that he would have hated reverence as much as triviality, but that does not answer the question.


Was Abraham a selfish solipsist, a narcissist bent on having his own way, whatever the cost to himself and others? Or, was he an extraordinarily principled man whose refusals were moral examples of how to live with integrity in the present age of consumerist conformity? That he was an architect of extraordinary creative gifts and originality is not in doubt, but with Abraham we cannot separate his life and work, considering one without the other. Together they present us with a unique intellectual and ethical challenge that this well-intended and -attended conference did not come close to addressing. Regardless of its shortcomings, however,  it marked a beginning of discussion, not an end.


LW


SCENES FROM THE CONFERENCE:


[![](media/ra-mak-lwpeb1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/ra-mak-lwpeb1.jpg)


.


[![](media/ra-mak-lwtmb2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/ra-mak-lwtmb2.jpg)



 ## Comments 
1. [Guillermo Núñez](http://latempestad.com.mx/)
6.18.10 / 7pm


Dear Mr. Woods, I was wondering if it would be ok if we translated this piece in order to reproduce it in the website of La Tempestad, a mexican journal on contemporary art. Needless to say, the original source would be credited.
3. el.Pedro
6.19.10 / 11pm


I like to learn more about him, think my forms have some similarity with of them, maybe he do what I can't, maybe… I will like to understand his theories.


Please any source on internet.(fast)
