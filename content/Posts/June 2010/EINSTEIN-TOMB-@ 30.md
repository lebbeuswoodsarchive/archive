
---
title: EINSTEIN TOMB @ 30
date: 2010-06-09 00:00:00
---

# EINSTEIN TOMB @ 30


[![](media/lwblog-et30-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/lwblog-et30-1.jpg)


It has been thirty years since the summer of 1980 when the Einstein Tomb was launched on a beam of light into deep space, never to be seen again.


Its vanishing was as it should be, because Albert Einstein, the inventor of the twin Theories of Relativity (one for the electromagnetic/human scale, the other for the gravitational/cosmic scale) wanted no site for the veneration of his memory. To insure this, he instructed that his body should be cremated (which it was) and his ashes scattered over the Atlantic Ocean, the vast fluid domain separating his two homes in Europe and America (which they were).


Still, we—the heirs of his memory and ideas—are entitled to honor them both in some tangible form. The tricky part is to do this in a way that also honors his humble wish to be only a memory and not a venerated celebrity, secular-saint or demi-god. Perhaps he, not being an overtly religious man (though he sometimes said “God does not play dice with the universe” and “the Old One is not malicious”), was remembering the Old Testament commandment, “make no graven images.” In any event, the only solution, it seems, was to make a tomb, then send/take it away. This was/is the Einstein Tomb.


Of course, it is more a memorial than a tomb, as it contains none of his earthly remains. And just as Étienne-Louis Boullee intended in his Newton centtaph, it demonstrates its author's interpretations of its subjects' particular ideas about time and space. Both Newton and Einstein changed our ways of thinking about time and space, and consequently everything that happens in them, everyday life included. Newton gave us the mechanistic, deterministic universe—if we know the active forces we can precisely predict their outcome. Einstein gave us the counter-intuitive universe—things are ultimately NOT as they appear, but instead follow laws not readily apparent, though still immutable. His thought gave us elastic time and space, dependent on our relative point of view, and such systems as quantum theory, which have resulted (among many ways) in solid state physics and microchip (computer and communications) technologies. His ideas have radically impacted our world.


In the film striip above, we see the Einstein Tomb being assembled in the low-gravity lunar orbit. This microcosmic scene occurs at a moment of the alignment of the Moon, the Earth, and the Sun—a moment of mythic or perhaps especially portentous energy. A foreign object intrudes and reveals itself to be rectangular solid, akin to the Monolith described by Arthur C. Clarke and Stanley Kubrick in “2001.” Soon, however, it is joined by another rectangular solid and these come together in a cruciform—a cross-like shape. A religious symbol? But no, it is more a “plus” sign, formed by the intersection of two “minus” signs. At some point, this cross can be seen to expand on two opposing ends—skyscrapers, perhaps? Is this a city? Then suddenly a flash of light on the Moon seemingly pierces the cruciform, but actually drives it into space. Because the Einstein Tomb is a massless idea, it can travel at the speed of light far into the universe. In this way it will test Einstein's Theory of Gravitation, which predicts that this space and time will, paradoxically, meet itself again, and again.


“This vessel, this tomb


containing nothing


wandering on a random pulse of light


has always existed. Tomorrow,


or the day after tomorrow, it will appear


among the fixed and stable constellations


of night. Even now it approaches


from [limitless] possibility.


All random journeys


all the immortal corridors


begin and end in the vaulted space


of Earth's night,


under the vagrant light of stars.”


.


(below) The Einstein Tomb in lunar orbit, just before the light-launch into deep space:


[![](media/etmontage-2det1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/06/etmontage-2det1.jpg)


LW


For more on the Einstein Tomb:


<https://lebbeuswoods.wordpress.com/2009/09/27/the-vagrant-light-of-stars/>


.



 ## Comments 
1. quadruspenseroso
6.12.10 / 12am


Olinto De Pretto
3. [General](http://www.general.la)
6.12.10 / 5am


Apotheosis
