
---
title: MANUEL DELANDA  Emergence
date: 2010-07-27 00:00:00
---

# MANUEL DELANDA: Emergence


[Note: The following essay is the Introduction to a new book by Manuel DeLanda, easily the most important philosopher of the present day, concerning topics and concepts of particular relevance, I believe, for contemporary and future architects. It is considerably longer than my usual posts, but the clarity of DeLanda's writing makes it a compelling read. It is published here under rights of Fair Use in international copyrights law, meaning for educational and research purposes only. The book, entitled **Philosophy and Simulation: The Emergence of Synthetic Reason**, will be published by Continuum, London, in January 2011. LW]


.


[![](media/delanda-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/delanda-1.jpg)


(above) Manuel DeLanda.


.


**EMERGENCE IN HISTORY**


The origin of the modern concept of emergence can be traced to the middle of the nineteenth century when realist philosophers first began pondering the deep dissimilarities between causality in the fields of physics and chemistry. The classical example of causality in physics is a collision between two molecules or other rigid objects. Even in the case of several colliding molecules the overall effect is a simple addition. If, for example, one molecule is hit by a second one in one direction and by a third one in a different direction the composite effect will be the same as the sum of the two separate effects: the first molecule will end up in the same final position if the other two hit it simultaneously or if one collision happens before the other. In short, in these causal interactions there are no surprises, nothing is produced over and above what is already there. But when two molecules interact chemically an entirely new entity may emerge, as when hydrogen and oxygen interact to form water. Water has properties that are not possessed by its component parts: oxygen and hydrogen are gases at room temperature while water is liquid. And water has capacities distinct from those of its parts: adding oxygen or hydrogen to a fire fuels it while adding water extinguishes it.


The fact that novel properties and capacities emerge from a causal interaction was believed to have important philosophical implications for the nature of scientific explanation. In particular, the absence of novelty in physical interactions meant that explaining their effects could be reduced to deduction from general principles or laws. Because deductive logic simply transfers truth from general sentences to particular ones without adding anything new it seemed like an ideal way of modeling the explanation of situations like those involving rigid collisions. But the synthesis of water does produce something new, not new in the absolute sense of something that has never existed before but only in the relative sense that something emerges that was not in the interacting entities acting as causes. This led some philosophers to the erroneous conclusion that *emergent effects could not be explained*, or what amounts to the same thing, that an effect is emergent only for as long as a law from which it can be deduced has not yet been found.  This line of thought went on to become a full fledged philosophy in the early twentieth century, a philosophy based on the idea that emergence was intrinsically unexplainable. This first wave of “emergentist” philosophers were not mystical thinkers but quite the opposite: they wanted to use the concept of emergence to eliminate from biology mystifying entities like a “life force” or the “élan vital”. But their position towards explanation gave their views an inevitable mystical tone: emergent properties, they said, must be accepted with an attitude of intellectual resignation, that is, they must be treated as brute facts towards which the only honest stance is one of natural piety.


Expressions like these were bound to make the concept of emergence suspect to future generations of philosophers. It was only the passage of time and the fact that mathematical laws like those of classical physics were not found in chemistry or biology – or for that matter, in the more historical fields of physics, like geology or climatology – that would rescue the concept from intellectual oblivion. Without simple laws acting as self-evident truths (axioms) from which all causal effects could be deduced as theorems the axiomatic dream eventually withered away. Today a scientific explanation is identified not with some logical operation but with the more creative endeavor of elucidating the mechanisms that produce a given effect. The early emergentists dismissed this idea because they could not imagine anything more complex than a linear clockwork mechanism. But there are many other physical mechanisms that are nonlinear. Even in the realm of human technology we have a plurality of exemplars to guide our imagination: steam engines, thermostats, transistors. And outside technology the diversity is even greater as illustrated by all the different mechanisms that have been discovered in chemistry and biology. Armed with a richer concept of mechanism the emergent properties of a whole can now be explained as an effect of the causal interactions between its component parts. A large portion of this book will be dedicated to describe the wide variety of *mechanisms of emergence* that have been elucidated in the decades since the original emergentists first wrote.


Thus, what is different today from the early twentieth century views is the *epistemological* status of emergence: it does not have to be accepted as a brute fact but can be explained without fearing that it will be explained away. What has remained the same is the *ontological* status of emergence: it still refers to something that is objectively irreducible. But what kinds of entities display this ontological irreducibility? The original examples of irreducible wholes were entities like “Life”, “Mind”, or even “Deity”. But these entities cannot be considered legitimate inhabitants of objective reality because they are nothing but reified generalities. And even if one does not have a problem with an ontological commitment to entities like these it is hard to see how we could specify mechanisms of emergence for life or mind in general, as opposed to accounting for the emergent properties and capacities of concrete wholes like a metabolic circuit or an assembly of neurons. The only problem with focusing on concrete wholes is that this would seem to make philosophers redundant since they do not play any role in the elucidation of the series of events that produce emergent effects. This fear of redundancy may explain the attachment of philosophers to vague entities as a way of carving out a niche for themselves in this enterprise. But realist philosophers need not fear irrelevance because they have plenty of work creating an ontology free of reified generalities within which the concept of emergence can be correctly deployed.


What kinds of concrete emergent wholes can we legitimately believe in? Wholes the identity of which is determined historically by the processes that initiated and sustain the interactions between their parts. The historically contingent identity of these wholes is defined by their emergent properties, capacities, and tendencies. Let's  illustrate the distinction between properties and capacities with a simple example. A kitchen knife may be either sharp or not, sharpness being an actual property of the knife. We can identify this property with the shape of the cross section of the knife's blade: if this cross section has a triangular shape then the knife is sharp else it is blunt. This shape is emergent because the metallic atoms making up the knife must be arranged in a very particular way for it to be triangular. There is, on the other hand, the capacity of the knife to cut things. This is a very different thing because unlike the property of sharpness which is always actual the capacity to cut may never be actual if the knife is never used. In other words, a capacity may remain only potential if it is never actually exercised. This already points to a very different ontological status between properties and capacities. In addition, when the capacity does become actual it is not as a state, like the state of being sharp, but as an event, an event that is always double: *to cut-to be cut*. The reason for this is that the knife's capacity to affect is contingent on the existence of other things, cuttable things, that have the capacity to be affected by it. Thus, while properties can be specified without reference to anything else capacities to affect must always be thought in relation to capacities to be affected. Finally, the ontological relation between properties and capacities displays a complex symmetry. On one hand, capacities depend on properties: a knife must be sharp to be able to cut. On the other, the properties of a whole emerge from interactions between its component parts, interactions in which the parts must exercise their own capacities: without metallic atoms exercising their capacity to bond with one another the knife's sharpness would not exist.


A similar distinction can be made between emergent properties and tendencies. To stick to the same example: a knife has the property of solidity, a property that is stable within a wide range of temperatures. Nevertheless, there are always environments that exceed that range, environments in which the temperature becomes so intense that the knife is forced to manifest the tendency to liquify. At even greater intensities the molten metal may gasify. These tendencies are as emergent as the shape of a knife's blade: a single metallic atom cannot be said to be solid, liquid, or gas; we need a large enough population of interacting atoms for the tendency to be in any of these states to emerge. Tendencies are similar to capacities in their ontological status, that is, they need not be actual to be real, and when they do become actual is as events: to melt or to solidify. The main difference between tendencies and capacities is that while the former are typically finite the latter need not be. We can enumerate, for example, the possible states in which a material entity will tend to be (solid, liquid, gas, plasma) or the possible ways in which it may tend to flow (uniformly, periodically, turbulently). But capacities to affect need not be finite because they depend on the capacities to be affected of innumerable other entities: a knife has the capacity to cut when it interacts with something that has the capacity to be cut; but it also has the capacity to kill if it interacts with large organisms with differentiated organs, that is, with entities that have the capacity to be killed.


Since neither tendencies nor capacities must be actual to be real it would be tempting to give them the status of possibilities. But the concept of a possible event is philosophically suspect because it is almost indistinguishable from that of a real event, the only difference being the former's lack of reality. Rather, what is needed is a way of specifying the *structure of the space of possibilities* that is defined by an entity's tendencies and capacities. A philosopher's ontological commitment should be to the objective existence of this structure and not to the possibilities themselves since the latter exist only when entertained by a mind. Some possibility spaces are continuous having a well defined spatial structure that can be investigated mathematically, while others are discrete, possessing no inherent spatial order but being nevertheless capable of being studied through the imposition of a certain arrangement. The space of possible regimes of flow (uniform, periodic, turbulent) is an example of a continuous possibility space in which the only discontinuities are the critical points separating the different tendencies. The space of possible genes, on the other hand, is an example of a discrete space that must be studied by imposing an order on it, such as an arrangement in which every gene has as neighbors other genes differing from it by a single mutation. As we will see in the different chapters of this book the structure of possibility spaces plays as great a role in the explanation of emergence as do mechanisms.


The chapters are deliberately arranged in a way that departs from the ideas of the original emergentists. These philosophers believed that entities like “Space-Time”, “Life”, “Mind”, and “Deity” (not “god” but the sense of the sacred that emerges in some minds) formed a pyramid of progressively ascending grades. Although the levels of this pyramid were not supposed to imply any teleology it is hard not to view each level as leading to the next following a necessary sequence. To eliminate this possible interpretation an entirely different image is used here, that of a contingent accumulation of layers or strata that may differ in complexity but that coexist and interact with each other in no particular order: a biological entity may interact with a subatomic one, as when neurons manipulate concentrations of metallic ions, or a psychological entity interact with a chemical one, as when subjective experience is modified by a drug. The book begins with purely physical entities, thunderstorms, that are already complex enough to avoid the idea that their behavior can be deduced from a general law. It then moves on to explore the prebiotic soup, bacterial ecosystems, insect intelligence, mammalian memory, primate social strategies, and the emergence of trade, language, and institutional organizations in human communities. Each of these layers will be discussed in terms of the mechanisms of emergence involved, drawing ideas and insights from the relevant fields of science, as well as in terms of the structure of their possibility spaces, using the results of both mathematical analysis and the outcomes of computer simulations.


Simulations are partly responsible for the restoration of the legitimacy of the concept of emergence because they can stage interactions between virtual entities from which properties, tendencies, and capacities actually emerge. Since this emergence is reproducible in many computers it can be probed and studied by different scientists as if it were a laboratory phenomenon. In other words, simulations can play the role of laboratory experiments in the study of emergence complementing the role of mathematics in deciphering the structure of possibility spaces. And philosophy can be the mechanism through which these insights can be synthesized into an emergent materialist world view that finally does justice to the creative powers of matter and energy.


**MANUEL DELANDA**



 ## Comments 
1. Bryan
7.29.10 / 11pm


As an architecture student, the concept of Emergence is particularly interesting because of the dynamic qualities implied by the individual mechanisms of emergence. The distinct concepts of properties, tendencies, and capacities speak of an architecture that is flexible and ephemeral, yet implies a defined, yet not always visible, structural order. Due to the innate transience of building conditions (i.e light, climate, use), it seems that the majority of emergent mechanisms are either tendencies or capacities. 


This is especially interesting because the concept of possibility is at work in my current project. The idea that a possibility is merely a result of properties, tendencies, and capabilities may help be reach a higher degree of logic for certain decisions. This is definitely a book I am going to pick up.
3. [Chris Teeter](http://www.metamechanics.com)
7.31.10 / 6am


i decided to state theory first and explain existential biased condition later…


Mr. Woods you are too modest when you say just “for contemporary and future architects”. For all of philosophy in general Delanda is the most important philospher alive today. I'd speculate this modesty is a result of the architect's reputation for incorrectly borrowing and butchering good philosophy in the past and since Delanda's biggest fans are architects we can't just say the man is right without some kind of disclaimer – i'm an architect.


I've noted the growth in the linguistic section next to the philosophy section in the bookstores over the last decade. When you get towards the end of the philosophy section you hit Ludwig Wittgenstein who spent a lot of time working out the whole issue of actual understanding versus what could actually can be expressed as a true presentation of actual understanding –  

 “6.7 Whereof one cannot speak, thereof one must be silent.” He of course like most old men dwindled off into issues “On Certainty.”  

You jump sections from philosophy and enter linguistics and you see Wittgenstein again a few books after Noam Chomsky…. My point is the section soon to be growing next to linguistics is Chemistry…


here's my quick 10 point history of philosophy of it all since the 1500's-


1. Galileo mathematizes everything.  

2. How do I know I exist? i'm asking the question (Descartes)  

3. philosophy of natural science becomes physics (Leibniz, Newton)  

4. i'm the only reality that can be known, I see the universe thru windows shooting me information.  

5. the world is the movie the Matrix and its all experienced based anyway. (Berkeley, Hume)  

6. Kant organizes it all and gives us absolutes and we can now all sleep at night. Thank you to a person who most likely was ‘suffering' from OCD, at least mentally…ha (I found that funny)  

7. Nietzsche…now that we are so certain we don't need faith in anything anymore and hence we don't really know what we want to become…it's about the will and not the thoughts…he went blind and insane or was he right?  

8. Husserl…Galileo fucked it all up for us, you really can't mathematize all the senses, just VISION.  

9. Heidegger…the question of being, the question, that's the becoming, that's the fun stuff we want to talk about but really can't because once you've talked about becoming you have become and therefore you're not realy talking about becoming anymore, but just a moment in time..you're talking about that which became which is not the same as becoming…so you are just there – DASEIN. Da Sein…there be.  

10. Deleuze – material existentialism, physchology…nevermind the problem of being so damn abstract all the time, that's the problem. Husserl was onto something people.


Delanda just keeps plugging on into the biological chemical materialism behind it all., wading thru this inevitable human error of abstracting everything into a concept, verbs becomes nouns…and Georg Cantor thinks he can make sets that contain multiple infinities greater than other sets of multiple infinities?!?!?


People still detach themselves from the material universe into a world of conceptual absolutes that form logics and languages purely based on the fundamental abstracted concepts they derived from a historical analysis of a past experience. To abstract a concept, make it a noun, build a philosophy on it – is to set in stone one moment in time and to disregard all moments that follow, therefore continueing an essentially erroneous interpretation of anything.  

-your epitaph is your epiphany.  

-or wait –  

 your epiphany is your epitaph.


Delanda is attacking straight on the very problem of humans constanttly calling personal time-outs and establishing something. Anxiety is a bitch. 


I felt like Giorio Agamben got really close to solving this damn problem for me in “Infancy and History” around page 64 in “Radical Thinkers” – summed up with “But the human is nothing other than this very passage from pure language to discourse; and this transition, this instant, is history.”


Delanda's discourse is material and that's why he's so damn important. He is at another level that most of us won't get for another 50 years or so i think…do i even get him yet?


existential historical condition while writing this – plenty of good belgium and german beer, Ry Cooder playing the slide guitar, and a bit of infused friday night insanity…i spent 1 hour playing with L-Systems (I used to do keg stands, what happened?)…





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.31.10 / 2pm
	
	
	Chris Teeter: I deeply appreciate your highly personal interpretation of the history of Western philosophy and found many of the line entries both astute and amusing. Is there a reason that you don't mention the Greeks, Plato, Aristotle and others? Also American pragmatists, James, Dewey, Rorty, especially in relation to DeLanda?
	
	
	
	
	
		1. Chris Teeter
		8.2.10 / 1pm
		
		
		Mr. Woods,
		
		
		There are legitimate reasons.
		
		
		1. I am not familiar enough with the American Pragmatists to make a claim of any sort…so I have some reading to do.
		
		
		2. The Greeks – I did enjoy reading greak philosophy and there were a few lesser known greek philosophers that got my attention…but ultimately it seemed a bit less sharp regarding rational thought and ontology. By sharp I mean the way in which the idea and existence is discussed by modern philosophy, direct language about the very questions. In this sense Nietzsche was a tough read the first couple times, but other philosophers helped me wade thru it.
		
		
		3. I've fully clung to the intro pages to Husserl's really long titled book regarding is observations on Galileo and find you could link just about everything that is naturally assumed in the sciences back to Galileo.
		
		
		4. there is a theme to my quest in philsophy and on this early Monday morning at work I can not summarize it clearly now, will get back to you tonight.
		
		
		thank you for the response and once again I have to congratulate you on being one of the few websites that delivers intense thoughtly effeciently (at least to me)
5. [River](http://www.routendesign.com/scott)
8.2.10 / 5am


I didn't even read this post. Just took a quick look at the pattern of stimulus and replies online. Bwa=ha=ha!


<http://subversivethinking.blogspot.com/2009/06/spiritual-dimension-of-time-travel-fred.html>


I'm going to read it more thoroughly tomorrow when I'm not drunk on the fading cusp of Lughnassad.


I'm going to be in NYC on the 6th from nine to noon qafter a long drive. Would we like to meet?
7. [chris teeter](http://www.metamechanics.com)
8.3.10 / 1am


I will speak like a prophet but all I state are my thoughts, this is my dialectical style. Hungry, piece of cheese, red bull, long day of work with a long night to come and body breaking down to an oncoming virus while on bus writing this with blackberry…ontological states should always be informed to others prior to releasing thoughts…


But this is why you have formalities, so everyone can jump onto the same wavelength or subscribe to the same context of thoughts, bullshit and myths requiring a faith in unified understandings. Formalities are boring and easy, the trick is to create formalities from informalities…


I believe this would be termed “play”? Which often leads to emergence? Artificial intelligence is a formal machine that can play? The event? The difference? But the funny thing is multiple repetitions emerge as new a difference if viewed as a whole…


My personal ontologocial quest is simple – determine “where” literal is chemical and ontologically clear but not not in the form of memory – true learning. Memory is important but a memory is not the ontological answer I am looking for. A memory can not be anything more than a noun, a number, a material at 0 degrees kelvin. Emergence as an instance can be remembered but to formalize, to define with static ontological objects is to strip the emergence of its essence and to merely state a moment of history, a result, a set number, a solid state of affairs.


You can now see my early attraction to Wittgenstein then quickly on to Delanda…


I believe both Poicare and Bergson thought the notion of multiple valued infinities was absurd. Further, Bergson clarified for me at least, that the number “0” or the mythical ontological object “nothing” was rather a movement, a thought requiring action, not a “thing” (noun, number, etc…) Rather both infinity and zero are actually formulas or actions like adding and subtracting. They “can not be” but only are “part of a becoming” and therefore you can not create a memory of nothing or zero or infinity.


Watch this, a total bullshit conclusion that makes perfect sense in context of this essay- “Without time neither infinity nor zero could exist.”. Makes sense right?


Every part of that sentence is incorrect and I won't do a 3 part parrallel conclusion here (language, math, physics)…just language. The statement is completely false because –  

1. Object “A” (time) must be present for objects “B + C” to be present. 1 guy and 2 gilrs hanging out at the bar.  

 2. This means object B +C must come from nowhere after object A comes from nowhere and surely you agree something does not come from nothing. Well the guy walked into the bar and girls came from the bathroom, but where were they before this?  

3. So object A has always existed but that means B and C did and therefore are they really necesarly dependent? The women depend on the man?  

And this could go on and on and on…in short this is what a paradox is – assumed beyond reasonable doubt that a verb really can exist lietrally as a noun. Time is a verb like zero and infinity. To create an abstraction, a memory of any of these three is to create a bullshit ontological object, therefore negating practically anything that would follow…time, zero, and infinity are prime suspects for foundations to a full philosophy.


To quote Wittgensteins “whereof one can not speak one must fall silent.” Is to give up. To get excited about emergence the way Delanda speaks of it is to continue my quest for what would ultimately be the verb or formula that makes artifical intelligence possible?!? or us human, a understanding of learning.


Is this even possible? for now I say no, but to push to understand learning is to accelerate intellectual growth of humanity. 


What does this have to do with architecture? Same as above accelerating practice and actual abilities of architecture.


Acceleration, hmmmm faster and faster and a new state emergences.
9. [Ken Jones](http://www.howcitizenshipworks.com)
8.4.10 / 6pm


Reading this I'm very curious to know Prof. DeLanda's world view — ie of human history … human “progress” … or lack there of, etc.  

And how do you see these things now, Leb?  

I remember your commitment to the “struggle” and your appreciation of Kazantzakis' abandonment of all hope — the necessity for man to muster the courage to plunge ultimately into the abyss.  

Well, what has surprised me is that the abyss seems to be approaching fast and furious whether we would choose to jump into it or not.  

A little reflection: The last and current centuries appear to have been dominated by the world view of just a few great utopian thinkers. A dozen or so decades of nearly constant war apparently owe their existence to the peculiar preferences these men showed for one or another linear view of man's evolution. Locke, Nietzsche, Marx, Janger, D'Annunzio, among others have all had their day, and they have each failed pretty miserably.  

Now it's the current ruling elite's turn to push their own preferred linear view of mankind's destiny to its inevitable collapse. They've chosen the philosophy of Leo Strauss to prove that the American Dream of unbridled capitalism is equally doomed.  

Well, my own world view has been influenced by Giambattista's Vico's cyclical view of history. Ever since it came to my attention in the works of James Joyce. Mankind is certainly evolving and even “progressing.” But this evolution is caught in an endless cycle of creation and decay.  

Unfortunately if Vico is right, the present moment finds us iin a period of massive social decadence and decay. Perhaps the greatest of such moments in human history.  

I'm inclined to feel that the next hundred years or so are not going to be very pleasant.  

Which brings me back to Prof. DeLanda, and how he can muster such creative artisitic enthusiasm in the face of this power-mad wall of ignorance, greed, and nihilism.  

What exactly is Mr. DeLanda's world view? Where's his existentialist angst after all. I'm really curious. (I scanned his book and did get various hints.).  

And what about your world view? Is it pointless and futile effort after all, but you do it anyway because of your commitment to the struggle?  

Whatever your world views, I gratefully thank you both for continuing to do what you do..





	1. [Chris Teeter](http://www.metamechanics.com)
	8.6.10 / 3am
	
	
	Ken, it's been 2-3 years since I read it – “A New Philosophy of Society – Assemblage Theory and Social Complexity” by Manual Delanda
	
	
	although a social ontological book I'd say closer to advanced economists thinking and its Delueze assemblage theory applied, now just looking for someone to produce the math.
	
	
	ontologocial indepenence in scale, important.
	
	
	Ken, if i was guessing, it's neither a linear or circular world view, rather via linear movement or cycles of patterns emerging assemblies as new wholes (entities previously unknown) will be created. Things becoming that didn't exist before due to the internet is an easy example.
	
	
	decay more as change in patterns of movements containing assemblies and no progress in the sense of linear.
	
	
	Mr Wood's world view, love to hear it? granted I bet we could find it on this blog…
	3. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.6.10 / 3pm
	
	
	Ken Jones and Chris Teeter: I love this kind of speculative discussion, not because it leads to any sort of definitive conclusion, but because it generates energy among we who participate, energy essential to living with a sense of purpose if not exactly with a sense of meaning.
	
	
	Of course, Ken, I remember the days when we pored over Yeats' “A Vision” looking for the key to knowing how any person comes to incarnate any one of the twenty-eight incarnations of the human soul it so meticulously lays out. What a beautiful folly! We never found the key, and concluded that it didn't exist. Understanding could not be attained by a formula but only by interpretation and, in a sense, by poetic vision. There are no short-cuts, indeed, no pre-determined truths.
	
	
	The question of Manuel DeLanda's world-view is a good one. He's a materialist and would probably answer (I'm guessing, never having discussed it with him) that he doesn't believe in the value of metaphysical systems of any configuration—they only get in the way of reality. He states the case pretty clearly himself: “…philosophy can be the mechanism through which these insights can be synthesized into an emergent materialist world-view that finally does justice to the creative powers of matter and energy.” Here he is an optimist, still believing in “mechanism” and the other ultimately deterministic processes. From an Existentialist point of view, he is whistling in the dark.
	
	
	Chris, you are right, my world-view can be found in the posts on this blog. In short, I would say that we—all of us—exist in a sliver of probability that enables our existences, our speculations. We—each of us—must invent the world anew every day, in what we think and do. When we cease to do that the probability vanishes and there is only oblivion.
11. [Chris Teeter](http://www.metamechanics.com)
8.7.10 / 4am


“…that he doesn't believe in the value of metaphysical systems of any configuration…”


is this why i picked up Kant's Prolegomena the other night trying to read myself to sleep? 


beautiful follies! if we could only alllow ourselves to be so niave…i'm attracted to this blog the same reason I imagine most are, nothing ontologically niave here.


“ultimately deterministic processes”…mr woods i had a slight disagreement with the placement of the statement in your analysis of Delanda, and maybe it's my opinion bleeding thru Delanda's works. I did have him as a lecturer at UPenn and remember him being quite same as those pastors i'd have to listen to in my youth (optimistic with hope, or masking the inevitable with hope). emotionally and “spiritually” there is more to his interpretation of ‘deterministic processes' than the connotations of that phrase will ever do anyone justice.


same time at UPenn Cecil Balmond was present and I read more than his popular book “Informal”, there was the search for the Sigma Code, presented in a fun fictional slightly mystical way. ultimately, my opinion, the book presented clearly that the patterns of the universe were set regardless of enumeration….the verbs can only do so much to the nouns…


anyway, here's a guy (Cecil) who in practice does real things but makes the material results mystical in thought..see informal (p. 367): the knot of the parking garage in holland was a mathematical afterthought on the back of some template page (green ink pattern on white page). while Delanda makes the material virtually real. there's a whole section in 1000 Plateaus where virtual is presented as real as the real, and if you take religion as an example with real material results, i think it's clear the virtual is very real…


i'd say Delanda is not whistling in the dark, he's riding the fucking wave, light or no light…but it stands to reason light comes with energy.


speaking of poetic, Jim Morrison's only friend was the end, and Nietzsche saw society's faith in itself failure long before anyone else did.. i'd rephrase your statement  

– “the creative powers of matter and energy are now a philosophy that can be mechanisms through which existential insights can be synthesized into an emergent world-views that finally do justice to thought and existence on a material level…and we answer Rene Descartes' anxiety…or we could just prescribe some pill to Rene Descartes. Philosophy book or medication, society is cycling down Ken into oblivion… ” 


i've really cleared a lot of my philsophy on this post, thanks Lebbeus and Ken…


“Repetition is a condition of action before it is a concept of reflection.” – Deleuze, Difference & Repetition, (p. 90) yes Ry Cooder still playing slide guitar, plenty of belgium beer and it's Friday night, ha…(i am still cool, because i never cared anyway;)
13. [Et par Manuel de Landa forelæsninger « Aakropolis](http://aakropolis.wordpress.com/2010/10/05/manuel-de-landa-forel%c3%a6sning-deleuze-and-the-use-of-the-genetic-algorithm-in-architecture/)
10.5.10 / 10am


[…] Link til et par Manuel de Landa tekster <http://www.t0.or.at/delanda/> Manuel de Landa om emergence <https://lebbeuswoods.wordpress.com/2010/07/27/manuel-delanda-emergence/> […]
15. [System Stalker Lab » Blog Archive » Emergence: Manuel DeLanda](http://www.mayaprzybylski.com/systemstalkerlab/?p=651)
10.31.10 / 9pm


[…] lebbeuswoods.wordpress.com/2010/07/27/manuel-delanda-emergence/ […]
17. [Oscilloscope](http://www.oscilloscopelab.com)
11.16.10 / 5pm


for kitchen knife, i would always use ceramic kitchen knifes because they are sharper and tougher than steel knifes ~.`
19. [Simulated Evolution in Architecture [Manuel DeLanda] | RASMUS BRØNNUM – en Arkitektur Blog](http://rasmusbroennum.wordpress.com/2011/08/08/simulated-evolution-in-architecture-manuel-delanda/)
8.8.11 / 1pm


[…] fødte filosof og forfatter, Manuel DeLanda,  der har boet i NY siden 1975. Avantgarde arkitekten Lebbeus Woods har bla. sagt om DeLanda: “(…) easily the most important philosopher of the present […]
21. [Filosofie en simulatie. « christophecalis](http://christophecalis.wordpress.com/2012/03/29/filosofie-en-simulatie/)
4.1.12 / 4pm


[…] leidt zijn boek in met de bespreking van het begrip emergentie doorheen de geschiedenis. Share this:ShareFacebookTwitterPrintEmailDiggLike this:LikeBe the first to like this […]
