
---
title: THE CUBE
date: 2010-07-21 00:00:00
---

# THE CUBE


When the Cube arrives, it will change everything.


The Cube is so simple, therefore so complex. Simple things are never what they appear to be. However, their seeming simplicity is not a deception, but rather a way of being everywhere and hardly being noticed. The change comes later, and slowly at first.


Many expect the Cube to arrive at night, when it will be difficult to see. There will be no public announcements, no welcome ceremonies. More importantly, there will be no defenses thrown up against it. Quietly, but surely, it will come.


There are several fine books written a while ago about its advent. The one penned by the well-known anthropologist (I do not think it wise to mention names, but you know the one I mean, the professor who received a Nobel earlier this year) laid out the case in unequivocal scientific language:


“There is nothing mystical about the Cube,” he writes, “to the contrary, it is a direct result of many lines of rigorous research coming together. It is the perfect synthesis of  scholarly fieldwork, communications technology, and epistemology. In many ways, really, a culmination of a wide range of scientific endeavor.”


Another book, reviewed in much of the popular press (including online), is authored by a theologian at the most famous Ivy League Divinity School. She asserts that “we must play down the Messianic aspect of the Cube, because that will only breed hatred and fear. Instead, we should consider it a part of Nature, like a lunar eclipse.”


The highly illustrated volume written by the architect widely celebrated for innovative projects concludes with the implication that the Cube is based on one of her designs. “It represents a radical shift,” she writes, ”not only in ideas about architecture, but also in city planning. Like Modernism in its time, it will change everything.”


These widely read and talked-about books, together with dozens of articles in newspapers and magazines, as well as incessant commentary on cable news channels and countless blogs, created for a while an almost unbearable sense of anticipation of the coming of the Cube. When it did not come, though, the attention of the mass-media, as well as the public, shifted elsewhere. Even the academics and scholars moved on.


Still, in the thoughts and dreams of many regular people, the Cube lingers as an idea, a possibility, a hope for the future. I am one of those, and consider myself lucky to believe that when the Cube arrives—which it will, it must, really—it will change everything, forever.


.


[![](media/cube-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-6.jpg)


.


[![](media/cube-52.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-52.jpg)


.


[![](media/cube-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-7.jpg)


.


[![](media/cube-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-8.jpg)


[![](media/cube-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-1.jpg)


.


[![](media/cube-91.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/cube-91.jpg)



 ## Comments 
1. el.Pedro
7.22.10 / 12am


good work it's necesary a cube urgently, but not so formal the sociology need it too.
3. [THE CUBE (via LEBBEUS WOODS) « Nico Jenkins](http://nicojenkins.wordpress.com/2010/07/22/the-cube-via-lebbeus-woods/)
7.22.10 / 1pm


[…] When the Cube arrives, it will change everything. The Cube is so simple, therefore so complex. Simple things are never what they appear to be. However, their seeming simplicity is not a deception, but rather a way of being everywhere and hardly being noticed. The change comes later, and slowly at first. Many expect the Cube to arrive at night, when it will be difficult to see. There will be no public announcements, no welcome ceremonies. More imp … Read More […]
5. [Yorik](http://yorik.uncreated.net)
7.22.10 / 1pm


This reminds me much of a wonderful graphic novel called “La fièvre d'Urbicande”
7. [Lewis Wadsworth](http://lewiswadsworth.net/)
7.29.10 / 2am


The previous commenter, Yorik, makes quite a valid comparison, Mr. Woods. *Fever in Urbicand* by Francois Schuiten and Benoit Peeters, NBM 1990 (for the English version) describes almost the inverse of your piece: an architect discovers a mysterious cube, which begins growing into a network; the exponential growth reconnects deliberately quarantined portions of a city controlled by a rigid Fascist government; that government finds it cannot control the situation due to the many possibilities for movement and communication opened up by the network and promptly collapses; the network spawned by the cube finally grows so large that it vanishes to human perception; and instead of reverting to its previous state the city determines to recreate the vanished artifact that had demolished its rigid organization.
9. Colonel Willard
7.29.10 / 8pm


This reminds me of the prismical cube that transports the evil villains wearing disco outfits from Krypton in Superman II. I am absolutely serious when I say this.


That cube (closely resembling an ice cube) could also be landing in a glass of Scotch, to make it a Scotch on the rocks.
11. Rat Fink
9.2.10 / 7pm


I imagine something akin to a Mecca and UFO baby.
