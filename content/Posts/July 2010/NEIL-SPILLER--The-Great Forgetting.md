
---
title: NEIL SPILLER  The Great Forgetting
date: 2010-07-17 00:00:00
---

# NEIL SPILLER: The Great Forgetting


**DIGITAL SOLIPSISM AND THE PARADOX OF THE GREAT ‘FORGETTING'** by **Neil Spiller**, Professor of Architecture and Digital Theory, Graduate Architectural Design Director, Director of MArch (Architectural Design), The Bartlett School of Architecture, London. It is published in the “Counterpoint” section of the July/August 2010 issue of Architectural Design (AD), London.


[![](media/ad-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ad-1.jpg)


(above) Cover of the July/August 2010 issue of Architectural Design (AD).


.


“The idea of truth is the most imaginary of solutions.”


Christian Bok, *‘Pataphysics: The Poetics of an Imaginary Science*, 2002, p. 18


“Neither a fine art nor a science, architecture has only recently begun to realize its true potential, mainly through a hermeneutic approach that can engage the intricacies of its historical reality. Yet teaching and practice continue to be polarized between those two false alternatives: fine art and applied science. The introduction of computers into architecture during the last two decades has helped reduce architectural discourse to issues of instrumentality. The most popular discussions presume the importance of this so-called paradigm shift and focus on the potential and limitations of this instrument, aiding the perpetuation of the dichotomy. Thus theoretical discourse tends to remain caught up in instrumental issues of form (innovation) and production (efficiency), while the humanistic dimension of architecture is further jeopardized and educational programs become increasingly vocational.”


Alberto Perez-Gomez, *Built Upon Love: Architectural Longing after Ethics and Aesthetics*, 2008, p. 199


.


This edition of AD can be applauded for its attempt to theorize the ‘new' processes and techniques of making and fabricating building and the emerging opportunities in the convergence of engineering and architecture. However, it is important to demand for architecture an approach to architectural production which, while valuing the new hybrid notions of making, also predicates its output on poetics. Much recent architecture, especially the well-known examples, has been devoid of humanity and panders to a need for ever more gratuitous complex surfaces and structures. This justifies or obscures their simple, apolitical and vacuous objectives. Our short-sightedness caused by the development of ever more dexterous ‘printing' technology, the ubiquity of global capitalism and the myth of the deity architect has encouraged a great ‘forgetting'—a forgetting that has subtracted the humanity from the architectural products of our era. This forgetting is threatening to ruin good schools of architecture, their graduating students and the profession that they enter.


Architecture and its creation is a complex entity; it cannot all be wholly produced by computers—no matter how powerful or artificially intelligent they may be. Architects need to be taught to understand the intricacies of space and the various yardsticks that can be used to measure it—and equally the number of creative tactics that can be used to create it. I'm not arguing here for some Luddite future, but for a symbiotic use of new technology with an understanding of the human longing to express humanity's rich spectrum of aspirations and hopes in architecture and its lineaments. I'm also not arguing here for a resurgent historically based Postmodernism style in architecture. I am arguing for an architecture that is not just about itself, that is not just narcissistic. An architecture that engages with humanity, its joys and fears, its actual and mnemonic context and its aspirations towards cross-cultural citizenry. This is hard to do, as Perez-Gomez states:


“Poetic forms such as architecture seek participation by speaking not about the speaker but about the ‘world,' by expressing not technologic control or political domination but true wonder and the supreme mystery of humankind…the difficulty of such a task should not be underestimated, however. Contemporary mental pathologies notwithstanding, modern man and woman remain determined to exclude whatever cannot be articulated through logical reason.”


**The Royalty of Science and the Nomadity of Architecture**


In our era we are led to believe that the pursuit of scientific knowledge is predicated on precedent, just like the law. Science allegedly utilizes controlled experiment through clear, succinct methodology and specific unambiguous language. Scientific concepts are refined over time with an epistemology that is about honing, simplifying, and reducing down to a fundamental, inescapable, holistic truth. Further, this truth is consistent and accepted across the universe, at all scales (from the microcosmic to the macrocosmic), and in all matter, organic and inorganic. Any worldview short of this ideological dictatorship is pushed into the realm of ‘art,' a world populated by erratic sophists—a world ultimately useless and marginalized. It is through this meta-methodology that science holds and controls society's reins. Without this form of imperial tyranny, other approaches might not be so easily dismissed as arcane or even evilly occult. Christian Bok describes this condition as “what Deleuze and Guattari might call the royal sciences of efficient productivity [which] have historically repressed and exploited the nomad sciences of expedient adaptability.”


At their root, the royal sciences seem to have a misconception about language and communication. Language has a propensity for inaccuracy, for personalization, for misconstruing and misreading meaning, for relativity. It is also emotively subjective. Scientists perceive themselves fighting against this ontology of language and asking us to believe in their (own) objective and ubiquitous language to describe their allegedly ubiquitous knowledge. It is here that science's biggest error has been made, and it is here that poetry through its acceptance of the ontology of language can offer a more fecund way of seeing the world.


Architects must not be radical solipsists, believing that everything in the world is dependent on their perception of it. One might consider the model of the second-order cyberneticist and that of radical Constructivists. The conversation between an architect's work and the user/viewer of it should be able to evolve in all manner of different ways, some of which will have been considered by me and others not. In short, it is reflexive and often beyond full creative control. It is full of elision and illusion, feedback and readjustment, depending on the system and its observers. Further, the radical Constructivist acknowledges that we make our worlds by interacting with them and that they are all different, exceptional, particular.


**‘Pataphysics and Exceptions**


While a second-order cybernetic understanding of our worlds is useful, we should also consider design conversations that use the errant poetics of Alfred Jarry's ‘pataphysics (apostrophe deliberate). It is my opinion that these two paradigms are not mutually exclusive as both deal in the particular and the exceptional.


Along with the creation of Pere Ubu, Jarry is remembered for his creation of Doctor Faustroll and the ‘science' of ‘pataphysics:


“‘Pataphysics had appeared in Jarry's most early work, but in 1898 he wrote the *Exploits and Opinions of Doctor Faustroll, ‘Pataphysician* (not published until 1911). This book set out the sketchy outlines of the poetic affrontery that is ‘pataphysics. ‘Pataphysics is the science of the realm beyond metaphysics…It will study the laws which govern exceptions and will explain the universe supplementary to this one; or less ambitiously it will describe a universe which one can see—must see perhaps—instead of the traditional one…Definition: ‘Pataphysics is the science of imaginary solutions, which symbolically attributes the properties of objects, described by their virtuality, to their lineaments.”


All of Jarry's prose and poetics are predicated on what Christian Bok has called the ‘Three Declensions of ‘Pataphysics.' These are the algorithm of Jarry's art: Anamalos (the principle of variance), Syzygia (the principle of Alliance), and Cinamen (the principle of Deviance). Jarry named the ability of a system to swerve, the Cinamen, in reference to Lucretius' poem De rerum natura (the minimal swerve of an atom).


In *Subliminal Note* (1960) Roger Shattuck attempts to define ‘pataphysics, a task he calls ‘self-contradictory:'


“'Pataphysics relates each thing and each event not to any generality (a mere plastering over of exceptions) but to a singularity that makes it an exception….In the realm of the particular, every event arises from an infinite number of causes….Students of philosophy may remember the German Hans Vaihinger with his philosophy of ‘al ob.' Ponderously yet persistently he declared that we construct our own system of thought and value, and then live ‘as if' reality conformed to it.”


The importance of both ‘pataphysics and the radical Constructivism of some second-order cyberneticists has a common philosophical precedent. The notion that by being in, observing and operating in the world we construct personal epistemologies is a trait of both paradigms. It is a way of thinking that is connected to the world and yet beyond it, which is a precedent an architecture would benefit from remembering. Bok writes, “As to understand on behalf of truth is to be reactive, accepting of the world ‘as is,' but to misunderstand on behalf of error is to be creative, inventing the world of the ‘what if.'”


 **The Vision**


Where does all this lead? It leads to a vision that is probably just out of reach right now but soon will be attainable. It is an architecture that dovetails into its site at not just the anthropocentric scale but at ecological scales, microcosmic and cosmoscopic scales. An architecture that has the capacity to reboot torn ecologies with helpful architectonic scaffolds, which dismantle themselves when all is well again. An architecture that traps more carbon than its environmental footprint. An architecture that contributes to the health of its users and environment. An architecture that hasn't forgotten history, or how we are all different. An architecture that rejoices in that difference. An architecture whose exquisite tailoring is imbued with nuances that resonate with familiar and non-familiar worlds. An architecture that knows where it is and why it is and what it has to offer, but doesn't deny its difference and ours.


This surely must be any architect's goal in the 21st century—a goal that denies ill-fitted containers and the design of objects as obstacles. Architecture that digitally, historically, uncannily and ecologically doesn't FORGET. An architecture led by structural expedience seldom delivers the rich tapestry of multivalent parameters so desperately needed in today's fast-moving world.


**Neil Spiller**


Images published with the AD article:


(below) *And It Can't Be Helped* (1981), by Daniel Libeskind


[![](media/ad-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ad-2a.jpg)


(below) *11 The Other Side, Icon and Idea* (1978), by Daniel Libeskind


[![](media/ad-2b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ad-2b.jpg)


(below) *Dee Stool (miniature ‘Pataphysical laboratory)*, (2003), by Neil Spiller


[![](media/ad-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ad-3.jpg)


(below) *Epicyclarium, night elevation* (1984) by Lebbeus Woods


[![](media/ad-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ad-4.jpg)


.


More on Neil Spiller:


<http://www.bartlett.ucl.ac.uk/research/architecture/profiles/Spiller.htm>


More on radical Constructivism:


<http://en.wikipedia.org/wiki/Constructivist_epistemology>


Even more on radical Constructiivsim from the LW blog:


<https://lebbeuswoods.wordpress.com/2008/11/09/zeroes-and-ones/>


More on second-order cybernetics:


<http://en.wikipedia.org/wiki/Second-order_cybernetics>


Get the issue of Architecural Design (AD):


<http://www3.interscience.wiley.com/journal/109924136/home>



 ## Comments 
1. [chris teeter](http://www.metamechanics.com)
7.27.10 / 12am


This indirectly reminded me of my first semester in grad school where I tried to find inspiration in study biological cell structures. Having only an architecture background with natural math and geometrical skills, by end of semester I realized I was really just studying royal science abstractions…nothing more than a biologists geometric summaries of their reality.


The following semester I converted over to Delanda, Kwinter, and Deleuze fully 


is this a counterpoint to computers? I could make the same arguement about architecture by pen…don't let the instrument determine your mode of thought, and sketching and drawing things hardly makes better architecture…just wanted to point that out.
3. Dean Morrison
8.2.10 / 7am


Turing machines are indeed useful, and a through a hermeneutic, cybernetic approach, can be used to generate articles like the one above:


<http://www.elsewhere.org/pomo/>
