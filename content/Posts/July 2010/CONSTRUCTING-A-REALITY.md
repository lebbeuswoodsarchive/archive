
---
title: CONSTRUCTING A REALITY
date: 2010-07-17 00:00:00
---

# CONSTRUCTING A REALITY


Following on Neil Spiller's reference to radical Constructivism, I thought it important to re-post ([ZEROES AND ONES](https://lebbeuswoods.wordpress.com/2008/11/09/zeroes-and-ones/), November, 2008, in a more readable form) one of its seminal texts by Heinz von Foerster, the founder of second-order cybernetics and a leading radical Constructivist.


A personal note. I had the great good fortune to meet Heinz when I was twenty-one years old and a student of architecture at the University of Illinois, where he headed the Biological Computer Laboratory, a major cybernetics think-tank. In the 60s we had a working relationship, as he asked me to make illustrations for several of his scientific papers, which meant that he had to explain to me his ideas, which he did with remarkable simplicity and clarity. I learned a lot in our conversations, and it has echoed down through the years in my work. Heinz always called me an “epistemologist,' a high compliment, coming from him. From time to time, I would send him publications of my projects and he would always comment. When I sent him a copy of Pamphlet Architecture 6, the “[Einstein Tomb](https://lebbeuswoods.wordpress.com/2009/09/27/the-vagrant-light-of-stars/)” (I was anxious about my understanding of the physics involved), he generously replied that it reflected my interpretation, clearly expressed. He also mentioned that he had shown the little book to a colleague who asked “What are all those wires?,” to which Heinz replied, “Exactly!”


LW


The essay “On Constructing a Reality”:


[![](media/hvf201.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf201.jpg)


[![](media/hvf211-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf211.jpg)


[![](media/hvf221-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf221.jpg)


[![](media/hvf23a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf23a.jpg)


[![](media/hvf23b-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf23b.jpg)


[![](media/hvf24-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf24.jpg)


[![](media/hvf25-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf25.jpg)


[![](media/hvf26.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/hvf26.jpg)


About Heinz von Foerster:


<http://en.wikipedia.org/wiki/Heinz_von_Foerster>


The text written for the November 2008 post on the LW blog:


There is much talk today about computation in architecture, not only its implications for the design and production of tectonic objects—from chairs to buildings and cities—but also its inescapable philosophical consequences. Understandably, most of this talk, by a few theorists and many practitioners, centers on the digital computer and its capacity for rendering complexity and simulating reality. I say ‘understandably,' not because this is a proper focus for issues raised by computation, but because very powerful digital computers have become handy and accessible tools for everyone, so…why not use them?  Actually it is not the proper focus, especially if our interest is in philosophical domains such as aesthetics and ethics, and how architecture both embodies and enables them. An even more powerful and accessible computing tool—the human brain—should be our primary subject, and object, of understanding the nature and consequences of computation, for architecture and the world.


There are precise, historical reasons for this. It was advances in neuroscience, during the 30s and 40s of the last century, in understanding how the brain works as an electrical machine, a “biological computer,” that led to the rapid development of artificial, electronic computers. This advance—a leap, really—was prompted by dramatic discoveries, in the teens and 20s, in physics and mathematics, especially the invention of quantum mechanics and quantum theory. Niels Bohr's Copenhagen Interpretation had radical and profound consequences not only for epistemology, but also for every branch of inquiry and practice. It states that when we describe with scientific precision any phenomenon, we must include in the description the manner in which we observed the phenomenon. The human brain and wider nature were thenceforth intertwined and inseparable; the old barriers between the ‘subjective' and the ‘objective' were shattered and a new era, the present one, began.


Architectural theorists and experimental practitioners would do well to give more attention to cognition theory, its origins and contemporary forms, when considering concepts of computation. One of the key concepts to come out of cognition theory is ‘self-referentiality,' having to do with the paradoxes created by the brain studying itself. Concepts such as ‘recursion' and ‘feedback,' ‘self-organization' and ‘autopoesis' are secondary consequences. Technological application, such as software design, communications networks, and their relevance for architecture—comes further down the line.


Indeed, anyone who wishes to understand the role of computation in human thought and activity must study the developments in this recent history, particularly in the field of cybernetics, which in the 50s and 60s, laid the theoretical foundations for contemporary cognition theory, general systems and information theories, which, in turn, underlay the rapid advances of computer technology. I close this post with a succinct essay by Heinz von Foerster, one of the founders of the transdisciplinary field of cybernetics. Hopefully, it whets your appetites for more.


LW



 ## Comments 
1. [EIGHT DIAGRAMS OF THE FUTURE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/08/eight-diagrams-of-the-future/)
8.8.10 / 2pm


[…] We can know the future to exactly the same extent that we can know the past or imagine the present—they are not interchangeable but do share the quality—owing to the structure of the human brain—of having been invented or, if you prefer, constructed. […]
3. [Subject to Vigorous Change – The Heuristics Laboratory](http://theheuristicslaboratory.org.uk/blog/subject-to-vigorous-change/)
8.19.10 / 2pm


[…] been puzzling over a post by Lebbeus Woods about Heinz von Foerster called Constructing A Reality for the past couple of weeks then this morning I checked the ol' facebook to see that friend […]
5. [INVENTING DISCOVERY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/02/01/inventing-discovery/)
2.1.11 / 6pm


[…] system makes perceiving any reality independent of it impossible. When we look out at the world, what we perceive are our perceptions, nothing more nor less. Hence, Isaac Newton did not discover gravity, a set of phenomena affecting […]
