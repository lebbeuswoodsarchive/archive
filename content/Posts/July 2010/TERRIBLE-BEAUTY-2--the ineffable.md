
---
title: TERRIBLE BEAUTY 2  the ineffable
date: 2010-07-24 00:00:00
---

# TERRIBLE BEAUTY 2: the ineffable


When was the last time you heard the word ‘ineffable' in a discussion about architecture? Never? Well, I'm not surprised. Ineffable means ‘unspeakable'—that which cannot be said—so I can understand why people do not speak of it. And yet, the ineffable is an important concept and even more so a momentous and profoundly disturbing experience when we encounter it, which most of us will, at one time or another, in the unfolding of our lives.


The ineffable is sometimes called ‘the beauty beyond expression,' having to do with the apprehension of the divine, or with some essence of existence hidden from us in normal situations. The ineffable is revealed only when the curtain of normalcy around us is pulled away and we are confronted with a very different world than we imagined we inhabit. This is often a frightening experience, even terrifying because we're not sure what to do next, or what to think. A car accident, a tornado, the loss of someone we love and need—traumatic experiences that shake us out of our accustomed, taken-for-granted reality and we are left to struggle for understanding. Only thrill-seekers who enjoy the adrenalin-rush of fear seek out such experiences. The rest of us try to keep things as they are, paying the price of boredom, if necessary, to keep ourselves in the comfort range of the familiar. The ineffable is well out of our comfort range.


For this reason, the ineffable is not a topic, let alone a goal, of architectural design. We can say that in fact design is the enemy of and a defense against the ineffable. As soon as we design, we start to control, to set up the defining boundaries and limits and we squeeze out the ineffable, which is something that emerges when systems fail, when the limits are transgressed, and when things fall apart. We like to set up things so we feel we are in control. Our environment is designed to reassure us that everything is OK. That is what politicians do, telling us “Everything is OK, don't worry about Iraq, it's going to be OK—don't worry about pollution, we are going to take care of it.” Architects are a big part of this game of reassurance. We design endless variations of the normal and the familiar, sometimes dressing it up to look different, but inside—when we inhabit it—we find that we can behave and think normally. Our perception of the world is not affected or changed.


I grow weary when I hear the optimistic talk of architects proclaiming, like salespersons, that architecture will make living easier, more pleasurable, safer, more secure. Our habits—the optimistic talk being one of them—only serve to reassure us that everything is OK, even if it is not. We don't want to feel uncomfortable; we don't want to have to move in a way that we are not habitually used to moving. But it is only when we are shaken out of our habits that we are able to change and to grow. What if to make things better, to enable people to cope creatively with the traumas of change, we have to make things more difficult, more risky, less secure? How often have architects dared to do that?


[![](media/ineff-sara3b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ineff-sara3b1.jpg)


A strong sense of the ineffable is seen in the photograph of a group of people obviously in distress. The photograph itself is not a self-conscious artwork, concerned with the limits of photography and the like, but a work of journalism, showing us a piece of a particular event.


What they are looking at is a moment of the destruction of the city where they live, Sarajevo in Bosnia in 1992. They are looking at the places where they have lived that have just been destroyed, by artillery and mortar fire. They are looking at their friends and neighbors shot dead by snipers, lying in the streets. Their sense of reality, their sense of the normality of life has been shattered. Intentional violence has destroyed the familiar for them, what they relied on, how they identified themselves, who they were, what they did every day. Theirs are the faces of the ineffable.


[![](media/ineff-sara2b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ineff-sara2b1.jpg)


A wounded woman is being rescued in a street under attack. There is an urgent sense of panic, of terror. In such moments, the ineffable fully breaks out and it is unspeakable. The photograph only makes us aware of its existence, being second-hand. You had to be there as Paul Lowe, a very courageous photographer, whom I met there in '93, was—to know the ineffable's full dimensions. “Is this the end? What is life worth, if everything that matters is destroyed?” Blind instinct takes over, and we are far beyond the realm of the habitual and any forms of comfort.


[![](media/neworleans2a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/neworleans2a1.jpg)


War is an extreme of destructive violence, but so are the ‘natural' disasters. In New Orleans, the violence of Katrina's extreme wind and flood destroyed people's worlds as effectively as war. Normal rooms are absurdly rearranged, becoming parodies of the everyday. Compared with a wholesale destruction of buildings the damage seems small, but the fabric of the everyday is more subtle and fragile than we think. The sofa is still there, but no one can any longer sit or lie on it. The ‘sanctity of the home' has been violated, and it matters little that it was by accident and not by intention. Some terrible event has occurred and the ineffable has broken through into reality, leaving us with the dread that our existence is really very tenuous and not at all assured.


[![](media/ineff-taiw2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ineff-taiw2a.jpg)


Painful ironies abound. The new buildings tipped-over by an earthquake in Taiwan fell because architects and engineers left the ground floors as open as possible for shopping malls, weakening them in disregard for the threat from powerful lateral earth forces active in a seismic zone. Who is to blame here—nature, or the architects and engineers? From the viewpoint of the inhabitants caught in this catastrophe, it hardly matters. The ineffable cannot be designed, but design can unintentionally invite it in.


[![](media/ineff-ww2b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/ineff-ww2b1.jpg)


The list goes on. The ‘urban clearance' of German and Japanese cities designed during World War II by British and American war planners unleashed hell on earth, and also an entire world of ineffability where ‘the shock of the new' was at once a sound of doom and the prelude to the construction of a post-war world. People affected simply had to ‘adjust' and ‘adapt.' Is it necessary to bomb cities flat in order to build them anew? Obviously not. But some form of destruction of the old is necessary, and that produces for many the trauma of change. Is, then, the ineffable also the inevitable?


Before answering, I'll extend my list of sources of the ineffable by one more: the destruction of the World Trade Center on September 11, 2001.


[![](media/lowman2a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/lowman2a1.jpg)


Until then, America had prided itself on its major cities never having been violently attacked by a foreign enemy. We had never suffered the sort of destruction experienced by Europeans and Asians in World War II, save for some home-grown exceptions like the Civil War (called by historians the first modern war, in part because cities became targets) and the bombing of the Federal Building in Oklahoma City. But now, major American cities were attacked, and our whole idea of reality was twisted. The destruction was limited, but the fact that it was caused by foreigners, using American airliners as weapons, qualified the attack as a national calamity. The sense of loss was overwhelming. Loss of so many lives, loss of physical symbols of American power, loss of the sense of invulnerability, loss of innocence, however misplaced it had been, loss of America's privileged place in the world. A profound sense of loss is the main effect of our experience of the ineffable.


Coming back to the faces photographed in Sarajevo, we might be able to see that they don't belong to some people somewhere else. They are our faces. These faces portray unspeakable loss, as do the photos of the collapsing towers, and their ruins.


[![](media/lowman5a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/lowman5a1.jpg)


But there is also something else.


Loss is inevitable in the story of each person. Losing your wallet, losing your job, losing your home, your family, your city—the degree of loss escalates from the inconvenient to the inconceivable, and with it the experience of the ineffable. Loss, however, is necessary in order for us to change, not only in our habits, but also in our understandings and beliefs. As long as we cling comfortably to what we are and know, we cannot learn, or create. If design is to be a creative act, it must take on the most difficult situations in our lives. It must offer more than comfort and reassurance. It must confront the unspeakable—the ineffable—and become a means by which we can transcend it. This means that we—as individuals and as architects—must, as the Existentialist poet Nikos Kazantzakis once put it, “build the affirmative structure of our lives over an abyss of nothingness.” A heroic—probably too heroic—task, it is true. Except for those who have no choice.


LW



 ## Comments 
1. [nick](http://www.nickaxel.net)
7.24.10 / 4am


I've been using the word ineffable a lot, according to my previous understanding it meant simply “that which cannot be explained”, dealing with connotations akin to mysticism and spiritualism.  

In your musings it has a much more serious, significant meaning: it is the experience of the Real. Expanding upon this idea through your writings and those of Zizek, it is “being-lost”. Ironically, the objet petit a is our context that we can only ‘see' by an event of political deterritorialization; we can only see it because it is not what our ‘reality' is anymore.  

Seeing is then inherently a product of a negative operation – or inversely, seeing is only possible if the thing one sees has no name anymore, so that we can attach a subjective meaning to it. Seeing then has nothing to do with sight, but with affect and emotion; where seeing becomes perception.
3. [Kenneth Howe Jones](http://howcitizenshipworks.org)
7.24.10 / 6pm


Brilliant. But you are always at your best when speaking of art and war. What you say is all the more relevant as we attempt to cope with “911” … and the very idea that we are capable of doing such a thing to ourselves.
5. Jared Karr
7.25.10 / 12am


I have a small ontological theory that ineffability is the only reliable indicator of reality. Anything that can be reduced to simple statements–e.g. mathematics, religious creeds, this pet theory of mine–lacks Being. Appropriately, this idea surfaced during a time of intense and *ineffable* grief.
7. [Josh](http://www.untitleddesigner.tumblr.com)
7.25.10 / 4am


LW  

Thank you for this post. As you know I served in the US Army for nearly 8 years with a tour in Iraq and have now begun my journey as a graduate architect. 


During my time in service, notably in Iraq, I found myself both oddly horrified and comforted by those experiences I care to label only as “ineffable.”


Those experiences have shaped a new perspective on life and what it means to be human…. to me.


Some experiences, of which I only hope to one day forget, have grounded me. I find myself connected through a deep bond with emotions of grief, hate, and love that I can only be thankful for because I feel more human than I ever had before.


Only when I was faced with the most ineffable moments did I realize that from this moment on the easy road was never going to be an option for me, ever again.


My reality… I thought I knew what my reality was. War, quickly became my reality. To a certain extent, it still is. War for me is no longer a physical moment in time, but all of time itself. It is something I cannot escape. 


I suppose the moments in our lives that define who we are, whether joyous, courageous, or dreadful, should not be calibrated to fit within our own previous vision of context, but should begin to create a new context. This expansion of our knowledge and consciousness is why we must experience life. 


I only hope to expand upon what I have experienced in life and use it as a tool in education and experimentation. It's hard for people to understand the idea of war let alone live with its repercussions. I find it appealing that you seem to understand it and some of its complexities and I admire you for that. Thank you LW
9. AL
7.26.10 / 8pm


Brilliant exposé!! Concise, deep and reaches far beyond simply matters of architecture but life for which architecture and its genesis is merely a part. Thanks!!
11. mr Black
7.27.10 / 1pm


Great article ..interresting theory about the new era of design and architecture… ineffable is a necessary step to evolve ourselves….I never saw those pictures of the earthquake…they are unbelievable…  

thanks a lot
13. [Ematheus](http://esteban.matheus@gmail.com)
7.29.10 / 12am


This was exactly what I needed to read at this moment. Thank you.
15. Gio
7.29.10 / 3am


LW


In your TERRIBLE BEAUTY 2: the ineffable


I like it.


Ineffable beauty in Architecture…..could be something like “viva la revolucion”….  

yes.. it starts with the uprooting of deep foundations, those that make the fabric of our culture,  

like :” everything is going to be OK” and usually happen with great natural catastrophes and wars.


Now,  

Ineffable is that which can not be described in words or inexpressible (Webster Dict.)


The ineffable beauty in Architecture is that pure act of creation that only exists where no words can express it,  

lives between the pencil and the paper.
17. Immanuel Kant
7.29.10 / 1pm


Your description of “ineffable” is correlated to the idea of the “sublime” – one's encounter with personal finiteness and mortality in the face of infinity.


Some research I've come across:  

[Click to access umi-umd-5500.pdf](http://www.lib.umd.edu/drum/bitstream/1903/8267/1/umi-umd-5500.pdf)
19. [TERRIBLE BEAUTY 2: the ineffable « Architects Directory](http://www.architects-directory.info/?p=501)
7.29.10 / 3pm


[…] When was the last time you heard the word �ineffable� in a discussion about architecture? Never? Well, I�m not surprised. Ineffable means �unspeakable��that which cannot be said�so I can understand why people do not speak of it. And yet, the ineffable is an important concept and even more so a momentous and profoundly disturbing experience when we encounter it, which most of us will, at one time or another, in the unfolding of our lives. – Lebbeus Woods […]
21. [THE EXPERIMENTAL « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/12/the-experimental/)
8.12.10 / 3pm


[…] 07.24 TERRIBLE BEAUTY 2: the ineffable […]
23. [dessalles](http://dessalles.com/2010/08/13/lebbeus-wood-the-experimental/)
8.13.10 / 4pm


[…] to solve the grittier real-world problems. The experience of beauty–especially difficult or ‘terrible' beauty—is one that gives us a sense of personal connection to a wider world. No doubt this sense […]





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.13.10 / 7pm
	
	
	Chris Teeter: I hope the inventor (or his heirs) reap some financial rewards, though a patent usually holds on seventeen years and can't be renewed.  
	
	I'm thinking about the word, experimental. It's a stretch but it seems to combine experience and mental. Nice combo, no?  
	
	As for the Mayne drawing, I find it extremely beautiful/ugly—my favorite kind!  
	
	Glad for your continued input….
25. [Lebbeus Woods on the Ineffable | Any-Space-Whatever](http://www.anyspacewhatever.com/2010/07/lebbeus-woods-on-the-ineffable/)
9.10.10 / 3am


[…] a recent blog post, “Terrible Beauty 2: The Ineffable,” Lebbeus Woods laments the unwillingness of architects to design the ineffable, as well as […]
27. harman
9.29.10 / 8pm


Agree with everything said about Ineffable, and though one agrees with the fact that architecture can and does for some of us at least, have a profound effect on our senses, asking for architecture to deliberately create the sense of ‘Ineffable' is asking for too much. Also Ineffable happens not when we want it to and definitely not when we are ready for it. Ineffable beauty can be found in spaces at certain times of the day or even always, but can we create an Ineffable sense of despair that would change us for the better i think not. We see a lot of things to despair about in architecture and our urban landscape but its not Ineffable its very very real.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.29.10 / 9pm
	
	
	harman: The crucial question is, once we—as architects—are confronted with the ‘uninvited' ineffable, how does it impact what we do? My position is that we architects need to integrate this least wanted of experiences in the situations impacted by them, if we want our work to have a fully human meaning and purpose. Most architects would disagree with this position and prefer to turn their gaze and efforts elsewhere, to more cheerful—and easier—tasks.
	
	
	
	
	
		1. harman
		9.30.10 / 10am
		
		
		It is impossible to disagree with that premise, what you are saying here is true not for architecture but or everything we create yes architecture is finite and permanent, but would hold the same true for writing, poetry,movies, music, art, parenting… i understand that you are asking for something that is sublime and yes, should architects be more difficult with themselves… yes one expects more out of them because of the kind of education we (mere mortals! no sarcasm intended) know they must have gone through. I would use a very simple word here can they just start by being ‘honest' about who they are and where they come from. Living in a country like India where everything works and ‘ineffable' is often just a turn of the head away, and you see trash being passed off for architecture ‘honesty' would be and is a comfort one is looking for.
29. [UNREAL RUINS « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/05/unreal-ruins/)
4.5.11 / 11pm


[…] on this blog, I have written about “aestheticizng violence” and “terrible beauty,” discussing artists'—and occasionally an architect's—risky depictions of […]
31. [JOHN SZOT: Interview with LW, ‘the ineffable' « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/02/26/john-szot-interview-with-lw-the-ineffable/)
2.26.12 / 4pm


[…] for radical intervention by identifying the ineffable as a pre-existing  condition. However, in ‘Terrible Beauty 2' you seem to advocate pursuing the ineffable as a legitimate, independent architectural enterprise. […]
