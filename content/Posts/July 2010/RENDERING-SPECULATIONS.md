
---
title: RENDERING SPECULATIONS
date: 2010-07-15 00:00:00
---

# RENDERING SPECULATIONS


RENDERING SPECULATIONS, A Symposium at the Architectural Association in London, May 7, 2010, article written by Jack Self appearing in the June 2010 issue of the Architectural Review


[![](media/rendspec-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/rendspec-3.jpg)


(above) from The Peak, Hong Kong series (1983) by Zaha Hadid.


“The idea of the architect determining an end result, a final object, is changing,' began Lebbeus Woods, broadcasting to the Architectural Association's packed lecture hall by webcam. “The architect as a controlling figure is a tyranny that is over.” A woman in the front row shifted uncomfortably.” Sorry, Zaha,” he tactfully added, dragging on a cigarette in the safety of his New York apartment.


It soon became evident that the symposium's title. “Rendering Speculations,” was going to cause disagreement among the six guest speakers, who ranged from heavyweights such as Woods and the patron saint of parametricism Zaha Hadid to younger, lesser known figures in the field. As pointed out by hosts Ricardo de Ostos and Tobias Klein—both tutors at the school—there is no consensus on what constitutes a ‘render.' It is neither a photograph nor a drawing; neither a construction document nor a faithful depiction of reality—its domain lies somewhere in the space between abstraction and resemblance.


The Bartlett's Marjan Colletti elaborated: “Is it always trying to describe something, or can it be a medium of expression in its own right?” Zlah Fogel (an animator who has worked on films from *Wall-E* to *Angels and Demons*) described the renderer as a creative technician, occupied with the mechanical reproduction of a fantastic reality. Whereas American artist Andrew Jones views the renderer as a cultural stenographer: capturing a moment's essence, not its photographic exactitude. His portable technique of ‘live digital painting'—which involves an enormous Wacom tablet connected to several displays—projects festival crowds back to themselves, splashed with the colors of real-time sunsets.


The two senior participants occupied the final slots. Hadid is reputedly terribly shy and, from Freud's theories of negation, one wonders whether this manifests in her as a volatile temperament. However, after voraciously cursing the AA's lectern the star settled into an explanation of her earliest paintings and drawings: pre-digital renders. Overcome by enthusiasm, she expressed herself eloquently and frankly. It was a beautiful moment.


Before the 70s, there existed only the plan and the section. What passed for a render of this era was nothing more than an isometric projection of one or both these views. Her own desire to show “more than was visible” resulted in the invention or employment of advanced drawing techniques: the x-ray, the exploded axonometric, and the display of multiple, at times impossible, views simultaneously. Computer technology rose to express thyese ideas, and building techniques rose to make these drawings constructible. Here the render is not about graphic presentation, but a storyline.


“There are images intended to create effects,” commented Zaha enigmatically, “and images intended to communicate political ideas.”


Woods, last to speak, ran with the baton. “I don't care very much about building buildings. I care about building ideas.” Since most ideas will never be built, Woods sees it as his role to build on paper, through graphic speculation. “These drawings are not preparations for construction—in most cases they are the project. The act of rendering is the making of a version of reality,” he concluded. His final message was simple, but potent: if architects wish to avoid obsolescence, they must reverse the de-politicization of architecture by the dominance of the beautiful, but meaningless, render. As architects, our aspirations for reality must begin in our drawings.


(below) from The UTOPX series (2008) by Lebbeus Woods:


[![](media/rendspec-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/07/rendspec-4.jpg)


The article on the Architectural Review website:


<http://www.architectural-review.com/marginalia/rendering-speculations-london-uk/8600510.article>



 ## Comments 
1. Rain Slowe
7.17.10 / 9pm


“I don't care very much about building buildings. I care about building ideas.”


Well said, Mr. Woods. I'll drink to that.
3. [scyg](http://cygielski.com/blog)
7.28.10 / 1am


“Before the 70s, there existed only the plan and the section. What passed for a render of this era was nothing more than an isometric projection of one or both these views.”  

What about the elevation, and something as basic as multi-point perspective? Or did Zaha Hadid invent these as well?
5. James Bezek
8.5.10 / 5am


Render typically (mis)represents either a hyper-realized vision that is not readily attainable or a parlor trick meant to fool the eye into seeing something not there. Mr. Woods description is more analogous to cooking where one renders fat, in essence pulls forward/extracts substance that can be used for a specific or other purpose. Too often the render is the end, the zenith whose reality can never match the representation.
