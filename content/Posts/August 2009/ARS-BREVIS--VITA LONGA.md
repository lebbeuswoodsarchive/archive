
---
title: ARS BREVIS, VITA LONGA
date: 2009-08-12 00:00:00
---

# ARS BREVIS, VITA LONGA


![LWblog-KNOW-2](media/LWblog-KNOW-2.jpg)


The truth is, knowledge does not endure, but must be recreated anew by each person, by each generation.


Knowledge is created by and dies with the knower. Only data, or evidence that knowledge has existed survives death, in all the books and buildings and artifacts that people have created as a demonstration of what they knew or came to know through the hard-won process of living and working. These things are not knowledge itself, though some make the mistake of thinking so. Rather, they are the applications of knowledge, its effects.


This thought is easily tested when we try to directly re-apply existing works to different or even, ostensibly, the same situations they address. So, for example, if we believe that Shakespeare's knowledge of the human condition and behavior are actually embodied in his plays, it should be possible to use, say, Hamlet, to write a new play that is even better, considering that we are adding our knowledge to his. Alas, when we try to use Hamlet as a model for a new play, we find that we are only able to recast what Shakespeare has done (as countless knock-offs in film and theater show), and thus create a lesser form of the same. To write a new Hamlet, we will have to draw on our own experiences and imaginations, think through the relationships of the characters and their situations, just as Shakespeare did—creating knowledge—then apply what we come to know to the construction of a play, which most likely will not resemble Hamlet at all. The way that we will have used Hamlet is as an inspiration for our effort—‘if he could do it so can I'— and also, perhaps, as a heuristic aid in understanding the idea, if not particular techniques, of construction. In the creation of knowledge, Hamlet is important, but not absolutely necessary.


The point is, we must re-invent Shakespeare's knowledge—it is not available to us, even from his extant works. The evidence that these works give that it once existed in a lone human mind provides us with a light in the immense, dark terrain of existence.


The same is true of science, and of technology.


The breakthrough papers of Albert Einstein, which with their verbal and mathematical arguments opened up the modern era in physics, and those of Werner Heisenberg and then Richard Feynman, who advanced the crucial theories, can serve only as guides for the continual reinvention of knowledge. Language and mathematics have changed. Their works are no longer the most relevant, because the knowledge they had, which would no doubt be useful to solving the leading questions of today, is not contained in their extant works, but vanished with them.


So, too, in the age of computerization and satellites, the technology once used to launch spaceships to the Moon, and to land on it and return to Earth, has been superseded and is, for us, useless. If we want to go to the Moon again, we will have to invent all over again the technical means—and the moral reasons—to do it. The old ways and ambitions, just like the old technologies, remain inspirations, but they do not embody the knowledge we need. We of the present must invent that for ourselves. Yet, we must accept in advance that our knowledge will always be greater than can be contained by any construction we make with it. And that it will die with us. Contrary to received wisdom, it is Art that is short, and Life that is long.


![LWblog-KNOW-1](media/LWblog-KNOW-1.jpg)


LW



 ## Comments 
1. aitraaz
8.13.09 / 10pm


Better Bergson than Einstein, but one must choose between the actual and virtual, science and philosophy. Art (creation) is infinite, and so it is with life (actualization). Technology will demand of us what it will, the drawings speak for themselves…
