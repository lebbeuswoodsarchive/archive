
---
title: A. TEHRANI  true freedom
date: 2009-08-07 00:00:00
---

# A. TEHRANI: true freedom


1- Your drawings have always been the most inspiring to me and years ago were my main motivation to try and look further, into what architects really can (and I believe should) do to fulfill their political responsibilities (e.g. to help reclaim the public space).


2- We have been subject to years of patriarchal schooling, both directly at school, or indirectly in the society, but one is not easily schooled and will resist. So ironically what I learned from the patriarchs in architecture school was to resist authority, architecturally too. But looking back, I won't seek victory anymore in defeating them into accepting some radical form or offensive strategy over the shallow Islamic/Iranian Po-Mo they favored or the conservative brand of Modern architecture they tolerated, but in getting involved in a conflict through the medium of provocative designs and drawings. To challenge the authority of the patriarch while others are watching. To let the others see, that his power is being challenged and that he has to fight to keep his status—that he is not invincible—and to slowly mobilize the oppressed creativity of the fellow students to stand up and resist suffocating authority wherever they spot it.


But apparently there is no going back…


3- I worked on quite a few commercial projects in my city, Tehran and other cities in Iran at Bahram Shirdel's and other offices. The point was always to resist the capitalist tendencies by designing spaces that allow new experiences or at worse cases, providing more “open space”—mistaken for “public space”—to materialize the abstract idea of a “free space”. The sad truth was, that even if you succeed in selling the client “the emperor's new clothes”, the spaces remain no more public or free than air bubbles trapped inside a concrete block. What won't allow them to be really “public” is that the “populus” has no rights to these spaces. Not only that they can never take part in the destiny of these spaces in person, but they are not even allowed to intervene through formerly “public” institutions such as architecture, for the market dictates everything from the style and mannerism of the design, down to every detail, in a complicated and multi-layered plan. The case becomes even worse in so called public (or cultural or whatever) projects for it's not the market you're dealing with, but an extremely corrupt bureaucracy, a reactionary political ideology and its own cultural hegemony.


The alliance between the architect and institutions of high authority, which was supposed to nurture some sort of “welfare architecture”, seems to have gone wrong and he has now become only a tool to strengthen the authority. The architect, sub-consciously aware of this tragedy gives up all his responsibilities and dives head first into his new role in a suicidal act to rid himself of his identity disorder.


4- What is going on in Iran is in the first place the result of a ubiquitous, truly horizontal grassroots movement of people from all social classes and generations reawakened after years to collaborate on ideas and strategies for an election campaign against Ahmadinejad. Designing posters and slogans, making campaign videos and music and most important of all having fiery debates on-line one day and then seeing and hearing them the next day on the streets and so on. What brought the people to the streets on June 15th was not cell phones (in fact cell phone services including text messaging had been cut off since the day before the coup d'etat) but this vast rewiring of social connections in the weeks before. The fact that this rewiring was possible only through the Internet and cell phones and the significance of these political machines cannot be denied, but there is a greater truth that looms above this.


Today in Tehran the people have indeed reclaimed the public space. Even though you won't see the sublime images of millions of people in one place which you saw in the first week because the protests have become dispersed—due to the brutal crackdown—they still own the public space of the city, for they still don't fear the authority anymore and don't feel that they need permissions to express themselves freely on the streets. The physical qualities of the spaces play no role in this game. Whether it is a square under a gigantic monument, a historical plaza, the labyrinthine bazaar or the whole of the city including its highways and dead-end alleys, today it is a home, an arena, a fortress and a shrine to the free people of the city. In fact the public space reclaimed, is far deeper than just the physical, it is the mental space of the city.


5- This does not negate your concession that “… architecture—the design of physical space—still has a role to play in human affairs…” in fact it teaches us a great lesson about that role, the same lesson it taught the people, politicians and activists in Iran and all over the world. It should be a reawakening for architects, to reconsider the mechanics of architecture itself as the tool for exercise of power it has become once again. The architecture of resistance to established authority first requires a redesigning of the role of the architect in relation to the people and to the sources of power and authority. Just like the architects did in the last century, before it was announced that it was the end of history and that the role of the architect in this eternal heaven will be to either please the megalomaniac tyrants, warlords, oil gods and their businesses, or to make shelters out of garbage for the children in Africa.


It is on us to rethink the way physical space is designed and built but this time not in the spirit of the gods and heroes of the 20th century but that of the 21st century man. It is only then that free architects and free people can create true free spaces and it is the job of the architects to do that before its too late.


P.S: In that light I see your drawings (specially those of the Freespace structures) not as inspirations and guides for actual structures but as inspirations and guides on how to challenge the cultural hegemony dominating architecture in order to initiate change in the role of the profession.


**A. Tehrani**



 ## Comments 
1. Mehran
10.22.09 / 6am


I have taught and worked in iran as an architect and i fully understand your statements,i had always riverbated(unlike my classmates) with the prejudgments of my tutors about architecture,after a while i understood that i was alone in this path,i began to learn to critisize myself and self study…your essay made me feel that this problem is felt by others too.I am currently starting my masters course in architecture at the staedelschule,Frankfurt, i hope that some day i will be able to introduce the correct educational environment for architecture in iran.
