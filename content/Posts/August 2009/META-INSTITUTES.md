
---
title: META-INSTITUTES
date: 2009-08-01 00:00:00
---

# META-INSTITUTES


We might imagine that the French Revolution failed because it installed itself in the buildings of the royalty it overthrew. While proclaiming a new social order, replete with new system of measure (the rational Metric system), a new metric calendar (ten months to a year), a radical declaration of universal human rights new to Europe, and other innovative ideas, the Revolution chose as its architectural setting the royal palaces of the *ancien regime*. While promising a new world order, it delivered an old world order of space and symbol. It was, perhaps, an understandable mistake—adopting the trappings of familiar power to borrow its authority—but it was fatal. Symbolically, the Revolution offered itself as merely a substitute for the old order, just another form of elite, begging to be rebelled against and overthrown. What, we might ask, would have happened if the Revolution had immediately proposed a new order of civic space, one reflecting its rational ideals—new town plans, a new plan for Paris (think of Le Corbusier's *Ville Radieuse*), and new types of public buildings, including those for the governing elite of the *nouvelle regime* itself? We will never know.


 We might also imagine that the Russian Revolution learned a lesson from history and inaugurated itself with the most radical designs imaginable for buildings and towns, at least in its earliest phase. Constructivist design, which included a revolution in print graphics, clothing, theater, film and music, as well as architecture and urban planning, was based on the premise that changing the social order meant changing the aesthetic order, indeed that the ethical and the aesthetic were not only synonymous but synchronous—you cannot invoke one without invoking the other. Perhaps, we might speculate, the new Socialist orderfailed because this understanding was abandoned after a decade or so, when the new order assumed the borrowed authority of the old royal, Tsarist trappings in the form of a Stalinist dictatorship that housed itself in the imperial Kremlin and commissioned neo-classical ‘skyscrapers.' What would have happened, we cannot help but ask, if the social revolution had been realized in the visionary Constructivist designs? In other words, what would have happened if the political revolution had proved to be more than words? Might we now be living in a brave, new Socialist world? Perhaps not, but we will never know, for sure.


 It is difficult to imagine carrying on government-as-usual in Vladimir Tatlin's spiraling ‘monument to the Third International,' with its suspended, rotating chambers, which was intended to house a radically new Socialist form of government. At the same time, it is easy enough to picture a labyrinthine bureaucratic regime carrying on quite as bureaucracies always have in the Kremlin, or in Albert Speer's pseudo-Greek buildings designed for the Nazi Reich. Phony revolutions are a perfect fit for phony architecture, or for architecture that inauthentically borrows its authority from a pre-revolutionary past. It is fortunate that the United States is not undergoing a political or social revolution, given its pseudo-Roman Capitol and neo-classical White House, though Barack Obama did campaign on a promise of “change” that culminated on a stage festooned with phony classical columns.


 Tatlin's Tower was actually a new building type, one with little history and even less foreseeable future, which I have called a “meta-institute.' Simply put, it is a building designed to house an institution for the study of the very idea of institution. When a new form of government must be created, a study of government must, in itself, be the first order of business. A building that breaks out of the familiar models or types can become an experimental tool in the process, at the very least an example of new thinking and a stimulus to innovation. The very spaces where discussions and debates about the new form of government are offered and carried on should be as much a challenge to old ways of thinking as new laws and principles, thereby linking the intellectual to physical reality. In effect, a meta-institute should present a spatial problem to be solved that reflects the ideological one: solving one aids in solving the other.


Kazimir Malevich (center) and others at the First All-Russian Conference of Teachers and Students of Art, Moscow, 1920:


![LWblog-METAinst2](media/LWblog-METAinst2.jpg)


**PETROGRAD, Russia.** Vladimir Tatlin, Monument to the Third International, 1917:


![Tatlin3a](media/Tatlin3a.jpg)


![LWblog-TATLIN1](media/LWblog-TATLIN1.jpg)


Bastardized version in a Moscow street demonstration, 1927:


![LWblog-METAinst1](media/LWblog-METAinst1.jpg)


Tatlin's “tower” as seen in the skyline of Petrograd:


![LWblog-TATLIN2](media/LWblog-TATLIN2.jpg)


**SARAJEVO, Bosnia and Herzegovina.** The city under siege, 1993; the damaged Parliament building on the right:


![LWblog-METAinst4](media/LWblog-METAinst4.jpg)


Haris Pasovic, Ivan Straus, LW (all center) and others at the “War and Architecture” exhibition in the damaged Olympic Museum, Sarajevo, 1993:


![LWblog-METAinst3](media/LWblog-METAinst3.jpg)


LW proposal for the reconstruction of the Parliament Building:


![LWblog-METAinst5](media/LWblog-METAinst5.jpg)


![LWblog-METAinst6](media/LWblog-METAinst6.jpg)


**HAVANA, Cuba.** Government organized political rally in the Plaza of the Revolution, late 1980s (photo by Gianfranco Gorgoni):


![GorgoniPlaza2](media/GorgoniPlaza2.jpg)


Mojitos at the La Bodeguita del Medio:


![BogDelMed1](media/BogDelMed1.jpg)


Meeting at the “Architecture Again” conference, 1994, l. to r., LW, Thom Mayne, Carme Pinos, Mario Coyula, Wolf D. Prix, Carl Pruscha, Gianfranco Gorgoni, unidentified, unidentified, Daniela Zyman:


![HavMeet1](media/HavMeet1.jpg)


LW proposal for a Meta-institute:


![HavMeta1a(1)](media/HavMeta1a(1).jpg)")


Meta-institute seen from the Old Plaza:


![EXP-1a](media/EXP-1a.jpg)


LW



 ## Comments 
1. river
8.2.09 / 6am


I've been trying unsuccessfully to find old Gregory Bateson, Ilya Prigogine, and Humberto Maturana books in local bookstores lately. Any suggestions?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.2.09 / 3pm
	
	
	river: I have no books by these authors, just a few articles in various out-of-print collections about cybernetics and/or ‘alternative' science. Any books that might once have existed are surely long out-of-print, in the hands of collectors and libraries. Sounds to me like an online search is the only (very slight) chance. I can certainly understand your connection of these thinkers with the concept of ‘meta-institutes.'
3. MLA
8.3.09 / 1pm


change ≠ future  

change = redirecting the present
5. [MLA](http://www.aa-nn-dd.com)
8.3.09 / 1pm


I do not think that there is anything that is functionally—by its very nature—absolutely liberating. Liberty is a practice. So there may, in fact, always be a certain number of projects is to modify some constraints, to loosen, or even to break them, but none of these projects can, simply by its nature, assure that people will have liberty automatically, that it will be established by the project itself. The liberty of men is never assured by the institutions and laws intended to guarantee them. This is why almost all of these laws and institutions are quite capable of being turned around—not because they are ambiguous, but simply because “liberty” is what must be exercised…Men have dreamed of liberating machines. But there are no machines of freedom, by definition. This is not to say that the exercise of freedom is completely indifferent to spatial  distribution, but it can only function where there is a certain convergence; in the case of divergence or distortion, it immediately becomes the opposite of that which had been intended…Nothing is fundamental…There are only reciprocal relations, and the perpetual gaps between intentions in relation to one another.


Michel Foucault. Space, Power Knowledge
7. [Chris Landau](http://resensitize.blogspot.net)
8.4.09 / 5am


This reminds me of epigenetics. The idea that certain genes drive other genes. Think of your fingers for example. Maybe the code is only written once for finger, but the epigenetic mechanism produces 10. Evolution thrives on these kind of layers of instruction.


I think a meta-Institute is just what is necessary as things grow in complexity. There should be no such thing as the “top”. Sure, some levels will collapse together, and others will emerge from the rubble, but we should not be afraid to create higher entities, conscious of bigger pictures.
9. thais
8.4.09 / 10pm


@river  

I found some of Gregory Bateson writings, Steps to an Ecology of Mind, feel free to download [Gregory Bateson](http://www.megaupload.com/?d=ZD704MF4)
