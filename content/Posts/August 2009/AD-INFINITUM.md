
---
title: AD INFINITUM
date: 2009-08-03 00:00:00
---

# AD INFINITUM


![ADINFINITUM](https://lebbeuswoods.files.wordpress.com/2009/08/adinfinitum.jpg?w=600 "ADINFINITUM")


 


![ADINFINITUM-3](https://lebbeuswoods.files.wordpress.com/2009/08/adinfinitum-3.jpg?w=600 "ADINFINITUM-3")


LW



 ## Comments 
1. [Toralf](http://analog77.deviantart.com/)
8.4.09 / 6am


When i see this Libeskind's Chamberworks come to my mind just because of the formal relation. I don't know which piece was first either the different motivation behind the two works. It would be interesting if there are any influences, discussions, shared sources of ‘inspiration'.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.4.09 / 1pm
	
	
	Toralf: Libeskind's source of inspiration is music and its notation (he was trained as a pianist). Mine is ‘the tower.' His Chamber Works drawings are about ‘writing architecture,' and privilege the horizon. Ad Infinitum is about negating the horizon and overcoming its gravity.
	
	
	
	
	
		1. [Toralf](http://analog77.deviantart.com)
		8.4.09 / 11pm
		
		
		I understand the part of your reply about ‘inspiration from music' and ‘negating the horizon' but I was referring to the vertical drawings in the chamber works which do not necessarily privilege the horizon, e.g. this one <http://www.daniel-libeskind.com/typo3temp/pics/db9eee2605.jpg> (poor resolution though). 
		
		
		What makes me bring the two pieces in a relation are two things: firstly the overall composition and some similar graphical elements and secondly the appearance of little plan- or section-like architectural events; you even emphasized them with color.
		
		
		I think both drawings challenging gravity in a different way: your tower overcomes it and in DL's architectural/musical (?) notation it does not exist in the first place. I still think both pieces are related on certain levels yet emerging from very different spots in the landscape.
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		8.5.09 / 3am
		
		
		Toralf: Thanks for making me aware of the vertical drawings. I'll look for them. Libeskind's two series—Micromegas and Chamber Works are certainly works of the highest inspiration, the full significance of which has not as yet been appreciated.
3. MrvA
8.4.09 / 8am


Greatest skyscraper ever.
5. river
8.4.09 / 10am


An extra no, shh… yet, Hiya! Lettuce break.To ingest alternative fresh green leafy survival vegetable weeds like broad leaf lawn Plantain, which is a popular grassy tasting invasive edible, yet rumored quite occultedly sooth as the precise parboiled poultice for poison Ivy.. It is a widely common Nitrogen fixer with edible and medicinal value. It is so common, that it may weigh into your pioneering garden.
7. slothglut
8.4.09 / 11am


did you put ‘scrolling down' experience of your blog readers immensely when you created this piece?  

just because it seems like it. ad-infinitum visualization done nicely.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.4.09 / 1pm
	
	
	slothglut: exactly. It's a drawing that could only be seen here.
	
	
	
	
	
		1. sahand
		8.6.09 / 9pm
		
		
		….or better yet, in a glass elevator shaft!
9. [c.smith](http://onedownnow.blogspot.com)
8.4.09 / 5pm


The sentence fragment ‘ad infinitum' has been popping into my mind like a mimetic trope lately, perhaps the last two weeks. Finding this in my RSS was a treat.


You are definitely working the web medium. I often wonder how you would transfer your aesthetic to motion graphics or perhaps film.
11. martin
8.4.09 / 7pm


hah! i really enjoy the pink scribble at the end.


its clear that those early drawings of junk piles still influence you. 


is there a reason you simply flipped the image and colored the bold (section?) parts instead of letting the composition end on its own? I would be interested to see the whole composition.
13. [wmwm](http://www.asasku.blogspot.com)
8.5.09 / 2am


without the mechanism of a webpage, we wouldn't be able to see this work in a continual flow





	1. martin
	8.5.09 / 6pm
	
	
	i disagree. while the scrolling certainly helps direct the viewer, those two mechanisms in your skull can do the same thing in person.
	
	
	granted this scrolling forces the viewer to see only a limited portion at a time, my curiosity is piqued by whatever is to the left and right of the cropped portions, and how the composition actually terminates.
15. river
8.5.09 / 6am


Take good notes on the potent triptych of eclipses in time and space of late.
17. Matthew
9.4.09 / 3pm


Are you sure this isn't your proposal for the High Line? When I see this without any narrative or explanation I see plan, a very long slender path, perhaps it is due to the eventual cropping of the image about a third of the way down. The various in's and out's and open and covered sections of such a path would make for quite an exciting experience. What's to the right and left?
19. [dpr-barcelona](http://www.dpr-barcelona.com)
9.5.09 / 10am


<http://dpr-barcelona.tumblr.com/post/180308564/similarity-02-logarithmic-map-of-the-universe>
