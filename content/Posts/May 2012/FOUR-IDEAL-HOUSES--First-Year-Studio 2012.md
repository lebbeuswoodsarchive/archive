
---
title: FOUR IDEAL HOUSES  First Year Studio 2012
date: 2012-05-14 00:00:00
---

# FOUR IDEAL HOUSES: First Year Studio 2012


[![](media/4houses-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/4houses-1.jpg)


*(above) The Four Ideal Houses, seen in the Third Floor Lobby of the Foundation Building of The Cooper Union, on the day of the final review, May 3, 2012. From left to right: the Cone House of Dawn; the Cube House of Dusk; the Pyramid House of Noon; and the Cylinder House of Midnight. See program description below.*


**ARCH111-S12   ARCHITECTONICS**


**The Irwin S. Chanin School of Architecture of The Cooper Union**


**Studio Faculty:**Professors Lebbeus Woods, Aida Miron, Uri Wegman


**Critics at Final Review:** Pablo Castro (OBRA Architects); Christoph a. Kumpusch (Columbia University); Firat Erdim (IIT); and Nicholas Karytinos; Dean and Professor Anthony Vidler (Cooper Union); Professor David Gersten (Cooper Union).


**Cone Dawn team:**Sam Choi, Maximillian Gideonese, Sang Jung Kim, Xavier Rivas, Alexander Ruiz, Diego Salazar, Kyle Schroeder, Mitchell Schuessler


**Pyramid Noon team:**Art Dushi, Pedro Esteban Galindo, Jenny Hsiao, Kyle Keene, Sehee Lee


**Cube Dusk team:**Alara Dirik, Diego Gonzalez. Isabel Higgins, Hannah Kim, Luke Kreul, William Siesjo, Vanessa Tai


**Cylinder Midnight team:**Yoonah Choi, Cassandra Engstrom, Jemuel Joseph, Soyoun Kim, Yoo Min Lee, Jacob Oredsson, Jonathan Small


.


**Semester PROGRAM**


***FOUR IDEAL HOUSES***


**Project**


This semester we will focus on the design of **four Houses**, which we term ‘ideal' because each occupies a different elemental **volume**—*cube, cylinder, cone,* or *pyramid*—and each embodies a program of habitation based on a different **cardinal time**—*dawn, noon, dusk, and midnight.* Furthermore, the **inhabitants** of each House are assumed to be ‘ideal,' in the sense that they embody, for our purposes, only certain universal human characteristics, such as physical size, capabilities of movement, perception of their environment, and interaction with it. The **site** of each of the four Houses will also be ideal, meaning sloped or flat, horizontal or vertical, and will disregard any inherently idiosyncratic features. In the design of each House, equal emphasis will be placed on the **interior and exterior** of its volume. In taking this approach, we realize that these ideal types exist primarily as ideas, yet find these ideas useful in the laboratory of the design studio as a means of **understanding the fundamental elements of architecture.**


**Historical background**


There is considerable historical precedent for our project. We find ideal architecture—of exactly the sort we are engaging—in examples from Vitruvius, through Alberti and Da Vinci, Ledoux, Semper, Taut and Le Corbusier, Archigram, up to the present in ideal projects by Abraham and Holl. These and other architects have found it important to define their personal principles of design, as well as to set a standard against which to measure their more reality-based work. Ideal architecture has been essential to defining building typologies, which serve the purpose of bringing a level of order to the diversity of urban landscapes. Each student is encouraged to look up and into historical examples, the better to understand the broader context of our work this semester.


**Method**


We will arrive at the designs of the Four Ideal Houses by a series of steps or **stages**, working both individually and in four teams, one for each House. As the design of each House progresses, it will evolve from the ideal forms of its beginnings to the particular forms of its development and conclusion. If we assume, for example, that the House of dawn has the form of a cylinder, we can expect that its ‘dawn-like' ambiguity (neither fully night nor day) will make any changes made to the volume uncertain in their purposes; yet, human inhabitation requires that changes enable specific uses, such as going in and out of the cylinder, and letting in light and air. Consequently, each opening in the volume might be determined, say, by enabling several uses simultaneously. In any event, such a transformation will, in itself, be considered a next higher level of the ideal, in that it embodies a fundamental aspect—a continual evolution in time— of both the human and natural worlds.


**Goals**


**Collaboration and teamwork**


Each of us will approach this project with our own aspirations, our own ideals of architecture. It is crucial that, even when we work in a tightly knit team, we keep our own personal ideals and goals in mind. Teamwork is at its best when individuals who are clear about what they want to achieve collaborate. The key to their successful cooperation is for them to emphasize what they have in common, not their differences [of course, if they have no important ideas in common, they should not be on the same team]. In that way, collaboration is never a compromise of what each believes, but rather a reinforcement of the most important aspects of it by the similar ideas of other team members.


When working on our projects, we should keep in mind that the making of architecture is always a team effort. At the same time, we should recall that successful teamwork bringing together the work of strong individuals, requires **leadership**. It is never a committee effort, with decisions made by voting. Instead, at each stage of the design work and, later, the work of building, a leader must guide the collaborative effort; it may well be that each stage a team's work will have a different leader. This leader usually emerges quite naturally, as he or she is the one who has the best idea about how to accomplish a particular stage of the work. It is rare that all members of the team will not recognize the best idea and agree on a leader for the stage of work it addresses**.** This sounds complicated but it will not be if all members of a team communicate with one another in an open-minded, relaxed and honest way. **Achieving successful design collaborations is one of our goals this semester.**


**Human scale**


We will emphasize in our work this semester the attainment of human scale for our projects. ‘Human scale' is a term used freely by architects, but often with very little understanding of what it means or how it can be achieved in their designs. All too often, they indicate human scale in their drawings by drawing human figures in or next to their buildings, or, in models, little figures and model cars. These are actually very poor techniques because they do not attain human scale in the architecture, but merely indicate it graphically. Human scale in even uninhabited architecture is attained in two basic ways:


1) **the presence of tectonic elements required by human use**—stairs, windows, doors, and other elements that facilitate human use of spaces. Their size in proportion to a building's overall form, and their relationship to each other do not merely indicate the relative size of people, but are necessary for people to inhabit the building and are therefore integral with it. Most of these elements are not arbitrarily sized, but confirm to the dimensional limits of the human body and its capacities.


2) **the presence of tectonic elements used to construct a building**—its walls, ceilings, floors, and other elements defining and articulating spaces. Buildings are not made of a single, solid material (*contra* CNC milling machines), but are constructed of many parts and pieces put together by human beings, and the pieces are sized accordingly. Whether they are assembled directly by hand—bricks or wood panels—or by the hand operation of construction equipment—steel beams, pre-cast concrete slabs—when these parts are visible in the completed building, they immediately establish the relative size of a person (the builder) and, thus, human scale. [Note that even the most seemingly monolithic of materials—reinforced concrete—is poured in parts and thus bears the marks of form panels and construction joints.] **Achieving human scale in our projects is one of our main goals this semester.**


**Ideal Houses**


The conception and design of ideal houses realizes the highest hopes of their designers, giving form and structure to their aspirations for themselves, architecture and through its place in the broader scheme of things, the many people engaged by it. The truth is that ideal architecture in the sense that we speak of it here can be constructed in the real world and with real materials—indeed, it must be constructed. The final drawings and models of the four Houses will—if made with intelligence, passion, and courage—**achieve the reality of ideals. This is our most important goal.**


**[January 19, 2012]**


*(below) A sampling of drawings and large models of the Four Ideal Houses. The most authentic and valued drawings are those made to solve tectonic problems, rather than presentation drawings made for show after the solving of crucial problems of how to shape spaces and join diverse materials together. The large models—five to ten feet high—allow the design and construction of details and interior spaces. All model photos in this post by Professor Uri Wegman.*


.


**The CONE/DAWN House:**


*Longitudinal section model—complete model not shown:*


[![](media/cone-dawn-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cone-dawn-1.jpg)


.


Cross sectional drawing:


[![](media/cone-sks-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cone-sks-3.jpg)


.


[![](media/cone-sks-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cone-sks-4.jpg)


.


[![](media/cone-dawn-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cone-dawn-3.jpg)


.


[![](media/cone-dawn-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cone-dawn-21.jpg)


.


**The PYRAMID/NOON House:**


[![](media/pyr-sks-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-sks-2.jpg)


.


[![](media/pyr-sks-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-sks-4.jpg)


.


[![](media/pyr-noon-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-noon-11.jpg)


.


[![](media/pyr-noon-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-noon-2.jpg)


.


[![](media/pyr-noon-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-noon-3.jpg)


.


[![](media/pyr-noon-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/pyr-noon-4.jpg)


.


**The CUBE/DUSK House:**


[![](media/cube-dusk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cube-dusk-1.jpg)


.


[![](media/cube-dusk-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cube-dusk-4.jpg)


.


[![](media/cube-dusk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cube-dusk-3.jpg)


.


[![](media/cube-dusk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cube-dusk-2.jpg)


.


**The CYLINDER/MIDNIGHT House:**


[![](media/cyl-sks-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cyl-sks-4.jpg)


.


[![](media/cylsks-2inv.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cylsks-2inv.jpg)


.


[![](media/cyl-mid-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cyl-mid-1.jpg)


.


[![](media/cyl-mid-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cyl-mid-3.jpg)


.


[![](media/cyl-mid-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cyl-mid-2.jpg)


.


[![](media/cyl-mid-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/cyl-mid-4.jpg)



 ## Comments 
1. z
5.19.12 / 6pm


cylinder + cone. nice work to all really





	1. Nora
	7.1.12 / 10am
	
	
	I do not agree with you. I was at the exhibition and the pyramid seems the best from all others.
3. [Terrapol](http://www.terrapol.com)
5.21.12 / 7am


Liking these models a lot. Once again, another extremely evocative futurist formal vision. I can see the trace of your hand in them Lebbeus although i cant really see how as ‘ideals' these might progress beyond the exercise you've devised. Not exactly architecture for the masses now is it.


I recently had the pleasure of visiting Hiroshi Hara's skyscraper in Osaka and i see a similiarity between your work and some of the more gothic elements of Bubble architecture. Im thinking of Shin Takamatsu and Hiroshi Hara specifically – theres certainly something of the night, something gothic in the creation of these windowless spaces, the deep materiality, and idiosyncratic detailing which is really refreshing to see. As if the craft of the architect as designer of alternate spaces, alternate visions, hadnt completely vanished. The architects hand is clear to experience in the physical work, and when its done well, can be such a joy to discover.
5. [Donatella](http://gravatar.com/1321c)
5.25.12 / 3pm


Lebbeus, I am in absolute awe by looking at the quality of the work that you are able to obtain from first year students. I very much appreciate the method, that allows for rigorous and yet evocative design. As always, you are of inspiration to us as architect, thinker and educator. Thank you.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.26.12 / 2pm
	
	
	Donatella: As you know, the creative potential is there, even at 18 or 20. All it needs is the challenge and the method to liberate it. And that is the teacher's task. Thank you for your encouragement.
