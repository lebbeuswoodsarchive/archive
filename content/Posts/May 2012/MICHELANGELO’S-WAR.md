
---
title: MICHELANGELO’S WAR
date: 2012-05-22 00:00:00
---

# MICHELANGELO'S WAR


***For those interested, see updates on the [construction of the Light Pavilion](https://lebbeuswoods.wordpress.com/2012/03/25/light-pavilion-under-construction/).***


[![](media/mff-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-2.jpg)


*(above and below) Michelangelo Buonarotti's drawings for the fortifications of Florence, made in 1528-9. Courtesy of the Casa Buonarotti, Florence, Italy.*


In 1528, when the Papal armies were threatening to attack Florence and restore the Medici family to autocratic power, the Florentine Republic gave Michelangelo Buonarotti the responsibility of strengthening the city's fortified defenses. By the time the attack came in 1529, he had designed and overseen the construction of a number of ‘bastions' at crucial junctures in the existing defensive wall around the city. These were so effective that the citizens of Florence were able to repel the superior attacking troops for nearly eleven months, until—through an act of political treachery—the city finally fell in 1530.


The design drawings Michelangelo made for the bastions had to consider their two main functions. First, to provide protected openings for the defenders to fire their muskets at the attackers, each of which covered a relatively narrow field of fire, but together covered the widest possible field of fire. Second, the walls of the bastion had to deflect incoming cannon fire in the form of cannon balls, which were as yet non-explosive. To accomplish both of these purposes, the walls had to be not only thick but relatively short and angled sharply with adjoining walls, creating a ‘corrugation' that would conceal gun ports and better resist the impact of cannon balls. In his drawings, Michelangelo primarily studied possible variations on this fundamental idea.


For all their practical purpose, these drawings have uncommon aesthetic power. Of course, this is because they are made by one of the greatest sculptors, and a self-taught architect—an “amateur of genius,” as he has been called—but it is also because the bastions required had too short a history as a building type to have ossified into a rigid typology. Michelangelo was relatively free to invent strong new forms and didn't hesitate to do so. Using straight and curved lines in various combinations, these designs assume—to the contemporary eye—the character of plans for buildings belonging to our era rather than his; or, at the very least, they anticipate expressionistic architecture of the present and last centuries that has been realized because of advances in building technology.


This bit of speculation is not, however, at the heart of the drawings' emotional and intellectual power. For that, we have to look to a fluidity of invention captured in the drawings. Michelangelo's mastery of mostly freehand pen and ink drawing (each line is precise and cannot be erased) gave him the freedom to experiment with form. Without this mastery, he would have sought, as any artist would, the safety of more familiar forms. We are struck by the *élan* of the designs, as much as by their visual coherence. We are moved by the seemingly effortless way they undertake the always risky task of invention. Not least, the fortification drawings inspire us to equip ourselves with the skills necessary to explore daring new possibilities for architecture, ones that engage the daunting challenges we—as Michelangelo—must confront.


LW


[![](media/mff-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-3.jpg)


.


[![](media/mff-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-1.jpg)


.


[![](media/mff-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-4.jpg)


.


[![](media/mff-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-5a.jpg)


.


[![](media/mff-5b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-5b.jpg)


.


[![](media/mff-7.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-7.jpg)


.


[![](media/mff-9a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-9a1.jpg)


.


[![](media/mff-9b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-9b.jpg)


.


[![](media/mff-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-10a.jpg)


.


[![](media/mff-10b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-10b.jpg)


.


[![](media/mff-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-11.jpg)


.


[![](media/mff-14.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-14.jpg)


.


[![](media/mff-12.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-12.jpg)


.


[![](media/mff-6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/05/mff-6a1.jpg)



 ## Comments 
1. [gerri davis](http://gerridavis.wordpress.com)
5.22.12 / 9pm


Thank you for sharing these powerful drawings Lebbeus – I agree they have the power to collapse centuries.
3. Romulus
5.22.12 / 9pm


Love the drawings over the human figures, i wonder if this had anything to do with process….





	1. [gerri davis](http://gerridavis.wordpress.com)
	5.23.12 / 1pm
	
	
	Rom while there is clearly a parallel formal investigation, the juxtaposition of the human figure with these fortification studies likely has to do with the scarcity and cost of paper. It would be incredible to find M's thoughts about the relationship of his sculptural, painting, and architecture practices. I wonder if those notes still exist…
	
	
	
	
	
		1. wildoo
		5.27.12 / 4pm
		
		
		I'm certain economics ‘figured' into this layered result, though I would wager, based on personal experience, that at some point in the execution of the latter dwg.s, that composition, on ‘some level', was considered…  
		
		design is after all, an “additive process” [imho]
5. [oletto](http://oletto.wordpress.com)
5.23.12 / 1pm


Hello Lebbeus, 


it seems to recognize alternatively natural and science-fiction inspirations


definitely a promising contemporary Italian architect.
7. [Mikey](http://www.mikeynitro.com)
5.23.12 / 10pm


This is so empowering and inspiring! Thank you for sharing your thoughts.


How's the “Illustrated Man” idea going?
9. [rafaeloshouldbe](http://rafaeloshouldbe.wordpress.com)
5.23.12 / 10pm


I have been looking-staring- at these drawings, and I still don't know what they are.  

I am debating myself to define them just as formal expressions or architecture of vanguard, disruptive architecture.  

We cannot deny the extreme formalism of these configurations, but they are challenging so many things in architecture that you don't know at the end.  

It is the same sensation I felt when I was in his built spaces, that in between masses, volumes, or space.
11. [Individuos opacos](http://individuosopacos.wordpress.com)
5.24.12 / 4pm


Hello,


The spaces created as a necessity born of concentrating on repel the outer limits. From outside to inside shows impenetrable but porous inside out, almost panopticon. Guard against a threat is one of the most exciting architectural themes.


Thank you for your revealing posts.
13. Firat
5.25.12 / 2am


These are some of my favorite drawings, of all time. Looking at them again, in this context, I see it as an architecture of urgency. It is really meant to anticipate one moment, of severe trauma to the city, but the walls will be around long after the war, and what is it to inhabit them in the aftermath? In this sense, I see a link here to the earlier four ideal houses posting. Great way to follow-up that wonderful work.
15. [Stratis Mort](http://arcsickocean.blogspot.com/)
5.25.12 / 11am


Really interesting how Da Vinci and Michelangelo became and proved they are great artists through constructing war machines, and actually supporting war and its effects. Architects probably have to be always related to their time.  

And in the same way how the architecture of the Roman Empire creates an environment that the human is lost inside the enormous and gigantic buildings, making as feel ‘nothing' inside their society, the importance is on the Empire and that was done successfully.
17. wildoo
5.27.12 / 3pm


Thank you Lebbeus for the seeds you germinate; the garden you grow.
19. Seriously
5.30.12 / 4pm


Merely wanted to stress I'm happy that i came upon your website page!
21. [Skeuomorphism 2: Authenticity](http://josephscherer.com/?p=121)
5.30.12 / 6pm


[…] new criteria of judgment if we look beyond digital interface design. Recently, Lebbeus Woods wrote a blog post about Michelangelo's designs for fortifying the walls around Florence that speaks to this […]
23. Sotirios
5.30.12 / 11pm


I doubt Michelangelo drew a fortification on top of bodies muscles skin and bones simply because paper was expensive or scarce. Michelangelo drew architecture ‘disegno' from the nude human flesh. He was not interested in mathematical abstractions or the preservation of ‘accurate' proportions. Michelangelo believed fragments of immortality, God, beauty and the infinite were embedded in the mortal flesh: in the muscles, bones and desires of the body. His plans read like bones with sinews and skins. Some of the drawings look like an anatomical vivisection of Vesalius. Michelangelo's drawings are rooted in a design theory that is based on the anatomy of a living body rather than a dead body. He observes naked bodies and draws their parts in union or all cut up. They are not allegorical drawings since they present us with the charged reality of the flesh. These anatomical plans vibrate with lines of life; unstable in vice and love, never static they move tear bend and flex.





	1. Pedro Esteban
	5.30.12 / 11pm
	
	
	Can you say what was the reason then, to draw the plan on top of the human figures? What is the relation between both elements in that specific illustration?  
	
	Can you elaborate more what do you mean, thank you. 
	
	
	Are these drawings just sketches or they are final in someway?
25. [Skeuomorphism 2: Authenticity](http://josephscherer.com/?p=47)
6.2.12 / 8pm


[…] new criteria of judgment if we look beyond digital interface design. Recently, Lebbeus Woods wrote a blog post about Michelangelo's designs for fortifying the walls around Florence that speaks to this […]
27. [Gio](http://gravatar.com/gb427)
6.6.12 / 1am


I do read them as anatomical drawings as well.  

Bones…the human pelvic etc..
