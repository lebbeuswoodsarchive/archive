
---
title: ARCHITECTURE AND BEAUTY
date: 2010-05-03 00:00:00
---

# ARCHITECTURE AND BEAUTY


The cast of characters is mostly familiar as are the images of their works, with a few surprises. Yet the new book by Yael Reisner, with Fleur Watson—“Architecture and Beauty: Conversations with architects about a troubled relationship”—stands out from routine compendia about contemporary architecture, because of its probing texts about the architects and their work. Based on exclusive interviews by Reisner with each featured architect, the texts offer critical evaluations altogether lacking today in most architecture books, which seem content with generalized commentaries. Our image-obsessed publishing culture often serves up the tasty visuals, but without much intellectual nutrition—a kind of fast-food approach that not only leaves us hungry, but also undernourished and ill-prepared to form considered judgments. This fascinating book sets out to give us plenty to think about, and so it does.


The well-written and unpretentious essays peer into the thoughts, aspirations, and, occasionally, even methods of the architects, often with carefully selected quotations from the interviews. The following are a few examples, with sample illustrations.


LW


FRANK GEHRY


[![](media/gehry-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/gehry-1.jpg)


“Gehry firmly rejects the notion that self-expression is a capricious act within the design process. Conversely, he believes that signature and democracy are integrally interlinked and, in fact, when an architect surpresses his or her emotions within the design process, it is an act that ‘talks down to people' and does not allow a full engagement with architecture. Certainly, the role of self-expression and its legitimacy in architecture is a familiar issue within architectural discourse, and one that resurfaces with a sense of self-righteousness with the digital realm. As a result, Gehry's position is consolidated by years of battling criticism that his architecture is too derivative of the art world—too sculptural and expressive. His response to this critique is clear and direct: ‘To deny the validity of self-expression is akin to not believing in democracy—it's a basic value—if you believe in democracy then you must allow for personal expression.'”


PETER COOK


[![](media/cook-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/cook-1.jpg)


“Undoubtedly, Cook has produced projects and buildings that are striking and provocative yet it is difficult to claim that his work—while pictorial—has ever engaged with a concern for conventional aesthetics. ‘The concept of aesthetics is a construct and a way of manipulating forces. For example, you can have an aesthetic of a conversation by introducing different subtopics such as balance, surprise, and intrigue,' he declares. ‘The Modernist aesthetic was a specific language and much of Modernism is linked to socialism and indirectly to a form of ascetic puritanism. You only have to read the text of someone like Hannes Meyer to feel the moral insistence.' Cook advocates an engagement with delight and holds an inherent mistrust of overly righteous values. ‘I was always irritated by piety: I'm too much of a natural hedonist. Yet I still proceed in a design with a mental checklist that involves ‘right' and ‘wrong' decisions, ‘rules,' ‘sequences of action' that have been trained into me from the Modernist ethic.'”


WILL ALSOP


[![](media/alsop-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/alsop-1.jpg)


“Alsop rejects the idea that he has a signature style and states that he finds it hard to understand the need for an overarching philosophy, as each project requires it own frame of reference and investigation. He claims that technology now affords the possibility to design anything. It is only a question of budget that restricts the process. However, he does acknowledge that there is characteristic thread through his work. ‘I can see that there is something that could be described as ‘Alsopesque.' But if I said ‘draw me an Alsop building, you couldn't really do it. You could draw a building I've done, but you couldn't draw my next building. I like that because I don't know what my next building will be and it continues to challenge me.'”


ODILE DECQ


[![](media/decq-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/decq-1.jpg)


“‘I think that the question of aesthetics within architecture is often equated with a question of moralization. So if you want to escape this oppressive doctrine of moralization then you have to be brave.' Decq recalls a discussion with Massimiliano Fuksas prior to him curating the Venice Architecture Biennale in 2000 that challenged the notion that aesthetics and ethics couod not coexist. ‘I said to him that he couldn't set the theme of ‘Less Aesthetics. More Ethics” because you can't disregard aesthetics completely. It's just not possible, because all human beings appreciate beauty—we are used to admiring, caring and being engaged by aesthetics in nature.'”


HERNAN DIAZ ALONSO


[![](media/alonso-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/alonso-1.jpg)


“Diaz Alonso's work has created a new approach to the architectural design process within digital culture, where the image is embraced as a primary generator for the work. As he explains: ‘I'm absolutely shameless about the heavy use of rendering within our work and the fact that we use shadow, reflection and so on as a vehicle for the direction of form. We work with computer renderings in a generative way from the very beginning of a project where we will start to speculate with color, reflection and so on. This starts to dictate the manipulation of the geometry and the form according to the effect we are trying to produce through the image.' Continuing, he suggests: ‘The difference between my work and the way other people work with these tools is that the image is something that is produced at the end of process, while we will start to speculate from the very beginning so that the image becomes the genetic code.'”


The full line-up: Will Alsop, Peter Cook, Odile Decq, Hernan Diaz Alonso, Frank O. Gehry, Mark Goulthorpe, Zaha Hadid, Zvi Hecker, Kol/Mac, Greg Lynn, Thom Mayne, Eric Owen Moss, Juhani Pallasmaa, Gaetano Pesce, Wolf D. Prix, Lebbeus Woods.


“Architecture and Beauty” is available online:


<http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=Yael+Reisner&x=0&y=0>



 ## Comments 
1. [Randy Deutsch AIA, LEED AP](http://bimandintegrateddesign.com/)
5.3.10 / 5pm


You wrote: “Our image-obsessed publishing culture often serves up the tasty visuals, but without much intellectual nutrition—a kind of fast-food approach that not only leaves us hungry, but also undernourished and ill-prepared to form considered judgments. This fascinating book sets out to give us plenty to think about, and so it does.”  

Thank you Lebbeus for presenting a few select quotes from the book. These are certainly not your run-of-the-mill filler/captions and help us to see the work – and yes, familiar images – in a new light.  

Thank you as well for maintaining the equally high quality of this blog.  

I had the pleasure of meeting you – when you were a visiting critic at U of Illinois in 1982 – and your work, ideas, thoughts and ideals have left an indelible and lasting impression on me.
3. [Chris Teeter](http://www.metamechanics.com)
5.4.10 / 3am


I like what Diaz is saying a lot…  

my biggest complaint about most architects who design, especially the pre-computer generation is their obsession with drawings and conceptual models, in my opinion, this ridiculous process of drawing, reviewing, drawing…annoying iterative process of back and forth. I know things change with clients because others are involved, but why do things have to change within the office or the mind of the designer, do they not know what they are doing? i'd answer after my short decade, yes they don't, hence intellectual design processes were invented, like the diagram…


it's just as bad to design by diagram as by graphic standards…bad as in, not creative.


why just start with the final architecture as the architecture? you know like in a dream, visions of pieces even…


drawings have nothing to do with architecture.





	1. JS
	9.20.10 / 7am
	
	
	I have been working with parametric architecture for two years now. While this offers new techniques, there are qualities of the design sketch and collaboration that can not be sacrificed in the design process.The drawing process is not only about collaboration, it is often the genesis of creative thought. It is a medium for experimentation and is an essential role of the architect as inquisitive explorer.
5. [Chris Teeter](http://www.metamechanics.com)
5.4.10 / 4am


avoiding work here and want to rant a bit, may just go get this book…


Diaz says “.. very beginning of a project where we will start to speculate with color, reflection and so on.” almost like it's a bad thing? 


what do other people do? look at black and white line drawings and imagine empty white boxes?!?!? you can't place color in a space without considering the general lighting, the space, the function, building technology, costs, light at noon, light at midnight, electrical power supply, materials, material finishes, the janitor using the wrong cleaning material causing loss in gloss, etc…


analogy – music & architecture


1. design by drawings is like just placing notes in cute patterns on the music sheet hoping some instrument will play it well


2. designing with one scripting language like modernism is similar to orchestra composed of only one instrument


3. designing by diagram or logical abstractions is like writing music based on chords and octaves, numerically etc…no fortissimo, no progression besides logics outside music…you know like Derrida and language and architecture and language?!?!?


4. asking a bunch of people to get together and crash design styles and procedures together hoping to get results…well that's John Cage scripting an unscripted session in a chamber somewhere.


5. design by intutition, well that's like playing music knowing its going to sound good to you, and what's wrong with that?


whats wrong with music that sounds good?  

whats wrong with architecture that is good?


i like everything Gehry says, above as well.
7. Sam
5.4.10 / 8pm


Oooooooooooo shIII-ny…  

Sorry let me wipe the drool off my mouth and get my head straight…


Let me break the illusion. Did Odile Decq just say that it's just impossible to disregard beauty? I'm an aesthete myself, but architecture can't just be captivating by putting aesthetics before ethics. I can get the whole idea about being an immoralist, but I thought it was about overcoming our mechanical desires and into more immunized or self-controlled state of being. In The First Elegy Rainer Maria Rilke wrote: 


“…For beauty is nothing  

but the beginning of terror, which we still are just able to endure, and we are so awed because it serenely disdains  

to annihilate us…….”
9. [Jorge Javier](http://arqwave@blogspot.com)
5.17.10 / 3am


Mmmm… i'll sound dated, but, beauty is in the eye of the beholder… and sometimes we must accept that architecture goes beyond mere functionalism, statements around the globe that shows how amazing the overall shapes amaze us, is basically a reminder that we must keep the idea of “impression and imprint” in our daily vocabulary. In the end, there's no right answer to beauty, is a human condition to observe and admire.
11. Cole Slaw
6.23.10 / 1am


Anyone else agree that Diaz Alonso looks a lot like Ron Jeremy aka “The Hedgehog”?
13. [Lebbeus Woods on Beauty « Adam Miller's Blog](http://admiller4.wordpress.com/2011/03/01/lebbeus-woods-on-beauty/)
3.1.11 / 11pm


[…] Woods' article is a response to the publication“Architecture and Beauty” by Yael Reisner.  Below is […]
