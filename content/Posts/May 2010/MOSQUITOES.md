
---
title: MOSQUITOES
date: 2010-05-10 00:00:00
---

# MOSQUITOES


[![](media/kk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-1.jpg)


(above) Cover of the landmark “Mosquitoes” publication.


In the annals of the architecture avant-garde, the team of Ken Kaplan and Ted Krueger—K/K R&D—occupies a truly unique place. Never before or since have satrire and humor been deployed to confront such serious architectural issues. The social criticism of Swift and the moral parables of Kafka live on in their writings and constructions, but also concepts such as the analog and renegade cities, which speak both to the mission of contemporary design and today's spontaneous expansion of traditional urban centers. Their use of the machine is both a critique of functionalism and a call for hands-on invention that transcends corporate goals and limits. For Kaplan and Krueger, paradox and contradiction confound existing institutions of power, but at the same time are political opportunities for the rest of us. Their work is more relevant than ever.


LW


[![](media/kk-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-2.jpg)


[![](media/kk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-3.jpg)


[![](media/kk-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-4.jpg)


[![](media/kk-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-5.jpg)


[![](media/kk-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-6.jpg)


[![](media/kk-81.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-81.jpg)


[![](media/kk-92.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-92.jpg)


[![](media/kk-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-11.jpg)


[![](media/kk-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-10.jpg)


[![](media/kk-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-12.jpg)


[![](media/kk-132.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-132.jpg)


[![](media/kk-16.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-16.jpg)


[![](media/kk-141.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-141.jpg)


[![](media/kk-151.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-151.jpg)


[![](media/kk-17.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/kk-17.jpg)


Pamphlet Architecture 14 is still available, online:


<http://www.amazon.com/Pamphlet-Architecture-14-Mosquitoes-Krueger/dp/B003156DTS/ref=sr_1_12?ie=UTF8&s=books&qid=1273449406&sr=8-12>



 ## Comments 
1. Marc Lewis Krawitz
5.12.10 / 4am


The analog of architecture begins to have a particular relevance to the realm of understanding surrounding contemporary views of program, of form and content, where ideas of space and paradigms of architecture are shoved into often a chaotic or inverted moment — as we may gain a spatial experience / comprehension from object rather than inhabitation. It comes to light whether the analog can actually explain experience, or whether it is a mere reducer of our perception of the world. But I imagine that depends on the motives of the analog. 


In the making of an object that seeks to physically rationalize, we must ask whether the analog's true meaning is in its making, or whether its foundations lie in its communicative role. But it then begs to ask whether a distinction can be made towards the operative occurrences of built form, and the power that a comprehensively tuned-to-the-body analog can provide for its audience. Is the (potential) scale of the analog, in this relationship to the body, a rendering of experience or content more valid to the (potential) built form it seeks to represent?
3. elan
5.14.10 / 3pm


-fish cities- or man rays/ analog or is it Action of the reel/ constructive-matrix-landscapes or ‘buildings'? avant-garde (see above) i suppose references this other black and white period that memo-rays have all but turned too grey. i welcome whole heartedly this vocal, timely flash (of clarity-in-depth not glow-of-sheen). . may its air be contagious with or without a mask.
5. [A Daily Dose of Architecture » Book Review: Pamphlet Architecture 11-20](http://dailyarch.l78z.org/2011/10/book-review-pamphlet-architecture-11-20/)
10.17.11 / 10am


[…] for survival” has some amazing body/device imagery and stinging commentary; as Lebbeus Woods puts it regarding the latter, “Their use of the machine is both a critique of functionalism and a […]
