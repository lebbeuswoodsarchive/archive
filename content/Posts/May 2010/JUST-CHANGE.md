
---
title: JUST CHANGE
date: 2010-05-16 00:00:00
---

# JUST CHANGE


Change, and in particular transformation—one form changing into another—is not simply a matter of the alteration of an existing form to create a new one. Rather, change creates what we could call a third form, really a third state which is the state of changing, itself. We are so accustomed to thinking in dialectical, binary terms, employing either-or reasoning, that we overlook or consciously avoid the in-between state of change, which is really the state we continuously inhabit. Our fixation on goals—forms that for whatever reasons we desire—induces us to pass over the so-called intermediate state, the state of transition, the actual state of change, in our rush to get to the desired form. When we get there, of course, we find that our goal, once attained, no longer holds our interest, so we set a new goal. In other words, we keep changing, but without ever embracing the state—and the forms—of changing itself.


As long as we can keep up with changes, moving from goal to goal, this does not matter so much. In fact human civilization has worked for thousands of years without much thought being given to the states, the forms of changing. However, when change starts to get ahead of us, that is, when we are not able to absorb one change fully before the next change comes, our goal-fixated system starts to break down. The is because we are more and more caught in the state of changing, and less and less able to reach our desired goals, even if only long enough to get bored with them. At a certain point, the only attainable goal is to live within the state of change itself, like refugees, gypsies, or nomads. It seems likely that in the future, if the pace of change—social, political, economic, cultural—continues to increase, this condition will become common in all social classes.


In such a world, the design and construction of permanent buildings will become less important than it is today, and architects will turn their attention to the development of concepts and techniques of building temporary living spaces. At their most primitive, these will involve portable structures such as tents. With increasing sophistication they will involve site-specific constructions that are created and, just as importantly, disappear as needed or desired.


LW


(below) *Air Architecture (1961), Yves Klein*. Living space created with water, fire, and steam. “Of course, with all the progress made by science, this is no longer a utopia today. Technique, however, could in fact realize such things!… To find nature and live once again on the surface of the whole of the earth without needing a roof or a wall. To live in nature with a great and permanent comfort.”


[![](media/klein-12.gif)](https://lebbeuswoods.files.wordpress.com/2010/05/klein-12.gif)


*Life on the Supersurface (1972), Superstudio:*


[![](media/supstudio-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/supstudio-2.jpg)


[![](media/supstudio-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/supstudio-3.jpg)


*Vector Architecture (2001-5), LW.* “…architecture can be understood as the organization of energy:”


[![](media/lw-3c2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/lw-3c2.jpg)



 ## Comments 
1. [Jorge Javie](http://arqwave@blogspot.com)
5.17.10 / 3am


In constant times of changing we must face when to go with the flow, and when to really create ourselves an evolutionary path towards a wider world. Architecture has been changing faster in the last 20 years than in the last 2 centuries. The problem that i see, is that we are losing our identity in order to attain global success… in theory it is not bad—however, lack of originality and constant comparisons led us to a commercial, almost waste-able craftsmanship. To me, change must be unique in itself, how do “I” (architect, person) can create a new idea based in history, to avoid repeating myself… then how to improve my surroundings with innovative ideas that eventually will express our new way of sharing our society. Not to fear but realize how incredible and not-so-crazy our visions can be. Utopias are based in these thoughts… why not to evolute before other people.
3. [Michael Phillip Pearce](http://www.carbon-vudu.us)
5.18.10 / 2am


The change of “forms” for most I would agree is a passed over state due to a limited perspective and the constraints of consciousness. Thus, it becomes a somewhat rationalized “goal oriented form”. 


However, what about organic change, where a form evolves through a series of events and unconscious notions that transpire from thoughts that have no rhyme or reason [binary logic] yet they feel “right as rain”?


Inhabiting this state of mind I could not even consider why we design as we do, i.e. skyscrapers to this day, post modern notions, and reflecting on the urban sea of abandoned architecture that haunts America today. Is this the after affects of this “goal to goal” inadequate form change? 


With the digital age at an all time high it is easier to visually see the chaos of the frequent disasters, governmental and big business control issues, and economical down falls. I think it is only a matter of time before we redefine our perception of the world, in matters of living, working and sustaining ourselves [where we work/live, when we work/live, why work/live and how we work/live]. But, then my thoughts quickly change as I type this, and I know people are fearful of change—in any sense of the word it is no wonder why its process is a skipped over .
5. Joshua Nason
5.19.10 / 7am


As I read an image came to mind early on for me…a wall. 


Now, to explain myself, I have been thinking of walls quite a bit lately and quite differently than I had before, as well. Something that I have began to understand more fully is what I discovered to be the lie of the wall. We see, understand and even design walls to divide, enclose, to set at odds. Walls are an apparatus that create, “dialectical, binary terms,” similar to what is described so well above. However, walls are also indicative of that state of change that goes unnoticed. We focus on the moments created on each side of the wall, whether interior vs. exterior or east vs. west or ours vs. theirs. We continue to watch to the poles and ignore the parallels, or even the equator as the case may be. As stated above we are drawn to the goals as the change goes unnoticed. However, that wall, that divider, is not just a analogy for that third state, it is an example of it.


Forgive a seemingly boring example, but in buildings walls are not just single membranes that separate inside space from outside space. They are space themselves. These are material assemblages, encased by analogous boundaries at a different scale. Within those boundaries are gaps / holes / spaces that actually create a thermal barrier, and yes, therefore, a separation between the spaces created at the walls edge. Within those layers, change occurs. Temperature from the outside is slowly altered as its transfer is rerouted while trying to penetrate the barrier. It is not, and cannot be just two completely unassociated states separated by a thin line of immaterial division. There will always be that third state. That transition between the poles that bridges more than it truly separates.


At the city scale, walls can enact a more demonstrative statement of authority and division. Take for example the walls spoken of in an early post on this blog entitled, “Wall Games”, or consider the Berlin Wall, the Great Wall of China…the list could go on. These walls were [and still are, in some cases] intended to keep “them” out [or in]. They divide the theirs from the ours. Again, this is not a clear division of dissimilar states. If the states were truly dissimilar, they would need no division; it would just happen, naturally. Rather it is the fear of the similar that builds such walls. It is the devastating idea of mingling or losing or finding out “we” are not that unique from “them” that builds these walls. So, in order to keep the wheat apart from the tares we build walls. Those walls then become an embodiment of that third, fluctuative state. The wall now is a transition, even if an abrupt one, between what is supposed to be two differing quantities. Further, as the Berlin Wall was erected and later as it came down, it was a marker of change. Or was it? Was it in fact an embodiment of a third state or energy of change? As the wall came down was the latent energy of change released into the city as an empowering force of social evolution? Or was it just the ends to someone's attained goal? Maybe both?


Now, as we look forward into change in architecture, how do we understand change. The examples provided powerfully display works without walls and rightfully so. In another piece by Yves Klein titled, “Leap Into the Void,” he climbs onto a wall and jumps off into a world of change. He is helping to change the definition of art. He is doing so by showing a body moving through space, therefore exhibiting a change of state, a change of energy. Even though that piece was nearly 50 years ago, maybe a leaping from the wall [as well as its limits and permanence], and therefore, a releasing of the wall's potential energy is what we need to enact some change in architecture.


Great piece, Lebbeus! Very insightful and inspiring. Thank you. I did not intend to write so much. I hope it is not too long.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.19.10 / 5pm
	
	
	Joshua Nason: A very thoroughly considered set of thoughts that I largely agree with. In fact, they prompt me to follow through on my next post, in a day or two, about the ‘walls of change' I have proposed for the Old City of Havana—La Habana Vieja. My point in the present post is not to propose either-or thinking—walls or no walls—but simply to emphasize the need to conceive of new ways to construct spaces that are more ‘fluid' in response to changing needs and desires.
	
	
	Speaking of ‘fluid,' have a look at [an earlier post](https://lebbeuswoods.wordpress.com/2009/06/28/fluid-space/), for an image of the Klein leap you write so well about!
	
	
	
	
	
		1. Joshua Nason
		5.20.10 / 4am
		
		
		Dear Lebbeus: Thank you for your reference to “Fluid Space;” I thoroughly enjoyed revisiting this post. I also enjoy the subtle connections between not only shared precedents, but topics as well. Fluidity is fascinating, as are aspects of state changes and each has added efficacy when juxtaposed to boundaries that alter, frame and even choreograph them. Mr. Delanda's comments to said post are amazing as well. I look forward to learning more about La Habana Vieja. Thanks again!
7. River
5.22.10 / 4am


Lebbeus, I've heard your space poems ringing through the ether for a long time now. But this one takes the cake! Please feel free to explore <http://www.evolver.net> with an open mind. We need to dispense with all the “crustimony proceedscake” (-A. A. Milne) and get to work.


Here's a sample from today's communiques:



And here's what it's about:
9. [Chris Teeter](http://www.metamechanics.com)
5.23.10 / 2am


so i just spent an hour on this and Evolver…and re-hashing Delanda….remembering that Sante Fe institute and some economist's point about policy allowing competitive change between states (geographically) being the most productive manner for advancement in countries' economical states…wall, threshold, moment of deconstruction, etc…


just going to state the obvious if it isn't obvious already – Change has been the goal of just about any “advanced” study of anything for the last century or so. for Architecture it came much later, since architecture changes quite slowly…1900's for physics and math…1970's for architecture.


you know what it really is, it's not so much structural goals versus yet-to-be moments of systems of change, etc..


it's two (2) type of people – I guarentee this


type 1 – it's the fact that most people have horrible memories and wake-up every day like it's ground hog day all over again. these people need goals and need a history to reference. every morning they forget the change they were in and have to re-hash their past and have to re-state their goals. reset, reset, etc…where I come from and who I want to be is who I am.


type 2 – then their are the others, who are the goal (or complete lack there of). it's kind of like they wake up every day in the fluid of change, like a vessel just moving along in some direction, they don't have to remind themselves of what their goals are and they don't have to remember from which port they came. i'd say typically outsider type of people, although outsiders can be very social and often are the social glue. these people would have very good memories, as if any re-hashing of a change that has happened is extremely boring and can just be skipped and you're yawning as you read this…they are and that's it.


Architects


i'd say most architects fall into the people with bad memory, and these same people while trying to understand the 2nd class of fluid changers get caught up and lost in the study of “Architecture of Process”. Look, if you don't get it naturally, don't bother….you need an -ISM and stick with it, ground hog day all over again and again…eventually you'll hammer it out, that ideal and goal you've been trying to attain for years, over and over again….Monograph it! then you go teach and tell your students that goal is architecture! please retire already, or just say – when I was a kid I thought architecture was this and we built it when I was a grandpa.


the intro to that red book by Libeskind, a speech I think he was giving at Bauhaus on it's anniversary, point of intro I think, how to continually attain that moment of serious change, genius change, brought on by Type 2 people….that's a hard goal for Type 1 people, they'll fail miserably trying to get that goal…now go draft for a Type 2…or wait by continually hammering out to be a genius of change you might just do it…the problem with America's academic training of architects – we all want geniuses of change.


(i saw this book at Borders, something about the myths of moderism and how the whole profession of Architecture went to the shitter when it became the training of Genius profession)


because clearly Type 2's are genius and Type 1's are not…no not really, but that's to complicated to get into now…not getting into how change as a goal and the perception and understanding thereof is the measure of genius, etc…oh wait here's the example – Chaos Chaos Chaos…then Dude One steps in – no not really it's fractal geometry blah blah blah – see we can control it now, Dude One's a genius. Was he Type 1 or Type 2, did he remind himself everyday something in the world must be Fractal…or did he speak and it was called Fractals.


Lebbeus, I think architect's have already begun to change the way they design, most architect students don't become architects, and most architects don't do traditional practice of architecture, they have weird cross-vers and collaborations, and the architects who still claim to be architects do architecture that tends to not be traditional in construction and methods….that leaves only a few architects, who at this point might as well be space Engineers or building technology experts.


makes me wonder why there even is an AIA? thanks for the forms and contracts.
11. [me](http://naturalinsecticides.net)
3.29.11 / 12pm


being a fool will always make you stronger
13. [Steve](http://www.buywheyproteinreview.com)
3.30.11 / 1am


What you write is so true. Times change so fast today that we never sit to smell the coffee. As you mention, we attain a goal (change), but just move past it quickly and never enjoy what we achieve. Today is a microwave society, it has to be fast, and with that change occurs at lightening speed. Sometimes it flies right by and we don't even enjoy it, even when it is a big accomplishment. So what you have in your article is right on and I really enjoyed reading it. Thanks so much,  

Steve
15. [Kent @ Whey Protein Review](http://www.buywheyproteinreview.com)
4.1.11 / 9pm


If there is one thing we can depend upon it is Change! Entropy is boring anyway, wouldn't you agree? And so much of the modern architecture reflects the same old mantra as before… however, most of the esoteric symbolism has been lost by this un-illuminated younger generation. I enjoyed reading your article and especially like the comments and theories of Teeter.
17. [Mike Wong](http://www.main-street-marketing-machines-2.com)
4.4.11 / 1am


The state of change seems like it comes from void first then the projection of the mind and then must result in clarity. 


“The is because we are more and more caught in the state of changing, and less and less able to reach our desired goals” this has got to be the most profound sentence to me. 


I actually had to contemplate this for several hours to let me mind grasp the totality of this statement and this post. 


Also i so agree with Chris's statement about #2 types are truly visionaries.
19. [pearce](http://www.carbon-vudu.us)
4.4.11 / 5am


Teeter your last line was hilarious–thanks.  

Reflecting on the title of this post…just change, or perhaps accept change? One of my favorite inscribed memories from the USMC: “improvise adapt and overcome”.
21. [Dover](http://www.dovertocalaisferry.co.uk)
4.7.11 / 9pm


Change is the only constant
23. [Jamal Stephens](http://www.floridadolphintours.com)
4.8.11 / 6am


That last photograph, with the accompanying quote, is really evocative. Is it from some sort of art project?


Anyway, I will look at architecture differently now, and consider the “organization of energy”.
25. [xMoDx](http://xmodx.com/)
4.8.11 / 8am


Create clear and well defined goals.


In whatever you attempt in life, you must first have clear and well defined goals. You are where you are now because you might have become indifferent about your goals in life.


Establish the action steps.


You should not think about making your problems go away. Instead you should set your mind to create actions steps to bring what you want into existence. Clear goals and precise action steps or strategy are requirements to start off.


Be willing to make changes.


Wanting something different means letting go of whatever is holding you back up until today. Your personality may need a lift, your self esteem a boost and your thinking definitely has to shift.


Start believing in yourself.


Even though you have made mistakes and gone through challenges in life, there is every possibility to turn things around. You have to start believing in yourself. Your beliefs about what you can do and achieve are vital to making your dreams come true.


Take immediate action.


It does not matter whether you are fully prepared to start. Once you have a goal, know what you have to do, and are willing to get out of your comfort zone and start believing in yourself, take immediate action. Do what you can with what you have and you will build the momentum and fight your fear. Your confidence will increase and so will your self-esteem.


Allow time for study and learning the skills.


In order to make life changes, you have to allow time to study and learn new skills. You gain knowledge through reading, listening and from doing things. If what you do does not give you the expected result, you have not failed instead have gained a new experience.


Take charge of your thoughts.


Your mind is endlessly producing pictures, images, voices and sensations without your conscious awareness. Those thoughts have influenced and affected your emotions and responses. It is about time you take control and give the instruction instead. Direct your mind by creating images of what you want. You should from now decide what you want to say to yourself. Otherwise it will go on with the endless cycle of producing thoughts from the past and that of doubts and fears.


Pause during the day and night to contemplate and to remain in focus.


You need to take a break in between to see whether you are moving in the right direction. Sometimes you may seem busy doing things that are of not much importance. Contemplation allows you to observe and listen and it provides inspiration.


Impose the disciplines. Persist even when progress seems slow.


Keep on even when progress seems slow. Remind yourself that it takes time to grow and to make life changes. Impose the disciplines to take action every day. Your next action or the next person you meet may provide the solution.
27. [Mike Geary](http://mikegeary.com)
4.14.11 / 5am


@xMoDx: very well said, i totally agree with you. When we make a commitment we have to also recommit ourselves to it everyday so we can have it refreshed all the time and we can regain our energy as we continue to chase the goal of our life.
29. [Roy](http://www.smallbusinessdebt.org)
4.26.11 / 6am


A truly visionary architect will be able to see the complete life cycle of the building in its proposed environment now and well into the future. That future includes the decay and destruction that will eventually come. The greatest classical architecture of all time has been inspired enough to be projected way into the future. Most modern buildings have decay built into them as a main influence.
31. [Amber](http://www.inflatablekayaker.com/)
5.3.11 / 5am


Very well said Roy, An architect that takes into account every little today in his design is someone every aspiring architect should look up to.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.3.11 / 11am
	
	
	Amber: your comment is clearly a bit of spam, but I thought ‘inflatable kayaks' were an interesting product, so I posted it anyway.
33. crista
7.14.11 / 5am


One thing permanent in this world is really “CHANGE”. And we cant do anything about it. We just hope that whatever changes brought us, it will be for the betterment of all.
