
---
title: WALLS OF CHANGE
date: 2010-05-28 00:00:00
---

# WALLS OF CHANGE


[![](media/berlinwall-2a3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/berlinwall-2a3.jpg)


*(above) The Berlin Wall, November 9, 1989.*


Walls are meant to separate, that is true. After all, it is an essential mission of the architect to ‘define space', which means to construct limits, edges, boundaries that carve out particular pieces of undifferentiated space for human purposes. Walls of many different shapes and materials are prime means at the architect's disposal, and we are used to thinking of them as dividers between one side and another. Most often these two sides are different, even opposing—cold/warm, dark/light, noisy/quiet, public/private—and the separating walls secure people or things on one side from people or things on the other. On rare occasions such walls reverse their roles and bring people together—think of the Berlin Wall on November 9, 1989, when East and West Germans swarmed around and on the Wall, effectively bringing the political division of Germany to an end; or, in a more hypothetical example, the Israeli ‘security fence,' as the site for [the Wall Game](https://lebbeuswoods.wordpress.com/2009/11/09/wall-games/) bringing Israelis and Palestinians together in constructive play. At yet other times, walls that separate and divide can become armatures for change, that is, for the transformation of conditions on one side or both.


Commonplace examples come to mind: the building wall on a public street covered with graffiti—at once an aesthetic and political presence that impacts all who see it; or, a simpler example, the interior wall of a private house, where the owners hang paintings, photographs, or a flat-screen TV, changing the mood, evoking nostalgia, or delivering news that subtly or abruptly changes their views of the world. In such cases the wall becomes more than a spatial divider, but rather an instrument of change.


[![](media/havphoto6-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/havphoto6-21.jpg)


(above) Street in *La Habana Vieja*, December 25, 1994. Clearly visible is the wooden bracing intended to prevent the collapse of deteriorating buildings.


In Havana, some years ago, it was proposed that a series of walls be constructed within and around the old city, *La Habana Vieja*. This section of the modern city of Havana dates back to the beginning of  The New World in the 16th century and still has buildings from that period as well as succeeding centuries, up to the present. Many are in a state of decay from lack of upkeep and restoration, simply because the communist government does not have the funds to do so, and the only private development is by foreign investors building luxury hotels and other sites for foreign tourism. Att the time of the proposal, the old city was populated by an economic and racial underclass that foreign capital had no interest in helping, so that any effort to save this unique city-within-a-city, would have to be from the ground up—not counting on much top-down funding by government or investors.


Ideally, the proposed walls would contain some infrastructural purposes: water purification, at the least, the generation of electricity, like an ‘urban battery,' at the most. This means that these would be ‘hi-tech' walls, into which individual dwellings would plug, as self-sustaining urban units. A good deal of clever engineering and sophistication in construction would be required to build them. Professionals, whether public or private, would have to be involved in the design and the supervision of construction. Their services could be donated to the project of saving this endangered district, or underwritten by international grants. The point is, the proposed urban walls would act as generators—or, more precisely, as re-generators—of the old city.


The decaying of the old city takes the form of collapsing or deteriorating buildings that leave voids to be filled with new construction one at a time, as needed. The building methods would be somewhat spontaneous, employing the construction skills of the builders—teams of inhabitants whose mastery of techniques and the assembly of materials would improve with time. Specialized teams would build the massive new infrastructural walls, the armatures of change. All together, they would form a community devoted to constructing their own environment. Its economy would be based on in-kind trading of goods and services, and would eventually extend beyond the newly walled old city, into greater Havana itself.


A final note. The new social circumstances of this community, as well as their necessity to invent new building techniques to deal with voided spaces in existing buildings and the new infrastructural walls, suggest that the spaces they build and choose to inhabit will embody new relationships within and without. In short, they could constitute a new urban architecture, unique to its place and time. For this reason, the drawings of the proposed projects shown here are to be understood only as aids to thinking and not as prescriptions. What would emerge in reality would be far more complex, subtle, and new.


LW


[![](media/lwblog-huer-141.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-141.jpg)


[![](media/lwblog-huer-101.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-101.jpg)


[![](media/lwblog-huer-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-10a.jpg)


[![](media/lwblog-heur-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-heur-11.jpg)


[![](media/lwblog-huer-61.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-61.jpg)


[![](media/lwblog-huer-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-21.jpg)


[![](media/lwblog-huer-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-4.jpg)


[![](media/lwblog-huer-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-11.jpg)


[![](media/lwblog-huer-13.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-13.jpg)


[![](media/lwblog-huer-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-8.jpg)


[![](media/lwblog-huer-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-7.jpg)


[![](media/lwblog-huer-122.jpg)](https://lebbeuswoods.files.wordpress.com/2010/04/lwblog-huer-122.jpg)



 ## Comments 
1. alex
5.29.10 / 3am


this is great. seems the most fascinating architecture cant be patronized by your ‘typical' client.
3. blackdog
5.31.10 / 8am


amazing drawings …those seems post war adaptation houses.
5. [insurrectionnow](http://yyyprgfjisgahfgp.wordpress.com)
6.2.10 / 10am


wonderful article.
7. wave
6.2.10 / 9pm


Every time i look at your drawings it inspire me to create and recreate what we have at our very eyes. Sometimes tearing some papers and then paste them together again finding a new meaning… i think that is a some sort of Derrida excersise… funny and with thousands of posibilities. So i consider your vision quite a magnificent interpretation of what would it be if we break a little bit our boundaries.
9. Joshua Nason
6.5.10 / 10pm


I agree. The drawings above, as always, are incredibly beautiful.


For me, the best thing about them is they parallel the beauty of the ideas with which they coincide. I fear that is too rare in architectural practice as well as in architectural academies. The simplicity of redefining the wall from divisor to generator houses incredible potential. Here, that simplicity is taken to enough depth that walls infused with infrastructural habitation can act as an “urban battery” as stated above. The key is habitation. When that which was divisive becomes inhabited it CAN be transformed into something connective. Now, that is a beautiful urban and architectural idea!
11. [dave](http://organicmobb.wordpress.com)
6.7.10 / 3am


Great post- What is a wall anyway? 


 Havana's post rationalization of crumbling buildings via the new supports between buildings makes me think of Dali's supports in his paintings. They begin to allow for ‘slumping' to occur(think glass slumping) and a new form emerges due to the imposed framework. They have less to do with the ground and more to do with a desired location or in this case object in space. Dali's supports contact what he depicts as skin, they contour to the body. The supports in your photograph have linear elements comprising a truss system. It is beautiful how each is so ‘site specific'.
13. [alejandrodiazbarrios](http://alejandrodiazbarrios.wordpress.com)
6.8.10 / 4am


Ha roto las barreras ideológicas del simple Cubano, sinceramente es un avance muy apreciado, bueno el pensar en resolver de una manera u otra los problemas que se enfrentan en la isla!  

Bravo Lebbeus
15. [Diehl Art Gallery](http://www.DiehlArt.com)
6.29.10 / 3pm


Absolutely brilliant drawings! Keep up the great work!
17. [Walls of change « Archivio Caltari](http://archiviocaltari.wordpress.com/2010/07/05/walls-of-change/)
7.5.10 / 8am


[…] via Lebbeus Woods […]
19. [# HETEROTOPIC ARCHITECTURES /// Walls of Change by Lebbeus Woods | The Funambulist](http://thefunambulist.net/2010/12/23/heterotopic-architectures-walls-of-change-by-lebbeus-woods/)
12.23.10 / 1am


[…] 2010 by Léopold Lambert| Leave a comment Lebbeus Woods‘ last article on his blog entitled Walls of Change investigates the wall as an urban element that separates two milieus from each other or punctually […]
21. Greg
3.31.11 / 10am


Nice post, thank you!
23. [Walls of change - Archivio Caltari | Archivio Caltari](http://www.archiviocaltari.it/2010/07/05/walls-of-change/)
11.8.11 / 10pm


[…] via Lebbeus Woods […]
25. [Efectos Espaciales](http://www.efectosespaciales.net/berlin-november-9th-1989-walls-of-change/)
8.8.12 / 1pm


[…] WALLS OF CHANGE « LEBBEUS WOODS (Source: <https://lebbeuswoods.wordpress.com/2010/05/28/walls-of-change/>) Tags: Berlin/city/history/photo […]
27. [Arash Basirat](http://etoood.com/Profile.aspx?MyKey=334)
8.14.12 / 12am


نسخه فارسی این یادداشت را با برگردانی از خانم فریبا شفیعی می توانید در ادرس زیر مشاهده فرمایید  

<http://www.etoood.com/Security/Article/Article.aspx?ArtKey=485>
