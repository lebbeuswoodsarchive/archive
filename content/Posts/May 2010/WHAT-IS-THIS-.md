
---
title: WHAT IS THIS?
date: 2010-05-28 00:00:00
---

# WHAT IS THIS?


[![](media/whatsthis-2b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/05/whatsthis-2b.jpg)


.


Perhaps simply to underscore the ambiguity of images when it comes to their meaning—and also to have a little fun—this blog will, from time to time, post an image and ask readers to write a caption for it. These should be submitted in the comments section below. After a week, the best caption will be chosen by our editorial board (consisting of me) and posted below the image


I hope you will participate!


LW



 ## Comments 
1. Kiel Bryant
5.28.10 / 3pm


Kallista, the first anti-grav architect.
3. [Sean P Murphy](http://none)
5.28.10 / 3pm


In jump-rope, as in all things, Kate was an overachiever.





	1. Christopher
	6.7.10 / 6am
	
	
	Yes. The clear winner. Simple, funny and clever.
5. [lorbus](http://www.lorbus.com)
5.28.10 / 4pm


The power of the scribble took me for a ride…
7. haveacupoftea
5.28.10 / 4pm


Trade the drawing board for fieldwork, she was advised.  

Little did she know what she was signing for.
9. harrison
5.28.10 / 7pm


Shadowed hands pull me down as i attempt to brand a city that was not mine.
11. Jeff Brown
5.28.10 / 8pm


drawn-out and strung-up in a surface tension of suspended thought……..
13. [Chris Teeter](http://www.metamechanics.com)
5.28.10 / 9pm


Harold and the Purple Crayon is not a true story, good luck scribbling your way out of that.
15. lepo
5.28.10 / 9pm


doodling the skyline is hard yet fun
17. David
5.28.10 / 9pm


Here is an edited version of Ryan McGinley's photograph of M.I.A. for The New York Times
19. Chris
5.29.10 / 4am


Here is my evidence of ownership. The city is mine ;P
21. [cygielski](http://cygielski.com/blog)
5.29.10 / 6am


Tower, switching to autopilot.
23. [shlomo](http://kafee.wordpress.com)
5.29.10 / 6am


‘Bazelitz' visits Lebbeus Woods
25. Ali
5.29.10 / 10am


Hanging from some one else's signature
27. monumentalinternational
5.29.10 / 11am


alright, you try poledancing with a square relic.
29. monumentalinternational
5.29.10 / 11am


the king-kong/blond-heroine scene isnt a 1 girl performance piece
31. ff
5.29.10 / 2pm


Headed to Manhattan
33. josh
5.29.10 / 3pm


When ‘hanging out' in New York:  

High-tensioned steel rope = Advisable  

Black licorice= Not
35. elijah
5.29.10 / 7pm


Architecture's survival  

may require us to  

expand above and  

beyond the city grid …
37. Michael Cranfill
5.30.10 / 1am


The Seventh Record Of A Floating Life. Now the heavens and earth are the hostels of creation: and time has seen a full hundred generations. Ah this floating life, like a dream…True happiness is so rare!
39. Nyvang
5.30.10 / 3am


(figure) sky skating
41. [gaguri](http://guriguriblog.wordpress.com/)
5.30.10 / 8am


At least she makes more sense than the city's planning





	1. [gaguri](http://guriguriblog.wordpress.com/)
	5.30.10 / 10am
	
	
	I'm probably thinking more than I should about this but I'd go instead with “Well, at least it's more legible than what is behind her writing”…
43. josh
5.30.10 / 2pm


…so much for buying “Spider-man for Dummies.”
45. Michael Cranfill
5.30.10 / 5pm


Shen Fu's reflections on his life in 1809 was titled  

Six Records of a Floating Life. He had a difficult life. Chiang Su-hui collaborated on the english translation. She added this little quote from Li Po, in my title submisson ” ….True hapiness is so rare”.


overcoming gravity,our desire,pushing against the perimeter
47. [thais](http://superthais@gmail.com)
5.30.10 / 6pm


free at last
49. Mazin
5.30.10 / 8pm


‘this is what i think of the empire state building'
51. steve
5.31.10 / 2am


Frank Gehry has given up on designing hats for lady gaga. He has moved towards bungee cord harnesses.
53. Christian Peter
5.31.10 / 3am


With[drawn], jettisoned and high in New York City
55. H
5.31.10 / 4am


if I could sign my death signature, this is what it would look like
57. [Ivan Ostapenko](http://molotok.ca)
5.31.10 / 6am


To draw a line is to take a leap of imagination, capable of overturning reality.
59. Chad Brown
5.31.10 / 1pm


from here perspective – look.
61. [Evan Bray](http://bigbuildingsandfireworks.blogspot.com/)
5.31.10 / 1pm


I'm going to go with ‘Tag Lines.' Or maybe ‘Sky Lines.'
63. [Evan Bray](http://bigbuildingsandfireworks.blogspot.com/)
5.31.10 / 1pm


Upon further consideration, my submission should be in the singular form: Tag Line or Sky Line.
65. SJ Lee
6.1.10 / 2am


The number one cause for the death of architects is the Messiah complex.
67. faizzohri
6.1.10 / 10pm


window cleaner
69. [Chris Teeter](http://www.metamechanics.com)
6.2.10 / 1am


This may be you if you do not successfully complete your 16 hour power swing suspended course in accordance with N.Y.C. building Department rules and regulations Rigger 9-03.
71. [Paul Anvar](http://anvararchitects.com)
6.2.10 / 8pm


Bungie jumping gone awry
73. wave
6.2.10 / 9pm


Dancing into the void
75. quadruspenseroso
6.3.10 / 12am


uoYevoLeWsuebbeL
77. A. Tehrani
6.3.10 / 2am


Many Atoms.
79. Jacob A. Bennett
6.3.10 / 3pm


swinging the high-rise signature line
81. daniela
6.4.10 / 3pm


flying upside down,  

hanging by a signature  

above the city. it's my job
83. Joshua Nason
6.5.10 / 3pm


Of all the factors influencing the growth and evolution of the city it is the diversity of human influences that leave the most indelible marks of change upon it. Although buildings, roadways, geographic and contextual factors have significant impacts upon the change and image of the city, they do not define it. In fact, the rapidity and often volatility with which inhabitants of and visitors to that city interact with it make the most significant contribution to its definition as a place.
85. [Oliver](http://dronelab.net)
6.7.10 / 8pm


“Ritual dancing is also exerted at occasions in life that require the aid of magic.”


Random band, random page, random line from my 1982 Swedish Bra Böckers encyclopedia.
87. G MOON
6.8.10 / 10am


a leap of faith, in the face of the linear city
89. LOW
6.8.10 / 7pm


They said “jumped”, and she always took it too high.





	1. LOW
	6.8.10 / 7pm
	
	
	jump*
91. joshua nason
6.13.10 / 6am


The psychogymnastic map (a.k.a. “documenting the vertical derive”)
93. Jackson
6.18.10 / 4am


Mind your head
95. el.Pedro
6.18.10 / 7pm


Lucy in the sky with drawings
97. Geraldine
6.21.10 / 8pm


Skipping Rope
99. Geraldine
6.21.10 / 8pm


Sorry, that's a UK way of saying ‘Jump Rope' – I like either
101. Geraldine
6.21.10 / 8pm


heehee… or “She was in over her head”





	1. [Sean](http://none)
	7.24.10 / 2pm
	
	
	Personally, I like this. I'm a sucker for one-liners.
103. Rios
6.27.10 / 2am


Kamikaze 9-11 Splooge
105. Rob
6.30.10 / 2pm


What's with the green roof?
107. Dave
7.9.10 / 10pm


she thought to herself ‘im confused'
109. [km](http://www.kuuworld.com)
7.19.10 / 1pm


your scribble sets you free
111. Ale
7.25.10 / 4am


riddle rides me like a know what I'm doing ha
113. martin
8.2.10 / 9pm


the hip way to commute!
115. [EIGHT DIAGRAMS OF THE FUTURE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/08/eight-diagrams-of-the-future/)
8.8.10 / 2pm


[…] DIAGRAMS OF THE FUTURE Having seen the many creative responses to the WHAT IS THIS? post, I've decided to take things to another level—to up the ante, as they say—with […]
117. Mark
8.9.10 / 8pm


Simply, this is how I want a city to make me feel.
119. [Diego](http://opacos.wordpress.com/)
11.30.10 / 7pm


after seven months of judicious analysis, I propose a caption:


couples: amoral isolation
