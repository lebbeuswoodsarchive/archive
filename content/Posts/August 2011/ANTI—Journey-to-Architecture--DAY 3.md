
---
title: ANTI—Journey to Architecture  DAY 3
date: 2011-08-23 00:00:00
---

# ANTI—Journey to Architecture: DAY 3


## **Day 3—October 23**


When Raimund and I met early in the small breakfast room of the hotel, we did not speak about the preceding night's events, focusing instead on the day ahead. We were finally going to La Tourette, and expected to be there by afternoon. Before leaving—our car was parked in the narrow hotel courtyard—Raimund said that he wanted to take a walk through the nearby streets and make some photographs. What interested him most were the fronts and windows of shops, which he found both mysterious and playful, and in any event more interesting and worthy of notice than the church on the hill or any other official attractions. He chuckled as he said that these were the real monuments of culture. We walked and he made his photographs and finally we stopped for coffee at a café rather crowded with people on their way to work. While we sat and sipped our bitter espressos, two vaguely attractive women entered, speaking loudly in French, obviously drunk. Either they were fortifying themselves for the workday ahead, or were finishing a long night's work. Spotting Raimund and I, they came to our table in what can only be called a solicitation mode, perhaps because they had already attracted our attention. Raimund laughed and joked with them in German and English, which the boldest one pretended to understand. The encounter ended abruptly when the proprietor brusquely chased them out.


What was to become the most eventful and stressful day of our journey began in beautifully clear weather, as we set off in the direction of Lyon, several hundred kilometers away. It was then that we discussed for the first time where La Tourette might actually be. Neither of us had any precise idea. Raimund believed it was east of Lyon, near the Saint-Exupéry Airport—so we studied our detailed road map and discovered a town named La Tour de Pin, east of the city, and that—because of the “Tour”—became our destination. We drove hard, in order to get there by evening. Perhaps we might arrive at the monastery early enough to begin our discussions and spend the night there, or nearby. The hard driving took its toll on the rented car's transmission so that, just as we arrived in the small town, the car moved forward reluctantly and in third gear only. Parking it on the street near the town center, we left the car to ask for directions to La Tourette. And here was our first bit of luck that day—a tourist office still open, inside of which a pleasant woman who spoke some English, responded to our inquiry by opening her computer and locating La Tourette. It was, of course, nowhere near La Tour de la Pin. Rather, it was to the west of Lyon some forty kilometers, near a town called L'Abresle. Back at the car, we decided that the only choice we had was to try to drive into Lyon, find a hotel for the night, and from there call the car rental company and demand a new car the next morning. Setting off slowly on the highway, Raimund miraculously managed to keep the car moving through rush-hour traffic until we reached the center of Lyon, where the noble machine, just at twilight, finally refused to move at all.


While Raimund used his cellphone to call the car rental company, I left the car and walked into the city square nearby to find a taxi. Seeing no taxi stands, I went into a bar just opening its doors for the evening and asked, in English, if the bartender could call us a taxi. Happily, the bartender understood—France is a country that prides itself on its loyalty to the French language and no other—and, within ten minutes, the taxi arrived. Here was our second but not last bit of good luck on that day: the driver spoke English. When I explained that we needed to pick up my colleague at the broken down car, collect our luggage, then find a suitable hotel, he—as cheerfully as his droll Lyonaise temperament would allow—agreed.


Picking up Raimund and the luggage, we set off to find a good hotel for the night. After a few stops and inquiries we discovered that all the hotels were fully booked—there was a big business convention in town. Our driver even took us to some dingy two-star hotels and spoke to the managers—all booked. It was getting late and beginning to look like we would at best spend the night on park benches, huddled in the chill with our bags. Finally, a last resort, we stopped at a five-star hotel and, with our helpful driver, went in to the reception desk. In his cool but persistent style, he cajoled the smartly dressed clerks to make some calls—no luck—then go online, and behold!—there were two rooms in a new five-star hotel back across the river. Flashing a credit card, we were assured they would be held for us. The hotel turned out to be one designed by Renzo Piano, and was the target of many remarks by Raimund, who was nevertheless glad to have a comfortable place to recover from our misadventures. Staying there, not so terribly far from our destination, we had never felt farther away.


**LW**


*All photos below are by Raimund Abraham:*


[![](media/lyon-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-3.jpg)


(above) Trusting in our taxi driver, we went from one hotel to the next. Even the two-star hotels were fully booked.


[![](media/lyon-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-7.jpg)


Finally, I suggested that we go to a top five-star hotel, where a well-trained staff might be more willing to help than the indifferent clerks at shabby hotels. At that point in the late evening, the probability that we would spend the cold night on park benches loomed large. But…the strategy worked. With the help of our driver and his cool, unpushy manner. A five-star hotel, just opened on the other side of the river, just happened to have two rooms. Voila!


[![](media/lyon-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-1.jpg)


Our refuge was not exactly the sort of place we had imagined we'd been staying in when we started our journey, but that night we were glad to see it.


[![](media/lyon-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-2.jpg)


Raimund took a rather perverse pleasure in documenting what he saw as evidence of the celebrated architect's commercialism.


[![](media/lyon-61.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-61.jpg)


A five-star view of Lyon.


[![](media/lyon-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lyon-5.jpg)


This statue in the five-star courtyard has an uncanny resemblance to a tipsy young V. I. Lenin working his revolutionary network.


*To be continued. Next—La Tourette.*



 ## Comments 
1. [Gio](http://gravatar.com/gb427)
8.25.11 / 2am


This is fun.  

Never crossed my mind to document front window shops as a monument of our culture,very clever indeed.


Thanks.
3. [metamechanics](http://metamechanics.wordpress.com)
8.26.11 / 12am


Make photos. (Nice)


English speaking people take photos.


I think the German makes more sense, no?
5. Caleb Crawford
8.29.11 / 1pm


I'm loving these posts. I'm curious, though: what did you two talk about in the car? Your goal was to discuss architecture at one of the great temples of architecture, but it seems that the journey is as important as the destination. 


I find it so amusing that two of the most profound thinkers I have had the pleasure to know didn't really know where they were going. There's a metaphor in there somewhere.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.29.11 / 9pm
	
	
	Caleb: It's a good question about our conversation during the hours of driving. The answer is “architecture” of course, and also architects, to a lesser extent. I did record some of that, but I had a not-great tape recorder that picked up all the road noise. Also, Raimund made a sharp distinction between ‘conversation' and what he was really interested in, ‘discourse.' The latter is a more formal, more demanding thing. One of the differences between us was that Raimund was a European and, even more to the point, an Austrian. Discourse is what serious people have; conversation is just passing the time. But I, being an American, found conversation just as serious. I'll say more about that in the next installment from La Tourette. There were several times when I was recording our conversation that Raimund insisted I turn the recorder off—“that's just bullshit,” he would say.  
	
	I also find it odd that neither of us had bothered to find out exactly where the monastery was and how to get there. There are many possible explanations that are quite obvious and maybe someday I'll sort them out, just for fun.
