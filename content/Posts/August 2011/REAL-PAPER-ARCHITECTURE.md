
---
title: REAL PAPER ARCHITECTURE
date: 2011-08-17 00:00:00 
tags: 
    - architectural_models
    - Old_Prague
    - paper_cut-outs
    - pattern_books
---

# REAL PAPER ARCHITECTURE


## *Breaking News*—- REBEL FORCES ENTER TRIPOLI. Qaddafi's brutal reign may be nearing its end. <http://www.nytimes.com/2011/08/22/world/africa/22libya.html?hp> . For previous posts on this blog, see <https://lebbeuswoods.wordpress.com/2011/02/27/solidarity-updated-226/>


 


## .


[![](media/prague-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-1-2.jpg)


When rummaging though her extensive library, Aleksandra Wagner discovered a pattern book of the old city of Prague. Similar to a dressmaker's pattern book, it is meant to be cut up, very precisely, and assembled into a three-dimensional paper model. Designed and printed in the early 1970s, in Czechoslovakia, it is remarkable in several ways:


First: in that pre-computer era, all measurements of the actual architecture had to be made by hand—a formidable task in itself.


Second: the patterns of the scale buildings had to be calculated and drawn by hand. In engineering school, these patterns are called *developments,* and must take into account the actual dimensions of walls, roofs, and all other architectural surfaces, and therefore are not simply orthographic projections of plans and elevations of the buildings.


Third: the several pages of written instructions on the model's assembly would seem to have been as difficult to compose and they will be to follow.


Fourth: in that pre-laser cutting era, the authors had a very high regard for a modelmaker's manual skills, not to mention surplus of time and determination.


It is also worth mentioning that the book's pattern pages have the visual allure of abstract paintings, enhanced by the knowledge that the shapes were arranged not for their composition but for efficient use of the paper they are printed on. Perhaps they can be thought of as ‘functional' art. If so, then they have more relationship to architecture than the representations that can be made from them.


LW


[![](media/prague-1a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-1a-2.jpg)


[![](media/prague-2-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-2-2.jpg)


[![](media/prague-4-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-4-2.jpg)


[![](media/prague-5-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-5-21.jpg)


[![](media/prague-6-22.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-6-22.jpg)


[![](media/prague-8-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-8-2.jpg)


[![](media/prague-9-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-9-2.jpg)


[![](media/prague-10-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/prague-10-2.jpg)


[![](media/pap-arch-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/pap-arch-2.jpg)


#architectural_models #Old_Prague #paper_cut-outs #pattern_books
 ## Comments 
1. birdseeding
8.17.11 / 11pm


Not sure if it was this exact model, but I remember building a paper replica of the castle area of Prague with my parents as a kid. It took a while – like a week of evenings – but it's actually pretty forgiving and a lot of fun. Paper can be bent and patched and stuff quite easily.
3. [Building blocks | things magazine](http://www.thingsmagazine.net/?p=4897)
8.18.11 / 10am


[…] paper models of Prague, for those with infinite patience. Found and blogged by architect Lebbeus Woods (linked via […]
5. golittlebook
8.18.11 / 2pm


would love to see scans of all the pages so we can build this!
7. Hal
8.18.11 / 9pm


Vskovsky just recently passed away. It's worth a look at other models he created. 


<http://www.ceskatelevize.cz/ct24/kultura/70218-richard-vyskovsky-vytvoril-vlastni-kralovstvi/>
9. [chris teeter](http://www.metamechanics.com)
8.19.11 / 1am


Very nice. Note: all things in computer were once done manually. If anyone needs a quick template to model prague ask Lebbeus for book (scan, trace, extrude, rotate, snap) 


Lebbeus check out a PBS documentary on Origami, can't remember title, but interesting stuff.
11. tom
8.19.11 / 2am


The style of 60's Central European graphics (at the top of the box) always convey a strange forelorn Soviet Bloc(k)-ed-ness. They're just geometric figures, but there's a sad pathos to them. Maybe there's a lost Jiri Trinka film of the geo woman, at the top of the box, walking around cardboard Prague.
13. Andrew McCauley
8.19.11 / 3pm


As a creator of card models myself, and interest in preserving peoples copyright, you can go to this web site, based in England, and see this model and more that are for purchase.


<http://www.marcle.co.uk/cataloguehome.html>


The Prague model is on page 55, so scroll down to near the end, there are 39 other card models on this one page alone of various landmarks of the Czech Republic.


Or this site ( <http://www.moduni.de/cards/> – German) for hundreds more card models. 


PS: each sheet can almost be considered a work of art unto itself, as the different parts are arranged on the sheet for reference to other parts, so there is a rhythm and rhyme and progress for each model as it is being assembled.
15. [Real Paper Architecture « A Right Dog's Dinner](http://andrewjohnberry.wordpress.com/2011/08/19/real-paper-architecture/)
8.19.11 / 7pm


[…] looks interesting : <https://lebbeuswoods.wordpress.com/2011/08/17/real-paper-architecture/> Share this:TwitterFacebookLike this:LikeBe the first to like this […]
17. ilze
8.21.11 / 12am


I love the colour of each roof. The greens, blues and reds are so beautiful.
19. Caleb Crawford
8.22.11 / 12pm


These are indeed quite wonderful. I love the pink shading on the instructions. The bizarrely irregular forms of many of the buildings of the old city and the topography make quite a challenge to the draftsman.


I never learned about developments in school. Puzzled by the problem of a botched model of intersecting gables, I set about the problem of figuring out how to draw the true shape. It wasn't until I started teaching drawing that I learned about “descriptive geometry” and have since acquired several books. One volume from the 50s is especially nice. I gave problems of intersections and developments combined, which resulted in fantastic objects (I still have some that annually adorn our Christmas tree). 


I am not entirely a Luddite wed to the old practices, but there is a deeper understanding of the construction when one has to reason-out the facets. I feel the same regarding shade and shadow – it gives the designer an understanding of how light passes and is blocked by form. Contemporary digital practices can do these things for us (Rhino's “unfold” command, for instance, and the rendering engines available). Parametric modeling leads to incredible complexity of form, and almost the possibility of the “denial of authorship” sought by deconstruction, in particular Eisenman's practices. Yet for all that, Eisenman didn't leave all to the operation – his eye was present, and his deep understanding developed by years of questioning the analog process of constructing drawings. Drawing happens in the mind and is then tested on paper.
21. [Earlier on Architizer Blog « Automático](http://lazzuri.wordpress.com/2011/09/16/earlier-on-architizer-blog/)
9.16.11 / 7pm


[…] architect, artist, and avid blogger Lebbeus Woods recently came across a 1970s pattern book of the old city of Prague. Comparing it to a […]
23. [Marvels of Paper Engineering | Hartanah.Org](http://hartanah.org/marvels-of-paper-engineering)
9.16.11 / 9pm


[…] architect, artist, and avid blogger Lebbeus Woods recently came across a 1970s pattern book of the old city of Prague. Comparing it to a […]
25. [“Real Paper Architecture” « UNIT FOUR](http://unitfour.wordpress.com/2011/10/07/real-paper-architecture/)
10.7.11 / 1pm


[…] <https://lebbeuswoods.wordpress.com/2011/08/17/real-paper-architecture/> […]
27. Mark Morris
3.29.12 / 3pm


How did I miss this entry before? On a rather bleak day, in a rather bleak mood, I find these stunningly beautiful pages of flat Prague and I cheer up. Just the thought of having the time and skill to follow those instructions makes one swoon. I am always telling students of the power of ephemera research. This is a fine example of how such a find could launch so many conceptual ships. Thanks.
29. [The Astonishing Post](http://theastonishingpost.com/?p=21613)
6.27.12 / 7pm


[…] REAL PAPER ARCHITECTURE « LEBBEUS WOODS) (Source: […]
