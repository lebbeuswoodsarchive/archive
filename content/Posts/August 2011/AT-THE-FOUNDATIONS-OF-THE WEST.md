
---
title: AT THE FOUNDATIONS OF THE WEST
date: 2011-08-14 00:00:00 
tags: 
    - Greek_protests_2011
    - Mycenae
    - Sotirios_Kotoulas
    - Tiryns
---

# AT THE FOUNDATIONS OF THE WEST


[![](media/greek-riot-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/greek-riot-1.jpg)


*(above) Greek workers and police clash in Athens during protests against European Union austerity measures, which cut jobs, wages, and government programs such as health care. Photo by Associated Press.*


## *Text and photos by SOTIRIOS KOTOULAS:*


I was in Greece recently and began my stay by landing in the midst of massive nationwide protests and a paralyzed Athens that allowed no car traffic in its center. I needed to get to my friend's place in Plaka, a few blocks from Syntagma square, epicenter of all the anti-government anti-EU anti-bank demonstrations. So, wandering around on foot with my bag, I became immersed in the drama, passing raging battles everywhere, people beating cops in the head with 2×4's, explosions, citizens tearing down marble facades to create projectile weapons, all under massive, toxic clouds of tear gas. Athens was gripped by another of its crazy modern medieval moments. My friends were getting married that evening and the wedding festivities carried on wildly and uninterrupted by the upheaval for several days, deep in the Cycladic islands. When I returned to the mainland, feeling a strong need to re-experience an older period of my bloodline, I visited the works of the Mycenaean warlords and the Cyclops in Mycenae and Tiryns.


Mycenae sits on a hill tucked between two mountains traditionally admired as the breasts of the earth goddess. Driving to the site, I imagined what it would be like to approach this Palace on foot. Which way would the original paths have led? When would the conical breasts have revealed themselves to a new arrival? How could I slip in between them with my army, march in, kill and take over? This 14th BCE structure housed the king – Anax – his scribes, artists, priests, lovers, and warriors. It is strategically located on a high elevation triangular plate that skirts the plain of Argos, a position that gave the king (Agamemnon being one) and his warriors an uninterrupted vantage point of the Argolikos gulf, a convenient entrance for foreign invaders. This was in the pre-phonetic Greek period, where written language took the form of hieroglyphs: Linear B. Walking away from the parking lot and up towards the Lion gate, the solid masonry acropolis rises. The stones are huge. The Cyclops were one-eyed giants from Thrace with impeccable craft who made thunderbolts and built these walls and structures with enormous blocks of stone. Their mega works instill terror and awe. Walking between the heavy, angled stone walls towards the gate I felt a threatening unease take over. It was a delicate moment. The gate is crowned with a column shaft flanked by lions. The column may represent the earth goddess who stands on guard; I honor her. This threshold frames Mt. Sara, another formation of the goddess herself. I passed the fine picture of her that it creates then veer right towards the circular cemetery.


The royal corpses stand vertically in the round. I wanted to walk into their endless dance, so I made a move to enter the roped-off area, but was held back by a scrupulous security guard. Cemeteries fascinate me. They are loaded memory theaters and I will reinvent them. As I rose in elevation, the wind picked up and thunderstorms drew a black streak across the valley. The plan of the palace subtly rises from the ground; the eroded columns and walls barely break off the horizontal ground plane, creating a rich iconographic surface. I imagine the extrusions, the sections these elements could form. I make up a totally diaphanous ghost palace and it is really good. At this site of pleasurable horror, warlord thrones and awesome construction I kept thinking of you, Lebbeus, your work and what an appropriate place this would be for one of your radical reconstructions.


[![](media/mycenae_aerial_1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_aerial_1a.jpg)


*(above) Aerial view of Mycenae today.*


[![](media/mycenae_entrance-a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_entrance-a.jpg)


*The entrance to Mycenae.*


[![](media/mycenae_sotirios-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_sotirios-3.jpg)


*The Lions' Gate at the entrance to Mycenae. Sotirios Kotoulas.*


[![](media/mycenae_sotirios-b1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_sotirios-b1.jpg)


*One of the lions at the Lions' Gate to Mycenae.*


[![](media/mycenae_cemetery_2a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_cemetery_2a.jpg)


*The cemetery at Mycenae.*


[![](media/mycenae_cistern-a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/mycenae_cistern-a.jpg)


*Cyclopean walls: the cistern at Mycenae.*


After Mycenae, we drove to Tiryns, which is about 30 minutes away and which floats in the middle of the valley plain. The acropolis was positioned much higher a few thousand years ago, but floods and rivers slowly filled the valley with silt and earth, burying much of the rock outcrop. The entrance to Tiryns is quite long and narrow, flanked by massive 8-meter thick stone walls capped with defense passages that allowed the army to shoot arrows down at enemies. The site was empty on the day of my visit. The twin brothers who founded Tiryns, Proitos and Akrisios, fought constantly, even as embryos in their mother's womb. As adults they fought for the kingdom, and their long struggle for power ended in a draw: Akrisios won and exiled his brother Proitos from Tyrins, but after some time strategizing abroad, Proitos came back with strength and took over. He rebuilt the acropolis with the help of the giant master craftsmen, the Cyclops. The walls are mysterious constructions. The stones are 4 to 5 times larger than the largest pieces of marble on the Parthenon. They are held together with no space between them and are not bonded with mortar.


Proitos' problems mounted during his tenure as king. His daughters, Iphinoe, Iphianassa and Lysippe, were possessed by madness from a young age because they did not participate in Dionysian rituals and made fun of the wooden cult figure (Xoana) of Hera. In their deranged mental state they wandered around Argos and Arcadia until their father managed to convince the great healer Melampous to extract the madness and purify them. Taking advantage of the situation Melampous agreed to heal the ladies for a steep price: one third of the kingdom. Not one to be played for a fool, Proitos refused this absurd offer, which drove Melampous into a furry; he used his powers to increase the daughters' insanity and, moreover induce madness in all the women of Tiryns. The women ended up killing all of their children. They then wandered randomly about the mountains. After all the women disappeared from the Palace, Proitos bent to Melampous' demand, but now the bar had been risen and Melampous wanted more land, this time for his brother Bias. To make his bid stick Melampous amassed an army of the strongest and fiercest youth of Tyrins against their own king. Without women around, the young males quickly aligned behind Melampous, who had the power to set things straight. Iphinoe died and the other two daughters married Melampous and Bias, and thus the royal line shifted from Proitos to his enemies.


Perseus was king of Tiryns and the demigod Herakles, great grandson of Perseus, served time in Tiryns after his bout of madness, which saw him kill his own children. Herakles also built a temple to the martial god Enyalios in Tiryns. I walked throughout the three datums of the acropolis. The megaron, with its throne and hearth, are located at the highest level. At the rear of the palace, below a small ridge within the thick exterior wall and behind a barricade, there is a long gallery with rooms off to the sides. I needed to totally experience this space. So I jumped the fence and entered this ancient passage. This is where I finally understood the masonry. Instead of mortar, the irregular stones are entwined into a stone weave. This weave folds over the corridor, veils the light, transfers the loads and envelopes the spaces. This elegant method of construction has been so successful that the site has stood for over 3300 years. I wish we could inhabit these structures and not merely visit them. I am not interested in short lifespan architecture. The best architecture will rise up and engage future millennia. After more time on the beaches of Pelion I returned to Athens and joined my friends on a two-day journey of pool parties where we escaped a lingering cloud of fresh tear gas and passionately discussed the future of our homeland.


**[![](media/tiryns_aerial_1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/tiryns_aerial_1a.jpg)**


*An aerial view of Tiryns today.*


[![](media/tiryns_gate-a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/tiryns_gate-a.jpg)


*The entry gate to Tiryns.*


[![](media/tiryns_gallery_2a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/tiryns_gallery_2a.jpg)


*An entry to the Gallery at Tiryns.*


[![](media/tiryns_gallery_1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/tiryns_gallery_1a.jpg)


*Cyclopean walls: the Gallery at Tiryns.*


[![](media/tiryns_wall-a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/tiryns_wall-a.jpg)


*Cyclopean walls: Tiryns' fortress.*


**SOTIRIOS KOTOULAS**


*For an in-depth critical look into the current Greek crisis, check out the collection of stories at* *<http://theprism.tv/index8.php>* *a project by Nikos Katsaounis and Nina-Maria Paschalidou.*


.


**NOTE the objection (in the Comments section below) to using the opening photo above to characterize the current Greek protests. I suggest that readers consider the commenter's remarks and follow the links offered.**


LW


#Greek_protests_2011 #Mycenae #Sotirios_Kotoulas #Tiryns
 ## Comments 
1. [designlevelzero](http://designlevelzero.wordpress.com)
8.14.11 / 9pm


Dear Mr. Woods,  

During the last few months I have been a very constant reader of your blog. I have always been fascinated by your posts as well as the articles you feature occasionally. Your writings have been a great influence to me and are a common topic in the discussions with my fellow students. 


However, I have to admit that this post caught me by surprise! When I've first saw the picture on top I was shocked! As a greek citizen, I am very sensitive in this kind of situations and I am deeply sorry that you have chosen a rather controversial and misleading photo to describe the current greek crisis… Even though events like this do happen, lately the majority of the protests have been peaceful. It was the police that unreasonably used brut force to suspended them…


Below I have attached several videos of the protest on 29/06/2011 recorded by protesters then uploaded on youtube and shared through social media.


(I am deeply sorry that the description of the videos are in greek as well as the lack of translation)


Video #1: At the beginning, the protestant shouts ” Μην με βαρέσεις” which means “Don't hit me”… He seems far from being lucky.


 


Video #2: Protesters are dancing in Syntagma square when they are attacked unreasonably by police using tear gases



Video #3: The protesters are running in the metro station in order to protect themselfs. Another protester is hit while lying down on the ground



Having read your posts about Ai Weiwei's arrest as well as the reason of removal of the first “Report from Beijing”, I know that you are a sensitive man when it comes in matters of suppression. 


I hope that my lack of knowledge in english language didn't make this comment offensive. 


With best regards,  

Tsakalakis Alexis  

3rd year architecture student  

University of Patras,Greece





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.16.11 / 10pm
	
	
	designlevelzero: When i titled this post “At the Foundations of the West,” I had two things in mind. First the two ancient sites that are proto-urban settlements and cultures, but, secondly, the contemporary political protests that—violent or not—testify to democratic processes invented in ancient Greece that remain a Western ideal today. Peaceful processes are of course preferable, yet are not always possible in order for people to effect changes in their government.
	
	
	
	
	
		1. [designlevelzero](http://designlevelzero.wordpress.com)
		8.17.11 / 12am
		
		
		I feel like I failed to make my point in my comment. When i decided to write this comment, it wasn't in order to point out if peaceful processes are preferable while others aren't . Actually, I couldn't agree more in what you have mentioned in your reply. However, events like the one depicted can be interpreted in several ways according to peoples' beliefs and interests. One can say that they are workers and students that fight for their rights. While others see a bunch of anarchists. Pictures like the one above are for example shown in the news or the newspapers when the goverment demands further suppression and they desire their voters' acceptance! What I really wanted to point out is our goverment's brutal stance to demonstrations even when they are peaceful!
3. sotirios
8.20.11 / 8am


Censorship erodes common trust and dismantles the mechanics of democracy. Shrouding the shameful elements of a society breeds misleading illusions of identity and social responsibility, and equivalent retaliations only result in self-proclaimed victories.  

Mycenae and Tiryns were pre-democratic societies ruled by Kings Warriors and Tyrants a millennium before the Athenian invention of democracy. The Mycenaean rulers tightly controlled the access and use of language and signs whereas the Athenians released them into the public domain. Emancipated language expanded the public theater to further transmit myth and oral history through drama and tragedy to the city. Citizens articulated language with persuasive oratory at the Pnyx, or the Bouleuterion in the Agora. The institutions that form the bedrock of a democratic system subsist on the citizens' transparent defense and cultivation of the common good.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.20.11 / 12pm
	
	
	sotirios: Thank you for this clarification.
5. der flaneur
8.25.11 / 12am


In the spirit of overthrowing totalitarianism, the following article seems to be of interest:  

<http://www.dailykos.com/story/2011/08/01/1001662/-Icelands-On-going-Revolution>
