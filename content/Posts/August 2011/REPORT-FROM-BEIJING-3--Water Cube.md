
---
title: REPORT FROM BEIJING 3  Water Cube
date: 2011-08-26 00:00:00 
tags: 
    - architecture
    - Beijing
    - Water_Cube
---

# REPORT FROM BEIJING 3: Water Cube


[![](media/wc-01.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-01.jpg)


*(above) [The Water Cube, National Aquatics Center](http://en.wikipedia.org/wiki/Beijing_National_Aquatics_Center), Beijing, China (2008) with the “Bird's Nest' stadium behind. Photo via Wikipedia.*


*In July 2003, the Water Cube design was chosen from 10 proposals in an international architectural competition for the aquatic center project. The Water Cube was specially designed and built by a consortium made up of PTW Architects (an Australian architecture firm), Arup international engineering group, CSCEC (China State Construction Engineering Corporation), and CCDI (China Construction Design International) of Shanghai. The Water Cube's design was initiated by a team effort: the Chinese partners felt a square was more symbolic to Chinese culture and its relationship to the Bird's Nest stadium, while the Sydney based partners came up with the idea of covering the ‘cube' with bubbles, symbolising water. Contextually the cube symbolises earth whilst the circle (represented by the stadium) represents heaven. Hence symbolically the water cube references Chinese symbolic architecture.*


*Comprising a steel space frame, it is the largest [ETFE](http://en.wikipedia.org/wiki/ETFE) clad structure in the world with over 100,000 m² of ETFE pillows that are only 0.2 mm (1/125 of an inch) in total thickness. The ETFE cladding allows more light and heat penetration than traditional glass, resulting in a 30% decrease in energy costs.*


*The outer wall is based on the Weaire–Phelan structure, a structure devised from the natural formation of bubbles in soap lather. The complex Weaire–Phelan pattern was developed by slicing through bubbles in soap foam, resulting in more irregular, organic patterns than foam bubble structures proposed earlier by the scientist Kelvin. Using the Weaire–Phelan geometry, the Water Cube's exterior cladding is made of 4,000 ETFE bubbles, some as large as 9.14 metres (30.0 ft) across, with seven different sizes for the roof and 15 for the walls.*


*The structure had a capacity of 17,000 during the games that is being reduced to 7,000. It also has a total land surface of 65,000 square meters and will cover a total of 32,000 square metres (7.9 acres). Although called the Water Cube, the aquatic center is really a rectangular box (cuboid), 178 metres (584 ft) square and 31 metres (102 ft) high.*


 from [Wikipedia](http://en.wikipedia.org/wiki/Beijing_National_Aquatics_Center)


*Text and photos below by Cheng Feng Lau:*


The irony about this project is that, although the plastic, [ETFE](http://en.wikipedia.org/wiki/ETFE) ,is supposed to be a type of self-cleaning material, the project looks incredibly dirty. Perhaps the material manufacturers underestimated the air pollution in Beijing? Certain parts of the building, inside the Voronoi structure for instance, seemed impossible to clean, yet the accumulation of dust is rather obvious.


It is  fascinating that to fix a broken “bubble,” one would simply stick a slice of special tape over the hole on the ETFE. This is such an easy fix that it sounds almost a little cheap. I wonder what this project my look like in 20 years? Will it be covered with band-aids?


I enjoy the fact that there are a lot more activity going on inside the Water Cube compared with the Bird's Nest across the street. Inside the Bird's Nest, there are only two things to do. One is to check out the architecture. The other is to enjoy the event going on inside, either sports or concerts. (If there are other programs within the Bird's Nest that I didn't notice, I think most people would not know they are there). Architecture aside, the water park is a major attraction in the Water Cube that keeps the building active and occupied. Restaurants, gift shops, museums are really secondary attractions.


One last little complaint: the renderings of Water Cube were so beautiful that disappointments were inevitable when comparing them with the actual project. It is unfortunate that most architecture publications lie. They do not show what an ordinary visitor actually sees.


.


[![](media/wc-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-1.jpg)


m


[![](media/wc-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-2.jpg)


.


[![](media/wc-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-3.jpg)


.


[![](media/wc-41.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-41.jpg)


.


[![](media/wc-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-5.jpg)


.


[![](media/wc-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-8.jpg)


.


[![](media/wc-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-7.jpg)


.


[![](media/wc-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-6.jpg)


.


[![](media/wc-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-9.jpg)


.


[![](media/wc-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/wc-10.jpg)


**CFL**


#architecture #Beijing #Water_Cube
 ## Comments 
1. Pedro Esteban
8.26.11 / 5pm


I will call it, disposable architecture… the waste stay
3. [Francisco Vasconcelos](http://www.facebook.com/csxlab)
8.26.11 / 6pm


The last statement is self-explaining:


“One last little complaint: the renderings of Water Cube were so beautiful that disappointments were inevitable when comparing them with the actual project. It is unfortunate that most architecture publications lie. They do not show what an ordinary visitor actually sees.”


We live in a world of image.
5. [metamechanics](http://metamechanics.wordpress.com)
8.26.11 / 11pm


Chinese architecture is starting to sound like the new york city developer special. Tons of money on eye candy renderings (sometimes more than on the designer), cheap as hell barely approvable construction, looks good for a year or two and then pretty much crap.


But…the projects being discussed above are experiments, no developer in new york does anything remotely close. 


So this chinese architecture is kind of forgivable, this pavilion architecture…these aren't buildings they are experiments. 


Experimental architecture is the best architecture one can do as an architect if you ask the magazines and academia.


Who needs master builders right?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.26.11 / 11pm
	
	
	metamechanics: You offer some very juicy bait, but I ain't taking it. Let me ask you this: If it looks like a duck, walks like a duck, quacks like a duck, then it's…….an experiment?
7. [metamechanics](http://metamechanics.wordpress.com)
8.27.11 / 5am


-trojan ducks.


-this is a partialy induced Chimay response, not a Coors Light wide mouth blue lable tells me the beer is cold gimmick in a aluminum bottle (?) to get you to drink colored mountain water. 


Composition Qoutes:  

—-  

“Real architecture is, then, architecture in which the quality of realness is paramount. And here, with realness, is how the idea of reality can best enter the realm of architectural discourse. Like “proportion” or “scale,” like any number of qualities ascribable to architecture good and bad, “realness” becomes an attribute of buildings that can be pointed out and discussed, can be found lacking here, present in greater degree there…and so on; in short, realness becomes an observable quality amenable to some level of conceptual formulation” (1)  

—-  

“It then became clear to me that it was not the task of architecture to invent form. I tried to understand what that task was….Truth is the significance of fact…The structure is the whole from top to bottom, to the last detail – with the same ideas.”(2)  

—-  

“Mies's work is developed not out of images but out of materials – materials in the strongest sense of the word; that is, the matter from which objects are constructed. This matter is abstract, general, geometrically cut, smooth, and polished, but it is also material that is substantial, tangible and solid.”(3)  

—-  

“Among the characteristics of physics (and probably of any other science) we find one additional feature which is rarely emphasized but which is absorbed naturally into the flesh and blood of every scientist. We mean the necessity of abstraction, that is the necessity to find and single out among innumerable relationships only those which affect the relevant phenomenon to the maximum extent.” (4)  

—-


Opinion and Conclusion:


It's pretty clear to me, by this third post of your guest China writer, that the disappoitment in this cutting edge “experimental” architecture in China is due to the lack in quality in truth of material. By material I mean more than the actual material, I also mean the design and labor that forced the ideas into creation in material.


I finalize the composition of qoutes above with a physicist's opinion on the importance of abstraction. The majority of todays experimental architectural creations, although claiming great (abstract) conceptual importance are no where close to defining the “relevant phenomenon to the maximum extent.”. 


“Realness, I think, can be divided into four components, the last one of which has two aspects: presence, significance, materiality, and emptiness (emptiness, emptiness).” (5) 


Experiments should further prove the truth and the significance of fact.


Lebbeus, does the experimental architecture of today really further the proof and discovery of truth in architecture?


References:  

(1)(5) “For an Architecture of Reality” – Michael Benedikt  

(2) “Modern Architecture: A Critical History” – Kenneth Frampton qouting Mies  

(3) ‘Differences” – Ignasi de Sola-Morales  

(4) “Electrons, Phonons, Magnons” – M. I. Kaganov





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.28.11 / 5pm
	
	
	chris: Thank you—this is much more than bait.  
	
	I, too, have been inspired by the scientific model, mainly for 1) its demand for rigorous thought; 2) its interest in clarifying complex questions and problems; and, not least 3) its spirit of collaboration among the best and the brightest to accomplish the difficult answers. These are the reasons I founded the Research Institute for Experiment Architecture (RIEA), more than twenty years ago. I cannot claim it has been a great success, but it still exists and is still hopeful of achieving these goals.
9. der flaneur
8.28.11 / 2pm


experimental space frame designed by a generic corporate firm


Eventually Chinese preservationists will replace all that ETFE cladding with something more durable
11. wwcc
9.2.11 / 6pm


why did chinese select experimental architecture for their pride national events and other important monuments? progressiveness was not a tradition in chinese culture since Song dynasty. simplicity was the tradition of their arts and architecture. has the cultural revolution changed everything or was it simply an imagination of the progressiveness? are western architects designing progressive architecture in china or simply indulging this chinese imagination of the modern world with erotic architecture?
13. Steven Chou
9.6.11 / 3pm


WWCC: Is it really just the cultural revolution that changed so many things?? I believe you have to put China into a post-colonial context. China was once a highly insulated yet extremely powerful economic back in the dynasties. So much arrogance and pride were damaged if not almost shattered by events like the two Opium Wars, the burning of YuanMingYuan…..On its way to the Republic, you see most of the revolutionaries denying the old Chinese culture: “if the old culture is what led to our defeat and the curruption within the country, then it must be destroyed. We shall learn from the west, Britian, France, Soviet, Japan….whoever (has defeated us before) can offer us the lesson….” [I quote from my memory a revolutionary in the summer 2011 film – Creation of the Coummunist Party.]


My question is: so where is Chinese culture and Chinese architectural Identity? We have not lost them, but we have been denying them. By denying what have led to those faults, we have also denied the virtues…. I see a country in identity crisis, with almost no confidence left in its traditional culture. Thus some random experts begin to compare China with other countries in strange ways: e.g. “China has surpassed US in its living standards” “China is becoming the biggest country in consuming luxury goods”…. All these B.S. came out of the mind and the mouth of people who are highly insecure about the country's status in comparing with the world. Funny that China used to be a country that would only compare to itself.  

Poeple lost the ability to judge for themselves… Why do you need all that extravagance to prove that you are better? Because there seemed to be no other way to secure that sense of supiriority. Why do you need to feel supirior than others? I do not have an answer….


The architecture field has been through several phases since the Cultural Revolution: destruction of all the old (it's practical value lies in the renewal of urban infrastructure…..), builidng new architecture in traditional styles (perhaps not in a very Post-Modernist way), thriving for the New,Strange,Rare (a.k.a. Beijing Olympic era, with short construction period….), and currently another wave of re-thinking: getting some of the old back….It is bizzare to see quite a number of architects in Beijingare asked to design architecture with traditional “touch” or large scale Chinese gardens that incoporates “north and south of China, from the new to the old; combining the eastern philosophy and the western architectural methodology….” One architect said to me: “I call this Karma. In our school days we skipped all the traditional Chinese architecture lectures, we used to consider those lectures useless….Now I have to study all that because this is the project I am getting  

…”  

What I get out of this situation, is the realization that the denial of one's culture can really lead to disasters. The denial of one's true identity is even worse…..


There is a very famous Tang Dynasty poem called “Lou Shi Ming” that almost every Chinese Junior high student are tested on in their exams.  

Here I quote its English translation from a website: <http://en.v.wenguo.com>


[An Epigraph in Praise of My Humble Home] by Liu Yuxi 


A mountain needn't be high;  

It is famous so long as there is a deity on it. 


A lake needn't be deep;  

 It has supernatural power so long as there is a dragon in it. 


My home is humble,  

But it enjoys the fame of virtue so long as I am living in it. 


The moss creeping onto the doorsteps turns them green.  

The color of the grass reflected through the bamboo curtains turns the room blue.  

Erudite scholars come in good spirits to talk with me,  

And among my guests there is no unlearned common man.  

In this humble room, I can enjoy playing my plainly decorated qin,  

or read the Buddhist Scriptures quietly,  

Without the disturbance of the noisy that jar on the ears,  

or the solemn burden of reading official documents.  

My humble home is like the thatched hut of Zhuge Liang of Nanyang,  

or the Pavilion Ziyun of Xishu.


Confucius once said: 


“How could we call a room humble as long as there is a virtuous man in it?” ////


I used to wonder is this Epigraph should be considered an antithesis to architecture, since if we all have such powerful minds, then there seems to be no need for any architectural gestures beyond a simple shelter.  

Now I am looking at some of the architecture in China, I wonder if one needs to re-assert the notion of Humble Home before thinking about anything extravagant??
15. Rhino
9.7.11 / 10pm


I would like to know how the Eden project in Cornwall, UK is holding up. It has a similar concept, structure and cladding to the Water Cube building, though, Eden is far more well done in my opinion. When compared I feel that the only experiments that the Water Cube building is conducting are in determining how long a forced aesthetic remains interesting to its audience and wheter its execution, ability to withstand natural forces, and need to provide the necessary duty of provide shelter prove its unnecessary aspects capable of anything beyond temporarilly appealing to the human imagination.
17. [Charlie 阿理](http://xuehuang800.wordpress.com)
9.26.11 / 7am


Frank criticism
19. [Paulina Wilkowska](http://www.facebook.com/paulina.phi)
11.6.11 / 3pm


I shudder to think what would happen should the cladding of the building ever burn as ETFE produces hydrofluoric acid (according to wiki) which in turn is so corrosive as to dissolve glass… curious about the fireproofing. interesting to see the interior, is pretty standard mall like atmosphere.
