
---
title: LAURETTA VINCIARELLI (1943-2011)
date: 2011-08-07 00:00:00 
tags: 
    - architectural_drawing
    - visionary_architecture
---

# LAURETTA VINCIARELLI (1943-2011)


[![](media/lv-3a2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lv-3a2.jpg)


Lauretta Vinciarelli has died, and her passing is a serious body blow to architecture. Not only have we lost one of our most rousing architectural draftsmen (sic) but also one of the rare visionaries of architectural space, Her exquisite watercolor paintings have presented to us new spaces of the almost-familiar, for example several series of what might be causeways or catacombs, that strike us as monuments of our time more convincing than the slick museums and corporate edifices that our most prominent architects favor. From a large body of work embracing many themes, it is these that I focus on here.


Vinciarelli, a daughter of Rome's environs, never lost her belief in classical order and beauty, yet at the same time had the social conscience of Rome's leftist perspectives, which elevated everyday life to a higher level of architecture and art. The largely hidden spaces she designed and painted so masterfully, often flooded with the water of uncontrolled or intentional flows, speak of unconscious worlds, devoid of people yet clearly intended for them, spaces we might guess for those who somehow sustain an unseen world beyond. They evoke the levels of emotion and thought that support a type of civil life founded on such precise and orderly infrastructure.


For all their calm and studied precision, Vinciarelli's spaces are disquieting. The harmony and grace of their austere forms and composition speak of self-possession and control, but also of melancholy. These are utopias, we might fear, too perfect for us mere mortals to endure and are thus lonely places. In her paintings, we never step back to see an overall picture, but are always within, remaining free to hope that her world of beauty and harmonious order might, in different ways, extend beyond the limits of the frame to a wider constructed landscape. Vinciarelli gives us a spatial and, indeed, an ethical foundation for such a world. Hers is an architecture of insinuation and suggestion. The dark side of her vision is not in shadows and the like, but rather in the possibility, even the likelihood, that her ideals, and perhaps all ideals, will be thwarted by the enemies of beauty, or will simply remain unfulfilled. This brings us directly to the political issues raised by her work.


The spaces she has designed and rendered do not seem to be the work of a single architect, though they are. Instead, they have the character of collective constructions, the products of a culture, even an entire civilization. In them we cannot find the self-aggrandizing, egoistic insistence on a signature style that separates their architect from others, but rather a profound modesty such as we find in Medieval Italian towns built by anonymous craftsmen and artists. Or also, we might say, in a socialist dream of free collaboration between the most dedicated and gifted of architects and builders working unselfishly together. Whether Vinciarelli ever wrote or spoke about her work in these or similar terms, I cannot say. But the collective character of her designs is nevertheless one of their most striking features.


This is not so with the watercolor paintings themselves—they are only hers. Like all great draftsmen (sic again, because I reject neutered terms like ‘draftspersons' or, even more, ‘draftswomen,' because it separates her from the great history of architectural drawing that has been made, like it or not, mostly by men) Vinciarelli invented her own techniques. This is important to recognize and consider because the way she draws is inseparable from what she draws. The texture and reflectivity of surfaces, the nuanced chiaroscuro, and especially the color that animates the spaces were painstakingly achieved by applying gossamer layers of pigment until the desired densities and luminosity were achieved. Though this technique was used by the ‘old masters,' her application of it to watercolor painting is unique. There was no way to erase or undo what had been laid down before. She could only go forward, with absolute commitment to her vision. This is what she did in her architecture, her paintings, and her life.


LW


[![](media/lv-1a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lv-1a1.jpg)


.


[![](media/lv-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/lv-2a.jpg)


.


*The above images are taken from an excellent [monograph on Lauretta Vinciarelli's work](http://www.amazon.com/Not-Architecture-Evidence-That-Exists/dp/1568981880/ref=sr_1_1?ie=UTF8&qid=1312742965&sr=8-1).*


*We can only hope that more complete editions of her work will emerge in the future.*


#architectural_drawing #visionary_architecture
 ## Comments 
1. [Ronald Bentley](http://www.bfivestudio.com)
8.7.11 / 11pm


These words about Lauretta's drawings are wonderful, and very well expressed. But along with being an inspired draftsman of an amazingly powerful architecture (and light), Lauretta was also a wonderful teacher of architecture for many years at Pratt, Columbia, and City College. It is possible to say that there were none who were not enthralled by her intelligence, elegance, and focus on what was genuine in architecture. Her husky, deeply Italian voice was cat nip to so many, but her penetrating insight always sealed the deal. She is irreplaceable, and her passing will be a loss to architecture, and to those who knew and loved her. 


Ronald Bentley
3. jose
8.7.11 / 11pm


Did not know he work. It is truly amazing!!!!
5. [campion platt](http://www.campionplatt.com)
8.8.11 / 1pm


sadly missed, a great spirit and strong soul for us. She was a great influence on me in drawing at columbia. I remember our dinners at Donald Judd's loft in soho. Great fun!
7. Michael Hays
8.8.11 / 6pm


Thanks so much for this Lebbeus.
9. Talya Nevo-Hacohen
8.8.11 / 11pm


Lauretta inspired me when I was in architecture school. She will always be remembered for her charm and intellect, her husky voice and the wonderful pasta dinners at the loft on Spring Street. She touched many in her life and she will be missed.
11. FJ
8.9.11 / 2am


Ciao, bella, ciao…
13. [The Fox Is Black » Lauretta Vinciarelli](http://thefoxisblack.com/2011/08/09/lauretta-vinciarelli/)
8.9.11 / 3pm


[…] Woods posted these kind, articulate words to commemorate the passing of Lauretta Vinciarelli. Vinciarelli was an Italian […]
15. [Lauretta Vinciarelli 1943-2011 - Beyond Buildings](http://blogger.hanleywood.com/CS/blogs/beyondbuildingsblog/archive/2011/08/08/lauretta-vinciarelli-1943-2011.aspx)
8.9.11 / 4pm


[…] Lauretta Vinciarelli, Night #6, 1996; architectural drawing; watercolor and ink on paper. Courtesy: SFMOMA. I was saddened to hear this weekend of the passing of Lauretta Vinciarelli. Lauretta created images that were elegiac: watercolors she layered in infinitely thin washes to evoke spaces filled with emptiness, grids, water, and light appearing from a source we could not see. She showed us an architecture of the imagination that we know we can never have, but that is perhaps buried in either a mythic past or deep inside buildings we have covered with all those bits and pieces that answer to the exigencies of everyday life. Vinciarelli's passing deprives us of one of the last few decades' great visionary architects.  Courtesy: Lebbeus Woods  […]
17. dzko
8.9.11 / 9pm


“In her paintings, we never step back to see an overall picture, but are always within, remaining free to hope that her world of beauty and harmonious order might, in different ways, extend beyond the limits of the frame to a wider constructed landscape.”


sigh
19. [grahame](http://gravatar.com/grahameshane1)
8.9.11 / 11pm


It is a great loss, and such a beautiful first drawing with the reflected blue of the sky beyond.  

Thank you Lebbeus for this true tribute.
21. Lisa Dubin
8.10.11 / 12pm


Lauretta had a gift as a teacher. She knew how to support and nurture a student's design and tease out it's promise. I have become the architect I am because of Lauretta's influence and inspiration when I was her student. She will remain in my heart.
23. [helen](http://www.helenfergusonvrawford)
8.10.11 / 3pm


Thank you Lebbeus. She is an inspiration to all that dream about space, paint, paper and documenting the real and possible world.
25. Charlie Anderson
8.10.11 / 4pm


I am very sad to hear this news. Reflecting now on many memories of Lauretta as an inspiring teacher and a wonderful and generous person.
27. Brooke
8.11.11 / 3am


Thank you for these thoughtful words and to commenters for sharing their memories of Lauretta.
29. Brian Kaye
8.14.11 / 10pm


I am very saddened to learn of Lauretta's untimely death; she was a wonderful teacher (Pratt) who took a genuine interest in her students and she had a profound effect on my work. I remember her invitations to Donald Judd's building in Spring St and her wonderful ‘sexy' voice. Her paintings and vision were always very clear, beautiful and inspiring. Il suo spirito continua nella mia memoria e il suo lavoro.
31. [elainaberkowitz](http://elainaberkowitz.wordpress.com/2011/08/30/51/)
8.30.11 / 3pm


[…] <https://lebbeuswoods.wordpress.com/2011/08/07/lauretta-vinciarelli-1943-2011/> […]
33. George Wilson
9.12.11 / 2am


The world is a lesser place now that Lauretta Vinciarelli is gone. I knew her only a few months as an Architecture student at Columbia College in the Spring of 1981. She was my Graphic Arts professor. I still have my drawings from that class with Lauretta where she taught us how to burnish the Berol Prismacolor on the drawings to give it varying degrees of sheen. She had a knack for drawings with perspective and how to alter it. I was taken by her joy for life and her speaking style.  

I am a better man to have known this wonderful person.
35. [Two Must-See Shows | Great Design](http://mylix.net/wpdirins/?p=734)
4.8.12 / 7am


[…] norm they become powerful. And third, Lebbeus Woods wrote a gorgeous eulogy to Vinciarelli when she died last year, strongly expressing his […]
