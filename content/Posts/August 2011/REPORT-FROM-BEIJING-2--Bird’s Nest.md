
---
title: REPORT FROM BEIJING 2  Bird’s Nest
date: 2011-08-04 00:00:00 
tags: 
    - architecture
    - Beijing
---

# REPORT FROM BEIJING 2: Bird's Nest


## **A Starchitect's Indigestion/Traveler's Diarrhea in China—*The Bird's Nest***


It is quite an enjoyable thing, in a very cynical way, to look for architectural details that should never be published in architecture magazines here in Beijing.


Some of the most “otherworldly” architecture has been realized in the past few years. Those works have been published worldwide and celebrated as great examples. Yet they suffered the pandemic of ridiculously tight deadlines and the terrible construction here in China.


It is everyone's fault. Simply blaming the local reality would be completely unfair. Those who decided to have their designs built in China knew about the situation here before they arrived. If they say they didn't, they lied. If they didn't lie, they are just naïve.


I am not trying to criticize anyone. That's not fun at all.


The fun thing here is the fact that the difficult situation within the building industry in China would require architects to work more intelligently and more responsibly. They could completely revolutionize the industry by keeping the quality of their work here in China on a high level. This is a country where one truly has to be a “superhuman/saint” to make things happen in the right way.


Many architects have been complaining that these 2008 Olympic buildings can only be seen from a distance. One can only enjoy the big idea, but not the details.


Perhaps I should correct myself at this point: I did not visit these buildings with the intention of looking for disastrous details. Those details simply jump right into one's face. It is perhaps my architectural education that has caused an extreme response from my immune system. But it would be quite a joke to say that since the Chinese have been accustomed to terrible food, our chiefs should not worry about the level of food safety. Even if an untrained eye cannot decipher some of the “disasters,” I can not help feeling embarrassed for their exposure. I am sure their architects feel the same way.


Some of the developers who worked with some our most famous and favorite starchitects really devote a lot of time to quality control and to looking for the right materials and  methods suitable for local culture. It is strange that architects have to wait for the developers to tell them what is most suitable.


Again, not trying to make fun of anyone, I am taking the matter of materials, methods, and details seriously. This is a challenge and a socially responsible architect who would like to work in China must deal with it. Perhaps I am too optimistic about how much an architect can do. But I am too young to feel otherwise. But wait—I think there's a saying that an architect should still be considered young before the age of 70. Well, let me do some subtraction….


Enough prologue.


I shall share with you some of the disasters, accompanied by some not so terrible images.


**Cheng Feng Lau**


[![](media/1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/1a.jpg)


(above) Apparently one of the most enjoyed details of the Bird's Nest…!…?


[![](media/2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/2.jpg)


[![](media/4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/4.jpg)


[![](media/5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/5.jpg)


[![](media/6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/6.jpg)


[![](media/7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/7.jpg)


(above) A concert in progress at the Bird's Nest: “Could you turn down the sound? It's really hurting my ears!”


[![](media/8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/8.jpg)


(above) “So where'd they say the elevator was?”


[![](media/9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/9.jpg)


[![](media/10-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/10.jpg)


(above) Frank Lloyd Wright once said, “Doctors bury their mistakes. Architects plant ivy.” Can we add, “…or paint them red.” [LW]


[![](media/11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/11.jpg)


[![](media/12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/07/12.jpg)


[![](media/13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/08/13.jpg)


**CFL**


#architecture #Beijing
 ## Comments 
1. Pedro Esteban
8.4.11 / 2pm


jajajajajaja  

I just need to laugh…. sorry  

I'm asking what kind of birds live in the smalls nests?  

In the big one I have an idea…  

China…Those details are so familiar to me, is the same everywhere
3. Manel
8.12.11 / 9pm


It's a dirty world. Once you leave Basel, building standards change.  

And I agree, it's also the architects responsability. No one's fault though.  

But if there are credits, then there's also criticism.
