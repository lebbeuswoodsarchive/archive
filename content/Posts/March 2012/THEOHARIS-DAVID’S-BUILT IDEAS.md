
---
title: THEOHARIS DAVID’S BUILT IDEAS
date: 2012-03-07 00:00:00 
tags: 
    - architecture
    - architecture_education
    - architecture_in_Cyprus
    - Theoharis_David
---

# THEOHARIS DAVID'S BUILT IDEAS


It was in the winter of 1988. Theo David, then the director of Pratt Institute's Graduate Design Program, had hired Michael Webb, of Archigram fame, and me, as adjunct teachers in the design studio, where he was also teaching. At the outset of our one-year tenure, the three of us had sat down and discussed the overall direction we wanted to take. Michael and I—architects who had no realized buildings to our credit—suggested that it would be great if we could focus on “science and architecture.” Theo, a gentleman through and through, asked if we meant exploring the relationships between technology and architecture, a topic worn thin over the decades but still, he supposed, always worth another look. No, we answered, it's pure science we thought would open up fresh perspectives and possibilities, its concepts and methodologies. Relativity, thermodynamics, evolution, cybernetics. Theo's eyes brightened and he smiled in his nuanced way. A great idea, indeed, he said—let's do it. And so began one of the most memorable teaching years in my experience and, I believe, that of many of our students.


*Science and Architecure* was a risky direction to take and not only because other faculty were vocally skeptical of how it would lead to buildable architecture, and how it would be taught by two non-building faculty, but also how, or if at all, it would be accepted by our students, many of whom had traveled far and were paying dearly for this conclusion to their professional education. Nevertheless, it was a risk that Theo took, because he believed in its premises and potentials. It was then that I realized what kind of man Theo is, and came to value his integrity as a friend and educator.


It was more recently—I am chagrined to say—that I discovered his architecture. The fault is mine, but was abetted by his innate modesty and the fact that many of his projects are in Cyprus. My fear that many others might share my ignorance is relieved by this exhibition and catalogue. In them, we find projects of a high order of thought and design, informed by a brave and compassionate spirit. Working with the known building types, he has transformed their conventions into inventions animated by new readings of their traditional meanings and purposes. The boldly abstract volumes of the Ayia Trias Church; the contrasts between delicacy and mass in the Bleu Residences; the soaring, sheltering lightness of the GSP Stadium; the open courtyards of the Stylianos Lenas School, playfully painted with light and shadow—all are unique interpretations of ideas vital to contemporary living that seem at the same time both familiar and, in these works of architecture, wholly original. It is their unforced, unpretentious character that gives them strength and poetry, a naturalness that imparts to us a sense of well-being and enablement.


As it is with the man, so it is with his works. Theo David is deeply committed to his students and his colleagues and to the architecture that embodies ideas celebrating their community, indeed, community itself.  In my view and from my experience, he is an architect setting for all of us an example for a better, more affirmative future.


LW


The following are several projects by Theoharis David, that appear in [the catalogue](http://www.built-ideas.info) of [the current exhibition of his works](http://www.cyprususchamber.com/2012/02/theoharis-david-built-ideas-a-life-of-teaching-learning-and-action-1969-2012/) at Pratt Institute:


*(below) BLEU Residences, 2010, Protaras, Cyprus:*


[![](media/theo-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-2.jpg)


[![](media/theo-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-3.jpg)


.


*(below) ALLEGRA GSP SPORTS CENTER, 2008, Nicosia, Cyprus:*


[![](media/theo-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-6.jpg)


[![](media/theo-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-4.jpg)


.


*(below) AYIA TRIAS CHURCH, 1972, Famagusta, Cyprus:*


[![](media/theo-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-1.jpg)


[![](media/theo-9.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-9.jpg)


.


*(below) GSP STADIUM AND ATHLETIC CENTER, 1998, Nicosia, Cyprus:*


[![](media/theo-8.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-8.jpg)


[![](media/theo-7.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/theo-7.jpg)


#architecture #architecture_education #architecture_in_Cyprus #Theoharis_David
 ## Comments 
1. le145minimalist
3.8.12 / 2am


We have all Great Ideas ( or good intentions ) but too few of us are able to materialize them. Thanks for sharing with us.
3. zale
3.10.12 / 9am


“unforced, unpretentious character” is beautiful. nice night cap and worth further looking-into. thank you for sharing
