
---
title: LOWER MANHATTAN Revisited
date: 2012-03-08 00:00:00 
tags: 
    - aerial_drawing
    - geological_evolution_of_Earth
    - Manhattan
---

# LOWER MANHATTAN Revisited


## 


*[![](media/lowermanhattan.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/lowermanhattan.jpg)*


*(above) Lower Manhattan, ink drawing by LW, 1999.*


*T**he f**ollowing is an excerpt from**an interview made by Geoff Manaugh and published on BLDGBLOG:*


**BLDGBLOG**: First, could you explain the origins of the *Lower Manhattan [see [larger size](http://www.flickr.com/photo_zoom.gne?id=1454491228&size=o&context=photostream)]* image?


**Lebbeus Woods**: This was one of those occasions when I got a request from a magazine – which is very rare. In 1999, *Abitare* was making a special issue on New York City, and they invited a number of architects – like Steven Holl, Rafael Viñoly, and, oh god, I don't recall. Tod Williams and Billie Tsien. Michael Sorkin. Myself. They invited us to make some sort of comment about New York. So I wrote a piece – probably 1000 words, 800 words – and I made the drawing.


I think the main thought I had, in speculating on the future of New York, was that, in the past, a lot of discussions had been about New York being the biggest, the greatest, the best – but that all had to do with the *size* of the city. You know, the size of the skyscrapers, the size of the culture, the population. So I commented in the article about Le Corbusier's infamous remark that *your skyscrapers are too small*. Of course, New York dwellers thought he meant, *oh, they're not* tall *enough* – but what he was referring to was that they were too small in their ground plan. His idea of the Radiant City and the Ideal City – this was in the early 30s – was based on very large footprints of buildings, separated by great distances, and, in between the buildings in his vision, were forests, parks, and so forth. But in New York everything was cramped together because the buildings occupied such a limited ground area. So Le Corbusier was totally misunderstood by New Yorkers who thought, *oh, our buildings aren't tall enough – we've got to go higher!* Of course, he wasn't interested at all in their height – more in their plan relationship. Remember, he's the guy who said, *the plan is the generator*.


So I was speculating on the future of the city and I said, well, obviously, compared to present and future cities, New York is not going to be able to compete in terms of *size* anymore. It used to be a large city, but now it's a small city compared with São Paulo, Mexico City, Kuala Lumpur, or almost any Asian city of any size. So I said maybe New York can establish a new kind of scale – and the scale I was interested in was the scale of the city to the Earth, to the planet. I made the drawing as a demonstration of the fact that Manhattan exists, with its towers and skyscrapers, because it sits on a rock – on a granite base. You can put all this weight in a very small area because Manhattan sits *on the Earth*. Let's not forget that buildings sit on the Earth.


I wanted to suggest that maybe lower Manhattan – not lower downtown, but *lower* in the sense of *below the city* – could form a new relationship with the planet. So, in the drawing, you see that the East River and the Hudson are both dammed. They're purposefully drained, as it were. The underground – or *lower Manhattan* – is revealed, and, in the drawing, there are suggestions of inhabitation in that lower region.


So it was a romantic idea – and the drawing is very conceptual in that sense.


But the exposure of the rock base, or the underground condition of the city, completely changes the scale relationship between the city and its environment. It's peeling back the surface to see what the planetary reality is. And the new scale relationship is not about huge blockbuster buildings; it's not about towers and skyscrapers. It's about the relationship of the relatively small human scratchings on the surface of the earth compared to the earth itself. I think that comes across in the drawing. It's not geologically correct, I'm sure, but the idea is there.


There are a couple of other interesting features which I'll just mention. One is that the only bridge I show is the Brooklyn Bridge. I don't show the Brooklyn-Battery Tunnel, for instance. That's just gone. And I don't show the Manhattan Bridge or the Williamsburg Bridge, which are the other two bridges on the East River. On the Hudson side, it was interesting, because I looked carefully at the drawings – which I based on an aerial photograph of Manhattan, obviously – and the World Trade Center… something's going on there. Of course, this was in 1999, and I'm not a prophet and I don't think that I have any particular telepathic or clairvoyant abilities [*laughs*], but obviously the World Trade Center has been somehow diminished, and there are things floating in the Hudson next to it. I'm not sure exactly what I had in mind – it was already several years ago – except that some kind of transformation was going to happen there.


*[end]*


## Epilogue


Once we begin to consider what lies below Manhattan, it is hard to know where to stop. The immense masses of rock on which the city rests are extremely stable, but they are moving, slowly, very slowly. The reality is that the Earth's mantle—of which the bedrock of Manhattan is a part—is floating on an underground sea of semi-liquid rock, known as the Outer Core, and its movement is a natural consequence of the planet's continuing geological evolution. Earthquakes are simply a normal phenomenon in regions of the Earth's surface where two ‘tectonic plates' meet and one slides slowly, slowly under the other. This is a process that takes tens of millions of years to make a noticeable difference in human terms. And, by then, will there be human beings as we know them today to appreciate it? Certainly not.


Yet, these epic changes-to-come affect our experience of Earth today. A year ago, an earthquake and resulting tsunami devastated the coast of Japan, as have earthquakes and the eruptions of volcanoes, not to mention related severe weather, throughout recorded history. Humans may not be able to see the mountains rise and the seas fall (as they did by hundreds of feet in the last Ice Age), only to rise again, but their knowledge that it is happening affects at the very least their understanding of the world they inhabit, and consequently their thoughts and plans for the future.


A couple of years ago, I did a bit of research in the form of drawings on the theory of tectonic plates and their presumed movements in the past, adding a few future projections of my own. This is science at its most speculative and risky, and art at its most tentative. Lower Manhattan, indeed.


LW


The boundaries of Earth's tectonic plates today:


[![](media/tec-1-plates-1a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-1-plates-1a-2.jpg)


The land masses of Earth, 225 million years ago:


[![](media/tec-comp-1a-1-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-1a-1.jpg)


200 million years ago:


[![](media/tec-comp-2a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-2a-1.jpg)


125 million years ago:


[![](media/tec-comp-3a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-3a-2.jpg)


65 million years ago:


[![](media/tec-comp-4a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-4a-1.jpg)


Today:


[![](media/tec-comp-5a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-5a-1.jpg)


40 million years from today:


[![](media/tec-comp-6a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-6a-1.jpg)


67 million years from today:


[![](media/tec-comp-7a-2-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/tec-comp-7a-2.jpg)


*Eons before the planet becomes mostly water, with the continents broken up and dissolved, Manhattan will become an island adrift on a vast, rising ocean. Though still inhabited, it will bear only slight resemblace to the city we know today. Most skyscrapers will be gone, because of their unsustainability—the few remaining will be maintained as relics of the past. Central Park will be gone, too, replaced by a great landfill on which are built cheap houses and shacks:*


[![](media/manh-4bw-3b.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/manh-4bw-3b.jpg)


LW


#aerial_drawing #geological_evolution_of_Earth #Manhattan
 ## Comments 
1. Ben Dronsick
3.8.12 / 12pm


Are you predicting, or observing the “unsustainability” of skyscrapers?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.9.12 / 2pm
	
	
	Ben Dronsick: Both. It's pretty obvious now that while skyscapers are very efficient in using expensive real estate, they are very wasteful of natural resources. For example, in the 24-7 consumption of electricity (have you ever noticed how the lights burn all night?) and the oil and coal it takes to supply it. Then there is the concentration of waste and pollution. Then there are the health and safety issues resulting from people being forced to live high above the ground. So, my prediction is not a revelation, but only a matter of extrapolating unsustainability curves. Or so I see it.
3. [parkergarden](http://parkergarden.wordpress.com)
3.9.12 / 10pm


Reblogged this on [parkergarden](http://parkergarden.wordpress.com/2012/03/09/33/).
5. Pedro Esteban
3.12.12 / 3am


Is interesting the approach to the skyscraper of course, the typology again!  

But my main question here is the usage of land, how we will use land in some years from now? Today land have become a finite source and our ”high rise friend” is not helping us to solve the problem of occupation of the horizon. We have not found a viable way to keep inserting shapes in space without occupy land, because space is measure and measure is not infinite when comes to real tectonic.  

The solution to this should be solved by architecture(tectonic) or politics(human decisions)?  

Architecture/Land is finite, but the growing population is not.
7. [Terrapol](http://www.terrapol.com)
3.15.12 / 2am


Looking at the sequece of tectonic shifts of the earth's plates you posted I can't help but feel that it captures quite neatly the human predicament. From one we came, into many we fractured, into nothing we become, swamped by the very thing which brought us into being. Sad, but its got a cyclical ring to it. Are those tectonic predictions your own Lebbeus?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.15.12 / 11am
	
	
	Terrapol: Yes, they are.  
	
	Like all cycles, the beginning and the end are the same in essence, but take different forms.
