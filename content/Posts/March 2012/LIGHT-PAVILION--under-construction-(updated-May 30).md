
---
title: LIGHT PAVILION  under construction (updated May 30)
date: 2012-03-25 00:00:00 
tags: 
    - Christoph_a__Kumpusch
    - Steven_Holl
---

# LIGHT PAVILION: under construction (updated May 30)


The Light Pavilion is finally under construction. Located within a large complex of mixed-use buildings—the *Sliced Porosity Block—CapitaLand Raffles City Chengdu, China*—designed by Steven Holl Architects, this ‘building within a building' was designed by me with the collaboration of Christoph a. Kumpusch, beginning in 2008. After struggling through many crises of budget, finding a willing and capable builder, not to mention the global ‘financial meltdown' of 2008-9, the project is now proceeding rapidly towards a construction deadline of mid-Summer, 2012. Because I thought it might be instructive or, better, fun to track its progress, this post will be updated as construction proceeds.  Refer to [this previous post](https://lebbeuswoods.wordpress.com/2011/02/15/a-space-of-light-2/) for design and construction documents.


All photos are by Manta Weihermann/Steven Holl Architects.


LW


[![](media/ltpav-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-2.jpg)


.


*(below) May, 30, 2012. Two of the structural cores of the illuminated end beams are welded into place.*


[![](media/ltpav-may-30-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-30-1.jpg)


.


[![](media/ltpav-may-30-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-30-6.jpg)


.


[![](media/ltpav-may-30-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-30-3.jpg)


.


[![](media/ltpav-may-30-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-30-5.jpg)


*Steel tubes for the secondary structural system, prepared for assembly and placement:*


[![](media/ltpav-may-30-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-30-21.jpg)


*(below) May 28, 2012. The construction scaffolding has been removed. The first of three structural cores of illuminated end beams is being raised into place. The structural steel stringers for stairs and platforms are being completed:*


[![](media/ltpav-may-28-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-28-3.jpg)


.


[![](media/ltpav-may-28-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-28-1.jpg)


.


[![](media/ltpav-may-28-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-28-2.jpg)


.


[![](media/ltpav-may-28-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-28-5.jpg)


.


[![](media/ltpav-may-28-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-28-4.jpg)


*(below) May 22, 2012. Secondary tubular structural system being installed among steel stringers for stairs and platforms:*


[![](media/ltpav-may-221.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-may-221.jpg)


*(below) May 16, 2012. Welders working on the structural steel stringers for stairs and platforms:*


[![](media/ltpav-5-16-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-5-16-2.jpg)


.


[![](media/ltpav-5-16-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-5-16-1.jpg)


*(below) April 14. 2012. *Construction scaffolding in the space of the Light Pavilion, with safety net:**


[![](media/ltpav-apr-14-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-apr-14-1.jpg)


*(below) April 10, 2012. Construction scaffolding in the space of the Light Pavilion. completed:*


[![](media/ltpav-4-10-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-4-10-11.jpg)


*(below) April 5, 2012. Construction scaffolding is being put in place within the space of the Light Pavilion:*


[![](media/ltpav-4-5-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-4-5-1.jpg)


*(below) March 28, 2012. The structural steel cores of the three illuminated horizontal beams that will support the glass and steel platforms have been placed:*


[![](media/03-28-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-28-6a.jpg)


*Views from inside the space of the Light Pavilion:*


[![](media/03-28-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-28-5.jpg)


[![](media/03-28-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-28-1.jpg)


*View of the Light Pavilion space from one of the other buildings defining what will become the CapitaLand Raffles City public space:*


[![](media/03-28-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-28-3.jpg)


Look for updates to this post as construction proceeds.


*(below)  March 27, 2012. Two more structural steel cores are in place. Already it is evident that the illuminated columns are not placed in a “riot” of angles, but form a direction and flow, dynamically animating the Light Pavilion space within the Cartesian frame of the CapitaLand Raffles City buildings:*


[![](media/03-27-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-27-1.jpg)


*Above photo by Manta Weihermann/Steven Holl Architects.*


*(below) March 26, 2012. Erection of the structural steel cores of the illuminated columns has begun. The Light Pavilion site as seen from the CapitaLand Raffles City plaza:*


[![](media/03-26-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-26-4a.jpg)


*The site from the plaza, with the first structural cores in place:*


[![](media/03-26-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-26-3.jpg)


[![](media/03-26-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-26-21.jpg)


*View from inside the Light Pavilion space:*


[![](media/03-26-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/03-26-11.jpg)


*.*


Look for updates to this post as construction proceeds.


*A recent news [article by Fred Bernstein](http://archrecord.construction.com/news/2012/03/Lebbeus-Woods.asp) in Architectural Record.*


*(below) March 19, 2012. The structural steel cores of the illuminated  columns, which will be encased in polycarbonate ‘sleeves' are fabricated and on the site. Steven Holl,  the architect of the CapitaLand Raffles City complex is at the center of this photo:*


[![](media/ltpav-3-23-121.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-3-23-121.jpg)


*The structural steel cores are visible in cross-section here. Erection of this structural steel is the next step and is due for completion in the next two or three weeks.*


[![](media/ltpav-32312-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/ltpav-32312-21.jpg)


.


*(below) Original concept drawing by LW of the illuminated column:*


[![](media/pav31.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/pav31.jpg)


(below) Developed design of the illuminated column, in collaboration with Christoph a Kumpusch:


[![](media/a-20a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/a-20a.jpg)


*TO BE CONTINUED. Check this post for updates over the next weeks.*


## OF NOTE: Article provokes a storm of criticism of the Light Pavilion, CapitaLand Raffles City, and their architects. [See Comments section following the article](http://archrecord.construction.com/news/2012/03/Lebbeus-Woods.asp).


#Christoph_a__Kumpusch #Steven_Holl
 ## Comments 
1. austinsakong
4.7.12 / 7pm


i can't help but think of mies' early charcoal elevations of his friedrichstrasse tower, kiesler's endless sketches of his endless house, and the watercolors of terragni's danteum. all promised new forms of space and perception, but not for the sake of newness alone– which would only result in fleeting novelty– but to address the necessity for architecture to participate in, and remain relevant to, the larger conversations going on in the world. the endless house isn't important because it simply invented a new kind of space; it's important because that new space was finally able to critique, and forge paths alternative to, the oversimplified and homogenized space of modernism, in a way that few projects before could. in that sense, the light pavilion looks to me to be a necessary contribution towards how we understand many key issues in our current day, both within, but most importantly outside of, architecture. 


finally (and this is a reaction to some of the comments on the arch record article), it seems to me that to limit the critique of the light pavilion to whether it's sculpture or architecture is a bit like limiting the critique of globalization to whether it's good or bad– a glib and unhelpful value judgement in lieu of a more difficult and relevant assessment of reality. and i'm less interested in what the ‘explanation' of the project is, and more in what the consequences of the project might be (i don't care if wright's guggenheim was inspired by the tower of babel or a parking garage, i care that it permanently cracked open the city grid). the test of this project should not be whether one can literally or metaphorically live in it. the test of this project should be whether its participants come away permanently cracked open, or unfazed and happily consuming another smooth spectacle.


after all, a work of theoretical architecture is not an unbuilt project that asks if it can be built; it's a built or unbuilt project that asks if architecture can still ask anything at all.
3. Ryan J. Simons
4.10.12 / 6pm


Lebbeus,


It is supremely exciting to see the progress of the Light Pavilion from its inception to these stages of construction. I have nothing short of pure admiration for your & Christoph's principled, determined, and adventurous approach to the project enlarge–I am slowly building a China travel fund with the intention of experiencing the Pavilion first hand. 


As for the anonymous digital shade throwing occurring on Architectural Record's site, it is very fitting that a generation experiencing such a massive decline in job prospects and/or meaningful architecture has resorted to such tactics. Please pardon the cowardly vitriol spewed by my contemporaries, their lack of will or spirit has afforded them the free time to participate in such activities. They obviously believe the right of free speech allows for their uninformed opinion (as far as I know, none of them have actually inhabited/experienced the incomplete Sliced Porosity Block or Light Pavilion yet) to merit the same weight as an intelligent discourse on the project.


I look forward to more updates and the building's imminent completion. 


En avant.
5. Jackie Luk
5.21.12 / 7am


Dear Lebbeus,


This appears to be just like what you have envisioned. Every detail is materialized as in the drawing. I am very excited to soon be able to visit the Light Pavilion when completed, to experience the space, the emotional quality, that one really need to be in it to understand the power of this piece.


Jackie





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.21.12 / 12pm
	
	
	Jackie Luk: You have been instrumental in making this design a reality. I'm very grateful for your dedication to it.
7. [MICHELANGELO'S WAR « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/05/22/michelangelos-war/)
5.30.12 / 1pm


[…] For those interested, see updates on the construction of the Light Pavilion. […]
