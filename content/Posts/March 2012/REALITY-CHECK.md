
---
title: REALITY CHECK
date: 2012-03-15 00:00:00
---

# REALITY CHECK


[![](media/con-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/con-5.jpg)


.


[![](media/con-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/con-1.jpg)


March 8, 2012


**Boldface Buildings in the Cold Light of Now**


**By VIVIAN S. TOY**


AT the height of the condo building boom in [Manhattan](http://topics.nytimes.com/top/classifieds/realestate/locations/newyork/newyorkcity/manhattan/?inline=nyt-geo), when it seemed as if a shiny new building was being announced every week, developers took a page from the motion picture industry and seized on star power to sell their projects.


In the same way that George Clooney and Meryl Streep could be counted on to carry a picture, these brand-name architects, or “starchitects,” as they came to be known, helped to set a building apart and gave buyers another reason to want to live there.


The names on the marquees included Richard Meier, Enrique Norten, Robert A. M. Stern, Philip Johnson and Annabelle Selldorf, as well as designers focused on interiors, like Philippe Starck and Giorgio Armani. The boldface treatment worked, helping to push prices ever higher.


But you don't need a subscription to Variety to know that star power sometimes dims. Now that the dust has settled and these buildings have had a few years to age and see some turnover, resale prices show mixed results. Although none of the starchitects have suffered enough to be considered box office poison, some buildings that had not sold out when the recession hit have had to offer hefty discounts. Others, while still commanding good prices relative to nearby buildings, are not holding their initial value.


Still others, like Mr. Stern's 15 Central Park West and Superior Ink, have continued to set sales records.


This era is not the first in which New Yorkers have turned to trusted names to build their homes. During the 1920s building boom, Rosario Candela, J. E. R. Carpenter and Emery Roth designed apartment houses that quickly came to define luxury. Today, their apartments are still coveted for their high ceilings, elegant details and gracious layouts. Their names, just like those of their 21st-century equivalents, often translate into added value, but do not guarantee it.


Mr. Meier's glass towers, 173-176 Perry and 165 Charles, off the West Side Highway, are credited with helping to extend the boundaries of the West Village, but his venture into Brooklyn at 1 Grand Army Plaza has not fared as well: sales started in 2007, and five years later the building has yet to sell out. Also, nearly every resale at Philip Johnson's Urban Glass House in SoHo, which opened in 2006, has been for less than the original purchase price, according to data on [Streeteasy.com](http://Streeteasy.com/).


Some buildings with celebrity architects or designers have maintained or exceeded their boom-time prices, and they undoubtedly owe their success at least partly to that star power. Where prices have not held up, though, a celebrated designer was not enough to overcome other market forces, including price levels still about 10 percent below the highs of 2008. Many buildings with lackluster track records are in less-than-ideal locations or in areas that perhaps weren't quite ready for high design and its corresponding price tag.


“The city is better for the starchitect phenomenon,” said Jonathan J. Miller, the president of the appraisal firm Miller Samuel, “because it enhanced the mystique of New York's residential housing market. But during the frenzy, those buildings were marketed as if they had inherent greater value, and the jury is still out on that.”


Mr. Miller said that people who bought apartments in buildings with design pedigrees may not be getting better returns than those who bought in luxury buildings without famous names attached.


“With the exception of the very top of the market, the city has gone into a much more austere mind-set,” he said. “I'm not convinced having a starchitect differentiates a building nearly as much as it did before.”


When Mr. Norten's One York Street in TriBeCa came to market in mid-2008, several units sold for close to their asking prices. But after the financial crisis, sales slowed and asking prices dropped by as much as 24 percent. Even then, however, many apartments sold at discounts of about 10 percent. Several apartments have been resold since 2008, each for less than the original purchase price.


Mr. Norten nonetheless sees One York as “a huge success,” noting that the penthouse sold for $23.7 million, or nearly $3,900 per square foot, an extraordinary figure for a building off Canal Street. Two-bedrooms in the building have sold for $1.8 million to $4.7 million.


Mr. Norten feels that the starchitect concept was overblown by developers and the media. During the boom, he said, the desire for a brand name may have gotten out of hand. “Developers were not just looking for architecture brand, they were looking for any brand,” including designers like Armani and Mick Jagger's daughter Jade, he said. “Of course I love Mick Jagger, but does that make a good apartment?”


Regardless, he said, “good architecture does sell better because people in a sophisticated society like New York appreciate good architecture.”


Indeed, several high-profile buildings are in the works, including Christian de Portzamparc's One57, a 90-story tower on West 57th Street; Jean Nouvel's Tower Verre, a 78-story structure that is to include gallery space for the Museum of Modern Art; and Herzog & de Meuron's tower at 56 Leonard Street.


Darren Sukenik, a managing director of Prudential Douglas Elliman, said that buyers had become more discerning in recent years. “People are no longer buying just for starchitect appeal,” he said. “Now, it's more about quiet consumption.”


At the high end, he added, “people aren't looking to be sold a lifestyle, they're looking to have their lifestyle understood and respected.” To that end, he is working on a project with an architectural firm chosen by the developers (he would not divulge its name) for its popularity with the target audience. “Instead of trying to sell them sizzle,” he said, “the developers turned to the buyer profile for inspiration.”


For brand-name buildings that have not maintained value, location may be a factor.


The price drops at Philip Johnson's Urban Glass House, which has interiors by Annabelle Selldorf, “had nothing to do with design and everything to do with the city's decision to build a sanitation building next door,” said Leonard Steinberg, a managing director of Prudential Douglas Elliman who is working with Ms. Selldorf on a recent project, 200 Eleventh Avenue.


Uncertainty about the garage, which is still under construction, definitely hurt resales, Mr. Steinberg said. But once the garage is done and “people see that it's not unattractive, I think prices will go back up.”


Designer buildings in the financial district, which is seeing about half the sales volume of 2007, have also had mixed results. Downtown by Philippe Starck, at 15 Broad, sold out quickly in 2006, with many owners flipping their places at huge profits. But since late 2008, about a quarter of the resales have been at a discount.


Ariel Cohen, an agent at Prudential Douglas Elliman who has sold or rented about 300 apartments in the building, said that the Starck design, with its iconic chandeliers and modern finishes, had held up well. Many of the units sold at discounts are open lofts, long spaces with few windows; their layout was dictated by the building's original function as a bank. He described the price decreases, ranging from 1 to 20 percent, as “stable pricing.”


At 20 Pine, a 409-unit building that is also a former bank, interiors designed by Armani/Casa were not enough to facilitate quick sales. The building came to market in 2006, and about 30 units are still for sale.


Deborah DeMaria, the agent at Warburg Realty who took over sales last year, was one of the building's first buyers. She said that she had paid about $1,200 a square foot for her apartment, and that asking prices for some units were as high as $1,700 per square foot. But the average sale price now is about $1,000 per square foot, on par with the rest of the neighborhood. The Armani name, she added, “is still a huge draw for Europeans and other buyers who are interested in design.”


Andrew Gerringer, a managing director of the Marketing Directors, says that today, a starchitect will not bring a big premium if the building is not both in “the right location and the right product for that location.”


By that reasoning, perhaps Brooklyn is not quite ready for contemporary design.


Having a sales office in TriBeCa probably didn't help Mr. Meier's 1 Grand Army Plaza when it opened in 2007, said Stephen Kliegerman, the president of Terra Development Marketing, which took over sales in January through Brown Harris Stevens. “They were trying to deny the location,” he said. “But most of the buyers have been Brooklynites, so that was a mistake.”


Brown Harris has lowered prices on many units, and the glass tower is now nearly 80 percent sold, with an average sale price of about $900 per square foot, respectable for Prospect Heights. “It's not as successful as originally planned,” Mr. Kliegerman said. “But that might have been overzealous.”


Mr. Meier says he can only guess at why his Brooklyn building has taken so long to sell. “Is it the location? Did the market change? Is it that people there like to live in town houses more than a modern building?” he said. “I don't know the answer.”


At the opening of an exhibition of his collages on the ground floor of the building last fall, Mr. Meier met many of the building's residents. “Everyone came up and told me how much they loved living there,” he said. “That was very gratifying.”


At another glass-clad building not too far away designed by Mr. Norten and promoted as “A Modern Signature to Park Slope,” only a handful of apartments have sold since sales started two years ago.


Mr. Norten ascribed the sluggish sales mainly to the building's location. It is on Carroll Street, near Fourth Avenue, the far western edge of Park Slope. “I think the developers were very brave in trying to upgrade with different architecture there,” he said. “They were taking a risk.”


Other buildings may have fallen victim to bad timing. The complexity of high design caused delays in some cases, landing projects on the market at its weakest. This may have been the case for Ms. Selldorf's 200 Eleventh Avenue, the “sky garage” building where each apartment has an “en-suite” parking spot; and for 100 Eleventh Avenue, a Jean Nouvel building with a shimmering facade made up of hundreds of irregularly shaped windows.


Sales at 200 Eleventh started in early 2007, but most units did not close until 2010 and 2011, many at a significant discount off the asking price. Ms. Selldorf directed questions to Mr. Steinberg, of Elliman, who said the waters were muddied because some final asking prices were higher than the original prices. He noted that one of the penthouses recently sold for nearly $4,000 a square foot as raw space.


Mr. Nouvel's building also came to market in 2007, though it did not start closing deals until 2010; some units have sold at 20 percent or more off the list price. Several are still available, including an eight-bedroom penthouse listed at $38.9 million. Mr. Nouvel was not available for comment.


Julie Pham, a senior vice president of the Corcoran Group who has sold several apartments at 100 Eleventh, says that prospective buyers seem to be looking for a view as much as a building designed by a famous architect. Many are art collectors who appreciate the design, and many are foreign buyers who also own properties in places like Miami and São Paulo. “Hypermodernism is not much of an issue for them: if anything, they like the glass,” she said. “Whereas New York buyers are more accustomed to not living in a fishbowl and not being on a highway.”


Recession-related problems have even affected Mr. Stern, whose 15 Central Park West recently set a new record with its $88 million penthouse sale. Highgrove, a high-rise condo in Stamford, Conn., built with the prewar aesthetic of Mr. Stern's other projects, started selling apartments in 2004 but has not yet opened. The original developer went into foreclosure, and buyers filed lawsuits as their promised move-in dates came and went. The building has now changed hands, and the new owners are considering what to do with it.


“That's an example of a building that missed the market totally,” Mr. Gerringer said. The large and gracious apartments in Highgrove are not well suited to Stamford's younger demographic, he said. Mr. Stern was traveling and unavailable for comment.


Mr. Norten recalled that a few years back, his New York office was assailed by condominium design requests. “Everybody was a developer and everybody wanted to do a building,” he said. “It was crazy.”


Today, he said, he views potential projects with a warier eye. “I think maybe we should all be a little more careful after we learned big lessons,” he said.


**VIVIAN S. TOY**


.


[![](media/con-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/con-4a.jpg)


[![](media/con-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/con-2.jpg)


*(above) What might we suppose is the “distiinctive signature of Philippe Starck” in this space? (LW)*


*The material above was [originally published](http://www.nytimes.com/2012/03/11/realestate/starchitect-buildings-test-the-value-of-a-name.html?scp=1&sq=Boldface%20Buildings&st=cse), except as noted, in the New York Times.*



 ## Comments 
1. [andy Hickes](http://www.rendeing.net)
4.16.12 / 4pm


I love the reference “none of the starchitects have suffered enough to be considered box office poison”. (“Unfortunately” I would add as I have liens on several “starachitects” for work.) 


That dialogue has always stuck with me for some reason. 


Maybe they should be called “Architecture Royalty”.
