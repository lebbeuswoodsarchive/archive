
---
title: CELEBRATING DEATH
date: 2012-03-18 00:00:00 
tags: 
    - 9/11
    - artifacts_of_disaster
    - memorials
---

# CELEBRATING DEATH


[![](media/911-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-1.jpg)


Hangar 17 at the JFK International Airport in New York City contains some of the strangest objects we might expect to encounter under the description *artifacts.* Twisted steel beams; battered and burned cars and ambulances; odd personal items bearing the traces of violence; items from a mall once lively with customers but no more—this is the stuff of many possible memorials to the 9/11 terrorist attack on the United States, collected and preserved for that very purpose. The strangest thing is, this hangar, in all its unpretentious modesty may be the best memorial of its kind to the event that will ever be devised. Unsentimental yet heart-wrenching to anyone with imagination, the straightforward presentation of these artifacts speaks directly to the impact of the attacks on so-called ordinary people who were killed in the course of their so-called ordinary lives. As we know in the years since the attacks, the impact has been enormous on everyone's life, ordinary and not.


This raises the question at to what is the purpose of the artifacts in Hangar 17, and the anticipated memorials for which they are intended? To stir the memories of people in diverse places of the terrorist attacks? If so, how can these strangely anonymous objects awaken such memories? And if that is the goal of displaying the artifacts, what is the point of awakening these memories, assuming that this is a good and necessary thing to do?


It is obvious, I think, that the events of 9/11 opened a new period in the history of human society, one still clouded by an ambivalence of meaning. The victims of the attack didn't willingly sacrifice their lives for a noble cause, but were innocent bystanders who happened to inhabit buildings that were symbols the terrorists—who did willingly sacrifice their lives—destroyed for symbolic reasons.


I personally have no sympathy for mass murder in the name of symbolism, and denounce the arracks on the World Trade Center. Still, it is hard for me to escape the feeling that the twisted steel, crushed vehicles, and scraps of clothing can too easily be understood as memorials to the terrorists as much as to their victims, because the terrorists can display the same artifacts from the places terrorized by those we presume to be ‘good guys'.


But the important point here, I believe, is that memorials cannot any longer commemorate death and destruction in the name of noble causes, but must somehow affirm the ultimate value of human life, under whatever name it goes. So, let these artifacts stay in Hanger 17, where they can be pored over by specialists for their various purposes. Or—more difficult—let them be creatively transformed into a new generation of memorials that celebrate the living.


LW


*Photos above and below were made by Francesc Torres, and are taken from [the book by him](http://books.google.com/books?id=Gu3T4QDi8BkC&printsec=frontcover&dq=Memory+Remains&hl=en#v=onepage&q&f=false) entitled “Memory Remains.”*


[![](media/911-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-2.jpg)


[![](media/911-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-3.jpg)



[![](media/911-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-4.jpg)


[![](media/911-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-6.jpg)


(below) Items from the WTC shopping mall:


[![](media/911-7.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-7.jpg)


[![](media/911-9.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-9.jpg)


*(below) Personal items found in the rubble of the World Trade Center:*


[![](media/911-10.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-10.jpg)


*(below) A category of artifacts called “composites,” comprised of many items and materials fused together by intense pressure and heat:*


[![](media/911-8.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/911-8.jpg)


#9/11 #artifacts_of_disaster #memorials
 ## Comments 
1. marmelic
3.18.12 / 6pm


I remember my first visit to the Holocaust museum in DC. There is a particular room which evoked in me such a disgusting feeling. But there's not much in the room, only rubber slippers, hundreds of them on a pile. At first, the feeling was evoked by the general atmosphere of the room: the strong smell of the rubber shoes, darkness, narrowness of space, and then nothing around you except the shoes on the floor. Only when I realized how uncomfortable I feel in this room I started thinking about the enormous number of those shoes, piling around me… I guess this is my answer to the question of: how can strangely anonymous objects awaken such memories? I like how the architecture of the Holocaust museum in DC bring the attention to the exhibited stuff and helps one learn about them, not just to see them better as in a hangar.


Another thing that crossed my mind while reading this article is the knowledge one has to have prior to looking at the strange anonymous artifacts and what knowledge do these artifacts reveal. If I did not know what holocaust is I would possibly think: the number of shoes is the same as the number of people who died; still it is an obvious fact that there are people missing in those shoes. I am, also, not sure what is the purpose of the artifacts in Hangar 17? From looking at these pictures what they reveal to me is: the violence, the fragility of human life, pain… Still non of the things I see here tell a story of a terrorist attack, nor the story of two towers, symbols of power that collapse in minutes. These things could have been found at any other terrorized place, or even after a car accident, hurricane, earthquake…


The third picture from above, puzzles me a lot. Imagine how hard the impact on that piece must have been in order to change it so radically. And what a beautiful, gentle looking shape came out of it… It is very confusing to look at this picture and the rest of them in the same context.
3. quadruspenseroso
3.20.12 / 2am


Perhaps Hanger 17 would be better understood if elements of the New World Trade Center were juxtaposed with the destroyed members. A sort of reversal from the actual site's proposed memorial; where a pair of relatively un-distorted tridents are surrounded by fastidiously scrubbed artifice.
5. [BEYOND MEMORY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/03/22/beyond-memory/)
3.22.12 / 12pm


[…] coping with traumatic experiences, such as those arising from 9/11, written about on my last post here. Memorials celebrating death, destruction, and loss keep the wounds caused by the trauma they cause […]
7. marmelic
3.22.12 / 3pm


Which memorial celebrates death, destruction, and loss? Memorials usually celebrate life after death, rebuilding of the previously destructed or discovering of the new land and similar. Which memorial celebrates the opposite?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.23.12 / 4pm
	
	
	marmelic: Any memorial displaying the fragments of murdered people or buildings speaks of their destruction and death more than of their living. Only the display of these fragments somehow transformed by those who still live can speak of—celebrate—the living.
9. [BEYOND MEMORY « LEBBEUS WOODS | The Chemistry Matter Site](http://chemistrymatter.org/beyond-memory-%c2%ab-lebbeus-woods)
3.22.12 / 3pm


[…] coping with traumatic experiences, such as those arising from 9/11, written about on my last post here. Memorials celebrating death, destruction, and loss keep the wounds caused by the trauma they cause […]
11. [Favorieten #24 « Villa La Repubblica](http://villalarepubblica.wordpress.com/2012/04/03/favorieten-24/)
4.3.12 / 7pm


[…] Lebbeus Woods schrijft over Hangar 17 op JFK International Airport in New York. Daar liggen overblijfselen van het 9/11 gebeuren. In een korte blogpost vraagt hij zich af wat er nu eigenlijk zou moeten gebeuren met deze brokstukken. De blogpost is voorzien van indrukwekkende plaatjes van Francesc Torres. Kijk hier. […]
13. [andy Hickes](http://www.rendeing.net)
4.16.12 / 5pm


I too wonder if memorials to “death and destruction” and just that. What is their history? What civilization began them?
15. [Celebrating Death – a post by Lebbeus Woods « AA Inter 3](http://aainter3.wordpress.com/2012/05/03/celebrating-death-a-post-by-lebbeus-woods-6/)
5.3.12 / 12pm


[…] article and photographs of the exhibition are here. Like this:LikeBe the first to like this post. Leave a Comment Leave a Comment so far Leave a […]
17. [george smyrlis](http://www.georgesmyrlisarch.com/)
6.3.12 / 2am


In my point of view it is just another “post-cold war” disorientation trick aiming towards the frighten up of the nation…On the other hand every sceptical person can take advantage of that unpleasant fact and its outcome (exhibition), open the discourse and finally questioning “who was responsible for the 9/11?”..then i m not too sure of wether the atmosphere of the exhibition's sceme or the answer to the question is more unpleasant!!
