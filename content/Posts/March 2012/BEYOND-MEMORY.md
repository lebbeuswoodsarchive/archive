
---
title: BEYOND MEMORY
date: 2012-03-22 00:00:00 
tags: 
    - architectural_reconstruction
    - forgetting
    - memory
    - remembering
---

# BEYOND MEMORY


[![](media/memory-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/memory-1-2.jpg)


*(above) Memory Field 1.*


Human memory is an especially contemporary mystery. For all the advances in knowledge in the past century or so, we really don't know how memory works, that is, how we are able to remember things that happened in the past. For much of that past, it has been assumed that memories are stored in the brain, much as in a storage cabinet or, more recently, in a digital computer. And yet. the more we understand about the brain, the less likely this seems. For one thing, there is simply not enough neural capacity in the brain to store a lifetime of memories, if we consider them as discrete little packages that we retrieve on demand. For another, if memory were simply a matter of retrieval, then memory would be more nearly perfect than it is. Not only do different people have different memories of the same past things, but each of us remembers the same thing differently at different times. This suggests that memories are not discrete packages, but fluid in their form and content, and the act of remembering is almost a matter of their being assembled anew for every recollection. This, in turn, suggests that memories are not stored at all, but rather continually reinvented from some kind of mental raw material available in the brain at any given time. There is no accepted scientific theory of memory that would explain such a process.


Jorge Luis Borges. in his short story, *Funes the Memorious,* forcefully introduces the antithesis of memory, which is forgetting. His protagonist is a man who has locked himself into a completely dark and silent room because he remembers everything and can't forget anything. Overwhelmed, he is simply unable to endure any new experiences and the memories they produce. It's a nightmarish dilemma, one that makes us realize how important forgetting is and also gives us some insight into how memory might work. Whatever the neural mechanism, there is a limit that can be reached emotionally: each of us can only cope with so many memories before a breakdown looms. The human psyche is limited by the personality of the individual who possesses it, which is formed by precise, unique experiences, different for each of us. Funes had his limits—we will certainly have our own, different ones.


Our limits are significantly defined by our capacity for forgetting. Funes could not forget. Fortunately, most of us can. What is the mental/neural process of forgetting? How do we un-remember? This is equally important as how we remember. Whether forgetting clears neural space for more remembering, or whether it prepares us emotionally to handle new memories, is not clear. Indeed the processes of remembering and forgetting may be linked in ways we do not yet know or imagine, but they are almost certainly mutually interdependent.


In a recent review of the book, “Memory: Fragments of a Modern History,” by Alison Winter, author-critic Jenny Diski writes a summary of findings by neuro-physiologists of the chemistry of remembering:


*“A traumatic experience is accompanied by a surge of adrenal stress hormones which increases the strength of the memory. and each time the event is recalled, a renewed rush of epinephrine and cortisol reinforces the event's emotional impact and its ease of recall. In other words, each time you recall something awful, the memory and its associated distress are strengthened. The trauma is recreated and enhanced with every recollection.”*


If this is true, then forgetting is especially important in our coping with traumatic experiences, such as those arising from 9/11, written about on my last post [here](https://lebbeuswoods.wordpress.com/2012/03/18/celebrating-death/). Memorials celebrating death, destruction, and loss keep the wounds caused by the trauma they cause painfully open, rather than help heal them. Whether this is desirable or not is a matter of personal and social choice, and how important it is for individuals and their community to keep feeling the pain caused by particular experiences. This choice will shape the character of an individual and his or her community.


Learning—the creation of knowledge—often emerges from painful experiences. We learn not to put our hand in a fire because, no matter how tempting it is to find out what fire feels like, the pain has taught us that it doesn't feel good. That is a memory we would do well not to forget. But there are other painful experiences that have taught us little or nothing, or have taught us something we don't know what to do with, such as being caught in the crossfire of a war-zone, a situation that has nothing to do with our volition or choice, but with decisions made by others. The only thing we can learn from the experience is how to recover from it, and that is a creative act of our choice that requires our transcending the pain, that is, not merely reliving it by remembering, but transforming the memory into something entirely new and affirmative.


[![](media/wascar2a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/wascar2a.jpg)This is what I had in mind in the [War and Architecture projects](https://lebbeuswoods.wordpress.com/2011/12/02/war-and-architecture-the-sarajevo-window/) of the 1990s, which addressed war-damaged buildings and their reconstruction. The *SCAR constructions* don't celebrate violence, destruction and death, but rather the creative *healing* of the wounds they have caused.


This is a critical distinction to make and I must admit that many have had trouble making it. Perhaps only artists of one sort or another—including what might be called ‘artists of life'—can make the distinction and act upon it. I was convinced when I made the projects—but not so now—that those who had lived through the trauma of war were best able to understand deeply its losses and also the necessity to transcend them, in short, to build a new society and its city. It was a romantic and much too heroic vision that badly underestimated most people's need to regain what had been lost, impossible as that is. For this reason, the War and Architecture projects and those for the reconstruction of Sarajevo, have not been useful in the reconstruction of that war-damaged city. People have preferred to remember what they lost through war than to ‘cut their losses'—forget it—and move on, making for themselves and others to follow something new from the ruins of the old.


*(above) SCAR construction, Sarajevo, 1993.*


*(below) Memory Field 1+n:*


[![](media/memory-1n-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/03/memory-1n-2.jpg)


LW



#architectural_reconstruction #forgetting #memory #remembering
 ## Comments 
1. marmelic
3.22.12 / 3pm


Wearing a scar proudly as a symbol and a reminder of a great battle one has fought with dignity and courage, for a great cause and the well being of others, not just one owns, is not unknown to various cultures. I always thought of the SCAR constructions in that way. But it always confused me why, of all the inspiring wars fought for the liberty of people against the cruel tyrant, you choose such a shameful war, as was the one in Bosnia. When I think of the SCAR constructions being applied to some other war it all makes sense. 


Regarding the subject of memory and forgetting… Maybe I never understood what the SCAR constructions are all about, but I choose to understand it in a particular way, because my way inspires me to go on. If architecture of memory does not dictate the memory but leaves space and time for my own interpretation, within that space and time I will bury/forget something. This way I never forget for good, only temporary. Until I am reminded of it, and I go back to the memorial place to remember what it was…
3. [metamechanics](http://metamechanics.wordpress.com)
3.23.12 / 2am


First half (memory-spinning)


I watched Hugo recently with my older daughter (she's nearly 4) and as a result I vaguely remembered Descartes' animal Automata machines and more clearly Charles Babbish computer ( a memory re-instated by your analog computer blog) and on my way to the gym I imagined a memory (a thought) – All organic and inorganic matter are a set of mechanical cogs wound up and spinning until their matter is spent (into energy) over time (unwinding). To achieve Artificial intelligence or to be human requires a mechanical cog pausing in its cycle of spin from its network of spinning cogs. To make a cog pause all you need to do is a pose a question relevant to the cogs cycle. Descartes famous statement “I think, therefore I am” is a question and answer relevant to possibly the largest mechanical cog – the question of being. Heidegger would tell you non-being is questioning. Like the butterfly effect in chaos theory one question or the pausing of even the smallest mechanical cog can throw the whole system into another state. Memory if not a stored object in the mind, could be a combination of mechanical cogs spinning at a moment in time, rotated into linking positions by the questions at hand, spinning in unison to create a thought, maybe one you have had before or you think you have had before (deja vu).


Second Half (memorials and trauma)


As noted in your qoute and in the language of mechanical cogs, the memory of the trauma is enhanced by recollection or by spinning more often in the same state without question, well oiled and worn into effective unison, possibly even synergetic. As a philosopher and artist who is offering a change, you must find the mechanical cog to pause, the relevant question that in affect could alter the entire state of the machine (the mind), reinvent combinations of spins that create memories differently. The memorial as you blogged in your last post to most I assume would offer little to question, hence designers and artists are often asked to create a memorial in such as way to pose a question that one would assume should change the mind to a better state. With that said though, most people prefer the same cycle I would assume or as you suggest salvaging a comfortable cycle from the past. Asking people to ask questions is no easy task.


Random notes:


Over the years I have decided some people actually are not habitual liars and bullshitter, its just their memory sucks.


There was a theory about alcohol not killing brain cells, just turning some off and others on, so in college I used this theory to its fullest, after finals week if everything looked like a mathematical modern piece of art history I would drink it into forgotten land and when I woke up only the important things would be remembered, usually nothing to do with the questions on the finals.
5. [Stratmort](http://arcsickocean.wordpress.com)
4.12.12 / 10am


Lately, I saw a winning concept design of an installation called ‘Memory Cloud' by Re:site and Metalab, using parametric design, the latest technologies of LED lights and remote sensors. It combines figures of past and present students of Texas University. The designer's approach, as seen in this video <http://www.youtube.com/watch?feature=player_embedded&v=VOr9O1ORSTQ> ,as well as the phantasmagorical effect created by the technology behind it, is surprising.  

 However, we must not leave out one of the most avant-garde minds of today in architecture, Daniel Libeskind, that approached the sense of Memory in a totally different way. His machine differentiates from this piece of ‘technological art', because of the fact that Libeskind didn't go for the obvious matter of memory. He did not want to present a machine that re-creates what our mind sees when remembering scenes of life. He took a step further from this thought and as he writes for his machine : ‘You can use it, manipulate it, pull the strings'. The way the machine is demonstrated shows that Libeskind test how we are in control of our memory, our mind and what happens in it. He doesn't present the result, but rather the process behind it.  

 As far as a way of interpreting memory in art is concerned, even then i would like to mention Bill Viola's video art and particularly ‘The theater of Memory' (<http://www.youtube.com/watch?v=b4a9i58IdIY>). The modern artist gave many more thoughts to the concept behind his work. He understood how ‘remembering' really feels like; momentary, with ups and downs, lights and darkness and quick visual effects of people and spaces. He plays with the real and the imaginary. Viola tempts to show the matter of space and time for the way our mind thinks, experimenting with the natural abstraction that our mind goes through.  

And although technology is being developed from one second to another, our rather technical age goes through a worrying transitional period, where, as Bill Viola himself states, ‘‘the way things work, the how, matters more that the reason why''. And maybe that proves why Libeskind and Viola have given us a second and a third thought wht their genius works, rather than presenting us a ‘showcase', an impressive facade or installation.
7. [Storms: Choices, Consequences, and Contentment | Social Behavioral Patterns–How to Understand Culture and Behaviors](http://organizationalchangesolutions.wordpress.com/2011/04/28/storms-choices-consequences-and-contentment-2/)
4.14.12 / 3am


[…] Beyond Memory (lebbeuswoods.wordpress.com) 0.000000 0.000000 Share this:TwitterFacebookEmailPrintStumbleUponLike this:LikeBe the first to like this post. […]
9. [andy Hickes](http://www.rendeing.net)
4.16.12 / 5pm


Have you read “A Sense of an “Ending” by Julian Barnes? Memory as fiction.
