
---
title: BUILDING LANDSCAPES
date: 2010-09-13 00:00:00 
tags: 
    - architecture
    - ecology
    - landscape
---

# BUILDING LANDSCAPES


We all know that architects design buildings, those discrete objects that sit on a site somewhere in a town or a city or the suburbs or the countryside. We also know that architects, at least most of them, make an effort to design their buildings with some sensitivity to the particulars of their sites and even to a broader landscape commonly known as ‘the context.' But in our contemporary urban world, with its aggregates of buildings that become in themselves artificial landscapes and contexts—entirely displacing the natural—the architect's role would seem to inevitably expand beyond designing built single objects. Also, in our contemporary world of environmental and global ecological concerns, it is clear that even the design of single buildings has broad consequences and must be framed in those terms by their designers, indeed by all involved in their realization. It is up to architects, with their presumably wider perspective, to take the lead.


A few architects have explored the possibilities of fusing buildings and landscapes, creating what Thom Mayne has called ‘hybrid landscapes.' While such are not in themselves inherently ecological, this approach indicates a sensibility that could lead in that direction. At the very least, it manifests a different attitude toward architecture, one that plays down a heroic conquest of nature and looks for modes of coexistence with it. As in all cases of coexistence, neither presence is sacrificed at the expense of the other; rather, each impacts the other in creating—hopefully—a balance, even a new form of harmony.


LW


.


(below) *Giant Group Campus*, Shanghai, China, by Thom Mayne, Morphosis, design 2005, construction completion, c. 2010. Actual photos of constructed landscape:


[![](media/maynecampus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/maynecampus-2.jpg)


[![](media/maynecampus-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/maynecampus-1.jpg)


[![](media/shanghaicampus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/shanghaicampus-2.jpg)


(below) *City of Culture of Galicia*, Santiago de Compostela, Spain, competition model, 1999, by Peter Eisenman, Eisenman Architects. Project under construction.


[![](media/eisenman-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/eisenman-1.jpg)


(below) *Terrain (earthquake architecture)*, by LW and Dwayne Oyler, 1999:


[![](media/terrain-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/terrain-11.jpg)


(below) *Terra Nova*, Korean DMZ, 1988, by LW:


[![](media/dmz26.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/dmz26.jpg)


[![](media/terranova-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/terranova-12.jpg)


[![](media/terranova-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/terranova-2.jpg)


[![](media/dmz27a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/dmz27a1.jpg)


Artists have covered the same ground, so to speak, in ambitious ‘land art' projects:


(below) *homage to El Lissitzky*, The Netherlands, 1986 [demolished c. 1993], by Lucien den Arend:


[![](media/llystad-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/llystad-1.jpg)


(below) *Spiral Jetty*, Utah, 1970, by Robert Smithson:


[![](media/smithson-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/smithson-1.jpg)


Industry has done the same, with less than encouraging results, bringing into question the very idea of transforming the natural. To what end is it done? And with what consequences?


(below) Coal strip mine, West Virginia, photo c. 2008:


[![](media/wvastripmine.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/wvastripmine.jpg)


#architecture #ecology #landscape
 ## Comments 
1. [terry](http://www.twt-photo.com)
9.15.10 / 2pm


Lebbeus:


Thought you might like to know about another fantastic landscape building… the Chemical Science Building at Trent University (<http://tinyurl.com/teeple-chem>) by Teeple Architects (<http://www.teeplearch.com>)
3. [José Nascimento](http://esquisso.tumblr.com/)
9.17.10 / 1pm


Lebbeus: I don't agree with you because these are not buildings nor landscape. This's camouflage architecture in response to our guilty ecological concerns.
5. [dude](http://www.telegraph.co.uk/news/worldnews/1544827/Gangster-who-built-worlds-tallest-log-cabin.html)
9.19.10 / 10am


site specific= local materials+ articulation of form and construction through local culture)  

tallest woodern skyscraper  

(no plans, built by one man, its awesome!) <http://www.telegraph.co.uk/news/worldnews/1544827/Gangster-who-built-worlds-tallest-log-cabin.html>
7. [oscar falcón lara](http://www.tallerdearquitecturamonterrey.com)
9.25.10 / 3pm


Landscape and terrain are not always a necessary entanglement, I agree. There is an inherent poetic justice, in my view, in reorganizing mother earth, and a veiled disdain in the discourse of want/need of our projects' sites. 


Interesting article.
9. spencer
9.29.10 / 4pm


I have a tendency to want to agree with those people who question this notion of the merger of building and landform. Historically there are many examples of this kind of thinking. Buildings have mimicked mountains, valleys and plants, they have been built into cliffs, covered with earth, and made of local materials throughout our existence. This makes me wonder, Lebbeus, what do you see that is so new here that I am missing? 


What I see is a growing trend toward ecological white-washing as a reactive response to decades of over consumption of natural resources. We expend so much energy to construct our buildings that we now have to compensate by covering them with more vegetation (for example). I feel this is what José is reacting to as well. Having studied and participated in several NGO methods of design, LEED (etc.), I have found they create an additive and subtractive methodology of design without inspiring a truly integrative (with nature) design process. They tend to encourage more mechanical forms of solutions. (I don't intend to spark a discussion on if this true or not but, rather, would like to talk about how we can improve on their beginning point)


From your above examples of buildings, I would wager that most of these buildings take up as much land area per person as a typical USA sub-division. Although, entwined with the landscape this would hardly be considered ecologically sensitive on all fronts. With the complexities of our impact on the earth it is difficult to consider every aspect when designing buildings. Still, we desperately need to explore a design methodology that answers many more questions than we are currently doing or seem to be capable of.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.29.10 / 9pm
	
	
	spencer: my point in this post is not that such projects as the ones I show are any more ecologically sensitive than normal buildings—obviously they are not. But I do think that their effacement of the heroic, free-standing building in favor of a landscape points in a direction that COULD lead to thinking and projects really integrating the human and the natural. Another approach to the truly ecological would be that long insinuated by Cedric Price: let's learn how to use and re-use the buildings we already have. In short, let's declare a moratorium on all new buildings. Hardly music to architects' ears!
11. spencer
9.30.10 / 6pm


that's a tune I'd like to hear from time to time.


I still have to wonder at what point does a building in the landscape and a building inside the landscape just equate to the same thing? José is correct to question this notion. At this stage we are just looking at another way to clad a building.


I agree with you that the above buildings constitute a dipping of the toe into the deep waters of creating living buildings. As the complexity of our world increases our need to integrate more variables into our design methods follows in a direct relationship. As noted by many environmentalists we are taxing our resources. Even if our current generations will not see the extinction of certain resources it can't be denied they are finite and we will run out if we don't change our course. In my opinion we are not doing enough right now to make that correction.
13. [THOM MAYNE'S MIND « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/25/thom-maynes-mind/)
11.27.10 / 4pm


[…] human. His new model studies push far beyond his previous work, coupling his growing interest in architecture-as-landscape with evocations of innovative technologies of building and, perhaps more importantly, of designing […]
15. [# HETEROTOPIC ARCHITECTURES /// Building Landscapes by Lebbeus Woods | The Funambulist](http://thefunambulist.net/2010/12/23/heterotopic-architectures-building-landscapes-by-lebbeus-woods/)
12.23.10 / 1am


[…] Woods released a new article on his blog entitled “Building Landscapes” featuring works of Thom Mayne, Peter Eisenman, Robert Smithson and some of his beautiful […]
17. [Briana](http://brimorrison.com)
6.22.11 / 1pm


I have to argue that architects don't have the “presumably wider perspective.” Collaboration of architects with landscape architects, ecologists, civil engineers, and other related professions is what provides a wider perspective.
19. [A TREE IS A TREE IS A…BUILDING? « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/01/27/a-tree-is-a-tree-is-a-building/)
1.27.12 / 8pm


[…] Related post on the LW blog: https://lebbeuswoods.wordpress.com/2010/09/13/building-landscapes/ […]
21. [On becoming Lebbeus Woods « typologica](http://typologica.com/2012/02/13/on-becoming-lebbeus-woods/)
2.13.12 / 3pm


[…] … “Dry your eyes, Lebbeus Woods explains why architecture school and years of unpaid labor might be worth it” is how Architizer tweeted their recent post summarizing Woods' lovely and concise true story, “Why I Became an Architect”. Posted in two parts on his blog this past week, the story is in actuality less about why and more about how one becomes and architect—or any creative professional, really—and therein lies the essence of its hard-won truth. […]
23. [By Hand | Guest Post by Pages From My Moleskine. | yellowtrace blog »](http://www.yellowtrace.com.au/2012/05/22/by-hand-guest-post-by-pages-from-my-moleskine/)
5.21.12 / 7pm


[…] Best in Show at Ken Roberts Memorial Delineation Competition. Terra Nova by Lebbeus Woods 1988 via Lebbeus Woods. Moon Joo Lee. Finalist in 2011 in the Professional Hand category at Ken Roberts Memorial […]
