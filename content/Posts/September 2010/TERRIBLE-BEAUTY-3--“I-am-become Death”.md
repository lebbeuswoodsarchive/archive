
---
title: TERRIBLE BEAUTY 3  “I am become Death”
date: 2010-09-14 00:00:00 
tags: 
    - a-bomb_tests
    - aesthetics
    - destruction
---

# TERRIBLE BEAUTY 3: “I am become Death”


[![](media/abomb-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-11.jpg)


(above) Atomic (nuclear fission) bomb detonating in a Nevada desert test blast, 1950s.


The detonation of nuclear fission and fusion—atomic—bombs, still the most feared WMDs, inspires many metaphors, including “hell on earth,” “a sun brought down to earth,” and the lines from the Bhagavad Gita quoted by the head of the Manhattan Project, [J. Robert Oppenheimer](http://en.wikipedia.org/wiki/J._Robert_Oppenheimer), as he witnessed the first test of an atomic bomb in New Mexico, in 1945, “Now I am become Death, the destroyer of worlds.”


Two atomic bombs have been used on cities—Hiroshima and Nagasaki, Japan, in August of 1945—though tens of thousands of nuclear fission and fusion bombs exist in the world today, in the hands of national governments that have so far been moved for various reasons not to use them in war or as an instrument of national policy. Yet they are here, a button-push away from fulfilling their terrible purpose of creating unimaginable destruction and suffering. As emerging national states and their often-autocratic leaders jockey for position and power in global society, the possession of nuclear weapons gives them leverage in being taken seriously. We hope that these weapons will never be used, but cannot be naive—we must also live with the prospect that they could be.


.


(below) Stills from a recent New York Times [article](http://www.nytimes.com/2010/09/14/science/14atom.html?ref=science), drawn from two new atomic documentaries, [“Countdown to Zero”](http://www.takepart.com/countdowntozero) and [“Nuclear Tipping Point,”](http://www.nucleartippingpoint.org/home.html) featuring archival images of the A-bomb test blasts.


(below) In the first milliseconds after a bomb is detonated, otherworldly blossoms emerge—the first intense shock and heat waves:


[![](media/abomb-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-3.jpg)


[![](media/abomb-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-5.jpg)


[![](media/abomb-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-4.jpg)


(below) The fireball rises in a Nevada desert bomb test, 1955:


[![](media/abomb-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-6.jpg)


(below) A-bomb test in the Pacific Ocean, 1946. Two test ships are seen in vertical position, rising in the blast's column of water:


[![](media/abomb-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-2.jpg)


(below) V.I.P. observers watching a bomb test in the Pacific, 1961—‘the sun-tan that lasts':


[![](media/abomb-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/abomb-7.jpg)


.


Historical references:


<http://en.wikipedia.org/wiki/Nuclear_weapon>


<http://en.wikipedia.org/wiki/Manhattan_Project>


A recent NY Times article:


<http://www.nytimes.com/2008/12/09/science/09bomb.html?_r=1&8dpc>


Related reference on this blog:


h[ttp://lebbeuswoods.wordpress.com/2008/11/13/big-bang/](https://lebbeuswoods.wordpress.com/2008/11/13/big-bang/)


#a-bomb_tests #aesthetics #destruction
 ## Comments 
1. Natale
9.17.10 / 2pm


This is very sobering stuff. 


What is the scale in the 3 images taken milliseconds after detonation? Are these atoms I'm seeing or something much larger? They are truly sublime.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.20.10 / 2pm
	
	
	Natale: In the three ‘millisecond' images, the third from the top shows a vertical black line, which is the tower on which the bomb was placed—100+ feet high—just before it was vaporized by the blast.
3. christian peter
9.18.10 / 3am


It is strange. “Now I am become Death, the destroyer of worlds”. But these first millisecond images take on an inverted quality, actually the first milliseconds of life. I have one child and another on the way and these images strikingly resemble the moments before birth.  

…  

hmmm?
5. theblobbyblog
10.8.10 / 2am


images 2-4 are incredible! the scale is great because it recontextualizes devestation, submilimity, etc etc. It would be interesting if this was the face of desctruction.
7. David
10.22.10 / 3am



A great visualization of nuclear explosions over time and showing the countries that have performed the tests.
9. David
10.22.10 / 3am
11. [ORIGINS « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/01/02/origins/)
1.3.12 / 12pm


[…] <https://lebbeuswoods.wordpress.com/2010/09/14/terrible-beauty-3-i-am-become-death/> […]
