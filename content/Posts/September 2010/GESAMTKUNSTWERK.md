
---
title: GESAMTKUNSTWERK
date: 2010-09-07 00:00:00 
tags: 
    - gesamtkunstwerk
    - integrated_arts
---

# GESAMTKUNSTWERK


[![](media/lwblog-asp-51.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/lwblog-asp-51.jpg)


(above and below) From the *Architecture-Sculpture-Painting series*, by LW, 1979.


The idea of *[Gesamtkunstwerk](http://en.wikipedia.org/wiki/Gesamtkunstwerk)*—a German word for “total art-work”—has long since gone the way of all 19th century Romantic ideals, into the trash-heap of history. It began with the belief that art really mattered in human society, morally and politically. This belief was rooted in the great value the ruling classes had always given to art as a symbol of their wealth and power, but also to its place in religions important to European history. It was only natural, therefore, that at the beginning of the modern age, many believed that newly emerging industrialized democracies, both capitalist and socialist, needed not only their own new forms of art, but also new forms of integrating the arts, as had been done in the great cultures of the past. Architecture, painting, sculpture had indeed been brought together in the important buildings of most ancient civilizations, such as Egyptian and Greek temples, as well as in Medieval cathedrals, Renaissance palaces, and Baroque churches, and were combined there with music and both religious and secular rituals and performances. Total art-works. The most notable modernist attempts to accomplish the same were at the Bauhaus and the Russian Constructivists, though were each defeated by political forces—but that is another story.


Today, art is a commodity separated from itself, so to speak, in order to break it down into salable units. Modernism never found its *Gesamtkunstwerk*.


At a certain stage of my life, I fervently believed that architecture could sponsor a reunification of the arts, in the service of both public and private life, even though it would have to do so very much against all tendencies and trends. Vestiges of this have remained throughout the succeeding years—in [System Wien](https://lebbeuswoods.wordpress.com/2009/06/05/architecture-of-energy/), for example—but never in such an ambitious and hopeful a form as these drawings. Auf wiedersehen, old friend!


LW


.


[![](media/lwblog-asp-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/lwblog-asp-31.jpg)


.


[![](media/lwblog-asp-61.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/lwblog-asp-61.jpg)


(below) From the *Houses* series, by LW, 1979. Less radical examples than the above of an integration of architecture, art, and landscape:


[![](media/lwblog-asp-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/lwblog-asp-11.jpg)


.


[![](media/lwblog-asp-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/lwblog-asp-21.jpg)


#gesamtkunstwerk #integrated_arts
 ## Comments 
1. slothglut
9.7.10 / 11am


Dear LW,


Could you please elaborate more on how these drawings could be read as ‘total artwork'? What kind of music does it play, what was the meaning ritual held those days? What events did it hold in order to become total?


as always, tQ for the great posts, + in advance for the answers,





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.7.10 / 12pm
	
	
	slothglut: The quick answer: the key thing in these drawings is the ‘integration'—you can't really separate what is architecture, sculpture, and painting—not visually or conceptually, and certainly not to detach any one of them to sell separately. The top three drawings address public space, the bottom two domestic space. I realize you want more specifics, which I will try to provide when I am able to write a longer reply.
3. Ben Dronsick
9.23.10 / 2pm


LW: pardon the obvious, but the first image in the '79 House series is eerily prescient of Abraham's OEC in Beijing. I had forgotten about that image. Thank you.
5. theblobbyblog
10.8.10 / 2am


lebbeus — doesn't unification tend to undermine the very thing about art, architecure, etcetc that we all come to appreciate? i wonder if it may not lead to a greying of the senses, only to [again] re-foster the emergence of the specialization of beauty as highly particularized, that which you wish to see abolished
7. [Matthew Weinreb](http://www.thearchitecturalphotographer.com)
11.11.10 / 11am


Hi Lebbeus,  

Matthew Weinreb here (you knew my Father, Ben Weinreb.)  

I am interested in the above… Would you not consider The Rietveld Schröder House a Gesamtkunstwerk? It seems to me to epitomise the idea.
9. jamiesaker
7.10.11 / 8pm


I'm somewhat perplexed at the claim that modern art has found no gesamtkunstwerk. Unless modern art is defined solely in architecture, there are plenty of examples of the expansion and integration of art into politics, economics, etc. A mere examination of the works of German Christoph Schlingensief, for instance, is rich with evidence of such engagement, as are many of the post-dramatic theater applications. Even in the United States secondary educational environment, numerous high school policy debate teams apply total art concepts to political debate, infusing blues music performance, visual art tapestries, multimedia projection, poststructural and postmodern theory, etc. into “policy debate” discourse.


And even should one declare that architecture is the sole realm of the arts (one I'm certain some of the greater architects would struggle to comprehend), one only has to look at the architecture of Christo and Jeanne-Claude in their wrappings, umbrellas, etc., or the architecture of Paul Virilio (such as his Bunker Church in Nevers France). Actually, a mere visit to Venice Biennale might surround such a skeptic with ruptures of art into spaces of the political, economic, scientific, etc. This year's U.S. exhibit featured a functioning ATM machine, provided with the courtesy of the exhibit's artists, and embedded into the recreation of a giant church organ.


Gesamtkunstwerk is extensive, but what isn't appears to be the awareness of those skilled trade technicians who have been trained to limit their practice to a repetition of the scientific methods drilled into them. I'm afraid this condition is indeed pervasive and problematic, but given the expression of creativity emerging globally in emerging economies, it's of little matter should the practice within western nation professions fail to engage the arts into their work.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.10.11 / 10pm
	
	
	jamiesaker: You have a wider definition of modern art than I do in this post. My points are tightly focused on the integration of the traditionally defined plastic arts—architecture, sculpture, painting. In past times and in many different cultures, painters and sculptors ‘decorated' architecture, creating the ‘total artwork.' Adding dance, music, and other forms of performance were further 'embellishments.' While the examples you mention mix the arts, they don't truly integrate them in the sense that they were made to be together. The exceptions might be theater pieces, which is why Richard Wagner claimed his “music dramas” (operas) were the only true gesamtkunstwerk of his time. Kurt Schwitters, who taught at the Bauhaus and did stage design, considered his *Merzbau* a total artwork in the old sense of the word, but he was probably the last Modernist to have such an ambition. Of course, a number of Post-Modernists made attempts to revive the idea.
11. [» Gesamtkunstwerk](http://itsderivative.com/5/2011/08/gesamtkunstwerk/)
8.9.11 / 7pm


[…] Lebbeus Woods […]
13. [Gesamtkunstwerk | Main](http://thefirstyeariscalledconnectionsandisconceived.as/gesamtkunstwerk/)
4.17.12 / 4am


[…] Via: Lebbeus Woods […]
