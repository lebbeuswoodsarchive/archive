
---
title: BARBARISM?
date: 2010-09-10 00:00:00
---

# BARBARISM?


[![](media/9-11mural-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/09/9-11mural-1.jpg)


(above) Street mural, North Fifth and West Annsbury Streets, Philadelphia, 2001, by Lotus and Dan Past Bird (photo 2005, by New York Times).


In some ways it is surprising how little society has ‘progressed.' One would think that thousands of years of human experience, well documented in histories and art would somehow inform people in the present about the best things to do. Not so. Consistent with my contention that knowledge cannot be passed on from one generation to the next, but only data, and therefore that knowledge has to be continually re-invented, it seems we have to relive for ourselves all the tragedies and the triumphs of being human, in order to learn their lessons. And when we die, the next generation will have to start all over again, with our data—the testimonies of our experiences—useful only as guides. *If they could do it, so can we*: “The best thing about history,” Goethe said,” is that it inspires us.” The downside of his bit of wisdom is that it can't do much else.


No doubt these ruminations are inspired by contemporary events in our global society, which make the Cold War of the last generation seem like elevated philosophical discourse about socialism versus capitalism. Today we have entered a slipstream carrying us backward into bitter rants about Christianity versus Islam which is simplistically comparing one religious apple with another. And here I thought Marx's dictum about religion being the opium of the masses had somehow stuck in our consciousness. This is not to mention Nietzsche's admonition that “all the gods are dead—they died laughing, when one god said: I am the only God!” And especially not to mention a half century of heartfelt Existentialist arguments for human beings taking full moral responsibility for their own actions, and not turning to a priestly class for knowing what is the wrong and right thing to do. But, judging from current events, all that data is lost on people living today, if they—we—ever knew of it at all. Rather, we find ourselves living in a Medieval state of conflict over religions and whether one is truer than the other and which one represents the true God. To borrow an old movie tagline: “Just when you thought it was safe to go back into the water…!”


The current regression of society is worrisome, if not alarming. Recent history warns us how quickly a civilized human community can revert to barbarism, committing terrible atrocities, such as genocide and mass destruction—through war or escalating acts of terrorism—in the name of political, moral and religious righteousness. To prevent this happening, it is not possible to rely on politicians or religious leaders to raise the level of concern, discussion or competition to addressing the most pressing human needs. Too often even the best of them are caught up in their own rhetorically-driven games to exercise any influence over emerging events, and the worst of them callously exploit raw, vengeful emotions for their own narrow ends. The best chance American and European leaders had to lead upward and on was immediately after the terrorist attacks of 2001, when they could have sought dialogue and a healing exchange, rather than violently invading Islamic countries (remember the U.S. President saying that the invasions were “a crusade”?). No, all we can do is work within our own domains—however small or large they may be, in whatever ways we are most adept—to reverse the slide backwards toward barbarism. In the end, this may not be enough, but the earnest work of many, in the spheres where they exercise some influence—is the only hope to counter the reckless indifference of the powerful few.


LW



 ## Comments 
1. david
9.11.10 / 2pm


Perhaps we can take some solace in the fact that, despite all the bad news, the story of civilization has actually been one of ever-decreasing chances of dying a violent death.


Steven  [Pinker](http://www.edge.org/3rd_culture/pinker07/pinker07_index.html) gives an encouraging account of the success of human institutions in lessening the barbarism of our “natural state.”
3. [Chris Teeter](http://www.metamechanics.com)
9.12.10 / 2am


ludites will be mysterious super heros in the future.
5. Arman Bahram
9.12.10 / 10pm


As long as religion is the lowest common denominator of a civilisation then all there is to feed off is the lowest common denominator, both in times of prosperity and poverty.
7. [Stephen Lauf](http://www.quondam.com)
9.13.10 / 8pm


The street mural at Fifth and Annsbury Streets, Philadelphia, dates from 2001. The artists, Lotus and Dan Past Bird, spontaneously started painting the mural around noon 11 September 2001 and finished the mural arounf noon 12 September 2001. The mural is not far from where I lived in 2001, in a neighborhood that was more part of my childhood than adult life. I happened to drive by the mural for the first time 21 September 2001, and I took lots of pictures of it, and asked about its history in the restaurant across the street. At the end of 2001, I named the mural my favorite metabolic (creative/destructive) work of art for that year. John Young might remember me doing that at design-l.


I don't know if the mural still actually exists, but it was almost as surprising to see it here as it was to see it for the first time nine years ago.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.13.10 / 9pm
	
	
	Stephen Lauf: Thank you for the detailed background info. The mural says a lot about high emotions at the time, which continue today. The architectural rendering of the context of the Twin Towers seems part of another world—maybe a mythical city—and very, in its way, ‘idealized'—very strange, don't you think?
	
	
	
	
	
		1. [Stephen Lauf](http://www.quondam.com)
		9.13.10 / 10pm
		
		
		Lebbeus, regarding the rendering of the context of the Twin Towers, I know what you mean by “mythical” and “idealized”, but I don't find it very strange. Seeing the whole mural in person and knowing when and how quickly it was done, my impression was that Lotus and Dan Past Bird did indeed paint the mural very spontaneously and without any visual aids except for the memory of what they saw on television. To the right of the Twin Towers is a man free-falling with sheets of paper flittering around him, and to the left of the Twin Towers is an outline map of North America with target cross-hairs marking the New York, Pennsylvania and Washington DC sites of that day.
		
		
		I had my camera with me the day I first saw the mural because I spent the morning visiting The Wagner Institute of Science in lower North Philadelphia. Steven Izenour died suddenly 21 August 2001, and anyone that wanted to make a donation in Steven Izenour's memory could do so to the Wagner Institute. For some reason, on the way home I decided to drive up North Fifth Street, something I hadn't done in many years. About ten blocks south of the mural I started taking pictures from my driver's seat, thus I have a picture of the mural from about a block and a half away, when I didn't even know the mural was there. I'll post this picture (and all the mural pictures I have) in like a day or so, because you'll see where the artists are from, and then their rendering of the Twin Tower's context may not seem so very strange.
		
		
		As to “THIS MEANS WAR,” like you say, emotions were very high that day, and, as Lotus and Dan Past Bird were busy painting, all the rest of us were still mostly unsure as to what the events of 9-11 actually meant.
9. [» Stumbling to Death twig](http://bluesink.com/twig/?p=199)
9.24.10 / 8pm


[…] found that sad article about the same time I came across the following Lebbeus Woods entry: In some ways it is surprising how little society has ‘progressed.' One would think that […]
