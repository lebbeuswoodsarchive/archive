
---
title: UNREAL RUINS
date: 2011-04-05 00:00:00
---

# UNREAL RUINS


[![](media/judah-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/judah-1.jpg)


The British artist [Gerry Judah](http://en.wikipedia.org/wiki/Gerry_Judah) makes detailed scale models of urban building ensembles, then carefully smashes them to resemble bombed-out fragments of cities. His final touch is to sanitize them by painting everything white, abstracting them into sculptures, but not quite. When we see them, the first thing we think of is not abstract shapes and shadows, but representations of blasted buildings, sites of human habitation, suffering, and loss. What is the artist really up to?


Elsewhere on this blog, I have written about “[aestheticizng violence](https://lebbeuswoods.wordpress.com/2010/01/15/aestheticizing-violence/)” and “[terrible beauty](https://lebbeuswoods.wordpress.com/2010/07/24/terrible-beauty-2-the-ineffable-2/),” discussing artists'—and occasionally an architect's—risky depictions of violence, destruction, and tragedy, always for a reason. The risky part is taking the chance that the exploitation of such events is worthy of their seriousness. Theodor Adorno said, “There can be no poetry after Auschwitz,” meaning that in a world where Auschwitz happened, no ‘poem' can avoid trivializing its universal tragedy. Whether we agree with him or not, we take a point. Those who take the risk will be criticized or condemned by anyone thinking anything like Adorno, and even those praising such work will do so with a certain discomfort. To withstand the criticism and qualified approval, the artists must have a strong sense of personal mission, believing their works are necessary to in some way help to heal the wounds caused by violence and destruction. If the intention is only to create interesting, attractive, salable objects, then their exploitation is not, in my opinion, justified.


Gerry Judah calls his white canvases three-dimensional paintings. He shows them in art galleries where, presumably, they are sold. He says that he is inspired by the destruction caused by war, as well as the horrendous human consequences. He implies that he hopes these paintings will move people to put an end to war.


The problem I have with this intention is that these works are far too attractive to stir people's compassion for war's victims, or the loss of culture, or repulsion at the lust for violence. Geoff Manaugh over at BLDGBLOG calls them “gorgeous.” There is a vital edge missing, something that cuts below the surface, laying open hidden layers, revealing disturbing emotions or ideas. It's for the artist to say what these might be, but I think the problem begins, materially, with painting everything white and calling the work finished. Conceptually it begins with thinking that ruins are the subject.


LW


.


[![](media/judah-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/judah-2.jpg)


[![](media/judah-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/judah-4.jpg)


[![](media/judah-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/judah-3.jpg)



 ## Comments 
1. bomoc
4.6.11 / 1am


Painting the constructed ruins white, for me, feels almost like a sealant – halting the destruction in the moment so that we can approach it safely, clinically, or objectively. The result of the work being ‘complete' at the stage of ruin conceptually denies us possibly the most traumatic stage, that comes after the ruin is created. It seems for the artist that the ruin is the definitive result.  

I think colour plays a critical role in the emotive potential of ruin. The shocking and tragic images coming from Japan have a powerful quality in terms of colour; the various identifiable hues of materials for home building become mixed and strewn in a way that strikes me as both familiar and bizarre, truly uncanny. The height that some photos are taken from seem to make the ground plane even more abstract to us, like some kind of Pollock, but one where the composition feels less than the sum of its parts.
3. C
4.6.11 / 4am


I agree with bomoc that there is something strikingly uncanny about the works. The purity of white echos the abject nature of bones. We are left feeling that the city and its inhabitants were frozen at a singular point in time, covered in a silent snow. I wonder though, what will be revealed when the snow begins to melt away?
5. dazed
4.6.11 / 7am


it kind of reminds me of google maps for some reason, or maybe sketch up (all white) But I am really happy for this artist. And thanks L
7. [arete design](http://www.aretedesign.blogspot.com)
4.8.11 / 1am


I think I have an issue with his work, it's like putting makeup on a corpse. I don't like the idea of making a shrine of war and selling it for the intention of putting a stop to war. This piece represents to me what media makes of war. It turns it into a box office movie and Disney-fies it for our entertainment and we don't know the truth for the pretty picture.  

Sorry to rant. I don't like the idea of someone beautifying war to put on my wall. Actually, the white paint makes me think of a geisha trying to sell her wares….
9. tom
4.8.11 / 3pm


To me, this seems like overreaction. They don't really resemble any real war zone. They look like pretty generic student “mixed media” work, to use a dated phrase. If they refer to anything, to me, it looks like the loss of hope of the space program and colonizing other planets. Silly stuff, actually. Or maybe just some academic riff on the New Babylon.
11. [MM Jones](http://www.bauzeitgeist.blogspot.com)
4.29.11 / 11am


They look delicate and quiet, but suggest violence. They remind me of pictures of the Titanic, discovered at the bottom of the ocean floor, covered with decades of marine dust. Maybe its interesting that this type of process of becoming ancient was performed artificially, miniaturized and accelerated.
13. [mudl.us](http://mudl.us.s3146.gridserver.com/wordpress/?p=2956)
9.2.12 / 1pm


[…] LW This entry was written by codybaldwin, posted on May 10, 2011 at 4:00 pm, filed under Uncategorized and tagged art, artists, british, Gerry Judah, sculpture. Bookmark the permalink. Follow any comments here with the RSS feed for this post. Post a comment or leave a trackback: Trackback URL. Next Post » […]
