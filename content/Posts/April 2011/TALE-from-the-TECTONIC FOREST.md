
---
title: TALE from the TECTONIC FOREST
date: 2011-04-16 00:00:00 
tags: 
    - 2012_Biennale
    - American_Pavilion
    - architecture_exhibition
    - Christoph_a__Kumpusch
    - design_concepts
---

# TALE from the TECTONIC FOREST


***Once upon a time, there was a proposal for an architecture exhibition:***


## ***THE TECTONIC FOREST****: ABSTRACT of a PROPOSAL FOR THE AMERICAN PAVILION, Venice Architecture Biennale, 2012*


**In contemporary world culture, the United States of America is often looked to for the most advanced thinking in many fields, including architecture. This proposal addresses this condition in several ways.**


**First, it exhibits the practices of emerging architects and firms from diverse regions of the United States. The overall aim is to show how these selected architects are transforming both architectural practice and the built environment, by offering a rich variety of innovative approaches and works.**


**Second, it employs an innovative exhibition strategy involving all-electronic displays of work. Visitors to the exhibition will use multiple tablet-type computers embedded in some of the Tectonic Trees in the Pavilion, as well as several with larger screens, to access files containing the work of the selected architects. They will be able to enter into the computers keywords from a prepared menu that will bring up works from any of the selected architects related to the keyword. Visitors will be able to interact with each other and will be linked to a website, *TECTONIC TREES*, allowing visitors to participate more fully in the exhibition. The [*sponsoring non-profit institute—redacted here*] will program a series of discussions and exchanges with the selected architects during the *vernissage* week*.* The result will be a lively atmosphere of exchange and exploration.**


**Third, it creates exhibition spaces that evoke the adventurous, often risky nature of innovation. The Tectonic Forest is a place where one can become confused and disoriented, even lost. At the same time, it is a place where one can make wonderful discoveries. A sense of mystery. A sense of play. Feelings of expectation. Feelings of accomplishment when expectations are realized. The visitor is not a passive spectator, but an active participant in making the exhibition.**


**The proposal will be realized by a highly experienced team. Their goal is to create an exhibition that will be a showcase for talented architects, enjoyable for visitors, and a unique expression of American innovation within the multi-national context of the Biennale. We have made a preliminary selection of eight firms from all regions of the United States (East, South, Midwest and West) and purposefully left the option to later include two more that might respond to the overall theme of the Biennale.The installation platform by the architects Lebbeus Woods and Christoph a. Kumpusch will be adaptable to accommodate the overall theme of the 2012 Biennale when it is announced later in the year.**


**by co-Curators:**


**[redacted]**


**Kimberli Meyer**


**Anthony Vidler**


**.**


*THE TECTONIC FOREST: conceptual drawings:*


**[![](media/tf-c-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-c-4.jpg)**


**[![](media/tf-c-31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-c-31.jpg)**


**[![](media/tf-c-51.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-c-51.jpg)**


**[![](media/tf-c-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-c-1.jpg)**


*THE TECTONIC FOREST: submission drawings:*


**[![](media/tf-p-0.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-0.jpg)**


*(above) Cover showing an image from a project by L.E.F.T, one of the invited architects for the exhibition.*


*(below) Submission drawings, showing the concept within the given limits of the American Pavilion. These limits are negotiable with the curators of the Biennale, allowing for a possibly  fuller realization of the conceptual vision:*


**[![](media/tf-p-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-1.jpg)**


**[![](media/tf-p-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-3.jpg)**


**[![](media/tf-p-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-4.jpg)**


**[![](media/tf-p-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-2.jpg)**


*(below) Construction drawings for the Tectonic Trees:*


**[![](media/tf-p-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-5.jpg)**


**[![](media/tf-p-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-6.jpg)**


**[![](media/tf-p-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-7.jpg)**


*(below) Details of shipping for the Tectonic Trees:*


**[![](media/tf-p-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-p-8.jpg)**


*(below) NEVER LOSE SIGHT OF THE ORIGINAL VISION….:*


**[![](media/tf-c-41-e1302990362458.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/tf-c-41.jpg)**


**.** 


***But then an unexpected thing happened. At eleven o'clock in the evening before the deadline for submission next day, when the submission package containing many forms, detailed budgets, production schedule, as well as installation design drawings was being assembled, the chairman of the board of the sponsoring non-profit institute called and said the board had changed its mind and would not support this proposal after all. This act of betrayal effectively killed the submission. You won't see The Tectonic Forest in Venice next year.***


***But the idea lives on.***


**LW and CaK**



#2012_Biennale #American_Pavilion #architecture_exhibition #Christoph_a__Kumpusch #design_concepts
 ## Comments 
1. Caleb Crawford
4.17.11 / 4pm


Bummer. No indication of why the waffle? Loss of nerve? or politics? (my top guesses).
3. Joshua Perez
4.17.11 / 5pm


Really unfortunate in a time where issues related to public space and our individual actions and reactions to it are so important. I was able to attend the Biennale last year and it was missing an intervention such as this, something not restricted to the boundaries of cordoned off areas delegated to specific nations or parts of the world. After all, this is what the biennale strives to achieve, isn't it? A collaborative inter-weaving of architectural thought and experience?
5. chris
4.20.11 / 3pm


As a proud american, I don't understand how you can even say that the US is at the forefront of advanced architecture. That's just not true. This country is actually reactionary in terms of architectural design and thought.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.20.11 / 5pm
	
	
	chris: I agree with you.  
	
	If you read what was written (with my input and consent) it says that the world often looks to the U.S. for advanced thinking in many fields, including architecture. The statement does not say that the U.S. delivers what people look for. Most often it does not and for the reason you give: as the leading capitalist nation, it is also a leading reactionary society—just look at the health care system, the gun laws, the rise of the Tea Party. Architecture in the U.S. is in a pathetic state, taking few risks and even when it does usually for the wrong reasons, such as marketing consumer goods of one kind or another.  
	
	It must be pretty clear that the design of the installation itself was intended as the primary manifestation of ‘advanced thinking.' Maybe it was too advanced for the non-profit institution that agreed to sponsor the proposal but then, at the last minute, backed out. Whether it is advanced or not deserves, I believe, some discussion.
7. chris
4.20.11 / 8pm


point taken,  

sorry to bite your head off





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.20.11 / 10pm
	
	
	chris: I like honesty and straightforward talk—saves a lot of time!
9. [chris teeter](http://www.metamechanics.com)
4.20.11 / 11pm


That plan reminds me of the Dianne Lewis article posted on your blog recently about the encounter Corb had with Einstein and the plan for some project effectively summarizing the thought culture of the time.


Speaking of Corb – tectonic forests – it also reminds me of a Iannis Xenakis music composition for an event such as this.


Either way a real bummer.
