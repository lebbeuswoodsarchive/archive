
---
title: THIS CANNOT PASS (updated June 22, 2011)
date: 2011-04-03 00:00:00 
tags: 
    - Ai_Weiwei
    - Chinese_government_repression
    - intellectual_freedom
    - political_dissent
---

# THIS CANNOT PASS (updated June 22, 2011)


## JUNE 22: AI WEIWEI is released from prison on bail: <https://lebbeuswoods.wordpress.com/2011/06/22/ai-weiwei-is-released/>


## JUNE 7: The art writer Charlie Finch has written a short but insightful article on Ai Weiwei's situation, which gives us information as well as proposing a plan of action: <https://lebbeuswoods.wordpress.com/2011/06/07/ai-weiwei-what-is-to-be-done/>


## MAY 18: An article from the Manchester Guardian gives us, at last, some word of Ai Weiwei's situation and condition: <http://www.guardian.co.uk/artanddesign/2011/may/16/ai-weiwei-physical-mental-health?CMP=twt_gu>


## This report is as strange as it can get, depicting a Kafkan detention without specific charges, by police without uniforms or official identification, in a mysteriously unprison-like setting. While Ai shows no signs of being physically tortured, the mental pressure on him is obviously extreme.


## One part of the article reports how an unidentified government official complains about “condescending” Westerners trying to tell the Chinese (government) how to run their legal system by demanding Ai's release. It might be true that Western protests about Ai will do little good or even harm his prospects. Or, it might be a case of the government mounting the best defense by launching a strong offense—an old and often effective tactic.


.


## MAY 3: A month has passed since Ai Weiwei's arrest and disappearance. Various protests against this blatantly repressive action by the Chinese government have been made, with little or no effect on their stance or silence about Ai's condition or prospects. Clearly, they are playing a waiting game, counting on the world's short attention span. But this cannot be allowed to pass.


## I am personally disappointed that more of my esteemed colleagues in architecture have not made a public protest in some form that would exert media pressure on the Chinese government to release news of Ai's condition, if not Ai himself. It is incomprehensible to me that architects with a public profile are not willing to risk something—future clients, public criticism, condemnation by other colleagues for “pulling a publicity stunt”—to help in a cause so obviously in their own self-interest, not to mention in support of a courageous artist who very much needs our help. I can only hope that the Chinese government's continued silence about Ai will stir them from their complacency at last.


## LW


## .


## Nothing has been heard from or officially about Ai Weiwei since he was arrested by Chinese government security police on April 3rd. See the post containing [Salman Rushdie's assessment](https://lebbeuswoods.wordpress.com/2011/04/20/salman-rushdie-dangerous-arts/) of the situation. Also, follow this link for speculation by Ai's sister on his possible fate: <http://www.theaustralian.com.au/news/world/letter-reveals-vanished-artists-deepest-beliefs/story-e6frg6so-1226041800256>


[![](media/aiweiwei-3-7-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/aiweiwei-3-7-11.jpg)


## *(above) Ai Weiwei in his Beijing studio on March 7. He was arrested today, April 3, 2011, on unspecified charges by Chinese security police.*


*.*
# **The Light Pavilion by me and Christoph a. Kumpusch is already under construction in Chengdu, China. I here state publicly that I will not accept another project in China until Ai Weiwei is released unharmed from detention or imprisonment.**


## **LW (April 3, 2011)**


# **Steven Holl has issued a statement for the immediate release of Ai Weiwei on [his website](http://WWW.STEVENHOLL.COM/news-detail.php?id=87&page=1): <http://WWW.STEVENHOLL.COM/news-detail.php?id=87&page=1>**


.
## **Article on Ai Weiwei from The New York Times, Tuesday April 5:  <http://www.nytimes.com/2011/04/06/arts/design/ai-weiwei-takes-role-of-chinas-conscience.html?hp>**


## **Article on Ai Weiwei's status as of Thursday April 7:  <http://www.nytimes.com/2011/04/08/world/asia/08china.html?_r=1&hp>**


## A world-wide protest occurred on Sunday, April 17:  <http://artsbeat.blogs.nytimes.com/2011/04/14/arts-group-calls-for-worldwide-sit-in-for-ai-weiwei/?scp=3&sq=Ai%20Weiwei&st=cse>


The following article is reprinted from the New York Times:
.
April 3, 2011
# Dissident Artist in China is Held as Crackdown Spreads


###### By ANDREW JACOBS



BEIJING — Chinese authorities on Sunday detained Ai Weiwei, one of the country's most high-profile artists and a stubborn government critic, as he tried to board a plane for Hong Kong, his friends and associates said. Mr. Ai's wife, nephew and a number of his employees were also taken into custody during a raid on his studio on the outskirts of the capital.


Rights advocates say the detentions are an ominous sign that the Communist Party's six-week crackdown on rights lawyers, bloggers and dissidents is spreading to the upper reaches of Chinese society. Mr. Ai, 53, the son of a one of the country's most beloved poets, is an internationally renowned artist, a documentary filmmaker and an architect who helped design the Olympic Bird's Nest stadium in Beijing.


Jennifer Ng, an assistant who accompanied Mr. Ai on Sunday morning, said he was taken away by uniformed officers as the two of them passed through customs at Beijing International Airport. Ms. Ng said she was told to board the plane alone because Mr. Ai “had other business” to attend to. She said Mr. Ai was planning to spend a day in Hong Kong before flying to Taiwan for a meeting about a possible exhibition.


A man who answered the phone at the Beijing Public Security Bureau on Sunday declined to answer questions about Mr. Ai's whereabouts and hung up.


Shortly after Mr. Ai's seizure, more than a dozen police raided the artist's studio in the Caochangdi neighborhood, cut off power to part of that area and led away nearly a dozen employees, a mix of Chinese citizens and foreigners who are part of Mr. Ai's large staff. By Sunday evening, the foreigners and several of the Chinese had been released after being questioned, according to one of Mr. Ai's employees, who was not in the studio when the public security agents arrived.


“It's not clear what they are looking for but we're all really terrified,” said the employee, who asked not to be named for fear of drawing the attention of the police. She said the police had visited the studio three times last week to check on the documents of studio's non-Chinese employees.


By targeting Mr. Ai, the authorities are expanding a campaign against dissent that has roiled China's embattled community of liberal and reform-minded intellectuals. In recent weeks dozens of people have been detained, including some of the country's best known writers and rights advocates. At least 11 of them have simply vanished into police custody. Two weeks ago, Liu Xianbin, a veteran dissident in Sichuan Province, was sentenced to 10 years on subversion charges.


Last week Yang Hengjun, a Chinese-Australian novelist and democracy advocate whose blog postings are avidly followed on the mainland, disappeared in southern China as he tried to leave the country. Mr. Yang reappeared four days later, claiming he had been ill, but many friends interpreted his cryptic explanation as a roundabout acknowledgment that he had been detained by the police.


Mr. Ai has had previous run-ins with the authorities. In 2009, while preparing to testify at the trial of a fellow dissident in Chengdu, the capital of Sichuan, he said he was beaten by officers who crashed though the door of his hotel room in the middle of the night. A month later, while attending an art exhibition in Munich, he was rushed to a hospital, where surgeons drained a pool of blood from his brain. Doctors said he would have died without the emergency surgery.


Last November he was briefly confined to his home in Beijing by the police, who he said were instructed to prevent him from attending a party in Shanghai he had organized to commemorate the destruction of a million-dollar art studio that had been built at the behest of the local government. Although he never found out who ordered the demolition, he said he suspected powerful figures in Shanghai who were likely angered by his freewheeling criticism of the government. .


Until now, Mr. Ai's stature has given him wide latitude in leveling public critiques against corruption and the strictures of Communist Party rule. Last year he created an Internet audio project in which volunteers read the names of nearly 5,000 children who were killed during the earthquake in Sichuan Province in 2008. The project, along with a haunting art installation in Germany composed of thousands of children's backpacks, were aimed at drawing attention to substandard construction that some experts say led to the collapse of many schools.


The most recent wave of detentions was triggered in February by an anonymous bulletin that originated on an American website urging Chinese citizens to publicly demand political change. The protest calls, inspired by the unrest in the Arab world, were effectively quashed by the authorities, who detained or questioned dozens of prominent reformers, lawyers as well as unknown bloggers who simply forwarded news of the protests via Twitter. At the time, Mr. Ai sent out a message that sought to dissuade people from taking to the street.


Nicholas Bequelin, a researcher at [Human Rights Watch](http://topics.nytimes.com/top/reference/timestopics/organizations/h/human_rights_watch/index.html?inline=nyt-org "More articles about Human Rights Watch") in Hong Kong, described the ongoing crackdown as an attempt by the country's public security apparatus to rollback the modest civil society advances that have taken root in recent years. “It's an attempt to redefine the limits of what kind of criticism is tolerable,” he said. “The government is moving the goalposts and a lot of people are finding themselves targeted.”


After his beating at the hands of the police in 2009, Mr. Ai said he had no illusions about the consequence for those who refused to toe the line set down by the country's leaders..


“They put you under house arrest, or they make you disappear,”‘ he said in an interview. “That's all they can do. There's no facing the issue and discussing it; it's all a very simple treatment. “Every dirty job has to be done by the police. Then you become a police state, because they have to deal with every problem.”



#Ai_Weiwei #Chinese_government_repression #intellectual_freedom #political_dissent
 ## Comments 
1. Pedro Esteban
4.3.11 / 9pm


This need to end!
3. [Jeffry Burchard](http://architectureev.blogspot.com)
4.4.11 / 12am


Architect's have and probably always will be willing to turn a blind eye toward the unethical and even inhumane tendencies of their clients in the interests of the professed importance of building “new” buildings. Today, China, though not necessarily the direct client, IS still the pre-eminent hotbed for young architects to develop their ideas at a one-to-one scale, and sadly it will take more than the arrest of Ai Weiwei to deter most architect's from working there. I am very glad that you have made this resolution!
5. Dustin
4.4.11 / 4am


I congratulate you for your resolution. I hope that you will not be the only influential figure to stand up against this.
7. [architecture - Zeichen](http://architektur.kaywa.ch/architektur-u-staedtebau/zeichen-1.html)
4.4.11 / 7am


[…] until Ai Weiwei is released unharmed from detention or imprisonment. LW (Der New Yorker Architekt Lebbeus Woods – Gründer des Research Institute for Experimental Architecture (RIEA)) Der Grund? […]
9. [赵雷](http://www.smartwoodhouse.com/)
4.4.11 / 12pm


谢谢你对艾神的支持。  

THANKS !
11. Waterboy
4.4.11 / 1pm


Thank you so much for your support. 


Thank you!


from China
13. [ss](http://5525990)
4.4.11 / 1pm


加油 艾老师！！！！！  

谢谢所有人！！！
15. Ben Dai
4.4.11 / 1pm


Thanks.
17. if
4.4.11 / 1pm


Many thanks!Everyone loves weiwei ai!
19. 刘洪军
4.4.11 / 1pm


中国人基本上都沉默了，只有在国外才能看到人性的一面。


说谢谢显得太傲慢，只有敬佩！





	1. 邹冉
	4.7.11 / 7pm
	
	
	不是这样的  
	
	在国内几乎没有有关他的报道 全部的被屏蔽掉了  
	
	所以知道他的人很少  
	
	我个人对他仅有的了解来自网络 而且这网络还经常的被攻击
21. Raintea
4.4.11 / 1pm


給力！  

Thank you very much！
23. Sainchi
4.4.11 / 1pm


Many thanks for your support.
25. tofu
4.4.11 / 1pm


Who cares, who do you think you are? LOL
27. 古灵
4.4.11 / 1pm


Thank you
29. Ming
4.4.11 / 1pm


thanks for supporting. Shame on CHN Government.
31. [jilu wu](http://THANKS！)
4.4.11 / 1pm


THANKS！
33. Jack
4.4.11 / 1pm


We thank for your calling!


Jack  

Shanghai, China
35. aiww
4.4.11 / 1pm


thanks you
37. Jeffrey Li
4.4.11 / 1pm


Thank you very much!
39. Anne Zeng
4.4.11 / 1pm


Thank you so much for supporting ai weiwei!
41. wang
4.4.11 / 1pm


THANK YOU VERY MUCH!
43. 记得
4.4.11 / 1pm


谢谢！
45. limon
4.4.11 / 1pm


Thank you from China.
47. ogawa
4.4.11 / 1pm


thank you for your support!! this needs to end.
49. Less is more
4.4.11 / 1pm


pillars are key part of constructions.Some right-hearted people including you,are pillars that support justified roof for human.Appreciate your resolution.There are always people in the world can not be bought by the biggest but smelly wallet.
51. Less is more
4.4.11 / 1pm


As a construction,pillars are key part of it.Some right-hearted people including you,are pillars that support justified roof for human.Appreciate your resolution.There are always people in the world can not be bought by the biggest but smelly wallet.
53. [Diane](http://www.dianegatterdam.com)
4.4.11 / 1pm


RIGHT ON!!!! So great and a strong statement to the pathetic Government of China


Diane NYC human rights activist
55. LeMonde
4.4.11 / 1pm


Thank you for you brave support in this special period.
57. [马腾](http://www.mabt.tk)
4.4.11 / 1pm


爱神被关，实在是国内政府的一种遮掩，我希望国外全部不和我国政府认识合作，我看某党妈妈怎么办呢？
59. [Sander Woertman](http://www.dearchitect.nl)
4.4.11 / 1pm


We support your reaction and are encouraging all Dutch (and international) architects to follow your example on our website and LinkedIn Group!
61. 艾的门徒
4.4.11 / 1pm


同谢！互勉！
63. [Jack](http://facebook.com)
4.4.11 / 1pm


Free Ai Weiwei
65. [马腾](http://www.mabt.tk)
4.4.11 / 1pm


晕，打错字了，我希望国外全部不和我国政府合作，我看某党妈妈怎么办呢？
67. liren
4.4.11 / 1pm


Thanks！
69. 中国屁民
4.4.11 / 1pm


谢谢你对我们国家最伟大艺术家的支持！
71. Matthew Chen
4.4.11 / 1pm


Thanks for doing the right thing…
73. [OX](http://www.myoxs.com)
4.4.11 / 1pm


Thanks lot！
75. Matt
4.4.11 / 2pm


谢谢你。
77. HDWu
4.4.11 / 2pm


Thanks ,Mr.Woods.  

Hope there will be more artists and architects show their support for Ai Weiwei in real action.
79. [bigasong](http://thankyou,aiww'sfriend)
4.4.11 / 2pm


We all aiww's friends
81. cor
4.4.11 / 2pm


As a chinese resident in Chengdu, I support your decision.
83. patz
4.4.11 / 2pm


Many thanks from China!
85. wing
4.4.11 / 2pm


Wish your renouncement will facilitate the release of Mr. Ai. Chinese gov is usually much more afraid of foreigners, especially Westerners, than of Chinese. It is a shame we could not fight for our freedom. Thank you, anyway.
87. max
4.4.11 / 2pm


appreciated !!!
89. ashuraiw
4.4.11 / 2pm


GOD PLS SAVE CHINA SAVE VIVI
91. [llision](http://blog.llision.cn)
4.4.11 / 2pm


I really can not understand what is the PRC government doing…
93. null
4.4.11 / 2pm


Thank you
95. lin
4.4.11 / 2pm


a lot thanks for all of you
97. randy
4.4.11 / 2pm


U r a truely friend of Chinese. Many thanks!
99. yc
4.4.11 / 2pm


释放爱神！！！！！！！
101. davidhuang
4.4.11 / 2pm


Thank you for your support to AIWW
103. lotus
4.4.11 / 2pm


This need to end! Our tolerance is limited! Release Aiweiwe!
105. lakin
4.4.11 / 2pm


You set a great example for me, as a responsible architect. Thank you for your concern about the situation of human rights in China.
107. rachel jin
4.4.11 / 2pm


let more and more chinese youth know human right and chinese goverment truth.
109. T.K from China
4.4.11 / 2pm


I'm so glad that you have made that decision.  

We all hope Mr. Ai will be alright.  

We all hope that China will be better soon.  

In face China has a large numbers of dissidents.  

But mostly are threatened by the freaking government.
111. todd
4.4.11 / 2pm


释放爱神！！！！！！！！！！！！！！！！！！！！！
113. ver
4.4.11 / 2pm


你们才是推动我们民族真正发展的人！  

政府是个p啊！  

谢谢！
115. sun
4.4.11 / 2pm


谢谢
117. Mou
4.4.11 / 2pm


I appreciate your courage!
119. xianql
4.4.11 / 2pm


Support you!
121. [watchzerg](http://www.watchzerg.com/)
4.4.11 / 2pm


Thank you, thank you for all you have done, for this abandoned land.
123. Chinese
4.4.11 / 2pm


As Chinese I appreciate your support for Ai. And I am shamed for China.
125. [陈狗二](http://t.sina.com.cn/puerxilian)
4.4.11 / 2pm


支持. 不放弃. 不倒下.
127. Clover
4.4.11 / 2pm


Thank you for support him!!!
129. [隔壁老王](http://michelange10.wordpress.com)
4.4.11 / 2pm


释放我们的胖老头
131. LoveWW
4.4.11 / 2pm


THANK YOU!  

FREE AIWW
133. Julia
4.4.11 / 2pm


As a human rights activist working on China, my heart is gladdened to hear of your stance to support Ai Weiwei, whose own work, such as his project to give a voice to the names of children who were killed by shoddy construction in the 2008 earthquake, is motivated by his huge heart for his fellow Chinese citizens. 


It is my hope that many other influential individuals will follow suit. Thank you!
135. chinese
4.4.11 / 2pm


just 顶.
137. aiweiwei
4.4.11 / 2pm


Thank you！！
139. Gr
4.4.11 / 3pm


Thanks for doing the right thing！
141. 道哥
4.4.11 / 3pm


多谢你的支持!Thank you
143. ppli
4.4.11 / 3pm


many thanks from china!
145. Chinese
4.4.11 / 3pm


Thanks you for your support
147. david
4.4.11 / 3pm


I really appreciated everything you did for Ai Weiwei.
149. [吉软糖](http://jiblog.jiruan.net)
4.4.11 / 3pm


感谢国外友人


我们要给一个中共放人的理由，即便它不言自明。但对于这个流氓政权来说，我们必须说出来，公开地说出来。


建了一个关于艾未未的博客  

<http://loveaiww.blogspot.com/>
151. Will
4.4.11 / 3pm


Lebbeus – I applaud your efforts to bring attention to this blatant repression of basic human rights. I hope that your statement causes other international architects (such as your friend Steven Holl) to likewise take a strong stand against such repression.
153. [Art Social Pages | Blog | Art and Politics: Ai Weiwei Arrested in Beijing, Prompting International Outcry](http://artsocialpages.com/2011/04/04/art-and-politics-ai-weiwei-arrested-in-beijing-prompting-international-outcry/)
4.4.11 / 3pm


[…] international image, New York-based experimental architect and Cooper Union professor Lebbeus Woods announced publicly that he would not accept another commission in China until Ai Weiwei was released, making waves in architectural circles. With Christoph A. Kumpusch, Woods is currently working on a […]
155. timlan
4.4.11 / 4pm


Thank you for supporting us.Freedom!
157. George Knight
4.4.11 / 4pm


The world community has an obligation to stop the intimidation of citizens in China. Otherwise to end all contacts. You've made a first step. Brave, thanx.
159. Chinese
4.4.11 / 4pm


Thank you！
161. Zion luo
4.4.11 / 4pm


Thanks!
163. macmini
4.4.11 / 4pm


Thanks a lot！We are Chinese ，and we want freedom too.
165. GhostZodick
4.4.11 / 4pm


All Chinese artists silenced about Ai weiwei. But you, as a foreigner, not. I salute you!
167. Kevin
4.4.11 / 5pm


gratitude
169. rov
4.4.11 / 5pm


free aiww
171. Brandon Bergem
4.4.11 / 7pm


You're doing the right thing, i fully support this.
173. [Jeffry Burchard](http://architectureev.blogspot.com)
4.4.11 / 7pm


On other sites/blogs today I have seen that some detractors are suggesting that Lebbeus Woods and Christoph a. Kumpusch are pulling a publicity stunt on the back of a very serious event. For those people I have a simple question… IF your accusation is correct please explain to me how this is more unethical that the standard day-to-day exploitation of China's available capital (money and workforce), via an absolute innatention to human-rights, in order to proliferate heedless adventures of narcissistic architectural forms imagined up in rather traditional/normative ways by firms both corporate and boutique?


I have no intention of suggesting that the Chinese goverment must or even should conform to the ideals of a Western society. That is a rather prosaic and naive compulsion. However, what IS disconcerting and downright savy are the abilities of architects to employ western concepts in the deployment of chinese projects. The ubiquitous clap-trap smacks of intolerable irony. For instance see this 2009 press announcment for the Central Business District by SOM – <http://www.som.com/content.cfm/pr_som_wins_beijing_cbd_urban_planning_competition>  

In the provided video, EIGHT PRINCIPLES are provided as the conceptual basis for the master plan. They are as follows:  

1) Great Minds  

2) Connectivity  

3) Great Parks and Green Boulevards “lands of inspiration”  

4) The Center “human touch”  

5) Intelligent Infrastructure  

6) The City Block  

7) Vibrant Districts  

8) Balanced with Nature.


The goals are laudable AND laughable given the rencent events in China (not just the arrest of Ai Weiwei.)


To NEVER work in China is doubtfully a good solution. To use Architecture, in all of its deployable variation, one of which may be the refusal of participation) as a measure to effect the betterment of human conditions everywhere IS a lofty aspiration. I am no expert on Chinese politics and society, but Architect's should be reconsidering the role of that great nation as it relates to the progression of the discipline.
175. Bugregen
4.4.11 / 9pm


thanks for your support! 


Free aiww
177. [One Artist Detained; Another Released | China Digital Times (CDT)](http://chinadigitaltimes.net/2011/04/one-artist-detained-another-released/)
4.4.11 / 11pm


[…] A note on Woods' personal blog states: “The Light Pavilion by me and Christoph a. Kumpusch is already under construction in Chengdu, China. I here state publically that I will not accept another project in China until Ai Weiwei is released unharmed frolam detention or imprisonment.” […]
179. ray
4.5.11 / 12am


i search from tencent MicroBlog of zuoxiaozuzhou. I hope he will be ok. I suppose you.
181. 马研
4.5.11 / 12am


释放艾未未海报http://www.flickr.com/photos/61377755@N05/5589024513/
183. dazed
4.5.11 / 2am


❤


seriously?
185. damien
4.5.11 / 2am


that fellow dissident in chengdu whom ai weiwei testified for is called Tan Zuoren（谭作人）
187. [FREE AI WEIWEI « archipression](http://archipression.wordpress.com/2011/04/05/free-ai-weiwei/)
4.5.11 / 3am


[…] <https://lebbeuswoods.wordpress.com/2011/04/03/7669/> […]
189. [# POLITICS /// Ai Weiwei incarcerated for the second time | The Funambulist](http://thefunambulist.net/2011/04/05/politics-ai-weiwei-incarcerated-for-the-second-time/)
4.5.11 / 4am


[…] Woods has written on his blog that he will refuse any new projects in China until Weiwei is liberated (he could have canceled the […]
191. [世界各国媒体对艾未未被拘事件的报道Global Reports on AiWW's Detention - 艾未未研究](http://aiwwstudy.appspot.com/42004.html)
4.5.11 / 5am


[…] Woods: THIS CANNOT PASS. (翻 […]
193. [One Shining Moment | Advertise Opportunity](http://advertise-opportunity.com/?p=4716)
4.5.11 / 6am


[…] THIS CANNOT PASS « LEBBEUS WOODS […]
195. Kuangjuan
4.5.11 / 6am


Thank U! I am an chinese architect,I appreciate you for your action。Free Ai Weiwei！
197. dy
4.5.11 / 6am


Thanks for your help to Mr. Ai. This is crucia to Chinese.
199. 0lla
4.5.11 / 9am


Thanks!  

But why only you? What other architects are thinking?
201. huangdou
4.5.11 / 12pm


pray for aiweiwei !
203. [james richards](http://jrich99145@yahoo.com)
4.5.11 / 4pm


Thanks LW


Pray for Ai Weiwei and all of the others that have been rounded-up by the police state that is CHINA.
205. [艾未未 – 维基百科，自由的百科全书 | 段海新@NISL@TU](http://netsec.ccert.edu.cn/duanhx/archives/1459)
4.5.11 / 4pm


[…] THIS CANNOT PASS. Lebbeus Woods. 2011-04-3 […]
207. Rodger
4.5.11 / 4pm


Thank you！
209. [Mark Lamster: The Plight of the Political Artist | Obsessed By](http://www.obsessedby.com/2011/04/05/mark-lamster-the-plight-of-the-political-artist/)
4.5.11 / 8pm


[…] a post on his blog entitled This Cannot Pass, Lebbeus Woods, who might be the conscious of the architectural profession, has vowed to accept no […]
211. Matthew Johnson
4.5.11 / 9pm


Thank you for this important statement.
213. [04: This Cannot Pass | books, notes, thoughts, life and all.](http://nuankhanit.wordpress.com/2011/04/05/04-this-cannot-pass/)
4.5.11 / 10pm


[…] a post on his blog entitled This Cannot Pass, Lebbeus Woods, who might be the conscious of the architectural profession, has vowed to accept no […]
215. [Stephen](http://skorbichartist.blogspot.com/)
4.5.11 / 11pm


If a society is not free to think, to imagine, to question, to create then it is doomed.  

It is time for the Chinese government to change.  

Free Ai Weiwei, free China!
217. [艾未未 Ai Weiwei « Hacksperger's Blog](http://hacksperger.wordpress.com/2011/04/06/%e8%89%be%e6%9c%aa%e6%9c%aa-ai-weiwei/)
4.6.11 / 8am


[…] THIS CANNOT PASS. Lebbeus Woods. 2011-04-3 […]
219. [Lewis Wadsworth](http://lewiswadsworth.net/)
4.6.11 / 6pm


Ai Weiwei's seizure has deeply disturbed me. Of course I am concerned for the artist's well-being. But what does this sort of behaviour, on the part of the PRC, bode for a world where – almost any day now it seems – China is destined to become is preeminent economic, military, and cultural power?


Yes, it seems definitely a correct stance to refuse to undertake further projects in China until Ai Weiwei is released, Mr. Woods. However – and I hesitate to suggest an alternative approach, for several reasons: because I have a great deal of respect for you and your work – I collect first-editions of your books, in fact – and I don't wish to offend you in the slightest…and because I can't understand all of the facts bearing on your decision. And because I do not know if the general phases in building construction in China are at all similar to those typical for architect-designed projects in North America, and therefore I do not clearly understand how much an architect must be involved when a work is “under construction” there. But assuming a certain amount of administration or approval of changes is still necessary during the remaining construction phase of your pavilion project, would it not be more effective a form of protest to simply refuse tor provide any CA or further services? …or even to renounce (or threaten to renounce, with a firm deadline for carrying out that threat) authorship of the design, even if that is largely a rhetorical gesture? 


I've worked on very small and very large architectural projects, and I am under no illusions as to the role of the designer once construction documents have been approved and the building begun. Most builders, and possibly many clients, would likely prefer the architect withdraw himself or herself at that point form the work. It can go on without the architect, although we like to imagine that without the approval and supervision of the designer the work's intergrity will be irrevocably compromised on mulitple levels. I am also aware that there must be local Chinese architects involved with your project, too, who could probably supply many essential services necessary to complete the project without further assistance from the foreign partner.


Still, the gesture of a strike or work-stoppage, rhetorical or not, has a certain impact. Can you and Mr. Kumpusch not simply do that, now, and threaten to wash your hands of the pavilion unless the authorities release Mr. Ai? Why can't Mr. Holl do that, as well, with his apparently innumerable grand schemes underway in the PRC…or for that matter, why can't any of the other Western firms engaged in architectural projects there? I suppose “architectural sanctions” are as likely to be ineffective as economic sanctions in forcing a powerful nation-state to behave according to basic humanistic principals. But still….wouldn't it be the better thing to do?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.10.11 / 4am
	
	
	Lewis Wadsworth: The Light Pavilion is finished, except for the installation. No need for me anymore on the project, so ‘resigning' from it would be a hollow gesture, indeed, and one that would hurt a lot of good people, many of them Chinese. Not putting my name on it would be denying my responsibility for its design, hardly an act of personal integrity. The only card I have to play is future work, which is represented by a number of offers that have recently come from from potential clients in China. I'll willingly sacrifice those in the cause of freeing Ai Weiwei.
221. [goochin](http://www.oik8.com)
4.7.11 / 4am


一如既往的支持。哪有暴君，哪里就有扔石头的人
223. hen
4.8.11 / 3pm


前来支持。
225. Xiaohu Wang
4.8.11 / 7pm


Thank you very much  

Free our Ai Weiwei
227. Kroy Wen
4.9.11 / 3am


Leave the Chinese alone… there is a saying, ” a cat who likes to smell his own shit, will one day be made to eat it”. Al its time to eat it…





	1. L. L.
	4.10.11 / 11pm
	
	
	so, when are you going to eat yours?
229. [世界各国媒体对艾未未被拘事件的报道 | 中国数字时代](http://chinadigitaltimes.net/chinese/2011/04/%e4%b8%96%e7%95%8c%e5%90%84%e5%9b%bd%e5%aa%92%e4%bd%93%e5%af%b9%e8%89%be%e6%9c%aa%e6%9c%aa%e8%a2%ab%e6%8b%98%e4%ba%8b%e4%bb%b6%e7%9a%84%e6%8a%a5%e9%81%93/)
4.9.11 / 8pm


[…] Woods: THIS CANNOT PASS. […]
231. David Ting
4.18.11 / 5pm


I appreciated for your decision. I hope we can find Ai wewe in close future.
233. [Who's afraid of Ai Weiwei? « being vague is almost as fun as this other thing](http://coffeedubs.wordpress.com/2011/04/29/whos-afraid-of-ai-weiwei/)
4.29.11 / 12pm


[…] Holl and Lebbeus Woods have released statements in which they condemn the actions of the Chinese government, and yet, in […]
235. [leezee](http://zhongg)
5.14.11 / 6pm


中國政府像個狗屎一样的 妈的比 为什么不能自由言论 被破坏了 这小伙子被迫害了！ 哎 遗憾了！
237. [AI WEIWEI IS RELEASED! « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/06/22/ai-weiwei-is-released/)
6.24.11 / 9pm


[…] For previous posts on Ai Weiwei, see: https://lebbeuswoods.wordpress.com/2011/04/03/7669/ […]
239. [THE CHINA PHENOMENON « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/03/20/the-china-phenomenon/)
3.20.12 / 12pm


[…] paradox in this is that China is a politically repressed society. Remember Ai Weiwei? And the many other rights' activists silenced by police brutality and detention? How can […]
241. [Siddid Srivastava](http://twn-ewrestling.com/omega/index.php?action=profile;u=22991)
10.1.12 / 10am


Wonderful blog! I found it while browsing on Yahoo News.  

Do you have any suggestions on how to get listed in  

Yahoo News? I've been trying for a while but I never seem to get there! Thanks
