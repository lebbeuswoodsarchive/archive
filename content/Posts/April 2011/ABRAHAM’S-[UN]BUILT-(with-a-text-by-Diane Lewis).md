
---
title: ABRAHAM’S [UN]BUILT (with a text by Diane Lewis)
date: 2011-04-12 00:00:00 
tags: 
    - architecture
    - architecture_books
    - Diane_Lewis
    - Raimund_Abraham
---

# ABRAHAM'S [UN]BUILT (with a text by Diane Lewis)


[![](media/ra-swimming1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-swimming1.jpg)


*(above) Raimund Abraham swimming in a lake in Mexico c.2009. Photo by artist [Joan Waltemath](http://www.mica.edu/news/joan_waltemath_named_director_of_hoffberger_school_of_painting_.html).*


The architect [Raimund Abraham](http://en.wikipedia.org/wiki/Raimund_Abraham) died tragically in an automobile accident in Los Angeles on March 4, 2010. He left behind an impressive, if not vast, body of work consisting of visionary urban projects, utopian houses, designs for projects never realized, as well as built projects of innovative design and uncompromised principles. It is a body of work instantly recognizable as by this singular architect, that serves as an inspiration for any architect, whatever his or her ideals, who aspire to a life of dedication to and passion for the art and discipline of architecture. It is very good news, therefore, that the book of the work and thought of Raimund Abraham, which he personally oversaw, originally published in 1996, has recently been published in a new, expanded edition, entitled *[[UN]BUILT](http://www.amazon.com/Raimund-Abraham-BUILT-Brigitte-Groihofer/dp/3709104688/ref=sr_1_1?ie=UTF8&qid=1302550069&sr=8-1)*, covering his works up to the present.


This new edition, edited as was the original by Brigitte Groihofer, is in the same straightforward, highly legible graphic format as the original, and is packed with information, both visual and textual.  It includes Abraham's own terse writings, as well as the original essays of Kenneth Frampton, John Hejduk, and myself. New essays by Frampton, Guy Nordensen, Carlos Brillembourg, Wolf D. Prix, and others have been added for this new addition and round out a contemporary critical view of his work and thought. A fine essay by Abraham's long-time, close colleague Diane Lewis was commissioned then inexplicably omitted, and is therefore included in this post. The politics of writing history can sometimes deprive us of important existential perspectives that here I hope to correct.


Abraham was an unusually scrupulous architect, publishing only a few books in his lifetime. *[UN]BUILT*, the Second Edition is destined, I believe, to become the single most authoritative source for his work in the decades to come.


In order to introduce the book, I have decided to forego the convention of showing samples from throughout the book and will instead show only one of the many projects, realized and unrealized, the series *Loci Ultimi: The Last Abodes of Mankind*. Very difficult to find anywhere but in this book, this project shows the depth to which the editor has developed all the projects, a degree that can only be called definitive.


LW


*(below) Table of Contents for [UN]BUILT:*


*[![](media/ra-book-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-1.jpg)*


*Project by Raimund Abraham, LOCI ULTIMI: The Last Abodes of Mankind:*


*[![](media/ra-book-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-2.jpg)*


*[![](media/ra-book-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-3.jpg)*


*[![](media/ra-book-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-4.jpg)*


*[![](media/ra-book-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-5.jpg)*


*[![](media/ra-book-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-6.jpg)*


*[![](media/ra-book-71.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-71.jpg)*


*[![](media/ra-book-81.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-81.jpg)*




*[![](media/ra-book-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/ra-book-9.jpg)*


.


## **[DIANE LEWIS](http://cooper.edu/architecture/faculty2/diane-lewis/)**


## **ANTILEGOMENA: The Disputed Texts**


.


On August 31, 2010 I began writing this text with the Acropolis in view. Surrounded by


the severity of Athens, I am en-route home, to New York, the city that Raimund most


identified with, and where I met him at Cooper Union in 1972, thirty- eight years ago.


It is a reward to put pen to paper here in Greece; a hallowed place that resonates with his spirit; the site of his unbuilt project for the Parthenon Museum.


Raimund taught me that architecture is as rooted in the rites of sacrifice, as it is in the


rites of consecration.


In all actions, Raimund was determined to achieve “authenticity”. His works address the


nature of architectural memory and interrogate the origins of architecture.


The imagery, and character of his every mark on paper, impart a sense of the archaic


to visions of the present and the future. Raimund's intrinsically sturdy and inventive


structural expressions are more forcefully the architecture –of –and – for – the – mind.


An existential dimension penetrates his work and his words.


He achieved a unique position through his severe and unrelenting search, which stands


as a definitive critique of this era.


He presented the idea of architectural memory in lieu of history as the focus of concern.


His radical and poetic resistance to the postwar attempt to relegate the raison d'etre of


the modern movement to functionalism took a subtle form. He knew and had witnessed


more- the total embrace and implementation of abstraction. He participated with a spirit


that generated a forum of his peers.


His spirit emerges now to illuminate the somewhat submerged aspects of the civilizing


issues taken on from the 60s to the present. He envisioned origins- the cut into the earth,


the city as architecture, the autonomy of form and program, and the civic structure as


house.


These examinations evolved in a profound vocabulary of form and character of form that


carries a fragrance of the enlightenment and impacts today's view of everything from


Kiesler to Kahn to Mies.


Now his insights are more pressing to all architects who are sympathetic to, and interested in the causes, the philosophy, and the freedoms he illuminated and supported.


I observed his ability to integrate circumstance, restraining conditions and the daily facts


of practice, while always endeavoring to derive a continuing dialogue within the historic


memory of the discipline. This demanding program for his existence effected the sometimes dramatic oscillations of his character, and behavior in public proclamations and critiques, that no matter how unexpected were earned and grounded in natural authority, hard won.


Raimund was often very solid and supportive to those he chose to have around him, and


if he wished, he was able to sustain perfect “form” in any situation. The bohemian urbane style was an overlay on the precise intellect that made him capable of explaining all pertinent legal and social hierarchies and underpinnings to any action at hand, and persevere to practice architecture around the world with a unique method that insured his direct hand and eye on all that he engaged and constructed.


As a friend he was at times- all seeing, and capable of offering great comfort, but he was


simultaneously apt to assert an unexpected component of the threatening, tragic, zany,


or bacchanalian.


He had magic.


Impassioned and precise on the relationship among the arts that are notational, he recognized that architecture, literature and music are linked. This spawned his commitment and concern for the definitive quality of the plan and a strict attention to the poetics of the plan and section, as seminal to the quality and significance of the architecture.


He communicated thought with a critical austerity that was embodied by his physical


presence, his energy and the precision of his words. His English was unequaled- he used it as a sharp tool to inspire action and imagery. The words were visceral and physical as he orated them. His influence in all spheres of architecture has stretched the critical horizon of our time, whether it be spoken, written, constructed or “un- built”.


Beckett said that one's freedom is in the quality of their language.


Abraham confronted the world with this ethos. The evidence are the drawings that reflect


his imagination and demonstrate the exquisite consideration with which he conceived


each element of his structures.


In the 80s, I began to recognize that Raimund had a deep insight and appreciation of the


free plan spatial implications in Corb's work. He was able to accompany this syntactical


ability with a unique resonance to the late works of Le Corbusier, where an almost


abstract expressionist rusticity and directness of construction was the model. Raimund


understood that touch and that model, and was among the likes of Rudolph and Kahn,


who were able to shed any sense of commercial convention in the finishes or fabrication


processes in his buildings. This continued into his late innovations including the use of


digital fabrication methods- he was always excited by cutting edge processes of


fabrication, full-scale-that he could nuance masterfully.


Raimund had a fantastic touch in construction –


he could turn lead into gold. Any material, no matter how humble, was in his hands,


transformed with a set of exquisite details and radical tectonics.


This was a great and voluptuous talent. Simply constructed projects were enriched by the most basic decisions; a soffit, a hinge, a thickness, a joint, an unexpected dimension – all with a direct existential head-on approach; a concreteness, a powerful structural and material honesty. He is among the rare individuals that were able to directly continue the formulation and invention of architectural elements rooted in the structural innovations, anti- formalist symmetry of Mies, and free sections of Le Corbusier, with philosophical principle, and without stylistic simile.


I was always alert when he spoke of the necessity for and the deep value of “labor”.


One felt that the manner in which he pronounced the word labor, which in his mouth


carried the weight of the labor he had actually devoted to his art.


He professed a difference between participating in the discipline as opposed to the profession of architecture. Having never relinquished that aspiration, and sacrificed in every aspect of his life in order to do so, he succeeded in asserting and requiring a continuity of architectural conscience in everyone he chose to engage. He sustained what I will call, an “inside” to his work. He invested his love of the work, sustaining his devotion to the inception of his projects with his pride in directly-making models alone, his hand in the drawings.


The autonomy he engendered from these procedures was profound.


He asserted an uber- excellence about all that he observed, enjoyed, discovered and


selected. This self- inflicted pressure for the esoteric, propelled him to continuously select and edit a set of friends, colleagues, and students who were able to enjoy participating in the existential self -searching that he required of them and of himself. He drew all the boundaries, and demonstrated the difference between comfort and pleasure.


He enunciated the search for what differentiates architecture from building, and designated those who were part of this endeavor.


Raimund protected architecture from the practitioners.


He wasn't interested in history as a past; more the relation between memory and


civilization.


He explored the immortality implicit in poetic syntax;


Invested the plan and section with that task.


Autumn of 1972 was the first and unforgettable time that I saw Raimund Abraham.


He was standing among a group of “grey flannel suits” at Cooper Union. He was wearing


a light blue Oxford shirt with an ascot, a vest, and had a cigar in hand. He shared the


spirit and posture of the art school critics of the time. They were the painters, filmmakers,


poets and conceptual artists who sat over at St Adrien's Company at the Broadway


Central hotel; Haacke, Beckman, Hewitt, Newman. He was refined, groomed, and alien to any aspect of the American corporate architects. Refined, literary, didactic, very vocal,


and charismatic – silent when the discussion moved toward the vocational or prescriptive. Later I understood that he could build exquisite structures and simultaneously inspire ideas, projects, and debate. Although Robert Slutzky was the in- house artist of the architecture faculty, Raimund was an immediate, inadvertent, and indirect counterpoint to Bob's somewhat academic and pre-60's bias on form. He was refreshing, philosophical, reflective and méchant.


Raimund quoted Wittgenstein; “What can be spoken can not be shown”.


He pronounced the word shown with three syllables. Knew Mallarme, Joyce, Kleist- saw


the fine points of surrealist literature, knew the Quattro Libri drawings in detail, owned


rare books of wood and masonry joints, and recognized the difference between Cinema


from movies, and loved both.


In 1972, he personified the aura and challenges of the auteur of the sixties revolution.


He approached each project with both philosophical and concrete challenges to the


making of form. The imaginary and the structural. From the time of his arrival at Cooper


into the 70's, he was a rock and an inspiration in the studio. Raimund spanned thirty


years here – as fresh and innovative a force in 2002 when he left, as in 1972.


Artistic strength, autonomy, fragility, and privacy, were qualities he wished to support


and protect. He worked to avoid any transgression of these boundaries with students or


colleagues even in dramatic moments of confrontation, of which there were many.


An emphasis on the poetic vision of each student was acknowledged within the tone of


critique. This aspect of teaching was shared and protected with John Hejduk, and with


me, and remains controversial. The idea that there are some things that need to be left


unsaid, and that a sacrosanct quality can be protected for the sake of the advancement


of an individual vision, must weather their absence. It is rare that there is such a tacit


agreement to perpetuate or appreciate such a climate of artistic laboratory.


In the early years up into the 90s-Abraham's presence provided a subtle support for


John Hejduk's move from what he called “the architecture of optimism”, which was the


transition from his projects derived form imagery of Neoplasticism, into his “architecture


of pessimism”, around the early and mid 70's where the existential aspects of participating or resisting were in play- across the board. At this time Raimund's great drawing of sliced landscape hung on the wall of Johns office. They were inspiring, and bonded in a search and a freedom. This was a great engine of creative energy at Cooper Union and in New York in the 70's and early 80s.


Raimund's presence in this bond at that time, guaranteed the diverse and weighty environment that was a magnet for artistic engagement with Rossi, Scolari, Tafuri, Pesce, and later, the participants of the IBA projects in Berlin. Eisenman was a force in this magnet too, and together they provided a forum specific to downtown Manhattan. This was the rich period of vision and debate in which I studied and subsequently entered the faculty at Cooper in 1982.


One beginning for Raimund in New York was his work with with Kiesler in the 60s.


Another was his respect for Abe Geller.


His love bordering on envy of New York “street –smarts”, led to his exciting regime for life here. It was astonishing how deeply he was able to “ read” New York. He had an acute understanding of all the subtle political differences among newspapers, the baseball teams, and the religious and social biases. The city was his studio.


He set an example for work with total absorption and solitude within New York.


He proved that one individual could do their work and project it from its inception in the


hand to its entry into the world without any adulterating forces. He engaged directly and


intimately, with each and every project no matter how large or small. His work, always


done with his hands on the models and in the drawings – at the saw, at the cutting table,


at the drawing board, was interrupted occasionally, for a good meal, a walk, a cigar.


I witnessed the Seven Gates of Eden Project at the Zattere of the 1976 Venice Biennale.


The great linear vaults and bronze light concretized his touch in a manner I had not understood in the context of New York before seeing Italy. I instantly recognized Raimund as completely of, and in the spirit and character of the Veneto; austere mysterious, and timeless and of the land. His drawings in their native environment, the place of his roots convinced me that he had planted a deep knowledge and sensibility of the Italian “disegno” at Cooper and in my own psyche.


From the 70s onward, his projects unfolded and were carefully and intimately constructed in parallel s to his profound teaching activities and independent projects. He was always working on the challenge of the time: exhibitions; at Leo Castelli, MOMA, Yale, in Athens; competitions like Times Square, the Peak, the Paris opera, the Acropolis Museum, the IBA Social Housing; projects for the Anthology Film Archives, civic buildings in Berlin, Bolzano, Moscow.


In 1983, he invited me to teach at the Sommerakademie in Salzburg where he had the


community that he had left for New York. There I was introduced to the activity and


experimentation that was shared among his favorite peers from his early days in Austria-


Lüpertz, Graubner, Pichler, Kubelka, and HC Artmann. It was ribald, brilliant and engrossing. He was already involved in the Berlin IBA and Joseph Kleihues was along in Salzburg. This resulted in the 1987 opening of the half circle of his Fredrichstrasse block housing prototype where Raimund engaged a priest to bless the courtyard with holy water and celebrated with a goulash prepared by his daughter.


Celebration, consecration, action.


The IBA era was followed by The Austrian Cultural Institute competition and the building;


this period was his great odyssey, a dividing line in his life and in his identity. Charles


Gwathmey was on that jury and I accompanied them on the joyous walk- through of the


finished structure . Raimund wanted to point out any detail that wasn't perfect – we all


smiled with the knowledge of how massive an effort and achievement he had arrived at


in Manhattan in regard to all he had aspired to and represented- his little discomforts


were outscaled. The scale of success and historical portent challenged his outsider maverick- rebel identity. But he did persevere and was able to sustain his new level of


recognition with his unconventional and eternal sense of the unknown, unexpected,


and disruptive intact to the end.


Winning the competition was the threshold for a new era of work that spanned more


than twelve years and defined a period of important commissions. Raimund mastered and survived all the obstacles and hurdles necessary to build the Austrian Cultural Institute in the New York of his time. He achieved a distinct achievement for this era in Manhattan: the intimate and highly articulate civic structure, with tectonic and programmatic diversity on every level from the street to the sky. It is a project that continues the genetics of the innovation spawned by Lever House, Seagrams, and Paley Park, great examples of a dignified and spatially unconventional New York fabric of subtlety and magic; a careful and memorable work of refinement within the urban fabric. Followed by the project in China, the work on the addition to the Anthology, and the continuing construction of the Music House in Hombroich- he never halted.


With the implementation of the project for Hombroich, fifteen years from when he first


designed it, I looked at the Musikerhaus drawings, models, and photos of its in- process


construction.


I was aware that the project in all its stages, had sustained a totally new and definitive


imagery and scale, from its inception into the present. I have been fascinated by the fact


that it has the scale of a medieval city but is a singular structure.


An implosive silhouette, it has a “meteor – fallen – to – earth – like”, fluctuating


expression of gravity, falling and levitating at once .


It is an image of the future and from the deep recesses of imagination and memory –


it is a space of paradox. Unique to this time, it is an embodiment of new form and a profoundly beautiful program as the house of the musicians; elements in a spatial force field.


I had a strange sense of portent, and was a bit afraid, from the time I first witnessed the


drawings and model.


The fear stayed with me throughout the ongoing construction when he would bring back


the dramatic photos of each memorable stage.


I was haunted by the recognition that this project, the Musikerhaus- was great enough to


be a final work.


“**For now we see through a glass, darkly; but then face to face”**



#architecture #architecture_books #Diane_Lewis #Raimund_Abraham
 ## Comments 
1. [chris teeter](http://www.metamechanics.com)
4.12.11 / 1am


Great article.  

Between the images of the work and the text I think I could slice a 2″ square brass bar into rich geometrical edges with a pencil.
3. Pedro Esteban
4.12.11 / 2am


his works put in crisis my works, is like if my work began to make critics to itself, is… a confuse sensation


 ”that serves as an inspiration for any architect”  

he strenght my work, isn't an inspiration, is… a confuse sensation
5. Gio
4.13.11 / 5pm


Mr. Woods thank you for posting this essay.


Professor Abraham has always been and will always be the true embodiment of Architecture.
7. jtotheviss
4.13.11 / 11pm


I could look at his drawings all day.
9. dazed
4.17.11 / 10pm


Note-to-self: Buy 


❤ RIP xoxo
11. [建筑师草图集 | 灵感日报](http://www.ideamsg.com/2011/10/the-sketch-of-architects-2/)
10.11.11 / 12am


[…] LOCI ULTIMI: The Last Abodes of Mankind by Raimund Abraham /  via […]
13. [07 Visionary Building 1 « new vs old](http://newold12.wordpress.com/2012/07/06/07-visionary-building-1/)
7.6.12 / 8pm


[…] Woods WAR AND ARCHITECTURE: Three Principles Raimund Abraham ABRAHAM'S [UN]BUILT (with a text by Diane Lewis) #gallery-85-2 { margin: auto; } #gallery-85-2 .gallery-item { float: left; margin-top: 10px; […]
