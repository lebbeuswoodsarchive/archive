
---
title: ZVI HECKER’S SENSE OF PLACE
date: 2011-04-26 00:00:00 
tags: 
    - architecture
    - Israel
    - museum
    - Palmach
    - Zvi_Hecker
---

# ZVI HECKER'S SENSE OF PLACE


[![](media/palm-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-3.jpg)


*(above) [The Palmach Museum of History](http://en.wikipedia.org/wiki/Palmach_Museum), Tel-Aviv, Israel, 2000, by [Zvi Hecker](http://en.wikipedia.org/wiki/Zvi_Hecker), with Raffi Segal. (photo by Yael Pincus)*


Most of Zvi Hecker's architecture achieves a rare combination of originality and modesty. Its originality is not in the introduction of novel forms, but rather in the ways it brings together already familiar forms in relationship to particular sites and programs. The modesty of his buildings is a result of this kind of originality and the social character it lends them: they read like distillations of some of a society's best ideas about the design of space, reinvented in ways that makes them new. In this sense,  Hecker's is an ‘ideal' architecture, celebrating both the individual and the community of which he is a part, and realizing their mutual interdependence.


Not to harp on this point, but it seems to me that this is what architecture, at its best, is about. You find a similar quality in the work of Mies van der Rohe, though in his case he invented the forms that became the social norm. Also the work of Aldo van Eyck and Jacob Baakema. Architecture as an idea cannot be limited to one-off masterpieces but should create an inspiring fabric of spaces and ways of living, a tapestry of human invention and aspiration. It is possible to imagine a vibrant urban tapestry—a city—with many Zvi Hecker buildings, while it is not possible to imagine the same with the singular buildings of, say, Zaha Hadid.


The Palmach Museum pf History in Tel-Aviv, Israel, designed by Hecker in collaboration with Raffi Segal, offers an excellent example of exactly what I'm talking about. In 1998, Hecker wrote: “The Palmach Museum of History is essentially a landscape. It is a landscape of the dreams that have made Israel a reality. The form of this landscape is homage to the ideals that Palmach stood and fought for. They are also the invisible foundations that carry the load built in fifty years of Israel's independence.” Typically, he invokes an ideal of social meaning, inferring the way—creating a landscape—architecture can bring it palpably into reality. Constructed of common local stone, tile and far-from-perfect poured-in-place concrete, the building offers us dynamic forms and roughly textured spaces appropriate to a history of struggle and conflict, and to a victory of hope over despair.


LW


.


*The Palmach Museum of History (photos by Michael Kruger, unless otherwise noted):*


[![](media/palm-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-1.jpg)


[![](media/palm-14.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-14.jpg)


*(below) Saving the existing trees on the site:*


[![](media/palm-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-5.jpg)


[![](media/palm-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-2.jpg)


[![](media/palm-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-6.jpg)


[![](media/palm-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-12.jpg)


[![](media/palm-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-8.jpg)


[![](media/palm-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-9.jpg)


[![](media/palm-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-10.jpg)


[![](media/palm-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-11.jpg)


[![](media/palm-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/palm-4.jpg)


*(above photo by Yael Pincus)*


#architecture #Israel #museum #Palmach #Zvi_Hecker
 ## Comments 
1. Kevin
4.28.11 / 7pm


With the current drive of the architectural community to praise glitz and glam, I find solace in the works of those like Hecker. I am of the opinion that great architecture throughout history establishes its own identity by appealing not only to visual stimulus but a full sensory immersion.


The Plamach museum is a single place formed through a conglomeration of experiences. thermal change, explosions of sound, textural and formal encroachment into your personal space, even smells allow the building to engage the user at a much deeper level. 


The result is memory. Memories which impact us in a way that allows us to identify associative meanings where we might otherwise see only fancy shapes form making.
3. tom
6.16.11 / 12am


You know- another one(Hecker) to use your Open/ Closed analogy in regard to Hadid. You are basically asking a question of geometry, and how it belies concept. I believe Hadid's geometry displaces the individual's space, and places it in a conceptual space beyond the horizon. It is a seduction of the individual viewer by a conceptual sublime. Hecker's geometry is full of contradiction that makes ideas of the sublime impossible, or ridiculous.  

I think you can relate Hadid's geometry to hierarchal systems like Versailles.  

First introduced to Hecker at an exhibit at Storefront in the early nineties. Liked him then, think I related him to Smithson, but now I would have to think about that more.


Am I off base with this, or am I just reiterating what you said in my own way?
