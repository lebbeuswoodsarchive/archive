
---
title: FORMS OF AUTHORITY
date: 2011-04-23 00:00:00 
tags: 
    - architects
    - authority
    - monuments
---

# FORMS OF AUTHORITY


From the art of resistance to unreasonable authority to the architecture celebrating it (authority is always unreasonable), that's the journey from the last post to this one.


Authority is a fascinating, disturbing, in some ways exciting and in others depressing topic for architects. It is at the root of our social relationships and the structure of our social arrangements. Social classes, chains of command and decision-making, distribution of wealth, the enactment and enforcement of civil laws—these and more are based on concepts of authority, or who has the right to act in the name of others to determine both the general shape of society and its particulars.


I have no intention here of tracing the origins of authority, which must be in pre-historical times when the strongest and most confrontational members of a tribe simply took control of its activities. Dissenters were dealt with by sharp blows of a club to the head. In present times, the nature of authority has not changed so much. Authority is still about taking control of others' activities and, indeed, the products of their activities, whether they are in the form of material objects—from artworks to parcels of land and the buildings on them—to the immaterial—government policies, corporate ethical standards, the rules of financial games determining the relative values of traded stocks and currencies.


The monuments that concern us here are those designed to commemorate and legitimize the enduring authority of the winners of great conflicts, political, military, and ideological. It is the winners who write history and commission the monuments as a form of that writing. However, it is a curious feature of most such monuments that their abstract character ends up commemorating the artist or architect who designs them, especially as the memory of the conflicts themselves fades from public memory. The poem by P.B. Shelley, quoted below, evokes this effect with grace and subtlety, as does the tale—no doubt apocryphal—that the great Lighthouse of Alexandria had a plaque naming the (long-forgotten) Pharaoh who commissioned it, designed to fall off in a hundred years and reveal yet another plaque, naming only the architect who designed it, holder, in rhese cases, of the most enduring authority of all.


*Sic transit Gloria mundi—*So goes the glory of this world.


LW


.


*(below) Monument to Rosa Luxemburg and Karl Liebknecht, Berlin, 1926, by Ludwig Mies van der Rohe:*


[![](media/mies-rosa-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/mies-rosa-1.jpg)


*Monument to the March Dead, Berlin, 1921, by Walter Gropius:*


[![](media/gropius-marchdead.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/gropius-marchdead.jpg)


*Monument to the Open Hand, Chandigarh, Punjab, India, built 1972, by Le Corbusier:*


[![](media/lec-open-hand.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/lec-open-hand.jpg)


.


*(following) Monuments commemorating various historical events, Federal Republic of Yugoslavia, 1960s-1980s, artists and architects to be identified:*


[![](media/m-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-1.jpg)


*(above) by Croatian sculptor [Dušan Džamonja.](http://en.wikipedia.org/wiki/Dušan_Džamonja)*


[![](media/m-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-2.jpg)


[![](media/m-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-3.jpg)


[![](media/m-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-6.jpg)


[![](media/m-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-4.jpg)


[![](media/m-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-10.jpg)


[![](media/m-71.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-71.jpg)


[![](media/m-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-11.jpg)


[![](media/m-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-8.jpg)


.


Ozymandias


by Percy Bysshe Shelley (1792-1822)


I met a traveler from an antique land  

Who said: Two vast and trunkless legs of stone  

Stand in the desert. Near them, on the sand,  

Half sunk, a shattered visage lies, whose frown,  

And wrinkled lip, and sneer of cold command,  

Tell that its sculptor well those passions read  

Which yet survive, stamped on these lifeless things,  

The hand that mocked them, and the heart that fed;  

And on the pedestal these words appear:  

“My name is Ozymandias, king of kings:  

Look on my works, ye Mighty, and despair!”  

Nothing beside remains. Round the decay  

Of that colossal wreck, boundless and bare  

The lone and level sands stretch far away.


.


[![](media/m-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/m-13.jpg)


*(via commenter Antoine) a link about locations and Kampenaers' book about them:  

<http://newworldorderreport.com/News/tabid/266/ID/7559/25-abandoned-Soviet-monuments-that-look-like-theyre-from-the-future.aspx>*


*Monumennt to Aviation, Sidney, Australia (unbuilt), 1979, by Raimund Abraham:[![](media/abraham1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/abraham1.jpg)*


#architects #authority #monuments
 ## Comments 
1. Pedro Esteban
4.23.11 / 3pm


the worst of all  

<http://en.wikipedia.org/wiki/Parthenon>


and others:  

(only for understand ”architectural behaviors”)  

<http://alongthemalecon.blogspot.com/2009/11/at-top-of-his-game.html>  

<http://alongthemalecon.blogspot.com/2009/11/flashback-mass-rally-for-elians-return_28.html>


not the architectural(or not)-structures in that place, just the square, the space who occupies the square,(space have form)  

The structures near are just reaffirmations.
3. [Antoine](http://www.presquilerecords.com)
4.23.11 / 3pm


One of them: <http://en.wikipedia.org/wiki/Du%C5%A1an_D%C5%BEamonja>


+ a link about locations and Kampenaers' book about them:  

<http://newworldorderreport.com/News/tabid/266/ID/7559/25-abandoned-Soviet-monuments-that-look-like-theyre-from-the-future.aspx>





	1. Pedro Esteban
	4.23.11 / 9pm
	
	
	thanks! really interesting!  
	
	Is like a write project I have done few months ago, the same idea.
	
	
	
	
	
		1. [Antoine](http://www.presquilerecords.com)
		4.24.11 / 10am
		
		
		Not monuments/spomeniks, but nice book from F.Chaubin too:  
		
		<http://www.guardian.co.uk/artanddesign/2011/feb/07/russian-architecture-soviet-union-photography>
	3. Pedro Esteban
	4.24.11 / 11am
	
	
	thanks again, I copy some sentences I like:
	
	
	”with the now valuable land they stand on becoming a place for unimaginative hotels, casinos, resorts, and villas for the rich.”  
	
	 (this is unbelievable! well things have change)
	
	
	“I have no nostalgia for the Soviet Union,” he says, “but in these strange and wonderful buildings, I saw the skin of a culture that fascinated me.”
	
	
	There isn't a better architecture today.  
	
	If you have more information related I'll be grateful.
5. Pedro Esteban
4.23.11 / 10pm


(only for understand ”architectural behaviors”)


Interesting how the forms looks familiars ones located in Cuba and the others in places really far.  

This are some projects to commemorate the victory of Cuba in Playa Giron, quite interesting history that concourse, my english don't let me explain it.  

The page is in Spanish sorry.


This is a general view of the projects, the winner was a polish team with Mario Coyula, too expensive to build it, and have some strange ”meanings” who at the end was the most criticized project. Cuba's architecture wasn't prepared for that kind of ideas.  

<http://arquitectura-cuba.blogspot.com/2008/09/uia-63-concurso-internacional-monumento.html>


Another project by Sergio Baroni and Vittorio Garatti  

<http://arquitectura-cuba.blogspot.com/2009/04/le-beau-serge-baroni-por-coyula-ii.html>


In other hand this:  

<http://www.meoevoli.eu/FotoPage/fotoPage.asp?l=E&f=349>


an aerial view is perfect to see the complex. In Cuba structures of this kind are more often squares, is normal Cubans are really ”opens”  

<http://maps.google.it/maps/place?cid=3017697539849503142&q=monte+de+las+banderas&hl=es&cad=src:kml_balloon&ei=QVmzTbeLK8OB_Qa1sfS6Bw>





	1. Pedro Esteban
	4.29.11 / 2pm
	
	
	one of the uses of that structure
	
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		4.29.11 / 3pm
		
		
		Pedro: Thank you! This Cuban music brightened up my day!
7. [The Fox Is Black » Monuments of the Socialist Republic, Photos by Jan Kempenaers](http://thefoxisblack.com/2011/04/25/monuments/)
4.25.11 / 6pm


[…] The photographs are the work of Belgian photographer Jan Kempenaers, for a series Spomenik: The End of History and went on display in late 2007 at BAM, Flemish institute for visual, audiovisual and media art. I've included a small gallery below. Lebbeus Woods recently used a few of these monuments, along with some others to lay out a few thoughts about authority. […]
9. [chris teeter](http://www.metamechanics.com)
4.26.11 / 1am


When the conflicts fade humans become rational as they step back and take a deep breath. The next generation can never pretend to understand the older generations struggles, they can only through analogies imagine equal emotion.


When this happens, what is left is the work of art, whether it be sculpture, painting, or architecture.


For a rational person to understand the meaning of the monument one must re-visit the psychology and memory of the person creating the work. Even if the creator had a political bias, each decision they made in their form, space, material, and energy making moves reflected not only their psychology, but the clients intended political agenda, and the society's accepted readings of the art and the political agenda.


Every action or non-action gives a persons or cultures character away and in hindsight deception is easy to decipher. Hindsight happens in seconds these days with the internet, which ultimately renders most authorities propaganda comical in minutes.


The lasting monument is a clear indicator of what authority meant to not only the artist but also society. If the creator crosses the line, the creator with the art disappears. When this happens we all want to know what this creator stood for, their values are amplified irrationally and as none of their work will take form their values remain slightly out of reach and the authority becomes scrutinized and their limits in character are clearly understood. Once an authorities limits are understood the authority has become a clear target for disassembling. For this reason secret societies and the very wealthy never tell anyone they are very wealthy.


Based on this my reading of these communist monuments where most the creators are unknown (based on this blog) is as follows – the authoritative figure valued rational thought over human emotions, they valued the collective over the individual, the creator who wanted to express anything could only express traces of human emotions in radical geometries and the use of materials without actually making a clear statement, the creator had to find life in the pure rational abstractions of man's conceptual thoughts…abstractions tend not to be of life, but rather formal conclusions of life in a rational context….until an irrational interpretation tied to an emotion filled cultural entity such as religion (sitings of mother marry in tree bark for instance).


The monuments nearly seem insane, but not the frantic emotional homeless man insanity, more the insanity of a human within a human's world of thought.


Nice post
11. [Jugoslaviens glemte krigsmonumenter | RASMUS BRØNNUM – en Arkitektur Blog](http://rasmusbroennum.wordpress.com/2011/04/27/jugoslaviens-glemte-krigsmonumenter/)
4.27.11 / 8am


[…] spørgsmål ved om disse tidligere monumenter kan overgå til kun at fungere som enkelt skulpturer? Lebbeus Woods har inkluderet flere af disse monumenter i en af hans seneste indlæg – “Forms of […]
13. Pedro Esteban
4.29.11 / 4pm


(only for understand ”architectural behaviors”)  

don't post if you can't, I'm now listening these great music!
15. milena
4.29.11 / 9pm


The sixth image of Yugoslavian monuments is a monument by architect Bogdan Bogdanovic. The monument is in Kosovska Mitrovica and commemorates Serb and Albanian partisans in the 2nd world war. <http://en.wikipedia.org/wiki/Bogdan_Bogdanovi%C4%87>


Greetings from ex-Yugoslavia!
17. euphemus
5.5.11 / 2pm


Just some facts:  

it's ignorant and a terrible terrible thing to name them soviet monuments! they are a significant part of post war/post revolution yugoslavian art described as a transition from socialist realism to social estheticism/modernism. far from forgotten and far from anonymous!  

chronologically:  

1. commemorations of concentration camps right after the war (<http://bit.ly/lMgwda> and <http://bit.ly/jM5M2M>)  

2. monuments to certain revolution victories and victims in 50s/60s (monumental sculptures <http://bit.ly/isOkoH>) as to provide a substitute to secularization of society  

3. 60s/70s secularization upgrade to cultural centres (golden era of architectural competitions, large scale community buildings, libraries, theatres designed by mostly young architects and built in medium/small towns throughout the country), large part financed by generous loans of international banks to fancy tito. well designed still form the majority of quality public buildings in ex-yu today.  

4. building monuments to recent and forgotten history (victims of postwar revolution, mass murders in Bosnia…) are an important (and uncensored) way of dealing with ideology.  

“artists and architects to be identified”:  

1. Podgarič (Croatia, 1967), sculptor Dušan Džamonja  

2. Kosmaj (Serbia, 197?), sculptor Vojin Stojić and architect Gradimir Medaković  

3. Tjentište/Sutjeska (Bosnia and Herzegovina, 1971), sculptor Miodrag Živković  

4. Niš/Bubanj (Serbia, 1963), sculptor Ivan Sabolić  

5. Ilirska bistrica (Slovenia, 1965), sculptor Janez Lenassi  

6. Kosovska Mitrovica (Kosovo, 1973), architect Bogdan Bogdanović  

7. Kamensko/Korenica (Croatia, 19??), sculptor ?  

8. Kadinjača memorial (Serbia, 1952-79), sculptor Miodrag Živković and architect Aleksandar Đokić  

(<http://bit.ly/jEdmwM>)  

9. Makljen, (Bosnia and Herzegovina, 1978), sculptor Boško Kućanski, destroyed 2000 (before pic <http://bit.ly/mM7k9H>)  

more:  

Petrova gora, (Croatia, 1982), sculptor Vojin Bakić (<http://bit.ly/iUpQmG>)  

Kolašin, (Montenegro, 1975), architect Marko Mušič (<http://bit.ly/mhsN4G>)  

Kruševo, (Macedonia, 1974), architect Jordan and Iskra Grabulovska (<http://bit.ly/kQxKih>)


It may be a question of distant authority and terrors of history for some or even an anonymous fascination out of context – but these solemn abstract giants are something of every day. For me, its a terrible story of my grandfathers – winners and victims at the same time, of artists and teachers that like great masters translate that emotion. (And many of architectures that just went beyond.)  

I think while Jan Kempenaers and Willem Jan Neutelings talk about abstract beauty of their ex-yu findings the photographs portray weird and bizarre emotions. And narrow-minded blather all over internet ever since. Not happy!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.5.11 / 11pm
	
	
	euphemus: You are right about the confusion of “Soviet” and “Yugoslavian.” In my post, I don't make that dreadful mistake. Of course, I am married to a woman from the former Federal Republic of Yugoslavia, with many friends from the now-independent republics.  
	
	It is wonderful that you've taken the time and effort to write these credits. Thank you! I will integrate them into the body of the post.
19. Pedro Esteban
5.6.11 / 9pm


this is what I mean! Just perfect!  

and others are discussing about Zaha Hadid renders!  

[![](media/Spomenik_17.jpg)](http://4.bp.blogspot.com/_apFpwnMI2hs/TP2pG6Z3SzI/AAAAAAAADds/sHGWBfHDdyM/s1600/Spomenik_17.jpg)
21. Pedro Esteban
5.6.11 / 9pm


the last picture, WOOOW!!!!!!!!!!!! PERFECT!!!!!! PERFECT!!!!


State Hydrometeorological Institute building, projected in 1975 by Krsto Todorovski 


<http://sajkaca.blogspot.com/2010/02/skopje-and-my-favorite-buildings.html>
23. [Simon](http://www.durchmessungen.wordpress.com)
5.23.11 / 11pm


Well, one month late, but still something to add: It is a minor inaccuarcy that Gropius “Monument to the March Dead” is not in Berlin but in Weimar – a fact that I just have to point out as aficionado of Weimar. The original monument though was not really built by “the winners of great conflicts”. The original monument was destroyed by the Nazis and the one existing today in fact is a reconstruction – built in post-war (socialist) GDR. This way Gropius design turned out to become used for a real ‘winners commemoration' rather than being it originally.
