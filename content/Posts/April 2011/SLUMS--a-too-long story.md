
---
title: SLUMS  a too-long story
date: 2011-04-29 00:00:00 
tags: 
    - cities
    - Sao_Paulo
    - slums
    - urban_design
---

# SLUMS: a too-long story


*The following is an exchange that recently took place in an online class on “Culture and Globalization,” at a major university. As a Faculty Guest, I was asked to focus on slums as a global phenomenon. Section 1. (below) repeats certain concepts already published on this blog, although it states them in a more concise way. The other Sections have not been published before and contain accounts of personal experiences and reflections.*


**1. Introduction by LW.**


Hello. Lebbeus Woods, again. I won't take our time to tell you about myself, but rather invite you to visit my [website](http://www.lebbeuswoods.net/), which is loaded with details, mostly about my work. Today, I'm going to talk about slums, a.k.a favelas, barrios, squatter communities and many other names in the countries where they appear around the world.


We should be thinking of slums as a global phenomenon and not only a local one, for several reasons. First, considered as local, slums can easily be blamed on specifically local conditions, such as political changes, environmental conditions, or narrow historical factors, making it too easy to dismiss their broader societal implications. Second, and more importantly, slums are much the same wherever they occur, and are created by global economics, global politics and other global events. If slums are to be seriously addressed at all, they must be addressed on a global level.


While it is true that slums have always existed in some form, it is also true that something new and disturbing is happening. With the rapid growth of urban populations around the world, the population and the present extent of slums has increased almost exponentially. The disturbing part is that slums are intolerable and their rapid expansion means that society has no effective control over them, thereby projecting a bleak image of the human future. Before talking about improving, reducing or eliminating slums, we must first concentrate on slowing down their runaway growth, making prospects for any real improvement a long way off.


In trying to grasp what is going on, we must try to understand the dynamics driving the creation of slums. If we think of slums as a [problem](https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/) that needs a solution, we must state the problem with more clarity than we have so far. We can begin by analyzing slums from three standpoints: their *spatial* construction, the *social* forces that underlie their construction, and the *philosophical* beliefs that generate the social forces.


**The Spatial**


The key aspect of their spatial construction is an overall **complexity** of a kind that results from lack of overall planning. The houses or shacks that slum dwellers build themselves are built spontaneously on any open piece of ground they can find in the slum. No one owns the ground or the house, when it's built. The materials used to build are whatever can be scavenged from dumps, the construction sites of ‘real' buildings, or in the refuse scattered about in the slum. The slum is a gradual accumulation of houses woven together along paths necessarily left open and that wind in a labyrinthine way through often chaotic landscapes that only the slum dwellers can find their way through. These paths widen here and there to serve as markets, where food and goods of various kinds are traded or bought and sold. Slums are modeled on normal cities and towns, but cannot truly imitate them, for lack of resources, but also because they support a different way of living.


Because slum dwellers live, in the poorest slums, by scavenging city dumps for materials or products that have been thrown away by richer communities, slums are often located near the dumps, usually but not always on the edges of large cities. The more well-off slums, inhabited by the working poor, who have found manual labor in the city or unskilled work in large factories, emerge within the body of the city. Slums are always urban phenomena and emerge similarly in cities around the world. In the same way that the cities themselves are similar, regardless of religious and traditional cultural differences, their slums are amazingly alike in appearance and spatial construction, varying only in responses to differences of terrain and climate. It is telling to note that slums we can readily see exist predominantly in warm climates, where it is possible to survive without electricity or natural gas, as slum dwellers most often must; but also, if they had to heat their houses, as cold climates demand, a slum would be unlivable because of the polluted atmosphere from the cheapest sources of heat, wood- and coal-burning stoves. In colder climates, slums hide behind the facades of existing buildings that have either been abandoned or are in such disrepair that no one but the poor are willing to live in them.


**The Social**


Slums exist because of the poverty of its inhabitants. Poverty is caused by an extreme inequality of incomes between those who control the wealth of a community or a society and those who do not—the ‘haves' and the ‘have-nots;' the rich and the poor. **The rich feel they are best qualified to manage and most deserve to enjoy the benefits of a community's wealth and therefore use it to secure their control, through the authority of both private and public institutions.** The poor hope to improve their situation, but have little institutional help in doing so, and therefore must learn to live with deprivations and depredations of every kind, calling on their ingenuity and resilience to see them through.


Communities and societies tolerate the existence of slums because many people believe that slums are inevitable—they are simply the refuse, the garbage, produced by a highly productive and consuming society. This attitude is rarely challenged by mass media, indeed is almost never discussed in mainstream newspapers, magazines, and television news or other programs. The existence of slums and the discussion of ways to eliminate or even improve the living conditions they foster, is a non-topic in the global network of information exchange. Institutions of government and private business have at best a ‘charity' interest in ‘helping the poor,' and leave it to charitable NGO's to deal with the ‘humanitarian crises'—starvation, outbreaks of contagious diseases, widespread incidents of violence—that inevitably occur in slums.


No long-term plans for the improvement of their conditions are being made by any but international institutions, such as the United Nations, and they can be effective only to the extent that the direct involvement of the wealthiest national governments can be assured. At present, their commitment reflects the same attitude they have toward poverty in their own domains, which is half-hearted at best. The elimination or even the substantial alleviation of the poverty that creates slums is not high on their agendas.


**The Philosophical**


Behind popular attitudes and government policies are basic ideas about the nature of our humanity that have evolved over millennia of human development. While philosophers and other academicians, and intellectuals continue to debate them, they are for most people so ingrained in human culture that they are rarely mentioned or publicly discussed. The exceptions are times of social crisis, such as political elections, where the basics of the human condition are sometimes publicly raised and debated.


No doubt the most potent of the basic ideas concerns **human rights**. These are commonly thought to be rights that we are born with, rather than acquire through the machinations of politics, economics and other activities. If you are born human, you have “certain inalienable rights” that in fact cannot be taken away by other individuals or institutions, public or private. Just what these rights are varies in the different cultures in which the concept is found. In the West it is exemplified and to some extent contrasted in the *[Bill of Rights](http://www.ratical.org/co-globalize/BillOfRights.html)* of the United States and the *[Universal Rights of Man and the Citizen](http://www.constitution.org/fr/fr_drm.htm)* of the French Revolution. In principle, slum-dwellers have the same human rights as everyone else, but in practice they do not. This is because these founding documents of democracy are also the founding documents of capitalism, giving certain individuals the right to control the wealth of a community or society and thereby deny it to others, creating poverty and slums. As the old saying goes, “All men [sic] are created equal, but some are created more equal than others.”


In conclusion, to consider and discuss slums anywhere in the world, it is necessary to engage them on all three of these levels simultaneously, finding the links between that can be used to construct as complete an understanding as possible, as a first step toward improving the living conditions in slums and, better yet to eliminate them altogether.


Note: For a more detailed discussion of the ideas presented here, together with numerous illustrative images, follow [this link](https://lebbeuswoods.wordpress.com/2010/10/12/knots-the-architecture-of-problems/).


**2. Followup comment by LW.**


It should be noted that my and other's concern with slums has been criticized by some as exploitative, the case of intellectuals who can write clever books or articles about ‘those poor people,'  shaking their heads in mock-pity, collecting fees for writing and lecturing, while doing nothing that makes any actual difference in the conditions of slums. These critics also say that people like me haven't lived in slums, so can't really know anything about them. This last complaint strikes me as a flimsy excuse for not doing anything, rather like a doctor saying he can't do cancer research because he's never had cancer. Still, this general criticism does make a good point: it's a long, long way from analyzing a seemingly intractable problem to effectively solving it.


My question for you is this: Is it possible for intellectuals, working ‘at a distance,' to contribute anything of real value to solving the problem of slums? And, if so, what?


**3. Response by student A.**


Reading all of the above comments and specifically the entry by Mr. Woods, titled: “SLUMS: The problem”, from his blog,  i can certainly agree on the idea of how it's easy to feel pity for the people that live in the slums while at the same time “…doing nothing that makes any actual difference in the conditions of the slums”. Unfortunately, I suffer from the case of those people who live or are considered to be part of the fortunate side of the vast gap between the poor and the rich in Jakarta and not really do anything about it at all. Urban poverty is very much part of the city of Jakarta, Indonesia, or in many other major developing cities in Southeast Asia. Of the 12 million people living in Jakarta, it is estimated that half are poor. Reflecting on the years that i have spent in Jakarta as an expatriate, i can say, sadly, that it was very easy to get use to the slums and the poverty stricken people that you see on the streets. The inner part of Jakarta is like a concrete jungle of new high-rise banks, offices, apartments, and shopping malls with air conditioned spaces. But on the other side of this visible wealth, you see street vendors, beggars, or children trying to sell food or beg for money on the streets and Kampungs (villages). It's very normal for lavish apartments to exist side by side to slum areas. Sometimes it's hard to come to terms with the fact that the conditions of the city is really bad, because Jakarta as a city is designed in every way to make it easy, to not come in contact with the harsh realities of the poverty that is growing in that city.  Living as either an expatriate or considered to be part of the “rich” side of the population in Jakarta, your work place or school will usually be behind gated walls or bars. To get to malls, restaurants, or other places, no one really walks, due to the polluted and traffic heavy streets. Instead you're in a car, and that is also another layer of distance placed between you and the outside world. After awhile, the children or beggars on the other side of that car window, don't really faze you  anymore. Most people who can afford or are well off, mostly live in gated communities or Apartments with security guards standing outside the gate. To cut it short, we live in a bubble.


Reflecting on Mr. Woods's question: “Is it possible for intellectuals, working ‘at a distance,' to contribute anything of real value to solving the problem of slums? And, if so, what?”, I'm not really sure what to say, but if I were to put it into context of Jakarta, there are a number of organizations that indirectly addresses the problems of the slums by opening up educational centers or orphanages to bring the children from the streets and slums and provide them with free education and resources. These organizations believe that the first thing to tackle the problems of the slums in Jakarta, is to provide education and opportunity for the children. In that way, these children are less likely to grow up as adults and continue to fend for themselves in the slums and possibly be able to get off the street and provide a living for themselves and their parents that have been living the slums for years.


**4. Response to Student A.**


A.—I really appreciate your candid testimony about living with slums and their impoverished people. No excuses. No self-serving rationalizations. The courage to ‘tell it like it is,' even if you feel some shame about it, as you obviously do. I think we all have to feel that shame, because we do—no matter which city we live in—grow used to other people's suffering and don't what to get involved in it—we have enough suffering of our own! I've lived in New York City for about forty years. Here the poverty is kept pretty well hidden by the police and behind the blank walls of crumbling old building and public housing projects—ghettos, really, but these are slums by any measure. I know about them, but avoid going near or even thinking much about them—I don't want to get my hands dirty. Being an intellectual, I am only capable, emotionally, of thinking about slums on a wider scale, and only hope that I can ameliorate my feelings of shame and guilt by contributing some ideas that can help solve ‘the problem of slums' in the hands of those with more emotional grit and physical courage than I have. Obviously, I have my doubts about whether this is enough.


[![](media/morumbi-favela-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/04/morumbi-favela-1.jpg)


Before this turns into a therapy session, I will tell you about my one experience in a serious slum—a favela—in Sao Paulo, Brazil, some years ago. I was an invited speaker at a conference on urbanism—a Norte Americano who was therefore instantly of interest to newspaper and television reporters. A group of them came to my hotel and asked if I would like to see one of the city's famous favelas. They knew I was an architect interested in the growth of cities and doubtless thought this would be part of my education. Also, naturally, they wanted to know what I thought could be done to help the slums. So we set off, a caravan of three or four cars, one of them with television cameras, for the neighborhood of Morumbi, one of the richest in Sao Paulo. A hilly section of the city, lush with tropical trees and shrubs, Morumbi was also home to a vast favela, crammed with makeshift shacks along dusty dirt paths, that began at the top of a hill just beneath some gleaming white residential towers and ran endlessly, it seemed, to the base of the hill far below. It was a sunny day in August—late winter—but nevertheless warm and cheerful. Our caravan entered the favela on a steep dirt road and drove to a point some hundred meters to a dusty open space where the road ended, a kind of square surrounded by shacks made of scrap wood, metal and plastic and other, unidentifiable materials. The square was empty, except for some children playing. The reporters, who were snapping pictures of me (dressed fashionably in a white suit, looking very important indeed) as I climbed out of the car, explained that all the adults were away at their factory jobs—the ones they'd come to the city for—leaving the children, unattended, at home. Given the infamously slow traffic in the city, and the distances to the factories, they would return, exhausted, by bus close to midnight. Driven by a mix of fear and curiosity, I walked over to one of the shacks and called out—in English—was anyone inside. There was no answer, so I walked in. Small, with a dirt floor, some ragged drapes, a broken table and chairs, some mattresses on the floor and bits and pieces of ‘normal' paraphernalia—pictures and posters on the rough tin and wood walls, pots, dishes—some clothes scattered about—no sign of a bathroom or running water—much neater than I'd expected, though hardly clean—sunlight streaming through gaps in the walls and ceiling—a home. I felt suddenly embarrassed to be there and left abruptly. In the meantime, our group and their cars and cameras had attracted attention. Some older boys were standing around looking rather sullen. Sensing what was about to happen, I told the reporters that we had to leave at once. They didn't argue. We jumped quickly into the cars just as the first stones and bottles hit. Charging up the hill on the dusty road, we made it to the paved highway beside the gleaming towers, never looking back. The experience made a powerful impression on me, changing my way of thinking about many things, including architecture. But that is another story.


**5. Response by Student J.**


The story in itself makes a powerful impression. It's interesting how after just a couple of minutes, looking around one of those homes, the perspective of the boys suddenly became so clear to you. I would certainly like to know how it changed your thinking about architecture–is it too long a story?


**6. Response by LW to Student J.**


J.—of course, its a too-long story—its about life-long things and changes. Still, I'll try to give you a sense of what this meant for me, as a person and an architect (which have to be the same thing).


Before going to Sao Paulo and experiencing what I did, cities were manifestations of abstract ideas. I worked on the design of ‘ideal' cities, governed by the concept of ‘centers.' Each neighborhood created by a particular community was a center, and these would overlap, creating a kind of ideal, complex geometry for the city. Also,  at the heart of this approach was the idea that each person was a center, certainly of their own consciousness, but my thought was that the community is the building-black of the city. I summarized my thinking in the mid-1980s in a project called “CENTRICITY.” When I experienced the 17-million city of Sao Paulo, with its chaotic, sprawling form, and in particular the Morumbi favela, I was stunned by what I saw into the realization that my urban idea(l)s were of absolutely no use there, in the reality of the human-urban condition. At best, my Centricity project had elements of the character—aging, complexity, ideals of community that gradually build up an urban form—but offered nothing to the sullen boys, their slaving parents, their struggles to make a fully human life together. I resolved that, henceforth, I would devote my energies to addressing their world and the worlds of others who lived in a continual state of crisis, not only to survive, but to find meaning and purpose in their lives.


I mentioned that I had been invited to a conference in Sao Paulo. I can't go into all the details of this conference on urbanism, which lasted several days. What I can say here is that [Michael Sorkin](http://en.wikipedia.org/wiki/Michael_Sorkin) (an urbanist who wrote the book “[Local Code](http://books.google.com/books?id=KBCkX6NRRcsC&printsec=frontcover&dq=michael+sorkin+local+code&source=bl&ots=g1h-YHXRZq&sig=RW9XOZOX30Wpd1H6zUFeJPwsEDg&hl=en&ei=g8m6Te6HAs3AgQf01_XlBg&sa=X&oi=book_result&ct=result&resnum=5&ved=0CC8Q6AEwBA#v=onepage&q&f=false)” with its *Urban Dwellers Bill of Rights*) and I protested that there were no South American architects among the principal speakers—the audience of about 3,000 applauded wildly but our colleagues from the US and Europe got angry. When the ‘architect' of the Morumbi favela showed up onstage, chaos ensued. Sorkin and I resisted the temptation to ‘lead a revolution' that would have gotten violent, and simply walked out of the conference.


After Sao Paulo, I understood that I had to get directly involved in the social crises where architecture could play a useful, constructive role. The next several years took me to Zagreb, Sarajevo, and Havana, at the height of their political and social crises, and led to my proposal of projects for those cities. No more ideal abstractions. One book I published at the time says it all: “Anarchitecture: Architecture is a Political Act.” It was soon followed by “Radical Reconstruction.”


That, in brief, is the story of my own transformation.


(end of excerpt)


#cities #Sao_Paulo #slums #urban_design
 ## Comments 
1. William Kirby
4.30.11 / 4am


Bravo, Mr. Woods, it seems you've hit all the main talking points of Socialism. Rich people hate poor people and oppress them by taking away their human rights. Poor people want better lives, but being oppressed, cannot change their situation. We therefore must divest in Capitalism and move to the proven theories of Marx and Ingels. All the great civilizations based on their philosophies have thrived…oh wait. They would have thrived if they had been designed correctly. Am I close?


I know your heart is in the right place, and that psuedo-intellectuals ‘working-at-a-distance', such as yourself, have all the best intentions. This argument has and continues to play out in universities around the world for the past 150+ years and it's still not solved (frankly, because universities are a breeding ground of malformed ideas and shallow thinking). The first step to a lasting solution is to head back toward an agrarian society of individuals that are beholden to themselves. Honestly, it's probably too late for that, but I will definitely be enjoying that thought as I work in my garden tomorrow. 


My suggestion for your readers and yourself, is to resist the ubiquitous hatred of success that elitist invariably have, and instead focus on love of the less fortunate by helping them be more like those you despise.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.30.11 / 2pm
	
	
	William Kirby: I would be more than happy if capitalist institutions of government, industry and commerce would get seriously to work on turning slums around with the aim of greatly reducing or eliminating them. But I don't see it happening. Even the current U.S. President, Obama, decried as a “socialist” by those on the Right, has done little or nothing. He realizes that slum-dwellers don't vote. Only a fool would believe that we can—at this stage of our society—somehow do without capitalism, therefore it makes little sense, and too much time and energy, to despise it. What we can do is try to convince capitalists that it is in their best interests to work for the improvement and reduction of slums, before the rapidly expanding slums overwhelm, economically, our present social system. See an upcoming post on this.  
	
	I believe you are sincere in what you wrote. I think, though, that you are caught in an either-or mindset that will not be productive.
	
	
	
	
	
		1. Toni
		5.4.11 / 1pm
		
		
		Mr. Woods I am currently a Latin American graduate student working in the US on different aspects of slums and as a general topic on the Latin American city. I agree with the reader above about that your heart might be in the right place, and that slums are indeed a global issue to pay attention to. I have found a lot of the information about slums on your blog to be somewhat shallow and in other instances wrong. As a starting point, I can write on the comment above.  
		
		One very big thing you're mistaken about is that slum dwellers don't vote. They do, and they have made the difference in many local and federal governments. Just look at the case of the election and continuation in power of Hugo Chavez– the Venezuelan elite certainly didn't vote him into power.  
		
		Slum dwellers are highly organized, especially in the largest slums. (ie. Slumdwellers International and satellite groups like Mahila Milan in India)  
		
		The second main thing, is the idea that slums aren't being upgraded or somehow that the people's conditions within them are equally dire throughout the world. This is simply not true. Look at the case of Medellin where ‘comunas' that used the be the homes of cartel hitmen and where the police would not dare enter have now become cultural centers for the city and the country. (project by Giancarlo Mazzanti)  
		
		The poverty that used to exist there is no longer the same and people's homes are adequate and in many instances even quite beautiful. They receive full services from the city and they are definitely not an exception in Latin America (look at Alejandro Aravena in Chile)  
		
		Last, make sure to check the references for the photographs used in the post about the slums on Bogota. Proper credit is not given (one of the photos belongs to a rather famous photographer) and most of the photographs are not even of Bogota at all.  
		
		This is just to begin, I would gladly have a discussion with you about my research and about a lot of the widely understood issues about these ‘urban villages' as FIlipe Balestra of Urban Nouveau calls them (look at his work on upgrading in India)  
		
		It makes me glad to see people's interest in the subject, and I agree that it is something we should study and seriously take on as a global issue, but we nee a strong, factual base in order to truly understand the issue and to seriously discuss it in an academic setting (using pertinent references) so that we may come up with viable and adequate solutions.
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		5.4.11 / 11pm
		
		
		Toni: I appreciate your corrections to what I have written on this blog about slums. You've certainly pointed out my lack of expertise in many areas of fact. Also, your comment gives me the chance (which I should have given before) to offer the readers a caveat to that effect. As for your judgement of my pieces being ‘shallow,' I hope that only applies to the factual aspects. For example, nothing you said in your comments really impacts my basic arguments. I would be very glad to have a longer post written by you offering counter-arguments to my views about the causes of slums and also the responsibilities of corporations and governments in addressing the growing problem of slums. Beyond that, of course, I am very willing to publish any pro-active ideas you might have about what we as architects should be doing (you might look at my post SLUMS: one idea to get an idea of what I mean by ‘doing').  
		
		Please consider this an invitation to write what you think about slums and have it posted on this blog.
3. Caleb Crawford
5.2.11 / 9pm


Two excellent posts. It was interesting to see that the reporters wanted to know what you thought could be done to “help” the slums. It is really beyond the ability of architecture, though not of architects. Architects can address any physical condition and plan and construct new realities. Architecture as a profession is reliant upon the political structures that control resources. As you pointed out in SLUMS: to the stars, we are dealing with a social construct of inequality. As “intellectuals” our contribution is to shine light on the darkness and hope that those with the power will see. The last time we had that kind of general illumination was during the New Deal. Right now in the US there appears to be a general denial of reality about everything from climate change to economics and politics. It may be that things have to get a lot worse before they get any better. I remember arguing with someone who claimed we got nothing from the New Deal. I agreed. We didn't get a socialist revolution.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.3.11 / 1am
	
	
	Caleb: Your distinction between ‘architecture'—a field of knowledge and practice—and ‘architects' is such an important one and rarely made. A ‘field' is comprised of so many different, sometimes conflicting, points of view that its responsibility can only be for very broad principles like “the public health, welfare, and safety,” whatever that might mean. Architects are, on the other hand, individuals who will define their responsibilities to society in quite specific ways. In this regard it's important to remember that as an individual architect I am not merely a representative of ‘architecture.' Rather, it is the other way around: ‘architecture' represents my point of view and sense of responsibility, together with all others who comprise the field. That's what makes it so complex, so human, so difficult and, yes, so wonderful.
5. [Ahmad Borham](http://drawingparallels.blogspot.com)
5.2.11 / 10pm


Elaborating on the notion of how “slums” are a global phenomenon created due to global economics and politics.  

i attach a post that tries to draw parallels between how new liberal economics created a similar informal kind of settlements in Istanbul and Cairo.


<http://drawingparallels.blogspot.com/2011/05/n0-fact0riesn0-w0rries.html>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.3.11 / 1am
	
	
	Ahmad Borham: Thank you for this link. It's extremely important to see how this global phenomenon takes particular forms in different contemporary cultures and settings. This way we can rid ourselves of the delusion that the problem can be blamed on any one people or place.
7. Mik
5.3.11 / 6pm


This question is poorly formed in my head, but I will try to get it out reasonably clearly. 


The first gentleman who posted suggests returning to an agrarian lifestyle, with the thought that the only way to solve societies problems is by putting the responsibility to survive on one's own shoulders. It seems pretty silly to romanticize how well society worked as an agrarian society with little flexibility or mobility, and a very strict social hierarchy. The problems of inequity have been around for long enough to show that it is not merely globalizaiton, but some result of human nature, and limiited availabilty of resources. 


When we are so removed from the situation it is easy to blame others for being poor, until you realize that every choice you make is some sort of vote in the globalized market. Living in San Diego I am confronted with an obvious example of disparity when I remember the proximity to Tijuana, but am completely overwhelmed with the complexity. 


With the increasing complexity of economics that has resulted in a similarly complex reality do you think a person has to have a clear understanding of global economics and politics to confront these issues?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.4.11 / 12pm
	
	
	Mik: Yes. There is no easier way.
9. Pedro Esteban
5.11.11 / 9pm


I haven't read the post, just want to share this


  

came from this page in Italian:  

<http://cafelab.blogspot.com/2010/12/kowloon-walled-city-la-citta-ad-alta.html>  

more here:  

<http://en.wikipedia.org/wiki/Kowloon_Walled_City>


all the info: thanks to my friend Federico Lepre
11. Pedro Esteban
5.26.11 / 8am


in the other hand we have this:


<http://www.boston.com/bigpicture/2010/09/human_landscapes_in_sw_florida.html>
