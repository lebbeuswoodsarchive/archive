
---
title: Salman Rushdie  DANGEROUS ARTS
date: 2011-04-20 00:00:00 
tags: 
    - Ai_Weiwei
    - art_and_politics
    - Salman_Rushdie
---

# Salman Rushdie: DANGEROUS ARTS


**April 19, 2011, London**
.
*The following is taken from an [article](http://www.nytimes.com/2011/04/20/opinion/20Rushdie.html?scp=2&sq=salman%20rushdie&st=cse) in The New York Times:*
*.*
**by [SALMAN RUSHDIE](http://en.wikipedia.org/wiki/Salman_Rushdie)**

THE  great Turbine Hall at London's Tate Modern, a former power station, is a notoriously difficult space for an artist to fill with authority. Its immensity can dwarf the imaginations of all but a select tribe of modern artists who understand the mysteries of scale, of how to say something interesting when you also have to say something really big. Louise Bourgeois's giant spider once stood menacingly in this hall; Anish Kapoor's “Marsyas,” a huge, hollow trumpet-like shape made of a stretched substance that hinted at flayed skin, triumphed over it majestically.


Last October the Chinese artist Ai Weiwei covered the floor with his [“](http://aiweiwei.tate.org/uk/ " ")Sunflower Seeds”: 100 million tiny porcelain objects, each handmade by a master craftsman, no two identical. The installation was a carpet of life, multitudinous, inexplicable and, in the best Surrealist sense, strange. The seeds were intended to be walked on, but further strangeness followed. It was discovered that when trampled they gave off a fine dust that could damage the lungs. These symbolic representations of life could, it appeared, be dangerous to the living. The exhibition was cordoned off and visitors had to walk carefully around the perimeter.


Art can be dangerous. Very often artistic fame has proved dangerous to artists themselves. Mr. Ai's work is not polemical — it tends towards the mysterious. But his immense prominence as an artist (he was a design consultant on the “bird's nest” stadium for the Beijing Olympics and was recently ranked No. 13 in Art Review magazine's list of the 100 most powerful figures in art) has allowed him to take up human rights cases and to draw attention to China's often inadequate responses to disasters (like the plight of the child victims of the 2008 earthquake in Sichuan Province or those afflicted by deadly apartment fires in Shanghai last November). The authorities have embarrassed and harassed him before, but now they have gone on a dangerous new offensive.


On April 4, Mr. Ai was arrested by the Chinese authorities as he tried to board a plane to Hong Kong. His studio was raided and computers and other items were removed. Since then the regime has allowed hints of his “crimes” — tax evasion, pornography — to be published. These accusations are not credible to those who know him. It seems the regime, irritated by the outspokenness of its most celebrated art export, whose renown has protected him up to now, has decided to silence him in the most brutal fashion.


The disappearance is made worse by reports that Mr. Ai has started to “confess.” His release is a matter of extreme urgency and the governments of the free world have a clear duty in this matter.


Mr. Ai is not the only Chinese artist in dire straits. The great writer Liao Yiwu has been denied permission to travel to the United States to attend the PEN World Voices Festival of International Literature, which begins in New York on Monday, and there are fears that he could be the regime's next target. Among the others are Ye Du, Teng Biao and Liu Xianbin — who was sentenced last month to prison for incitement to subversion, the same charge leveled against the Nobel Peace Prize laureate Liu Xiaobo, now serving an 11-year term.


The lives of artists are more fragile than their creations. The poet Ovid was exiled by Augustus to a little hell-hole on the Black Sea called Tomis, but his poetry has outlasted the Roman Empire. Osip Mandelstam died in a Stalinist work camp, but his poetry has outlived the Soviet Union. Federico García Lorca was killed by the thugs of Spain's Generalissimo Francisco Franco, but his poetry has survived that tyrannical regime.


We can perhaps bet on art to win over tyrants. It is the world's artists, particularly those courageous enough to stand up against authoritarianism, for whom we need to be concerned, and for whose safety we must fight.


Not all writers or artists seek or ably perform a public role, and those who do risk obloquy and derision, even in free societies. Susan Sontag, an outspoken commentator on the Bosnian conflict, was giggled at because she sometimes sounded as if she “owned” the subject of Sarajevo. Harold Pinter's tirades against American foreign policy and his “Champagne socialism” were much derided. Günter Grass's visibility as a public intellectual and scourge of Germany's rulers led to a degree of schadenfreude when it came to light that he had concealed his brief service in the Waffen-SS as a conscript at the tail end of World War II. Gabriel García Márquez's friendship with Fidel Castro, and Graham Greene's chumminess with Panama's Omar Torrijos, made them political targets.


When artists venture into politics the risks to reputation and integrity are ever-present. But outside the free world, where criticism of power is at best difficult and at worst all but impossible, creative figures like Mr. Ai and his colleagues are often the only ones with the courage to speak truth against the lies of tyrants. We needed the samizdat truth-tellers to reveal the ugliness of the Soviet Union. Today the government of China has become the world's greatest threat to freedom of speech, and so we need Ai Weiwei, Liao Yiwu and Liu Xiaobo.



***Salman Rushdie, the author, most recently, of “Luka and the Fire of Life,” is the chairman of the PEN World Voices Festival of International Literature.***





#Ai_Weiwei #art_and_politics #Salman_Rushdie
 ## Comments 
1. dazed
4.20.11 / 7pm


speechless.dumbfounded & upset.  

what do i need to do? and so many other questions
3. [Salman Rushdie: DANGEROUS ARTS | designlevelzero](http://designlevelzero.wordpress.com/2011/04/21/salman-rushdie-dangerous-arts/)
4.21.11 / 11am


[…] Lebbeus Woods On the PopPressed Radar Loaded: Heavyweight Craft Exhibit Showcases Designs Made from Iron […]
5. [Jacquie](http://rousjh.wordpress.com)
4.21.11 / 1pm


my imagination fails me…i repeat, what do i need to do?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.21.11 / 4pm
	
	
	Jacquie and dazed: Not everyone can do the same thing. The key is, do what you can in your personal ‘sphere of influence.' If you write for a newspaper, write an article. If you have four good friends in the world, talk to them about it and what you might do together to influence the Chinese (in this case) government to free Ai and the others—an email campaign? If you're famous, speak to the press, who are always eager to hear anything you have to say. If you're a songwriter, compose and perform a song and post it on TouTube. If you're a painter, make a painting expressing your feelings and thoughts and hang it in your house or include it in your next exhibition. I believe in the ‘Butterfly Effect' from chaos theory: if a butterfly flaps its wings in North Carolina, it can cause a typhoon in the Indian Ocean. Small events can have big consequences. Many small events occurring at the same time can change the course of history. The point is: DO something.
	
	
	
	
	
		1. Gavin Keeney
		4.26.11 / 4pm
		
		
		A propos of all of that, I have recently been invited to deliver a paper at the 11th Essex Conference on Critical Political Theory in June …
		
		
		<http://www.essex.ac.uk/centres/TheoStud/documents_and_files/html/11th%20Essex%20Conference%20in%20Critical%20Political%20Theory_call_for_papers.htm>
		
		
		My paper is called “The Origin of the Arts” and is a re-reading of Jacob Burckhardt's great book The Civilization of the Renaissance in Italy (1860). More importantly, it is an oblique critique of the West Kowloon Cultural District plan, a massive scheme to redevelop the lower edge of the Kowloon peninsula by way of a government sponsored arts district.
		
		
		<http://www.wkcda.hk/pe2/en/conceptual/index.html>
		
		
		The centrally planned WKCD is intended for a site exactly opposite Hong Kong proper on Victoria Bay. It is a superb example of authorized culture, versus the somewhat promiscuous and chaotic sources cited by Burckhardt as the “origin” of the Renaissance, including wandering poet-scholar-monks. As such, the paper serves as a critique of neoliberal cultural production post-2007, inclusive of Communist Chinese neoliberal cultural production – a huge contradiction in terms, to say the least.
		
		
		Moreover, as independent scholar (wandering poet-scholar), I must find funding to attend the conference.
7. [THIS CANNOT PASS (updated April 20, 2011) « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/03/7669/)
4.21.11 / 4pm


[…] since he was arrested by Chinese government security police on April 3rd. See the post containing Salman Rushdie's assessment of the situation. Also, follow this link for speculation by Ai's sister on his possible […]
9. Diego
4.23.11 / 11am


Meanwhile, in an office in Beijing or Shanghai, Western politicians negotiate their debt with communist yuppies that push his people to poverty and oblivion.
11. quadruspenseroso
4.23.11 / 12pm


Here we go again: Sir Rushdie the costumed iconoclast smoking his pipe in a baijiu distillery – dangerous puffing indeed. Mr. Weiwei's middle finger and Sir Rushdie's humming ego will back anyone's pride into a corner, especially the ancient variety perpetually fearing peasant revolt.  

Patiently implanting creative versions of proper habeas corpus justice, independent of police authority, administered at lower court levels would better serve the PRC than self-aggrandizing cowboy noise. Why has the PRC built modern architecture and left out modern justice? That's the smell of cheap shampoo on a street-side prostitute.  

Personally I would never attempt to confine a difficult stepchild in my home, especially when she's old enough to take care of herself. Perhaps the Chinese authorities take the opportunity to declare Mr. Weiwei a persona non-gratis and issue him a one-way ticket to Vancouver?
