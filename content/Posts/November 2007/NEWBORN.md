
---
title: NEWBORN
date: 2007-11-03 00:00:00
---

# NEWBORN


This blog is a month old, almost. 


Like any infant, its character has already been formed, partly by genetics, partly by the nurturing environment it inhabits. A consistent theme of my posts has been the topic of criticism—the genetics. The consistent contributions of commentators has been the critical discussion of ideas–the environment. I must say that I am more than satisfied by the quality of the comments made so far. They have been thoughtfully considered and skillfully crafted. More than that, they have been deeply felt, and probing. It is clear that those who have chosen to join the discussion so far are not afraid of words. That is extremely encouraging to me, as I believe words—particularly those written—are a vital part of architecture.


What I have in mind for the future of this blog is to post works and writings of others, and not just architects. This doesn't mean that I want to stray far from the subject of architecture, but only that I believe architecture, as a field of knowledge and also a practice, touches upon, or actively embraces, other fields and practices. This short post is, in effect, a solicitation for written works from anyone who has something to say or contribute to the discussion already begun. It is up to contributors to shape the discussion, to give it life, and direction. As you might expect, I will moderate, and critically.


Anyone interested in doing so should go to my website (lebbeuswoods.net) and fill out a ‘contact form,' letting me know what exactly you would like to contribute as a post. I will consider each contact in confidence and with discretion. I will reply to each contact, and in substance. Contacts, and potential posts, that I believe contribute to the discussions underway, or that establish valuable new directions, will be posted, with a succinct introduction by me.


Consider the possibility.



 ## Comments 
1. sergio machado
11.4.07 / 1am


What can I say when a man with your qualifications and experience takes time to talk with the monkeys?  

THANK YOU!  

I completely agree with you about the necessity (and urgency) of exchanging ideas in architecture.
3. [Super Colossal » Write for Lebbeus](http://www.supercolossal.ch/2007/11/05/write-for-lebbeus/)
11.4.07 / 8pm


[…] Blog Write for Lebbeus Posted by Marcus Trimble on November 5, 2007 Lebbeus Woods is calling for contributors to his blog. Just drop him a […]
5. river
11.19.07 / 7pm


I'm grateful for the opportunity to read the discussions which are arising from your blog. And more so for the invitation to participate. It may be my own limited field of view of the resources related to architecture on the Internet, but I feel as though you have initiated something both quite new and long overdue.


I should mention that I found my way here by way of Geoff Manaugh's interview with you. I appreciate his work with BLDGBLOG too!


My scholastic education in architecture was an exciting time period in my life. The design studio was a hot crucible of camaraderie and productive dissent. I have missed that environment; and I've found it difficult to maintain the dynamic conditions which are necessary for the growth of ideas since then. In the few days I have been reading here, I have pulled old drawings out, started to take stock of where I was going then and how I have changed, and thought of, or even contacted some of my old peers. We all kind of went away to our lives and quit talking. I guess most of us have been busy making our livings and/or raising kids in these incredulous times.


This has been a great pill for my nihilist leanings. If we are indeed approaching a singularity, or some other order of transformation, at least individuals who appreciate words, ideas, and the processes they drive in the architecture of our environment have an identifiable, accessible place to discuss and share them now, widely. I second sergio's sense of urgency.


Thank you!
7. [lebbeuswoods](http://www.lebbeuswoods.net)
11.19.07 / 11pm


River: it means a lot to me that the discussions here have captured your interest and, more so, that they have reawakened some contact with ideas and comrades that have meant something to you. Making a living and raising kids is something I understand very well. One purpose of the blog is to create a common ground for those of us who think the idea of architecture is worth struggling for, not as an ideology (God knows we don't need any more of those), but as a deeply human enterprise. We'll disagree as often as we agree, and that's a good thing. Our agreements give us a sense of community, and our disagreements will lead us to a finer understanding of our own thoughts, and enrich—if only by a little–the field of architecture as a whole.


Stay in touch!
