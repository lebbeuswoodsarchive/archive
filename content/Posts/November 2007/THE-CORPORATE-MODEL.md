
---
title: THE CORPORATE MODEL
date: 2007-11-25 00:00:00
---

# THE CORPORATE MODEL


[This is the second in a series of articles on the relationship between ethics and aesthetics in architecture.]


One of the main roles of architecture, historically speaking, has been that of making a particular kind of impression on the public, and that is the impression of the power of a few over many. The few are those who control the wealth and resources of a society, and have the financial capability to commission and realize large buildings in the public domain. The many are those who depend on the few for their livelihoods. The ascendancy of democratic, even populist, forms of government has done little to change architecture's role. Democracy has resulted in some redistribution of wealth—the merchant and the professional classes have a greater share of it than ever before, on the average and at their extremes, where many business owners, lawyers, doctors and architects have become rich. Indeed, the professional and merchant classes have largely merged, a phenomenon that has occurred in the past thirty years or so. 


For those who are willing to look back to an earlier era, the professional was distinguished from the businessperson by an ethical position. The aim of the businessperson is to make a profit. The aim of the professional was to provide a service. This position found an example in the United States by the fact that doctors, lawyers and architects had fee schedules, agreed upon through their professional organizations, which set the amount they were to be paid for a particular service. While there were always the exceptions, on the high and low ends, the professional charged according to the fee schedule, so that he or she would be judged (and hired) on the quality of the service they provided, their reputation, and not on whether they charged a higher or lower fee. At a point in the early 70s the distinction between product and service was challenged by the federal government, which said that the professional was a merchant whose product was a service. Fee schedules, it contended, were a form of price-fixing, which is illegal under ‘fair trade' laws, and could no longer be used. Professionals had to compete in the ‘marketplace,' just like everyone else. While the ethical implications of this ruling by the government were enormous, it was little debated at the time in ethical terms. Most professionals accepted the change and joined the merchant class, setting up various forms of professional corporations that took the place, in more ways than the economic, of private practices.


In a real-world sense, the ‘practitioner' was by then an anachronism, one belonging to a social system that had functioned more on a level of personal responsibility than the present one. For example, in the pre-credit card era up to the early 60s (when computerization made credit cards practicable), there were two ways to pay a merchant for a purchase. One was cash. As a result people carried more cash with them then they do today. The other was a personal bank check. The check is a promissory note that depends on the truthfulness—the personal promise—of the check giver, and on the trust of the check receiver, the merchant, for its efficacy as a medium of exchange. This exchange was more on a personal level than an institutional one. On a similar level, the professional practitioner had been judged by the efficacy or his or her service, no doubt, but also on a personal level that, by the 70s was being swept away society-wide by computerization and a homogenization of economic standards that effaced personal distinctions in favor of computable data. However, the loss of the personal, the intuitive, as well as of the lone professional practitioner, who assumes personal responsibility for his work and its consequences, has been the price to be paid for the expansion of economic opportunities gained through technology and globalization.


It is difficult to argue that the loss of the lone practitioner in architecture and the rise of the corporate architect has had a great effect on the way cities look or how they impact people living in them. To do so, one would have to demonstrate that cities ‘after' are appreciably different from those ‘before.' Still, the rise of corporatism has had one obvious effect. Architects, rarely leaders in society, have followed corporate trends toward the expansion and consolidation of markets by extending their practices first to the scope of the national, then to the international and global. One result has been the radiation of a new type of generic architecture, one that is reproduced in one city after another, around the world. This is not a universal architecture such as Modernist theory proposed would be produced in the same way everywhere by different architects following a fixed (implicitly Socialist) set of principles, but a generic of the idiosyncratic styles of highly individual architects, each practicing independently, not necessarily following any principles at all. It is the generic of the name brand, of the consumer commodity, of the franchise. Corporate architects market their styles globally, filling far-flung urban centers with their high-end products. This follows precisely the global corporate model.


LW



 ## Comments 
1. Godofredo
12.4.07 / 10am


In relation to the book itself, I find your situationist proposal very exciting (though I tend to agree with Sorkin when he defends that the solution of this conflict cannot expect to be based on groundbreaking political transformations).  

Even so, I find your proposal important because it proposes to ignore the causes of the conflict and reduce them to a specific political reading. In such a way it produces a strong criticism of the mechanisms of power (state and religious) behind the conflict and its imposition on both People's lives. I believe that if this criticism gets into people's minds (both Israelis and Palestinians) then it has a chance of producing something.  

But I'm also afraid that such proposal risks becoming a sterile intellectual exercise when removed from the Israeli-Palestinian context.  

Could you tell me if this book was published in Palestine or Israel? Who were you trying to address, and who do you think you've addressed?
3. [lebbeuswoods](http://www.lebbeuswoods.net)
12.5.07 / 6pm


Godofredo: I agree that my proposal for The Wall Game is very specific to the Israeli-Palestinian situation. That was my intention. The idea of ‘play' is, as Huisinga says, a valuable mode of exchange. I'm surprised no one has taken it up before, in whatever form.


My project is just one amomg many proposals in Michael Sorkin's book, which I consider an important initiative that is somehow trans-political. I don't know whether it's been published in Arabic and Hebrew—check the publishet's website. But it should be.


I don't trust politicians, of whatever nationality or persuasion. The latest iniative by them, in Annapolis, is mostly rhetoric. We can hope, but……
5. [lebbeuswoods](http://www.lebbeuswoods.net)
12.5.07 / 6pm


At the moment, I am at the University of Cambridge, at the conference Power and Space, organized by a group of perceptive and active younger people in architecture and media. This terrain is where I place my best hope.
