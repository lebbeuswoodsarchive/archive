
---
title: DENSITY FIELDS  Oyler Wu Collaborative
date: 2007-11-12 00:00:00
---

# DENSITY FIELDS: Oyler Wu Collaborative


(This is the first in a series of posts by architects whose recent projects are of interest to this blog. The text is by the architects.)


[![photo by Scott Mayoral](media/photo_by_Scott_Mayoral.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/dojwinstall2.jpg "photo by Scott Mayoral")


Density Fields grew out of a series of spatial ideas constructed over several years. The early sketches were an investigation into the build up of material densities and their resultant spatial affects. Our interest in constructing those ideas (which varied greatly in their formal qualities) was coupled with the desire to align them with a strategy of engagement specific to a site.


One of our primary concerns in the development of the project has been in seeking a method of development that uses a form of creative and generative study that takes into account the conditions that occur beyond the boundaries of our site. The first series of models study those two conditions separately, indexing and giving physical presence to the otherwise unnoticed series of geometrical relationships.


With that idea in mind, our site investigations took into account two primary “lines of communication” occurring within the space: 1) those running perpendicular to Silver Lake Boulevard, existing as a series of lines connecting points on both sides of the small courtyard, and 2) those running perpendicular to Silver Lake Boulevard, connecting windows, edge conditions, and points of entry to similar conditions found in and around the existing courtyard.


In combining those lines, the result is an extensive network of connections that are brought to bear on the small and otherwise nondescript site. This model speaks of more than a simple formal intervention, as it suggests a set of social connections, overlaps, and confrontations. The model's ability to provoke a sense of underlying energies was key in the formation of the intervention that was to come. Our primary intention in the design of the final piece was in translating the provocation, geometry, and vigor of those models into the confines of the site itself.


 In doing that, the project required a careful balance of structural ideas, the programmatic needs of the space, and the desire to use basic geometries to create a rich spatial experience. The result is a tremendous cantilever that relies on a delicate and structurally complex combination of aluminum framing and polypropylene rope. We were especially interested in the intricacies of the ropes as they wound their way through the frame creating a complex network of trajectories. It is within this “field” of rope that the piece is probably most successful, and it is here that it relies heavily on spatial qualities seen in the early ideas. 


It is probably also important to say…the piece was welded and installed by our office along with the help of a few volunteers (most notably a few extremely talented individuals in our office); this process of making has been a particularly inspiring development for our office. It has been the third in a line of summer projects that we have designed and built. The lack of conventional separation between design and fabrication has allowed us to use the construction phase as an extension to the design process. It has been especially helpful with our aspiration to create a level of engagement that is equally as powerful at the scale of an individual as it is as a site strategy. Overall, this process has led to a period of material discovery, invention, and experimentation that comes only through the difficult, but profoundly rewarding task of realizing the work on a given site.


Density Fields will hover over the exterior courtyard at Materials and Applications in Los Angeles, CA, through February of 2008.


[![DENSITY FIELDS, model study](media/DENSITY_FIELDS,_model_study.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/dojwmod2.jpg "DENSITY FIELDS, model study")[![model study](media/model_study.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/dojwmod1.jpg "model study")[![photo by Art Gray](media/photo_by_Art_Gray.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/dojwinstall3.jpg "photo by Art Gray")[![photo by Dwayne Oyler](media/photo_by_Dwayne_Oyler.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/dojwinstall1.jpg "photo by Dwayne Oyler")



 ## Comments 
1. river
11.17.07 / 6am


“It is probably also important to say…the piece was welded and installed by our office along with the help of a few volunteers (most notably a few extremely talented individuals in our office); this process of making has been a particularly inspiring development for our office.”


I resonate best with that quote, as subtextual as it may seem. I helped my dear pagan friends build a bucky-dome a coupla years ago, which is very outsider. They don't care. A few key folk, made it across the threshold of happening versus not-happening! This is what I think is important.


<http://www.elflore.org/thunder.htm>
3. [lebbeuswoods](http://www.lebbeuswoods.net)
4.30.08 / 3pm


NOTICE: THE “POSSIBLY RELATED POSTS” (AUTOMATICALLY GENERATED) DO NOT BELONG ON THIS POST. UNTIL I FIND OUT FROM WORDPRESS HOW TO REMOVE THEM, I ASK VISITORS TO IGNORE THEM.
