
---
title: WHAT IS ARCHITECTURE?
date: 2007-11-13 00:00:00
---

# WHAT IS ARCHITECTURE?


I would argue that any architect worthy of the name addresses in one way or another the prospect of building. That is to say, the architect's primary concern is the built environment, the physical domain of our experiences that is tangible, material, and constructed. However, I would also argue that this fact does not mean that an architect's work will necessarily be realized in the built environment. The reason is obvious: the architect is not the one who decides to build or not to build. That decision is made by others who control the financial and material resources necessary to build, those who own the land, or represent the prevailing governmental and legal systems. Architects do not build–rather, they make designs that instruct others what and how to build, if those others should so decide.


If this the case, then the question is: wherein resides the architecture? Is it only in the realized, built design? Many would argue this is so, and that the unbuilt design of the architect has only the potential of becoming architecture, and is not such in itself. It is hard to quarrel with this outlook, in that we have not yet invented an objective measuring device for detecting the presence of architecture. In this domain, architecture is detected by the attuned sensibilities of individual observers, which can and will vary widely. 


Rather than challenge this outlook, however, I will interrogate it a bit more. If architecture is only in the building, what, we might say, puts it there? The labor and skills of the contractors and mechanics who build the building? If that were true then every building would be architecture, and not even the most skeptical of observers would claim that to be the case. Architecture is something different from building. So what makes it different?


The usual answer is: a concept. Architecture is the built realization of a particular concept, or idea. This idea can be about construction, or the way people will use a building, or how the building fits into a physical, or a social, landscape. But wait. Every building has such concepts, even warehouses, convenience stores, garages–they are embedded in its typology, so much so that we don't even need to discuss them. Everyone knows not only how to behave in such spaces, but also what they mean in simple, everyday terms. This is the basis of what we call vernacular architecture, which is a vital part of architectural history. But a vernacular cannot account for, or lead to, the creative architecture that responds to the changes dramatically affecting the contemporary world. That architecture is something else. So, then, what is it?


I would say that architecture, as we understand it today, differs from building in that the concept, or ideas, it embodies are formulated in a unique, and not merely generic, way. In order for this to be so, it must originate in a single mind–the mind of an architect. One consequence of this understanding is that the architecture is in the architect's work–sketches, drawings, models–from the beginning. If this were not so, it would be impossible to somehow ‘inject' it later. Given this, it is merely a semantic debate as to whether the instrumental products of an architect's design process are architecture, or only have the potential for architecture. In either case they cannot be dispensed with, if architecture is to exist. 


And there is something else. The more concepts and ideas formulated by the architect have an immediacy for contemporary conditions of living, thinking, working, the higher we will value it as architecture. We want architecture to participate in the crucial changes affecting our lives, and not simply form a backdrop to them. 


Why is this issue important at all? Because the idea of such an architecture today is disappearing. Developers, corporations, and politicians understand the marketing value of architecture, as long as it's attention getting. But that cannot be all there is. Architects themselves, some of whom devote their uniqueness of mind and their talents for embodying ideas to serving the interests of developers, corporations, and politicians, are ignoring more urgently critical conditions. Continuing the struggle to understand what architecture is helps keep everyone—especially the architects—more honest.


LW



 ## Comments 
1. river
11.17.07 / 6am


Architecture is an art, in the old sense of the Latin word [artifex](http://www.babylon.com/define/112/Latin-Dictionary.html), which includes connotations of both [craftsmanship](http://links.jstor.org/sici?sici=0026-8232(192905)26%3A4%3C433%3ATSDOO%22%3E2.0.CO%3B2-J) and the requisite preconditions we might use to identify the work of an artist, such as [inspiration](http://en.wikipedia.org/wiki/Inspire). Modulation of the boundaries of space-time, including all of their visceral, political, and metaphysical properties, is the mode of architecture. The distinction between an architect and a composer is small. Composers tend to employ their artifice in the mode of sound. Choreographers bound their art in the body of dance. Poets, in words. Doctors, disease? (I refer to the television series ‘House'). These linguistic differences are very tiny indeed.


What is the aim of Art? is, perhaps, a better question. Why do we make, mark, record, and draw? Why are the hand silhouettes of [Lascaux](http://www.francealacarte.com/prehist1.htm) still so compelling?


With that kind of geologic and historic time frame in mind, I would like to propose something we already know. The environment we dwell in is malleable. We overlay our languages and hopefully inspired thoughts on it's fecund [nascence](http://dictionary.reference.com/search?r=2&q=nascence). But why do we do that? Fecundity just seems to outwit our moral instincts? We are too hot to rush in?


Now we have done so many, many times over. And we find that, from the lost myths of prehistory, we have moved into a thriving virtual world via technology. Imagine it! I am sitting in my den in Indiana real-time “where the extra hour of sun burnt the corn” saying this over the vehicle of my pow-wowerful workstation, full of software that I acknowledge, many cannot afford.


(Nonetheless, I am called poor by my kid's peers at school, as measured by standard USA Today pie-graphs. I live paycheck to paycheck. I do not have a wealth management plan, or even health insurance. I just have a ripping hot-rod computer. Built it myself, yep! Sweet ain't it? Don't get me talkin' about thermal constraints!


Oh, To bring this back to Architecture, my environment is full of neighbors and friends who have no clue what architects are talking about. These are plain folk who live in the shadows of towers and find ingenious workarounds to do their own stuff in their own way. Like gardening or Fixin' guns. These are also really, really great grandkids of the old genetic generations who wiped out the buffalo (nickel memorial), with really cool new machines, and left their carcasses on the plains to rot.


As we enter an age where architecture can be finally be defined more specifically as “software”, we are freer than ever to experiment with synesthesia. We can finally get into the mind of the targeted experiencer by the back door, and say something important about who we are. 


So, one of my deepest questions about creating worlds in architecture, is raising versus razing our children. Encoding the way they will parent their own and so on. Is there any consensus among architects about this point? I think my Pisces daughter sleepwalks at night because of cellphone chatter and widespread anxious dreaming beyond the threshold of perception. I think my hearty boys need to spend some time alone in the woods, if the woods just weren't so subdvided these days. More contiguous like. More wild. More old school.


I realize the proposition of lost wilderness is kinda anti-architectural. But architecture, especially now, sets the context for the enjoyment of teas, bacon, and even sex. It justifies war or nullifies the urge. Setting contexts is an ultimately responsible political power, which is demonsterably easy to overlöök. No use Babeling on about inter-nunsense.


Carry on, chaps!
3. Godofredo
11.17.07 / 6pm


This text is somewhat disappointing, not only because of your vague argument (understanding what architecture is keeps the architects more honest) but especially due to the ambiguous way in which you position yourself in such discussion. 


You seem to be concerned with the way in which most architecture is today reduced to a pawn in a marketability game controlled by developers and corporations. But this is apparently a battle that you avoid by keeping your immaculate conceptions in an exclusively architectural and academic circuit. Even though I admire your work I would also like to admire its confrontation with people and time. I think it would be very interesting to admire the way in which it relates with “the changes dramatically affecting the contemporary world.” 


Even if it is true that architecture has always been strongly influenced by un-built projects, and that some of the most exciting architectures of our century have never seen the daylight, one has to keep in mind that such architectures gain their meaning from the feedback circuit they establish with the built ones. By referring to the architect as someone who instructs you immediately avoid the complexity of the discussions between different agents, and the importance of these discussions to generate something much more intense than the polished ideas of imagination As you said it yourself, “architecture is a political act” and not simply a one-directional instruction of commandments… 


It is very easy to complain about the developers not paying attention to the ways in which the world is changing. But aren't the developers the ones which are there, in loco, changing the world – or at least large chunks of it? How do you fight the developers, how do you subvert their economic strategies, how do you dialogue with these people whose only point is to make loads of money out of peoples houses and cities? 


In this context trying to define architecture seems to be a very vague and pointless exercise, for if there is a quality in every creative act, it is the quality of always evading any sort of categorization and control.  

It is not enough to think about what architecture is (and particularly not when using such simplistic oppositions in between built and non-built, or vernacular and contemporary) when a real battle for the survival of any architectural thinking is at stake. But if one is to avoid such battle in favour of the mellow safety of the studio than the developers and the corporations have immediately won. 


In the same way, the concern about an ostensive contemporary approach in the conceiving of architectural strategies has, in most cases, done no more than ignore the complexity of the world (temporary instead of con-temporary), feed the publishing industry with pseudo-vanguards, and multiply dozens of Bilbao-Guggenheins.  

What is architecture and were it resides is not a relevant discussion at all. It is a discussion where no answer can be achieved which will not impose a totalitarian control over architecture itself.  

And it is there, in its un-definition that architecture's power to fight against developers lies. It is in its un-definition that its relation with the world lies. In the same way that there are not one but many worlds, there are also not one but many architectures.


The creative intensity of the confrontation with the world and the permanent feedback that it ensures, bring forth an architecture of permanent transformation, an architecture in becoming, an architecture not so much concerned about what it is but about what it could become.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
11.18.07 / 3pm


Godofredo: Your heartfelt, articulate comment is much appreciated. But I think your rather angry and defensive tone might be getting in the way of what you are trying to say. After your criticisms of my post (which I take seriously), it is not so clear what your position is. 


Clearly, you resent architects who make pronouncements from the safety of their studios, rather than being in the trenches, struggling with developers and other clients to see how architecture can confront reality. Such clients, of course, represent only one aspect of reality. 


Just as clearly, you think trying to define what architecture is is a waste of time, and that it is better to rely on architects' creativity and where that takes architecture. What is not so clear is whether you are endorsing the present status quo, or proposing a new approach. 


It seems to me that discussions of principle independent of building-oriented practice are valuable and necessary. The field has relied on the creativity of architects, without much theoretical reflection, for quite a while now. The result has been some spectacular new buildings, but little progress in the relationship between formal innovation and new ways of thinking and living. This is because developers and other clients may have a good eye for the look of innovation but are socially and politically very conservative, which is understandable, given their interests. It is up to architects, given their broader interests, to think through on a deeper level the changes affecting all of us and to formulate, in words and designs, innovative principles and programs.
7. Godofredo
11.18.07 / 11pm


I also believe that to think in terms of a principle independent of a building-oriented practice is valuable and necessary. But it is the act itself of posing the question of “what architecture is” that has originated my comment. On your request I will gladly clarify what I meant. 


Firstly I am concerned about the importance granted to such question when one is trying to address the problems of the relation between architects and developers.  

My problem lies in the amount of theoretical discussion which is produced in universities and magazines, and whose specific nature prevents it from bridging the gap to architectural practice. I would argue that it is not so much that the field has relied on the creativity of architects, without much theoretical reflection, but that the field has been separated in two, in between pragmatic and idealist architects. And I can't bring myself to find great deal of interest in none.  

There are plenty of architects with a strong theoretical grounding for their work. Unfortunately the ambition of most of such theories never seems to concern the practice.  

And at the same time there is this idea that architects who build do not possess such theoretical interest. I find this an impossible claim to make. Nothing grants me that architects who build the most horrid bits of commercial architecture are not in fact following a very precise idea about what architecture should stand for.  

It is not because architects do things which I don't agree with, that I will assume them to be an ignorant bunch, with neither honesty nor intellectual value.


I am also not saying that architects should bend their principles to be able to build. I am just relying on the examples that managed to bridge such gap. If we look to Corbusier or Kahn or Koolhaas, we can see how it is possible to question, to imagine and to build at the same time. But to develop a theoretical discourse is not simply to discuss what architecture is or what it stands for. Often such posing of the problem sees itself rapidly transformed in a simplistic exploration of what architecture could be if we would be living somewhere else. But how can we make that somewhere else come to be? How can we, trough architecture, participate in such transformation? And essentially, what does architecture do? What is it made of? How does it do it? What does it relate to?  

And this leads us to the main issue at stake here: to create architecture, to be inventive, implicitly implies a conception and a proposition of what architecture is, and what the world could be. Architects such as Zvi Hecker or Alvaro Siza or Lina Bo Bardi (just to name a few) have apparently not developed such a theoretical work. I say apparently because such theoretical work has in fact been thoroughly developed. Not through articles and publications but through its actualization in buildings. 


Secondly, I am concerned about the impossibility of such discussion: to make architecture implies its permanent re-invention, and between architects there can only be casual encounters, or singular paths that cross at a certain moment to part immediately after. I am in permanent discussion (with myself) about what is the nature of architecture, and hope never to achieve any satisfactory conclusion. I think to discuss what architecture is or should be only feeds totalitarian manias of creative control, and most of all I think architecture should avoid any predefined principle.


Finally, even if I obviously agree that “It is up to architects, given their broader interests, to think through on a deeper level the changes affecting all of us and to formulate, in words and designs, innovative principles and programs”, I think one should also keep in mind that the problems that this century's architectural vanguards have faced concerned the moment in which they've tried to build. (Maybe a far more complex discussion could be on what it means to “formulate innovative principles and programs”).  

And one can only profit form building, form having to relate with people with different backgrounds, from touching the materials, from learning the distance in between a sketch and wall. Any theory which attempts to relate to all this can only gain in interest and complexity.
9. [lebbeuswoods](http://www.lebbeuswoods.net)
11.19.07 / 1am


I agree with much of what you say. If you re-read my post, you see that I believe the goal of all creative work in architecture, theory included, is transforming the built environment. Also that it is work that addresses the critical conditions of the present that we should value most highly.


Our sticking point seems to be you think there is too much theory and I think there is too little. No doubt we have two different ideas of theory. 


As for formulating innovative principles and programs, my work over the years has addressed what I would call ‘spaces of conflict,' in their various forms. In these cases, it is not possible to apply normative theories, principles, or programs of living, without losing the meanings imbedded in the transformative human experiences. The architect who chooses to address these has no choice but to be innovative. 


However, I am not as trusting as you that innovative buildings communicate their theoretical aspirations. It is all too easy to manipulate images and misappropriate buildings. One notorious example is the way Philip Johnson—in the 1932 MoMA exhibition—stripped the architecture of the Bauhaus of its radical social theory in order to promote it as a mere style. 


The architects of innovative buildings need to not only express in words the theoretical intentions of their works, but also publicly fight for them.
11. aes
11.19.07 / 4am


A singular and totalitarian definition of architecture, or art in general, is of course impossible; indeed there will always be plurality in art and culture since it is precisely a society's art and culture that best encourages peaceful and productive pluralities (as opposed to, for example, a society's politics). Assuming that art thrives on (indeed survives on) the debates and exchanges between these pluralities, then for me the question of “what is architecture” prompts the following additional questions: 


What is the common ground upon which such debates can take place? What do we all agree on as the fundamental qualities/necessities of architecture? Extremely base questions, these certainly are. Primitive and unacademic, I know. 


But I raise them out of a concern that comes out of witnessing a day-long conference held at CCNY some days ago, called “Ineffable”, of which Lebbeus took part among many other distinguished panelists. And in this conference, a recurring rift appeared, effectively dividing the fifteen or so speakers into two basic camps. On the one side there were those (accused by their opponents of being conservative) for whom architecture needed to (re)focus on the human experience. On the other side, there were those (accused accordingly of being technophiles) for whom technology offered the means to seek non-anthropologic modes of architectural thought, and for whom the question of what it means to be human remained an open and flexible question.


On the one side, Heidegger, poetics, and phenomenology; on the other, Leibniz, computation, and cyborgs.


I don't mean to revive that debate here; but what did occur to me during this day-long seminar was that there simply wasn't enough common ground for the debate to prove productive; how could there be, if they couldn't even agree on the human condition, the basic pathos of our efforts as architects?


And in this sense I agree that there is both too much and not enough theory. There is indeed an inflated amount of empty theory in certain academic trends: I worry when architects start talking about data-crunching. When an architect spends 20 minutes talking about complex computational models that can (at great effort) simulate the patterns that waves make on a beach, I worry that they have forgotten the physical weight of sand and water, instead obsessing over abstract immaterial (and no doubt imprecise) forms. And yet I also feel that this is not theory, this is not architecture. This is not reality, this holds no gravity. If contemporary architecture seems hollow, it's because the theory that much of it claims to espouse has no substance.


And in this sense I greatly appreciated Lebbeus' presentation, which consisted of sublime, and terrifying, images of war-torn Kosovo, earthquake-stricken Taichung, Ground Zero. It was real; it was a reminder of what our theories and our debates need to address. 


If the human condition as we know it and live it, if the necessity for anthropologism and the human experience at the center of all art and civilized culture is not enough of a common ground, then what's the point of architecture in the first place?
13. aes
11.19.07 / 4am


sorry, one final point–


in the previously mentioned debate, I've obviously taken a definitive side. so unabashedly i offer the following quote from alberto perez-gomez:


“To understand and project the lessons of our human heritage, we need memory, and memory is built from linguistic interpretation. This basic operation enables the project to become an ethical promise, contributing to the evolution of humanity and not merely producing irrelevant novelties. All the while we must acknowledge that the full transformative power of architecture is an act that can be paraphrased poetically but is impossible to explain systematically.”


Architecture is/as/in an ethical promise.
15. Godofredo
11.19.07 / 10am


Just to clarify a final point. I'm not saying that there is too much or too less theory. I am criticizing the nature of that theory and its lack of ambition.  

And I am also criticizing the proliferation of the specific status quo of the experimental-innovative-theoretical architect who never builds. By working in London I am very aware of the reality of the developers work and the subservience of architectural practices. And I am also aware of the distance in between what is made in this city and the ideas explored at the Bartlett or at the AA for instance. But I've studied in Portugal where we have a completely different situation. If Portugal is in the need of a very strong non-pragmatic discussion – in order to bring back desire into architectural practice – the Anglo-Saxon world seems instead to be slowly dying from incapacity of communication between pragmatics and theorists. In Portugal there is not a publishing and media circuit for the discussion of architecture. We have only a hand-full of magazines and journals. If one wants to say something one has to design and build and fight for it. And, despite the fact that so many Portuguese architects don't have really nothing to say, the fact remains that they have a much more aggressive and fighting attitude then their British counterparts – who assume most of the times the role of a simple spatial and aesthetical technician.  

I think it is vital to engage in a fruitful dialogue between theorists and pragmatics, and if possible to abort such distinction at all.  

And I am having this discussion with you, precisely due to the fact that your work is one of my main architectural influences.
17. [lebbeuswoods](http://www.lebbeuswoods.net)
11.20.07 / 12am


aes: As you know, I'm sure, the ‘anthro' or ‘techno' divide has been there since Descartes divided mind and body. The techno side runs in modern times through Hannes Meyer's radical functionalism, Alexander's design methodology (which owes something to Skinner and behaviorism), right up to today's digital simulators (simulation being the bane of the digital world) and cyborg architects.


What makes me uneasy is the success of the anthro side, which is too often clothed in pseudo-humanistic, quasi-religious raiments. I would be happier if we anthros had harder-edged arguments for why human beings—including their imaginations and dreams—are still relevant at all. I take that on as a personal mission, though I grudgingly acknowledge my limitations, generational and otherwise.
19. [lebbeuswoods](http://www.lebbeuswoods.net)
11.20.07 / 12am


Godofredo: you don't have to live in London, or Lisboa, to confront the reality of developers. But that's not the only front line in our field. Another is internal: the formation of a coalition of architects, based not on a consensus of agreement (like CIAM), but on an intensity of discourse–one that can effectively assert the power of ideas in a game now dominated by the power of money. I believe that—together—we architects have real power that we have not been able to assert as yet, because we are so divided by competitiveness. I do NOT believe that it is necessary to relinquish our creative competitiveness (to make the best) in order to flex our collective muscle. Let's turn up the heat….
21. christian
11.20.07 / 9pm


That mentioned relevance of the human being – I guess is meant here for architecture – is – I agree on that – controversial. For me I see a kind of flipping the relation between mind and body over. The modes of simulation or generative processes actually get features of a work doing machine. Creation of things thoughts and so on. The purpose a body was used for. Whereas the body for me has notions of a memory and sensing tool. The term “experience” seems to me to a bigger extent orientated towards emotions than to actual enlightening thoughts. Also the memory of your own shaped tool (body – fitness – overweight – illnesses – genetics – etc.) exposes history to people who share the same social context with you. My arguments are maybe not totally convincing but I know for sure that the mind is the least trained “muscle” for many, including myself. So to say .. in the strife for the higher existence where the mind is able to “disconnect” from the body it is actually being pushed in the background. there is this quote ” Only if everything is dead man will see that you can't eat gold” – so to say with death it ends. For all of us. Even if you think of Vitruv and his still existing influence he would be nothing important without living humans. That is obvious. There are so many things wrong or held back or never done because of the human ego. But it needs so little to keep one person alive. I rather would build a house that makes people smile – like a great song does. What reaction more does it need?
23. [lebbeuswoods](http://www.lebbeuswoods.net)
11.20.07 / 10pm


Christian: I like your flipping of the mind-body duality, because it forces some fresh thinking about both, and also, perhaps, about the artificiality of the division. Descartes ‘method' has been so successful because it ‘works,' pragmatically, not because it is true. 


Obviously, mind and body are inextricably interwoven, but no philosophy has emerged since the Cartesian that enables us to operate so precisely on questions of matter and meaning. If we look at Bergson, for example, his synthesis of ‘matter and memory' reunited mind and body very convincingly, but left us with nothing like Cartesian geometry to plug into the every day. His idea of “elan vital,” had about it more than a whiff of the mystical. For that reason, perhaps, it was very popular for a while, but, for the same reason, is now neglected.


On your last point. I would say that you can divide architects into two groups. The first is satisfied “to make people smile” with their work. These are the poet architects, who are happy to make something wonderful and moving. The second group immediately asks, “which people?” These are the philosopher architects, who are satisfied only when they believe their work has effectively addressed the wider human condition.


Both are equally necessary and valuable. The ideal is, of course, to unite the two, and that's a condition worth aspiring to.
25. aitraaz
11.21.07 / 12am


Could part of the problem be that architects are so easily seduced by “poorly stated problems” (Bergson) and weak transcendental concepts?


What is a concept or idea's relation to power?


As Deleuze and Guattari so eloquently put it, language has much more to do with power than it does with sense, i suspect the same applies for concepts.


Content…expression….content…expression…


An architect nowadays needs neither poetry nor philosophy. He needs a toolbox.


What relation did the invention (yes, invention) of the Cartesian coordinate system have to power? 


Built, unbuilt, poetic, philosophical…


“One gives a child a language like one gives a worker a pick axe and chains”
27. Godofredo
11.21.07 / 2pm


Lebbeus: Aes has referred to an opposition between phenomenology and a creative understanding of technology. I find such opposition very amusing for Heidegger himself showed how there is no separation between technology and being. Heidegger showed us also that to understand technology one has to go beyond an instrumental and utilitarian view. Ok, maybe he could have learn a bit more from his predecessor Jarry (as argued by Deleuze) in order to discover how “the machine will do the decent thing” but still he stated that were the danger lies the answer lies also…  

Both he and Merleau-Ponty completely dismissed the Cartesian dualism in favour an essence of the phenomenon which lies neither in the thing itself nor in the mind apprehending it, but in its movement of becoming…  

So why do you insistently confuse that with Cartesian thought? Or are you in fact criticizing the lack of ability of some architects to learn from what phenomenology was giving us? 


But then we have the cyber world. Wouldn't that be far more related with the Cartesian divide than the phenomenology and the “anthro” side? And why are you only concerned the “anthro” side when the last years have been dominated by a Cartesian and instrumental view of technology, a world where architects look to technology as a source of possibilities and are unable to grasp its virtualities? Or how are we to conceive a world dominated by the “unlimited finitude” of the bit? Not by conceiving it as a mix of both – as in Haraways's cyborgian style. It is still the divide that feeds the cyborg. Could we have low-tech cyborgs? What is the nature of the current divide in architectural technological theory, between low-tech and high-tech?


Would it not be far more interesting to conceive a body in terms of what is can do (back to BWO, and the assemblage?) and not on what it is – be it the body of architecture or of any machine?  

We need to provide strong conceptual creations, avoid badly formulated discussions, and as Aitraaz said, “poorly stated problems”. 


You refer to the question “which people”? And I ask again, which people? The ones that correspond to the architect's notion of what the present might be, or the ones that correspond to what the architect desires the future to become? Should you not be asking which events? Which creations?


I would argue that the only solution to avoid the soft-edged utopianism that has flooded the architectural discourse lies in the radical experience of making.
29. Godofredo
11.21.07 / 2pm


I would like to clarify that when I state that you confuse the Cartesian divide or the birth of modernity, with phenomenology and a more general “anthro” side is simply because you refer to the distinction between “anthro” and “techno” as something originated in Descartes. I find this arguable, and referring to architecture I would instinctively place it the opposition between Reyner Banham and Ernesto Rogers.
31. lebbeuswoods
11.21.07 / 9pm


Godofredo: As I am now accustomed to, your comments require a lot of unpacking. Today I can only manage a little. There will be more.


When I hear the word ‘essence,' I get out my….caution. For me it is a very soft and fuzzy term. Indeed, the whole of phenomenology seems to me the materiel of the poet architect. I think of Steven Holl, whose work I admire, who not only quotes Merleau-Ponty, but credits his ideas with inspiring some of his best work—the chapel in Seattle, the Nelson-Atkins Museum addition, among many others. His work is far from fuzzy, and for that we can all be grateful. He is a poet architect of singular distinction.


When it comes to the urban, if not global, issues of cities and their complexity, phenomenology is of little help—at least so far. Maybe with the involvement of the digital. But now we come to the Cartesian.


Cartesian (analytical) geometry (the main distinction of which is the exact description of complex curves) gives us a precise tool for analyzing, and designing, space of any shape. Newton combined it with pre-Socratic philosophy (partucularly Zeno's Paradoxes) to create Differential Calculus. My admiration of Cartesian geometry rests not so much on its elegance, as on its raw utility. Phenomenology, in whatever guise, needs a poet to interpret and apply it. The Cartesian method and system is something that can be applied by anyone, even non-poets, and most often is. Also…let's not forget that the digital owes (almost) everything to the Cartesian.


I am looking for a harder edge to the ‘anthro'—human centered—side, and I do not find it in phenomenology as it now stands.


More later….
33. [lebbeuswoods](http://www.lebbeuswoods.net)
11.21.07 / 10pm


aitraaz: Great style! 


Would you kindly provide examples of the 1) poorly stated problems, and 2) weak transcendental concepts that seduce architects? Only to better understand what you're saying….
35. Godofredo
11.25.07 / 11am


Dear Lebbeus, as you well know there is a lot more to architecture than drawing shapes. And that's why architecture has been influenced not only by Descartes but by Heidegger, Merleau-Ponty, Foucault, Sartre, Baudrillard, Lefevre, Derrida, Debord, Virilio or Deleuze and much more. A lot of hard-edges if you're willing to look for…  

Even so, you ask for more precise tools and an objective definition of architecture.  

But that I hope philosophy will never give.


As for the influence of Cartesian thought on the digital:  

Nothing more than the same fear of the unknown that produced all the great meta-physical narratives of the western world, and that has now found for itself a nice clean and objectified territory. It has even destitute the word “virtual” of its creative beauty and reduced it to a puppet of simulations. It has mixed sign with information. It has imposed representation as the necessary currency of an “interiorized world”, cast away form presence or experience. 


(It has nonetheless produced a great playground for subversion…).
37. [lebbeuswoods](http://www.lebbeuswoods.net)
11.25.07 / 4pm


Godofredo: No, I'm not looking for an objective (so-called ‘scientific') definition of architecture, but rather a less ambiguous, less manipulable relationship between architecture and the human condition as it now stands, particularly in its social and political dimensions. If we're looking to philosophy for clues, I think we can better look to epistemology, than to metaphysics. One example (from Lyotard's 1979 “The Postmodern Condition: A Report on Knowledge”):


“Finally, it must be clear that it is our business not to supply reality but to invent allusions to the conceivable which cannot be presented. And it is not to be expected that this task will effect the last reconciliation between language games (which, under the name of faculties, Kant knew to be separated by a chasm), and that only the transcendental illusion (that of Hegel) can hope to totalize them into a real unity. But Kant also knew that the price to pay for such an illusion is terror. The nineteenth and twentieth centuries have given us as much terror as we can take. We have paid a high enough price for the nostalgia of the whole and the one, for the reconciliation of the concept and the sensible, of the transparent and the communicable experience. Under the general demand for slackening and appeasement, we can hear the mutterings of the desire for a return of terror, and the realization of the fantasy to seize reality. The answer is: Let us wage a war on totality; let us be witnesses to the unpresentable; let us activate the differences and save the honor of the name.”
39. AndrewC
12.8.07 / 7pm


“The taste of the apple… lies in the contact of the fruit with the palate, not in the fruit itself; in a similar way poetry lies in the meeting of poem and reader, not in the lines of symbols printed on the pages of a book. What is essential is the aesthetic art, the thrill, the almost physical emotion that comes with each reading.” Borges.
41. Zach V
1.29.08 / 3pm


I'm a philosopher, not an architect, but was turned on to this blog via my brother who is…


It's true that every definition of architecture is made for a purpose, and that it can be defined in many ways, as AES points out. What is the value of this definition?


You make a Deleuzian argument about architecture, LW. Like philosophy, it's about the production of concepts–let their value be judged by the events (bulidings, sensations, dreams, etc.) they make possible. This puts the emphasis on the imaginative work of providing new courses of action, ways of being, human relationships. This is necessary because we are often stuck in a style or set of materials that narrowly determine the ways we could build.


But this definition has a danger of dividing theory from practice. I think this is one of Godofredo's worries and it is one reason why Gehry is often criticized–he has one idea and just builds it wherever he can. If there is a danger of totalitarianism, it is in separating theory from practice, idea from application, and simply fetishizing the idea.


Your definition also threatens to demean building and builders–those who do it are not “real” architects (now we see power in play!). Time and again, dreamers (philosophers in particular!) have come up with ideas that have consequences they cannot control when put into practice. For instance, Nietzsche could not know how the Nazi's would use his concepts. Corbu didn't know what “mass production” would come to mean. Thus, the pure idea must take into account its possible applications.


This is why I would define architecture as the entire interplay between concept formation and development. The site will determine the concept, and vice versa. The constructive interplay between these two poles is an art, requiring attunement to the needs of developers and sites as well as an imagination for what might be possible.


I would thus call what you do, LW, not architecture but concept formation. This is important, but is only one pole of the architectural process. If architects become too wrapped up in their ideas and their universal applicability (sustainability is always right, media technology is the way we must go, etc), then they not only corrupt their ideas and ignore the needs of sites, but they make building into just the application of an idea. The idea and the site, however, need to be dialectically related. 


You may come up with radical ideas, but if the public is not radical, there's nothing you can do about it. Let's separate politics from architecture and write treatisese like hypotheses. “If you want to be environmental, here is how you should build…” Whether or not people are going green is not the architect's problem. “If you want a building to be a disruptive experience, you ought to…” Whether or not people want big boxes and house farms is, again, not the architect's problem. As a dreamer, you may give the public ideas, but you cannot force them upon sites/peoples that don't want them.


-Zach
43. [Think Ark » Quick Post | What is architecture?](http://thinkark.com/uncategorized/quick-post-what-is-architecture/)
11.24.09 / 10am


[…] Lebbeus Woods in What is Architecture? […]
45. [Architecture « Jakob](http://jakoblawman.wordpress.com/2012/03/11/architecture/)
3.11.12 / 7am


[…] This blog from Lebbeus Woods states that “the architect's primary concern is the built environment, the physical domain of our experiences that is tangible, material, and constructed.”  Ultimately, architects create an experience, which comes from the nature of the building's function, endurance, and aesthetics.  Architecture exists to sustain the flow of society, fulfilling our needs to progress into the future. […]
47. Timothy Latim
5.8.12 / 10am


Reblogged this on [letters and lines](http://atpaa.wordpress.com/2012/05/08/147/) and commented:  

There is some element of truth to this. But I disagree to the notion that architecture is only associated with a unique concept. I think it is either good or bad architecture, every time a work gets realised (built or still on paper) it is architecture. The question should be whether or not it is good or bad.
49. Timothy Latim
5.8.12 / 11am


I like the definition of architecture, however i disagree that architecture only resides in a unique concept. 


” would say that architecture, as we understand it today, differs from building in that the concept, or ideas, it embodies are formulated in a unique, and not merely generic, way. In order for this to be so, it must originate in a single mind–the mind of an architect.”


All works once realised (on paper or built works) are architecture. The question that we should be asking is whether or not the work is exemplary? Is it good or bad architecture. I don't think we can disassociate the profession to only include its good works.
51. Sakshi Gupta
7.9.12 / 8am


Sir plaese give me full knowladge about Archicture Course. what is Archicture
