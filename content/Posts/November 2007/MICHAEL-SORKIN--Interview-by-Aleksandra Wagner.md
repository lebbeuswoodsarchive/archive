
---
title: MICHAEL SORKIN  Interview by Aleksandra Wagner
date: 2007-11-26 00:00:00
---

# MICHAEL SORKIN: Interview by Aleksandra Wagner


[This interview originally appeared in COVJEK I PROSTOR (Man and Space), a journal published by the Croatian Architects Association, #07-08, 2006, pp. 39-47, with text in Croatian and English. The title of the interview was “Against the Wall.”] 


[![photo by Miki Kratsman](media/photo_by_Miki_Kratsman.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/wall1.jpg "photo by Miki Kratsman")


The book Against the Wall was published in 2005, by The New Press. Edited by Michael Sorkin, it is a collaboration in which 21 authors – from architects to psychiatrists and philosophers – offer their analytic views of the political-architectural artifact, the aim of which is to separate and secure (by the fence, according to some) or to wall off (by the apartheid wall, according to others), Israel from Palestine. The barrier which started to emerge on June 16, 2002, the length of which will extend over 400 miles, with the projected cost of $ 3.4 billion, has since become the subject of numerous controversies. Despite its specific location, the book does not treat the fence/wall as an isolated phenomenon, but rather approaches it as an element in the contemporary repertoire of “wallings” – from Berlin to Nicosia to Kashmir to the 38th parallel in Korea to the US/Mexico border. While providing security for one side or the other, these wallings mark both the depth and the scope of failed politics. 


Michael Sorkin, an architect and urbanist, principal of Michael Sorkin Studio, is the Director of The Graduate Urban Design Program at the City College of New York. From 1993 – 2000, he was Professor of Urbanism and Director of Institute of Urbanism at the Academy of Fine Arts in Vienna. He has authored and co-authored numerous books: Variations on a Theme Park, Local Code, Giving Ground (with Joan Copjec), Some Assembly Required, After the World Trade Center (with Sharon Zukin). With his written word – especially as a long term architectural critic for The Village Voice, as well as contributing editor to Architectural Record and Metropolis – Sorkin has distinguished himself as a clear, spirited, and uncompromising man and architect whose ideas and interests are in the conflicts of the contemporary world. 


AW: Michael, how would you chart your trajectory as the critic, author of books? What drives you?


MS: I am political, so I act in the spheres where I can. I was drawn to architecture – and to urbanism in particular – because of their intimacy with the social. I believe in the idea of critical practice, that one must make propaganda for justice wherever possible. Yet, the stack of books that I've been producing is also an emblem of my frustration at not finding an accommodating building practice. 


On the other hand, I would like to think of myself as an artist. The way in which one strategically reconciles the autonomy of an artistic practice with the directly engaged character of a social one is the eternal juggling act. The answer to the crisis of exponentially growing cities, to the millions living in slums, to unequal distribution of access and privilege in the world, is – among other things – to build different cities than those we have now. In the absence of that possibility, the least I can be doing is argue for such cities and to try to show what they might be like. 


I am presently working on Indefensible Space: The Architecture of the National Insecurity State. To be published by Routledge, it is a collection of pieces by various authors about post-9/11 paranoid spatial practices, about the way another view of the city is taking over. 


AW: How did the book Against the Wall come about? 


There is, in the background, a long-standing interest in the region that has led me to several projects. I went to Israel in 1967, trying ‘socialism in action' on a kibbutz. What timing! At that time, I was only dimly aware of the real situation that unfolded so dramatically after the acquisition of the territories. Then followed a very long period in which I didn't go back. Finally, some thirty years later, I was invited to a very glossy architecture conference in Jerusalem: lots of international stars were there to discuss interventions in historic cities of which Jerusalem was the prime example. 


On that visit, I was gob-smacked by two things; first, actually seeing the settlements on their ravaged hilltops – the literally stratified hegemony that Eyal Weizman speaks about so eloquently, and second, being at a conference attended by many who consider themselves progressive, yet without one Palestinian participating. I inquired, and got absurd answers, like, “We wanted to invite them but we thought they wouldn't come, so, we didn't.” This is particularly absurd once you actually encounter Jerusalem, a small city in which everybody is physically living cheek by jowl, if not emotionally, then politically or communally. It just seemed mad to me that one could sanction such a level of myopia and denial. 


So I decided to organize a conference of my own – insisting on equal representation of Palestinians, Israelis and others – where people spoke about the future of Jerusalem as a shared environment. The premise I had, as the organizer, was based on an obvious fallacy – that we could somehow get together as architects and urbanists to speak about the future of the city outside of political arrangements. Although this may have been a complete fiction, it was a useful one and I asked each participant to make a presentation based on a physical proposal for the just amelioration of the city. This is how the book, The Next Jerusalem (2002) came about; at the same time, it established a cadre of colleagues and a connection to the politics of the region. When the construction of the Wall started – and I regard it as an architectural event, among many others – I decided to make the next collection which is much more confrontational and not at all infused with the utopian mood which characterized some of my earlier projects. This was the book, Against the Wall. 


AW: Can you speak more to the fallacy – a belief that architectural issues can be discussed outside of political arrangements? What sort of a corrective to this fallacy did you establish for yourself?


MS: That the arrangement of space somehow produces the arrangement of souls or politics is an historic fallacy beloved by architects and planners. This particular view is the gift of Enlightenment ideas of perfectibility, channeled through utopian socialist thinking of the 19th century, and it is the substrate for the ideology of modernity. Obviously, there are vast reciprocations between spatial and political or social arrangements, if only in that architecture is a fairly unerring record of contemporary distributions of power, authority, and meaning. The fallacy is to reverse this relationship, to think that the making of space can ever, by itself, change society, that it is the equivalent of revolution, as Corbusier famously insisted. In my heightened expectations about what architects should know – as proprietors of an artifact that is so eloquent in representing the social – I always expect my colleagues to be a bit more sensitive about what they are doing, about what they're making concrete. Architecture is never non-political: it always reinforces a set of social relations, whether within the family or between the ruler and the ruled. Architects were and are participating in the construction of the Wall, in the construction of the settlements in the West Bank. This inescapably colonial and military practice throws the responsibility of professionals into particularly high relief. Such practitioners cannot escape being held to account for what they are doing. 


AW: You say that the Wall is an architectural event. Some authors in the book refer to it as to an unfinished project, where the perpetual act of (endless) construction is the core of its ‘meaning,' rather than the constructed object itself. 


MS: I am assuming that anything that involves visible construction in the environment is at least a cousin to architecture. To the degree that the Wall is a giant physical artifact, that it is material, that – as a wall – it is one of the most primal architectural devices, I cannot see it otherwise. And, as many contributors to the book have recognized, the Wall is not an autonomous object. It does not exist in isolation, but in relation to the numerous other spatial practices, including the bypass system, military encampments, the settlements, the laminar division of space through intense topographies of control, deployed in layers starting with archeological digs and ending with the domination of air space. All of this represents ways by which power organizes space in its own interest. The problem is not simply the Wall, but the set of social and intellectual constructions – the politics – that produce different physical manifestations of which the Wall is only the most visible. 


AW: Several authors in the book, most notably Mike Davis, address the fact that in the present all-globalizing and seemingly open and fluid environment, more and more ‘wall-efforts' are pursued.


MS: Globalization is Topic A in almost all political, social, architectural, economic conversations nowadays. Despite neo-liberal rhetoric of free “flow,” ideas of walling and control are absolutely pervasive and the ‘homeland security' regime we are suffering in one iteration after another in the United States is completely symptomatic. We are the world leaders when it comes to inventing the disciplinary protocols of the global environment; raising the level of suspicion about the other; walling off the homeland with both physical and virtual technologies; introducing insane levels of surveillance; attacking, invading around the world. Clearly, though, all our technologies of instant adjacency are Janus-like. They do link us in a liberating, McLuhan-oid, global-villagesque way, enlarging our networks and spreading information. On the other hand, we know that the same technologies are crucial instrumentalities of surveillance, networking, control and domination. Sorting this out is a crucial struggle. 


One of the reasons why I find this particularly salient for architects, urban planners and all space-making disciplines, is because virtual space risks becoming a surrogate for physical space and is destabilizing traditional, body-based, neighborly relationships that grow out of physical forms of propinquity. Virtual connections, despite their ubiquitous reach, produce immobility – think of those kids glued to their computer screens who never leave their homes. They also produce the crazy regimes at the airports, where one is constantly being recorded and surveilled, and create one of the characteristic urban forms of our age – variously known as ‘the edge city,' ‘the generic city,' ‘the non-place urban realm' – in which all stable artifacts of locationality are blown away; where everything can be anywhere. Again the Janus: there is something potentially liberatory about this possibility. If we love our neighbors, if our industries do not pollute, if we can move freely … then there are some radical possibilities for thinking about an open, accessible city. On the other hand, if anything can be anywhere, then there is also another possibility – one which seems to be developing ever more rapidly – of universal equivalence, of a narrow repertoire of forms deployed according to the imperatives of cheap labor and cheap land. So, the world of universal sameness emerges on the other end of the spectrum, with Starbucks on every corner. The struggle to both conserve and invent within this context is something fascinatingly difficult and utterly critical. 


AW: In your Introduction you refer to a “non-violent, ethical activity, a model for this book” by which you call for a different practice of architecture and for a different stance in thinking about the built environment. I am interested in the notion of ethics, as well as in violence that might be linked with architecture. What did you have in mind?


MS: Architecture is capable of participating in, and of producing, all kinds of violence; it parallels other structures of violent behavior. In this case of the Wall, there is the violence of the disruption of familiar patterns and relationships, there is a violent assault on the possibility of leading a life of calm and tranquility, there is the violence of imprisonment, there is the violence of ugliness. Architecture can do the violence of homogeneity, of deficiency, of displacement – it can be a barrier to many, many things. 


I would argue not only that other ways are possible, but that architecture does have clear ethical duties. These begin with many about which all agree – to be safe, to protect against fire, rainfall, toxins. But there are more, including enhancing the possibility to live a good life, stewarding our relationship to the finite resources of the planet, or respecting the physical compacts of existing good-city forms. And, architecture's duties also include establishing and symbolizing various means of equitable distribution of spaces, resources, goods, relationships, experiences. Our duty is to make things better – what could be more depressing for an architect than images of Beirut reduced to rubble? I do think we have a big problem with the lingering romance of images of the violent. It seems that, because we live in culture in which things are more and more disposable, we fall into stupid exaltations of the so-called creative violence of capital, constantly engaged in the allegedly productive cycle of renewal and re-invention. Not all destruction is creative.


AW: From what you know and hear, what is it like to be an architect in Israel today – working in the shadow of the Wall, so to speak? 


MS: Let me start from a different perspective. I remember the first specifically architectural demonstration I went to, just after I had begun design school. It took place in front of the offices of SOM in New York where they were designing a big corporate skyscraper in Johannesburg during the apartheid days. The leaflet that was handed out by the organizers was succinct, stating that somebody upstairs was designing two bathrooms – one marked ‘Black' and one marked ‘White.' 


A lot of architects in Israel confront situations that are as schematic as that. Deciding whether or not to participate in projects like constructing settlements or the Wall offer a very clear choice. But, stepping back a bit, Israeli architects also must deal with the same kind of choices the rest of us have to make: at what remove from a system that, among other things, is behaving criminally, must you situate yourself in order to keep your conscience clear? In this country, for example, one of the few activist architectural groups still on the scene has been trying to organize a boycott of the design of prisons, contending that we tend to look to disciplinary solutions rather than considering more constructive ways of solving social problems. According to this argument, one has to refuse to participate in the larger project, no matter that there are people who deserve to be incarcerated for one crime or another, and withdraw all participation in order to make a larger point about the need to draw distinctions in a culture that is refusing them. 


It's very important to remember, though, that there are many architects, geographers and other spatial practitioners in Israel who are remarkably articulate in framing these issues, and who are powerful activists in the resistance. Israel is fascinating for the compact trajectory of its own evolution and the high relief of its self-consciousness and debates. The association of the Zionist project with the project of modernity in general; the strong self-identification among Israeli architects with modernist architectural forms; the way in which they were forced to confront the essentialist modernist position about subjectivity and its universality in a context which begs questions about the meanings of difference so profoundly, so loudly, so irresistibly … Israel is a place where many issues of general importance are intensified and articulated by a culture that is smart, neurotic, beleaguered, angry, and living in a small, highly contested, deeply anxious environment in which things operate at the extreme. Therefore, it produces many gross instantiations of issues that also occur in our own everyday life. The settlement in the West Bank is related, in kind, to the gated community in Orange County, California; it is completely comparable physically, and it raises the idea of conflict and of an exclusionary relationship to the Other to an absolute flash-point. 


AW: In more than one article in the book, the new – yet pervasive – emergent features of our time are labeled by terms that rest on –cide. There is an urbicide, but also, spatiocide, politicide … Do you find this language useful for thinking about the specific, as well as global, social conditions?


MS: I do find it useful. The oldest word on the list, urbicide, a Marshall Berman coinage, refers to the use of one spatial practice to annihilate another, something that New Yorkers like Berman have experienced with our famous “master builder” Robert Moses and his Haussmaniacal interventions around the city. This is what is happening in Palestine; the colonial power is using the arrangement of space in order to demolish the native spatiality of Palestinian identity, of Palestinian life. This is not intrinsically different from the Robert Moses example, from the urban renewal era in the United States in which the other's habitat was described as blight – a factory for crime and “terrorism” – and simply blown away. If anything, we've been going backwards: the great boom in prison construction in the U.S. is in many ways analogous to the housing projects of the great slum clearance days. Both were meant to solve the problem perceived by privilege, which was that the other was behaving criminally. This line of thinking led to the following logic: by re-housing Them, we will interrupt the production of criminality. But, we will re-house Them in isolation – in colonial fashion – on Their reservations. This process has been re-inflected in current Republican prison-mania by a rhetoric that shifts from the language of amelioration to that of retribution. The racism remains the same.


AW: One of the most disquieting moments in the book is the analysis that shows in what ways the project of the Wall – along with practices surrounding it – represents the sort of occupation that invades the private life of the Other, whereby surveillance is of such nature and intensity that the private life ceases to exist. 


MS: This is one of the great trajectories of the dark reading of modernity, the Foucaldian argument. It is easy to see how the Wall – walls of various kinds – functions within it. Foucault's central point is not simply about the Panopticon as an efficient means of universal surveillance; that is only a metaphor. The argument is that those who are subject to surveillance internalize the idea of their inescapable surveillability; they behave as if surveilled even in the absence of the actual presence of their jailer's gaze. That is the true genius of Panopticon: it works even when the guard is not there. It is a an instrument that produces in us a state of consciousness such that makes us willing to surrender those rights that power prefers us not to have. And this is our peril: that we are complicit in turning architecture into a medium of enslavement. Something, I think, to resist.


Aleksandra Wagner is a sociologist, teaching at The New School, and a psychoanalyst living and working in New York.


[![photo by Dafna Kaplan](media/photo_by_Dafna_Kaplan.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/wall2.jpg "photo by Dafna Kaplan")[![photo by Dafna Kaplan](media/photo_by_Dafna_Kaplan.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/wall3.jpg "photo by Dafna Kaplan")[![photo by Dafna Kaplan](media/photo_by_Dafna_Kaplan.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/wall4.jpg "photo by Dafna Kaplan")



 ## Comments 
1. Godofredo
12.4.07 / 10am


In relation to the book itself, I find your situationist proposal very exciting (though I tend to agree with Sorkin when he defends that the solution of this conflict cannot expect to be based on groundbreaking political transformations).  

Even so, I find your proposal important because it proposes to ignore the causes of the conflict and reduce them to a specific political reading. In such a way it produces a strong criticism of the mechanisms of power (state and religious) behind the conflict and its imposition on both People's lives. I believe that if this criticism gets into people's minds (both Israelis and Palestinians) then it has a chance of producing something.  

But I'm also afraid that such proposal risks becoming a sterile intellectual exercise when removed from the Israeli-Palestinian context.  

Could you tell me if this book was published in Palestine or Israel? Who were you trying to address, and who do you think you've addressed?
3. [lebbeuswoods](http://www.lebbeuswoods.net)
12.5.07 / 6pm


Godofredo: I agree that my proposal for The Wall Game is very specific to the Israeli-Palestinian situation. That was my intention. The idea of ‘play' is, as Huisinga says, a valuable mode of exchange. I'm surprised no one has taken it up before, in whatever form.


My project is just one amomg many proposals in Michael Sorkin's book, which I consider an important initiative that is somehow trans-political. I don't know whether it's been published in Arabic and Hebrew—check the publishet's website. But it should be.


I don't trust politicians, of whatever nationality or persuasion. The latest iniative by them, in Annapolis, is mostly rhetoric. We can hope, but……
5. [lebbeuswoods](http://www.lebbeuswoods.net)
12.5.07 / 6pm


At the moment, I am at the University of Cambridge, at the conference Power and Space, organized by a group of perceptive and active younger people in architecture and media. This terrain is where I place my best hope.
