
---
title: NOBLESSE OBLIGE
date: 2007-11-06 00:00:00
---

# NOBLESSE OBLIGE


[This is the first in a series of posts on the relationships between aesthetics and ethics in architecture.]


[![](media/seagram-1.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/seagram-1.jpg)


The interplay between the ethical aspects of exclusivity and those of ubiquity are operative in a clear way in the case of significant public buildings. Considering, for example, the Seagram Building in New York, the wealth and power of the client are immediately apparent, amply demonstrated by the haughty aloofness of the building's sober and restrained design, and at the same time the sumptuous costliness of its materials and details. The large open space in front of the building seems almost extravagant, considering the staggering prices of land in midtown Manhattan, and contrasts sharply with its more egregiously commercial neighbors, who have crammed the maximum square-footage of leasable space onto their sites. To the average passerby, the building and its siting have the aesthetics of a civic monument, an architecture that goes far beyond advertising its client, and becomes a kind of gift to the city, a form of noblesse oblige—the obligation the rich and powerful have to the society that made them so—that confirms their superior station. The Seagram Company assumes the aesthetic raiments of government, bestowing on the public space of the street an imposing demonstration of social hierarchy and the ethical relationships of New York's social classes. The rich give, the poorer receive. The rich are generous—they bestow on the teeming masses beauty, and space for gathering and enjoyment, and ask, in return, what the givers of all gifts ask, appreciation and a kind of fealty that amounts to a confirmation of the social arrangement the gift expresses. “I would rather be respected than loved,” the old saying goes. That is the Seagram Building, in short. Respect is the essence of an ethics based on power and its separateness from more common, and ambiguous, everyday emotions. This is what the aesthetics of its architecture has achieved, in the name of a company that makes and sells everyday liquors and wines and stands for no lofty ideals of public life.


The Seagram building, it is true, is an exception in the realm of commercial office buildings, but only in the extremity of its aesthetical qualities and, accordingly, its ethical implications. Today, all such buildings offer various forms of public plazas and usually street-level spaces for shopping, entertainment, eating… Part of Seagram's uniqueness is in the austerity of its offerings, its lack of conscious appeal to popular tastes, which characterize most of its building type today. No benches, no quaint lamps, no signs, no color, just hard-edged geometry in unapologetically, elegantly ‘timeless' materials. The public—the masses—have the rare opportunity to enter the living space of an intellectual and social elite, and to feel the distance, the gulf, really, between. It is not, as it turns out, an alienating experience, but a rather popular one. The majority of people, or so it would seem, enjoy knowing—viscerally feeling—the presence of a higher, to them unattainable but not wholly inaccessible, stratum in the life of the city and its society.


LW


[![](media/seagram-plan1.jpg)](https://lebbeuswoods.files.wordpress.com/2007/11/seagram-plan1.jpg)


 



 ## Comments 
1. AndrewC
11.8.07 / 9pm


I am reminded of Fosters HSBC in Hong Kong, a building that I visited ten years ago as a student more out of a sense of obligation than any interest in the project per se. It was a Sunday and the building was closed. The space below had been taken over by thousands of Filipino domestic staff, on their day off, carpets were spread out and families were picnicking and chatting loudly amongst themselves. Above them the glass belly of the building loomed, one could just glimpse the dim innards running upward out of sight. At dusk the lights came on inside and the contrast between the empty, pristine interior and the gregarious, colourful humanity illuminated below was even starker. The masses below are at once drawn close and distanced. However this contrast is expressed more directly than the subtlety with which Seagram does.  

There is a quote, I don't know who by, which states that the difference between manners and etiquette is that manners exist to make others feel comfortable, while etiquette exists to keep them aware of their place. Perhaps buildings such as Seagram possess could be said to possess a highly refined etiquette.  

As an aside what is intriguing about buildings such as Seagram is that (to me at any rate) the refinement of their expression communicates in pure form an almost idealised design process, one from which we are forever closed, which in itself is simultaneous distancing and enticing. I could say the same about some of Lewerentz buildings. It is not any aesthetic sense these works share, but rather an austerity born out of an architect grappling with their art, and finding the means to express it as succinctly as possible.
