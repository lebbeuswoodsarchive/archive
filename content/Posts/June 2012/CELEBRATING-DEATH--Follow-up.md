
---
title: CELEBRATING DEATH  Follow-up
date: 2012-06-19 00:00:00
---

# CELEBRATING DEATH: Follow-up


[![](media/9-11-memorial-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/9-11-memorial-1.jpg)


The following are articles published in the New York Times about the ongoing debates concerned with the most appropriate ways to commemorate the victims of the terrorist attacks of September 11, 2001:


****June 6****


Laying Unidentified Remains to Rest


By PATRICIA COHEN


***Determining a Final Resting Place for Unidentified Remains***


*There have been anguished complaints by several families over the decision to eventually place the unidentified remains of people killed at the World Trade Center in an area, closed off to the public, in the National September 11 Memorial Museum complex. Seventeen family members who lost loved ones on 9/11 have sued the city as part of an effort to drive to revisit the decision. They argue that putting the remains in the underground museum is a disgrace; they prefer that the remains be placed in a spot outside the museum, in a manner similar to the Tomb of the Unknowns at Arlington National Cemetery. Museum officials as well as the families of other victims counter that putting the unidentified remains at bedrock at the site fulfills an earlier promise made to organizations representing a majority of families. They also note that the repository will be controlled by the city's medical examiner and not the museum, and that only the families will have access to it.*


*How should the decision about the final resting place be made, given deeply held but conflicting viewpoints?****Chip Colwell-Chanthaphonh***, *curator of anthropology at the Denver Museum of Nature & Science and an expert on the repatriation of Native American skeletal remains, has consulted with some of the 9/11 families who oppose the museum's plan. He started off a discussion conducted by e-mail.*


**Mr. Colwell-Chanthaphonh**: The decision should be made through consultation with all of the 2,753 families whose loved ones perished on 9/11 at the World Trade Center. The tragic dilemmas raised by the 9/11 terrorist attack parallel the museum world's struggle to decide the fate of Native American skeletal remains. Different cultures and religions around the world have long maintained that the tangible vestiges of a human life have a distinctive power. For centuries, Western common law has affirmed that human skeletons are not “property” that can be taken without consent. In 1990, that principle was enshrined in the Native Americans Graves Protection and Repatriation Act.


Over the last two decades, we have learned that with human remains, the most important stakeholders are the descendants and the best way to resolve conflict is through open and respectful dialogue. These lessons would recommend that the 9/11 museum start consultation by writing a letter to all of the victims' families on this issue, holding a series of forums exclusively for them and inviting them to take a principal role in the decision.


This work will be hard but imperative. Ideally, a consensus among the families can be reached. In its absence, the 9/11 museum could either devise a means to determine the majority of families' viewpoint (majority rules) or offer a new solution that strives to address the widest array of concerns (multiplicity rules). But more than offering a quick fix, the mere process of dialogue will demonstrate respect and an acknowledgment that curators do not have exclusive power over the dead. The families should be given the chance to give consent — to exercise their rights as kin.


***Thomas Lynch****, a funeral director in Milford, Mich., and the author of several essay and poetry collections, including “The Undertaking: Life Studies From the Dismal Trade,” underscored the difference between the sacred space of a cemetery and a museum:*


**Mr. Lynch**: Ours is the species that deals with death by dealing with our dead. The obligation to consign them to a final resting place of our choosing — the grave, the tomb, the fire, air or sea — is a signature of our humanity. And among the ongoing grievances the families of the unidentified dead of 9/11 share is that they never got to enact these deeply human duties for their deceased family members. The impulse to regard the victims of 9/11 as somehow belonging to all of us is an honorable and natural one. They were our own. But the dead of that horrific crime belong first to their families, who have, after all, been brutalized not only by the murder of their family member but by the act that commingled their death with the disappearance of their physical being. The remains of the dead, however indistinguishable from fellow victims, nonetheless occupy sacred space in the minds and hearts and memories of their people, who want and need some sacred space in which to place whatever remains.


While a museum is an important civic space, it is not a cemetery. It is not sacred, set apart. It may be educational and instructive, illuminating and entertaining — all worthy and beneficial outcomes. It may, as its etymology suggests, become a place of meditation and reflection. But it is not a cemetery, nor does declaring it a “memorial museum” make it so.


Once, on the upper floor of the British Museum, I came upon the exhibition of Egyptian antiquities which included several mummies of various ages. At some level I felt complicit, by my interest, in a desecration: that these were fellows of my own species being disallowed, for the sake of worthwhile archiving of human history, the sacred rest to which the living endeavor to consign their dead. Placing the dead in a museum may well be such a well-intentioned but ultimately mistaken decision — the right thing done in the wrong place.


Whether the space “adjacent to and accessible through” the 9/11 museum does not constitute a space that is “part of” or “in” the museum is a matter on which some families understandably disagree.


While I agree entirely with the decision to include family members of all the 9/11 victims in a dialogue about the proper disposition of their dead — those that have been recovered and identified and those that will never be — where the dead cannot be specifically identified, the will of the majority of families should prevail.


***Charles G. Wolf,*** *whose****wife, Katherine, was killed at the World Trade Center, was a member of the Lower Manhattan Development Corporation's Families Advisory Council*** *and participated in the conversation series sponsored by the Sept. 11 museum during the planning stages. He argues that the kind of full-fledged discussion that Mr. Colwell-Chanthaphonh and Mr. Lynch propose has already occurred.*


**Mr. Wolf:** In making a decision about the unidentified remains from the World Trade Center wreckage, the first thing one must do is acknowledge the very definition of what we're dealing with: unidentified remains. No one but the Creator knows whose remains are among these remains and whose is not. Therefore, no one family or group of families can lay claim to saying, “my son,” “my daughter,” “my husband,” etc., is among the remains, and no one can have exclusive say of what should happen with them.


This is why these remains are still in the custody of New York City's chief medical examiner.


Since no one can say their loved one is or is not among the remains, one of two ways to decide disposition of the remains can happen. It becomes either an official decision of the chief medical examiner, or the World Trade Center 9/11 families as a group decide.


This same issue for the Pentagon families (who lost 125 people) and for the Flight 93 families (who lost 40) is an easier process, in relative terms, compared to the 2,753 people (excluding hijackers) killed in the World Trade Center and the two planes that crashed into the towers. How do you work with all those people? Do you do it in a democratic fashion? If so, whom do you talk with, only the next of kin? To a married person, next of kin is their spouse, but what about the blood relatives? Marriage is a legal relationship. How can you not include blood relations like the parents, grandparents or siblings in the conversation? This becomes a gargantuan task, if you even can get people to deal with such a touchy subject as this.


An alternative way to do this is in a republican manner, where a much smaller number of representatives of the W.T.C. families can discuss this and decide what to do — just like Congress, our state legislatures and city councils do.


By the way, this has already happened.


In the first weeks and months after Sept, 11, several 9/11 family members started advocacy groups. Eventually, the leaders of these groups, and other 9/11 family activists, were invited to be part of the Lower Manhattan Development Corporation's (L.M.D.C.) Families Advisory Council (F.A.C.).


L.M.D.C. was created after Sept. 11, 2001, by then-Governor Pataki and then-Mayor Giuliani to help plan and coordinate the rebuilding and revitalization of Lower Manhattan. The F.A.C. provided a legitimate and formal way for 9/11 family members' wishes, viewpoints and suggestions about 9/11 matters that pertained to them to be provided to city and state officials. There were many meetings with lengthy discussions and formal votes. Minutes of the proceedings were recorded.


Members of the F.A.C. included heads of all the family groups. It was through the Families Advisory Council that the decision regarding the placement of the unidentified remains at bedrock came about.


*Mr. Colwell-Chanthaphonh remained unconvinced that all the families were give sufficient opportunity to express their views.*


***Mr. Colwell-Chanthaphonh:*** Mr. Wolf is skeptical of consulting so closely with all of the families in New York, and instead suggests that a “republican” approach of representative government is the best solution. The major problem with Mr. Wolf's proposal is that it simply has not happened.


Again, the museum world's work with Native American communities may be instructive. Many Native American remains in museums have large numbers of potential descendants. Consequently, we consult not only with kin but also tribal governments who represent their citizenry. These tribal governments have constitutions and constituents; elected tribal officials have the authority of their people and are accountable to them.


There is no equivalent political body for the 9/11 families of New York. Instead, there is only an eclectic assortment of small groups that have been selectively involved in discussions. The concerned families may not disagree with Mr. Wolf's republican proposal: rather, they insist that they have not been represented in these decisions. Without fair representation, they sensibly want every next of kin to be included.


In fact, when asked directly, many families seem to reject the current plans. Several family advocacy groups have explicitly called for new and inclusive consultations, which the 9/11 museum has rejected. But until the 9/11 museum solicits feedback from all the families, this is the best information we have, which shows a wide range of viewpoints.


Either the 9/11 museum must devise a true democratic representative method or it must invite all of the next of kin to participate in the decision-making process about the unidentified remains. Those are the museum's choices, but it has refused to do either.


***Mr. Wolf offered a detailed reconstruction of how the decision to place the unidentified remains at bedrock was reached.***


**Mr. Wolf**: The September 11 Memorial Museum had nothing to do with the decision. The proposal was made by the Families Advisory Council and it preceded the existence of the National September 11 Memorial and Museum at the World Trade Center Foundation, which began fund-raising in the spring of 2005. Here is how it happened.


Somewhere around late 2002, before we knew what the memorial would be, or that there would even be a museum, the members of the Families Advisory Council began discussing the unidentified remains. These remains were (and still are today) residing in a makeshift temporary setup in back of the medical examiner's office near the East River. The overwhelming feeling among the council members was that these remains should be “repatriated” back at bedrock at ground zero from whence they came. Bedrock was “sacred ground” to the 9/11 families, and the F.A.C. strongly felt this was the right place for the unidentified remains.


For the purposes of lobbying on very important matters, several family groups joined together under the banner of the Coalition of 9/11 Families, including those groups headed by Jennie Farrell, Mary Fetchet, Marian Fontana, Anthony Gardner, Monica Iken, and Sally Regenhard.


The coalition, as they were called, supplemented the F.A.C. process and took the issue of the placement of the unidentified remains directly to the 9/11 families themselves. They mailed thousands of cards to family members, asking their feeling about placing the remains back at bedrock. The coalition reported that the families were overwhelmingly in favor of this. The coalition then proceeded to send a letter to Governor Pataki, with the power of their survey behind it, formally requesting his support for the remains be returned to bedrock. Every coalition member signed this letter, including the one coalition and F.A.C. member who now leads the fight against the remains being placed at bedrock.


Meanwhile, the Families Advisory Council asked the chief medical examiner if he would allow the return of the unidentified remains to ground zero. He said, yes, but only if his staff could continue to have access to the remains, so when new technology was available, they could go back and attempt to identify more remains.


With the blessings of Governor Pataki and the chief medical examiner, the placement of the remains at bedrock at ground zero became part of the formal planning document that the Lower Manhattan Development Corporation drew up and handed to the foundation for implementation and that subsequently was formally adopted by them.


That direction was an inviolable part of the planning of the memorial and museum. Even during the redesign process headed by developer and construction expert Frank Sciame in the spring of 2006, the F.A.C. strongly and unanimously stated that whatever else he did, the space for the unidentified remains, midway between the footprints of the twin towers, had to remain as part of the plan.


Sciame honored this request, and the designers found a way to place the repository adjacent to and accessible through the 9/11 Museum, but not in the museum.


Of all the topics the F.A.C. dealt with, this was one of, if not the most important topic. With the wishes of the 9/11 families known through the coalition's survey, the F.A.C. knew it had made the right decision on behalf of all the families.


***Mr. Colwell-Chanthaphonh:*** As Mr. Wolf points out, these decisions unfolded before a museum was even planned for the World Trade Center site. Not until October 2006 did the 9/11 museum — then under the banner of the World Trade Center Memorial Foundation — release its request for qualifications for exhibit designs that explicitly included the human remains repository as a “programmatic element” for the visitor experience.


The families I have spoken with who knew about these early discussions have reasonably explained that in the early 2000s they assumed ground zero would become a civic monument, like a “tomb of the unknown.” Things change. Fair enough. But there is a stark difference between placing unclaimed human remains in a public memorial versus a private museum.


Given that the site's plans have been evolving, the dialogue about the human remains should also be ongoing. In the museum world, one of the principles of consultation is that it must be continuing, because consultations are about positive long-term relationships. With such a goal, one would think the 9/11 museum would welcome the opportunity to speak directly with all the families — one of the key stakeholders in its institutional mandate — particularly when they have such a direct stake on this issue.


More than 40 percent of the families did not receive any fragment of their lost loved ones who died at the World Trade Center. This decision about the human remains is thus radically different from the museum's judgments about the size of exhibit photos or whether to mention the Patriot Act. For more than 1,000 New York families, the 9/11 museum may be the only cemetery they will have.


I have heard different versions of the events that Mr. Wolf describes, but I wonder whether it really matters since the decisions made by those without authentic authority should not be binding. What democratic process elected these leaders? What recourse was there for those next of kin who disagreed? Did all 2,753 families even consent to a “republican” process? Many seemingly did not. As one New York family member recently told me: “When it comes to my brother's remains, I wish to have a voice equal to that of all the other people involved. No one can, or should, represent my wishes for something so personal.”


I hope that we do not lose sight of the rights of these individuals as we seek solace for our collective grief.


***Patrick White****, president of Families of Flight 93, wrote about how the families of the 40 passengers on the plane that crashed in Shanksville, Pa. on Sept. 11 handled the unidentified human remains retrieved from the site. Mr. White, whose cousin Louis J. Nacke II died on board that day, noted that the Flight 93 families confronted a different situation from those whose relatives died at the World Trade Center or the Pentagon, notably that at least some remains were recovered and matched for every one of the passengers as well as the four terrorists. The relatively small size of the group also meant that all the families could be directly involved in meetings. The crash site is now a cemetery, and off limits to everyone except family members.*


**Mr. White**: Recognizing that the circumstances surrounding the World Trade Center's and Pentagon's respective crashes were distinctly different in many regards, the experience of the Families of Flight 93 may not serve as the “best” or “right” way to reach the decision about a final resting place for unidentified remains elsewhere. Similarly, at the beginning of the discussion about final disposition of the Flight 93 remains, the respective family members, including the next of kin, certainly had a wide variety of differing views. However, we quickly recognized that because the remains were “unidentified,” no one voice of a particular family member, or even next of kin for that matter, was more meaningful or to be given greater weight than that of any other in the decision-making process.


And because no one could claim that their loved ones remains either were or were not part of those still”unidentified (including those of the terrorists) remains, there was an inherent equality of views between the participants that arose early on that forged a collegial and respectful dialogue. That in turn allowed these most sensitive and challenging issues to be productively discussed. Although many issues arose during the dialogue of “where, by whom, when and how” the unidentified remains would be finally be treated, by including all family members who desired to participate, while focusing on and being more deferential to the concerns and opinions of identified next of kin, the group meetings and teleconferences proceeded in a respectful and effective process leading to a consensus of next of kin for the final disposition of remains.


The Flight 93 National Memorial has also benefited from the group's positive and productive partnership with the National Park Service, its federal advisory commission, former task force, the National Park Foundation and, most recently, the Friends of the Flight 93 National Memorial, all of whom have deferred to the Families' position on questions such as this. Most significantly, close coordination and involvement with the county coroner assured that all issues of chain of custody and respectful care of the unidentified remains were properly addressed. In the case of the Flight 93 passengers and crew members, their next of kin and other interested relatives participated in a ceremony where on Sept. 12, 2011, three caskets of unidentified remains were reinterred in close proximity to the primary crash impact site. Responsibility for perpetually preserving the integrity of those remains is now in the hands of the National Park Service as part of the Flight 93 National Memorial's Sacred Ground.


Although an arduous process, requiring great sensitivity and respect, all of those who had a potential interest were contacted and solicited to participate, and they did so according to their individual desires. Deeply held viewpoints and differing voices were allowed and encouraged to be heard such that when they finally melded, their decision lead to an outcome and ceremony acceptable to all that honored those we seek to remember as our heroes for their selfless sacrifice and courage. In how we acted, I believe we've further legitimized their legacy of hope.


**June 5**


For a Sept. 11 Museum, How Realistic Is Too Realistic?


By PATRICIA COHEN


***How does a museum transmit the horror of the Sept. 11th attacks without traumatizing its audience? Where do you draw the line?***


*The National September 11 Memorial Museum's staff and advisers have painstakingly combed through the mammoth collection of artifacts, audio recordings, videos and photographs in choosing what to display. Many items capture all too clearly the gruesome horror of that day and museum officials have been constantly forced to decide what is appropriate material for a museum exhibition and what might be too upsetting for visitors to see. We asked a group of museum professionals and trauma experts to discuss, by e-mail, how to get the message and history across accurately without being gratuitously shocking.* ***Kari F. Watkins****, director of theOklahoma City National Memorial & Museum, started off the discussion. She has dealt with the same issues in her own institution, which commemorates the bombing of a federal office building by Timothy McVeigh, a white supremacist, that killed 168 children and adults on April 19, 1995. She has also frequently consulted with the staff of the Sept. 11 museum.*


**Ms. Watkins:**  It is important to show the trauma of that day without “prettying” it up too much.  People have to see the horror to believe it because what happened here and on 9/11 is too unimaginable for most people to believe. When you see a person walking down the street holding a torn shirt as a bandage, it becomes all too real. Plus, that is what happened and how people responded to these acts of terrorism. You have to also keep in mind that there is horror like this on daily television, Web sites and video games.


***Edward Linenthal******, author of “The Unfinished Bombing: Oklahoma City in the American Memory,[”](http://www2.facinghistory.org/campus/memorials.nsf/0/BFC807D12960743885256ED8006986C2) and a member of*** *the Flight 93 Federal Commission in Pennsylvania* ***,*** *also pointed to the difficulty of communicating an event's destructive power when popular culture is so saturated with violence. Mr. Linenthal also consulted with the Sept. 11 museum.*


**Mr. Linenthal**: I think we use the term trauma much too easily, and I am skeptical that in a culture awash in horrific images people are very easily shocked. More's the pity, perhaps, but it is an enduring —and ultimately unanswerable question —whether or not traffic in horrific images makes it harder for us to empathize with particular people in particular situations, or easier, since in some sense such images offer the opportunity for a public to participate, to “bump up” against the horror of an event.


The Oklahoma City National Memorial & Museum offers a compelling immersion in the events of April 19, 1995, and some of its legacies, most powerfully, in my view, through the presentation of oral histories. I know, however, that for me, having come to know many of the people impacted by the bombing and involved in the creation of the memorial, the horrific power of the event meant more to me because of those personal connections.


***Grady Bray,*** *a disaster psychologist who consulted with the Fire Department in the aftermath of 9/11 and, later with the Sept. 11 museum, said it was better to err on the side of showing too little rather than too much.*


**Dr. Bray**: For me, the first issue influencing the selection of images and sounds to be used in a public forum is safety. During their reporting of atrocities in the Congo, for example, CNN repeated numerous times in a 24-hour period graphic images of mutilation, dismemberment and beheading. There was a significant increase in reports from pediatricians concerning children who had witnessed these stories and developed nightmares, fearfulness and anxiety-related disorders. Primary victims are those directly — physically — present and affected by the events; secondary victims include responders and those who witnessed the events; and tertiary victims are those affected by subsequent information concerning these events. This last group would be typical for those visiting a museum or memorial. If one follows the old axiom from medicine, “Above all else, do no harm,” then imagery will be selected judiciously. It is possible to convey the horror of the event without the most gruesome and disturbing images and videos. Also, workers and volunteers at a museum or memorial will be repeatedly exposed to the presentations and be vulnerable to emotional trauma.


The second issue deals with the selection of gratuitous violence for shock value or because those who were powerfully affected by the event want others to experience the horror which they endured. Sometimes these victims have described an initial decrease in symptoms when they see the reactions of others to the imagery, but these improvements are short-lived. In reality, repeated exposure to sensory input of traumatic events does not decrease problems for these victims but, paradoxically, it seems to strengthen the destructive power of the event.


***Sandro Galea******, chairman of the department of epidemiology at the Columbia University Mailman School of Public Health, is the author of a study that looked at symptoms of post-traumatic stress disorder among 3,271 civilian survivors two to three years after the 9/11 attacks. Like Mr. Linenthal, he was much more cautious about predicting what effect viewing violent images and hearing anguished audios will have.***


**Dr. Galea**: There is little doubt that images and reminders play an important role in helping us remember, and cope with, tragic experiences. One always worries that watching images or other reminders of horrific events may contribute to poor mental health or may otherwise substantially disturb those who watch these images.


The science on this question remains unclear.


Some studies have shown that people who have already experienced an event may, when seeing reminders, develop psychopathology triggered by memories of the event.  Other work has shown that the greater the “dose” of exposure (for example, number of hours of television watching commemorating 9/11) the greater the risk of post-traumatic stress disorder in this group.


There is, however, little evidence that people who are not at already elevated risk are harmed by images of a horrific event.


As with many other things, the science here needs to be balanced with the larger social good that is intended. I think some warning that witnessing extremely graphic images, which remind us of a real tragedy, may be harmful to some may be a reasonable caution.


***


***Ms. Watkins*** ***of the Oklahoma City museum emphasized that while it is important for visitors to understand the impact of real violence, ultimately the museum should tell a story that leaves people feeling optimistic about the future. Mr. Linenthal, Dr. Galea and Mr. Bray cautioned that it can be impossible to predict what reactions an exhibition might provoke.***


**Ms. Watkins:** I think the bigger challenge is to tell a complete story, and tell it sincerely, without manipulation or sensationalism, and make people of all ages understand the horror and why teaching hope is so important. When guests walk through the door, some know what happened, but we are also educating an entire generation who didn't live this story, so it is “new” to them. We must make sure to teach visitors to remember those who were killed, those who survived and those who were changed forever. May all who leave this Oklahoma City Memorial & Museum know the impact of violence. It is our job to ensure each visitor has the opportunity to experience comfort, strength, peace, hope and serenity.


**Mr. Linenthal:** With the Oklahoma City National Memorial, there was a strong conviction that the entire project should move beyond just a focus on the violence, to educational work, and, as Kari Watkins notes, the “opportunity” for visitors to experience emotions, convictions other than horror and sadness. No historic site or memorial can program what a visitor may or may not experience or take away from a visit, hence the word opportunity is significant in Kari Watkins's observation. There is, I think, an often unspoken hope that the cautionary tales, the purported “lessons” of an exhibition will be as transformative in a humane way as the event represented was transformative in a destructive way. Whether this is possible or not is, I think, open to question. No one can guarantee what a visitor takes away from such a visit. And the formulaic response of, for example, “Never again,” should in reality be, “Will it ever stop?”


During the planning of the permanent exhibition of the United States Holocaust Memorial Museum, there was concern that the allure of Nazi martial music, Hitler's voice, the vibrant colors of a flag had the potential to be, well, alluring, as was the goal of Nazi spectacle. But in a storytelling museum — to use the characterization of the late Shaike Weinberg, the former director of the Holocaust museum — the rise of National Socialism is a crucial part of the story. There are ways to neutralize somewhat the allure of spectacle — a kind of screen in front of a large Nazi flag, for example — but the truth of it is that each visitor will inhabit the site in their own distinctive manner.


**Dr. Galea:** It seems to me a large, but laudable goal that the museum aspires to provide comfort, strength, peace, hope and serenity for all visitors. I also think it might be unrealistic. Some will be unsettled by visiting a memorial to tragedy, and that is perhaps how it should be. It is the museum's role to help us mark and commemorate events and accept that such memorials may provoke a range of human reactions. It is also the role of the museum to ensure that, guided by science, insofar as possible, those who are particularly at high risk of being harmed by reminders of tragedy are warned and given opportunity for comfort and mitigation if needed.


**Dr. Bray:** For most who attend a memorial and museum such as 9/11, it is more common to experience:


1. Sadness, a function of grief for those who died and their families;


2. Increased anxiety due to a greater awareness of the human potential for violence;


3. Resiliency focus, which is fostered by the stories of self sacrifice and bravery; and


4. Disquiet based on their personal confrontation with mortality.


The museum's function is to provide information and experience.  Every attempt should be made to be factual, as opposed to sensational, but each visitor personalizes their experience far beyond what the organizers can control.


***


*Mr. Linenthal pointed out that withholding disturbing images can also have a serious downside.*


**Mr. Linenthal**: There are some images, aren't there, that are seemingly too “indigestible” for us to make part of an enduring visual narrative. For example, how quickly did the images of those people forced to leap from the World Trade Center towers on the morning of 9/11 disappear? Or how rarely do we see a fascinating photograph by Thomas Hoepker on that morning of people in Brooklyn seemingly indifferent to the catastrophe unfolding in front of them? While this is not a horrific image, it is one that does not fit easily into any narrative of that morning.


I think as well of photographs not taken, sparing us from thinking too much and too long on the enduring cost of violence. Invisible to most of us, for example, are veterans' hospitals, and the human cost that the conflict has wreaked on the patients within. But we are spared such scenes, which would make the all too-easy rhetoric of martial enthusiasm more difficult to engender and to absorb.


***


*Mr. Linenthal emphasized that there is not necessarily a right or wrong decision in some instances. To illustrate, he explained how the United States Holocaust museum reasoned through a couple of difficult questions.*


**Mr. Linenthal**: For survivors, their extended families and many others, the U.S. Holocaust museum was a sacred place, a place for commemoration, a site for the power of the voice of the witness to be paramount. This voice often rests uneasily with the more detached voice of the historian or curator, and at sites that combine memorial and museum, this can lead to strong disagreement. During the planning of the permanent exhibition at the museum, for example, there was an ongoing debate over whether or not to display in the most intense part of the exhibition actual women's hair that had displayed in the Auschwitz museum. Women's hair was used by Germans to make socks for the Wehrmacht, insulation in submarines, and was much more personal, more intimate, than other artifacts. But for more than a year there were those on the museum's content committee who opposed its use, and finally a female survivor said that the hair could have come from her mother and should not be displayed. While historians and museum professionals had made good arguments about why it was appropriate to display the hair, in this case the late Shaike Weinberg, the museum's director, honored the commemorative sensibilities of some survivors and chose not to display the hair.


In another situation, however, Weinberg chose to act on the need for the exhibition to offer graphic visual evidence of killing processes, even though it was uncomfortable for some survivors. This issue arose because out of respect for commemorative sensibilities: early drafts of the permanent exhibition were careful not to “defile” the sacred environs of the museum with the horrific images of perpetrators at work. But this meant that the museum's narrative would have been one in which people were leading themselves to their death, with perpetrators invisible. Weinberg and many others recognized that fidelity to a full historical narrative had to include the presence of perpetrators, uncomfortable as it may be. So, in this case, the historical voice trumped commemorative sensibilities.


I do not think about these revealing moments as ending with “right” or “wrong” decisions. Rather, they illustrate how mindfully those tasked with the creation of such exhibitions must weigh often conflicting convictions and emotions about highly charged events, often under intense public scrutiny.


**June 4**


Choosing From the Many Lessons of Sept. 11


By PATRICIA COHEN


Fred R. Conrad/The New York Times


In eight years of planning a museum at the National September 11 Memorial, every step has been muddied by contention.


*One of the difficult questions that the National September 11 Memorial Museum has grappled with is how to explain why a group of radical Islamists wanted to bring down the World Trade Center towers and attack the Pentagon. We asked a group of historians and educators to talk about what they thought was essential for people to understand about the history leading up to 9/11.*


***David Blight******, a professor of American history at Yale University and an adviser to the museum****, started off the discussion by urging people to take the long view and place the attacks in a larger tapestry of human experience.*


***What do you think is essential for people to understand about the history leading up to Sept. 11?***


**Mr. Blight:** To concentrate only on the horror and drama of that day and the events in its immediate aftermath, while utterly compelling, does not necessarily leave us understanding anything. While mourning, people are not easily capable of seeking historical understanding, of taking a long view; 9/11 seemed so unparalleled, unique, without precedent. But is it? The methods and the overwhelming and immediate results were new. But the human impulses, as well as our own human reactions, were and are hardly new. In the immediate aftermath of 9/11, we desperately searched for historical analogies through which to find understanding. Was this a Pearl Harbor? An Antietam or a Fort Sumter? Was this only the beginning of an enveloping conspiracy against Western values and societies? Was this warfare of a new kind for which we had no analogies?


*All the panelists agreed about the importance of understanding the historical context. But the other commentators —* ***Anthony Gardner*****,** *whose brother died in the attacks and is on the museum's advisory committee;* ***Bill******Braniff,*** e*xecutive director of the National Consortium for the Study of Terrorism and Responses to Terrorism [(START)](http://www.start.umd.edu/start/) based at the University of Maryland; and **Wilfred M. McClay*** ***,******a professor of humanities and history at the University of Tennessee****, Chattanooga — took issue with Mr. Blight's approach, particularly his insistence on a centuries-long timeline and his reluctance to single out the Sept. 11 attacks as special.*


**Mr. Braniff** ***:*** I agree that while terrorism is a painfully human phenomenon that has analogous moments in the history of political violence, the tragic events of 9/11 were unique. They were the product of a specific and violent modern history and a group of violent individuals who rejected it, lashing out in an attempt to reorganize society according to their interpretation of a pre-modern Islamic political doctrine the world had largely left behind.


***Mr. Braniff explained the particular nature and aims of Al Qaeda.***


**Mr. Braniff**: Al Qaeda was born during a decade-long war in the 1980s, constituted largely by aggrieved citizens of failed post-colonial nations and nurtured by a confluence of specific geopolitical and ideological factors. From the resulting safe havens, Al Qaeda has endeavored to enable the violence of others and orient that violence against the West, attempting to bleed it in a war of economic, military and political attrition.  According to this logic, attrition would lead to the severing of ties between the West and what they view as the illegitimate regimes of the Muslim world, allowing just governments informed directly by Islam to ascend in their place. Al Qaeda is a rational, zealous and murderous organization that has influenced other militant groups and individuals.  And because of its ability to incite other locally or regionally oriented organizations and individuals through training, funding, networking and propaganda, we see Al Qaeda as a central node is a larger system of violence.


When teaching about Al Qaeda and the events of 9/11, I try to force my students to grapple with the multifaceted reality of Al Qaeda's coexistence in a crowded landscape of actors.  Many treatments of the topic look at Al Qaeda as an ideological manifestation exclusively — appearing out of the ether of religious ideas to attack and disappearing once more.  Others treat Al Qaeda as if it exists in a vacuum untouched by the forces affecting the rest of us, or perhaps in a boxing ring going toe-to-toe with only the United States as an opponent. Al Qaeda can only truly be understood in a broader context.


*Mr. Gardner said that in too many accounts of 9/11, the focus on Qaeda members as rational actors has been obscured.*


**Mr. Gardner**:  In most 9/11 narratives, the perpetrators — Al Qaeda operatives, plotters and financiers — are often absent.  Planes crashed into buildings as if  by accident, the tragedy caused by some illusive invisible evil. Following is a good example of this: The 9/11 Commission Report includes a timeline of the hijackings. Several of these hijacking timelines reference the words of the terrorist captured through radio communication, i.e. “We have some planes,” and yet each timeline lacks a few important words. Consider:  “8:46:40, AA 11 crashes into 1 W.T.C. (North Tower).” Some might say this is a nuance, but there is no reference to the men who deliberately crashed those planes. A more historically accurate description might be: 8:46:40, Al Qaeda terrorists crash AA 11 into 1 W.T.C. (North Tower). Indeed, men, fueled by hatred for the Western world, flew those planes with innocent civilians onboard deliberately into buildings with innocent civilians in them. These were educated men who made choices. They were radical Islamists.


***


*The Sept. 11 Memorial Museum begins its recounting of history with the Soviet invasion of Afghanistan in 1979, which incited an armed resistance by Islamic fighters supported by the United States. Mr. Braniff, Mr. McClay and Mr. Gardner* ***agreed that this date is a logical starting point.*** ***Mr. Gardner, who is also the director of a museum, spoke of his experience as executive director of the September 11th Education Trust.***


**Mr. Gardner**: Over the years I have interacted with thousands of high school teachers who are struggling with how to teach 9/11 in the classroom.  They wonder about what to say about the perpetrators.  For the most part, they all report the same thing: the biggest challenge is that students want to know why this happened. Some education programs out there go as far back as the crusades to begin the conversation of religiously motivated conflicts and brutal acts through time. The scope of a timeline affects the depth of context it provides for historic events such as 9/11. A shorter timeline may lead to very different causes and conclusions than a timeline of greater scope.  This raises the interesting question of when does the history of 9/11 begin. In the September 11th Education Program, the timeline begins in December 1979 with Soviet forces invading Afghanistan. Our first mention of Osama bin Laden references how he provided financial and organizational aid to the mujahedin in Afghanistan in 1980.


*He and Mr. McClay followed with varying lists of terrorist threats and attacks that should be included in a historical review, including, for example, the seizure of the American Embassy in Tehran in 1979; the* *1983 bombing of a Marine barracks in Beirut; the 1993 bombing of the World Trade Center; bin Laden's 1998 fatwa instructing all Muslims “to kill the Americans and their allies, civilians and military, in any country where it is possible”; the bombing of the United States Navy destroyer Cole in Aden, Yemen in 2000; and the suicide bombing less than two weeks ago by a Qaeda militant in Yemen's capital. They emphasized how an accurate understanding of this history was essential to current policy-making.*


**Mr. McClay**: The situation in which we find ourselves has developed through the Carter, Reagan, Bush, Clinton, Bush and now Obama administrations. No political party escapes blame for where we are now. And the revolutionary Iranian regime that started it all back in 1979, whose apocalyptic religious vision has been shared to a greater or lesser extent by others in the loose network of Islamist militant groups, now appears on the verge of acquiring nuclear weapons.There seem to me to be some fairly obvious inferences to be drawn from this history.


*Mr. McClay addressed what he viewed as the shortcomings of Mr. Blight's perspective.*


**Mr. McClay**: Mr. Blight's willingness to constantly sift through and reconsider the past from a variety of perspectives, including those antithetical to one's own interests and settled convictions, is admirable. But we academics have a tendency to think that all the world is, or should be, a freewheeling seminar room. And that becomes a problem when one wants to draw fruitfully upon the past to make consequential choices as a citizen or a statesman. The long view can be the wrong view when it blinds one to the demands of what is proximate.


**Mr. Gardner:** I agree with Mr. McClay.  Because the threat of terrorist attack is ongoing, the study of events leading up to and since 9/11 provides context and has the power to save lives.  We must ensure that generations that follow us do not become complacent once again. In the spring of 2001, I had a conversation with my brother Harvey about terrorism, specifically about the 1993 World Trade Center bombing.  That day, we stood at the base of 1 World Trade Center and looked up at the soaring tower, and concluded that the only way terrorists could take it down would be through an air strike. We quickly dismissed this as highly unlikely because no enemy would be able to get jets anywhere near the buildings. After all, our military would intercept the enemy long before they could reach the towers and by extension — us.


What my brother and I both lacked that day was an in-depth understanding of the history leading up to the 1993 bombing and a timeline of terrorist activities and threats to that very moment in time. This lack of knowledge provided a false — albeit convenient — sense of security. We never could have comprehended that less than a year later, Al Qaeda terrorists would use commercial airplanes as weapons, killing 2,977 innocent people (including Harvey), damaging the Pentagon and reducing the W.T.C. to a smoldering pile in their brutal, coordinated attack. As a nation we were stunned by the terrorists' actions on 9/11. No Americans should ever be caught off guard again. I want people to understand the history of 9/11 is still being written, that it was a deliberate coordinated attack  and it is an ongoing narrative that has an impact on all of us.  If the lessons of 9/11 history are heeded, perhaps 50 years from now, when two brothers stand below (the new) 1 W.T.C. and assess their safety, they are informed by the in-depth knowledge 9/11 history provides.


**Mr. Blight**: I take the point that urging too much of a “long view” of the causes and immediate consequences of an event like Sept. 11 can — in its own urgent crisis — stifle necessary responses, even impede decisive military action. Yes, it is the job of leadership to provide people with a clear sense of direction. And yes, indeed, 9/11 came from a “specific and violent modern history” that forged Al Qaeda out of radical Islamism. But is 9/11 really “unique” because Al Qaeda is a “rational, zealous, and murderous organization” bent on recruiting other disgruntled, post-colonial militants from failed states to their heinous methods and ideology? This too has happened before.


I am all for understanding and teaching the very specific historical origins of 9/11 in the post-1979 Iranian-hostage world. But history never stops happening, and what the famed French historian Marc Bloch called the “idol of origins” will always, and necessarily, take us further back, back into pasts — even our own — that we often prefer to avoid. Fifty years of U.S. policy toward Israel and the Palestinians are relevant. Why were those U.S. Marines in those barracks in the first place in Beruit? We in the West have a long history in Afghanistan well before Osama bin Laden created bases there. The U.S.-supported Contras in Nicaragua carried out terrorism in our name in the 1980s. And President Reagan called the U.S.-backed Jonas Savimbi — the rebel leader in Angola for so long and one of the worst mass murders of modern history — the “Abraham Lincoln of Angola.” Totalizing forms of war on civilians are not new to the past decades; we too practiced it on a massive scale in order to win World War II, and whether in the tales of Homer or of Shakespeare, the Trojan War was all-out slaughter of civilians in the name of “nation,” or “people,” or “cause.”


In the “Supplement” to his collection of Civil War poems, “Battle Pieces,” published in 1866, Herman Melville left this plea: “Let us pray that the terrible historic tragedy of our time may not have been enacted without instructing our whole beloved country through pity and terror.” Melville looked down deep through time at the human condition. All world-historical events should make us do the same.


**Mr. McClay:** I think it's important for us to stay focused on the particular issue we're trying to address here: What are the essential things that everyone should understand about the history leading to 9/11? Mr. Blight poses profound and thoughtful questions, but I don't think they get to the issue at hand. He seems primarily concerned to stress two things: first, that America is not an entirely innocent nation, and second, that every war brings unspeakable horror in its wake. In short, he wants to stress that America is not an exceptional nation — not in its virtuousness and not in its suffering. There is something to be said for both contentions, particularly the latter, but they do not seem to me to be the most helpful or important points to be made in this context. Hence Mr. Gardner's passionate rebuke seems to me entirely warranted, and is a useful warning about the dangers of attempting an Olympian posture with respect to these still-vivid events.


Mr. Blight asks, “Why were those Marines in Beirut in the first place?” The short answer is that they were there as part of a multinational peacekeeping force, also including French, British and Italian troops, which had been brought in to stabilize the Lebanese government and society after many years of devastating civil war. The peacekeeping force enjoyed some success, but eventually became a target for the various factions in the civil war, and after the calamitous barracks bombings, President Reagan chose to withdraw the Marines and leave Lebanon to its factions. It is a complicated history with complicated lessons, but one extremely relevant point emerges from it all. Osama bin Laden told John Miller of ABC News in a 1998 interview that “the decline of American power and the weakness of the American soldier”/// — whom he dismissed as “paper tigers” — had been “proven in Beirut in 1983, when the Marines fled.” In other words, the Beirut bombing, and the passive American response to it, played a key role in emboldening bin Laden to undertake the audacious 9/11 assault.


And, as much as I admire the beautiful and oft-quoted words of Melville with which Mr. Blight concludes, I do not think they should be read out of context. Earlier in that same “Supplement,” Melville insists that “we should remember that emancipation was accomplished not by deliberate legislation,” and that “only through agonized violence could so mighty a result be effected.” Yes, it would have been far better had a civil war over the issue of slavery never been necessary. It should always be the goal of statesmen to prevent wars or minimize their effects. And yet I doubt that Mr. Blight would contend that the American Civil War should not have been fought, or that it was fought in vain, or even that the ruthless but decisive tactics of General Sherman were unacceptable. For all of its horrors and atrocities, war is sometimes the least unacceptable alternative on offer. That fact, too, is part of the dark moral complexity of the human condition, a complexity from which none of us is exempt.


**Mr. Blight**: Mr. McClay misses the point of my use of Melville, even as he too appreciates complexity. If by “instruction” from “tragedy” we mean that the only lesson to be learned is to know thy enemy and kill him first, we would have a good clarion call for war-fighting. But what will we have learned as a civilization or as human beings? And of course some wars are worth fighting. What Melville meant was that even emancipation, the cause that might redeem the blood, itself came through pity and terror. The best lessons learned from history are not those that confirm what we want to hear, or think we already know; they are those that expand us, deepen our awareness, make us look way back in time to see ourselves anew.


*******


*Mr. Gardner challenged Mr. Blight's assertion that* *“while mourning, people are not easily capable of seeking historical understanding, of taking a long view.”*


**Mr. Gardner**: As Mr. Blight suggests, concentrating on the horror of that day and the events in the immediate aftermath alone does not lead to understanding causes. But there is a supposition in Mr. Blight's statement that grief took away our ability to be historically cognizant.  Ironically, those with the most direct experience of 9/11 (families, survivors and rescue workers) became its first historians, struggling with redevelopment officials to ensure generations that followed would have a tangible connection to 9/11. Despite our best efforts, the National September 11th Memorial at the World Trade Center lacks context. In the years since Sept. 11, 2001, I have been an advocate for the preservation of artifacts (like the structural foundations, or footprints, of the towers at bedrock) and the return to the plaza of the large “Sphere” designed by Fritz Koenig that originally stood between the two towers. I have also pushed for 9/11 education because I want the generations that follow us to learn from this event (including its pre- and post- history). The stories from that day and its aftermath have the power to inspire.


********


***Mr. McClay underscored that a review of the history leading up to 9/11 should include America's intelligence failures.***


**Mr. McClay**: Related to these matters is another essential and practical point that I think can be drawn from the lead-up to 9/11 and about which there should be broad consensus: we need far better and more reliable sources of intelligence than we had then. The 9/11 Commission Report was clear and emphatic in making that point, and the situation has not changed in that regard. Better, more trustworthy, less politicized intelligence could have changed a great deal about the history of the last decade and a half. We will need it even more in the years ahead, to know when to act and when not to act.


*Mr. Braniff took issue with Mr. McClay's disparaging characterization of America's intelligence capabilities.*


***Mr. Braniff****:* The call for better intelligence is understandable but may set unrealistic expectations for the capacity of intelligence to predict the future and may also discount the intelligence successes since 9/11. The decision to fund anti-Soviet militancy in Afghanistan was a well-informed national security decision made through the cold-war lens, just not a prescient one. In terms of post 9/11 intelligence, according to data and analysis through 2010 from the START Consortium, the United States ranked higher on the list of most-frequently attacked nations before 9/11 (No. 10) than after 9/11 (No. 21).


The data also demonstrates a similar decline in the percentage of attacks globally: from 3.03 percent of terrorism incidents globally before 9/11 to 0.57 percent after 9/11. One of the many possible explanations for this decline is that the counterterrorism community disrupted, deterred or interdicted plots more successfully in the post 9/11 world. It is always worthy to strive for better and more forward-looking intelligence, but the efficacy of intelligence is notoriously hard to determine and even the best intelligence cannot obviate all risk.


*He also pointed out that despite the tremendous loss of life on 9/11, the primary victims of Al Qaeda's terrorism have been Muslims, adding that the organization has also failed in achieving its political aims.*


**Mr. Braniff:** What actually has often been overlooked until more recently is Al Qaeda's failure to limit the violence it inspires to the non-Muslim world.  While 9/11 was intended to bait the West and serve as a rallying cry for anti-Western violence, the legacy of 9/11 includes horrific internecine violence within portions of the Muslim world itself. Our data pins only 0.3 percent of the terrorist attacks in the world on Al Qaeda and its associated movement from 1998 to 2008, but these attacks were responsible for 5.4 percent of terrorism fatalities over the decade.


The lethality of Al Qaeda-inspired violence belies the organization's political failures — increasing levels of ideological rejection in opinion polls in the Muslim world and violent rejection by citizens in active jihadist fronts such as Iraq and Somalia. Al Qaeda's violent brand of anti-Western Islamism failed to take root, catalyzing Muslim-on-Muslim violence instead. The Arab Spring and its aftermath illuminates Al Qaeda's political failures further; while Al Qaeda intends to overthrow the nation-state system, the Arab Street sought representative government within the nation-state system. Today, Islamist parties rejected by Al Qaeda for operating within that same nation-state system are capitalizing on the revolutionary moment.


***


*Mr. Blight, who took on the task of starting off the forum, was given a final opportunity to respond to the other participants' comments.*


**Mr. Blight:** I appreciate very much Mr. Braniff's broadening of the contexts in which we should understand Al Qaeda. As the enemy we have been fighting for 10 years, it is crucial to know that it has spawned larger patterns of violence across the world, and that we are not its only targets. I also honor Mr. Gardner's eloquent voice as a family survivor, a witness, and a passionate teacher about 9/11. He may have, indeed, been one among the mourners who could step up and lead efforts for education and awareness of just who attacked us that day and why.


But most people do not have Mr. Gardner's passion, time and devotion, and most prefer historical narratives kept simple, reassuring and triumphal. They want their sense of heritage confirmed, and not disoriented or obliterated, as 9/11 did at first. Merely fulfilling such popular emotions and desires (which I know are not Mr. Gardner's goals) is hardly the job of historians and educators. Mr. McClay's suggestion, therefore, that “the long view can be the wrong view when it blinds one to the demands of the proximate,” while perhaps a good prescription for fashioning a history that serves a current war effort, is hardly the way to educate the public up to larger understandings — especially in museums and classrooms.


Of course, all the world is not our “seminar room.” Would, though, that a little longer view of history may have emerged in the National Security Council's war room when the president made the decision to invade Iraq in 2003. If what we wish to provide is a useful history for current policy makers about to decide matters of war and peace, then by all means offer options, buttressed with a short view. But if our goal as historians and educators is to expand the knowledge and imagination of the public and our students, then let us please offer the long-form essay at least, rather ideological sound bites.



 ## Comments 
1. [Screenplay |](http://pokk.se/?p=1365)
6.25.12 / 8pm


[…] fascinating work and words by others as well. Today there was a post catching my attention about Oyler Wu Collaboration and their project “Screenplay,” which could be experienced at Dwell on Design 2012 at LA Convention Center this weekend. It is a […]
3. [metamechanics](http://metamechanics.wordpress.com)
6.28.12 / 12am


The use of materials is awesome.  

The built looks like a really nice computer generated rendering.  

Its obsessive and detailed, good stuff.
5. michaelotchie
7.4.12 / 6pm


Reblogged this on [Architextile](http://architextile.wordpress.com/2012/07/04/91/) and commented:  

Listening to KCRW's most recent Design & Architecture podcast – <http://www.kcrw.com/etc/programs/de/> –  

I heard about this innovative use of materials and craftsmanship to realise highly complex patterns or forms. Knitting on building scale – why not!
