
---
title: REPORT FROM BEIJING 4  Big and Empty (2)
date: 2012-06-14 00:00:00
---

# REPORT FROM BEIJING 4: Big and Empty (2)


[![](media/ccm-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-1.jpg)


## **Text and Photos by Cheng Feng Lau**


Big and Empty – Part 2 – Capital Museum


If the National Museum is a place to exhibit the entire country's history, should the Capital Museum be considered a museum that showcases the history of Beijing since the time it first became the capital city, or should it be considered a museum that happened to be located in the Capital city of China?


Why am I asking this question?


First of all, one would not build a museum for no reason, right?


There is probably more stuff than exhibition space in a city like Beijing, right?


If the imperial architectures can be transformed into museums, they would probably only be suitable for exhibiting imperial history.


So it seems to make sense to have another separate museum that is dedicated to everything else about Beijing?


If the architectural form of the National Museum is one of Western Neo-classical influence, what should be the style of a Capital Museum?


The architects and the politicians were probably thinking about a “modern-looking architecture that incorporates certain Chinese character, done with modern materials and construction technology, capable of showcasing a future-oriented spirit that is representative of the status of a capital city in a country that must not forget its past, and is capable of walking proudly into the new era…..” instead of something like a “traditional-looking architecture that is nostalgic about the capital city's past, done with modern materials and construction technology, and has the capacity to hold any exhibition that shall exemplify great achievements within the city in this modern era”


One begins to read much contemporary architecture in China as not the manifestation of certain architectural concepts, but as certain struggles against some ideology/statement from higher up…


I find myself asking these questions:


Why do so many projects here seem so incomplete?


Or are these projects very well completed yet unrefined?


We know there is the problem of short construction phases in many buildings, but should that be considered as the most determining aspect in making certain architectural experiences less compelling?


If one is to look at certain Soviet architectures, the construction can be done rather poorly, but is still capable of conveying passion….


So how come so many architectures/buildings in China look so “unfiltered and full of noise”?


I begin to wonder, if that is not the issue of bad architecture, but simply the issue that the building is telling exactly that story behind its conception to realization…… too many inputs from too many important people might have led to the architect's sacrifice of the purity and integrity of the project, and the short construction phase simply meant that the architect never had the time to “fight back” (if he/she is so capable, if he/she dares)…


In any case, should the exhibition tell a story, it should be a story about success, right?


So here we have the successful story of the successful architect/developer John Portman who is very much involved in major architectural/development projects in Beijing and other cities. (But we care about him in Beijing mostly because he did many big projects here, as one can point out a landmark on the exhibition poster.)


Strangely, there is something that catches more attention than the John Portman exhibition.


So what exactly is that bronze looking thing that sticks out of the façade that looks like a cone yet turned out to be a gigantic post/drum/column that sticks diagonally into the ground inside the museum?


[“It is a big gesture, so it must have a legitimate idea/function behind it to back it up, and this thing must not be a huge sculpture, because first of all this is not the bronze museum, and even if it is, there is no single artist in China whose name is big enough, whose connections are solid enough to make a sculpture this big”. At that moment, I felt I was thinking like my project manager….]


So it has a theater, a gallery, and a few bathrooms inside it. The circulation along the interior walls of the museum atrium is too distant from the main exhibition hall to be considered major access into the “drum.” But that still, in my opinion, does not quite justify the use of the circular ramp within that thing to take people up. The space inside is simply too tight for the use of ramps. Somehow the architects were able to proportion the size of the drum in relation to the atrium in such a way to create a spectacle, yet not capable of managing the space within the drum, to the point that all three architects who went into the “bronze sculpture” that day decided that they just want to “get the \_\_\_\_ out of it” and did not even bother to take any picture. It is just badly done inside…


One simply wonders what led to the “architects' decision” to make such a thing and to make it that way… Perhaps someone can help me to read into this, assuming architecture always conveys to the trained eye many things about its realization….


To be continued….


[![](media/ccm-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-6.jpg)


.


[![](media/ccm-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-4.jpg)


.


[![](media/ccm-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-2.jpg)


.


[![](media/ccm-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-3.jpg)


.


[![](media/ccm-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ccm-5.jpg)


Cheng Feng Lau



 ## Comments 
1. [metamechanics](http://metamechanics.wordpress.com)
6.14.12 / 11pm


Thank you for the blogs, I was in China in 2000 and was blown away by the sea of cranes.


It could appear you might be presenting a case for “slowness”, but I don't think “slowness” would solve anything there. 


On one hand developing design ideas into something meaningful may take time but on the other the architect is not in charge of time unless they are the client, and to be your own client you must be independently wealthy, and if you are independently wealthy architecture practice is really a hobby since the supply and demand would not apply to you. Are there any John Portmans or Philip Johnsons in China? If not, I would wonder why?


In other media reports I have read that much of the design is done so quickly for projects in China that borrowing straight out of a book and essentially making the building a collage of other architectures is a common design practice.


I think the question should be: how can an architect design faster to keep pace with society?


When we (technical university of dortmund) visited Hong Kong's architecture school they told us at the time (year 2000) that freshman year everyone got a computer and after 5 years a masters in architecture. I was actually on exchange program from Kansas (KU) and compared this with my first two years at Kansas where at the time for the most part using a computer was not allowed and we wasted a whole semester doing ink on mylar. A most useless and “slow” skill. I was convinced HKU had it right. Then we met a former employee of I believe Ken Yeang and an AA grad. A Hong Kong native probably in her mid 30's at the time and she walked us thru her recent project as she talked about the project at 100 miles an hour. She kept asking us (german students and professors) if we had this and that in germany, the response a few times was “yes we developed that”.


With that said, if my reading and experience of Chinese architectural education and practice is any representation of how fast they work over there, I am not sure an architect could work faster.


Can we produce meaningful designs faster and why can't meaning be something as a result of speed? Why are we commited to a philosophy that prefers the turtle over the hare(spelling?). Why can't the hare (rabbit) be equally wise as the turtle?


In my opinion we waste a lot of time in architecture schools developing concepts and diagrams and frivilous research into fields that already exist with experts of their own.


If we could write in details as fast as we think maybe the architecture would become meaningful again in a market that gives two shits if the architect is involved. This would mean building technology and construction modeling/drawing and management would have to be the core of an architects education and not abstract concepts on how to use the materials and lights if at all. How many projects have you seen in school that just barely arrive at space planning?


How can you be critical of writing if you can not write?


Once again thank you for thought provoking posts.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.15.12 / 3am
	
	
	metamechanics: First of all, we should thank Cheng Feng Lau for his courageous reporting and commentary. It's very risky business in today's China. Second, your commentary, while not exposed to the same risks, expands our understanding of both the projects and the social and political dimensions of the work at hand. For this, I am very grateful. Architecture, deprived of such meanings, becomes empty formalism, the current ‘plague of the West.' Keep your critique coming!  
	
	Also, your personal testimony is worth a thousand merely theoretical musings.
	
	
	
	
	
		1. [metamechanics](http://metamechanics.wordpress.com)
		6.16.12 / 12am
		
		
		yes we should thank CFL – a lot! My trip to China was just over a month long from Shanghai to Beijing to Xi'an to Guangzhou to Shenzhen to Hong Kong…  
		
		Bamboo scaffolding might of been one of the coolest things I'd ever seen.
3. [chris hamby](http://www.chrishamby.com)
6.15.12 / 2pm


Thank you for this series, it's been wonderful so far. This site reminds me of Kisho Kurokawa's National Art Center in Tokyo, which makes use of similar interventions from the facade into the atrium space. That site, however, has a human scale and subtlety that appears completely missing from the Capital Museum.
5. [metamechanics](http://metamechanics.wordpress.com)
6.16.12 / 12am


“It took Dubai more than five years to build the 828-meter Burj Khalifa, the world's tallest building (for the moment, anyway). But Chinese architects and engineers reckon they need a mere 90 days to leave the Emiratis in the dust. At least, that's what they've claimed.” — cnngo.com
7. [Caleb Crawford](http://calebcrawford.wordpress.com)
6.18.12 / 2pm


I am a little disturbed by metamechanics' manifesto of speed. It expresses a kind of Futurist nihilism; a mechanistic, materialist, technocratic, and capitalist view of the world which is frankly unsustainable, and leads to the kind of thoughtless architectural gestures – gestures and mistakes – to which Mr. Cheng's post alluded. While I am no opponent of building information modelling, parametric modelling, scripting, nor a proponent of ink on mylar, the current obsession with speed is part of the sad capitalist push for lowest first costs – so called efficiency and maximization. Janine Benyus said “nature doesn't maximize, it optimizes.” Thought takes time. 


On the other hand, as a teacher of architecture, one thing I frequently see missing in students is an ability to quickly propose a series of distinct alternatives. I call this alacrity of thinking. We all frequently react well in critiques to earlier study models that exhibit a freshness, which often the later developments lose. It is difficult to maintain that freshness. However, as we see in the Capital Museum, that early spark of an idea did not have the time to mature and perhaps even be rejected. The Capital Museum sounds like a student project that got built, perhaps only having 15 weeks to develop, and foundations started after the first week.





	1. [metamechanics](http://metamechanics.wordpress.com)
	6.19.12 / 1am
	
	
	a speedy goolgle search reveals so far no name on the architect, so it would be great to know…or maybe it was students working on it for 15 weeks.
	
	
	Caleb…actually optimization is “efficiency and maximization” if you switch the words around “maximized efficiency”…
	
	
	prescribing “meaning” is overated and I don't think humans can do it anyway. “meaning” is a post-rationalization. 
	
	
	a question that would meet me and Caleb half-way – when do you stop developing a fresh idea? do the details, build the model, make real renders or whatever says its convincingly architecture – and NO diagrams & conceptual sketches do not count (they leave imagination to interpret)….or is that when you stop developing a fresh idea…if so are you an architect or an artist?
	
	
	i loath to mention this endeavor on Wood's site, but I'm doing somewhat a personal study/lack of design development study by simply modeling my dreams into 3D worlds with little analysis at first. Then to actually tie it all together use standard design principle from typology assumptions to building technology solutions. Somewhat of a lengthy process but what I've learned so far and in theme with the above post and much of China's architecture – (mind you based on my brain not anyone elses so far)
	
	
	– nearly all the dreams and visions of architecture although not recognizable exactly as they appear in the real world are actully collages of visual memories, logical explanations, and other narratives. Many of the parts can be traced back to real sources, although “meaning” of the collage is not clear (no attempt is being made to study the meaning, leave that to Freud).  
	
	– these collages can be produced even faster if the imagination is exposed to a lot of architectural stimuli (books and publications, experiences, etc…)  
	
	– falling into a trance, half-way to sleep, allows the visions of architecture to appear quickly (this is actual academic scientific dream stuff, look it up)
	
	
	Point being as related to this blog – if you were in China or someone else in quite the hurry, you might just produce the dream architecture immediately from vision to detail. I think the meaning of this whole unavoidable speed thing of “capitalism” will find its place eventually (meaning develops during post-rationalization of an era that seemingly happened and now we notice). whether it's here by our blogger from China – Cheng Feng Lau or later in the Architecture duration – the wise hare will prevail. no more turtle stories please, there is no validity in that slowness arguement EVER.
9. [Caleb Crawford](http://calebcrawford.wordpress.com)
6.18.12 / 2pm


By the way, who was the architect?





	1. Steven Chou
	7.14.12 / 2pm
	
	
	Designed by the AREP, France. Finished by China Architecture Design & Research Group
