
---
title: OYLER WU COLLABORATIVE  Screenplay
date: 2012-06-25 00:00:00
---

# OYLER WU COLLABORATIVE: Screenplay


[![](media/ow-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-4.jpg)


## SCREENPLAY


## By Oyler Wu Collaborative


*Screenplay* by Oyler Wu Collaborative will be on view from 6/22 to 6/24 at Dwell on Design 2012 at LA Convention Center.


*Screenplay* is conceived of as a ‘play' on one's visual perception.  This twenty-one foot long screen wall is constructed of forty-five thousand linear feet of rope strung through a series of lightweight steel frames.  The wall is designed with the intention of provoking a sense of curiosity by slowly revealing its form and complexity through physical and visual engagement with the work.  The wall is made from a repetitious steel framework with rope infill that varies over the length of the wall in three dimensions, forming a thickened undulating screen made up of dense line-work.   In its orthographic, or ‘straight on' view, the wall forms a meticulously organized series of patterns easily recognized by the viewer.  As the viewer moves around the wall, its three-dimensional qualities reveal a more complex system of deep sectional cavities, twisting surfaces, and material densities.  The experience is meant to build on an ‘on again/off again' system of pattern legibility, using optical effects as a means of provoking engagement in the work.


Project Design and Fabrication Team: Dwayne Oyler, Jenny Wu, Huy Le, Sanjay Sukie, Yaohua Wang, Qing Cao, Farnoosh Rafaie, Jie Yang, Clifford Ho, Joseph Chiafari, Tingting Lu, Qian Xu, Mina Jun, Vincent Yeh, Kaige Yang, Shouquan Sun


.


**Comment by LW**


In the perilous game of speculation about the design of space for human use, Dwayne Oyler and Jenny Wu have often insisted on constructing what they have conceived at 1:1. Drawings and scale models have not been enough. Consequently, the size of their speculative projects has been small. In their latest—*Screenplay—*we are given, as I read it, a domestic setting, let's call it part of a room, defined by a wall and a piece of furniture.  What is remarkable about the project is the way the architects have reconceived these conventional elements and invented new techniques of construction to realize their new ideas. The wall becomes a three-dimensional screen into which is projected a complex network of spaces, a transformative sequence that—like Schwitter's *Merzbau*—is inhabited by our own creative invention. There is a fantastical ‘play' of scales here, an architectonic drama heightened by the wall's unique construction, which hovers at the edge between transparency and solidity. It is from this vertical field that the rather conventional couch emerges, becoming radical in its familiarity.


Oyler and Wu show us here how closely linked full-scale construction and new spatial concepts always are. There is much to be learned from *Screenplay,* and much to be enjoyed.


[![](media/ow-7.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-7.jpg)


.


[![](media/ow-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-1.jpg)


.


[![](media/ow-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-3.jpg)


.


[![](media/ow-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-6.jpg)


.


[![](media/ow-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/ow-2.jpg)


.



 ## Comments 
1. [Screenplay |](http://pokk.se/?p=1365)
6.25.12 / 8pm


[…] fascinating work and words by others as well. Today there was a post catching my attention about Oyler Wu Collaboration and their project “Screenplay,” which could be experienced at Dwell on Design 2012 at LA Convention Center this weekend. It is a […]
3. [metamechanics](http://metamechanics.wordpress.com)
6.28.12 / 12am


The use of materials is awesome.  

The built looks like a really nice computer generated rendering.  

Its obsessive and detailed, good stuff.
5. michaelotchie
7.4.12 / 6pm


Reblogged this on [Architextile](http://architextile.wordpress.com/2012/07/04/91/) and commented:  

Listening to KCRW's most recent Design & Architecture podcast – <http://www.kcrw.com/etc/programs/de/> –  

I heard about this innovative use of materials and craftsmanship to realise highly complex patterns or forms. Knitting on building scale – why not!
