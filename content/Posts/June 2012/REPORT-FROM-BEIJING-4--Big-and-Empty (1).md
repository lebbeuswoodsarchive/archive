
---
title: REPORT FROM BEIJING 4  Big and Empty (1)
date: 2012-06-08 00:00:00
---

# REPORT FROM BEIJING 4: Big and Empty (1)


[![](media/cnm-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/cnm-3.jpg)


.


**Is this room empty? – Part 1 of 3 – Chinese National Museum**


Text and photos by Cheng Feng Lau


After visiting a few museum projects in Beijing I began asking myself: why am I having this bizarre feeling that a few of these museums have spaces that are quite a waste?


The term “wasteful” did not occur to me at first. The first impression of the situation that I am describing is usually “big” and then “empty”.


I did not quite understand why as an architect I am considering the “grand entrance halls” of these museums to be big and empty. At certain moments I even ask myself, am I thinking too much like an engineer looking at an atrium space in a building and wondering how inefficiently that space is organized… But this is not about issues like optimization or efficiency…


So I start to ask myself another question: Is there a threshold to bigness?


Or in other words: is there a size limit to which one may design a space before it triggers the sense of emptiness? In one's attempt to create the impression of grandeur through the means of increasing the size of a room, one somehow loses control….


So I attempt to search into my memory for “big spaces” in museums that do not make me feel “empty.” And then I remembered the turbine hall at Tate Modern.


To say the least, the turbine hall has certain sublime quality to it. Maybe that slightest fear/awe somehow filled the void. Or it is probably the fact that the turbine hall used to be completely filled (with machines) rather than emptied-out, the remaining walls are turned into palimpsests of its past, thus became a story book rather than merely a shell. At the same time one has to acknowledge the importance of the great works of art that occupy the space. I guess if it were bad art, the place would seem empty?


Now back to Beijing.


There are three projects – three great halls of three museums – that I would like to discuss in this series: Chinese National Museum, Capital City Museum, Chinese National Movie Museum.


China National Museum 


It used to be the Chinese Military Museum, but got transformed into China National Museum. I guess that means instead of talking about a country's military history, it now tells the story of the Chinese civilization from the very beginning…


The renovation is done by GMP. Since I have never visited this museum before the renovation, I cannot tell you if GMP has done a good job or if the original building is a good piece of architecture.


Of course you would have a grand hall on the central axis of this building, and this grand hall should be marked with a great piece of art. What you have here is the bas relief of the story *the old fool who moved the mountain.* 


The story goes that a mountain blocks the entrance of the old fool's house. (I still don't get why the old man or his ancestor had to build the house at that location in the first place. But I was also told that I shouldn't question why Achilles's mother had to hold on to his heel and why just one dip and not multiple dip…….) Every time someone from the old fool's family need to visit the town, they had to go over the mountain. It was such an inconvenience that the old fool decided to mobilize the entire family to level that entire mountain. How are they going to do it? Bucket by bucket, they tried to move the earth of that mountain and dump it at the sea hundreds of miles away. His neighbors began to call him a fool because of that. Yet the old man responded to those who called him fool: “I have sons and grandsons, and my grandsons are going to have sons and grandsons. Our family tree will go on and will never end but there's a limit to that mountain. Eventually, one day we will be able level that mountain.” Somehow, the old fool's action deeply moved someone in heaven. So heaven sent down someone to take the mountain away. End of story. Happy ending. Moral of the story? NEVER GIVE UP!!!


Interestingly, this story does incorporate some of the most important cultural philosophy of Chinese civilization: first of all the idea that a pure soul can move heaven with his/her action, which often leads to the solution of some problem.  Second, one must extend the family tree/gene pool, aka the notion of “bloodline”. Third, the deep attachment to the land, both in the sense of land is the basis of an agricultural society and also in the sense that land represents one's origin. Fourth, Chinese civilization has been at the frontier of creating artificial landmass ever since pre-historical time. (Just joking)


I believe all elementary school kids in China would know this story either from their parents or their Chinese textbook. And I also believe that by the time they reach their senior years in junior high, they would have learned another phrase that probably came out of Taoist philosophy: If the mountain does not turn, the road will turn; if the road will not turn, one's heart shall turn……


So *the old fool that moved the mountain* in the form of bas relief does in a way represent the Chinese civilization, which also means it is a suitable piece to occupy the center of the great hall of the museum with National status. So however big the space here might be, this bas relief would be capable of demarcating that bigness as a moment of contemplation and reflection…Maybe…..


One should probably say it is the materiality of the interior that made the space seems rich instead of empty. There was too much to look at, and the details were not bad. If one is to compare the size of the elevator shaft with the column with the space in which it situates, that dramatic contrast in scale would definitely cause some “wow”.


Anyhow, this space did not trigger the sense of emptiness that I am critical of. Yet I cannot say for sure if I would feel the same on a second visit, and I am not quite willing to say the presence of visitors is the most determinant factor…


To be continued….


[![](media/cnm-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/cnm-1.jpg)


.


[![](media/cnm-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/cnm-21.jpg)


.


[![](media/cnm-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/06/cnm-4.jpg)


**Cheng Feng Lau**


## Thank you, Mr. Cheng, for your outspoken and courageous reporting on the architecture of Beijing, for this blog.



 ## Comments 
1. [Bob](http://bennet121.tumblr.com/)
6.9.12 / 10pm


love the blog- always really interesting- we want more! : )
3. [betamagellan](http://betamagellan.wordpress.com)
6.10.12 / 3pm


One question/nitpick—are you sure this was once the Military Museum? I don't remember the Military Museum being so big and empty—if anything, it was cluttered with dioramas of the revolution, armaments, and socialist classicist ornamentation. Maybe was it formerly the Museum of the Chinese Revolution…


No matter, I loved the analysis and am looking forward to the next installments.





	1. [Charlie 阿理](http://xuehuang800.wordpress.com)
	7.23.12 / 2am
	
	
	‘Big and empty' precisely describes the state of many monumental buildings in Chinese cities, where the municipal leaders crazily drives to ‘big and spectacular', regardless of finance and actual usage. There are several books talking on this topic, for example, Building a Revolution: Chinese architecture since 1980, Hong Kong University Press, 2006; World Architecture in China, Joiint Publishing Ltd., Hong Kong, 2010.
