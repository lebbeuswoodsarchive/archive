
---
title: MAGIC MARKER
date: 2011-03-31 00:00:00 
tags: 
    - architecture_theory
    - Chris_Marker
    - criticism
    - evocation
    - La_Jetee
    - movies
---

# MAGIC MARKER


[![](media/laj-00-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-00-1.jpg)


*(above) The jetty, or pier, at Orly airport in Paris, c. 1960, where the story of *La Jetée* begins and ends.*


Speaking of masterpieces, the 1962 science fiction film, *La Jetée*, by Chris Marker, ranks very near the top in films, inviting comparison with early classics such as *Nosferatu* and *Faust*, as well later tours de force like *Beauty and the Beast* and *Alphaville*, even though it is only twenty-eight minutes long. Without question, it puts to shame contemporary films of its genre snd most especially its re-make, *Twelve Monkeys*, which had an all-star cast, a vastly bigger budget and the slickest special effects money can buy. How could this be, given *La Jetée*'s black and white format and the fact that it is made entirely (almost) from still photographs? Its actors are little known and anyway we never hear them speak, instead hearing only music and a narrator's voice superimposed on the still images. The story is rather conventional, a tale from a post-World War Three world, in which the hero must travel into the past and the future in search of energy sources and food for a ruined, radioactive city. And yes, there is a man-woman love interest that, in Romeo and Juliet style, is in the end tragically thwarted by fate. How did Marker turn such mundane ingredients into a masterpiece, a work of art of the highest order? The answer, I believe, is of value to artists in every medium, including the stubbornly resistant-to-art field of architecture.


Let me sum up the answer in a simple phrase: the power of evocation. For everything that is not shown, the filmmaker counts on the power of imagination of his viewers. What this approach depends on for success is the discretion of the artist, his or her ability to know what to put in and what to leave out. The omissions are as important, or perhaps more important, than knowing what to put in. Only the voids—what is not there—can be fully filled by our imaginations. It's an extremely delicate game of balance, one that varies from scene to scene, sequence to sequence. The film evokes a sense of continuity, a wholeness, that is not actually there. In this way, the viewers are active collaborators with the filmmaker and not merely passive spectators. The film is completed each time and only when it is viewed.


What makes this game work is the film's verisimilitude, its accurate construction of a  parallel with our actual experience. If we use architecture as an example, we begin with the realization that architecture is still, not itself moving, thus it lacks—in our perceptions at least—continuity. Walking through a building is much like the film, a sequence of still views that rely on a turn of the head to evoke movement. In that instant of turning we effectively perceive a blur at best, which our brains don't register, much as they don't register the background noise we live with, so that turning our heads becomes a quick cut from one image to the next, much the same as the film. It is telling to note that Marker does not use dissolves. Rather, he stays close to the way we actually see, discontinuously.


So much for what is left out. What is left in *La Jetée* is a set of photographic images of characters and places that are visually striking but at the same time informal. Rather than aiming to be stand-alone works of art, the stills are like frames from a motion picture—a movie—that does not exist. This is another instance of incompletion, of making evocative voids to be filled by imagination, but something more. The stills, which stay on the screen for minutes, not seconds, invite us to go into each image in a way we cannot when watching a movie, discovering details and studying the actor's expressions, appearance, gestures, in depth. The effect is highly emotional, yet in a contemplative, even intellectual way. This paradoxical quality enhances the evocative power of *La Jetée*.


How might Marker's approach inform the plastic arts and especially architecture? It cannot not be through the design of intentionally incomplete buildings—though every architect knows the excitement of seeing a building still in construction, or another that has slowly, gracefully become a ruin—this kind of self-conscious literalism is fit only for theme parks and B-movies. The *La Jetée* approach in architecture must be as abstract as architecture itself. So, the creation of spaces of the imagination can only be found, it would seem, in an architecture of fragments, loosely joined like neurons in the brain, separated by synapses that can only be bridged by an electrical charge of thought. The key to such an architecture would be the potency of its fragments and thus its capability to inspire leaps of imagination that join them into a whole within the mind. Exactly how such an architecture would appear to us we cannot know at present, as its emergence is still in the future.


LW


*In an underground catacomb, the survivors of a nuclear war escape the radiation afflicting the world above. Scientists and doctors experiment on survivors in the hope of traveling in time to find new sources of energy and food:*


[![](media/laj-9-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-9-1.jpg)


Some of their subjects die and others go mad:


[![](media/laj-10-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-10-11.jpg)


Understanding that only subjects with very strong memories of the past can survive time travel, they find one survivor with a particularly powerful memory of a day long ago at Orly airport:


[![](media/laj-2-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-2-1.jpg)


A woman is the focus of his memory:


[![](media/laj-6-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-6-1.jpg)


He travels to the past several times, on each visit spending time with her:


[![](media/laj-0-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-0-1.jpg)


[![](media/laj-4-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-4-1.jpg)


Finally, the scientists and doctors decide he is ready to go to the future:


[![](media/laj-8-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-8-11.jpg)


There he finds people, who after a few visits give him a power-plant that fits in his hand and can provide energy to satisfy all the world's needs, which he takes back to the present. Finally, the people from the future invite him to stay with them:


[![](media/laj-20-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-20-1.jpg)


But he decides to go back to the past in search of the woman, and at Orly meets his fate:


[![](media/laj-7-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/laj-7-1.jpg)


.


*La Jetée can be seen in its entirety at several online sites. Go here and take your choice:*


<http://www.google.com/search?client=safari&rls=en&q=la+jetee+video+online&ie=UTF-8&oe=UTF-8>


#architecture_theory #Chris_Marker #criticism #evocation #La_Jetee #movies
 ## Comments 
1. Stan Wiechers
3.31.11 / 1pm


<http://art-agenda.com/shows/view/1542>


there is a chris marker exhibit in the city this weekend
3. Pedro Esteban
3.31.11 / 9pm


bravo!!!! excelent!!!!!!!!!!!!!!!!!!! too much!!
5. [Film & Architecture | workspace](http://www.johndan.com/workspace/?p=2292)
4.1.11 / 3am


[…] Lebbeus Woods riffs on Chris Marker's 28-minute masterpiece, La Jeteé. Famously the inspiration for Terry […]
7. [Fine Grain II « Dig This Blender](http://bomoc.wordpress.com/2011/04/07/fine-grain-ii/)
4.7.11 / 1pm


[…] Steven Holl, Urbanisms; Working with Doubt (New York: Princeton Architectural Press, 2009), 17 v <https://lebbeuswoods.wordpress.com/2011/03/31/magic-marker/> vi Andrei Tarkovsky [director], Stalker, Screenplay written by Boris and Arkady Strugatsky […]
9. aida miron
4.8.11 / 4pm


You write that it is telling that he does not dissolve, that he stays close to the discontinuous…it is this very clarity of the image that is so powerful…..in Sans Soleil: he writes to the speaker: “I will have spent my life trying to understand the function of remembering, which is not the opposite of forgetting, but rather its lining….” We do not know who the speaker or the writer is, but the relationship between image, sound, voice, memory, the visible and the articulable is acute. To think of memory as a double-sided surface, to try to understand this fold. The stills as you say, invite us to move through them..the tectonic characteristic of the still-image-time frame in La Jetee makes it possible for the viewer to ENTER….there is materiality in the moment….The linking of images to re-construct this event, the way the moment solidifies with gravity with materials: “glass”, “plastics” and then the “ruins”.. Linking this discontinuity comes closest to memory. Memory like history leaves out events, rewrites itself. Here the repetition of a moment, but “different…” again the double sidedness of memory… in Sans Soleil again, he writes: “history only tastes bitter to those who expect it to be sugar coated.” It is very telling that your next blog is about the capture of Ai WeiWei, for both Chris Marker and Ai WeiWei's work deal directly with power, ethics and freedom, and the reverse side of it.  

It is the political and ethical of your work and posts that are so important to different abstract-not so abstract digital communities. Thanks Lebbeus





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.8.11 / 5pm
	
	
	aida: I like very much your metaphor of the double-sidedness of memory. Also your link between Marker and Ai. Certainly it was the link between ethics and aesthetics that attracted me to architecture long ago, and more recently inspired this blog. I'm very glad you underscored this aspect of the work. As for the digital communities, each member should read once a day the extraordinary, succinct essay by Heinz von Foerster on this blog, [CONSTRUCTING A REALITY](https://lebbeuswoods.wordpress.com/2010/07/17/constructing-a-reality/). It shows that one can hide in the computer only at the expense of one's common humanity. And this is from one of the founders of Cybernetics.
11. [Lebbeus Woods on La Jetée « rutkacinema](http://rutkacinema.wordpress.com/2012/02/09/lebbeus-woods-on-la-jetee/)
2.9.12 / 3am


[…] <https://lebbeuswoods.wordpress.com/2011/03/31/magic-marker/> Share this:TwitterFacebookLike this:LikeBe the first to like this post. Published: February 9, 2012 Filed Under: Uncategorized […]
13. [Lebbeus Woods on La Jetée | poeticsofcinemaam](http://poeticsofcinemaam.wordpress.com/2012/02/16/lebbeus-woods-on-la-jetee/)
2.16.12 / 3am


[…] Lebbeus Woods Share this:TwitterFacebookLike this:LikeBe the first to like this post. This entry was posted in Uncategorized. Bookmark the permalink. ← Week 4 […]
15. [La Jetée – To call past and future, to the rescue of the present. « Neither this nor that](http://neitherthisnorthat.wordpress.com/2012/07/16/la-jetee-to-call-past-and-future-to-the-rescue-of-the-present/)
7.15.12 / 7pm


[…] <http://www.chrismarker.org/jean-louis-schefer-on-la-jete/> <https://lebbeuswoods.wordpress.com/2011/03/31/magic-marker/> Share this:FacebookEmailLike this:LikeBe the first to like […]
