
---
title: ZAHA’S WAY
date: 2011-03-27 00:00:00
---

# ZAHA'S WAY


Judging from the photographs I've seen, Zaha Hadid's new opera house in Guangzhou, China, is a stunning tour de force of design and construction, a masterpiece by a gifted architect at the height of her creative powers. It is difficult to imagine that she or anyone else could top this building in its invention of monumental yet elegant forms. That said—and it is quite a lot—this work raises vexing questions.


The first question occurred to me when I received an emailed invitation to the grand opening of the opera house.


[![](media/zaha-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/zaha-3.jpg)


The invitation was accompanied by four photographs of the completed building. At least I assumed they were photographs, because I assumed Zaha Hadid Architects would want to proudly show nothing less than the authentic building and certainly not computer renderings. There was not a single human figure in any of the images, nor any trace at all of a human presence—a scratch on the wall, a seat folded back in the grand seating area of the theater itself, tire-marks or footprints in what seemed to be a driveway, a small, overlooked scrap of paper on the floor. I found this uncanny and, after looking over the ‘photos' decided that they were, after all, and even considering the inappropriateness of this for a grand opening invitation, very skillful computer renderings.


[![](media/zaha-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/zaha-11.jpg)


[![](media/zaha-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/zaha-6.jpg)


Everything about them was just too perfect, down to the reflections in shiny metal panels in the skylight structure. Shaking my head a bit, I filed the invitation and ‘photos' in a folder called ZAHA: HER WAY, and put it out of my mind. I had neither the desire nor the ability to jump on a jet and fly to the other side of the plant to attend what was sure to be a very swank party sponsored by Moet Chandon and Hennessy. Much as I like their products, I long ago lost my taste for mingling with glittering crowds being nonchalant and making polite chit-chat. It wasn't until a week or so later that a close colleague of mine stopped by and between some very impolite chit-chat asked me about the opera house. I opened the folder and the building ‘photos,' stating my conviction that they were computer renderings. No, no, he said, explaining that he had seen the same images [online](http://www.dezeen.com/2011/02/25/guangzhou-opera-house-by-zaha-hadid-architects/) credited to the photographer Christian Richters. Besides, he said, this was the look Zaha and a few other top-of-the-heap architects wanted, a kind of pure architecture. What followed was an hour-long discussion of why an architect would want to build buildings that look like computer renderings.


[![](media/zaha-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/zaha-4.jpg)


My conclusion was less about pure architecture and architecture-for-architecture's-sake, than a utopian hubris, a belief that architecture is the most important thing in the world, because it can tame the infernal machine and turn it to the perfection of human thought, feeling, and activity. This was the fiercely promoted viewpoint of utopians such as Kasimir Malevich and Bruno Taut and to some extent Le Corbusier and Frank Lloyd Wright, and in the early days of Modernism in its varied forms, it can seem to us innocent and admirable, but no longer so. Aware of the history of the past hundred years and the turbulent character of the present, such an attitude can only seem arrogant and self-indulgent. This appraisal is not simply about images, but about buildings, even masterpieces of architecture regarded as an extension of an architectural history of masterpieces, that are utterly oblivious to the uncertain and conflicted human condition of today, which is unprecedented in history.


[![](media/zaha-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/zaha-5.jpg)


What is urgently needed now is the very antithesis of utopian purity: *masterpieces of imperfection.*



LW



 ## Comments 
1. Pedro Esteban
3.27.11 / 3pm


”What is urgently needed now is the very antithesis of utopian purity: masterpieces of imperfection”





	1. Jason Lee
	4.4.11 / 1pm
	
	
	antonio garcia abril perhaps?
3. [Yorik](http://yorik.uncreated.net)
3.27.11 / 4pm


About the relationship between architecture and rendering, it is funny to think that if renderings awaken a kind of desire among architects (I want my building to look like this rendering), most computer artists who do those renderings try to achieve the contrary, maximum photo-realism (I want my rendering to look like this building)… That's a bit the story of the snake that bites its tail…
5. [scutanddestroy](http://scutanddestroy.wordpress.com)
3.27.11 / 4pm


You've summed up in this brief article my exact frustrations when I pursued my master's degree in design. These kinds of discussions were unofficially verboten in the school I attended.
7. Pedro Esteban
3.27.11 / 4pm


I like the idea of no rendering, the project itself have his own soul, I can decide materials but the end is impossible to know. The drawing can show how, but envision the final product, that don't have anything of fun, my projects are in a black space, they after came to a context, too much artigianal, but I like it.  

Is interesting see how you can show with the drawing a part of that creation, and that creation become a complete object by itself.


The render maybe is a demonstration of ours fears, is just a ratification.
9. Patricio De Stefani
3.27.11 / 4pm


An ideological ouroboros, a concrete abstraction… a fetish
11. Pedro Esteban
3.27.11 / 5pm


But I don't like to put the render as a bad tool, at all, anyone can make architecture as he/she(haha refering to Zaha) want to, the only problem is who make the render and for what is doing it.
13. Lucas W
3.27.11 / 8pm


Perhaps renderings should begin to exhibit imperfections? I've always wished to see photorealistic renderings of the building during heavy rain, a few people outside huddling under umbrellas. Or maybe snow-covered, obscuring all the details and angles, accumulating on the cantilevered angles and non-vertical surfaces. Or a theater after the patrons have left and leave the assorted detritus behind.





	1. Matt Lewis
	8.22.11 / 5pm
	
	
	This could be a step in the right direction. Too often do we see the same graphic conventions repeated ad nauseum (my particular pet peeve is the use of the silhouetted breakdancer to suggest the flexibility of an unprogrammed space.)
15. andrei
3.28.11 / 1am


I always think of architect's choice of methods as a window into their ideas and drives. Architects do not build buildings, they build ideas. The drawings therefore become important in communicating these ideas; they become a text. From zaha's representations (shown here) i read nothing but a compulsive obsession with form. These renderings become generic in a sence that this building could be anyone of zaha.s creations located anywhere. Instead of numerous computer renderings that tell nothing about its contex, human scale, underlying ideas, they should have created one that told us everything (and i am sure this project is deservant of such a rendering). Just because you can render every square inch of your computer model, no matter how wonderful it is doesnt mean that you should.
17. [Tom Gallagher](http://www.semplebrowndesign.com)
3.28.11 / 1am


Not sure if I'm adding to the discussion here or steering ti on a tangent:….@Lucas W, and lo, to Lebbeus: for renderings that “begin to exhibit imperfections”, check out Peter Guthrie, (vis-a-vis, dandelions in front of Farnsworth House): <http://www.peterguthrie.net/visualisation/farnsworth01/>…and for that matter, “the Third and the Seventh”….  

I've been hoping that someone could/would give Lebbeus' OneFiveFour Architectures a new life through this media.  

Lebbeus, I'm interested in your take on that idea/ these artists….
19. [seier+seier](http://www.flickr.com/photos/seier)
3.28.11 / 8am


oh yes. 


salman rushdie wrote well about this too after his own showdown with “the apostles of purity”, as he called the religious utopians.


did wright fall into the same trap? maybe he did. I always felt you could live your own life in a usonian – provided you weren't too tall, of course.
21. Diego
3.28.11 / 9am


( From lat. vanitas-atis).


1. f. The quality of being vain.


2. f. Arrogance, conceit, vanity.


3. f. Expiration of the things of this world.


4. f. Word useless or futile and pointless.


5. f. Representation vain illusion or fantasy fiction.


It is interesting to note the definition 3, because it opens the doors of the imperfection.
23. johnk
3.28.11 / 6pm


LB, the dialog you bring with your blog is great! I appreciate the opportunity to join in. As far as this project is concerned these images are fantastic, the first two, i can sense an idea of movement, and not just a building. But overall where is the transformation as you often write about. As Siza also mentions, he praises transformation rather than invention. Does this project tell a story of becoming, Do you think an existing fabric or movement of the city could have brought scale to the project, which you mention is missing?
25. S. v. Stuckrad
3.28.11 / 7pm


Hello,


thanks for the article. After I once listened to a lecture of Zaha Hadid, I do not like her any more. But I do acknowledge her work. I think as professionals we should be able to differentiate between architecture and our emotional feelings to it.


By the way:  

the pictures taken are not from Iwan Baan, though he is a great photographer. This time they are from Christian Richters. Please correct that in the blog.
27. [chris teeter](http://www.metamechanics.com)
3.29.11 / 1am


Lebbeus you just answered my question from the last post I believe – Zaha is riding the machine as the futurists did off into war. This clearly isn't an escape from the machine man often tries to rallying against, a machine he created – neil spillers take on this would be great. 


(Zahas guy) Peter schumacher and parametricism…this makes to much sense now regarding my question in previous post. she is number one on riding the machine list. I was going to say maybe pomo, but pomo leads to freakish adaptations of new building systems – GFRC, EIFS, thin slabs of stone and wp'ing -in place of real stone, leading to rationally imperfect conditions. Matter fact, faking wood molding with bendywood or poly something is not pure like Zaha, its impure leading to more impurities in logic railing against modernism? Maybe.


 I am a huge fan of parametricism, which erronously the same schools teaching parmetrics in grasshooper (can be done in 3d Max using Ali Torabis pligin), the same schools have you read oedipus:1000 plateus (deluze + guitarri) encouraging you to try to find the non linear moments of bifurcation and invention  

 of new modes beyond the prescribed..plenty Delands. its either controlled prescription via complex math gymnastics or rebellious invention of yet to come logics…a hedonistic fuck you and visceral encounter, although the overwhelmingly virtual could be quite visceral!


Correct me if I am wrong – zaha painted before she had people rendering, her renderers matched the style of her paintings, her renderings have been built


Frank lloyd Wrights – building used in the movie Gatacca – that's what this reminds me of Gatacca, so damn pure and rational.


I have never read any zaha theory though.


But building permanent usually ephemeral virtual concepts seems odd…thinking out loud…the house that built mies in koolhaas S,M,X,Xl, a temporary 1 to 1 mock up…maybe building the overly pure concept will lead us to something?
29. [Stephen](http://stephenkorbichartist@blogspot.com)
3.29.11 / 2am


These are strange photos. If you follow the link to the photos with people in them you may find that they are still strange.  

The building is overpowering to the human scale, interesting and sculptural but alien.
31. [Lebbeus Woods for Zaha//renderings? | Future-Giraffes](http://futuregiraffes.wordpress.com/2011/03/29/lebbeus-woods-for-zaharenderings/)
3.29.11 / 10am


[…] on LW's blog (+) LW's website […]
33. leslie
3.29.11 / 2pm


“There is hope in honest error; none in the icy perfections of a mere stylist.”
35. quadruspenseroso
3.29.11 / 2pm


Lebbeus, your thoughts have put more spring in my step. I'm a fan of gritty sprezzatura but not the kind that sends out invitations to watch foil-wrapped cadaver parts in a microwave. 


Who do we thank for the cold purity of morbidly sick space executed à la Zaha, Gehry et alibi? Weak institutions, they always do something monumental.
37. [JSavage](http://jksavage.tumblr.com/)
3.30.11 / 3pm


How do we design for imperfection? How can we take this understanding that the architecture as it is built in the World is never going to be “pure” and apply it to the design process?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.30.11 / 10pm
	
	
	JSavage: Please look for my upcoming post MAGIC MARKER. It addresses your questions.
39. [Greg Miller](http://www.pindropacoustics.com)
3.30.11 / 8pm


I think it is insightful that you mistook these for renderings because of the lack of human figures. I can be convinced to forgive that in the photos of many types of buildings, but an opera house?!? The entire purpose of an opera house is to be a gathering space for many people to enjoy a shared community experience. The seats are not in the hall to express geometry, they are there to hold people and I reject the notion that any performance space can be considered fully beheld without it being occupied by people. 


I have worked with many architects who design excellent performance spaces–halls that promote connection between audience and performers–and I nearly all of their renderings include an audience.


There is nothing wrong, in my view, with exploration of the form of performance spaces (and I hope to visit the Guangzhou hall to see/hear it for myself) but these explorations must be based on shaping the performances and communal experiences of the people who are present.
41. RosCon
3.31.11 / 5am


Figures would only serve to spoil the architecture, like flies on Zaha's cake. This opera house isn't for the flies, though when the flies come buzzing in, they can enjoy getting drunk on French booze.
43. [J.Stinson](http://www.coroflot.com/stinson)
3.31.11 / 9am


I'm a student and in my current project I'm trying to elucidate this exact point. I've posited that the current trend in computer rendering is the production of sanitized visions, images that are repositories for effects that formulate utopian apparitions, void of any sense of danger, uneasiness, erosion and spectacle which all certainly exist in the built world. By expunging these objectionable conditions, designers/artists depict formal and spatial scenarios that are photo-real, but not realistic.


My initial reaction to this was to create a series of images that would combat this trend, so in contrast to Sanitized Visions, I created Dystopian Dreams. These images are not meant to glorify, but in a sense meant to destroy, undermine or subvert the manufactured Utopian image. So I took that little gem box Farnsworth House, aged it and let nature have its way. 


<http://www.coroflot.com/stinson/dystopian-dreams>


Farnsworth is great because its this simple little glass box in the middle of nature, and at the same time its dismal for the same reason. In the winter its too cold, in the summer its to hot, at night its a lantern for all the bugs, and its flooded like half a dozen times, since its creation its been nearly unlivable. 


We want to enrich the world, make it a better place but as designers we can't do that by creating or formulating some sort of autonomous architectural theory. We have to engage with the complex, and sometimes ugly truths that is reality. I think any medium used to convey an artists ideas has latent clues to their thought process. These pristine renderings/photos just show how architects are envisioning the spaces they create, beautiful, flawless,….and self serving, void of all signs of life.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.31.11 / 12pm
	
	
	J.Stinson: Fascinating vision of “deferred maintenance.” You might like to read my post on this blog titled TOTAL DESIGN. This is not, as you know I'm sure, what I have in mind for ‘masterpieces of imperfection'—they would be livable, and in new ways. Thanks for your clear-headed comment, crisply written.
45. Wayne White
3.31.11 / 1pm


Lebbeus, lighten up.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.31.11 / 6pm
	
	
	Wayne White. Oh, my….
47. dazed
4.3.11 / 6am


It's obvious how imperfect computer rendering is. Therefore obvious how it's built building is. That is why hand drawing remains the most emotional. And overlooked pieces of paper can sometimes be a lost treasure. “monumental” “elegant” EitherwaY
49. bhb
4.4.11 / 12pm


The rendering is the byproduct not of architectural vision but of pandering to the client. (They want to see what they're paying for.) If it is indeed of the architect's imagination, then there is no longer any productive “gap” between the image and what is to come. This is no longer exciting for me as an architect and moreover as a thinking, feeling person.


It's also revealing that in this discussion, with Mr. Woods' commentary included, we've confused the photographs for renderings. And because the renderings are indeed photographs, this leads me to point an accusatory finger at the architecture itself. It's been made for the image from the start, and experience follows next, even if closely.


@J.Stinson. Your thoughts are rather interesting, and I believe in an intriguing direction. Overly academic writing aside, I'd also point out that while your images show architecture in imperfect conditions, the medium and the method are still too clinical. In other words, the vegetation is still too perfect for me to believe that some kind of natural decay is happening. But the idea is fascinating. Of course, no one would really pursue these images in the end, and frankly I'd not really like to inhabit any of those spaces.


There are good examples of renderers who add a little “style,” a kind of atmospheric storytelling to their images. Here are a couple of examples:


<http://marekdenko.net/>  

Though not explicitly an architectural renderer, this guy adds loads of detail to make some scenes feel rough at the edges.


<http://www.mir.no/>  

Obviously done digitally, but with heavy stylization.


My suggestion to all of us is this: either make the renderings meet their full potential by making imperfections believable–and actually inhabiting the architecture–or step back from the absurd pursuit of photorealism. It's an age old cliche perhaps, but the harder we push for such realism, the bigger the possibility for disappointment by the real thing.
51. John Doe
4.6.11 / 6am


They are not “photos”, they are “photoshopped-photos”: something between a real photo and a virtual render…
53. [mgerwing](http://www.mgerwingarch.com)
4.8.11 / 6pm


People always seem to forget that a building is a massive, singular, handmade construction, not a prototyped, highly-machined object or product. Every other handmade endeavor we praise and desire its “handmade” nature, the marks of the craftsmen and inherent and beautiful imperfections.
55. Alessandro Orsini
4.9.11 / 8pm


Hi Labbeus,  

we crossed several times at Steven's office when I was working there….  

I was impressed by Zaha's invitation…..It perfectly makes sense. It all starts from the Guggenheim exhibition where her projects for a Stadium got confused with a bowl. I though it was a piece for Alessi instead.  

But Zaha Hadid is a brand exactly like Chanel…and big brand are part of LVMH company.  

I still think her Drawings from 1982 were amazing.  

See you soon again!  

Alessandro
57. [Josh](http://rndrd.com)
4.15.11 / 4pm


Another look at it in a different light: <http://www.flickr.com/photos/dzahsh/sets/72157626503948068/>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.15.11 / 6pm
	
	
	josh: Thanks for these. They certainly disspell the idea of perfection that was achieved in the (apparently) PhotoShoped photos on the invitation.
	3. Pedro Esteban
	4.15.11 / 8pm
	
	
	At least, one who don't let carried away by the ”renders” world. This shows how right I'm in architecture, I hate contemporary architecture for this stuff, seems be and is not.
59. Pedro Esteban
5.1.11 / 12am


a radical comment:  

this lie will be ”superior” to classic architecture?





	1. Pedro Esteban
	5.1.11 / 1am
	
	
	————————————————————————  
	
	I just have answer my own question, this can be and is, superior or equal, to classic architecture  
	
	<http://www.zaha-hadid.com/education/vitra-fire-station>  
	
	(drawings and video too)
	
	
	comparatives are in some way difficult, but sometimes are really necessary!!!!! Hope my comment be understood.
61. Loaei Thabet
5.1.11 / 5am


The post on Zaha Hadid's invitation for her new Opera House in China with the four pictures attached with the invitation triggers a utopian perfection- related debate. The pictures Zaha and other architects chose to include with the invitation have taken Lebbeus Woods, who received the invitation, to an imaginative planet; to the extent that he thought that they were “computer renderings”. LW started to question the authenticity of the images and totally ignored the efficiency indicators of such a building. It might be that Zaha's choice of those four images was to imply that authentic buildings can exhibit perfection, as architects try to demonstrate that they can turn things to be perfect, and neglect the fact that, according to the blog, even the masterpieces sometimes exclude the human conditions. 


Craig Delyen, one of the principal founders of Black Line LLC. Architectural firm, stated that, in one of the panel discussions prepared for professional practice class at Ball State University, architects should be so supportive of arts, as arts is the most effective way to enhance the normal people's awareness of the value of architecture. Hence, they would understand the importance of the architect as beauty creators, while Maria Novak, a graduate architect at arcDesign, agrees on the principle that she looks at architecture through the art lenses; she, on the other hand, perceives it as a perfect human thoughtful art, where the idea that architects who are problem solvers can be slightly felt. Considering those two views, the latter architect focuses not only on architecture as a pure art but also as an art with a purpose.


It was noticeable that all the photos were taken at the night time, and I think that the night time adds more glittering touches to those pictures. Moreover, it has to be mentioned that the night shots as opposed to day shots help hide flaws and defects in both the materials and the buildings. Looking at the rest of the online pictures that were taken by the photographers, you can see more things there, such as the people inside those building. So the question is, why were those specific four photos chosen? As mentioned above, I believe that Zaha had intentions to create a gap in an attempt to invigorate the people's critical thinking, to prove that authentic building could also reach perfection, and, maybe, drag the people to the opening. I think her choice of those pictures with the invitation was smart; as it contributes to attract people's attention to her achievement, as well as to approach it with a critical eye.


The final statement in the post “What is urgently needed now is the very antithesis of utopian purity: masterpieces of imperfection.” sounds like an exaggerated reaction to the pictures, and merely based on LW's personal beliefs. Even trying to put myself in his shoe, I was encountered with many questions such as: how LW came to such a final statement of “antithesis of utopian purity: master pieces of imperfection”, which I consider still undefined and uncertain approach he adopted to address the uncertain and conflicted human condition of today. Why there would not be an approach that combines both the admirable and innocent principles in the utopian views, and the practicality and feasibility of the so called “antithesis of utopian purity” to maintain the wonder and hope of pursuit the perfection, and yet still consider all the current conflicts of human as the ultimate goal for architecture.
63. Pedro Esteban
5.1.11 / 2pm


1.the market choose the pictures not Zaha  

2. these pictures above, have the same principles you say and I think you will go there:  

<http://www.fosterandpartners.com/Projects/1496/Default.aspx>


btw: I don't want to go to the world who Zaha ”or others” is inviting me.
65. Fine Alleydone
5.1.11 / 6pm


@Lebbeus Woods,


The photos presented in this article by Christian Richters have been taken in a way to showcase the building and focus on the overall creation and form. This idea of trying to achieve “utopian perfection” may not be intended by Zaha or the photographer. In many other cases, similar images are used all the time in books and magazines that illustrate the overall grandeur of a building or space without any sign of people or imperfections. I feel as if this is just the way buildings are typically photographed, and in this particular case, could be the only way to photograph it since it had not been open or used yet. I think it is a tribute to Zaha and her designs; material choices and lighting strategies to be so well thought out and constructed to look “too perfect”.


On the other hand, people that can create imagery that is perceived to be photorealistic is astounding in my opinion. Being a recent M.Arch graduate, my experiences in trying to make photorealistic renderings is a great feat in itself. This (Realistic Rendering) is an exceptional skill to possess when trying to convey to clients what the overall look and feel of their building is going to be. Many times this is used as a marketing tool and is an effective option for helping everyone visualize the building; stimulating community interest. The “perfect” images, photos or renderings, make individuals wonder what it would actually feel like to be in the space, adding a certain level of mystique and ambiguity.


A second round of photos, possibly during the opera house's grand opening, portraying the uses in various spaces and individuals interacting within them would be effective. These images including people would merely help the viewers discern a sense of scale, considering the images shown above do not convey this information well at all. As technology continues to get better, the line separating renderings and photographs will continue to get thinner as well until it is indistinguishable between the two. 


So the idealistic photographs successfully illustrate the building's beauty and elegance, which is what the goal was (I believe). I would also question Lebbeus' statement of “needing masterpieces of imperfection”; why wouldn't we want to pursue masterpieces that are perfect? That would mean the project was successful throughout, formally and functionally, and as the architect, the fundamental objective!
67. powerlessness of perspectives
5.2.11 / 9pm


A lot of comments above had been given about how should design renderings be like and how should design be related to them. Besides these discussions, I saw another problem from this matter—the powerlessness of perspectives to convey designs contents.


The contradiction I found in the argument in the post was that on one hand, you emphasized the presence of human figure and activity traces, and denied a design aiming at aesthetic purity and graphic effect, but on the other hand you had given up the opportunity to experience the building in person, and was judging the whole design just based on four photos.


It is a common sense that architecture is a three dimensional existence, perceptual phenomenon that combines time, material and other aspects. The paradox is that unlike the use of architecture which is based on perception with all senses, the communication of architectural design mainly depends on images, which is basically visual. Let me take Zumthor's Swiss Pavilion at 2000 Expo in Hanover for example. Also called “Swiss sound box”, the pavilion had a maze-like interior and was built in timber with special technique, and was haunted with musicians. The most important part of this design is the amazing spatial experience it created. I don't think one can understand or feel this design by merely looking at a photo taken from somewhere outside the building.


Therefore in this case, technically we can only reach some aesthetic judgment with the available perspectives. I believe the design consist of other great qualities, if it is a great design. For instance, is the theater performing with sound effect enhanced by the design? Is the streamlined space organized under an impressive sequence? Is the seemingly cutting edge material and structure intelligently designed? The answers to such questions are exactly the uncertain and conflicted human conditions that you are calling the contemporary designers to pay attention to.


So, it might as well be worth attending the opening and experiencing the building to find further idea toward this issue. Plus, I think a lot of people are looking forward to the follow up posts.
69. Rellim Mada
5.3.11 / 1am


This is a very interesting discussion. Lebbeus, while you shift the conversation from the photography to utopianism, I would like to comment on the prior.


I am interested in not only the design carried out by the architect, but also the portrayal of the space by the photographer (or visual artist, in the case of renderings). In the early days of modernism, Julius Shulman helped form an aesthetic of architectural photography that was based on pure space, often lacking notions of scale or people within the photographs. Not only were the architects aiming to produce pristine forms but the architectural photographers were also emphasizing these utopian visions. It is like the models we find on the cover of Vogue today, illustrating perfectionism. Now, in contemporary culture, we know that the beautiful Neutra house probably leaked, and the cover model was retouched.


I'm curious why this trend still exists in the representation (and design) of architecture. I do believe, though, that a shift is occurring to what Lebbeus describes as “masterpieces of imperfection”. A photographer at the forefront of this discussion is Iwan Baan (who, ironically, was originally listed as the photographer of the above images). Iwan attempts to break from the utopian Shulman-style with a certain amount of realism. Rather than an elaborate photo shoot setup, Baan works with a point-and-shoot camera, regularly capturing groups of people and messy power lines in the foreground of his photographs. His style seems more directed towards that of photojournalism. It would be interesting to think of an architect designing through a similar process, highlighting these “imperfections” as Lebbeus states.


I have to agree with Iwan Baan's approach to architectural photography and feel that a raw portrayal of the built environment is favorable. This type of photo depicts the true form of the structure, without any illusions. It allows one to get a grasp of all of the elements of the building; while also being able to form one's own opinion. It strays from an almost propagandistic strategy as well. When first looking through the photographs, my opinion and emotions towards the building were predetermined by the photographer. If a more realistic style had been practiced, I may have had more room to determine my opinion of the space.


I am also drawn towards Phillip Gerou's definition of architectural ethics and feel there is a correlation to these two approaches to photography. I cannot help but think that it is more ethical to have the realistic version than to cover imperfections within the photograph. It seems dishonest to hide such imperfections and realities from the viewers' eye. While I do not agree entirely with Lebbeus, I can see where his thought process is being derived from.





	1. mailinator
	7.19.11 / 2pm
	
	
	Baan frequently includes the messy details, however his portrayals may not be as raw as you think they are; in fact many of his photographs of [this](http://www.iwan.com/photo_Guangzhou_Opera_House_China_Zaha_Hadid_Patrik_Shumacher.php?plaat=3Guangzhou-Opera-ZHA-7418.jpg) [same](http://www.iwan.com/photo_Guangzhou_Opera_House_China_Zaha_Hadid_Patrik_Shumacher.php?plaat=Guangzhou-Opera-ZHA-5834.jpg) [building](http://www.iwan.com/photo_Guangzhou_Opera_House_China_Zaha_Hadid_Patrik_Shumacher.php?plaat=Guangzhou-Opera-ZHA-5860.jpg) give the same emptied, purist impression as Richters' (his interiors do tend to be more populated, though).  
	
	Even the ‘messier' of his photos are still highly processed and highly aestheticized. Never make the mistake that any image you come across is without any illusions. Even the wide angle lens of the photographs (a technique common in Baan's photography as well) which seems innocuous changes the presentation of the space in an intentional way, here contributing to the ‘rendered' appearance. 
	
	
	I suspect much of the ‘rendered' quality is simply the result of the technical requirements of long-exposure photography, which makes it difficult to include naturally posed people, softens shadows, and gives ambient light that glowing feel.
71. RS
5.3.11 / 7pm


‘Zaha's Way' has not been or never will be traditional or easy. I admire Zaha in a way that she has always stood for what she wants, all through her career. She dared to move out of the box and show us ways in which we can do things differently from what we are doing now. Besides, what exactly is THE traditional way? Designing has always evolved and been evolving all through the life of architecture. When Frank Lloyd Wright, Le Corbusier, or Antonio Gaudi started designing buildings in ‘extraordinary' ways for the time, there were people who supported it, who were inspired by it, who wanted to start something from where they stopped. That stimulated a whole new revolution of how we perceive design. We know art and we know architecture. Always they work hand in hand in producing masterpieces. These new technologies of parametric modeling and computer renditions are enabling the two disciplines work more strongly together in an entirely new way. Curves, straight lines, and huge volumes composed in a very elegant and flowing way. Maria Novak (one of the panelist in my class) mentioned architecture is art with a purpose, which can be proved through these evolutions of design.  

The works of Zaha have opened up a lot of possibilities for building design using parametric modeling tools. When Lebbeus Woods mentioned pure architecture, it makes me wonder what exactly ‘pure architecture' is. Is it just giving simple forms and shapes to buildings? Carrying on the traditional ways of how others worked? There was a time when architects did not have the technology to experiment with structures, with forms, with strange shapes, as we do now. Now we have it. We have what we need to figure out where a building can fail, even before we build it. In all other professions, incorporating whats brand new in the field is happening. Renderings, eccentric forms, etc. are all results of latest advanced technological in the field of architecture. Appreciating them can open new possibilities and opportunities for all of us. Computer renditions are increasingly becoming a part of the design process. The better quality renderings are, the more attracted clients and audiences are going to be. These show more details and perspectives about how a building is going to be which was never possible with hand drawings.
73. [Lebbeus Woods « Tom Clark](http://cocatopia.wordpress.com/2011/09/09/lebbeus-woods/)
9.9.11 / 1pm


[…] <https://lebbeuswoods.wordpress.com/2011/03/27/zahas-way/> […]
