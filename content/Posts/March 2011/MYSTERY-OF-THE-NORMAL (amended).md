
---
title: MYSTERY OF THE NORMAL (amended)
date: 2011-03-04 00:00:00
---

# MYSTERY OF THE NORMAL (amended)


Ross Racine's computer drawings of imaginary or perhaps ideal suburbs struck me as profound, in a trivial sort of way. The trivial part is the obviousness of their contrivance—they have a jokey, one-liner sort of message alluding to the absurdity of tract layouts that aim for interest even as they invoke the boredom of predictablity. The profound part is their revelation of a human mystery to be found precisely in the obvious, where no one would expect to find it. I'll try to explain.


Artists and poets have struggled over the centuries to make works that startle us with their originality and, in effect, wake us up to depth of human feelings in our own uniqueness and individuality. The artist's and the poet's originality connects with our own, invoking the feeling that to be human is to be unique. The artist is a mirror of ourselves, inspiring us not to be artists but individuals, shaping our lives much the same as an artist shapes a block of marble or a blank canvas.


But the raw fact is, most of us are not so unique. Our lives, except for the smallest details, pretty much resemble the lives of others, particularly those in our social group, whatever it might be, defined by economic class, race, educational background and many others. The truth is that we are intensely social creatures and our social context often overwhelms our individual traits and aspirations. This would seem to be the message imbedded in Racine's drawings of suburbs.


Still, the mystery of these diagrams (for that is what they are) is to be found precisely in what is missing from them: the machinations of human desire and hope, which varies—if ever so slightly—from person to person.


Because each human creature is different from all others—think of DNA and each life's story—the ordinary and the normal are ultimately revealed to be paradigms that lead us away from the actuality of the human condition. Racine's drawings, through the extremity of their normality, bring us back. For this reason, they have a profoundly powerful, unforgettable effect.


LW


[![](media/rossracine-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-4.jpg)


.


[![](media/rossracine-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-2.jpg)


.


[![](media/rossracine-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-3.jpg)


.


[![](media/rossracine-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-1.jpg)


*(above) *Drawings by Ross Racine. Go to see [this website](http://www.bauzeitgeist.blogspot.com/) for the source of their publication and the context of a critical discussion.**


**(below)  *Color added by LW, without permission of Ross Racine. Hopefully, you, dear readers, and he will accept my intervention in his work,  for this blog only. My intention is take them one step further from abstraction and into the normal:***


[![](media/rossracine-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-4a.jpg)


.


[![](media/rossracine-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-2a.jpg)


.


[![](media/rossracine-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-3a.jpg)


.


[![](media/rossracine-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/rossracine-1a.jpg)





 ## Comments 
1. Steven Shimamoto
3.4.11 / 2am


this is very interesting. i agree that we are very much social creatures just as much as we are solitary throughout the course of our lives. We tend to think of ourselves as unique individuals within a whole, but in reality, our lives are only a tiny pixel in the scope of everything. We are a functioning cog in this great machine of man on his path to self destruction, or perhaps something greater(?). And so we decorate our lives with philosophy, architecture, math, science, art whatever to cope with the uncertainty of our existence; to give meaning to the blank slates that are our lives. At the end of the life, does it really matter? In our dying moment, does it matter if we were a world class architect or a garbage truck driver? I don't know. And so we go about our lives, searching for new social connections even if they may be only transient. I'm not really sure where I'm going with this, but I just felt like writing.





	1. ayuna mitupova
	3.11.11 / 9pm
	
	
	i think theres something more beyond the neccesity of fillinf the gap of our existence. and it s perhaps so much evitable as undescribable..maybe its more about observance, conciousness, intellection, that make it all the diffrence. plus we should never forget about the thousands millions chances of the coincedence in our every second of life. that creates and affects also a lot in our personalities.  
	
	 so to say ///// maybe the words solitary and socila are not the appropriate words to desribe human being. they are not precise in their meanings and they are also unavoidable features of people. and art philosophy and /////// matters.
3. [Dare You Be Different? Can You Be Different? « Prison Photography](http://prisonphotography.wordpress.com/2011/03/04/dare-you-be-different-can-you-be-different/)
3.4.11 / 7am


[…] On Ross Racine‘s digital drawings, Woods says: […]
5. [gerridavis](http://gerridavis.wordpress.com)
3.13.11 / 9pm


(apologies, the second sentence of the previous post should have been deleted:)


Thank you for posting these drawings and for your insight.


In making we destroy, whether it's making our bodies by consuming plants and animals or any other, larger, projection of that basic function – from drawing to building neighborhoods and cities.


It seems to me that this ‘machine of man' to use Steven's phrase, is a giant replica of the bodies we are born into, a symbolic enlargement of our selves. And after all, isn't everything we do just a futile attempt to defy death? Isn't the idea of uniqueness that artists offer a sort of light crack in the wall of the coffin we all would love to find our way out of? But in the end, we are the same as any other living thing, bacteria for instance, which come to mind when looking at these drawings. I wonder if ringworm struggles with identity as it organizes itself into consuming loops, as well?





	1. Steven Shimamoto
	3.20.11 / 2am
	
	
	Nice. I agree that everything we do is an attempt to defy death. We are defying the earth's state of equilibrium by creating large populations that necessitate the production of commodities to consume. There is no Polis in the suburbs. There is no coherent effort toward the natural; the nomos. Streets that lead snake to nowhere and dead-end at culdesacs, nostalgic, prairie style mass produced housing projects that have no architectural value…I guess this is America, and there is nothing the Architect can do about our diminished state of existence in the suburbs.
7. [Oliver](http://baufunk.blogspot.com)
3.14.11 / 1pm


Perhaps the 21st century will be one of extraordinary ordinary, instead of the ordinary extraordinary of the late 20th century?


Wrote about this some time ago (shameless plug): <http://dronelog.tumblr.com/post/498499938/ordinary-the-common-the-usual-the-boring>





	1. Pedro Esteban
	3.23.11 / 9pm
	
	
	so let's do it extraordinary!!!!!!
