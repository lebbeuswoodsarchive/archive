
---
title: SPILLER’S WORLD
date: 2011-03-19 00:00:00 
tags: 
    - architecture
    - concepts
    - drawing
---

# SPILLER'S WORLD


For [Neil Spiller](http://www.bartlett.ucl.ac.uk/otherhostedsites/avatar/spiller.html), drawing is thinking. He does not ‘express' thoughts already formulated, in which case his drawings would be mere illustrations. Rather, he formulates thoughts through drawing, indeed by drawing. Each line, each tone is a word. Their groupings are sentences. Their total ensemble is an essay, presenting a fully formed thought. However inadequate this analogy may be to suggest the fullness of Spiller's works, it does underscore their strong conceptual nature and what I believe are his intentions for them. Nor does the analogy detract from their lyrical, evocative mood; again, it underscores it in that the thoughts realized in the drawings are complex and subtle ones that defy simple explanation or illustration. In a sense, they are thoughts that can only be formulated by the means he has chosen, that is, by drawing and indeed by exactly the drawings he has made.


These statements may seem to form a tautology, a loop of logic closed in on itself. As such, they would present Spiller's drawings as self-referential, self-contained and solipsistic, constructing a private world lacking any accessible meaning. Happily, this is not the case. Their relevant content resides in two aspects: first, the precise character of the marks he makes and their interplay; and, second, the character of the forms, objects, fields, and other things they bring into being.


The variety of marks he makes is truly astonishing. Lines thick and thin, from delicate to bold, from dense, hard black to soft, ethereal gray, precise straight lines and nervously jittery lines, lines that group into recognizable and abstract shapes, lines standing, or moving, alone. Their effect in any of the drawings is encyclopedic: of a richly depicted, subtly inflected linear world, in which tone and color most often play subordinate roles. We feel, upon entering the drawings, as though we have found an entire world, whose exploration will take us away from our familiar one, but eventually bring us back to it, our perceptions enriched, our imaginations stimulated and expanded, the better to appreciate the familiar in new ways.


Spiller's world includes much of the familiar—boundaries, edges, limits, creating forms we half or fully recognize. Then there are the mysterious forms, the ones we don't recognize at all. Bringing them all together to form a continuous landscape suggests above all else a transformation—the familiar past will become the unfamiliar future. What we know will change, sometimes slowly, often quickly, into what we do not know. Spiller's drawings are unsettling, even frightening. He presents us with a world we must work at to navigate. Rationality and emotion are needed in equal measure and will meet in our imaginations. The sheer beauty—or ugliness—of the drawings seduces us to try, to match his creative efforts with our own. This brings the drawings firmly into the domain of architecture and far from that of art. The architect has designed spaces for us to inhabit, rather than objects for us to appreciate from outside.


Spiller has long been a champion, through his [published books](http://www.amazon.com/Neil-Spiller/e/B001IXPRLK) and articles, of what he and others call ‘visionary architecture'—a term I don't like because it cuts off the most radical design concepts from the main body of architecture, where I believe they belong. Still, his advocacy has encouraged many, mostly younger, architects, to take the risk of re-imagining what architecture is or might become. In this latest group of drawings he practices what he preaches, and more, opens up new prospects for drawing in relation to architecture.


LW


*Cenetic gazebo:*


[![](media/genetic-gazebo-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/genetic-gazebo-1.jpg)


*Baronesses filaments:*


[![](media/baronesses-filaments-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/baronesses-filaments-1.jpg)


*Two queens:*


[![](media/two-queens-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/two-queens-1.jpg)


*Aughtman's Square 1:*


[![](media/dr-aughtmans-square1-1.png)](https://lebbeuswoods.files.wordpress.com/2011/03/dr-aughtmans-square1-1.png)


*Homage a courbet:*


[![](media/homage-a-courbet-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/homage-a-courbet-1.jpg)


*Ga-twistedchrist:*


[![](media/ga-twistedchrist-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/ga-twistedchrist-1.jpg)


*Growing vista stage 11:*


[![](media/growing-vista-stage-11-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/growing-vista-stage-11-1.jpg)


*Vista section:*


[![](media/vista-section-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/vista-section-1.jpg)


*Untitled 5:*


[![](media/untitled-5-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/untitled-5-1.jpg)


*Com ves site plan:*


[![](media/com-ves-site-plan-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/03/com-ves-site-plan-1.jpg)


*Analysis of Beauty (Part One):*


[![](media/analysis-of-beauty-part-one1-1.jpg)1-1")](https://lebbeuswoods.files.wordpress.com/2011/03/analysis-of-beauty-part-one1-1.jpg)


*Analysis of Beauty (Part Two):*


[![](media/analysis-of-beauty-part-two1-1.jpg)1-1")](https://lebbeuswoods.files.wordpress.com/2011/03/analysis-of-beauty-part-two1-1.jpg)


.


*NEIL SPILLER is the Head of the School of Architecture and Construction, the University of Greenwich, UK.*


#architecture #concepts #drawing
 ## Comments 
1. Gio
3.22.11 / 6pm


Beautiful drawings!  

Certainly new.


I can see his sencibility and love for the process of creating a thought on paper.


His analysis like you said it, and his superimpositions and hybrids of languages and images in lines, shades, zones, and tones are very precise but free at the same time.  

Architecture needs these explorations and yours in order to advance.


There is something of Beuys and Duchamp ( without reducing it just to that ) in his way of thinking.


If I am not wrong, I think he is the Dean of the Barlett School of Architecture in London. 


Thanks for posting this work I really enjoyed it.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.23.11 / 11am
	
	
	Gio: Neil Spiller left the Bartlett, where he was Vice-Dean, to become Head (Dean) of the School of Architecture at the University of Greenwich (in London). last September.
3. [chris teeter](http://www.metamechanics.com)
3.25.11 / 2am


I have been trying to find more writings by Neil on the web but not much luck. Based on what I did read and what you present here in both graphic and essay presentation, I see something else going on, quite different than what maybe we should think he and the older generation is doing and saying about contemporary architecture and its tools.


Maybe this is my generalized view of  

not only older generations but also of humanity…


It seems Neil Spiller is directing us to escape the technological marching machine we sub-consciously lead via technology itself in a both surreal and extremely practical and real understandings of the tool (the computer) itself. Its obvious to me Neil Spiller understands the computer in architecture like very few people of his generation can, but it also seems he is cautioning us as we mindlessly get led by machines – but who is really leading the machines? Neil spiller describes using algorithms in a discovery exploratory manner, but by definition the only discovery being made when letting algorithms run loose is our lack of comprehending formulae and predictable events…


What I am deriving from all this reminds me of a qoute in a Kurt Vonnegut book (breakfast of champions? -maybe) where in short Vonnegut says we made machines do what we do and we made them so good at what we do, namely war , that the machines then got rid of us.


I get this, this longing for warm fuzzy feeling social being human thing, and clearly so many artists and architects express this as desired, but aren't we just avoiding the fact that the technology and its drive is an extension of our drive – we are the technological war machine.


I think the Futurists expressed this well, and Lebbeus many of your drawings for me at least evoke the destruction of the machine…  

My question is – can I get a list of the architects riding this machine? I don't think the Delanda, deleuze, or even Kwinter camp are riding the machine, I think they too are looking for a way out through nomad thinking.


Thanks in advance.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.25.11 / 11am
	
	
	chris: the questions you raise so astutely are crucial ones. I'll give you my response to them is the next day or so—just wanted to thank you and to let you know I'm on it.
	3. Gio
	3.30.11 / 3pm
	
	
	Hi Tom,
	
	
	I like your reading of these drawings.
	
	
	If I understood you correctly, Is Neil Spiller trying to caution us about the technological advances and uses of the machine-computer as a tool for the only way to arrive a new forms by intentionally creating and using the language-image of a crafted drawn computer image by hand?  
	
	I can see that all of them with the exception of two drawings at the end where definitely created by hand.
	
	
	I am believing more and more, than at the end, your inner interpretations and understandings of an era and their advances in all the fields,  
	
	that eventually create the forms,languages and programs for the Architecture of an era.  
	
	I saw the same language and end result in the work of two artists at different times, Duchamp and Beuys. Both understood the advances of multiple fields, mathematics, science, chemestry, physics, poetry, philosophy etc….
	
	
	Yes, they both have walked through the surreailist fields and produced images and spaces with that character as well.  
	
	Yes, I believe in the aid of the computer and an extention of our self but no as the only way and final.
	
	
	Now, what causes me to think about these drawings are these forms and compositions as if they live in a foreigner  
	
	cybernetic space which conspicuously enough are created by hand.
	
	
	
	
	
		1. Gio
		4.2.11 / 2am
		
		
		My replay was to Chris and not Tom.
		
		
		Sorry Tom.
		3. [Chris Teeter](http://www.metamechanics.com)
		4.7.11 / 3am
		
		
		i agree gio. where are we going to live? many seem to be falling into the cybernetic space and never returning.  
		
		if culture materialize in material form in architecture (kwinter reference) then how does culture dematerialize in cyber space?
		5. Gio
		4.8.11 / 8pm
		
		
		Chris,
		
		
		I think you will like the writings of Ananda Mitra- “From cyber space to cybernetic space.”
		
		
		This is my interpretation of what she said:
		
		
		She differentiates cybernetic space from cyber space.
		
		
		Cybernetic space is the condition and interaction of the human with a tool or tools at multiple instantaneous locations that enable us to emerge ourself in to cyber space or the WWW. As we all know it cyber space is filled with billions of numerical combinations that form images and letters that create information and so on….
		
		
		With this light in mind, I believe now more than Spillier's drawings are the manifestations of these spaces where the human and the tool create a point of entrance into cyber space. 
		
		
		I really like this post by Mr. Woods.
5. tom
3.26.11 / 6am


Coming more from the drawing field that the architectural field, these drawings are filled with cliches of the hand that artists try to rid their work of.  

I don't know but, I think architects get put in the position of working in their heads so much that they yearn for the open pastures of a piece of paper, over the confines of the computer screen; but the grass is not always greener, nor whiter.  

I guess I'm mostly reacting to the antique patina that most of the drawings have, and the collaged juxtapositions cribbed from surrealism; the wooden figure and classical statuary.  

The hybrid bio/ geometric are not so interesting that they verge on Omni magazine illustrations.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.26.11 / 11pm
	
	
	tom: I appreciate your comment, especially because it is critical of Spiller's drawings. But I must say that I am always suspicious of criticism—con or pro—that relies on comparing the works in question with other works or references. It is a too-tricky way to avoid confronting the works themselves and what they ARE. The comparison approach of your comment is not criticism of the best kind, which analyzes particulars and totalities and, perhaps, finds them wanting. Its not easy and takes time and thought. Your comment, I hate to say, is little more than a dressed-up way of saying you don't like Spiller's drawings. Fair enough. I respect that. But don't imagine you've said anything more.
7. tom
3.27.11 / 3am


Appreciate the comment very much and will keep it n mind as good advice.
9. [chris teeter](http://www.metamechanics.com)
3.29.11 / 1am


Any luck? I have been thinking about this some more and on other sites people were discussing the Futurist's and the failure of their thinking in material form. My first thoughts were who is doing virtual architecture in the form of internet social interaction based on the movement of capitalism and thought that was too easy as I remembered many Paul Virilio and Jean Baudrilliard essays where is short space has been erased as everything has become so fast. I also remembered ctheory.net, late 90's theory site(Thinking out loud). Maybe that is it, media is the machine now and more specifically instantanous communication of information witnessed live virtually? Or who are the modern day futurists in architecture? I know kwinter starts with sant e'lia, put I think ultimately all architecture theory ends with a view against the machine.  

Is being a futurist in architecture today even still being an architect? Thanks for getting my brain moving as usual. – c
11. Roger Kopet
3.29.11 / 9pm


as seriouss an undertaking that it is, to document ones epoch through ones chosen means of communication, It becomes clear here, in the work of Mr. Spiller, that Technology and it's role in culture, is given it's proper place as “tool” and not generator of form. There is no “I can, therefore, I shall” mentality here. I find the quantitative material, efficiently expressed with little regard to “form for form sake”. The “excessively complex forms, being used today in an “Overwrought” and self consciouss exhuberance, is at once revealed for what it is. “CAD gone wild”. The critical thinking and Rational Inventory involved in these works gives me pause for the appreciation that the mind is far more complex than any superficial CAD model could ever substantially convey. This is a minds work, worthy of documenting it's own epoch. Ideas still mean something.
13. Caleb Crawford
4.1.11 / 1am


“In my own case, the process is more or less unvarying. I begin with the glimpse of a form, a kind of remote island, which will eventually be a story or a poem. I see the end and I see the beginning, but not what is in between. That is gradually revealed to me, when the stars or chance are propitious. More than once, I have to retrace my steps by way of the shadows. I try to interfere as little as possible in the evolution of the work. I do not want it to be distorted by my opinions, which are the most trivial things about us. The notion of art as compromise is a simplification, for no one knows entirely what he is doing.”  

Jorge Luis Borges  

I've always loved this quote. It seems to express the act of making (at least it strikes a chord with me). Might I suggest that Spiller's drawings are somewhere between the known and the unknown? They must start somewhere. They seem to be a kind of conversation, perhaps initiated by a found image. And like any good conversation, it exuberantly feeds upon itself. I'd love to see them in person.
15. Monica Effendy
4.16.11 / 8pm


fantastic drawing….  

a new futurist!  

thanks for sharing it!
