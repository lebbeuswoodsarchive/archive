
---
title: THE POLITICS OF ABSTRACTION
date: 2008-10-23 00:00:00
---

# THE POLITICS OF ABSTRACTION



Representation occurs when one thing, say, colors smeared on a canvas, makes us think of something else, say, a mountain valley at sunset. Abstraction occurs when one thing, say, colors smeared on a canvas, makes us think of, well, colors smeared on a canvas.


Abstraction is shapes, lines, colors that represent only themselves—we might call it a special case of representation. Or, we might (as is most common) say it is a case of non-representation. Abstraction does not re-present anything. This does not mean, however, that it has no meaning, but only that its meaning lies in its own substance and structure. An abstract painting may evoke an idea, say, of movement, or of energy, as is often the case with Abstract Expressionist works. But this is only because most of us demand that a work of art ‘means' something. A mathematical equation, such as E=mc2, is symbolic of a basic relationship between energy and matter, as opposed to a representational description of that relationship. As a symbolic expression, it is more scientifically useful than a picture of an atomic explosion, which re-presents that same relationship, but only in terms of its effects. Most of us would agree, however, that a movie of a fireball and mushroom cloud rising over the desert or the city of Hiroshima, gets the point across more vividly than the equation. The effects are what are important, or at least more understandable, to most people.


Architecture and music are the most abstract of the arts. They are both rooted in precise mathematical relationships played out across space and time. Still, their abstractness can become representational when it starts to symbolize something. What does a Gothic cathedral symbolize? Technological progress? The victory of ideas over matter? Or, the power of religion? The importance of piety? For most people, it will be the latter more than the former, and in a quite literal way: the Gothic cathedral and all the buildings modeled on it, are didactic symbols of devotion to Christianity. So, too, a Bach Cantata cannot be heard entirely apart from its intended setting. Even the most agnostic listeners will feel the force of its religious passion.


What is the symbolism of a Modernist icon such as the Tugendhat house? Technological progress? The victory of ideas over matter? Or…what? However wide its influence on the design of houses and other buildings that followed (and it was considerable), this seminal work of architecture somehow resists being symbolic. To the contrary, it remains emphatically abstract, even to those who know about it. Beethoven's late piano sonatas fall into the same category. They move us for what they are and not for what they represent.


It is fairly clear that political systems relying on propaganda to control the way people think and feel can make the most effective use of representational music and architecture. When the national anthem is played, everyone knows what it means and responds on cue. The same with the Capitol in Washington, D.C., or the Arc de Triomphe in Paris. Abstract works cannot be used to the same ends. For example, the revolutionary Communist regime in the USSR, which for a brief period promoted an ideal of egalitarianism, encouraged the uncompromising abstractions of Constructivism and Suprematism. However, when this regime became a rightist autocracy, it tolerated only Social Realism—even Malevich started painting colorful images of peasants—and a Stalinist version of classical architecture. The same in Germany. During the liberal Weimar years, abstract art and architecture flourished, but, when the ultra-right Nazis took power, abstraction became ‘degenerate,' and representational design was the only acceptable possibility: the classicism of Speer, which symbolized, via Greece and Rome, Germany's historically superior moral authority over its decadent enemies; or, the mock-rural styles of houses and other buildings that made the same point by extolling Germany's honest peasantry. In Rome itself, during the same period, we discover that the Rationalist movement in architecture was encouraged by Socialists, one of whom was Mussolini. The abstractions of Futurism and Terragni's *astrattisti comaschi* were, however, pushed aside by Novecento art and a vaguely classical Fascist architecture, when Mussolini took a sharp right turn to absolute dictatorship.


Rightist, totalitarian rule needs to constantly enflame people's emotions with conflicting fears and hopes that can only be resolved in the person of a charismatic Fürher, Duce, Maximum Leader, or Big Brother. Democracy and egalitarian systems employ propaganda, but it usually has a different message: think, understand, choose. Unlike autocracies, democracies rely on the intelligence of their people, and believe that through education (as opposed to training) and the freedom to make difficult choices, people will act wisely in their collective self-interest. Here, abstract art and architecture—which demand personal interpretation and inventive adaptation to change—are appropriate challenges.


In the USA, which is arguably more egalitarian in its aspirations than many other societies, abstraction has had a hard time taking root, not to say becoming widely popular. Nevertheless the USA has been a sympathetic environment for artists, composers, and architects of radical abstraction, from Pollock to Rothko, from Ives to Cage, from Neutra to Bunshaft. Of course, abstraction is in retreat at the moment, as American politics has moved relentlessly to the right. It is no surprise that picturesque post-Modernism emerged during the reactionary Nixon years and flourished under the charismatic leadership of Reagan, and has never retreated, even though its forms have mutated, on occasion mimicking even those of egalitarian Modernism.


Today, there is a nostalgia for the abstractions of Modernism that amounts to reducing it to a mere style among many others. That appears to be finely democratic with regard to offering choices, but it is a false choice if the abstractions resemble the old ones, which have by now become comfortable representations of a lost innocence. Abstraction, like democracy, is demanding of each of us—often discomfiting, and always difficult to attain.


LW


 




 ## Comments 
1. david
10.24.08 / 2am


I believe you are making a few false jumps here, leading to a strange conclusion.


1. Abstraction is not intrisically “non-representational.” In the graphic arts, abstraction has come to mean BOTH “to represent a thing in a way other than visual mimicry,” AND “to represent nothing external.” These two definitions of abstraction are consistently ignored, but their difference is huge. (say, Mondrian and LeWitt).


2. There is a false dichotomy implied between right/totalitarian and left/democratic. In fact, the time period in which modernism arose was one in which totalitarianism on both left AND right was prominent. So are we trying to say that abstraction = left politics? Or abstraction = totalitarianism? Or representational arts = democracy?


3. “Architecture and music are the most abstract of the arts.” Really? Architecture (using its common definition [real buildings]) seems to me the *least*  abstract of arts. It often signifies its function, or grows to over time. There is, of course the issue of architecture's conveyance of “ideas.” I would posit that that abstraction derives from these ideas (democracy, justice, stability, etc), not the medium. While architecture does often take on this abstract role, we must remember that this is already it's secondary representational activity. It has already signified its program, and nothing could be less abstract.


4. Do you expect us to accept that the Washington Monument, for instance, is not abstract? Or does an abstract form, once it is used in history, cease to be abstract? Does the monument signify Egypt or phallic strength? Or both? There is no getting around the fact that Modernist Architecture is in the same boat as the obelisk. What was a simple abstracted form is now forever married to a historical moment. Some feel nostalgic for Modernism, in the same way that the Italians of the quatrocento felt nostalgia for Rome. It's just that the political success of the Romans far exceeded that of Stein, Savoye, or Tugendhat. That's why the political message of the latter forms are more ambiguous.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
10.24.08 / 4pm


David: It's not so much that what you say is wrong—I'm in agreement on several of your points—but that if we follow the threads of your thinking, we end up back where we are today, in an aesthetic-ethical confusion. What I'm looking for here is a position that helps us sort out the differences—politically and aesthetically—that have shaped and are shaping our thinking, our field of knowledge and practice, our world. 


I'm not being as formulaic as you suggest. The political left can be anti-democratic, as The Communinst Manifesto proves, with its proclamation of a Dictatorship of the Proletariat. The political right can support individual freedoms, as in the idea of a free market. And, obviously, it takes a melding of left and right to achieve a balanced poltical state. However, abstraction is (I argue) the ideal aesthetic mode of democracy, because it demands our participation in giving meaning, and does not, by re-presentation of what we already know, give us a predetermined meaning, or allow us to have knee-jerk reactions. 


Your point about the Washington Monument: of course, everything we make is abstract. But sometimes, when things we make acquire a commonly agreed upon symbolism, they become more representational and less abstract and interpretable. By the same ‘of course,' everything we make represents something else: your reference to semiotic ‘signifiers' and the ‘signified.' Acknowledgement of these ‘of courses' does not get us anywhere we haven't already been. What I'm looking for is a more fruitful way of thinking that will help us move forward.


Not incidentally, the only way architecture can signify its program is if it follows (re-presents) familiar typological models, in which case, its inherent power as an abstraction is greatly diminished, indeed.
5. Adam
10.24.08 / 6pm


How does architecture cease to be an abstraction if it is also seen as containing representational content?
7. Marc K
10.25.08 / 7am


The most thoughtful composers of history: Mozart, Beethoven, Bach, etc., were men of thoughtful, qualitative, design. And their work reflected such. As Architects, we, at least those of us who are learning, are taught to become those thoughtful, determined designers. 


However, some of the greatest musicians ever: John Coltrane, Dizzy Gillespie, Wes Montgomery, etc. are known increasingly for their ability to compose, but more so their ability to improvise, and create stream of consciousness music (Coltrane in particular). As a student who studied jazz, I was taught to practice this means of improvisation, and was heralded when I demonstrated my ability to create phrases and ideas through playing.


As an Architecture student, I am hardly encouraged to do so. If this strong connection between Architecture and music (as creative or representative arts) exists, why then are the conceptual or representational parts of making subject to commentaries on a piece's arbitrariness or amount of informed moves?


In short, can Architectures be improvised in the sense of a Coltrane solo?
9. [Penelope](http://www.arct.cam.ac.uk/powerspaceconference)
10.25.08 / 11am


Dear Lebbeus 


Thank you so much for continuing to be so stimulating and for providing an open space for dialogue. 


I wanted to add to this discussion another aesthetic stance (which I am not sure how to name it) that is political in its intention and effect.


There has been lately a growing culture of the street art. The celebrity in London of the street artists is Banksy . His work is a political act that intervenes into the public domain and asks us to see or experience a place in a different way. It is an intervention outside the gallery or any other art institution that makes a critique of an existing reality, and this intervention is made available to everyone. 


Gordon Matta Clark's work falls in this category and not only. Wood's early works which were less abstract than his latest ones are interventions into existing buildings and existing cities, that propose a different way of living and interacting with them. In this aesthetic category we might include the Dada movement as well? An ordinary object out of his ‘known' context was turned into something else by acquiring a different meaning. 


This aesthetic stance can actually bring in this discussion previous ones of this forum, like the ‘Cities without Names' and ‘Stonehouse'. All three discussions celebrate the openness of ‘unknownability'. That we do not take for granted what we know about an object, or an existing reality, but we question it by intervening to it. This allows us to perpetually invent; invent a new meaning in a painting, a new way of living in a house, or in a city. 


How could we name this aesthetic stance, which is not necessarily abstract or re-presentational in its nature?
11. Brad
10.25.08 / 4pm


Great point of exploration.


One might also consider the act of experiencing art which is, I believe, what you are getting at. Experiencing fascist-end-of-the-spectrum art is a passive, non-participatory act. It gives you meaning. Work that requires participation and engagement allows the viewer/participant to determine their own meaning, and is therefore despite claims otherwise, more meaningfully populist (in a good way).


It is perhaps the sad characteristic of a consumer society that art experiences are expected to be consumable rather than participated in.


I suspect, however, that good art and architecture can (and maybe should) offer both a titillating, consumable experience as well as offer greater degrees of meaning for the willing participant. The sugar on the pill or something like that. Think Tom Friedman, Tim Haakensen, etc.
13. [lebbeuswoods](http://www.lebbeuswoods.net)
10.25.08 / 5pm


Marc K: In my view, in the best of all possible worlds, new architecture would be improvised in a way that combines—analogous with musical improvisation—the acts of design and construction. But it would also, as with music, be transitory. This would enable a new kind of society that, like such an architecture, would be lighter, more dynamic, freer and more playful than the present one. The role of the architect would be to design the rules of different modes of improvisation, and to use them to make some leading examples—very similar to the musicians you mention. 


As such a best possible world would also include the buildings of other, different types of society, the two would necessarily collide, sometimes fuse, but otherwise interact and synthesize. A very lively and complex world, indeed. Perhaps one toward which we're slowly moving.


Some contemporary music examples are dj-remixers who literally weave together Coltrane and Mozart, and much else. In architecture, I can only think of my own work for cities of crisis—Berlin, Sarajevo, Havana—but also more stable centers such as Paris and Vienna, where social conditions demand extraordinary methods and techniques to enable new social and spatial forms struggling to emerge. 


For the earlier projects, I coined the term ‘freespace'—a term, and an architecture, that didn't exactly catch on….
15. [lebbeuswoods](http://www.lebbeuswoods.net)
10.25.08 / 6pm


Penelope: My comments to Marc K (above) were also stimulated by your comments here. Any really creative artist —whether ‘street' or ‘gallery'—changes the rules of the game and creates models/examples that inspire others to use them for their own purposes, and even to change the rules once again.
17. [lebbeuswoods](http://www.lebbeuswoods.net)
10.25.08 / 6pm


Yes, Brad, participation is a keyword. It's more than interesting that you mention ‘consumption' and consumerism. It's easier to sell ‘pre-cooked' and ‘prepared' food and ‘ready-to-wear' clothing, so that's what the consumer society does to maximize profits. The same with most architecture: pre-digested ideas in the form of familiar building types, where we all know how to behave, not really participate.


You won't be surprised to learn that, some years ago, I gave a lecture on my “Theory of Indigestibility.” The title pretty well sums it up.
19. [Penelope](http://www.arct.cam.ac.uk/powerspaceconference)
10.25.08 / 7pm


Participation could be linked with notions of ambiguity (an object/space that is not pre-determined in its program or form and therefore allows for exploration and playful engagement) and the unfinished (where the user/participant physically changes the space he/she inhabits).
21. david
10.26.08 / 2am


Graffiti is the perfect real-life example of the ideas discussed here. as it has always been representative, abstractive of representation, and consciously self-referential. 


However, in my view, the true issue we are discussing is: 


“Does the mode of creation (whether something is abstract or representative) affect the power structure under which the creation happens? Or does power express itself in all sorts of modes, in the interest of self-preservation?” Really rolls of the tongue, no?


By its very nature, graffiti violates basic notions of property rights and public space. By doing this, street artists are seemingly able to place their message outside the realm of existing power structures. So if power structures (laws, manners, style, common decency, the “idea of art”, etc) are conceived as a glass bubble, graffiti is a dancing monkey outside the bubble, beckoning us to walk right out, since the bubble exists only in our minds!


But is that true?


First, as graffiti becomes ubiqutous, its message is diluted. That is, if our cities are covered in graffiti, how far outside the bounds of power could it really lie? 


What graffiti does demonstrate, though, is the power of abstraction. The few representational pieces of street art can be enjoyed, perhaps, from an aesthetic point of view, but they pale in comparison to the secret world and language that is both hidden and hinted at by the myriad of incomprehensible graffiti. Incomprehensible to me, anyway. The very fact that there is a widespread common visual language hiding before our very eyes suggests an alternate/underground power structure. 


But, can this power be exercised without revolution? It seems to me that the underground power that graffiti points to emerges when those who control planes and tanks are disorganized. This, of course, demonstrates the centrality of Mr. Woods' crisis projects.


When an underground (aesthetic) power tries to emerge within stable (real) power structures such as those in the UK or US, it can only become another commodity, as Banksy has. 


Before we lament the lack of participation in “art experiences,” though, lets have a bit of perspective. There are plenty of parts of life that are highly participatory. The internet, the academic world, and for many, their professions and their faith. Just because some people are really into going to white-painted galleries and gawking at paintings and sculptures does not mean all art is non-participatory. Or that it is any less participatory than it has ever been. 


I guess I mean that I don't think the New York art world is non-participatory because it is capitalist. I think that New York has non-participatory art (whatever that means) *for some of the same reasons* that it is capitalist. Those reasons, we call culture. 


The Thais tend to have lots of participatory public festivals, while those in New Hampshire don't. Is this because the Thais are
23. david
10.26.08 / 2am


Oh, I almost forgot to respond to Mr Woods: 


You wrote that “the only way architecture can signify its program is if it follows (re-presents) familiar typological models, in which case, its inherent power as an abstraction is greatly diminished, indeed.”


That's not quite right. Architecture can signify its program in a far simply way than referring to typological precedents. It can *simply, literally reveal* the program. Usually through glass or by being outside. Referring to past typologies is a second-level signification, which often only works for those who have studied architecture. That's why buildings in the modern world all have signs.
25. river
10.26.08 / 5am


You [rawk](http://hcz.org/) kin street level. Bypassing abstractions of our lingua Roma. And undermining the definitions of a plurality of words I had to learn gooder to keep up with my old school bud's constant vocab tests!
27. [slothglut](http://fairdkun.multiply.com)
10.26.08 / 6am


one question suddenly popped up in my head this morning:


money, both coins + prints as well as the concept that impregnate it with fluctuating value: representational or abstract? [just looking for an insight to attack this never ending problem of the modern human existence.]


then, another one did:


would it be easier to explain the difference between the two concepts this way?  

take recycled products – recycled paper sheets + wallets made out of used plastic wrappings: one was made out of an abstract thinking – starting the process of thinking on a matter's own intrinsic values; while another was made out of more a representational one, the looks of these plastic wrappings being thread together more than what they were made of.  

this brings us to the consequence of finding the ‘selling point' of each product: the first would be using statements/symbols/logos that notify that this is, indeed, recycled. while the later need not to be so blatant, just by holding it in their hands [or even seeing the photograph of the product], some one could already tell what the product represents. this is the consequence of choice between being abstract or representational.


what's the point of the second question? it's the communication of an idea. while the abstract product is more democratic + ask for the public's participation in itself: a sheet of recycled paper would mean nothing + can't be an object of exhibition on its own unless it has been put in use; the representational one is the easiest: it's loud in its appearance, like bon jovi band members + their hair-dos, unlike if someone met john cage or david byrne in person, no one would ask them ‘what kind of music are you playing dudes?'


like it or not, that's the state of the surrounding society in which i am standing. everyone's asking ‘meanings of a product' to be threaded beautifully by an architect [+ also popular musicians.] be it sustainable, open, luxurious, comfortable, useful, up to [questionable + highly subjective] terms such as chic, trendy + fashionable. to revolutionize this condition of society, one must concentrate itself to breaking down this favor of ‘meaning' by subverting these words + what they stand for. i think.
29. will
10.28.08 / 1pm


subverting the words?


This Brian McKay Lyons guy in NS makes buildings and calls them “plain” or “zero”. He prides himself in building things “light on the land”.  

Admittedly, his homes do charm me a little; albeit it would be hard to mess up a pristine open oceanfront site.  

More to the point,  

Lyons is quite vocal about branding his stuff “modern vernacular” or something to that effect. He points to fishing shacks and barns as inspiration, which mercilessly dot the landscape of Nova Scotia. He always mentions that his sites are the ruins of his ancestral towns.  

Indeed, all of his writings discuss references to past typologies, programs and ultimately, worldviews.


All the while, his buildings remain 80's era modern wedges. Go figure.
31. river
10.30.08 / 7am


I take issue with one micro-point of your discourse. 


[No](http://www.papeldeparedegratis.net/wp-content/uploads/2008/09/atomicwater.jpg), the Gothic cathedral not only symbolized but rather Vitalized an ineffable nothingness for the population-which-was-will-be called West. or NAN. Or divide by Arabic zero error for a certain time in history. We got over it and moved ahead, surely, but the memory of that moment is somehow, very strange, still now. We pause to contemplate the social power that must have gone into that old, brilliant Westwork. And the work of apprentice historians that are laboring still over the last case in Washington. It recalls, popularly, the mound-building enterprises of much-more lost cultures than ours. It's a ghost-shirt'esque thingee. And you don't do it justice in your post by a long-shot. That's all I that disagree with.


Architecture and music are knit in symbiosis. I agree completely!


What intrigues me about the hexadecimally calculated leanings of very old towers on the eve of this new election, is best [described](http://www.youtube.com/watch?v=amv7DN6P29Q) by an article I didn't expect today from a [popular magazine](http://www.metropolismag.com/cda/story.php?artid=3538). A magazine that is somehow not the same tonight as the one I read yesterday, or last month. 


My fiancee and I have had some vitalizing discussions this week about the influence of older deconstructionist literature on newer, MSW textbook authors, in anticipation of shadows that might be cast by any of us professionals in a manner that we could actually stand against our worst personal habits, with.
33. aqua alta
11.1.08 / 7pm


i was reading an essay by sebald titled ‘strangeness, integration, and crisis' before i came across this discussion, and in his essay, he also seems to suggest that abstraction is more desirable than representation regardless of socio-economic class in a sense that ‘literature can transcend this dilemma (of dead abbreviation) only by keeping faith with unsocial, banned language, and by learning to use the opaque images of broken rebellion as a means of communication'. at this moment of progress after and/or still inbetween modernism and postmodernism, the topic opens up images of all kinds of cultural phenomena not only in rather high class forms of art including architecture, but also internet porns, hip hop and other expressions of suffering inner worlds. i think there is an inevitable issue of femininity at risk. reading kafka, batille and cela while contemplating on the fact that so many modern intellectuals and artists suffered from syphilis, i cannot help but thinking about what is abstracted or represented in the images of rather horrific femininity, love, eros or whatever. personally, i prefer the images of more refined manners of high class women than the honest expressions of what inner worlds i don't want to know, but women almost always want to test the sincerity of love by revealing both flowers and roots. in any case, i would like to make an inquiry if architecture is more or less confined within the art of representation rather than abstraction, and if not, can we think of any built work which is not in the form of representation?
35. Marc K
11.8.08 / 8pm


In response to the ideals of representation and the question of experience in graffiti:


Yes. The work of Lebbeus can be described as graffiti-esque. However, it seems to me that graffiti is, or has been known as a formal expression. the beginnings of graffiti talk about the idea of the ‘tag'; the undeniable mark of a person on a place. It was a sign of history, time, and memory. Graffiti then grew out of the evolution of the tag into an art piece; and redefined what it was to be a vandal and to destroy property that was not yours to begin with.


This is where Lebbeus's work separates from the act of graffiti. 


The idea of vandalism and graffiti has long been overused, and any passer by on a Philadelphia subway ride can see this first hand. It is no longer about the sign or the memory as it is about leaving the mark. (In my own opinion) Interventions like those of Lebbeus are meant to be reminders of the struggles of war and other such events. Graffiti fails at this. And you will rarely see a graffiti artist talking about issues that Lebbeus talks about.


In addition, the act of graffiti is an act of design. These artists will labor over a piece for weeks before it is made, making it not as improvised as it may seem. And today, graffiti artists are hailed for their abilities; often being paid to do their work. This is slowly removing them from the vandalist movement of graffiti in the 60's.


That said, Graffiti remains to be a mode of representation. Often a sketch in a book can turn into an abstract piece that becomes a flourish of color and form. Architecture is able to do the same, but graffiti is a mode that often lacks the motives that architecture has.


In the end, the graffiti artist is an artist of form; he makes for a shortened experience of a ride on a subway or a drive in a car. He often does not look for evolution or for exploration, but merely for the exploitation of his proficiency. In the case of graffiti, it often lacks the substance that architecture has. Instead, it fetishes the alais. Beauty in this case, is only skin deep.
37. Fay Kirsten
11.20.08 / 11am


Marc K; I think that you are selling grafitti a tad to short…


Perhaps we should also return to Penelope's reference to ‘Street Art' rather than grafitti, which I find more descriptive. Banksy is a good example of how street art becomes perhaps a political commentary in cityscapes – but in some instances also a commentary on how a certain space could be perceived or used in a different way from its present. Street art interests me architecturally because it is in fact as Penelope suggests a form of participation with the physical form of the city. 


Obviously there will be lots of simple acts of vandalism (as you suggest tags are really just about leaving a mark – often left by people simply trying to draw attention to themselves within their own social group) in between the more profound works of art/acts of participation. I have had several opportunities to spend time in Berlin and have loved studying the route that street art there is going down. For those that have been – most will have noticed the random pieces of packaging and other garbage that has been stuck on walls and turned into smilies. For me this seemingly simple act has created an openness to discovering other ‘smiling' forms in the city scape – ones that exist by ‘accident'; well, at least not intended by their designers. Surely this banale example shows that street art is a way of participating and thereby changing the way a cityscape is perceived by others than the artist.


What is really interesting about participation as a way of viewing street art – is that the whole suggestion that participation is possible with the cityscape, puts an end to the idea that buildings and spaces have a finite value of representation. That those who inhabit the cityspaces can easily change their use and meaning – therefore I think that street art can in fact have the substance and motives that architecture has – however, in a less permanent form – more improvisational perhaps – something that many cities can benefit greatly from.


Finally I think that the beauty of (some) street art is really not only skin deep – if it changes peoples way of perceiving a space or a building, then it is a profound beauty that it holds.
39. Fay Kirsten
11.20.08 / 12pm


Oh… and by the way I think that Penelopes comment about intervening and questioning – not taking things at face value (or predefined value) – has great merit in the discussion of abstraction. Brad also mentions that architecture should perhaps be both easily consumable AND offer deeper meaning for those interested in making the effort to explore it – I agree! -> Intervention, participation and questioning are a way of exploring this deeper meaning, adding to it, or changing it. So architecture can be immensely abstract – its abstract qualities are brought forth by those who question it. 


The clever architect will have left plenty of questions unanswered or only partially answered. It is wonderful to experience a building that doesn't scream ‘I am a library (or other easily recognized function)' from its every nook and cranny – but leaves you thinking – just thinking!
41. A\_D\_E\_
5.9.09 / 4am


Lebbeus,


Thank-you for your thought provoking essay, however I am not sure if I understand you correctly as to the egalitarian/democratic nature of abstract art, especially abstract-expressionist art. The global recession has exposed the non-egalitarian nature of capitalism where-by main street suffers as executives receive bail-out financed holidays. 


Capitalism created the abstract-expressionist American avant-garde through the wealthy patrons of the US museums of modern art who saw artists such as Pollock as representing their own political ideology. An ideology that has proven non-egalitarian.


This article contains evidence of CIA sponsorship of abstract expressionists, a hot topic in Art History today:


<http://salondeverluisant.org/texts/aewcw.html>


<http://www.amazon.com/Cold-War-Confrontations-Exhibitions-Cultural/dp/3037781238/ref=sr_1_1?ie=UTF8&s=books&qid=1241842764&sr=1-1>


Given, we have also seen that capitalism and reactionary conservatism are complementary constructions that greatly impact upon democratic processes, I find any relationship that suggests “abstraction is in retreat at the moment, as American politics has moved relentlessly to the right” dubious.
43. Peter
8.4.09 / 9am


An “aesthetic-ethical confusion“ only arises if we think of architecture as an abstract art reduced to sheer symbolism and self-projection. In fact the growing awareness of our corrupted environment shows that ethics and moral are not absent at all but an inconvenient factor in terms of economic practise. In a sane society an architect would aim for the highest integrity resources and most advanced technology available on earth rather than cosmetic surface adjustments. Efficiency and sustainability definitely decrease pollution but at the same time tremendously slow our cyclical consumtion and weaken the “economic growth“ forever. The challenges we are facing today – and I am not only talking about architecture here – cannot be blamed on left- or right-wing politics or even be solved by an obsolete capitalistic democracy with it's monetary system. 


Concerning the “egalitarian aspirations of the USA“ I agree with you as long as it concerns the US citizens themselves. However, in perspective the violent history of exploitation caused by the western society remains until the present day whereas two-thirds of the world play the counterpart in our “free market“ game providing the scarcity we need to balance supply and demand.


Contrary to politics, abstraction and art manifest themselves in the absence of fear.


*Mr Woods, thank you for providing this as platform for exchange and dicussion. I greatly appreciate your work.
