
---
title: STONEHOUSE
date: 2008-10-02 00:00:00
---

# STONEHOUSE



[![](media/steinhaus12.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/steinhaus12.jpg)[![](media/steinhaus2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/steinhaus2.jpg)[![](media/steinhaus3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/steinhaus3.jpg)  




More than twenty years ago, Günther Domenig began to build a house for himself on a narrow sliver of lakeside property in the mountains of Carinthia, Austria. He conceived it as a work of architecture limited only by his imagination and skill, at once a manifesto and an experiment, the outcome of which he could not be sure of at the beginning. The structure grew year by year, piece by piece, following an ever-evolving set of sketches and technical drawings, and was financed from his own architectural practice in Graz. When he had a little extra money, he put it into the construction. While the building is called a house, it was never intended as a residence. In fact, Domenig, when he lived at the site over the years, stayed in a small, box-like metal trailer away from the house, not wanting, perhaps, to confuse its purposes. It's sole purpose was to be architecture.


 On Sunday, October 5, 2008, the Stonehouse was officially dedicated and, in effect, declared complete. Various luminaries participated the ceremony (orchestrated by MAK director Peter Noever), including architects Thom Mayne, Wolf D. Prix, Carl Pruscha, Hans Hollein and Raimund Abraham. While it is easy to share the excitement of such a moment, one can secretly hope the idea of completion is provisional. Essential to the house is the vitality, power, risk, existential optimism and idealistic doubt that have characterized its long gestation.


 Domenig's own statement made during the years of work is candid and unflinching: “I have reached my limits in every respect. Here we shall see what I really can carry out in architecture. I have reached the limits of myself. I am standing in front of the limits of my technical and financial possibilities. There is no way out, no way back. I feel the hopelessness of my own consequence. The better I am, the better each step is, the harder the next one becomes. Maybe I will fail.”


**Written by LW on the occasion of the official opening of the Stonehouse**


One of my earliest statements about the meaning and purpose of architecture was: “We should make our buildings first, then learn how to live in them.” Born into a world we did not create, this is always the task facing us: to adapt ourselves to the world as we find it, or the world to us.


 Why should we find only architecture limited to some assigned purpose? Why should architecture limit its potential to create space to satisfying the demands for the already known, for some normal ‘program of use'? Architecture should be freed to follow its own rules and ways to its own spatial, and spiritual, conclusions. Architecture should awaken in us new understandings and knowledge, and inspire us to embrace previously unimagined experiences. It should demand from us a level of invention of our own lives at least equal to the level of invention that brought it into being.


 This view of architecture is not widely held by architects themselves. But it is the view held by the architect of the Stonehouse, Günther Domenig. In its architecture he has invested his most poetic powers of analysis and his most inventive powers of the conception and design of spatial forms. He has gathered together over many years the diverse strands of a wide and celebrated building practice into a single building, and presented us, his fellow architects, with a model of dedication and commitment to ideas and their realization. The Stonehouse presents us with difficulties, of course, the very difficulties that mark our present world: the clash of different forms and modes, the unresolved collision of different systems of thought and action; the ambiguity of meanings that do not declare their purposes in advance. The Stonehouse belongs to a different world than the one we normally inhabit, and it dares us to find ways to inhabit it, or even to talk about it. Yet here it is, both realized, and real. How real are we, as we stand in confrontation with this difficult work of architecture? Let us be grateful to Günther Domenig for giving us a chance to find out.


 LW


<http://en.wikipedia.org/wiki/Günther_Domenig>


<http://www.htl-vil.ac.at/stein/PROJEKT%20Steinhaus%20Domenig_eng.htm>




 ## Comments 
1. [rb.log» Blog Archive » Should we stop designing for purpose and use?](http://www.richardbanks.com/?p=1520)
10.3.08 / 9am


[…] This sentiment from Lebbeus Woods, on the meaning and purpose of architecture really resonates with me when it comes to thinking about technology. He's talking about how people should adapt to space, rather then space be totally pre-conceived with use and purpose in mind. I think the same can be true with technology. STONEHOUSE « LEBBEUS WOODS […]
3. Nenad
10.7.08 / 6pm


A prospect of (re)inventing one's life in a through magnanimity of the imagination that is to say through creative interpretation of the body of architecture is a wonderful idea but less probable as a realistic proposition. Yes, architecture can distil poetics of a life, but I think it can not instigate a different form of one. On other hand Architecture being ‘freed to follow its own rules and ways to its own spatial, and spiritual, conclusions' has great likelihood of becoming a true reinvention of the practice of architecture. But how we do it? I have been to too many exhibits or student's work presentations witnessing how one's imagination succumbs to someone else's enclosure of postulations which are usually devoid of life itself or simply of individual richness. This is also a good point to ask a question: how do you teach this kind of architecture? Or better say how one enables young architects to decode his own life through architecture. Often just as in practice I see failed attempts, and exercise in futile corroboration of the probability of the same reality, or even worst, in instituting parallel reality which only exists in macro-environment of design studios. Sometimes these intellectual corroborations are just alien interpretations extending from other disciplines rather than from formal, generative core that informs inherent processes to art and architecture. In other words criticality is an issue one should pursue but not at the cost of denouncing formative architectural language.
5. [steinhaus3.jpg (JPEG Image, 1000×1022 pixels) – Scaled (57%) | Дизайн Блог!](http://scumdesign.ru/steinhaus3-jpg-jpeg-image-1000x1022-pixels-scaled-57/)
2.9.10 / 5am


[…] <https://lebbeuswoods.wordpress.com/2008/10/02/stonehouse/> Комментариев […]
7. [austrian Stonehouse « Couleurblind](http://couleurblind.com/2010/10/13/austrian-stonehouse/)
10.12.10 / 10pm


[…] Domenig's own statement made during the years of work is candid and unflinching: “I have reached my limits in every respect. Here we shall see what I really can carry out in architecture. I have reached the limits of myself. I am standing in front of the limits of my technical and financial possibilities. There is no way out, no way back. I feel the hopelessness of my own consequence. The better I am, the better each step is, the harder the next one becomes. Maybe I will fail.”( via Lebbeus Woods) […]
9. [Adel Zakout: Top 10 Architects' Homes (PHOTOS)](http://www.huffingtonpost.com/adel-zakout/top-10-architects-homes_b_920379.html)
8.11.11 / 11am


[…] experiment on daily life. "We should make our buildings first, then learn how to live in them.", says Lebbeus Woods and the common mantra of clients that "my house should be a reflection of my personality" here […]
