
---
title: AMERICAN DREAMS
date: 2008-10-01 00:00:00
---

# AMERICAN DREAMS



Could there ever be an American Dream of non-ownership of one's house or apartment? Leaving aside the issue of who really owns them (for most, it is actually the ‘mortgage holder”—whoever that might be in today's arcane and shaky financial webbing), it is interesting to consider the concept of home ownership itself, from the perspective of any idea of the cherished Dream of a Good Life and a Better Future.


The idea of owning your home has the sound of securing it, of making it a safe haven for you and your family. It is yours and, as long as you make the payments, and pay your taxes, and stay out of too much debt, will remain yours, and your children's (if you have any) in perpetuity. Also, no one can violate your home, or the land it sits on (if it has any), by entering without your permission—a sanctity the law says you can enforce with a gun, if necessary. Another part of this American vision is that home owners are the most responsible citizens of their communities, for the practical reason that they have the most invested in them, not just in terms of money, but also of moral capital—they play by the rules of their communities, which is the basis for their being granted, and sustaining, ownership of a part of them. Or, that's the way it used to be.


Increasingly, Americans buy their homes and condominiums as a financial investment. Far from seeing their homes as places to be handed down to their children, or, for that matter, to live out their lives in, they are viewed as instruments for getting a return on their money, primarily through selling them at a higher price than they paid. Whether they are living in Denver suburbs or Lower Manhattan, homeowners have an eye on the real estate market. If they can sell at a high price, they can afford to move upward, to a better and more expensive home. Leaving aside the issue of those who cannot keep up in this game (and there are many), and lose their homes (becoming effectively ‘homeless'), or who have to move because their jobs are lost, or they get transferred and have to sell at any price, it is clear that the character of the American Dream of home ownership has changed radically.


Is it time for a new Dream? Clearly it is, and Martin Luther King, Jr.'s (“I have a dream”) is hopeful as a model of what it might be. Architects, locked for so long in the ideal of home ownership—from Frank Lloyd Wright's Broadacre City, where everyone would have their sovereign acre of prairie (and a Wright house planted squarely on it), to Frank Gehry's twisty luxury condo tower in Lower Manhattan—have difficulty generating any comparable vision of the American home. It is telling that the most gifted designers today—American and not—can only come up with snappy new wrappers for prevailing, but finally fading, ideas. The current bursting of the “housing bubble,” and the coming  financial shakeout, which will be global in extent, and giga in scale, could leave them with more time to consider the reality of how most people live, and about the nature of home in the contemporary world.


The concept of non-ownership would be good a place to start. Or, at least, with the idea that money is not at the heart of it.


LW




 ## Comments 
1. [slothglut](http://fairdkun.multiply.com)
10.1.08 / 8am


i had the same idea when me + some of my friends made a proposal for mit's envisioning jerusalem some times ago.  

the wild scheme was to make clear land-use separations in the city, with total fixed numbers that house the stakeholders of the city [e.g. 4o% for palestinians, 4o% for israelis + the remaining 2o% for foreigners.] we advanced with mapping the areas of jerusalem [+ consequently their physical borders], then planned a physical rolling scheme of every 6 months [or a year] that would take these stakeholders [inhabitants?] to changing location every time of those periods.  

the interesting part for me when we came up with that idea was that no one in this way would actually own their land [as opposed to the reason of the prolonged conflict still happening], + everything -up until the very detailed little things- would be sustained in a state of temporariness, therefore reducing or even eliminating the cause of physical violence in the first place. would it be a spectacle in itself to see how this city could grow under the very condition?  

being to radical, the idea was shot down in the end by my partners. reading this post of yours, though, it brings back these memories. further question is: would the strategy be pragmatically applicable in every conflict-ridden zones?
3. David
10.1.08 / 5pm


Americans have **always** had this illusion that we once lived in the same home for generations. Russell Lynes'  *The Domesticated Americans* (1963) has a great collection of American complaints for the past 200 years that we move around too much and don't commit to a place. 


The irony is that image of homesteading families masks the largest speculative real estate society ever.


Of course, if we actually lived like that (and I don't think we should), we would have no need for the **architectural symbol** of permanence. Reference the common Turkish tradition of many generations residing in a single apartment building. The **image** of the family estate is unnecessary since the reality is manifest. 


Slothgut – I don't think your idea is “too radical.” That area of the world has a long tradition of nomadic culture. The problem with the idea is that it does not address the actual desires of either side.


How about this for a radical – and completely practical/traditional – “answer”: People can live where they want, and own their houses, etc. They simply must live with a lingering dread that the violence will not ever end. 


That seems more realistic than somehow forcing people to move every 6 months, and it is the situation that most of humanity has always lived with.


Or do you think that some architects scheming in Boston can “solve the problem” of land disputes?
5. [lebbeuswoods](http://www.lebbeuswoods.net)
10.1.08 / 6pm


David—my concern is obviously not the nomadism, but the real estate speculation that has come to dominate the American mentality. Money—as it often does—distorts the issues and what's really at stake. In this case, it's the concept of the community based on ideas of sharing responsibility and collaboration. I would gladly embrace the model of a wandering (e.g., Bedouin) community, which changes its constituency, but follows a consistent set of self-defined rules, but have a hard time accepting the everyone-for-themselves anti-communities of today's America. Developers and banks have found a way to spread around the financial risks of building dwelling spaces by drawing everyone into the mortgage game. But in the process they have marginalized those who can't or won't play, and pitted those who can or will against one another. That's the pernicious part of today's “Dream.”
7. [lebbeuswoods](http://www.lebbeuswoods.net)
10.1.08 / 6pm


Slothgut—a spirited idea, if there would be a way that people would want to ‘change places.' Government, or any other form of coercion would not be acceptable….right?
9. [slothglut](http://fairdkun.multiply.com)
10.2.08 / 11am


david,  

it's true what you said about the difference between ‘scheming in boston' + ‘making a change.' the scheme was my own way to make fun of the condition itself. humor yourself wittily, make everything looks like fiction around you [including this fiasco in housing condition in the us + uk.] apparently, the answer to the conflict is in the conflict itself. as fast as you can see by reacting to my post, it's not radical at all, the answer is as clear as summer sky, yet failed to be grasped by the authorities for reasons such as money + power: a condition which made me think about the idea for the first place.


lw,  

tQ for pointing it out. yes, any kind of human-to-human forced desire should be forbidden. looking at the fact, isn't that where the beauty of design + planning lies? when + where you can give +/or take human desires + tendencies subtly. don't you think?
11. Alex Bowles
10.6.08 / 8am


A home seems to represent two vital things – shelter from the elements, and a clear boundary between the public and private aspects of life. 


As such, the dividends it ‘pays' are not realized in terms of monetary gain. In fact, in a ‘normal' market, housing is properly regarded as an expense. Rather, a home should offer value in terms of health and well-being (making those toxic FEMA trailers as one of the worst kinds of housing imaginable). I'm wondering if loosing sight of this value really means loosing sight of why we have friends and families in the first place. 


Feeling a compulsion to entertain dangerous levels speculative risk in what should be the most stable and enriching area of one's life is disconcerting, to say the least. 


Perhaps we've reached a point where, in the wake of this global housing meltdown, people will begin to reconsider the source of a home's value in terms of the life it actually helps you lead in the here and now, with everything else (form / function / finance / freedom) developing accordingly. 


It's a much more personal measure of value, and can lead in directions very different from those that are followed when market value (i.e. what others want) is paramount.
