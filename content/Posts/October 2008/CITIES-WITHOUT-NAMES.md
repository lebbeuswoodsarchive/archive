
---
title: CITIES WITHOUT NAMES
date: 2008-10-07 00:00:00
---

# CITIES WITHOUT NAMES



[![](media/on1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/on1.jpg)[![](media/on3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/on3.jpg)[![](media/on4.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/on4.jpg)[![](media/on2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/on2.jpg)[![](media/on5.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/on5.jpg)


The cities without names as those we see from a distance, often as a glow on the night horizon, but which we cannot approach and enter. Perhaps their distance is maintained because we are only passing by, on our way somewhere else more familiar; or, perhaps, by our desire to keep them in our minds as inviolate and unknown, places for the future and not for now. All the elements of cities we know are there, the more or less familiar profiles of buildings where people like us, we imagine, live out their lives. But some differences are apparent. As we strain to see more, these differences reveal the utter strangeness of these cities, their remoteness in more than distance and time from us and what we have known. Any one of these cities seems—for want of a better word—uncertain. The structure of the city is tentative, as though it were a series of inconclusive gestures in space, almost like the *pentimenti* of an artist's drawing, as he or she searches for a form not known in advance. Gone is the familiar sense of work accomplished and, in its place, the uneasy feeling that the work undertaken was not meant to be accomplished.


Our companion on one such journey, a distinguished architect, said, “Oh, don't be so surprised. Haven't you heard that those cities were designed by crazy fools? They started to build, but don't know how to finish. Still, they think they've created Utopia!” 


LW 


 




 ## Comments 
1. Alex Bowles
10.8.08 / 6am


If cities are arrangements of people, would not knowing anyone who lives in a particular one make that place seem exponentially more disconcerting and inaccessible?
3. [m](http://www.asasku.blogspot.com)
10.8.08 / 7am


hello Mr. Woods,


I do wonder at times, if every cities and even towns have the same mall, the same convenient store, the same advertisement billboards that took the most of the cityscape spaces.. The cities become so faceless that it have to depends on landmarks to state its place in this world. 


In this sprawling growth, will there still be a sense of place in cities? 


thank you.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
10.8.08 / 4pm


Alex Bowles, m: both your questions reflect an uneasiness with the anonymity that is, like it or not, inherent in large cities. 


So many places do look alike, and there few faces we recognize in the streets. Anonymity is liberating because no one knows us or particularly cares what we do, but it's also depressing, for the same reasons. Freedom, real freedom, which is more about not having than having, is scary. Most of us who live in cities learn to survive, or thrive, with a few friends or family, and work we think has some meaning in the bigger picture.


I don't think it's the role of architecture to relieve the alienation natural to cities, certainly not with landmarks, nor with any form of pseudo-domesticity, like so many gentrification projects attempt to do. All architecture can or should do is create networks of spaces that have the greatest potential for lonely city-dwellers to invent their lives. This means, in a way, to increase the complexity, and the unknowability, together with its inevitable anonymity.
7. Josh V
10.9.08 / 1pm


What I like to find to define a city is the local artists. When I visit a city I pay particular attention to the graffiti. I've found beatiful, yet incredibly different, graffiti in Athens, Berlin, Buenos Aires, and New York. Very definitive of the underground culture, the pulse of the city.


A very talented friend of mine once told me the best way to discover a city is to read the poets of that city. They best could describe its feeling.
9. aes
10.10.08 / 2am


The drawings to me imply anonymous cities (like Calvino's invisible cities), not necessarily the anonymity of the city dweller. In this sense, the horizontal format is particularly interesting to me because it implies that the distance between the cityscape and the picture plane is the instrument of that anonymity. Conventionally, of course, cities are most recognizable from a distance by its skyline, and only when one is completely immersed does a city become anonymous. Yet the horizon in each drawing makes sure we pass by, not allowing us in.


So there's a paradox in the drawings that point to this idea of the strangeness of the known; that is, we know these are cities because we recognize the comprising parts (we may even recognize the city itself), yet the amalgamation itself feels like a slightly uncomfortable deja vu, sublime, half-remembered, half-invented.
11. Alex Bowles
10.10.08 / 7pm


I'm not sure that anonymity *per se* is the source of unease. After all, I quite enjoy my own. What really seems disconcerting is the sense of isolation that comes from places where you lack any personal connection (sorry, but that really friendly concierge and the nice taxi driver don't count).


I suppose I'm stating the obvious here, but the fact remains that humans are fundamentally social creatures. We like creating, developing, challenging, and rebuilding hierarchies of all kinds. Urban landscapes have become the tangible expression of our essential natures, desires, contradictions, and points of confusion. 


Not knowing anyone – that it to say, having no references within the prevailing social orders – leaves the social impulse within each of us at a very loose end. At the same time, we all seem to understand that even thought this impulse is at the root of all culture, the impulse – untutored and alone – will not provide access to any *particular* culture. 


This may be I find Josh's comment about poets so good. Poetry provides access, in a very distilled form, to the essential consciousness that develops from involvement within a particular city's history, networks and hierarchies. 


Even if you're still by yourself, you at least have the basis for making connections. Then the place begins to open, and you start to participate. 


The funny thing is that when you leave, and reconsider these places from afar (most likely the air) you (or at least I) tend to look for those places where some meaningful exchange took place. For me, this extends to a city's map, and the way I sometimes find myself using maps to reflect on series of exchanges in terms of the positions they had in space.
13. Alex Bowles
10.11.08 / 8pm


Mr. Woods,


I just came across a reply you left in one of your earlier threads (Dumb Boxes).



> I have argued that serious, creative, innovative architects have to take up the design of spatial fields, networks, addressing transitory conditions, and not concentrate so much on single, monumental buildings.
> 
> 


It's true – people don't invent lives for themselves in monuments. And unless they're custodians of some sort, they tend not to build lives around monuments either. And yet, that's what so many architects seem interested in building. Everyday life, on the other hand, doesn't seem to be such an inspiring subject, even though this – through the creation of spaces that create new possibilities for self-invention – is where architecture can have the most profound effect on life as its lived. 


Given the inherent conservatism you noted in the Pritzker Prize, perhaps this is because Architecture, at its highest levels, is actually behind the times. Sergi Brin has pointed out that the internet exists virtually everywhere there's electrical power, and he would know. This is a radical development. A standard for networking, shared globally is as significant as the discovery of fire, electricity, and the law of universal gravitation, all rolled into one. 


And now, people – not to mention entire cultures – are routinely inventing lives for themselves that are dramatically richer and more interconnected that anything that's fallen within the norm, as its existed to date. 


And yet Architecture hasn't seemed to notice. Perhaps because it's playing out at the mundane level of everyday life, which is notoriously unspectacular, even if the adaptations people are making to preserve a sense of normality are truly astonishing.
15. [Christina McPhee](http://christinamcphee.net)
10.17.08 / 6am


These cities without names as images seem like flags or emblematic fields.
17. [::Ciudades SIN nombre:: | Grupo Gutiérrez Cabrero](http://gutierrezcabrero.dpa-etsam.com/2009/12/02/ciudades-sin-nombre/)
12.1.09 / 10pm


[…] Lebbeus Woods (y también en su blog) […]
