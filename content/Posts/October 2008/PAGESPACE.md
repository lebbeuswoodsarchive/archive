
---
title: PAGESPACE
date: 2008-10-19 00:00:00
---

# PAGESPACE


[![](media/eliot1bw.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/eliot1bw.jpg)[![](media/stewart8bw.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/stewart8bw.jpg)[![](media/woods3abw.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/woods3abw.jpg)



*(l to r, T. S. Eliot/Ezra Pound, Susan Stewart, Lebbeus Woods)*


Some poets and writers—even the occasional architect—think of a page as a space to be explored and defined by words. Whether typeset or handwritten, words are marks of a precise nature—they have shape, density, extension—that establish spatial boundaries and limits, in other words, they are the stuff of architecture. While the marks made by architects on a sheet of paper are most often abstractly pictorial, writers work with alphabets and languages, their form and syntax. The writer's work is complicated by the fact that the marks called words have a pre-assigned meaning that is shared by a community of people who use them as instruments of understanding. The writer's creative task is to make people see the words afresh, as though they were at the same time both new and known. Words used in too-familiar ways are clichés, trite and tired and worn, and refer in the reader's mind more to the writer's dullness than to what the words might potentially mean. The bad writer is one who does not arouse the reader to a fresh understanding of the familiar.


Even the best writers—those who enable us to read the same old words with new meaning—most often stay with the conventional arrangements of words on the page. A few of the best, and some of the less-than-best, employ the arrangement of the marks called words on the page to help us discover their familiar meanings in new ways. It is the most experimental, riskiest, and least often successful of techniques. All the more reason it remains a way of writing to be further explored. 


LW 



 ## Comments 
1. aitraaz
10.21.08 / 11am


I admit I've always admired these ‘textual/spatial' drawings of yours and agree with your observation concerning the syntatic and geometrical codings/decodings embodied in text. Would be curious concerning the introduction of typography into this economy (form, syntax, content & expression on the ‘space of the page.' In any case very inspiring 🙂
3. aes
10.22.08 / 2am


The act of writing is a profoundly material exercise: the movement of the pen-point in words, then lines, then pages, to finally form a book, precisely follows the movement of the point, to a line, to a plane, and finally to a volume. The making of a book is a deliberate and physical mapping of the cartesian, and therefore spatial, world. The laying of words is parallel to the laying of bricks; the construction of a page, to the construction of a wall. The open book is a section-cut. Ultimately, the book and the house are both iterations of the same bodily exploration of our surroundings. The malleable space we occupy between pages to read the book is the same space we occupy between walls to build, dwell, and think.


(The fact that we map ourselves in cartesian space does not predate the idea of ‘volume;' in fact, the word originates from ‘volvere,' as in ‘revolve,' and was used for scrolls before the invention of books. A volumetric construction in polar coordinates before the establishment of cartesian ones.)


Like Einstein's model of curved space bending the trajectories of both matter and light, what bends the trajectories of both the built world and the written word are our philosophical inventions for dwelling.


In this sense, successful re-examinations against the gridded grain of the traditional pagespace have always been a philosophical undertaking. They have always had implications not only for the content of words, but also for their construction; simultaneously, not only for the meaning of architecture, but also for its medium.
5. Adam
10.24.08 / 5pm


Perhaps it is ‘freshness' when a writer uses text to snap a set of virtual fingers and bring you to conscious attention of both your act of reading and the content being communicated. It is refreshing to find yourself aware of the fact you are reading something worth reading, but it requires great skill to provoke that interest of the mind out of the autistic numinosity of unconscious status quo. It seems rather stupid, but when an author provides me with evidence that he/she is actually very talented at writing, the content becomes more ‘real' and compelling.
7. river
11.7.08 / 5am


I like this one page especially like [this.](http://www-groups.dcs.st-and.ac.uk/~history/Posters2/Monge.html) And its comments [too](http://en.wikipedia.org/wiki/Monge%27s_theorem).


It has a LOL! a whole lot to say about [typoing](http://en.wikipedia.org/wiki/Descriptive_geometry) better on the Internet of tomorrow, IMHO.


“Low amounts for F.D.A approved treatment's; shipped to your house” (from my personal inbox, nearly simultaneously)


Too bad we can't draw or volumize as easy as TXTng, yet.  

Or is it?


I'm pretty sure that exacting protocols for dreaming are approaching our kids from a distance even now. As they are working out the preliminaries of hacking them.
