
---
title: ONCE UPON A TIME  Macdonald and Salter
date: 2008-10-12 00:00:00
---

# ONCE UPON A TIME: Macdonald and Salter


[![](media/lwblog-ms15.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/lwblog-ms15.jpg)[![](media/lwblog-ms4.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/lwblog-ms4.jpg)[![](media/lwblog-ms5.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/lwblog-ms5.jpg)[![](media/lwblog-ms3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/10/lwblog-ms3.jpg)


 


Once upon a time, before computers came to be the pre-eminent architectural design tool, architects made drawings by hand. Instead of leaving it up to the computer's software to make and assemble the lines defining contours and edges of forms, architects would draw line by line, gradually building up the drawing. Somewhere in the backs of their minds, perhaps, the Italian term *disegno,* which means both ‘drawing' and ‘design,' worked to convince them that the two concepts were synonymous: to draw was to design, and to design was to draw. In the same way, the ideas of ‘analysis' and ‘synthesis' came together in the act, and the artifact, of drawing. To build up a drawing line by line is an analytical act—-one chooses exactly where to place the line, based on an understanding of the problem or conditions to be addressed, and, at the same time, of the need for the sum of lines to create a greater whole, a coherent, cohering and integrated form….


Once upon a time, long before notions of ‘green' and ‘sustainability' became fashionable, some architects were interested in the question of how to integrate architecture and nature. Their approach was formally architectural, rather than dependent upon technological devices, such as solar panels and geothermal wells. They reasoned that the forms of architecture, very much like the forms of the natural world, embody not only morphological principles of shape and its elaboration, but also those of the origins and meanings of things. Forms could speak the truth. Their approach was to study the forms of a site and of the spaces they wished to create, to collide them, to meld them, and enable new forms that would emerge….


Once upon a time, long before the invention of 3-D computer modeling, architects believed that architecture could best be described by orthographic projection—through plan, section, and elevation. While they would make, for their clients, the occasional perspective drawing, sometimes rendered in tone and color, with human figures for scale, cars and leafy green trees for realistic effect, they actually worked out their thoughts and designs through the rigorous coordination of two-dimensional projections. This amounted to a philosophical approach, because it channeled emotions, even those of great passion, into a game with precise, mathematical rules. This was not a game for amateurs, or casual wannabes, but only for those serious enough to master the rules, that is, to employ their great potential for expressiveness with exactness and economy….


*There are many ways to begin the story of Chris Macdonald and Peter Salter, two architects from London who, in the 1980s, collaborated as Macdonald+Salter on a number of innovative projects, which were exhibited at the Storefront for Art and Architecture, in New York, in December of 1987. (The project shown here is the ICI Trade Pavilion, at the Royal Agricultural Showground, Stoneleigh, England, 1983). Their projects were never built, though were clearly intended to be, and their collaboration ended many years ago. All that remains are their drawings, or the inferior reproductions of them in a few yellowing exhibition catalogs. Even so, the ideas they embody live on, as does the spirit of their architecture.*


LW


 


 


 


 


 



 ## Comments 
1. david
10.13.08 / 10am


What glorious drawings… While I can respect equally architecture which remains on paper and that which is constructed, these works do evoke some sadness – to experience this as a built structure would have been incredible.


“4 + 1: Peter Salter Building Projects” is excellent further reading, and I'd be grateful of any recommendations of yellowing exhibition catalogues and other publications…
3. Will
10.13.08 / 5pm


I seem to hear this same sad song more often these days.  

My school, Dalhousie, in Halifax, NS, is maybe the only institution in Canada which encourages the paradigm of hand-craft: sketching, drawing, drafting, modelling. It is taught as being analogous to the act of construction itself.  

In fact, a professor of mine Essy Baniassad once confronted an entire class at a review for the lack of “fun” in the drawings. The majority of work had been done in a very orderly and final manner, which upset him. ” You must dig for the design”, he said. “I see no evidence of this search”. In truth, I believe the very presence of computers affects the way architects draw, as shown that day. It is a turn towards design as production rather than process.  

And what precisely is an architect if not a person knowing of the process of design and construction?
5. Alex Bowles
10.13.08 / 10pm


It stands to reason that hand-drawing demands a type of mental engagement that is reduced, if not eclipsed, altogether by computer ‘aided' design. If so, digitally driven disengagement should make itself apparent in the kinds of buildings that are created with these new work flows.


I wonder when people seduced by tools that make ‘anything' possible will start to notice that none of this potentiality resolves itself into the satisfying results that analog work actually produces?


And even if, for practical reasons, most architects cease designing on paper, shouldn't the serious ones still play on paper, if only to keep the spatial sense that results sharp enough to actually inform their use of the new tools?
7. [Leon\_London](http://www.finalcrit.com/architecture/leonduffy)
10.13.08 / 11pm


Hello Lebbeus,


Thank you for the heads up on Macdonald's and Salter's wonderful drawings. Do you feel CAD has taken out certain elements of the design process? I feel that just as architects have evolved to pre-visualize through the power of computers, they have also simultaneously built up an understanding and respect for this new gesaltung. There are new horizons and philosophical paths to be explored with CAD and if it makes architecture accessible to a broader amount of prospective students doesn't it increase the chances of there being more architects with more innovative ideas?
9. Adam
10.24.08 / 5pm


During my education I detected a rift between my professors and my fellow students, which I was not sophisticated enough to articulate, but seemed to position us as two groups trying to talk about one thing through a solid wall – that one thing being the position the designer takes with the drawing. It made perfect, clear and tangible sense to us that technical drafting and drawing were two different modes – and also that the content of the drawing was in itself virtual and completely unrelated to the existince of lines, who were there both to (hopefully) serve the virtual content and to express something of their own volition as things-in-the-world. My professors would flinch for a millisecond when this subconscious objection became apparent but we never knew to address it – my professors were still teaching from the time-culture they learned to draw in, a world which lacked a significant saturation of the digital-virtual – they could not see the drawing, the image, the content, as things which could be divorced from correlation. I am not sure if there is a loss to either party in terms of skill, for one age should certainly be different from the last, but in terms of translation we reached a fault. But perhaps this will only be while it remains and unexamined and unconscious condition – for my generation and those that follow, the act of drawing will ALWAYS be preceded by cognition of the virtual dimension of an act.
11. Jim Moses
11.9.08 / 6pm


While I share a sadness for the loss of the hand-crafted drawing, and the very direct brain-hand-paper connection that enabled the designer to ‘inhabit' the spaces he or she designed, in the text there is another compelling issue in Macdonald & Salter's architecture's relation to nature. 


The ancient idea that nature is unaffected by culture, it must be agreed now, is disproved. The impact of human activity on the earth's systems is clear. Technological innovation may get us some distance toward mending the damage. More critical though may be a shift in attitude toward our relationship with nature. Our creative production – our culture – is part of nature, because of, not in spite of, its being man-made. Given the biological imperative of building shelter, and therefore the impossibility of not building, I find myself returning to the Vitruvian triad of firmness, commodity, and delight as the sustainable ideal. If our buildings are well-crafted, generous, and well-loved, they will be cared for and have long lives. The embodied energy of a building with these qualities will be amortized many times over.


Other books containing Macdonald and Salter's (and later Salter's alone) fine work are: Osaka Follies, Harvard Architecture Review Vol. 9, The Idea of the City, Violated Perfection, Exquisite Corpse, Primitive, any number of AA Files from the 1980s and early 1990s. Peter Salter is currently working with Baylight Properties on two projects.
13. [General](http://underconst.)
11.11.08 / 10am


Drawing above all else. If not for any other reason than that of signature, hand, style. Integrity.  

Once upon a time— There were ‘artists' and ‘architects' through the act of drawing participated in a self-reflective and meditative process which enriched the spiritual knowledge of the individual , and then it became commercially fashionable for machines to render our ideas, and with it our feelings.  

However, concept art, beyond 3d modelling, is an extraordinary evolution of being able to communicate space and with only a sketch, a tablet, and layers of digitized color.
