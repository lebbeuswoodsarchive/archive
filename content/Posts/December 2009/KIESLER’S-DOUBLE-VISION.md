
---
title: KIESLER’S DOUBLE VISION
date: 2009-12-22 00:00:00
---

# KIESLER'S DOUBLE VISION


[![](media/kies-vm-2a2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-2a2.jpg)


[Frederick Kiesler](http://en.wikipedia.org/wiki/Frederick_John_Kiesler) is probably best known for his Endless House project of the 1950s or his Shrine of the Book in Jerusalem of the early 60s, which was built before his death. However, he was more than an architect whose startling innovations were so far ahead of his time that they have only recently influenced other innovative architects such as Greg Lynn. He was also deeply interested in the plastic arts and the latest developments in science and worked to bring these together with the design of space to advance human knowledge and experience. One of his efforts in the late 1930s was the design of what he called ‘a vision machine.' He explains it in his own words, as well as it can be explained. I will only add that the machine itself, apart from its theater/gallery/museum setting, is an analog computer of the type mentioned in earlier posts—its input is described in Kiesler's brief texts, but its output would require the interpretation of its users. The inventor wants us to “see seeing,” but, as with seeing itself, our understanding of the consequences of this phenomenon is highly personal.


LW


Frederick Kiesler's texts:


[![](media/lwb-kies-11.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-kies-11.jpg)


[![](media/lwb-kies-21.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-kies-21.jpg)


[![](media/lwb-kies-31.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-kies-31.jpg)


[![](media/lwb-kies-41.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-kies-41.jpg)


Kiesler's conceptual and design sketches:


[![](media/kies-vm-7a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-7a1.jpg)


[![](media/kies-vm-8a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-8a1.jpg)


[![](media/kies-vm-4a2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-4a2.jpg)


[![](media/kies-vm-3a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-3a1.jpg)


[![](media/kies-vm-6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-6a1.jpg)


[![](media/kies-vm-9a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-9a1.jpg)


Kiesler's plan for installation of the Vision Machine in a gallery or museum or, perhaps, a new type of building:


[![](media/kies-vm-1a2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-1a2.jpg)


[![](media/kies-vm-1b2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/kies-vm-1b2.jpg)



 ## Comments 
1. [Chris Teeter](http://www.metamechanics.com)
12.31.09 / 6am


forgive me i've been drinking Chimay and some of those new Coastal Wheats by Samual A. 😉


anyway…reviewing this article on my blackberry i had a couple visions about the visions machine…yesterday


see i thought you were going to write about vision in architecture, the only thing that counts in my opinion, since i'm bias towards visions…delusions…illusions…imagination being confused with memories…if you're mind is photographic and you invent the photographs how do you know reality anymore?


first thought – on 3D re-representation in 3DMAX of Kiesler's vision machine


ray tracing, (POV-ray) the original form of computer rendering, equivalant to the light to retina description described above, the computer calculates backwards from object to camera the properties of the path of light…


take a human head, put it under a “spot light” ray trace light, form a box of vison…perspective setup – a box of 3x3x1 divisions,pop out the XY middle 1/3 and put a 35mm virtual camera behind the wall peering though to see the head. render 6 sides (like a cube) of the head. (rotated towards camera view, i.e. view perpindicular to camera angle)


in 3DMAX you have the UNWRAP command, you can take the head based on “real” virtual rendering settings and UNWRAP the texture map, the final solution of light on a materials properties of a 3d object into a 2D representation…like the orange peel world atlas example…this is why Greenland appears so small on the globe but so large on correctly adjusted flat maps…


compare the UNWRAP with a logical 6 sided collage of views of the head. each time the head has been rotated to be visible from the camera, the light and shadows based on rotation alter the appearance, etc…changed.


compare this 6 sided cube to the object (original form)…it's clear experience brings the concept of the object in the mind together…experience included all the other senses and just pure experience…much better than a logical step for represenation via abstract collages of rationally categorized statements (images) into a 3D correlation of 6 sidedness. 


but what I wanted to do, assuming someone has developed a plug-in script into 3DMAx (or maybe a command I don't know) you can take an opacity map, the map translates the following – black to clear and white to solid…opacity maps for all 6 sides based on rendered flat 2D imaged from 35MM camera through slot in box…into 3d MESH!


the cool thing if an opacity map exsisted that could convert the texture map qualities into 3D mesh objects you could run such a script to merge the 6 sides into one solid 3D object…the in-between birds eye views of this object could be this little fun zone of unknown assumed logics colliding.


look, i'm sure some dude came up with a formula to calculate how 2D 6-sided views fall together in 3D, but that's not the point…google earth guys, my old roomate in germany…whatever…


when you take logics that are scientificly known to all as absolutes and try to use them to explain something not so absolute you realize the huge gap between conscious rational epistomology and actual sub consicious intuition of reality…


when it comes to architecture we're no where close to making it scientific…


the vision machine makes this clear to me.


if you can only visually remember exactly 6 sides, how do you know what the other possible 999 sides look like?


the fact that you know, is a determining quality in a mind of a visionary architect. the guy who knows the shit without drawings…feel the project through, express the design via drawings if need be, but drawings are just there as partial communications of the vision.


because drawings are only minor represenations of the imagination. 


Al DeVido, as per him worked with Richard Meier at Marcel Breuers office back in the day (assuming delusion hasn't superceded imagined memory), he would tell me almost everyday in my short 9 month employment for him that Meier couldn't not draft (Meier was the only architect i claimed to be great at the time)…


DeVido could draft, but I wasn't sure about his imagination, surely deterioating at 70 years of age…(like my spelling)


when I walked into Den Hague – Meier's building blew me away, a true experience of abstract concept of modernism…


saw one side only, but could feel all sides….
3. [Seeing Looking « Painting for Insects](http://daphnelasky.wordpress.com/2010/10/05/seeing-looking/)
10.5.10 / 5pm


[…] October 5, 2010 tags: Drawing, Expansion, Kiesler, Seeing by Daphne via Lebbeus Woods and (perhaps this was unintentional) W.G. Clark, here are some images of Frederick […]
5. [Kiesler light | Killersquad](http://killersquad.co.cc/2011/05/30/kiesler-light/)
5.30.11 / 9am


[…] KIESLER'S DOUBLE VISION « LEBBEUS WOODSFrederick Kiesler is probably best known for his Endless House project of the 1950s or his Shrine of the Book in Jerusalem of the early 60s, which was built before his death. … ray tracing, (POV-ray) the original form of computer rendering, equivalant to the light to retina description described above, […]
7. [Vision Machine | XHIBITOR](http://toolkit.xhibitor.org/vision-machiine)
9.28.12 / 6pm


[…] via Lebbeus Woods. Previous postFundamentals of Exhibition […]
