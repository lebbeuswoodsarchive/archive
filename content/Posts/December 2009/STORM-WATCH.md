
---
title: STORM WATCH
date: 2009-12-19 00:00:00
---

# STORM WATCH


[![](media/lwb-strm-12c.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-12c.jpg)


The realm of architecture is one of apparent stability, but is actually one of restrained force or of forces held in equilibrium. The implications of tectonic stability for inhabitation are immediately apparent, but the epistemological implications are less so. I will touch on them here in only one regard.


In thinking of architecture as ‘at rest,' we are adopting a position based on stability and predictability. Also, we are constructing a system of knowledge that privileges these qualities. This underpins our actions and dictates our goals. The unity and symmetry of monumental architecture refers symbolically to a harmonious and balanced universe, in which contending forces are reconciled. The traditional role of architecture has been one of reassuring us that things are under our control. But it is quite another thing to think of architecture as ‘in tension.'


An architecture in tension suggests a struggling architecture, and a humanity with limited control of nature, and of itself. The forces in such an architecture are activated, not pacified. For the moment, they seem to be held in check, at least to the extent they can be measured at all. Still, they are straining against the materials holding them. Experience teaches that this is not a stable or predictable situation. Change is inevitable, as the materials age or tire, or as they are affected by disturbances within or around them. The forces are, in effect, at war with the materials; they want to overcome them; they want to be free of materiality, to flow into the world's vast oceans of energy, from which they will be reborn again and again, in countless cycles of transformation. Such an understanding of architecture conditions our outlook on the world and leads to the construction of a knowledge-system based on concepts and processes of transformation.


The installation of a tension field in The Cooper Union was small in size. The room was the size of a large classroom or of a modest laboratory for the testing of materials. At the same time, the installation had no absolute scale. The room was a frame placed around a 1:1 constructed landscape that could just as easily be read as much larger—or much smaller. Forces have magnitude, but no inherent scale. Only the materials—braided steel cables and wood rods (painted to conceal their wooden-ness)—spoke to both the magnitude and the scale. It was a test model, a mock-up of conditions that had the ambition to address a general, if not universal, condition: a spatial field in increasing tension.


The cables were strung from wall to wall,  passing through vertical tubes of steel that allowed for changes of direction, until a fluctuating stream of forces were captured in tension. The cables, and the forces contained within them, were not connected. Then wood rods were inserted into the cables, increasing their tension by pulling them together or pushing them apart, putting the rods either in tension or compression.  As more rods were added, the tension in the whole field increased. There was a breaking point, never reached, unknown in advance, at which the weakest point in the field would exceed the limit of its capacity to restrain the forces. Because the structure acted as a field and not as collection of independently stable, ‘classical' objects, the failure of any element reduces the tension in the entire structure. The idea of transformation in a tension field is linked with inter-dependence of the elements in the field, and, more accurately, to their inter-connectedness. The field changes as an integrated whole, whatever its size or scale. And it performs as a space.


This tension field was a spatial field, that much is obvious. But the nature of its spaces cannot be accounted for by thinking of them as the products of design as we are accustomed to thinking of it. To the contrary, they were constructed in a kind of heuristic game in which I, as designer, actually designed only a set of ‘standard' elements and general rules of their assembly, embodied in a small set of drawings, and left it for collaborators to make. It would have been possible to assemble the elements—cables and rods—in any number of different ways without changing in essence the nature of the field or its spaces. In short, there was no hierarchy in the process of designing, or in the placement of elements and their resulting spaces. But it still mattered, in an existential sense, and without an imposed hierarchy to signify degrees of importance, exactly how the elements are assembled. Different configurations result in different effects. These impact our experiences of the field. They also facilitate our knowledge of it differently. A dense assemblage of rods and cables informs us, in a spatial sense, differently than a dispersed assemblage. Yet, the subjects of our knowledge are still ‘field' and ‘assembly,' and, ultimately, ‘experience,' even though understood from quite different points of view. By way of analogy, the difference would be roughly the same as the idea of ‘landscape' as understood by a person living in the country and a person living in the city. In the tension field, unlike the apparently stable field of independent objects dispersed in a designed spatial order, the non-hierarchical order of spaces, the spatially variable and conceptually indeterminate order of spaces, enables each person—regardless of standpoint—to comprehend that they share with others not only the same planetary surface, but the same degree of experience. There is a caveat, however. Inter-connectedness and global consciousness come with a price. Each person feels the increase in tension produced by the others. The tension field acts as a whole, though without being unified in the classical sense of being designed. Uncertainty as to where the next pressure point will be increases the tension in the system. As the tension increases it feeds back into the entire field. Independent actions occur, and often, but the idea of pure autonomy is effectively rendered obsolete.


[![](media/lwb-strm-9.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-9.jpg)


[![](media/lwb-strm-3.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-3.jpg)


[![](media/lwb-strm-2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-2.jpg)


[![](media/lwb-strm-4.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-4.jpg)


[![](media/lwb-strm-10.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-10.jpg)


[![](media/lwb-strm-5.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-5.jpg)


[![](media/lwb-strm-6.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-6.jpg)


[![](media/lwb-strm-13.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-13.jpg)


[![](media/lwb-stm-12.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-stm-12.jpg)


[![](media/lwb-strm-201.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-strm-201.jpg)


LW



 ## Comments 
1. Marc K
12.20.09 / 5am


I presume that the markings on the walls enable a shift in perception of the field as it meets a flat surface? 


It seems that we are trying to comprehend an indeterminate system through formal representation or other architectural means? It reads as though the spatial field in itself, as landscape, or however it is read, becomes a drawing of sorts… it would have been nice to see a new group of people interpret how the system operates within the given set of directions… 


Does the field ever become anamorphic? Is there some sort of hint as to its logic or its organization/disorganization within the system/framework itself?… I read the markings on the walls as if they could tell me this.
3. [Maurits](http://www.nightlybuilt.org)
12.23.09 / 4pm


The proposition of an architecture responding in real time to the forces of the outside world, whether they are physical, political, or economic, is an interesting one. Especially when considering the argument that architecture is out of pace with the dynamics of society. 


In the words of Koolhaas, in an interview with Charlie Rose: “…almost no ambition, and no coalition, and no agreement lasts for more than 3 years. So there's a painful discrepancy between the slowness of architecture and the constant turmoil with which everything moves and changes and mutates…” 


From that perspective, The Storm is an interesting thought experiment. Great stuff.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.23.09 / 5pm
	
	
	Maurits: As always, Koolhaas sounds good—until one examines what he says closely. His pronouncement is valid only if we accept his tacit assumption that architects wait for clients to commission projects and contractors to build them. But isn't design a form of anticipating the future? Aren't architects, therefore, responsible for anticipating, at least a few years in advance, the developments Koolhaas speaks of? If architects design for the future, they may not always get the particulars right, but they can design according to principles (that they or others devise) that allow for future adjustments according to unknown developments at the time of design. The contemporary architect has no excuse for being “slow.”
	
	
	
	
	
		1. [Maurits](http://www.nightlybuilt.org)
		12.23.09 / 6pm
		
		
		Koolhaas used this argument to motivate the founding of conceptual think tank AMO as a counterpart of his office OMA. As he argued in an article in [Wired](http://www.wired.com/wired/archive/8.06/koolhaas_pr.html) some time ago, AMO may even recommend ‘operations of erasure', thereby challenging the in his view automatic response of architects to deliver a building in response to any problem they encounter. 
		
		
		I am not sure how OMA and AMO ought to interact. It has the appearance though that it is either/or, rather than and/and, and that there seems to be no interaction at all. In that case, AMO just provides an escape from the daily sores of the architectural practice; thereby disregarding the role architecture has in shaping the physical appearance of society. 
		
		
		We rather need to combine both approaches. I think you are right. We need to design a conceptual framework in anticipation to any commissioned work that comes along. Whatever that framework may be.
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		12.24.09 / 3pm
		
		
		Maurits: Modernism offered the conceptual framework of ‘universal space,' as advocated by Mies and practiced by ‘open office' designers. Post-modernism offered the concept of ‘collage,' as advocated by Rowe and Venturi and Scott-Brown, and also ‘layering,' an outcome of collage. I have advocated what amounts to (for lack of a better term) the permanently ‘unfinished project,' which is the product of continuous collaboration between designers and those who inhabit spaces—the World Center project being one of many examples. Of course, other frameworks are possible, and necessary.
5. Paul
1.3.10 / 5am


I wonder if there is a way to marry the currently practiced ‘commission and contract' approach to the production of architecture and the imagination of architectural futures yet to be (and likely never to be) commissioned. Lebbeus, the theoretical musings here seem to hearken to your Malecon proposal. Do we need more than just a (or many) framework(s) of architectural thought, i.e. modernism etc.? Something that might provide a platform of presentation to bridge the creativity of architects and designers so apparent in this project with the ‘real-world' issues of economics, politics, and policies? Maybe a new institution, like a board of trade, or rather, a board of design open to new design solutions to public problems could usher design imaginings into the public and governmental spotlight, so that such projects could become commissionable.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.3.10 / 4pm
	
	
	Paul: You go right to the heart of the problem: the lack of solidarity among architects. They are so engaged in competing with each other for commissions that they cannot join together in any way to demand basic changes in the way architecture is practiced. Existing professional organizations are supportive of current ideas of competitive practice in the usual ‘free-market' sense. The only solution I can see is for leading architects to become very vocal publicly about the need to reorder architecture's priorities and, if necessary, go on strike. Some form of radical group action is required, but is very unlikely.
7. [Maurits](http://www.nightlybuilt.org)
1.4.10 / 1pm


Lebbeus, Paul: it seems to me we have to move from a ‘product' approach to a ‘service' approach. We have to convince our clients not to pay us for the delivery of a set of drawings, but for guidance in the process of design and construction. 


As a consequence, our contracts will have to be hour-based rather than activity-based. Much like software-architects are commissioned, because in the end, the work they do does not differ that much from the work we do, as ‘hardware-architects'. It is all about the facilitating processes. They facilitate information-based processes. We facilitate human-based processes. 


This approach would also suit the ‘unfinished project' approach Lebbeus mentioned, as the end of the design activity will no longer be clearly defined. This approach may be more suitable to the dynamics of modern society.
9. Paul
1.7.10 / 12am


I agree… it seems so much of our society is focused on product, not only of architecture but of everything material in nature. Even the conception, design, and construction of the original trade center was completely political rather than social. Moses and Rockefeller with all their power in policy and politics aided so much in the idealism of what the towers would represent and the money they would bring, there was no sight line at all toward those that the towers would serve. The project was solely focused on the Program of 10 million square feet, no matter what the future ‘users' desired. This seems to be outside the realm of aesthetic theory of modernism or postmodernism, and although Mies definitely influenced skyscraper design for better or worse, many decisions like this ignore any input of the architect or his/her collaboration with the public… We do need something better, a framework for projects that take the dialogue of designers and those who will inhabit the spaces that takes place above what will have the best economic maximization.
11. [STORM WATCH – thoughtfox](http://www.cocreate.ca/thoughtfox/?p=36)
1.30.10 / 3am


[…] …/more […]
