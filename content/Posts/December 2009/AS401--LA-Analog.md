
---
title: AS401  LA Analog
date: 2009-12-07 00:00:00
---

# AS401: LA Analog


[![](media/lwblog-laana-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-10a.jpg)


The hypothesis of the analog, as both a teaching and design tool, has been well explained in a number of posts on this blog, so I will not discuss the LA Analog—a construction made by eleven architecture students at the Southern California Institute of Architecture (SCI-Arc) in 2004, under the tutelage of myself and James Lowder—in the same terms. All I will add in that regard is that the project began with individual analyses of specific sites in downtown Los Angeles, progressed through the development of appropriate analogical studies, and concluded with the collaborative construction shown here. In method and conceptual basis the LA Analog is similar to other studio projects, such as the [Buffalo Analog](https://lebbeuswoods.wordpress.com/2009/05/15/as401-buffalo-analog/). In form, however, and its design particulars, it is unique.


 What I would like to do here is discuss the work not as one who participated in its creation, but rather to interpret it as a completed project, as something ‘found.' Architecture, or any work that claims to be strongly related to it, must, in the end, be understood in this way.


 If I came across this construction without any prior knowledge of it, I would certainly have no clue that it had anything to do with downtown Los Angeles, or with a city at all, or, for that matter, architecture. Of course, I might have the same problem with, say, the Seagram Building in New York, if I, emerging from an isolated culture in the Amazon jungle, had never seen that type of building before, or knew how such buildings are normally used. In that case, I could easily see the building as a huge abstract form, a kind of monumental totem (which it is), and nothing more. However, because I know all about buildings, and even a little about ‘architecture,' I immediately understand the huge abstraction is useful in other ways as well. In short, I can understand the relationship of its form to its human purposes. When it comes to urban or architectural analog constructions, most of us are in the same situation as the Amazonians—we can only see them as icons of a sort, at best, and—at worst—as visually compelling but nonetheless pointless abstractions.


 If we start from the position that we know nothing—the position Socrates always took when confronting a question—then the only way we can proceed is by interrogating the object, the analog itself. In the one before us, perhaps the first thing that stands out is its complexity. Even though the construction materials are limited, they appear in a great variety of forms and connections. This fact alone suggests that the whole ensemble is a work expressing a number of different ideas probably designed by different people. To put it another way, it would be difficult imagining only one person designing it all. This impression is reinforced by the three-dimensional grid that dominates the overall form: each cube in the grid holds a different set of forms, as though it was the boundary for one person's design. Upon closer study, we can see that the distinct forms in one cube project in different ways into one or more adjoining cubes, intersecting or interfering with their distinct forms. This, we imagine, is an interactive ensemble: different designers occupied their assigned spaces but also transgressed the given boundaries by entering the spaces of others—with or without permission. This, we can reasonably assume, is a collaborative work, created by both conflict and cooperation.


 From these observations we can deduce the rules that governed the design and construction of this work. A rigid set of regular boundaries as sites for each person's design—an egalitarian grid. A ‘code' establishing limitations on materials that can be used, insuring structural integrity of each part and the whole construction (it must ‘stand up')—the overall project will have continuity, if not unity. The opportunity was given to expand each designed project beyond its boundaries, engaging adjacent projects—establishing a set of ‘dialogues,' hostile or friendly, in an interactive community of separate projects.


 The only thing that remains unclear in this interpretation is the origins of the individual designs. Abstract in nature, composed of differing volumes, structures, lines of connection, the forms seem more or less arbitrary and not formed by any necessity other than geometry and available technologies for shaping and joining the materials. However, even if this is so, then the forms are not in fact arbitrary but controlled, if not altogether determined, by rationally definable factors. The forms have a ‘natural' logic.  


 All these deductions and interpretations do bear resemblance to those we might make of a city. Given the specificity of the forms—indeed, their eccentricity—we might further interpret this construction as analogous to a particular city, not only as it exists, but as it might yet become. This is seen, for example, in the opportunity given designers for sites given as bounded to cross over into neighboring sites, interacting with them in ways that most cities—indeed, capitalist culture—do not allow. Or, in the expansion of the quotidian urban grid fully into three dimensions.


 The LA Analog is an experiment in the laboratory of a school of architecture that allowed play with the rule-structures governing cities that would not occur in professional practice.  Further, their analogous nature allows them to be freely interpreted in ways that, even in the laboratory of the academic design studio, could never be possible if the given context was the actual one. The sheer weight of received reality would dampen any free search for principles. “Don't confuse me with the facts!” goes the old joke. In any search for understanding of cities, their present and future, there is a moment when the weight of history and the intractable urgencies of important but transient political and economic issues overwhelm desires for a deeper understanding. It is at such moments that they must be put aside, at least for a while. The analog is one way of doing this.


 One more thing, for now. There is about this analog construction a sense that there is much to be discovered, learned, and invented; that there are new domains beyond the haze of clichés and stereotypes that we too often use to frame our perceptions of the city, and which cloud our imaginations with conventionality. There is a sense of a mystery to be solved and of a hidden meaning waiting to be understood that has drawn us to cities to begin with and which must be continually renewed. 


LW


[![](media/lwblog-laana-12.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-12.jpg)


[![](media/lwblog-laana-21.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-21.jpg)


[![](media/lwblog-laana-31.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-31.jpg)


[![](media/lwblog-laana-5.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-5.jpg)


[![](media/lwblog-laana-6.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-6.jpg)


[![](media/lwblog-laana-12a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-12a.jpg)


[![](media/lwblog-laana-71.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwblog-laana-71.jpg)


[![](media/la13.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/la13.jpg)



 ## Comments 
1. [Michael Gibbs](http://www.dasl.me.uk)
12.8.09 / 9pm


Great piece…


Architects as collaborators – most importantly with other professionals within the urban environment enhancing the quality of the interventions – so does the education system which is based on the motivation to gain a mark against a set criteria help develop the understanding of an urban environment? Particularly in a collaborative situation where each student would contribute in a different manner – thus warranting a better mark? I may answer my own question by stating that the framework of an academic situation should stimulate and drive students but I am yet to experience that!


The analogue vs. digital discussion continues – my interpretation of this is that on the whole city still operates in an analogue manner and thus our current understandings of the city should be based on this, however, augmentation as generated by Venturi and Scott Brown in Learning from Las Vegas brings the digital world into our understanding of the developing urban environment. Times Square – Picadilly Circus – the shop front, and so on. Further more as the technology advances and the financial situation changes so does the city. A static understanding of an urban environment might not be the question but how a transforming theory could be generated and consistently revised.


Thanks Again, Michael.
3. pedro
12.9.09 / 12am


I will like to do one of those excersices with you, and how my designs look to others so different, will be interesting.
5. Marc K
12.20.09 / 4am


Given the previous analog exercises, I believe that the origin of form is, as you may have stated, not of the utmost importance as it pertains to an academic study such as this. I believe it is a question of the act of collage, given that it is occurring within a school of architecture; designed and fabricated by developing minds. 


Collage is a difficult thing, especially given the mixed ideologies of converging egos, but it seems that within the other analogous studies, an overarching language (that of the grid, framework, etc.) that captivates the others within it…


Is it this hierarchy that allows the disperse fragments to read as one? Or is it the “code” by which they re constructed? I would posit that commonality in material or joinery does not necessarily grant cohesion to disparate languages.
7. [Miller Taylor](http://www.middle-grey.com/images/)
3.23.10 / 4pm


Mr Woods,


Excuse me for being somewhat off-topic, but I am contacting you about the potential of including some of your thoughts on architectural education in the Student Publication of the College of Design at NC State. We have also messaged you on your other site as well, and would like to discuss it further with you if possible. 


As I know you are very busy working on your book, I greatly appreciate any time you can spare.


Thank you.  

[studentpublication@gmail.com](mailto:studentpublication@gmail.com)
