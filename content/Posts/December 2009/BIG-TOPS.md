
---
title: BIG TOPS
date: 2009-12-02 00:00:00
---

# BIG TOPS


[![](media/candela-1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/candela-1.jpg)


(above) Felix Candela's innovative hyberbolic-parabaloid, thin-shell, reinforced concrete ‘umbrella.' Mexico, 1950s.


Roofs are such a basic element of architecture—giving shelter from rain, snow, cold, heat and extremes of light—we often take them for granted. True, modernist architecture, *a la* Rietveld, Mies, Corbusier, and others who followed them, reduced roofs to just another bounding plane in the architectural vocabulary: flat and dull. So be it. However, when it comes to roofing large, open interior spaces, even modernist architects have had to go beyond simplistic abstraction and invent structural systems that can span sometimes great distances without disrupting the spaces beneath them with columns, walls, and other structural supports. Most of the spaces sheltered by big roofs serve straightforwardly pragmatic purposes: warehouses, airplane hangars, markets, sports stadia, pavilions—hardly exalted in the cultural canon Still, they are necessary in some sense that is more than the merely useful.


 Following are works of five of the architects/engineers who innovated structural systems, but also methods of analysis, materials, and methods of construction in the design of what some simply call ‘long span' structures. They brought something poetic to the task, something that brings their work into the realm of architecture.


(below) **Felix Candela, warehouse, Mexico, late-1950s**. Candela used his hyperbolic-parabaloid geometries and thin-shell, reinforced concrete constructions in a variety of uses, from the profane to the sacred.


[![](media/candela-3.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/candela-3.jpg)


(below) **Konrad Wachsmann, airplane hangar, United States, early-1950s.** Wachsmann invented the steel space-frame, including the complex joints making it feasible.


[![](media/wachsmann-22.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/wachsmann-22.jpg)


(below) **Eduardo Torroja, Zarzuela Hippodrome, Spain, mid-1930s**.Torroja was one of the earliest pioneers of  thin-shell, reinforced concrete design, and pushed it to its limits. 


[![](media/torroja-1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/torroja-1.jpg)


(beloe) **Pier Luigi Nervi, airplane hangar, Italy, 1950s.** Nervi virtually invented the two-way, reinforced concrete space frame, constructed with his patented material ‘ferrocemento,' which used high-strength cement and only fine aggregates, achieving unprecedented thinness and lightness.


[![](media/nervi-11.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/nervi-11.jpg)


(below) **Frei Otto's Olympic Stadium, Germany, early-1970s**. Otto was the pioneer of tensile, tent-like structures, which have had limited application up to now, but may have a much bigger future in a world that values lightness, transparency, and transience.


[![](media/otto-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/otto-4a.jpg)


LW



 ## Comments 
1. [generall](http://General.la)
12.2.09 / 1pm


I would like to see a hard-shell inflatable roofing system.. floating.. tethered: either modulated or tesselated : formally referential to your own schizophrenic linearity, or logarithmically proportioned: dancing above some coliseum of space, at the whim of the gestures of wind: light as a feather, and yet behemoth in dimension: Like many barges tied together in a great storm, mediating the forces that animate them.
3. Patrick
12.2.09 / 2pm


!Hola Señor Wood! !Su ”Eduardo Torroja Sports Stadium Brazil” es el Hipodromo de la Zarzuela en Madrid, España!  

!Adiós y felicitationes para su blog!  

Patrick Marechal Ginebra Suiça  

(LW note: Normally I only post English language comments, but this correction is important! Thank you, Patrick.)
5. Solly
2.25.12 / 12pm


hi  

i'm a student of architecture-VERY INTERESTED in everything i've seen on this page  

how/where can i get detail drawings of connections, and specs on the type of wood you use???





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.25.12 / 4pm
	
	
	Solly: These is no easy, single source. I would suggest you begin with researching both particular structures and their architects/engineers—engineering and architectural libraries (such as Columbia's Avery Library) would be the best place to start. However, construction details are the intellectual property of the architects and engineers, and many would not be so generous to publish them.
