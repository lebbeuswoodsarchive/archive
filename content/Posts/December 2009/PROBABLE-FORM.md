
---
title: PROBABLE FORM
date: 2009-12-05 00:00:00
---

# PROBABLE FORM


The late architect Balthasar Holz left behind several series of drawings that have influenced architects of my generation, including me. He also bequeathed to us some of the strangest writings to inhabit the domain of architectural thinking, over which scholars and others in the field continue to puzzle today. One example: **“Only those who love architecture can destroy it.”** Another: **“The danger of the false optimist is that he probably an architect.”** In any event, the following is a selection of his aphorisms or comments concerned with ideas of ‘transformation' that are thought to illuminate his series of drawings I have elsewhere called the “White Drawings.” Given their actual appearance, I now call them “transformers.” Ironically, their physical state, deteriorating because of conditions in which they were stored for many years, have become further, if unintended (?), illustration of Holz' probing, sometimes disturbing, often arguable insights.


Holz apparently believed that the act of designing is no more than the beginning of a process of formation and reformation, that is, trans-formation, that continues far beyond the architect's domain of intention and control. The emergence of any variable along the way changes a building's or space's form for the rest of its evolution, and these variables are usually unpredictable. Hence, design is important, but limited in its effects. Holz's contempt for architects who set out to create ‘masterpieces' is barely concealed in his aphorisms, and this tends to give an impression that architecture would be better off without them. However, I think Holz actually believes that architects are absolutely important as initiators of a transformative process.  Designing, in effect, sets rules by which all future transformation is perceived and handled. He is, it seems, demanding more of architects than most presently give—namely, that they factor into their designs the subtle and variable impact on architectural form of the unpredictable and the unknown.  


A note about terms: when Holz writes of “frames,”  it is probably in the same sense as the physicist Ernst Mach's “inertial frames” that establish relative spatial relationships, but also the “frames” defined by sociologist Irving Goffman as the conceptual devices though which we perceive the world and each thing in it.


LW


 **“Time condemns us to change. We would rather not change, but we have no choice.**


**Change does not happen in a sequentially linear way, but simultaneously, in many directions at once.**


**Each thing is growing and decaying at the same time, only at different rates.**


**Change is not defined in a sequence of succeeding frames, but in a matrix of frames that each occupy the same space and moment.**


**Change is not defined by a steady sequence of frames, but unpredictably within a field of probabilities.**


**Architecture is a conflict between differing ways and means, never a harmonious resolution of them.**


**Architecture is a form of conflict from which change emerges, first of all to architecture itself.**


**Architecture that does not bear the traces of conflicts that created it is dead architecture.**


**The conflict of differences in architecture dooms and redeems it.**


**There is no ideal form of an idea, but only a set of equally suitable variations on it.**


**Change provokes variations of an ideal form, or, orders of its probability.**


**The task of the architect is to set in motion, in a particular direction, a chain of events he** **cannot control.**


**Transformation. Transmutation. Transfiguration. Terms that dignify the fate of architecture.**


**Architects, like most people, like what is new and fear what is old.**


**Architects do not understand change, how it works and what it means—and they do not want to understand.**


**Architects want to protect their designs from changes made by others, who they think do not understand them. They are right—the others do not understand and that is exactly their virtue. That is exactly the virtue of the changes they want to make.**


**Architects strive for a moment of perfection—when their building is finished. But as soon as that moment passes, their building begins to decay. A finished building is really unfinished, the first frame of a descent to destruction.**


**Architects must embrace the decay of their buildings, at least mentally. They should forget about perfection, the complete realization of their design, and understand that the only truly finished building is a heap of rubble.”**


[![](media/lwb-trans-3a2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-3a2.jpg)


[![](media/lwb-trans-7a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-7a1.jpg)


[![](media/lwb-trans-6a4.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-6a4.jpg)


[![](media/lwb-trans-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-10a.jpg)


[![](media/lwb-trans-9a1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-9a1.jpg)


[![](media/lwb-trans-12a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwb-trans-12a.jpg)


**Bathasar Holz**



 ## Comments 
1. pedro
12.7.09 / 4am


At the begining of the end what we can choose? All of the ideas, one or a yuxtaposition of all? 


The end of the process, the beginig of the unpredictable.
3. homeless
12.7.09 / 4pm


Hi, I had never heard of Holz before this – thanks for yet another provocative post! I am a student from North Carolina, but I discovered you and your blog in Zagreb in 2007. Its was a brilliant coincidence to stumble upon “War and Architecture” while in the area. Your work and this blog have been invaluable to my education – where as the thinking and criticism of the everyday is often mundane, guiding me in safe steps towards the generic – your work and this blog are continually refreshing and provocative, with certain blogs marking critical points in my architectural endeavor and thus my life. 


After reading this entry I searched in the libraries and online for more resources on Balthasar Holz. Sadly, however, I found none. Is his work in any major publications or does it reside in private collections only? Could you point me in a helpful direction?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.7.09 / 6pm
	
	
	homeless: I wouldn't waste any more time looking for information on Holz—you won't find anything, except here and on my website. Glad you like the LW blog. I'll do my best to keep it going….
5. Flavin J
12.8.09 / 5pm


Great stuff, thanks Lebbeus. What year are these from? (I left email on other site) -FJ
7. [Chris](http://mccormick.cx/chrism)
12.9.09 / 5am


Wow! Thank you so much for this post.


“There is no ideal form of an idea, but only a set of equally suitable variations on it.”


This is also true of software


“Architecture that does not bear the traces of conflicts that created it is dead architecture.”


This is also true of Graffiti.
9. Bernadette
12.10.09 / 6pm


Lebbeus,  

Have you published a book on Holz?  

Bernadette





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.13.09 / 4pm
	
	
	Bernadette: I've worked on such a book since I came upon the work of Holz in 2000, but it's slow going. Information about it, or him, is incredibly difficult to find!
11. marwin bald
12.13.09 / 8pm


i´m loving herr holz´s philosophy, mr. woods.
13. [Balthazar Holz « Living Architecture – Inspiration and Links](http://archiviva.wordpress.com/2009/12/13/balthazar-holz/)
12.13.09 / 10pm


[…] here or on the image above. Holz apparently believed that the act of designing is no more than the […]
15. Oliver Zarandi
12.14.09 / 2am


Hi Lebbeus, good read. I was wondering if it is possible to ask you a few questions, possibly by email? My teacher Alan Read reccomended you, and I read your essay ‘No Man's Land' and found it interesting.
17. [Ninjas on the ‘Net | AMNP](http://architecture.myninjaplease.com/?p=6807)
12.15.09 / 6pm


[…] Lebbeus Woods on architect Balthasar Holz [Lebbeus Woods] Share this […]
19. [General](http://www.general.la)
12.16.09 / 10am


Chris: associating graffiti with conflict for the sake of emphasis is capricious and dare I say irritating. Much graffiti (and art, and architectural proposal for that matter) that ‘bears the traces of the conflicts that created it' can sometimes become immersed in, or lost in such process; and whose results are much less balanced than their intent, or focus. Any ‘creation' needs a bit of tension to give a point of reference to it's harmonies, but too much is simply too much. 


Something quite amazing about the drawings above is the incredible sense of balance that is employed through what could be considered a jumble of lines… however the precision of angles and measurements, and proportions creates an almost irreconcilable harmony within ‘storm' or shattering. Much like the will of nature, a fine line.





	1. homeless
	12.17.09 / 1am
	
	
	I found the connection to graffiti astute. Through comparison and analogy we can see architecture in new ways. 
	
	
	“too much is simply too much”
	
	
	General, you propose a building propped in contraposto with a facade of conflict – it is the classical canon. Also, a recipe for the superficial and a form of cooptation which waters down true expression by an ideological disposition against conflict and extremism – though these are fundamental conditions of humanity. How is too much always too much? This hardly seems simple. Is too much truth, too true? The world is increasingly conflicted, more so then we like to admit and often for moderation's sake. As it was in classic greek society, moderation is a virtue in the US, even if that mean abridging the truth or presenting a false picture. In your sense of the word, balance is a limiting factor, a value not without wisdom, but often used to curtail true expression and prevent the experimental.
	
	
	How can we know balance if we do not know polarity?
21. [General](http://www.general.la)
12.18.09 / 8am


Homeless,  

I believe you have missed the point, entirely.  

Although I can appreciate you finding the graffiti connection astute, as someone who has actively painted graffiti for 13 years, and mind you– not with the obscure or weak-of-heart, I can speak with experience that the connection between conflict and graffiti is not only cliché, but narrow-minded.  

Of course “through comparison and analogy we can see architecture in new ways”.. yet whichever ‘new way' it is, it still must be defined specifically as what ‘it' is. A graffiti piece burdened by conflict is simply a graffiti piece burdened by conflict. An architecture that transcends its process is architecture that has transcended its process.  

As far as the assumption that I would prefer “a building propped in contraposto with a facade of conflict”… well, not only is that not the case, i'm having trouble locating your sources.  

 I'm merely stating, works that become overly immersed in their own conflict can become distant from their purpose, no longer resemble their objective, and tend to become conflict itself. There IS such thing as losing focus.  

As far as my preference towards ” a recipe for the superficial and a form of cooptation which waters down true expression by an ideological disposition against conflict and extremism – though these are fundamental conditions of humanity.”… well, I am again advocating moderation. War seems to be a fundamental condition of humanity, and i'm not much of a fan of that, either. Perhaps this is an ideological disposition against extremism– yet specifically, and particularly, any accusation beyond the topics within this conversation is purely speculative, at best. I most certainly do believe in true expression.. enough to live it… yet I still prefer a true expression of balance, and a responsible expression of extremism.  

To address what I do believe in (and I find any continuance of this chat awfully inappropriate for a blog dedicated to Lebbeus) I would suggest you emailing me.  

In response to “how is too much always too much?”… I wrote ‘simply too much', not ‘always too much'.  

..And too much truth, IS too much truth.  

Likewise– Balance, primarily, is the function of a center of gravity.
23. [Architects strive for a moment of perfection—when their building is finished. But as soon as that moment passes, their building begins to decay. A finished building is really unfinished, the first frame of a descent to destruction. | Sites+Assemblies](http://1dd.me/wordpress/?p=59)
4.15.12 / 6pm


[…] Balthasar Holz, as quoted by Lebbeus Woods […]
