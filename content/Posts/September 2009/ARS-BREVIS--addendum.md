
---
title: ARS BREVIS  addendum
date: 2009-09-12 00:00:00
---

# ARS BREVIS: addendum


The reality that knowledge has to be reinvented by individuals in each new generation has an upside and a downside.


The downside is that civilization, which depends on knowledge of ourselves and the world and how they work together, is fragile. It can be lost in a single generation, if the struggle for knowledge is abandoned in favor of simplistic thinking. This, indeed, happened in the last century—think of Naziism to start with—and could just as easily happen today, if people act out of indifference or fear and not compassion, curiosity, and reason. Civilization could slip back into barbarism.


The upside is that each individual, and each generation, can invent their own versions of the realities of being human, and on a changing planet. Nature is alive, and we are part of its aliveness. The wonder of discovering all over again what it is to be human and a part of a living, changing nature comes with being born into a world we did not create but, paradoxically, must each reinvent from our own experiences.


Today is, as they say, a challenging time, as most times have been, and perhaps more so now because of the lightning speed at which global events that affect all of us unfold. There is so much to be lost, and gained. All depends, it seems, on our commitment to knowing.


LW



 ## Comments 
1. [Pato](http://www.aparienciapublica.org/)
9.15.09 / 9pm


Great reflection that remind me a kind of ethical question architecure must always answer: how should be our world?
