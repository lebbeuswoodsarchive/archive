
---
title: UNDERGROUND BERLIN  the film treatment
date: 2009-09-15 00:00:00 
tags: 
    - architecture
    - movies
---

# UNDERGROUND BERLIN: the film treatment


What follows is a treatment (Hollywood slang for story synopsis) with sketches that I made for a projected film in which new forms of architecture—and the way of living they enable—would play a central role. This followed hard on the heels of my experience as a “conceptual architect” for the big-budget movie Alien3. Working on that project, I realized that set designers have no power over how their designs are used, and certainly no influence on the story or its social or ethical implications. So, I decided to write a screenplay that—contained in a melodrama—would project architecture as a vital instrument of social change.


 After writing the treatment, I wrote, together with Olive Brown, a complete screenplay, which significantly expands key ideas in the story. It was shown around in Hollywood and deemed to have “potential.” I became absorbed by other projects, having nothing to do with movies, and never pursued it further, though the idea still intrigues me. I post the treatment here, together with proto-storyboard drawings, as an item of curiosity, but even more so as an example of how experimental architecture might find its way, however tentatively, into the mainstream of public life.


Note that the treatment and drawings are copyrighted by me, and I reserve all rights to them. Fair Use is, of course, permitted.


![LWb-TITLE](media/LWb-TITLE.jpg)


![LWb-UB18-1](media/LWb-UB18-1.jpg)


![UBfText1-1a-B](media/UBfText1-1a-B.jpg)


![UBfText1-2a-Ba](media/UBfText1-2a-Ba.jpg)


![UBfText1-2b-B](media/UBfText1-2b-B.jpg)


![LWb-UB19-1](media/LWb-UB19-1.jpg)


![UBfText1-3a-B](media/UBfText1-3a-B.jpg)


![UBfText1-3b-B](media/UBfText1-3b-B.jpg)


![LWb-UB17-1](media/LWb-UB17-1.jpg)


![UBfText1-3c-B](media/UBfText1-3c-B.jpg)


![LWb-UB23-1](media/LWb-UB23-1.jpg)


![LWb-UB5-1](media/LWb-UB5-1.jpg)


![LWb-UB5-2](media/LWb-UB5-2.jpg)


![LWb-UB5-3](media/LWb-UB5-3.jpg)


![LWb-UB6-1](media/LWb-UB6-1.jpg)


![UBfText2-1a-B](media/UBfText2-1a-B.jpg)


![LWb-UB14-1](media/LWb-UB14-1.jpg)


![UBfText2-1b-B](media/UBfText2-1b-B.jpg)


![LWb-UB16-1](media/LWb-UB16-1.jpg)


![UBfText2-2a-B](media/UBfText2-2a-B.jpg)


![UBfText2-2b-B](media/UBfText2-2b-B.jpg)


![UBfText2-3b-B](media/UBfText2-3b-B.jpg)


![LWb-UB25-1](media/LWb-UB25-1.jpg)


![LWb-UB1-1](media/LWb-UB1-1.jpg)


![LWb-UB1-2](media/LWb-UB1-2.jpg)


![LWb-UB13-1](media/LWb-UB13-1.jpg)


![LWb-UB13-2](media/LWb-UB13-2.jpg)


![LWb-UB24](media/LWb-UB24.jpg)


![UBfText3-1a-B](media/UBfText3-1a-B.jpg)


![UBfText3-2a-B](media/UBfText3-2a-B.jpg)


![UBfText3-2b-B](media/UBfText3-2b-B.jpg)


![UBfText3-3a-B](media/UBfText3-3a-B.jpg)


![UBfText3-3b-B](media/UBfText3-3b-B.jpg)


![UBfText3-3c-B](media/UBfText3-3c-B.jpg)


![UBfText4-1a-B](media/UBfText4-1a-B.jpg)


![LWb-UB26-1](media/LWb-UB26-1.jpg)


![UBfText4-1b-B](media/UBfText4-1b-B.jpg)


![UBfText4-2a-B](media/UBfText4-2a-B.jpg)


![LWb-UB20-1](media/LWb-UB20-1.jpg)


![LWb-UB20-2](media/LWb-UB20-2.jpg)


![LWb-UB21-1a](media/LWb-UB21-1a.jpg)


![LWb-UB21-2](media/LWb-UB21-2.jpg)


![UBfText4-2d-B](media/UBfText4-2d-B.jpg)


![LWb-UB22-1](media/LWb-UB22-1.jpg)


![UBfText4-2b-B](media/UBfText4-2b-B.jpg)


![LWb-UB15-1](media/LWb-UB15-1.jpg)


![LWb-UB15-2](media/LWb-UB15-2.jpg)


![UBfText4-2c-B](media/UBfText4-2c-B.jpg)


![LWb-END](media/LWb-END.jpg)


 


For reference, I include the following images (models and drawings) from the original Underground Berlin project, which was made to find an ‘unauthorized' way to reunite the city divided into East Berlin and West Berlin. The secret tunnels and experimental Living-laboratories:


![LWb-UB6](media/LWb-UB6.jpg)


![LWb-UB7](media/LWb-UB7.jpg)


![LWb-UB10](media/LWb-UB10.jpg)


![LWb-UB8](media/LWb-UB8.jpg)


![LWb-UB2](media/LWb-UB2.jpg)


A Projection Tower in the Alexanderplatz:


![LWb-UB1](media/LWb-UB1.jpg)


![LWb-UB9](media/LWb-UB9.jpg)


LW


#architecture #movies
 ## Comments 
1. PEstebanGL
9.16.09 / 4am


Who can't denied architecture determines society?


¡Espectacular!  

Any other word.
3. [Link love for September 16th — Sympathy for the Robots](http://www.sympathyfortherobots.com/2009/09/16/link-love-for-september-16th/)
9.16.09 / 6pm


[…] Underground Berlin: the film treatment by Lebbeus Woods. […]
5. [Daily Digest for September 16th](http://howtoimplode.com/?p=325)
9.16.09 / 8pm


[…] Shared UNDERGROUND BERLIN: the film treatment […]
7. [Francisco Vasconcelos](http://www.csx-xmz.com)
9.24.09 / 2am


Hallo Wie Getz …


I'm an Portuguese starting architect and i really got intriged by your movie plan, i wanted to stated that if possible i would like to help in any way possible to make this project live, i'm photographer and 3D animator, and i have some friends starting in cinema industry. If anything i can help please let me know. It's a wonderfull idea to show how architecture envolves society (this theme is my wifes master research).
9. Paul
9.24.09 / 10pm


Dear Lebbeus,


very thought provoking piece of work. I'm currently reseraching the governing building typology. A a starting point I'm looking at the Athenian forum, an open architectural device that was provacative of political discouse in everyday life. I'm going to explore this evolution of the governing type and if the evidence is compelling enough (as i predict it will be) I will arrive at a documented and considered justification for a new type. Contemporary politics can appear to focus around intimately composed planning rather than fierce political posturing. Current democracy is failing, let Architecture liberate. 


Any thoughts?
11. Pedro Esteban Galindo Landeira
9.25.09 / 2am


Paul  

“the governing building typology”, please more about that for the students, it sounds quite interesting.
13. egrec
9.25.09 / 4am


This ought still be made — if not filmed, then animated; if not animated then as a graphic novel; if not a graphic novel then a prose novella. It should seep into young and imaginative minds, where it would surely transform and inspire…
15. dave
10.1.09 / 9pm


Lebbeus–these are gorgeous drawings; I'm curious about your technique, especially in the film stills. How do you achieve the textured coloration? I've seen it in other drawings of yours; is it something you add digitally to the images, or do you do it by hand?
17. [gameworlds - mammoth // building nothing out of something](http://m.ammoth.us/blog/2009/10/gameworlds/)
10.1.09 / 10pm


[…] by the citizens of SimCity and Cities XL so dull? Where's the game that lets us manage Underground Berlins, New Babylons, or Cable Cities? You know you'd play […]
19. [baskonusbirakdinle aka pushtalkreleaselisten](http://baskonusbirakdinle.wordpress.com/2009/10/09/28/)
10.9.09 / 12am


[…] has a marvelous blog and one of the newest entry is a film treatment that is named as Underground  Berlin. It is like a comic more that a scenario […]
21. David J
10.21.09 / 5pm


i would first all have to say i think tis should be produced as an animation of sorts.


secondly i would like to ask as to, if there are any, why was Berlin chosen as the setting for this story?
23. [actor/network narratives « Ancientevening's Blog](http://ancientevening.wordpress.com/2009/11/05/actornetwork-narratives/)
11.5.09 / 4am


[…] Lebbeus Woods: Underground Berlin – the film treatment. […]
25. [Lebbeus Woods: Underground Berlin | AMNP](http://architecture.myninjaplease.com/?p=5870)
12.1.09 / 5pm


[…] was simply going to link to this post over at Lebbeus Woods' blog, UNDERGROUND BERLIN: the film?treatment, in the sidebar – but I was worried that people might not follow the link without seeing some […]
27. [Comicarchitekturen - tektorum.de](http://www.tektorum.de/andere-themen/6237-comicarchitekturen.html#post37381)
1.26.10 / 10pm


[…] Comicarchitekturen #9 (Permalink) Social Bookmarks: bei BLDGBLOG gefunden: UNDERGROUND BERLIN: the film treatment LEBBEUS WOODS \_ \_ \_ \_ \_ vulkanbros.com \_ viktor antonov \_ \_ \_ \_ \_ bei Baunetzwoche gefunden: […]
29. nfgayle
7.17.10 / 2am


I would think now would be the time to resurrect this project as an animated feature. I think the story and the medium proposed practically give themselves to the other. 


Consider it please.
31. [Underground Berlin « AntiTerra](http://antiterra.blogsport.eu/2010/08/17/underground-berlin/)
8.17.10 / 4pm


[…] Woods Konzept für seinen Science Fiction Film Underground Berlin über eine Architektin, einen verschwundenen Zwillingsbruder, Neo-Nazi Aktivitäten in der […]
33. [\_ at zeitverschwendung](http://zeitverschwendung.blogsport.eu/2010/08/17/_-15/)
8.17.10 / 4pm


[…] BLDGBLOG: Fiktionale Architektur. Ein spannendes Beispiel ist das unrealisierte Konzept für den Film Underground Berlin. […]
35. [Die Welt ist gar nicht so. » Linkschleuder (15)](http://blog.dieweltistgarnichtso.net/linkschleuder-15)
9.24.10 / 11am


[…] UNDERGROUND BERLIN: the film treatment (via) […]
37. [Julian McCrea](http://portalentertainment.co.uk)
10.20.10 / 3pm


Just came across this. Absolutely lovely.
39. [# CINEMA /// Underground Berlin by Lebbeus Woods | The Funambulist](http://thefunambulist.net/2010/12/16/cinema-underground-berlin-by-lebbeus-woods/)
12.16.10 / 6pm


[…] weeks ago a storyboard/synopsis about a movie he wrote called Underground Berlin. You can see it on his blog and read this Underground Research Station under Berlin being explored by Amelia and Leo and you […]
41. [Underground Berlin | The Multi Collective](http://anfsfoundry.co.uk/Multi/?p=162)
5.23.11 / 10pm


[…] Underground Berlin – The Film Treatment Add your comment 0 Responses to this post Add your comment […]
43. [Underground Berlin | Uma representação do futuro « Feed The Reader](http://feedthereader.com/2011/08/underground-berlin-uma-representacao-do-futuro/)
8.18.11 / 12am


[…] da história, com um convite ao click para uma melhor visualização. Para toda a história, ‘click here‘.   var disqus\_shortname = 'feedthereader'; var disqus\_identifier = […]
45. [know it all… « hickcoxmfp](http://hickcoxmfp.wordpress.com/2012/01/23/know-it-all/)
1.24.12 / 5am


[…] the comment from Dave about “complexity of function.”   Take a look at Lebbeus Woods' blog, [https://lebbeuswoods.wordpress.com/2009/09/15/underground-berlin-the-film-treatment/](https://lebbeuswoods.wordpress.com/2009/09/15/underground-berlin-the-film-treatment/ ) This image is ‘Underground Berlin, a film treatment' he did in 1988. Share […]
