
---
title: SHANNON MATTERN  the BLDGBLOG Book
date: 2009-09-12 00:00:00 
tags: 
    - architectural_publications
    - architecture_blogs
    - book_review
---

# SHANNON MATTERN: the BLDGBLOG Book


## Blog to Book, Space Beer to Ballard: The BLDGBLOG Book


**Geoff Manaugh, The BLDGBLOG Book: Architectural Conjecture, Urban Speculation, Landscape Futures (San Francisco: Chronicle Books, 2009).**


 In the past few years there have been several exhibitions, conferences, talks, and written texts examining the present and future of architectural publication—how it's responded to commercial demands; how consumer, trade, and academic publications are related; how publishing has been informed by digital technologies; what contemporary architects and publishers might learn from the little magazines of the early 20th century and the zines of the 60s and 70s, etc. Explicit or implicit in these discussions and presentations is an interest in publication form—in the materiality of architectural discourse. Geoff Manaugh's The BLDGBLOG Book (Chronicle, 2009), in both its content and its form, sheds light not only on the architectural and urban issues addressed on its pages, but also on the future of publishing in these fields.


 For several years I've enjoyed Manaugh's (almost) daily musings on BLDGBLOG, his popular blog on “architectural conjecture, urban speculation, [and] landscape futures.” Manaugh typically starts off, in traditional blog-style, with a link to an article he's read in The New York Times, a quoted passage from a J.G. Ballard story, or some satellite images he's found online or photos he's taken on a recent trip. But then, unlike your typical blogger, he launches from those references into provocative, often fanciful, speculation—about the relationships between architecture and climate change, about how a particular building would make an ideal setting for a movie about urban pirates or hypochondriac aliens. He writes stories—“architecture fictions”—that often interpellate “you,” the reader, as the central character. His posts are much longer than most blog posts, and they include much more original “content.” Manaugh doesn't simply respond to the found texts or links that inspire each post; he uses his found material to conjecture about architecture's potential relationship to seemingly disparate realms of experience, to speculate about its potential future impact on everything from political matters of global importance to the microscopic details of everyday life.


 Manaugh's purpose is nothing short of changing the way we talk about architecture and urbanism. BLDGBLOG's multiple voices, its integration of ideas from myriad disciplines and practices, its melding of popular culture and academic theory, expand the architectural field and its audience. Manaugh encapsulates the ideology of BLDGBLOG in a manifesto he published in Icon magazine in August 2007:


If architects, critics, historians, bloggers, professors, journalists, construction magnates, city planners, etc. really want to talk about architecture, in a way that has any meaning at all for anyone who actually lives in this world (and who doesn't teach at Columbia), then they need to talk about architecture in its every variation: whether a structure is real or not, built or not, famous or not, or even standing on the surface of the earth…. Stop limiting the conversation.


BLDGBLOG promises to eradicate these limits. Manaugh intends to get people from all walks of life to realize that “the everyday spatial world of earthquake safety plans and prison break films – and suburban Home Depot parking lots and bad funhouse rides – is worthy of architectural analysis” (BBB 32). But his work is not only about educating everyday people; it's also about valuing what they think about the spatial world: “what do janitors or security guards or novelists or even housewives – let alone prison guards or elevator-repair personnel – think about the buildings around them?” (quoted in Nissley). Manaugh explores these other subjectivities through his architecture fictions.


“If academia is to be believed, then architecture is something in the drawings of Vitruvius or Le Corbusier; if today's architecture critics are to be believed, architecture is just parametrics and Zaha Hadid. Either way, it's claustrophobic” (BBB 14). BLDGBLOG intends to expand architecture, urbanism, and landscapes – to make them into fields that non-practitioners and non-academics can understand and contribute to. “Forget academic rigor,” Manaugh advises (BBB 11). He is intent in particular on breaking Deleuze's monopoly on architecture thinking. The book, Manaugh explains, is “not theoretically rigorous or disciplinarily loyal or beholden to one particular style of design – even one historical era – but that's the point” (BBB 11).


Critics and admirers have described Manaugh as “omnivorous” and his blog as a wunderkammer. What ties everything together, however – at least in theory – is its connection to architecture and landscape. Through this connection-making, Manaugh says, he has cultivated a particular “voice, a tempo, an energy, a feel” for the blog – and over the years, the blog has formulated “its own kind of genre: architectural criticism as a kind of literary form. Somewhere between science fiction, a short story collection, a Don DeLillo novel, and a kind of technical catalogue for a world that didn't exist” (quoted in Sellars). BLDGBLOG isn't your typical blog, so it seemed perfectly fitting when Manaugh announced to readers in 2007 that it would take on a new form: BLDGBLOG would become a book. Many of Manaugh's blog posts had been more like short essays or mini book chapters, anyway. And over time these mini-chapters had organized themselves into particular liberally defined themes that would make sense to folks familiar with the way Manaugh's brain works – themes like “Architectural Conjecture, Urban Speculation,” “The Underground,” “Redesigning the Sky,” “Music/Sound/Noise,” and “Landscape Futures” – and which, if polished, seemed to make for provocative and fitting book chapters.


 Manaugh wanted to exploit the specific material qualities of the printed book. The increased length of the print form, and the fact that reading a book is different from reading a blog, would allow for longer, more sustained explorations in the book of particular ideas and themes from the blog. Manaugh uses an architectural metaphor to explain how he approached this writing at a new scale: “It was a bit like someone who's only been given enough space to assemble doll houses suddenly finding literally acres of space in which to explore the construction of new rooms and hallways – imagine the mazes of space you could build then!” (quoted in Nissley). And textual mazes he does build: each chapter is broken into multiple, often only tenuously linked, sections. In several blog posts and interviews, Manaugh has argued that alternative media formats, like travel guides, comic books, and playing cards, might be more appropriate than traditional publication forms for the presentation of architectural or urban research, so, predictably, inside the book he has included a mix of long and short essays, interviews, architectural proposals, photos, newspaper clippings, original artwork, and comic strips. Individual pages often feature detouring sidebar stories, and chapters are broken up with interstitial “short, bloglike minichapters” on differently colored paper (BBB 35). The exterior hints at what's in store inside: an assortment of images on the book's cover convey the diversity of its contents, and the table of contents is printed on the book's spine.


 This is a book that materializes the editorial strategy, writing style, and multimedia format of a blog in print form. It's undoubtedly a hybrid format, a new genre of publication. An autograph page at the end, where readers are encouraged to collect the signatures of the book's contributors, combines the interactivity of the web with the materiality and “aura” of the book. The “Further Reading” section includes references to printed texts, websites, films and radio shows. The book's conditions of production are also a blend of print and web publishing. When Manaugh announced his book contract to BLDGBLOG readers in May 2007, he invited them to write in with suggestions. Its contents are thus the material manifestation of online discussions; it is in part a participatory production. The resulting volume resembles a blog in that Manaugh functions as both author and curator “linking” to content created by others, although in the production of the printed book Manaugh of course faced physical and material demands that he needn't contend with in publishing his blog.


 The book's wide-ranging content, the collaged formats, the myriad voices from disparate fields (his interview with DJ/Rupture is a personal favorite) together define the book's subjects – architecture, urbanism, and landscape – as expansive, flexible, mutable. It's certainly not claustrophobic. Yet the juxtaposition of topics and presentational styles within the text does sometimes test the book's rhetorical flexibility. Manaugh indicates that the book can be skimmed, read out of order, revisited – but the linear format of the book, and all of its deeply ingrained conventions of reception, occasionally make for jarring leaps of logic. Chapters do not unfold as a singular, integrated argument; instead, they are composed of loosely related themes, which, because they're typeset such that one theme seems to flow into the next, suggests a logical connection that isn't always there. We skip from sleep labs to rice genes to World War II enemy aircraft warning devices.  Sidebar essays, set in a sans serif font to distinguish them from the main text in each chapter, occasionally explode from their marginal locations to occupy entire pages, and thus disrupt the narrative.


 I realize that in regard to my comment about the layout's spatial and logical leaps Manaugh may again explain: “that's the point.” It's all about playful, creative juxtaposition. He admits that translating the blog to book required him to set constraints – that forcing himself “to relate things back to architecture has made it all feel more like a game:” “Six Degrees of [Architectural] Separation” (BBB 14). Yet it sometimes feels like a game to me, too – as if my challenge as a reader is not only to tease out the links between adjacent parts on the page, but also to discern the relationship, which isn't always clear, between each essay, each sidebar, each clipping, each image, and the chapter's theme. The individual flights of fancy and provocative tales that Manaugh relays in 200- to 1000-word bites on the blog (big bites by blogging's standards, of course) are more easily digestible than the monumental tasting menu Manaugh presents in the book. I enjoy encountering a BLDGBLOG speculation – one of Manaugh's many “what if…?” stories – online in the morning, then ruminating on it for the rest of the day. But in the book, one “what if” after another is a bit too much speculation for a double-page spread or a single reading session. On the blog, I enjoy following Manaugh as he draws connections between space beer and horticulture, or between fairy tales and bank robberies, but when a single spread of the book takes me from crystal cities to the International Space Station to near-death experiences to Rogaine, my brain starts to hurt a little. I have to figure out what these things have to do with one another, and what they have to do with architecture and urbanity. Lawrence Weschler's Everything That Rises: A Book of Convergences is also about seemingly random connections – but Weschler, who blurbed Manaugh's book, usually sustains his connections over several pages, using multiple sets of images to cement the links. Everything's wide margins, heavy leading, and page breaks give the reader space and time to ruminate on the connections proposed, and clearly indicate where the reader should seek a connection, and where she should take a breather. The sheer volume, pace, and density of Manaugh's book – although indeed a beautiful object, as many have noted – is cognitively draining. Rather than feeling claustrophobic while reading it, I feel a wee bit schizophrenic.


 If “alles ist architektur,” as Manaugh, following Hans Hollein, aims to convince us, we should consider the architecture of the medium as well. The architecture of code, graphic architecture, physical architecture – and their corresponding rhetorical, poetic, and political architectures. Blogs and books are different intellectual forms, which present their content differently and require different structures of engagement. Conversely, some subjects and arguments lend themselves to presentation better in some forms than in others. The BLDGBLOG Book is a different species than its digital sibling. For me, that Manaugh style of speculation – the wunderkammer that is BLDGBLOG – is in its best form online. I'll stick with the blog.


 SHANNON MATTERN


 Works Cited


Geoff Manaugh, Manifesto #46 Icon (August 2007): Accessed online on August 10, 2009 at [http://www.iconeye.com/index.php?option=com\_content&view=article&catid=46:manifestos&id=3004:manifesto-46-geoff-manaugh–blogger](http://www.iconeye.com/index.php?option=com_content&view=article&catid=46:manifestos&id=3004:manifesto-46-geoff-manaugh--blogger)


Tom Nissley, “Building The BLDGBLOG Book: Questions for Geoff Manaugh” Omnivoracious (August 12, 2009): Accessed online on August 10, 2009 at <http://www.omnivoracious.com/2009/08/building-the-bldgblog-book-questions-for-geoff-manaugh.html>


Simon Sellars, “The Politics of Enthusiasm: An Interview With Geoff Manaugh” Ballardian (November 7, 2006): Accessed online on August 10, 2009 at <http://www.ballardian.com/politics-of-enthusiasm-geoff-manaugh-interview>


 Lawrence Weschler, Everything That Rises: A Book of Convergences (San Francisco: McSweeney's, 2006). 


**More on Shannon Mattern:**


<http://newschool.edu/mediastudies/faculty-subpage.aspx?id=4294967747>


**More on the BLDGBLOG book:**


<http://www.amazon.com/BLDGBLOG-Book-Geoff-Manaugh/dp/0811866440/ref=sr_1_1?ie=UTF8&s=books&qid=1251238761&sr=8-1>


#architectural_publications #architecture_blogs #book_review
 ## Comments 
1. [Daily Digest for September 12th](http://howtoimplode.com/?p=311)
9.12.09 / 3pm


[…] Shared SHANNON MATTERN: the BLDGBLOG Book […]
3. [Globo.com | SHANNON MATTERN: the BLDGBLOG Book « LEBBEUS WOODS | Globo.br.com - Impreterivelmente Notícias, esporte, lazer vídeos](http://www.globo.br.com/shannon-mattern-the-bldgblog-book-%c2%ab-lebbeus-woods/)
11.28.10 / 6pm


[…] Mais: SHANNON MATTERN: the BLDGBLOG Book « LEBBEUS WOODS academic-rigor, bbb, deleuze, loyal-or-beholden, manaugh, monopoly-on-architecture, […]
