
---
title: THE VAGRANT LIGHT OF STARS
date: 2009-09-27 00:00:00
---

# THE VAGRANT LIGHT OF STARS


The Einstein Tomb project was created as a memorial to the life and work of Albert Einstein, a symbolic structure in the same spirit as Boullee's Cenotaph to Isaac Newton. Because the self-effacing Einstein—who transformed physics as much as Newton before him—explicitly stated that after his death he wanted no such memorial as a site of veneration, I designed it to be launched into deep space, traveling on a beam of light, never to be seen in terrestrial space and time. However, owing to the gravity-warped structure of space (which Einstein's greatest work—his theory of gravitation—described) it would return to Earth in sidereal time, an infinite number of times, or at least until the end of time and space at the death of the universe.


This project was created for and published as number six in the Pamphlet Architecture series, which was begun by Steven Holl and William Stout, and is now in the able hands of the Princeton Architectural Press.


It was presented with some remarks of mine at the Storefront for Art and Architecture in New York, on Saturday, September 26, at a book-launch event for Geoff Manaugh's BLDGBLOG book, in which it appears. 


![LWB-ETbk-cover2](media/LWB-ETbk-cover2.jpg)


![ETbk-1a](media/ETbk-1a.jpg)


![ETbk-1b](media/ETbk-1b.jpg)


![ETbk-2a](media/ETbk-2a.jpg)


![ETbk-2b](media/ETbk-2b.jpg)


![ETbk-3a](media/ETbk-3a.jpg)


![ETbk-3b](media/ETbk-3b.jpg)


![ETbk-4a](media/ETbk-4a.jpg)


![ETbk-4b](media/ETbk-4b.jpg)


![ETbk-5a](media/ETbk-5a.jpg)


![ETbk-5b](media/ETbk-5b.jpg)


![ETbk-6a](media/ETbk-6a.jpg)


![ETbk-6b](media/ETbk-6b.jpg)


![ETbk-7a](media/ETbk-7a.jpg)


![ETbk-7b](media/ETbk-7b.jpg)


LW



 ## Comments 
1. Alan
9.27.09 / 11pm


Thanks for posting this work. And thanks for your excellent presentation at the book launch.
3. [Daily Digest for September 28th](http://howtoimplode.com/?p=349)
9.28.09 / 7pm


[…] Shared THE VAGRANT LIGHT OF STARS […]
5. [Mason White](http://www.infranetlab.org)
9.29.09 / 3am


Enjoyed your talk on Einstein, speed of light, and cycles. Also I often use your text ‘After Forms' as a polemic in my first-year MArch studio at U Toronto. It is a great piece.
7. [The Vagrant Light of Stars « Fire EXIT](http://fireexit.wordpress.com/2009/09/29/the-vagrant-light-of-stars/)
9.29.09 / 6pm


[…] pm on September 29, 2009 | # | 0 Tags: Architecture, Art, Science The Vagrant Light of Stars The Einstein Tomb project was created as a memorial to the life and work of Albert Einstein, a […]
9. aitraaz
9.30.09 / 8pm


Perhaps the debate between Einstein and Bergson (1911) regarding the theory of general relativity still has relevance – the debate between ‘rationailty' vs ‘intuition,' space-time vs. time-space.


With Einstein, the ‘metaphysics of spatialization' can be traced back to Newton (Descartes, pure ‘extensivity'), with Bergson a duration “heterogenuos, qualitative, creative” – “Time is invention, or it is nothing at all.”


I have always associated (for whatever reason) your works more with Bergson than Einstein, maybe for their temporal qualities or the fact that they demand such invention from their inhabitants (or perhaps its the technology that demands it of them), or, as Deleuze once put it:


“The people no longer exist, or not yet…”





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.1.09 / 12am
	
	
	aitraaz: Bergson was a philosopher and Einstein a hard-core physicist. As far as I know, Einstein never drew philosophical implications from his theories, though others did. Bergson't ‘elan vital' had about it a rather mystical quality—so, for example, it was never formulated mathematically. Bergson certainly has the greater appeal to artists and other creative folk, and probably I am his child more than Einstein's, as you observe. However, I found Einstein the more appealing of the two because, while he was set on rationality, his theories were so utterly fantastic in their consequences. Time slowing down for relative motion closer to the speed of light. Solid measuring rods getting physically shorter for the same reason. Space itself being warped. 
	
	
	Einstein was able to convince other, serious people of the utter reality of such things. Another one: particle-wave duality. In the world of everyday experience, light had to be one or the other. He showed how it could—paradoxically, according to ordinary logic—be both at the same time, opening up the field of quantum physics that changed the way we think and live: micro-chips and micro-processors are a direct result of it, but also we've had to expand our idea of logic to incorporate its uncertainty, unpredictability, complexity.
	
	
	Einstein didn't formulate his ‘theory of general relativity'—theory of gravitation—until 1915. His main interlocutor then was David Hilbert, a mathematician.
11. aitraaz
10.2.09 / 7am


lebbeus: correctly you point out that Bergson was a philosopher (one deeply interested in materialism and physics) and Einstein a physicist (whose great importance has never been put into doubt). A physicist certainly bases his work on certain metaphysical assumptions as does a philosopher concerning science.


Einstein told us ‘God does not dice,' perhaps Bergson would have told us that God does indeed play dice, big time.


Of recent, Bergson has kind of eliptically creeped into empiricist ideas and debates (new materialism, Delanda etc.), so he hasn't been written off as that ‘creative mystic' just yet. Before Newton came Descartes.


No doubt the universe created by Einstein takes Newton's ‘spatialized' universe (rational) to its most extreme and inspiring, as you have mentioned…multiplicities (also conceptually developed by Bergson, but of a metaphysical nature), quantum physics, complexity theory etc.


The cenotaph itself is fascinating, from many points of view.





	1. [Francisco Vasconcelos](http://csxlab.org)
	12.29.10 / 5pm
	
	
	I believe that the Quantum Physics bridges the gap of the metaphysics, as we can observe, this set of phenomenon's at a quantic level, (sub-atomic-scale) where all matter is substantially the same: “atoms”, between this physical and metaphysical levels of matter, there's the phenomenology to where we can try to understand this ocurring phenomenas to our consciousness.
13. [Einstein Tomb @ 30 « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/06/09/einstein-tomb-30/)
6.9.10 / 9pm


[…] <https://lebbeuswoods.wordpress.com/2009/09/27/the-vagrant-light-of-stars/> […]
15. [CONSTRUCTING A REALITY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/07/17/constructing-a-reality/)
7.18.10 / 2pm


[…] my projects and he would always comment. When I sent him a copy of Pamphlet Architecture 6, the “Einstein Tomb” (I was anxious about my understanding of the physics involved), he generously replied that it […]
17. [‘Rembrandt' an Alien on Spaceship Earth « korzacsol](http://kafee.wordpress.com/2010/08/03/rembrandt-an-alien-on-spaceship-earth/)
8.3.10 / 7pm


[…] “The Einstein Tomb project was created as a memorial to the life and work of Albert Einstein, a symbolic structure in the same spirit as Boullee's Cenotaph to Isaac Newton. Because the self-effacing Einstein—who transformed physics as much as Newton before him—explicitly stated that after his death he wanted no such memorial as a site of veneration, I designed it to be launched into deep space, traveling on a beam of light, never to be seen in terrestrial space and time. However, owing to the gravity-warped structure of space (which Einstein's greatest work—his theory of gravitation—described) it would return to Earth in sidereal time, an infinite number of times, or at least until the end of time and space at the death of the universe.”.. see more on his blog. […]
19. [Francisco Vasconcelos](http://csxlab.org)
12.29.10 / 5pm


I bought the Pamphlet 1-10 Edition to read your essay. Loved it.  

Specially the forewords in which i realized that you first meet with Steven Holl in April 1978, I was born at day 6 of that month of that year. Don't know why, but lot's of this coincidences happen, maybe it is … the cosmos.
21. [THE VAGRANT LIGHT OF STARS « LEBBEUS WOODS – Ghost Town Media](http://ghosttownmedia.com/_/?p=211)
1.25.11 / 5pm


[…] <https://lebbeuswoods.wordpress.com/2009/09/27/the-vagrant-light-of-stars/> Author: […]
