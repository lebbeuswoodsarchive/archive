
---
title: LINE-UP
date: 2009-09-19 00:00:00 
tags: 
    - architecture
    - Christoph_a__Kumpusch
    - exhibition
    - Reggio_Calabria
---

# LINE-UP


When the police want to identify the culprit in a crime, they bring all the suspects together and make a ‘line-up' of them. Witnesses to the crime are then asked to look at the suspects–who are put under bright lights—from a darkened vantage point, so they can make their identification of the criminal anonymously. The suspects, however, must suffer the humiliation of being publicly displayed ‘in bad company.' Such is the ignominy of fame or, as it were, infamy, because a crime, a violation of laws, is the reason for the line-up to exist.


It is not without interest that most of the suspects in the line-up are innocent of any crime, not to mention the particular crime in question. They are suspicious simply because they happen to resemble, more or less, a given description of the criminal, or were simply at the wrong place at the wrong time. Sometimes, an innocent person in the line-up is wrongly identified by the witnesses. Sometimes, the real criminal. It all depends on the perceptiveness, and the honesty, of the witnesses. The suspects are at their mercy.


And now we come to architecture.


Have crimes been committed, crimes against humanity? Have architects broken the rules, violated the laws of common decency, of responsibility for the welfare of others, purely for their own gain and gratification? Or, is there, in the personal aesthetics of architects, a personal ethics that sets an example for the common good?


Interestingly, a line-up has been assembled on a rooftop in Reggio Calabria, Italy, in the form of a **Biennale of Architecture and Art of the Mediterranean (BaaM)**, that will open in December of this year. Many of the usual suspects will be in it, and will be exposed in their capsule projects to the bright lights of public display. The witnesses will come, from the Mediterranean, from Europe and other parts of the world, and are obliged, by their presence, to judge for themselves.


LW


Excerpt from the **BaaM** program:


![BaaM-Text-1](media/BaaM-Text-1.jpg)


![Reggio-ALL1](media/Reggio-ALL1.jpg)


![Reggio-ALL2](media/Reggio-ALL2.jpg)


Visitors to the exhibition can walk its entire length through the interiors of the double-sided individual constructions:


![Reggio-ALL3](media/Reggio-ALL3.jpg)


![Overall-1](media/Overall-1.jpg)


![Overall-2](media/Overall-2.jpg)


![Overall-3](media/Overall-3.jpg)


![Overall-4](media/Overall-4.jpg)


![Overall-5](media/Overall-5.jpg)


A closer look at the structure, EARTHWAVE, 6m x 6m x 2.8m, by LW and Christoph a. Kumpusch, with Adam Orlinski. Wood model of Primary Structure:


![WDmodel2a](media/WDmodel2a.jpg)


Wood model of Primary and Secondary Structures:


![EW-MOD1a](media/EW-MOD1a.jpg)


#architecture #Christoph_a__Kumpusch #exhibition #Reggio_Calabria
 ## Comments 
1. aitraaz
9.21.09 / 4pm


Reggio Calabria, interesting region for finding ‘suspects'…


Speaking about Ethics and telluric currents, taking the recent example of the earthquake in L'Acquila, Italy, its' pretty common knowledge that most construction, for many decades onwards, has been done in complete disregard of anti-seismic standards with an pretty surprising low level of construction grade material quality. (eg., concrete with extremely poor reinforcement and also of an extremely poor quality).


Its' also pretty common knowledge that Calabria is in the same conditions, and many experts predict that a ‘catastrophe' is inevitable.


And thus the usual suspects are ‘interpreting' (eg., representing, urgh) the ‘strength' of the ‘telluric force' in Calabria? This is Ethics?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.21.09 / 5pm
	
	
	altraaz: You are certainly right about much new construction in the hills overlooking Reggio Calabria, with the extensive ‘wild building' there. However, I don't understand your reference to telluric forces, which are electro-magnetic, not seismic. In any case, maybe the expression of forces in many of the architects' constructions can be understood as reminders of the power of earth forces, hence an admonition. That would be ethics.
3. aitraaz
9.21.09 / 9pm


A telluric current (from Latin tellūs, “earth”) is an electric current which moves underground or through the sea. Telluric currents result from both natural causes and human activity, and the discrete currents interact in a complex pattern. The currents are extremely low frequency and travel over large areas at or near the surface of Earth.


Seismology (from Greek σεισμός, seismos, “earthquake”; and -λόγος, -logia, as a whole “Talking about earthquakes”) is the scientific study of earthquakes and the propagation of elastic waves through the Earth. The field also includes studies of earthquake effects, such as tsunamis as well as diverse seismic sources such as volcanic, tectonic, oceanic, atmospheric, and artificial processes.


Telluric forces are thus in nature eletromagentic, while seismic forces are an aggregate of linear elastic forces (stress and loading conditions). No reliable relationship has been determined between telluric anomalies and seismic activity: 


“Some phenomena have been observed a very brief time before an intense earthquake: variations of the terrestrial magnetic and electric fields, rumble and also often animal nervousness. In order to give a sole explanation of all these phenomena, a general theory of the earthquake has been built, based on separation of the electric charges when the internal terrestrial masses are subject to strong compressive stress for piezoelectric effect. If the separation of the electric charges takes place on very large masses, intense electric fields are produced so to generate strong electric potential differences. If these differences are higher that the dielectric rigidity of the materials, a strong disruptive discharge can occur between the terrestrial masses. The destruction of some masses allows other masses to move generating intense seismic waves.”


As regards the ‘palazzate,' they must recall the ‘strength of earthquakes” by interpreting a single telluric movement. (? – telluric meant as ‘of the earth' generically?) The issue was secondary…


Another example of Ethics: For L'Acquila – after the earthquake, the Italian government approved a rapid reconstruction project, providing for vast housing complexes in record time – the first of which have been unveiled at the beginning of September. The actual towns hit by the earthquake, remain closed indefinitely (and most likely for a very long time) due to the necessity to structurally ‘requalify' them and thus render them inhabitable once again. 


Recent analysis has determined that many of the actual sites for the new housing are in fact on ‘unstable terrain,' or terrain which is not stable for building. This data comes from sample drilling up to a great depths.


No surprise here – there is very little ‘ethics' in the construction business, nor for the architects behind it (at the bottom of the food chain) more than willing to see their designs realized at any cost. 


The point here is this (regarding Calabria), and may only serve to provoke: if architects are limited (or limit themselves), in such a context, to constructing ‘reminders' of the powerful forces at work in the earth, and this is to serve as an example of ‘ethics,' it would demonstrate how far architecture has strayed from the forces which give it expression and essence – forces such as the earth, the social, matter, memory, politics, the polis…


Perhaps a stronger sense of ethics would lead to use such resources at disposal to confront the political, financial and social forces that are responsable for another disaster in the making – roughly 70% of construction is abusive in one of Italy's and Europe's most dense and active seismic regions.


<http://eurus.over-blog.com/article-30560335.html>


Architecture, in such a case (and any other), needs to turn away from interpretation and representation and towards expression and essence, the vital impulse…





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.22.09 / 12am
	
	
	altraaz: your explication of terms–and the relationship of electromagnetic to seismic—is appreciated. My motive in posting this is not to defend it in advance, even though I am one of the suspects, but rather to expose the whole idea to a preliminary view and discussion. I understand your impatience with an exhibition about catastrophe in a region likely to have another one of the same kind commemorated by the exhibition. It would seem to be better if the resources used for the exhibition would be applied to preparing buildings for the next destructive quake, but the fact it that the exhibition resources are paltry compared with the costs of preparation. And this does not take into account the political and economic barriers that effectively block preparation. Perhaps the best thing for architects to do in this limited situation is make the statement about the presence of earth forces that can—and eventually will—be destructive again, thereby raising the subject for public discussion, at least.
	
	
	I should clarify that this making of a statement is not necessarily the concept behind all—or any—of the architects' proposed constructions—it may only be an effect. For example, anyone who knows my speculations on “earthquake architecture” knows that the main goal has been to conceive and visualize architecture that *works with* the inevitable seismic forces, and does not resist them. I'm certain that for each architect in the exhibition a personally derived point of view is developed. We can debate them for their potential value, or lack of it, but must be glad that the points-of-view have been articulated and presented to us.
	
	
	On the other hand, there is always some earnest person who pronounces works of art as frivolous in the face of life's real problems, and considers them a waste of resources. The counter-argument is that it is only works of art that can make sense out of tragedy and show us a way forward. You and I and others are not going to settle this difference of outlook here, but the very fact that we are discussing it is promising of perhaps eventually solving the real problems, or of transcending them—that is, taking them to another level that we need to define.
