import requests
from bs4 import BeautifulSoup
import os
import sys
import re
from markdownify import markdownify as md

lebbeusArchive = """

<ul>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/08/">August 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/07/">July 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/06/">June 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/05/">May 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/04/">April 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/03/">March 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/02/">February 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2012/01/">January 2012</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/12/">December 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/11/">November 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/10/">October 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/09/">September 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/08/">August 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/07/">July 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/06/">June 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/05/">May 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/04/">April 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/03/">March 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/02/">February 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2011/01/">January 2011</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/12/">December 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/11/">November 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/10/">October 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/09/">September 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/08/">August 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/07/">July 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/06/">June 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/05/">May 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/04/">April 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/03/">March 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/02/">February 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2010/01/">January 2010</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/12/">December 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/11/">November 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/10/">October 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/09/">September 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/08/">August 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/07/">July 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/06/">June 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/05/">May 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/04/">April 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/03/">March 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/02/">February 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2009/01/">January 2009</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/12/">December 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/11/">November 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/10/">October 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/09/">September 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/06/">June 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/05/">May 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/04/">April 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/03/">March 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/02/">February 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2008/01/">January 2008</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2007/12/">December 2007</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2007/11/">November 2007</a></li>
    <li><a href="https://lebbeuswoods.wordpress.com/2007/10/">October 2007</a></li>
            </ul>

"""

lebbeusSoup = BeautifulSoup(lebbeusArchive, 'html.parser')

for a_tag in lebbeusSoup.find_all('a', class_='', href=True):
    # get url name and href
    archive_url = a_tag['href']
    archive_month_year =  re.sub("[.,:]", "-", a_tag.get_text(strip=True)) 

    # Folder names
    Folder_path = os.path.join(sys.path[0], archive_month_year)

    # make folders
    if not os.path.exists(Folder_path):
        os.mkdir(Folder_path)

    # get data from each url and parse
    r = requests.get(archive_url)
    lebbeusSoupspoon = BeautifulSoup(r.content, 'html.parser')


    mainlist = lebbeusSoupspoon.find('ul', class_='dates').find_all('li', class_='post')
    for element in mainlist:
        a_sub_tag = element.find('a', class_='', href=True)
        # get a href of each link
        a = a_sub_tag['href']
        # strip certain characters for name
        Article_name = re.sub("[ .,:?/]", "-", a_sub_tag.get_text(strip=True)) 
        Article_name_spaces = re.sub("[ /]", " ", a_sub_tag.get_text(strip=True)) 

        # find dates
        a_date_tag = element.find('span', class_='date', href=False)

        date_tag = a_date_tag.get_text()


        #get data from url
        r = requests.get(a)
        lebbeusArticle_unclean = BeautifulSoup(r.content, 'html.parser')

        # Remove share section
        lebbeusShare = lebbeusArticle_unclean.find("div", {"id": "jp-post-flair"})
        lebbeusShare.decompose()

        #find primary div
        html = lebbeusArticle_unclean.find("div", class_="primary")
        

        #convert to markdown
        replacements=[
            ('[—]', '-'),
            ("[’]", "'")
        ]

        for old, new in replacements:
            cleanhtml = re.sub(old, new, str(html))
        mkdown = md(cleanhtml, heading_style="ATX")

        #find tags, but extract unwanted tags first
        tags = []
        tagshashless = []

        unwantedtags = lebbeusArticle_unclean.find_all("a", {"rel": "category tag"})
        for unwantedtag in unwantedtags:
            unwantedtag.extract()

        tagElem = lebbeusArticle_unclean.find_all("a", {"rel": "tag"})
        for tag in tagElem:
            tagtexthashless = re.sub("[.,: ]", "_", tag.get_text(strip=True))
            tagtext = "#" + tagtexthashless
            tags.append(tagtext)
            tagshashless.append(tagtexthashless)

        #Make content
        if tagshashless:
            yamltags = """ 
tags: """ + "\n    - " + str("\n    - ".join(tagshashless))
        else:
            yamltags = ""
        yaml = """
---
title: """+ Article_name_spaces +"""
date: """ + date_tag + yamltags + """
---
"""

        content = yaml + mkdown + str(" ".join(tags))

        # File names
        File_path = os.path.join(sys.path[0], archive_month_year + "/" + Article_name + ".md")

        if os.path.isfile(File_path):
            # if file exists, overwrite
            with open(File_path, "w", encoding="utf-8") as mkfile:
                mkfile.write(content)
                mkfile.close()

        else:
            # fi file does not exist, make new
            with open(File_path, "x", encoding="utf-8") as mkfile:
                mkfile.write(content)
                mkfile.close()