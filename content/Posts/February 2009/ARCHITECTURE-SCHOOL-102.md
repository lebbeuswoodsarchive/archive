
---
title: ARCHITECTURE SCHOOL 102
date: 2009-02-06 00:00:00 
tags: 
    - architecture
    - education
    - school
---

# ARCHITECTURE SCHOOL 102



If half of any school's story is the faculty, and the other half is the students, then why is there a third half—the administration, dean and department chairs, and why is it a half, not a third of the whole school shared with the other two thirds? This is the conundrum, and the paradoxical situation of architectural education today.


 In theory, the only indispensable participants in a school are teachers and students. They are equal partners in that their roles depend equally upon each other. In an ancient Athenian academy, the most basic sort of school, and the most famous of which is described in Plato's *Dialogues*, there was only a teacher and those who came to study with him (those were notoriously sexist times). The teacher in the *Dialogues* was Socrates. The students were the brightest young men in Athens from wealthy and influential families (those were notoriously elitist times, too), who engaged Socrates in discussions about a broad array of ideas. What made Socrates unique among the wise men of Athens was his method of thinking and of discussion.


 It is telling that Plato's account of Socrates' academy is called the ‘Dialogues' and not the ‘Monologues.' While a teacher such as Protagoras preferred to give lectures—monologues—displaying his wisdom, Socrates method of getting at the truth—which he believed was the goal of knowledge—involved the back-and-forth, the give-and-take, the vigorous exchange of views between individuals who were free to develop their own thoughts and understandings. Because these were bound to be different, the dialogues are filled with arguments and counter-arguments which advance step-by-step, focusing on key questions, toward a conclusion. Socrates questioned everything, especially his own knowledge and assumptions. The conclusions arrived at by this method were not known in advance. It is easy to understand why the Socratic method has had such a great influence on Western science, art, philosophy, and learning—it frees the mind and liberates its creative powers.


 It would seem that the Socratic model might be a good one for a school of architecture today. Well…yes and no. Surely the method of continual questioning and discourse based on dialogue not only works, but is essential in a community that prizes both diversity and inventiveness. The nature of knowledge, however, has changed since Socrates' time, and the nature of society, as well. Partly because of the pervasive influence of Socratic skepticism, the body of human knowledge has expanded enormously and become highly specialized and fragmented. It is no longer possible for a single teacher to grasp the different fields of knowledge relevant to so comprehensive one as architecture, in the way the direct inheritors of Socrates—such as Aristotle—did, mastering ethics, mathematics, logic, poetry, art, and politics. Or, as in the Medieval guilds, where masters were able to teach students to emulate them and learn mastery of a craft. Today we have colleges and universities, with many compartments and departments of specialized knowledge, with individual teachers for each. A university or college is a complex webbing of people, ideas, methods and data, which is not reconciled or integrated under any single philosophy. It is a type of controlled chaos, in short, a paradoxical structure. Enter the third half of any school today: the controllers—administrators, deans and department chairs.


 The administrators are a half and not a third because the extent of their power to influence the character and quality of a school is so very large. They are not part of a tri-partite ‘balances of powers,' as in a constitutional government. If we restore the normal arithmetic, we would say that they are a half that completes a whole, the other half being students and faculty.


 This is not to suggest that administrators are opposed to students and faculties, though there are times when that has happened. In the 1960s, the policies of some administrators differed so much from the political and intellectual positions of the students and faculties that revolts took place on many campuses, not only in the United States, but also in Europe, South America, and other parts of the world. The responses of administrators—and those in the higher tiers of authority who administer them—ranged from the reasoned, Socratic negotiation of changes to brutal military and police repression. Deans and chairpersons were caught in a vise of conflicting pressures from above and below and those who were also teachers especially so. Many academic careers were abandoned or wrecked during this period of tumult and change.


 Today, in a calmer (more pacified?) social climate, the administrators of any school of architecture can focus on their main task: giving a school its shape and direction. They hire and fire the adjunct faculty. Tenured faculty cannot be fired, at least not for their political views or ways of teaching, but even they can be marginalized by a dean or chair, if he or she so wishes. Also, they have the power to set the standards for the admission of new students, by persuasion, if not by fiat. A dean has the power to lead a school in a particular direction, and not in others. Together with this power comes the responsibility to have, and communicate, a clear idea of what that direction is, so that faculty and students know where they stand. If they agree, they freely stay and work together; if they disagree, they can leave, or not join the school at all. A dean, acting also through department chairs, sets the tone of a school—whether it is to be experimental or rooted in traditional values—and also its character—egalitarian or autocratic. A great school cannot be all things to all people. Intelligent choices can be made only when the available choices are clear. A dean who lets a school be pushed this way and that by its own internal struggles within the faculty and the students is a failed leader, and the school suffers. A great dean is not afraid to lead in the direction he or she thinks best. The courage to do that is the essence of the job description.


 *(to be continued)*


*LW*



#architecture #education #school
 ## Comments 
1. [Diamond](http://www.intheblackbox.eu)
2.7.09 / 4am


The greatest developments in modern thinking have come from men and women who were not afraid to push boundaries and ask tough questions. These artists and architects found a way to voice their ideas and promote their artistic achievements because they were not bound by rules or suppressed by those of higher rank. they were either learning and developing independently or under a teacher who encouraged individual/independent thinking. 


All true architects must of course learn how to be an architect before they can start pushing boundaries, and for this the Master is invaluable. However once education is formally finished within the walls of a school it is only beginning in the offices of a firm as an apprentice. Here once again an architect must suppress whatever individuality he/she still possesses and conform to a style handed down from yet another elder…


Of course individuality is always possible within the most rigid frameworks, but architecture has slowed down in its developments and seems to have lost it forward momentum. The very basis of architectural style that new architects are learning from schools and firms is stale and without the same fire that drove modern thinking of the early 20th century… Even if the dean remains a leader and forces conformity amongst his teachers and students, how can he rid of this lack of momentum if he does not allow the “exception”, the radical teacher or student to pass on his knowledge and ideas??


Surely forward thinking requires unusual and unprecedented actions?
3. alfonso
2.7.09 / 1pm


Dear Mr. Woods, i´m a big fan of your work and since I discovered it, also a big fan of your blog. Thanks for sharing this. Just one question on this faculty matters:  

What´s your point on Bauhaus amazing failure? I mean, the most important and brilliant minds of architecture at the time were there, but what ever came out of it for architecture advance? This always made me curious.  

Also, the suspicion that not every good architect is a good teacher by far. While good teachers -no matter their proffesional succes in the architecture practive- are so hard to find and i think history is not being fair to their legacy. We might work hard in order to solve that.
5. [Josh](http://diastudy.blogspot.com/)
2.7.09 / 5pm


Lebbeus. Your words ring more true than ever in the world of academia. As a 4th year architecture student studying in a 5 year program, I myself have been chastized and labeled as a trouble maker only because I, as a non-traditional student, see the short-comings of our current dean and chose to speak up about it. I only wish you could come to our university and deliver these exact words to our faculty. Maybe then they would rediscover the passion they once had for architecture and teaching– and maybe then they would gain the respect they deserve from the students.


Thank you for your thoughts.
7. David
2.11.09 / 3am


We always turn back to these Platonic vs Socratic dichotomies.  

I'd like to suggest that there is another relevant model. 


<http://en.wikipedia.org/wiki/Epicurus>


Epicurus lived a generation or two after Plato, and gives us a detailed model for education that differs from either you've mentioned.
9. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.18.10 / 2pm


[…] lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 […]
11. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.18.10 / 11pm


[…] lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 […]
13. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] on his blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
15. [Lebbeus Woods on Architecture School | Architecture EV](http://architectureev.wordpress.com/2011/01/02/lebbeus-woods-on-architecture-school/)
1.2.11 / 6pm


[…] School 101 Architecture School 102 Architecture School 201 Architecture School 202 Architecture School 301 Architecture School […]
