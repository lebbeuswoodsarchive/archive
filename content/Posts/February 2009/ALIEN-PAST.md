
---
title: ALIEN PAST
date: 2009-02-03 00:00:00 
tags: 
    - movies
    - set_design
---

# ALIEN PAST




For a month or so in the summer of 1990, I was hired to work as a ‘conceptual architect' on the movie project Alien III.


Given my legally contentious relationship with Hollywood film studios, I have to preface the post with this explanation: *the drawings made by me that I reproduce with this post, and their copyrights, are the property of the Twentieth Century Fox Film Corporation; I reproduce them here under my rights of fair use, for educational purposes only; copying or reproducing the images of these drawings for any commercial purposes, without the explicit permission of Twentieth Century Fox is strictly forbidden by law.*



These drawings were made—in Hollywood and Pinewood Studios, England—for a movie that was never made. The movie called Alien3 that was made and seen around the world was conceived and directed by David Fincher, and is notable for it's unremarkable sets and its unrelenting grimness. The movie I made designs for was directed by Vincent Ward, but ended in its early stages, when he left the project.


 The story of the Ward movie was radically different, though it deployed the same basic characters, in that the setting was a religious colony that had escaped the earth and inhabited an abandoned commercial facility deep in space. They had adopted a Medieval way of life, without electricity or modern technology. The Ripley-Alien drama was to be played out inside this crumbling, artificial world. Under Ward's direction, this would have become something highly original, a movie in which the architecture would have had a central part.


LW


My first idea about the colony's artificial world:



![a3-10-1](media/a3-10-1.jpg)


![a3-10-31](media/a3-10-31.jpg)


 


![a3-93](media/a3-93.jpg)


Vincent Ward's sketch of the colony's world as a sphere:


![a3-14](media/a3-14.jpg)


My attempts to give form to his vision:


![a3-12](media/a3-12.jpg)


![a3-13](media/a3-13.jpg)


![a3-5](media/a3-5.jpg)


Section of the artificial world, showing Medieval within modern, modular metal construction:


![a3-61](media/a3-61.jpg)


![a3-15](media/a3-15.jpg)


![a3-16](media/a3-16.jpg)


![a3-19](media/a3-19.jpg)


Drawing of a set to be constructed in the Pinewood sound stage, showing its actual dimension:


![a3-17](media/a3-17.jpg)


Model of ‘the Cathedral,' built by Christopher Otterbine:


![a3-3](media/a3-3.jpg)


#movies #set_design
 ## Comments 
1. peridotlogism
2.3.09 / 6pm


Ripley never caught a break.
3. the0utfit
2.4.09 / 6am


beautifully brilliant! Piranesi would be jealous
5. [Dante Straw](http://danstewart.building.co.uk)
2.4.09 / 9am


I read an article in Empire magazine at the end of last year about the “wooden planet” version of Alien III, which included a long interview with Vincent Ward – no idea who had done the designs though. A difficult call to follow H.R. Geiger's aesthetic for the first Alien films, but I'm sure it would have been great if Hollywood hadn't got in the way…
7. ark
2.4.09 / 6pm


@the0utfit: Great reference.


@LW: Didn't William Gibson write that script?
9. [Mike White](http://www.impossiblefunky.com/archives/issue_12/12_alien3.asp?IshNum=12&Headline=The%20Metamorphoses%20of%20Alien%20III)
2.4.09 / 9pm


@Ark — Different version of the script.


[Article about Alien 3 rejected drafts](http://www.impossiblefunky.com/archives/issue_12/12_alien3.asp?IshNum=12&Headline=The%20Metamorphoses%20of%20Alien%20III)
11. [Gong Szeto](http://www.gongszeto.com)
2.4.09 / 9pm


dear god lebbeus! i remember you verbally describing these to me (decades ago) – and i was rapt. i cannot believe i am actually looking at the drawings!!!!!!! OMG!!!!


thank you so much for sharing. WoW.
13. [Duncan](http://duncanmckean.co.uk)
2.5.09 / 9am


Excellent concepting sketches. Thanks for sharing. Have given me inspiration to go sketching.
15. [Paul](http://hal9000.squarespace.com/)
2.5.09 / 1pm


That's an amazing concept, the setting for this one is far more imaginative. Really amazing….
17. [m](http://www.asasku.blogspot.com)
2.5.09 / 1pm


Wow. I'm awed to see this work.  

Thank yu for sharing LW.


\_


Whenever i'm watching kevin costner ‘waterworld' and terry gilliam 12 monkeys, I couldn't help myself in comparing how much these sets seems similar to LW works. 


What is with Hollywood when it comes to collaborating with architects? They seems to be more against it than the other way around. 


I suspected that they (people in films and drama production and sadly including writers) have no clear understanding of architectural approaches towards design, ideas, our way of work and why did we stand by the whole conceptualized idea without any compromise that may seem as stubbornness to them. 


And this is also why there aren't many movies or dramas portraying the real life of an architect and our way of work.
19. [Jonathan](http://www.wholecommunitydesign.com/)
2.5.09 / 11pm


These are amazing. I appreciate you sharing. The cathedral sketches remind me of Piranesi. Just lovely.


I agree it's unfortunately true that movie studios rarely see the benefit in hiring architects to do design work for their sets. There are a very few architects in the business (Eric Hanson comes to mind, working on features from 5th Element to Spider Man), but not many. The irony is that many of the art directors that are employed by the studios and the gaming industry derive their inspiration directly from architects. I spoke with Viktor Antonov (at the time he was with Valve) a few years ago at Siggraph after he gave a preview of his work on Half Life 2 and he directly cited your work [Lebbeus] as inspiration in his process. 


So many people go on vacation and all they take pictures of is architecture, and yet very few of them realize exactly how important architecture is to their lives and culture, and by extension, how important it is in (re)creating life and culture.
21. [Top Posts « WordPress.com](http://botd.wordpress.com/2009/02/05/top-posts-1014/)
2.6.09 / 12am


[…] Alien Past For a month or so in the summer of 1990, I was hired to work as a ‘conceptual architect' on the movie project […] […]
23. [Bright Garlick](http:otherworldlyencounters.blogspot.com/)
2.6.09 / 5am


This would have made a wonderfull alternative. Far more majestic in scope and personality. Great work none the less.
25. Magic Whiskey
2.9.09 / 6pm


Simply stunning concept, no doubt allowing the Alien to be a demon in such a religious world. The decision to go another way was such a loss.
27. AJ
2.9.09 / 7pm


Absolutely spectacular.
29. [nexialist organization](http://www.nexialist.com)
2.9.09 / 8pm


thanks for posting. love sf since being a kid, admired your work before studying architecture. nice to see how things do come full circle every now and then. respect. more, please!
31. [chiwan](http://chiwanchoi.com)
2.9.09 / 8pm


awesome. i actually have a copy of the william gibson “alien 3” script that i found in a desk while i was a student at NYU.
33. [The Alien 3 that could have been « Sea SVC Vacant](http://seasvcvacant.wordpress.com/2009/02/09/the-alien-3-that-could-have-been/)
2.9.09 / 8pm


[…] So at some point in 1990, Alien 3, was in the hands of Vincent Ward, director of What Dreams May Come.   From what I've read it seems that the setting was to be a church colony, floating about in space.   Designing this world was artist/architect Lebbeus Woods.  Scans of his design for said floating colony reside here. […]
35. Will
2.9.09 / 8pm


These designs are very interesting and would have looked amazing fully-realized. However, having read the script for this version, I still think the film would have fallen flat. Design is great, but if the story is weak, all the design in the world can't save you.
37. doublethinker
2.10.09 / 4am


All of this looks ripped straight from Warhammer 40K. From the orbiting monastery to the techno-medieval architecture, it all has precedence in the background material for that game. 1990 cuts you a lot of slack, but I think some of the WH40K stuff predates that, even.
39. DB
2.10.09 / 5am


A full account of the Alien3 saga, including more conceptual artwork by Mr. Woods and others, and lengthy interviews with Vincent Ward and others involved in the beginning stages of the film, can be found on the Alien3 DVD. I'd like to note that the final film can't be said to have been conceived by Fincher – IMDB lists the official writing credits as story by Vincent Ward, and screenplay by David Giler, Walter Hill and Larry Ferguson. They put Fincher in and started shooting without a finished script, frustrating all involved. The final product obviously had some potential but the project lost all meaning along the way. The making-of on the DVD is a must-see. Too bad they didn't make this movie.
41. [Kiel Bryant](http://flickr.com/photos/kielbryant)
2.10.09 / 9am


I recommend each of you read the Fasano/Ward draft (Will notwithstanding). It accomplishes what the filmed movie could not: narrative evolution. It originates rather than derives; provides fresh concepts over scavenging the carcasses of what Ridley and Cameron made before. It is urgent and intricate and compelling. After that, it establishes a beautiful mystery to be resolved on celluloid or purely in the minds of fans: the status of home. The state of Earth. You leave the theater of mind cheering — and wanting more.


The Minorite Abbey of Arceon, which Lebbeus Woods was tasked to illuminate, represents — as subtly orchestrated by Vincent — the human potential for good, and the psychosexual malice of the Alien represents the human potential for good's opposite. This elevates the story beyond mere survival into the philosophical feast of choice.


The world that might have been:


VINCENT WARD, director


NORMAN REYNOLDS, production designer


LEBBEUS WOODS, conceptual architect
43. [Kiel Bryant](http://flickr.com/photos/kielbryant)
2.10.09 / 9am


and


DOUG CHIANG, concept designer



We're grateful you've shared these with us, Lebbeus. An alternate reality slowly rises. Thank you.
45. [Kiel Bryant](http://flickr.com/photos/kielbryant)
2.10.09 / 9am


The work itself, Lebbeus:


Christopher Wren meets Paolo Soleri.
47. [Extenuating Circumstances – links for 2009-02-10](http://danhon.com/2009/02/10/links-for-2009-02-10/)
2.10.09 / 10am


[…] ALIEN PAST « LEBBEUS WOODS Why do I blog this: I hear papercamp 2010 will be held here (tags: paper wood architecture alien3 concept drawing sketch alien) […]
49. [Buamai » ALIEN PAST « LEBBEUS WOODS](http://www.buamai.com/image/4797)
2.10.09 / 10am


[…] gravity, lebbeus woods, master wheel, steel, truss Posted by will patera on February 10th, 2009 Original Source Post Comment Click here to cancel […]
51. [Den](http://likealittlekidwithabeard.blogspot.com/)
2.10.09 / 3pm


This is great. Years after you'd done this, Dan Abnett wrote a comic story called Durham Red: The Scarlet Cantos, I forget the artist, it may have been Henry Flint… anyway, they also have a wooden planet… but it ain't as purdy as yous…


Damn that's cool.
53. Petronius Maximus
2.11.09 / 9am


Not to be disparaging to our gentleman Mr Bryant above, but I'm afraid I have to take to converse view to him.


While it's lovely to see Mr Woods' artwork here, there's no denying one simple thing: the Fasano/Ward draft of the movie, while an interesting *fantasy* scenario, is quite simply a totally dumbass movie. The characters — within their context — work okay. But outside? They're stupid. Ripley is barely recognizable. The Alien “action” is fairly “meh”. And the environment itself has little-to-no logic. When you look at the thoughtfully put together Cobb Nostromo, the Fasano/Ward draft is a joke. (As I understand it, Ward was flying to Los Angeles over the arctic, looked down on the vista, and “got” the whole story there. Hmm. Yeah. It shows.)


I hated the various Hill/Giler drafts of A3. (Where they were so lazy, that characters who had died resurrected themselves pages later.) When I read the Rex Pickett draft after the movie's release, I was overjoyed. And, having seen the Lauzarika re-cut on the “Alien Quadrilogy”, I'm STILL overjoyed: that was the cut that made A3 palatable.


And that cut, is far, far, FAR superior to the Fasano/Ward draft, problems notwithstanding.


Any fanboys espousing the superiority of Fasano/Ward out here, simply don't know what the hell they're talking about.
55. [Steven Millan](http://www.myspace.com/stevenmillan)
2.11.09 / 10pm


Very interesting and exquisite designs there,for that religious colony/wooden planet concept would work much better as a solo film and without both Ripley and the Alien.
57. [pheadx](http://alienv.blogspot.com)
2.13.09 / 2am


It's good to see these late drafts. They create a certain nostalgia of what could have been.  

I'd like to ask you for a favor: I have this screenplay called Alien Planet. It's the final chapter of the Alien saga, an intimate and terrifying coming-of-age story about 200-year-old Ripley. I'd like to send it to you and ask you for a simple feedback.  

<http://alienv.blogspot.com>
59. [Linkdump for February 10th at found\_drama](http://blog.founddrama.net/2009/02/linkdump-february-10th/)
2.13.09 / 5pm


[…] ALIEN PAST by Lebbeus Woods (via B&sup2;): this movie would have been far more interesting. Then again, I said that when I heard about the alternate William Gibson script as well. (tagged: design science scifi art fiction architecture illustration film ) […]
61. [Jacob North](http://www.jacob-north.com)
2.16.09 / 8pm


I See it. Incredibly beautiful. .. The Alien occupying the cavernous winding space of something brand new, yet all TOO appropriate for the killing machine.


I AM using the lighting from the popular version though.. the orange and yellows and blacks… but against the textures of these walls.


Cool.
63. [Lebbeus Woods and Alien 3 | Oscar Scarlett design blog](http://www.weloveyourwalls.com/php/blog/2009/02/18/lebbeus-woods-and-alien-3/)
2.18.09 / 1pm


[…] Woods and Alien 3 Posted by seanOn February – 18 – 2009 I love that Lebbeus Woods has a blog. A recent post provides a peek into his imagined world for Alien 3 under the direction of Vincent Ward, before […]
65. [Mike White](http://www.impossiblefunky.com/archives/issue_12/12_alien3.asp?IshNum=12&Headline=The%20Metamorphoses%20of%20Alien%20III)
2.19.09 / 4pm


I wish I had seen these prior to sending off my book to the publishers. I think these pieces would go great with my Alien 3 article. Perhaps it's not too late to still discuss their inclusion?
67. [lebbeuswoods](http://www.lebbeuswoods.net)
2.19.09 / 4pm


Mark White: I do not hold the copyrights on these drawings, and therefore cannot give you permission to use them. I am able to post them only under my (or anyone else's) rights of ‘fair use,' meaning for educational or scholarly purposes only. Not for profit. The 20th Century Fox Film Corporation holds the copyrights. I suggest you consult with your publisher about obtaining permission to use them. Good luck….
69. Kiel Bryant
2.22.09 / 1pm


A closed decree, Petronerous, and thus impervious to rejoinder. For clarity's sake alone, then, I say: subtlety and texture are enriching virtues. Pickett, Twohy, Giler & Hill settled for the path of least resistance. Charles Lauzirika re-cut nothing — merely furnished an Assembly Cut seldom-seen (yet no less narratively ruined). Lauzirika even removed his name from the documentary, to be titled “Wreckage & Rape” (now “The Making of Alien 3”), adopting instead the disdainful pseudonym Frederick Garvin. 


I must assume, by law of averages, you are a failed artist. If true, an admonishment formulated by the late Gene Roddenberry may be of value:


“You can't be shackled to the audience's wants. That way lies prostitution.”


Lebbeus, saw your Centricity selections at SF MOMA. Mesmers, all.
71. [Linkdump for February 10th | Game Blog](http://blog.dramalava.com/2009/03/05/linkdump-for-february-10th/)
3.5.09 / 11pm


[…] ALIEN PAST by Lebbeus Woods (via B&sup2;): this movie would have been far more interesting. Then again, I said that when I heard about the alternate William Gibson script as well. (tagged: design science scifi art fiction architecture illustration film ) […]
73. [Alien Past « Diezimedia](http://diezimedia.wordpress.com/2009/03/26/alien-past/)
3.26.09 / 3pm


[…] Link: https://lebbeuswoods.wordpress.com/2009/02/03/alien-past/ Posted by diezimedia Filed in Architecture, Art, Design […]
75. RaiulBaztepo
3.28.09 / 11pm


Hello!  

Very Interesting post! Thank you for such interesting resource!  

PS: Sorry for my bad english, I'v just started to learn this language 😉  

See you!  

Your, Raiul Baztepo
77. [http://phenomenon-s.ru/](http://Phenomen)
4.8.09 / 7pm


Very nice post, thanks man!
79. [Viet Do](http://www.blend-em.com)
11.4.09 / 9am


Dear Mr. Woods and all those who posted,


Suppose we were to actually make some key shots of this movie through 100% CGI using Maya and Max. Just as an exercise…IS there any worth in that ?


Viet Do
81. [ArchiTakes» Blog Archive » Architecture Meets Science Fiction at 41 Cooper Square](http://www.architakes.com/?p=4140)
12.4.09 / 2pm


[…] voluntarily put his skill at fantasy to work for the movies as a “conceptual architect” on the movie Alien 3 in 1990.  The movie changed directors and Woods' images weren't […]
83. [Aliens 3 | firemusic](http://www.firemusic.co.uk/2010/06/aliens-3/)
6.28.10 / 1pm


[…] The image above is taken from the concept art from one of the scrapped script revisions. In an early version the monastic/futuristic medieval themes which eventually turn up in Fincher's version are explored in more depth. (More here) […]
85. [Adam Ryder](http://www.adamryder.com)
8.25.10 / 4am


It's about time that architects, especially conceptual ones be the driving force behind big budget science fiction movies, then they might even get a decent story of the damn thing instead of a lot of trite drama and stale messianic narrative.
87. JimTwixt
3.2.11 / 9am


What a concept. Extraordinary and twisted. Black blood under enormous church arches. A vast echoing world with crusted shadows lit by fitful candles.


The terrifying atmosphere of the medieval.


What a movie this would have made. There would not have been one to equal it. The atmosphere would have been unique. Like A Canticle For Leibowitz, filmed in some rotted black cathedral inhabited by the devil.


I've always thought H. R. Giger was inspired by a demon when he created the alien design. It's probably what a real demon would actually look like. Ward's Alien 3 would have made that connection tangible. The alien as the devil itself. How appropriate. 


I'm amazed that several people I've talked to have never made that connection – the ugliness of the alien with the demonic. Too bad this movie couldn't have done it for them. 


The Ward-Fasano script is a masterpiece. I know enough about script-writing to fill in the blanks in the writing. Even when the characters says little or nothing, a vast amount would have been conveyed by the silences alone.


In the movie these gaps would be filled in with atmosphere. In a script they lie dead on the page. How sad this was never filmed. How sad we cannot see the power in those seemingly empty moments. This takes imagination and vision. I suspect that Vincent Ward (as a director) would have been gifted with both.


This movie should still be filmed one day. Computer effects would suffice. A fully animated creation. This story is just too good – and original – to waste.


Some people have criticized the monks-in-space angle. They say it has no sense. No logic. This strikes me as bizarre. It has all the sense of an intense vision. Fierce like a toothache. Rigid as a nightmare.


Different is good. It's the only thing worth doing. The first Alien movie introduced the alien. The second movie combined the alien with the military. The third would have combined it with religion. What a brilliant synthesis. And it had never been done before.


With the loss of this movie, that pattern of newness was broken. Now the alien simply eats people. There is no grandeur in that vision. Nothing higher or lower. Just one level that never seems to go away.


What a towering disappointment.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.2.11 / 1pm
	
	
	JimTwixt: Your assessment of the film that still could be is quite inspiring. Thank you!
