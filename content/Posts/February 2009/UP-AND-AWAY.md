
---
title: UP AND AWAY
date: 2009-02-24 00:00:00
---

# UP AND AWAY



I live in a hole.


Oh, the apartment is fine enough and has a large skylight that the occasional rain keeps reasonably well washed. However, it is the skylight that reveals daily the hole I speak of: a deep recess in Lower Manhattan, made not by excavation but by building up. Tall buildings of varying heights loom all around, affording a bottom-up view of a higher world. It is a normal perspective when one is on the street, but there one rarely looks up, focusing instead on the eye-level landscape. Inside my apartment, the skylight works like a lens that allows only upward views. In particular ways they are perfect for an architect—dramatically perspectival—only buildings and sky—pure geometry and space. But social and political aspects intrude on the pure aesthetic enjoyment. Who owns the buildings? For what purposes are they used? In this Financial District, the real site of contemporary disasters, from the destruction of the Twin Towers to the collapse of the American economy, what does all the ingenuity and investment of human and natural resources embodied in these buildings signify or, more simply, mean?


As is often true of vivid architectural experiences, the views through the skylight are both thrilling and troubling.


 LW 


![lwblog-upaway](media/lwblog-upaway.jpg) 


![lwblog-upaway2](media/lwblog-upaway2.jpg)


![lwblog-upaway43](media/lwblog-upaway43.jpg)


![lwblog-upaway5](media/lwblog-upaway5.jpg)




 ## Comments 
1. augustan
2.25.09 / 3pm


He looked at his own Soul with a telescope. What seemed all irregular, he saw and shewed to be beautiful Constellations; and he added to the Consciousness hidden worlds within worlds.


Coleridge, Notebooks
