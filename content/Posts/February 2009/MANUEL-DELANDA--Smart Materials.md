
---
title: MANUEL DELANDA  Smart Materials
date: 2009-02-27 00:00:00
---

# MANUEL DELANDA: Smart Materials


*Another in the ongoing series of articles on materiality by philosopher Manuel DeLanda.*





Ordinary load-bearing structures, such as those built of concrete and steel, would beneﬁt enormously if they could actively damp dangerous vibrations or detect and stop fractures before they propagate. Self-monitoring and self-healing are adaptive capacities characteristic of many biological structures but conspicuously absent from those designed by humans. Research on smart materials is supposed to change this. But what makes a material “smart”? The ﬁrst ingredient of material intelligence is sensors. Embedding optical ﬁbers into concrete, for example, allows the gathering of information about distributions of stress in structural components because the transmission of light through the ﬁbers changes with stress. A distributed network of ﬁbre optic sensors, complemented by some computing processing power, can use this information to monitor the curing process and to check the structural integrity of concrete structures as they age. A different type of sensor utilizes piezoelectric crystals, that is, crystals that generate an electrical potential difference (a voltage) whenever they are mechanically deformed. Since any structural element that carries a load is so deformed (or strained) embedding such crystals into materials like concrete can generate electrical signals carrying information about its structural health. 


Sensors, however, are only one component of smart materials. As cybernetics teaches us, implementing a negative feedback control loop involves both the ability to monitor conditions as well as the ability to causally intervene in reality to modify those conditions. The thermostats that regulate air conditioners or toaster ovens, for example, must be able not only to detect changes in temperature relative to the desired one, but also to change that temperature if it falls outside the desired range by turning the appliance on or off. In other words, a thermostat (or any other servomechanism using negative feedback) must possess not only sensors but also actuators. In principle, piezoelectric crystals could act as actuators because they not only generate a voltage in response to deformation, they can also perform the opposite function, mechanically deform in response to an applied voltage. The problem is that this deformation is literally microscopic, too small to be able to drive the necessary changes in shape that a structural component would need to actively counteract spreading cracks. Deformations on the scale of centimeters would be needed for this. Fortunately there is a new class of materials that may be able to perform this task: shape memory alloys. 


Certain metallic alloys, such as alloys of nickel and titanium, display the ability to return to their original shape after being deformed. While this peculiar capacity was discovered in the 1930's it was not seriously studied until three decades later. Today we know that the shape memory effect is a kind of phase transition. This term is usually associated with the changes of state (from gas to liquid or from liquid to solid) that a material undergoes as a result of spontaneous molecular rearrangements taking place at critical points of intensity. Shape memory alloys also undergo molecular rearrangements but without loosing their solidity. The phase change is between two states of crystalline structure, a relatively soft and deformable state (called “martensite”) and a more rigid state (referred to as “austenite”). The difference between the two states is that successive planes of atoms in the martensite state are realigned so that one portion of the crystal becomes a mirror-image of the other. In a sense, the two regions of the crystal are like “twins”, a reﬂection of each other, so this type of deformation is called “twinning”. It is these highly ordered internal deformations that give the martensite state its ductility by allowing entire planes of atoms to slide over one another. 


The shape memory effect may be described by the following sequence. Materials are given a rigid shape at the relatively high temperatures favoring the austenite state. This overall shape is maintained when the material cools down and spontaneously transitions to the heavily twined martensite state. When a load is applied the material deforms (by a process of de-twinning) and changes shape. If at this point the material is heated and its temperature increased it will spontaneously return to the austenite state, the original orientation of the crystals will be restored and with it the original shape. Although nickel-titanium alloys can be given a variety of shapes they are still limited to those with a small cross-section, such as wire, tubing, and foils. In other words, load-bearing components like columns and beams cannot be made out of these materials. Nevertheless, their peculiar thermo-mechanical behavior can be highly useful in the creation of actuators, as is their behavior during a related phase transition (called “pseudo-elasticity”) that is stress-induced rather than temperature-induced. 


In one proposed scheme, for example, load-bearing components are made of “intelligent reinforced concrete”, that is, ordinary concrete augmented with piezoceramics and shape memory alloys to make it self-monitoring and self-healing. Patches of piezoelectric material are embedded in the concrete structure to detect incipient cracks. Cables made of a shape memory alloy are used to reinforce the concrete, much as steel cables are, using the method of post-tensioning, that is, stretching the cables after the concrete has been poured and has had time to cure. A spreading fracture will deform both the piezoelectric sensors and the cables acting as actuators. The sensors respond by producing electrical signals that are analyzed by special damage-monitoring software, software that, in turn, activates a mechanism to electrically heat the cables, causing them to contract to their original shape and close up the cracks. 


As our civil infrastructure ages a variety of challenges will have to be faced. One of these challenges will be to achieve a better understanding of the aging process itself, such as better mathematical models of fatigue and corrosion processes, or accelerated testing methods to simulate deterioration in the laboratory. But a different kind of challenge will be to inject new life into this infrastructure by embedding into its constitutive materials some of the negative feedback that already animates servomechanisms in other spheres of engineering design. 


**Manuel DeLanda**



 




 ## Comments 
1. s
3.7.09 / 2am


❤ the technical discourse, respect your contribution to the field, much read\


…Sensors are the modern Achilles heel, aka limiting factor, for iA (Interactive Architecture). yeah, we can make lights pulse and sounds chirp as we walk about to infer their pleasurable purpose (woo hoo for a hot minute), but what about interactive structure, the meaty problem : and thank you for opening this door here.. yet another coffee-table book. hint.


Given: Architects are in turn, and behind the armed forces, waiting on sensor ‘technology' to find a way to combine the developed rational program with the physical world. Until this is created we are at a standstill and can only develop the programs responsible for controlling a structural reaction in the hopes that a sensor interface will be refined to a usable measure. 


Question, how many times can this alloy be stressed/moved without permanent failure? I saw a plastic industrial ‘sticker' that you apply over brick to keep old buildings from crumbling in seismic zones, like plastic wrapping. How does this metal alloy compare in function and application to the upfit of older structures that you mention?
3. [orhan ayyuce](http://archinect.com)
5.14.09 / 11pm


as much as i appreciate mr. delanda's in detail assessment of concrete material failure and self remedying technology, via smart materials heading, i find the whole idea a little bit overstretched and misplaced.  

concrete, is pretty much quantifiable and well resolved in its calculations and applications as far as the building technology goes.  

as long as it is applied according to the specifications, it is pretty much a resolved building material. of course this is not to say unusual failures can happen under unusual circumstances, but i suggest mr. delanda to consider millions of buildings being built everyday relying this material which is manufactured and applied in almost every country on earth.  

i would not take the position of saying delanda's assessment of smart building materials is mute but seems like, in this particular area of concrete, it seems like an over effort.  

yes sure i'd like to addmixture as long as not shooting the price of my placement (pour) skyward.
5. Alvaro García
10.3.09 / 2pm


Thanks a lot for your article. It is quite similar to my actual research. I really love these topics.


Not only I think it is a good idea, but it is very feasible. You have completely forgotten asphalt concrete pavements. There, the self-healing and self-monitoring fields have much more things to say than for cement concrete. By the way, these concepts have been already proved for concrete and asphalt and they really work.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.4.09 / 2pm
	
	
	Alvaro Garcia: The article, Smart Materials, is by the philosopher Manuel DeLanda. I simply posted it, with his kind permission. I will forward your comment to him.
7. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] M. DeLanda, ‘Smart Materials'in Domus, No 894, July 2006, pp. 122-23 <https://lebbeuswoods.wordpress.com/2009/02/27/manuel-delanda-smart-materials/> M. DeLanda, ‘Crucial Eccentricities'in Domus, No 895, September 2006, pp. 262-63 M. DeLanda, […]
