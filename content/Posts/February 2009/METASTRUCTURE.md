
---
title: METASTRUCTURE
date: 2009-02-07 00:00:00 
tags: 
    - architecture
    - Bosnia
    - cities
    - war
---

# METASTRUCTURE



In the last decade of the 20th century, the newly recognized country of Bosnia and Herzegovina was under attack by two neighbors, intent on destroying it and dividing the spoils between them. Croatia attacked from the north and west, making Mostar, the provincial capital of Herzegovina, the center of their assault. From the east, Serbian forces attacked, focusing on the national capital, Sarajevo. Both enemies of the fledgling nation claimed that the attacks were made by local militia, but it was clear that the materials of war and its strategies came from the two largest countries of the former Yugoslavia. The sieges of Mostar and Sarajevo, which lasted for years, and other towns such as Srebrenica, were resisted by the undersupplied armies of the small nation, at a great cost in lives, many of them civilians.  


 It was during this dark time that I imagined a defensive wall that could be constructed to protect Bosnia from the invaders. Aerial warfare had been effectively banned by the European powers and the United States, through the creation of a no-fly zone over the entire country, enforced by NATO fighter aircraft. The war was fought, then, on the ground, in an almost Medieval manner, though with tanks and artillery. The idea of the wall was not to build an armed fortification in order to repel invaders, but rather to make it function as a sponge, and absorb them.


 The wall would be built very high, with a vast labyrinth of interlocking interior spaces, creating a structurally indeterminate system that would be extremely difficult to bring down by demolition charges or artillery fire. Tanks and mobile artillery could not be brought through the wall. Foot soldiers could not climb over the wall in large numbers, but would have to go through it. Once inside, they would become lost. Many would not be able to escape. They would either die, or, as it were, move in, inhabiting the spaces, even forming communities. Local farmers from the Bosnian side, could arrange to supply food and water, on a sale or barter basis. In time, they would move in, too, to be close to their market. Families would be living together. The wall would become a city.


 Of course, it was a fantasy. There was not enough time to build such a wall, even if there had been the will, and not enough metal and industrial scrap-yards to supply the materials. I never proposed that it should in any way be realized, as I did with other reconstruction projects during and after the war. However, as a metaphor and even an architectural strategy, it has some value. Walls can be an armature for transformation, an instrument not for dividing and separating, but for bringing opposing ideas and people together. It all depends on the design, the architecture of a wall. Later, in my proposals for La Habana Vieja and The Wall Game, I pursued this idea at a less fantastic and more realistic, realizable scale.


 LW  



The post-Cold War world, breaking into pieces:


![lwblog-bfsglobe1-21](media/lwblog-bfsglobe1-21.jpg)


Presidents Tudjman (Croatia) and Milosevic (Serbia)—the architects of destruction:


![lwblog-sellout2](media/lwblog-sellout2.jpg)


Sarajevo's embattled position:


![lwblog-bfsmap1-23](media/lwblog-bfsmap1-23.jpg)


The wall of the Bosnia Free State:


![lwblog-bfs231](media/lwblog-bfs231.jpg)


![lwblog-bfs21](media/lwblog-bfs21.jpg)


![lwblog-bfs221](media/lwblog-bfs221.jpg)


![bfs24](media/bfs24.jpg)


![lwblog-bfs1a1](media/lwblog-bfs1a1.jpg)


![lwblog-bfs1a-a](media/lwblog-bfs1a-a.jpg)


![lwblog-bfs1a-b1](media/lwblog-bfs1a-b1.jpg)


![lwblog-bfs1a-c1](media/lwblog-bfs1a-c1.jpg)


![lwblog-bfs2a-c1](media/lwblog-bfs2a-c1.jpg)


![lwblog-bfs2a-b1](media/lwblog-bfs2a-b1.jpg)


![lwblog-bfs2a-a3](media/lwblog-bfs2a-a3.jpg)


![lwblog-bf-s-10-2](media/lwblog-bf-s-10-2.jpg)


![lwblog-bf-s-10b-2](media/lwblog-bf-s-10b-2.jpg)


![lwblog-bfs301](media/lwblog-bfs301.jpg)


#architecture #Bosnia #cities #war
 ## Comments 
1. Pedro Esteban Galindo Landeira
2.8.09 / 7am


With an intervention (that we will not discuss the type now) the project could be carried out, with more reason, in an extreme state as a warlike conflict. I understand a setback of the attacking troops (problem of strong political importance of which we could to chat, but speaks about the wall now) by means of a structure, that has a strong character, with an aesthetics directed towards the neutral or better try to diminishing the conflict.


Do you itself deny your form of work, which has a strong Utopian and philosophical load?


These places, Bosnian / Herzegovina and Havana (take care where I live), you see it as accomplishment of your projects, as the construction of his works?


I ask for excuses about my English.
3. [bojana vuksanovic](http://bojanavuksanovic.blogspot.com)
2.8.09 / 2pm


The proposal is quite amazing graphically.


It is a fantasy proposal and for me this qualifies the fact that it is showing a real lack of understanding of things that happened in Bosnia and Herzegovina and Sarajevo. 


BiH and Sarajevo was, and still is, inhabited with many nations. There was no line which separated that clearly the ‘external' attack and ‘internal' (self) destructive forces. Any wall, even in fantasy, would have to interweave with the very substance that created it.


Even if such a line existed, as if anywhere else in the world, the ‘walls' were at the very core of the conflict. To open to the diversities would result in appreciation of everyone who is different – the real solution that would result in richness.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.8.09 / 5pm


bojana vuksanovic: You are certainly correct in saying that the war in Bosnia and Herzegovina was not as simple as my project seems to suggest. However, there were trench lines around Sarajevo, manned by the Bosnian army and Sarajevo citizens, and these prevented the Bosnian Serb military forces from overrunning the city. I was in Sarajevo several times at the height of the siege, and knew many architects who would fight in the trenches half the day, then return to their houses and offices and work on their ideas for rebuilding the city.


I also agree that walls are often used to divide people. This work and others are meditations on how walls can unite them. It remains to be seen whether or not they might be useful.


There is no doubt, however, in light of the known facts that Serbia and Croatia directed the destruction of BiH. The Muslim majority bear some responsibility for having declared independence based on their slim numerical majority, without considering the consequences. And the governments of the West bear some responsibility, too, for offering recognition of BiH independence too quickly and also heedless of the feelings of Serbs and Croats. But the ultimate responsibility for the destruction belongs to the destroyers.


Finally, it must be said that Sarajevo was much more diverse in its people before the war than after. Many Serbs and Croats left with the signing of the Dayton Accords. Many Muslim refugees sought refuge from the notorious ‘ethnic-cleansing' campaigns carried out by Serbian military and paramilitary forces in the smaller towns and villages. Sarajevo is not the cosmopolitan city it once was, but today far more ethnically polarized.
7. [bojana vuksanovic](http://bojanavuksanovic.blogspot.com)
2.9.09 / 12am


Sarajevo had a very specific position – for its geographical configuration also – closely surrounded by the mountains (constituting sort of walls, preventing the escape – one way to look at it). What happened to the citizens of Sarajevo is unquestionably one of the most sinister events of the 21st century Europe.  

Conflicts are always a composite occurrence. They form out of a web of events that weave over a period of time between the conflicting sides.  

Where there is an internal conflict, an external manipulation can happen effortlessly. In this sense, I do not believe in lines (walls) and in one sided guilt.  

A conflict can only persist if sustained by each side involved. Accepting the responsibility gives the power in realization that ultimately, the decision lays within each.  

I think that your thoughts (meditations, as you call them) on the wall that connects are very valuable. Thinking in contradictory terms always gives more space to interpretation, and, at its least, helps clarify it.
9. river
2.9.09 / 8am


The Architect wants a certain treeishness now. The Architect does not care about why the specific treeishness can't be manifested. She'll go to the [3d Warehouse](http://sketchup.google.com/3dwarehouse/) herself and shop for hours if she has to. She wants her presentation trees just so, on time.


The Architect wants color. He wants it now! He wants it exactly as it flows from his brush. Too bad for the Architect that he can only realise the promise of RGB in concert with [pigment](http://en.wikipedia.org/wiki/Pigment) engineers. The realities of actualising color take more time and money than he has.


The Architects, ultimately, want people. He and She both want people to move and live among points of free happiness. They want imaginary structures of tomorrow realized simultaneously.


Both Architects have a shared instinct about the [index](http://en.wikipedia.org/wiki/Ior) oops, [of refraction](http://en.wikipedia.org/wiki/Index_of_refraction) of the liquid product Windex. That's why they get along with each other so well.


What proportions of lemon juice and vinegar match the IOR of [Windex(tm)](http://en.wikipedia.org/wiki/Windex)?


I have personally experienced the reality of old newspaper being more effective than the latest, strongest paper towels.
11. [Dave](http://intheblackbox.wordpress.com)
2.9.09 / 6pm


Very interesting structure..and concerning the worry that the materials required to build such a thing (regardless of the problem of time or location) in a war time situation, if a country commits itself to total warfare.. if a total war effort is set in place the greatest projects can be achieved. Essentially if it has to be done it will be done if a country and its people are committed to it. 


However the irony of such an effort to protect a city is that in order to build it, the war effort would require the procuring of every available asset, including those already in use. Rubble, steel, glass, brick, wire mesh, concrete, stone etc, etc. these things can be found on the streets…but if need be the city itself would have to be dismantled and committed to the wall… i suppose starting at the least important buildings based on function and/or national/emotional value..the buildings themselves would have to be sacrificed just like the people.


And so in protecting a city from destruction the wall itself would in fact systematically destroy the city.


However if the people themselves die to protect it one could argue that this is no different from their homes being sacrificed also… 


…in the end i guess one would have to ask whether they are trying to protect the aesthetic qualities of the city or the territory on which the city is built.


If the latter is true then the theory behind the wall is sound. If the former is the case then the wall will only aid in total destruction. Architectural Suicide if you will.
13. Damir
2.9.09 / 11pm


What's beautiful and purely theoretical about the proposal is essentially the cities forceful evolution into the wall, which is what happened to Sarajevo as a response during the bombardment form the surrounding hills. The city became a wall with literary trenches as streets. What is more interesting is what happened in terms of social contracts among people at the time. If the attack shaped the evolution to dig a network of public trenches, could this wall be its long-term evolution? Which buildings are expandable for such a project commercial and social?  

“A conflict can only persist if sustained by each side involved.” What is genocide than a non conflict? An external conflict? Violent conflict can be created and sustained without violent opposition to the aggressor. What I like about this proposal (call it a wall or a punching bag or the heavy iron door to a castle), is its human venerability and its understanding that it is in some peoples nature to exercise violence if not in all of ours. It becomes completely symbolic, the effort to destroy coupled by an outlet for survival.
15. [bojana v.](http://bojanavuksanovic.blogspot.com)
2.10.09 / 2am


Damir: In my note, the word conflict was referring to the events over period of time. “They form out of a web of events that weave over a period of time between the conflicting sides”.


The events that brought things to the point of collapse. 


I am sorry if you misunderstood the word ‘conflict' to refer to one sided attack or genocide. This was not implied in any way.
17. [Maurits](http://www.nightlybuilt.org)
2.12.09 / 4pm


I fully agree with Damir. Today's sprawling cities are like horizontal walls. Gaza City and Rio's favelas have proven to act like sponges in response to military force. Isn't the wall about urban warfare?
19. djuro
2.26.09 / 7am


Your historical backdrop to this project is less than factual.
21. [Worldtravel » Blog Archive » Tunnelizing Migration 3: From Headwalls to Super Walls](http://tourist.bl0x.info/tunnelizing-migration-3-from-headwalls-to-super-walls/)
6.7.09 / 11am


[…] The wall of the Bosnia Free State, by Lebbeus […]
23. [Bridges Used as Transport Links, Eerial Housing, and Skyborne Agricultural Complexes, All in One « VANGUARQ](http://vanguarq.wordpress.com/2009/11/28/bridges-used-as-transport-links-eerial-housing-and-skyborne-agricultural-complexes-all-in-one/)
11.29.09 / 1am


[…] impact on the future of Bosnia; and Lebbeus Woods has long explored the architectural effects of political separation, from Paris and Berlin to Israel and Sarajevo, seeking out those fissures wherein geopolitics […]
25. [A Mountain of Metastrucure: The Wall | Robb Schiller](http://robbschiller.com/a-mountain-of-metastrucure-the-wall/)
1.15.10 / 1am


[…] Metastructure from Lebbeus Woods via Space Collective Gallery […]
27. [(B) for Blog » Blog Archive » METASTRUCTURE](http://createbuilddestroy.com/wordpress/?p=12098)
12.10.10 / 8pm


[…] M E T A S T R U C T U R E […]
29. [Zones of Contention | Between Borders and Frontiers « dpr-barcelona](http://dprbcn.wordpress.com/2011/02/24/zones-of-contention/)
2.24.11 / 9am


[…] readings: – On borders and walls [but not between US-Mexico but in Bosnia] Metastructure by Lebbeus Woods – Border Film Project. An art collaborative that distributed disposable cameras to […]
31. [Traverse I « boundarymkng](http://boundarymkng.wordpress.com/2011/03/02/traverse-i/)
3.7.11 / 11am


[…] 4. Metastructure: Bosnia Herzegovina […]
33. [METASTRUCTURE « LEBBEUS WOODS | Doozgle.com](http://doozgle.com/metastructure-%c2%ab-lebbeus-woods)
7.9.12 / 11pm


[…] via <https://lebbeuswoods.wordpress.com/2009/02/07/metastructure/> […]
35. [METASTRUCTURE « LEBBEUS WOODS « TOBOKOZ](http://tobokoz.com/metastructure-lebbeus-woods/)
7.9.12 / 11pm


[…] <https://lebbeuswoods.wordpress.com/2009/02/07/metastructure/> […]
37. [METASTRUCTURE « LEBBEUS WOODS | buruq](http://buruq.com/metastructure-lebbeus-woods/)
7.10.12 / 2am


[…] <https://lebbeuswoods.wordpress.com/2009/02/07/metastructure/> /* This entry was posted in Uncategorized and tagged LEBBEUS, METASTRUCTURE, woods. […]
