
---
title: ARCHITECTURE SCHOOL 201
date: 2009-02-16 00:00:00 
tags: 
    - architecture
    - education
    - school
---

# ARCHITECTURE SCHOOL 201



Once we have understood the basic structure of a school of architecture, its hierarchical—if paradoxical—composition of faculty, students, and administration, we can move on to the consideration of its content: what it teaches and why.


 Most schools of architecture today are ‘professional' schools. This means that their goals, methods, and content, are aligned with the demands and expectations of the profession of architecture, as generally accepted.


 It is interesting that a great university, Harvard, was once reluctant to accept a school of architecture into its program, because of its professional, not academic, orientation. The goal of Harvard's academic programs is to immerse students in different bodies of knowledge, in order to prepare them to assume active, creative, leadership roles in society. The university was and, perhaps, remains suspicious of professional training, which tends to be narrow by comparison, focusing on technical or other parochial skills. Even today, Harvard's Graduate School of Design (GSD) must support itself independently of the wider university's budget.


 Schools that grant professional degrees must be ‘accredited' by professional groups, notably (in the United States) the National Architectural Accrediting Board (NAAB), which is comprised of professional architects and educators approved by professional organizations—such as the American Institute of Architects (AIA) and the National Council of Architectural Registration Boards (NCARB)—who continually visit schools, examine them, and decide whether they meet the standards they have set to be accredited to confer professional degrees. A professional degree is, today, a necessary step toward becoming a state-licensed professional practitioner. Because most graduating architects want to design buildings that will be built in the public domain, and which requires a state license, the accrediting process has an enormous influence on the content of almost any school's curriculum. The power of a dean to lead the school in any but an orthodox direction would seem to be very limited. But this is not always the case.


A historical example of the exception to the rule is Irwin S. Chanin School of Architecture of The Cooper Union, in New York. In the 1970s and 80s, when John Hejduk was dean of the school, he pursued a radical architectural design program and managed, at the same time, to maintain the school's accreditation to confer professional degrees. It was not easy.


During that time, his faculty included Raimund Abraham, Peter Eisenman, Bernard Tschumi, Elizabeth Diller and Ricardo Scofidio, and a host of lesser known avant-gardists, who brought their radical ideas into the design studios they taught. Hejduk himself was teaching the fifth year undergraduate thesis, with an emphasis on innovative concepts of space and design. Seminars by the likes of Jay Fellows and David Shapiro added depth to the unconventional approach to architectural education. Often, accreditation committees would arrive at the school on their regular visits in a hostile mood. ‘What has all this to do with architecture?” was their question when they saw projects that did not resemble any buildings they had ever seen. While the school's catalogue listed an orthodox program of architectural studies, the ways in which the studios and classes were conducted were utterly unorthodox, and this worried the examiners. Yet, time and again, Hejduk—with the support of his faculty and students—was able to convince them that this was a valid, indeed a valuable approach to architectural education and practice. Hejduk's charisma? The sheer quality of the student work, however unorthodox? The need for change, however grudgingly recognized? The allowance for exceptions in a vast field of sameness? Certainly all these were factors in the survival of the professional degree program by a radical school. By the 80s, the worst fears of the more conservative critics and examiners were already being realized. With the publication of *[The Education of an Architect](http://www.randomhouse.com/catalog/display.pperl?isbn=9781580930406)*, a book laying out the school's philosophy and methods, a revolution in architectural education had already been effected, as many schools adopted aspects of Cooper Union's innovative approach.


The Cooper Union story is a prime example of working within and at the same time extending the limits of a prevailing system of orthodoxy. Whether the same story could be written today is an open question. One can certainly argue that it is easier to transform an already working system of education than invent an entirely new one. But the latter prospect may be necessary, if and when the prevailing system has failed. To consider that we should look at the institution of the Bauhaus, as well as more contemporary examples of experiments in architectural education.


 *(To be continued)*


*LW*


 



#architecture #education #school
 ## Comments 
1. Nikola Gradinski
2.16.09 / 10pm


The total requirement of regulating bureaucracies, in line with their corporate underwriters, to ensure that architecture schools remain in line with established guidelines and to produce conformist exponents seems to have created a system of one way education. One way because one can only enter a school of architecture at a specific point, at a certain age and level of mental cleanliness (in other words, not too polluted with ideas that may run contrary to the specific institutions' current ideologies and more importantly, brand standards) and because there is one exit point – the professionally endorsed degree, they are selling the complete package deal. Even at the most experimental and radical, this system seems oblivious, indifferent or unwilling to engage lateral currents, of which I am sure there are many. Granted there are material restrictions, but how do these kinds of institutions address the need or desire of architects who are interested in furthering their knowledge or exploring uncharted possibilities? Is the only way to remain on the periphery, buried at the heart of the corporate system, trying to conduct some kind of independent study? Even so, navigating this terrain will certainly not bring recognition or endorsement within or without the establishment any time soon, and the conditions may turn out to be too barren to yield any result in any case – perhaps only the most resilient may survive this no-man's-land.
3. river
2.17.09 / 5am


Curoius. One of my first students is applying to Cooper Union. She is one of my superstar students, but don't tell her that.. She is working on her degree via the hardware in my classroom even though she is finished with my class. I asked her what she was doing here in my class again. She said she was working overtime on her curriculum and applying to schools. I asked her which ones. She mentioned Cooper Union. I lit up. I said one of my favorite Architects was once a dean there. I told her an abbreviated story about my thesis. She said “Nobody else understands why I would want to apply there…” I hope she gets in.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.17.09 / 1pm


river: The Cooper Union is still a top school of architecture, under the current dean, Anthony Vidler. It has always been extremely difficult to get in, and is today even more so. The school is full-scholarship, meaning the students pay no tuition. In the current economic downturn, the are some 750 applications to fill 30 places in the first year. The same ratio applies to the few transfer places open. Here is a recent article in the New York Times: <http://www.nytimes.com/2009/02/09/education/09cooper.html?scp=2&sq=Cooper%20Union&st=cse>
7. Anonymous
2.17.09 / 11pm


Could it be that academic architects are pretending at being visual artists (with rules) and they sour at the thought of someone reminding them of the path they chose?
9. [lebbeuswoods](http://www.lebbeuswoods.net)
2.18.09 / 12am


Anonymous: yours is one possible reading of the situation. Another one is that the field of architecture needs new directions, and that the only way to find them is to make explorations. When one explores, there is no guarantee that one will find the best way forward. Still, the attempts have to be made.
11. Pedro Esteban Galindo Landeira
2.18.09 / 3am


What directions are you thinking, Lebbeus? And how the school will change to go on that direction?
13. Kevin Rhowbotham
2.18.09 / 10am


At a time through which the very nature of corporate reality will be drawn down to a point of disappearance it is self evidently inappropriate to valorise previous institutional practices which have failed to deliver the professed consequences of self interested philosophising.  

Neither professional nor academic propositions have offered successful solutions to a plethora of ingrained and perennial problems , legion within the social realities of global states, the consequences of which void the well meaning naiveties of both positions.  

Considering the position of schools of architecture is, given the implosion of the neo-liberal model of international capitalism, somewhat isolationist in its political scope.  

Things are not joined up. Self pleading is the order of the day. ‘It's not my fucking problem' , the most explicit sentiment allied to an all pervasive institutionalised Gallic indifference.  

As much as I would like to feel that schools of architecture can construct an opportunity to remake the world or even a fragment of the world in the liberal image of the global state's apologists, nothing could be further from the rude fact of reality.  

And what then is the material fact of things?….that schools of architecture, their pretentions, actions, productions, personalities………don't amount to a hill of beans; Sub specie aeternitatis.


Let the lions escape.


Kevin Rhowbotham
15. [lebbeuswoods](http://www.lebbeuswoods.net)
2.18.09 / 2pm


Kevin Rhowbotham: There doesn't seem to be a ray of hope in your comment, at least for schools of architecture. You have been a teacher in several schools over many years, and I wonder if you still are. If so, I would be interested to know your approach to teaching in light of your pessimistic outlook. Also, does “letting he lions escape” refer to the students, their imaginations…?
17. [lebbeuswoods](http://www.lebbeuswoods.net)
2.18.09 / 3pm


Pedro Esteban Gallindo Landeira: I do have some possible directions and will speak about them in a post I am preparing.
19. Anonymous
2.18.09 / 4pm


[Huh?](http://dictionary.reference.com/browse/academese) How do you draw the nature of a corporate reality *through* a time?
21. Pedro Esteban Galindo Landeira
2.18.09 / 7pm


Well I'm waiting for Architecture School 301(it's the one), I hope that school are an alternative for those who think the architecture as more than structure and space. I will be your fist student, jajaja. Please, If you could explain some exercise as example.
23. martin
2.18.09 / 8pm


“a revolution in architectural education had already been effected, as many schools adopted aspects of Cooper Union's innovative approach.”


which aspects were adopted? which schools in particular? are any of those schools truly on par with Cooper in its breadth of unorthodoxy? is it possible to only adopt SOME of its tenets and still utilize them effectively?


you wouldnt happen to know anyone there currently, who could help my recent application find its way into the right hands, would you?


just thought i'd ask..the squeaky wheel gets the oil.
25. martin
2.18.09 / 8pm


oh, one more thing. do you find that a schools principles are exhibited more strongly in their undergraduate programs, rather than graduate programs, due to the fact that graduate students are often attempting to pursue their own research goals.


in that nytimes article, there was no mention of the statistics for graduate student applications. hopefully its a significantly better ratio of applicants to open spots..
27. [Mark Primack](http://www.markprimack.com)
2.19.09 / 3am


Lebbeus,  

Are you aware that in many states a college degree is not required for architectural registration? Here in California, a high school graduate working construction and/or in an architect's office can become registered after as little as eight years. The shortest path a college-bound candidate can hope for is, I believe, seven. I can tell you the relative value of eight years experience in the field over any degree or diploma to a small design firm such as my own, but that might only reflect the salary each could expect. I can compare the relative wealth and mobility (financial freedom) of the high school grad to the probable indebtedness and insecurity of the collegiate (the bonds of financial ‘aid': one Harvard grad who worked for me was paying off his student loan to the tune of $700 a month). But I'd rather posit the possibility that a motivated, curious, debt-free young person able to afford periodic research sabbaticals and occasional travel, might acquire the values, skills, wisdom and idealism to become the architect you speak of- the architect I believe you are- without ever enrolling in a college program. Even the best of those programs reflect an age without an internet or a blog.
29. Kevin Rhowbotham
2.19.09 / 11am


I find it difficult to understand how a notion of hope may function for the dispossessed in any way other than as a mask to systemic alienation. You use the term hope rather like the term faith, or even talent ; terms which offer something at a distance , always displaced and never at hand. Terms which reserve power to the giver, like a gift might, begging a dependency.  

But I don't want to dwell on a single point.  

It has always been my personal contention that any educational institution which took itself seriously would, as its first declarative act, disband itself in the name of truth. At the very least it would construct the means of self-criticism. The fate of empirical ideologues is always a descent into febrile positivism.  

You ask me am I still teaching? As you know me, and since I have recommended to you, as a teacher to a teacher, the innocent and impressionable, you might have concluded, without too much reflection, that, for me at least, teaching is a matter of human grace, generosity and love; qualities , I seem to remember you exhibit in abundance.  

Let me answer you more directly, I contend that teaching is not subject to the institution, rather the institution is subject to it. We all have needs for this to be well remembered.  

This is precisely why the school is threatened by the teacher and the teacher undermined by the school; and can it be any other way? ….a dialectical inversion subtended by an uncritical need , common to all institutional interests, to perpetuate themselves uncritically and self-interestedly, in the face of self-evident social requirements. This last having been recently exhibited, and in an exemplary fashion, by the banking system so I understand.  

teachers teach that knowledge waits,  

leads to hundred dollar plates,  

 that goodness hides behind its gates……………


The purpose of teaching is empowerment; more precisely, the transfer of empowerment. This demands risk on the part of the teacher since the institution places a vicious break upon anything which attempts to divert its control.  

Subjects to be taught are mere vehicles and are of only secondary importance. It can never be the subject of teaching which is liberating, be it macro economics, techniques of torture , or architectural design, but rather the political will of the teacher to impart methods of inversion, necessary to make the world anew. This is precisely a negative project.  

If there is no social product to teaching, there is little social benefit. To wish for an isolated art without social determination is to wish for solipsism. This is precisely the position of architectural education , (a category which was itself manufactured by the academic project); solipsistic, self-referential, blinkered, decadent, careerist.  

The object of teaching is to secure the maturity of those who wish to be taught, the successful outcome of which must culminate in the redundancy of the teacher. 


To answer your implied question as to agency, vis…if you think like this what would you have us do?  

Disband all institutional forms of teaching: de-school society.  

Yes we can!  

You don't need a weatherman to know which way the wind blows.  

Rather than pursing a debate on architectural education it might be more pertinent to examine the structures which constrain the way education thinks of itself. The first topic might then be the ideology of corporate empiricism and its alternatives. This would unlock a means to establish different intellectual landscapes and different methods of reflection.





	1. dog
	7.10.09 / 10am
	
	
	Enough of this verbal self masturbation!!!… 
	
	
	Quite honestly there is nothing worse than the useless underskilled members of this profession who like to think they are architects discussing the system in which they created.
	
	
	Unfortunately there are thousands of people like you out there, ‘teaching' architecture… when if you were truly honest with yourselves, you are no more teaching arechitecture than actually prolonging the status quo in order for 1. you to keep your jobs and 2. so that you dont have to go into the real world and work in a profession in which you have no real skills to do so…
	
	
	architectural education, particularly in the UK, does not teach students what they need to learn and the profession has to pick up the pieces.. quite frankly after 5 years in university, most students do not know the first thing about the practical application of design within the commercial environment… 
	
	
	the profession is dying because the education sector is too frightened of being found out !
31. [lebbeuswoods](http://www.lebbeuswoods.net)
2.19.09 / 2pm


Kevin: You make many good points about the limitations of schools, some of which I actually agree with, such as their insularity and self-referentiality and how these work against improving the actual conditions people have to live with.


However, I do believe that schools can provide some things of real value:


—the space and time to think and talk freely about ideas, including those that are taboo or have no currency in the workaday world;


—a sympathetic, supportive community that shares interests, but not opinions. In the workaday world of capitalist competition people tend to become isolated and have to do it ‘all on their own,' or adopt conforming attitudes or ideologies;


—an organized community with the potential for political action or influence; 


—the possibility, even likelihood, of encountering people and ideas one would otherwise never encounter.


Then there is the fact that each generation must recreate for itself the sum of human knowledge. Only data can survive death. This recreation is a collective task, and a messy, inefficient one—a school is one place where the necessary experimentation (with its failures, too) can happen. Also, a school is, or should be, a place where the best data is available.


I agree with Dylan when he wrote, in the politically turbulent 60s that “you don't need a weatherman to know which way the wind blows.” But you might need one to know why it's blowing the way it is. Or to change the way it's blowing.
33. [lebbeuswoods](http://www.lebbeuswoods.net)
2.19.09 / 2pm


Mark: I stand corrected. I was under the mistaken understanding that a degree had become, in recent years, necessary to be licensed. It's good to know that the practical training way is still open. Still it's a tougher road to go, as the exams require knowledge (or data!) that is much more easily acquired in school.
35. [lebbeuswoods](http://www.lebbeuswoods.net)
2.19.09 / 4pm


martin: The “Cooper Union revolution” changed two aspects of architectural education. First, it insisted that the ‘program' or ‘the brief' should be written by the student for his or her project (rather than be handed to the student by the teacher). Second, the project did not necessarily take the form of a building per se. Both changes shifted more responsibility (and gave more freedom) to the student. 


Concerning the current Cooper Union: only this year, for the first time, will there be a Master of Architecture degree offered. There are less than a dozen (full-scholarship, tuition free) places open for the Fall of 2009. You can get more information by contacting the school.
37. [Mark Primack](http://www.markprimack.com)
2.20.09 / 5am


Lebbeus and Kevin,  

The ‘weatherman' referred to by Dylan does not know why the wind is blowing, any more than you or I. The meteorologist might, but the weatherman is just a pretty-face smooth talking to a camera while pointing at -what for him in the ‘studio' is- an illusory map. He may well describe the institution that Kevin condemns, and you may well be one of a very few exceptions to Kevin's rule. But this blog of yours alone defeats most if not all of your reasons for placing an exclusive value on the physical institution of architectural education. You sound just a bit like those record companies insisting that buying a CD is the only legitimate way to experience music. In Notre Dame de Paris, Dom Claude Frollo points with one hand to the Gutenberg bible and with the other to the buttresses of the great cathedral, and says, ‘This will kill that.' Well, might one not point to this blog on the internet and then to your precious college and say the very same words?
39. penelope
2.20.09 / 11am


Dear all,  

I was thinking that in many cases, individualism starts from the school itself. The way a teacher deals with his/her students can promote individualism and competition (not in its noble sense) instead of a sense of community and collaborative/team spirit.  

I think it can change one's attitude and actions towards the society if the school itself encourages collaboration, dialogue, sharing/discussing ideas, the ‘doing things together.' 


How a teacher (in the design studio) or a school of architecture could encourage the students towards a collaborative/team spirit, the sense of community while cultivating independency, responsibility and a personal way of thinking and working?
41. [lebbeuswoods](http://www.lebbeuswoods.net)
2.20.09 / 2pm


Mark: If a teacher is no more than a ‘presenter,' then they should not be allowed to teach.


The internet and blogs are already part of many schools' way of teaching. Though they will no doubt increase in their influence, it's unlikely that they will supplant the school as a physical place or as an institution any time soon. Personal contact is still important. And, until the true egalitarian society is created, people will still need institutions—of government, law, education—to mediate for them in a world of competing and often conflicting interests. 


The question, then, is not whether we will have institutions—we will—but what will be their content and goals, and how will they work.
43. [lebbeuswoods](http://www.lebbeuswoods.net)
2.20.09 / 2pm


penelope: You're right. In my advanced design studios at several schools, I and my colleagues have framed collaborative projects and devised methodologies for students working together, yet at the same time being able to express their individual ideas. See previous posts TOWERSPACE, MATRIX and PROTO-URBAN CONDITION, as well as on my website [lebbeuswoods.net] in Studios, under Work. 


Creative collaboration is the future…I hope. It is up to schools and teachers to figure out ways to make it happen.
45. russellclark
2.24.09 / 11pm


lebbeus :: thankyou again for the sarajevo talk and workshop you presented at unbau. 


i wasn't aware cooper union started an m.arch program.. perhaps a phd program is in order as im nearly finished with my masters.
47. peridotlogism
3.3.09 / 6pm


“Still it's a tougher road to go, as the exams require knowledge (or data!) that is much more easily acquired in school.”


Then why the IDP process? The picture, as I've seen it so far so please consider my naivete, is that becoming a Licensed Architect (which is the professional/academic culmination, but certainly not the ‘educational' terminus) requires field experience precisely because academia is intentionally incomplete. 


The changes in consciousness cultivated in design school are without merit until they are refined and battered by the changes in conscience which are prevalent in the professional field. At least this has been my experience.
49. [archiz](http://www.archiz.com)
9.26.09 / 6am


Glad to know you guys exploring the new way of architectural education.
51. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.19.10 / 10pm


[…] lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 […]
53. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.21.10 / 10am


[…] lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 […]
55. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] his blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
57. [Lebbeus Woods on Architecture School | Architecture EV](http://architectureev.wordpress.com/2011/01/02/lebbeus-woods-on-architecture-school/)
1.2.11 / 6pm


[…] School 101 Architecture School 102 Architecture School 201 Architecture School 202 Architecture School 301 Architecture School […]
