
---
title: ARCHITECTURE SCHOOL 202
date: 2009-02-27 00:00:00 
tags: 
    - architecture
    - education
    - schools
---

# ARCHITECTURE SCHOOL 202


![vkhutemas8](media/vkhutemas8.jpg)   ![bauhaus5](media/bauhaus5.jpg)   ![lwblog-archschl202-35](media/lwblog-archschl202-35.jpg)


Some systems and institutions become moribund. At the beginning of the 20th century, European monarchy was one of them. With industrialization and the ascent—by virtue of wealth—of the middle, merchant class to social and political power, the idea of rule by an unproductive aristocracy that derived its power solely by inheritance was dying. Although a few monarchies survived what was then called The Great War, they were not autocratic, but constitutional, that is, with power greatly limited by democratic political processes.


At this same time when the political landscape was changing, some architects believed that architectural education had to change, too, and in much the same way. The idea that architecture should be derived from the models of history—by inheritance—was no longer thought appropriate, even adequate, for the education of architects in a world of new materials and technologies, and new social responsibilities to urban dwellers. Architecture could no longer be considered the instrument serving royalty and aristocracy, demonstrating their wealth and power in monumental civic buildings, but had to respond to the growing masses of industrial workers, living in cities, who made the factories hum and brought profits to their owners. There had to be a new architecture and new schools to educate the architects who would make it. The old educational system had to be replaced, but with what?


The Bauhaus (founded 1918, Weimar) and the Vkhutemas (founded 1920, Moscow) were two new schools established to enable radical social changes underway in Europe after World War I. The aim of both was to integrate the fine arts and crafts, which were previously separated, thereby erasing class distinctions. They both flourished in the 1920s and were terminated when reactionary political forces achieved power and saw them as an ideological threat. In their brief life-spans they managed—-by the combined brilliance of administrators, faculty and students—-to produce creative work that changed ideas and methods of art and design for decades to come. Shorn of their ethical and political purposes, however, their advances were reduced to stylistic choices. In the present postmodern, consumerist society, one can adopt a Bauhaus aesthetic or a Constructivist style without the slightest awareness of the ideas or intentions that inspired them. Such is the abstract nature of modernism, or the decadence of the present society, depending on point of view.


More recently, there have been other attempts at forming new, independent schools. The Architectural Association in London, which sprang to life in the 1960s and 70s was, to quote a recent commentor on this blog,  “originally formed as a ‘club' of young architects who hired people to teach them what they were interested in knowing at that moment. No tenure, no repetition, no hierarchy.” The school continues today.


 SCI-ARC (the Southern California Institute of Architecture) was founded in 1971 by “a small group of emancipated faculty and students, most of whom had rejected the prevailing institutional models of the time, in favor of a more free-form intersection of teachers and learners, a patient critique of the old idioms, and an aggressive pursuit of the promise of an ever-renewable pedagogy.” The school continues today.


 The RIEAvico school (the Research Institute for Experimental Architecture) was instituted in a small village in Switzerland in the late 90s as an experiment in the education of architects, focusing on collaborative urban interventions. It no longer exists.


 One thing we learn from all these attempts is that as schools move toward obtaining official approval, either by government or professional organizations, some energy vital to their independence is lost. Not all, but enough to say that becoming legitimized by the professional architectural community extracts a price in freedom of thought and method. Not a fatally high price, necessarily, but enough to raise the question: why is professional certification necessary? Why cannot a school of architecture remain free?


Today, if one were founding a school of architecture with a radical program, defined in terms of philosophy, method, or the content of courses, past experience would seem to indicate that it should remain free of control, or even significant influence, by the established architectural profession. In such a school, no professional degree would be offered. If the school were connected to, say, a small college or university, a normal undergraduate degree could be offered. This would mean the students of architecture would necessarily study humanities, literature, language and other requirements for a ‘liberal arts' degree—and that would be a very good thing. With such a degree, graduates could apply to one of many master of architecture programs, if they were interested in getting a professional degree. The crucial distinction is that their basic architectural education would be free of the constraints (some would say the bias) imposed by professional accrediting institutions.


An even more radical approach would be that the school of architecture would be completely independent, offering not an academically accredited degree of any kind but rather a diploma. It used to be, in Europe, that the diploma from a highly regarded ‘academy' would be accepted by universities and professional programs of study, but the ‘Americanization' of European university education has all but ended that practice. In any event, there are two groups of people unconcerned about professional degrees: those who want to study architecture, but not practice it, and the idealists, who will find their own ways to practice, and on their own terms.


 *(to be continued)*


*LW*



#architecture #education #schools
 ## Comments 
1. [Topics about Home Decoration » bARCHITECTURE/b SCHOOL 202 « LEBBEUS WOODS](http://homedecoration.linkablez.info/2009/02/27/barchitectureb-school-202-%c2%ab-lebbeus-woods/)
2.27.09 / 10pm


[…] lebbeuswoods added an interesting post today on bARCHITECTURE/b SCHOOL 202 Â« LEBBEUS WOODSHere's a small readingThe idea that barchitecture/b should be derived from the models of bhistory/b—by inheritance—was no longer thought appropriate, even adequate, for the education of architects in a world of new materials and technologies, and new social b…/b […]
3. river
2.28.09 / 6am


[Yes!, but not quite the same band of the same name.](http://language.nativeweb.org/Lakota_translation.htm)
5. JC
2.28.09 / 7am


Now that we have a new wave of moribund economic institutions I agree with you that we should have a concurrent new way of teaching architecture. One not tuning architects to peddle their designs to the capitalist powers that be, and then letting them build what they will until the money runs out, but one in which the designers learn some of the construction agency for themselves to shape future cities. Though this sentiment seems to be echoed in many schools now, I wonder if it is just another shift in rhetoric. Since the upheaval and creation of the new architecture schools of the 70's architecture theorists, now the deans and teachers of the finest schools in the land, have perfected their revolutionary lingo. At least so it seems. As one who, after working a few years in various sides of the architecture/design table, is waiting to hear back from my graduate school applications, I am wondering what is the value of a $100,000 degree. To learn from these theorists and writers how to construct a more agile and meaningful practice is? Don't get me wrong, I am truly looking forward to getting back to school for all the reasons you mentioned in previous posts; time to read, experiment, and expound on deeper meanings and lateral relationships into other fields. But I'm still a bit nervous jumping into it for no other reason than to for people to take me more seriously. While I used my time well at your old alma mater, the U of Illinois, taking classes and developing relationships with people outside of the architecture school, I still slogged through 4 years of structures, mechanics, and design. Now I'm learning that that may have to retake those classes in a three year program somewhere. Had I known better I would have done as you said and focused on the pure liberal arts, then done the professional degree.
7. Pedro Esteban
3.1.09 / 2pm


If that new school has the ideas of Bauhaus or Vkhutenas, will be a school with a historical perspective,  

which will be the perspective of those schools? Which political principles, of our days, will affect the school? None? You believe that can be possible today?
9. sergio machado
3.1.09 / 9pm


If all that we can learn from history is style, so maybe it's time to define style.
11. sergio machado
3.1.09 / 9pm


Richard Meyer said that the light inside the baroque churches of Germany were inspiring to him.  

I think history has many things to teach…
13. spencer
3.2.09 / 5am


Lebbeus,  

I'm right with you. Do you think this method of education will create parity in education where we all will be getting the best education? Or will we continue on with the class based system we are using? Will the degree/diploma from Arizona be as useful as the degree/diploma from Andrews University be as useful as the degree/diploma from Columbia, etc.?
15. river
3.2.09 / 8am


I should have linked to [this](http://pro.corbis.com/search/Enlargement.aspx?CID=isg&mediauid={05C27593-83C5-49DD-8750-D875B7CB2A93}) picture earlier, rather than that dictionary. In all that has happened since the proto-1600's (which is about the scope of my educationally enhanced pre-Wikipedian peripheral American memory), one of the most heinous, imho, is the re-education protocol for native Americans (there are many equally disturbing parallels, of course. Including the in-progress closing of Gitmo). One of the most exciting, however, is the unexpectedly rapid evolution of multi-dimensional computer communication from the prior intent of hard-core, full-fledged, cold-war, self-defense funding and purpose. The aging allied scientists just wanted to interact with the new tube computers in a more natural way. They wanted to draw. Then, they wanted to talk to each other about it across continents… Then they found themselves talking shop across borders with their enemies. They were still, mostly all guys. A lot of them liked poetry and maths. They shared some of their favorites! Then 1984 came and went.


Yet, if Turing hadn't successfully cracked Enigma, he would have been hung, later, on sight. I hope we all understand what we are saying right now, including myself.
17. Trumbo
3.2.09 / 12pm


This is becoming more and more interesting.  

Now i wonder about worries like the “master and disciples” warning, rather than a truly open community in deep contact with its environment and society. Also worried about this “cool” exploitation still surviving in the studios, workshops or schools, now calling it interns or whatever, etc. But finally i do agree that architects role in society is in urgent need of redefinition. and therefore, schools must evolve. A lot of damage has been done so far by not doing this precisely, accurately, responsibly.
19. [Mark Primack](http://www.markprimack.com)
3.3.09 / 5am


Frank Lloyd Wright claimed that an architect did not hit his (sic) stride until he was fifty. Though I suspect he was eighty when he said that, I believe he was referring to something beyond experience or knowledge. Art is worthy to the extent that it is practiced as a craft, that is, as a discipline born of humility. Wright's craft transcended his narcissism. I think your own craft has carried you past the confines of academia. I'm very interested in your views on the place of craft in architecture, and particularly it's place in the ‘education' of architects.
21. joe
3.4.09 / 7pm


Mr. Sergio,


You mention styles… and defining sytles… yet you clearly didn't read what lebbeus said, or you would have realized that you categorized yourself under what lebb calls the “present postmordern consumerist society” or your behaving in the “decadence of the present society”…


In talking about these past examples of reform in education… leb states “creative work that changed ideas and methods of art and design for decades to come. Shorn of their ethical and political purposes, however, their advances were reduced to stylistic choices. In the present postmodern, consumerist society, one can adopt a Bauhaus aesthetic or a Constructivist style without the slightest awareness of the ideas or intentions that inspired them. Such is the abstract nature of modernism, or the decadence of the present society, depending on point of view.”


anyways, stop trying to define style. that will happen on its own. however, you can try to understand whats gonig on around you and the effects our architectural implementing of any which style will have on society!
23. [Alex](http://sohbrstudio.com)
3.5.09 / 6pm


RIEA:Vico still exists as an outpost of SCI-Arc.


<http://www.i2a.ch/>
25. [lebbeuswoods](http://www.lebbeuswoods.net)
3.5.09 / 7pm


Alex: As an outpost of SCI-Arc it is no longer RIEAvico. This is more than a change of names, but a difference in method, philosophy, and faculty.
27. sergio machado
3.6.09 / 1pm


Joe:  

I think that labels such as “postmodern consumerist society” and “unproductive aristocracy that derived its power solely by inheritance” do more for obscuring than to clarifying ideas. Society is plenty of diversity and, depending on how we define production, we would have to include among the unproductive aristocracy, the Greek philosophers and the Medicis.  

I agree with Woods, when he says that today “one can adopt a Bauhaus aesthetic or a Constructivist style without the slightest awareness of the ideas or intentions that inspired them”, but that not imply in the irrelevance of history.  

For me, style is more a question of Form, than of Shape. Otherwise, as Aldo Van Eyck said, “an architect can not be prisoner of tradition, in an age of changes, (but) he can never be even, prisoner of changes.”  

Architecture is not about using new materials and technologies.
29. [lebbeuswoods](http://www.lebbeuswoods.net)
3.6.09 / 1pm


sergio machado: The Medici were of the merchant class (bankers) and not of the aristocracy. The Greek philosophers were mostly from the citizenry of their various city-states—many worked to earn their time to think by teaching.
31. sergio machado
3.6.09 / 4pm


lebbeus: You are right, but in a figurative way, we can use “aristocracy” to indicate a group of people that distinguish themselves by skill or other forms of merit, even if they do not deserve it: it's a privilege. But this is not the most important issue in your reflections. I think we value excessively systems and institutions, even when we fight against them. You pointed an (the?) excellent alternative when talking about the importance of faculty: the action of individuals will put things in movement and change. In such an ambient of freedom, those who believe that architecture is most continuity and not eternal disruption, may not be disqualified.
33. will2build
10.7.09 / 5pm


Mr. Woods,  

Your Architecture Schools series is vital for everyone- student, faculty or otherwise. I agree with your points and appreciate your acute interpretation of recent history. Yet I wonder if there isn't an alternative to your either/or conclusion: the way of the hacker. Losing none of his idealism, he ably navigates the messy fissures of our moribund institutions, appropriating the unique opportunities of a broken system. Don't get me wrong, though- I'm all for a new paradigm, just unsure we have the critical mass.  

Thanks for your guidance.
35. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.19.10 / 10pm


[…] lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 […]
37. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.21.10 / 10am


[…] lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 […]
39. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] blog. In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
