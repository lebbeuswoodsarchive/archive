
---
title: WORLDS APART
date: 2009-02-20 00:00:00
---

# WORLDS APART



The fragmentation of existing systems of order under the intense pressures of change has yielded new, more complex systems we have yet to formulate, hence understand, in clear terms. A period of exploration has been forced upon us, and we have no choice but move deeply into a new landscape of unknowns. The previous systems of order, which are based on well-understood principles of unity, repetition, variation, and hierarchy, are useful only as points of origin, and no longer as destinations. Entering the unknown terrain, we understand that unity, even totality, can no longer be rationally achieved. We must say goodbye to them as we would to old friends whom we will never see again.


We are confused by the apparent randomness—the seeming chaos—of the new conditions. We cannot easily accept the random as reasonable—our self-consciousness insists on control, and the random resists control, in the same way as a storm or an earthquake. The random pushes us toward mere reaction, and we lose initiative. We throw the dice, or the straws. They land this way or that, and we struggle to interpret. Our creative powers become useless. But, maybe not.


 In our explorations and experiments, we discover that there is a human equivalent to randomness, and that is spontaneity. When we are spontaneous, that is, when we act without thinking, we are not really acting without any thought about what we are doing, but releasing our personal knowledge in other than the step-by-step way we—in the West—usually consider rational. We cannot be random in the same ways as the world fragmenting around us, but we can engage the randomness, not merely react to it, with our powers of spontaneity.


Architects are the most controlling of creators, who want to see the final result of their work in advance and then do all they can to see it realized. They are, accordingly, the least spontaneous of people. They work well, and always have, with autocrats in business and politics, who also have pre-determined goals and are ruthless in pursuit of them. Autocrats uphold the old systems of order, which they understand very well how to control, and so do architects.


A newer generation of architects will certainly take up the challenges of complex and confusing new systems of order by engaging the fragmentation and randomness with new principles of design that do not insist on controlling outcomes, and integrate their own spontaneity with that of the many who build—and inhabit—our emerging worlds.


LW



Early studies for *The Fall* installation in Paris:


![lc3-2](media/lc3-2.jpg)


![lc9-21](media/lc9-21.jpg)


![lc4-22](media/lc4-22.jpg)


![lc6-25](media/lc6-25.jpg)


![lc21-22](media/lc21-22.jpg)


![lc7-22](media/lc7-22.jpg)


![lc8-22](media/lc8-22.jpg)


![lc10-22](media/lc10-22.jpg)


From the Carnegie Museum of Art exhibition “Lebbeus Woods: Experimental Architecture” (2004-5):


![cmainst5b-21](media/cmainst5b-21.jpg)


![lc16a-2](media/lc16a-2.jpg)


![lc16b-2](media/lc16b-2.jpg)


![lc17a-2](media/lc17a-2.jpg)


![lc23x-2](media/lc23x-2.jpg)



 ## Comments 
1. Pedro Esteban Galindo Landeira
2.20.09 / 6pm


what do you think about the random, or chaos as fashion on design?, when the years past, at the end it will be like the modern movement, who was copy from europe to Brasil, and failed in some cuestions for me, the same could happen to this new way of design?,  

(this is an historic point of view, we could think in a futuristic point of view and this change completely)
3. James Groundes
2.20.09 / 6pm


I've just recently read a paper on the very subject that you write about, called ‘Planned Spontaneity' by B. Vuksanovic


(available at: <http://www.plannedsp.blogspot.com>).


You might find it interesting, 


I certainly have, best, James
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.20.09 / 7pm


James Groundes: Thank you. Fine piece by her. No doubt one of the ‘newer generation' I hopefully refer to…
7. [lebbeuswoods](http://www.lebbeuswoods.net)
2.20.09 / 7pm


Pedro: At some point(s) modernism was a fashion, too, and this can be a problem. However, new ideas have to be brought forward and ‘suffer' some popularity in order to take root. Let's endure that and move forward.
9. Raul
2.21.09 / 12am


I totally agree and would add the following:  

What is curious to me with regards to spontaneity in architecture is the unfortunate misreading by many in academia of the notion of morphogenesis as articulated by Manuel DeLanda. Whereas his incredibly clear and rich propositions call for us to re-investigate the emergent nature of materiality and expression, architects seem to pathologize these truly revealing ideas by attempting to ‘set up' a process of emergent architecture, embedding (or better locking in) spontaneity to the process itself. The irony of emergent-based methodologies and processes so popular in architecture schools is that they ultimately betray their initial intent by demanding of this process a rigor of emergence, as if suddenly process was independent of the author. What should be a freeing of conventions, protocols and biases turns in on itself and becomes a new ‘emergent' set of conventions and protocols. True spontaneity, i.e., the freedom of turning the whole process upside down, inside out or of ‘randomly' taking one fragment and re-inventing it, is what gives back to the author his or her power of agency over the entire process which can then yield a truly unexpected outcome. Are architects afraid to be authors and respond to their random moves?
11. ysgs
2.21.09 / 12am


As contemporary physics, and mathematics have shown us, our world is not as simple and as understandable as the mid 20th century modern mind wanted to believe, on this I must agree with you. It is now the time for us as architects to experiment with far more complex systems of order and to transform them into the built environment.  

But I think one should differentiate between complex, adaptive or emergent systems and chaos or randomness.  

Although as a musician I am a great lover of improvisation, or of 12 notes music, I would still fear random architectural design, as this can be understood as the freedom of the architect to organize the windows on a building's façade randomly.  

I guess I would appreciate much more a well planed project which enables emergent and complex systems to be activated upon it, then to see the frozen built spontaneity of an architect's design process.  

(for example U-Tube has created one of the largest complex systems we know, by producing a well planed base for it to grow on.)  

So wouldn't you prefer the possible spontaneity of the built environment, than the one which can be exercised by the Architect in his design studio?
13. [lebbeuswoods](http://www.lebbeuswoods.net)
2.21.09 / 2pm


ysgs: I understand and share your fear of ‘freezing' spontaneity in fixed architectural forms, which is really a contradiction, anyway. Rather, what I have in mind is architecture as transitory as music. One example of a proto-architecture of this type is the SYSTEM WIEN project—a spatial construction of constantly changing energy vectors.
15. Pedro Esteban
2.21.09 / 4pm


James Groundes:  

I'm a student, and I allways are looking for answers of things, that paper was a lot of responses for me, I prefer to call it a masterpiece, and her work really good too, do you saw the set for Bukowski?
17. Raul
2.21.09 / 5pm


I look forward to researching about SYSTEM WIEN. In literature (and I am not sure if there is an analogy here) Calvino and George Perec with his system of poetics have been a constant source of inspiration for an integration of improvisation within a methodology, applying certain constraints to liberate oneself and explore actualizations of expression. 


It seems like in architecture there are many mine fields one must traverse (in theory and practice) in order to stay fresh and allow a project to take on all of its positive potentials to emerge. Ultimately architects have a great responsibility towards ensuring that whimsy is not confused with true exploration and the example of a random organization of windows on a facade would certainly fall in the first category. But one can also come up with a ‘process' through which windows on a facade emerge and are then justified by the process and this goes back to my earlier comment, that just because an organization emerges out of a process it does not mean that it is final (although it could be). 


If you look at inventions throughout history many have emerged out of random or chance discoveries, so it is a door which should always remain open.
19. [lebbeuswoods](http://www.lebbeuswoods.net)
2.21.09 / 7pm


An introduction to the SYSTEM WIEN project is on my website [lebbeuswoods.net] in the section Work under Projects. There is a book through amazon.com and other bookstores. I will make a post about this project very soon.
21. [Adam](http://www.vad.am)
2.22.09 / 1am


Controlled randomness. Maybe that's the model which has been adopted by architects in their new design methodology. I think it's associated to contemporary state of computer generated design. Using evolutionary digital system that can run endless, multiple equations, designer is able to stop the process in the right (spontaneous) momentum in order to synthesize envisioned effect. The problem however, is that this process involves intuition on many levels (recognizing the right moment to stop). And intuition, if bad, is leading to formalism. Is it possible to separate spontaneous trajectories from accidental? Can spontaneous-ness be thought? Can someone's intuition be equally evaluated by everyone?
23. river
2.22.09 / 8am


The most powerful feature, IMHO, of Google's SketchUp, which I teach for four or five weeks before submerging my new student's heads in Autodesk's 3ds Max Design 2009 Educational version, FOR EDUCATIONAL USE ONLY!, is the Shadows dialog box. Which can be accessed from the Window menu.
25. rmark
2.22.09 / 1pm


to concur with ysgs, I think, isn't the spontaneity, randomness and chaos we speak of really just part of the designers process? While the formalism and unity we desire (or not) aspects of the outcome? I still have a hard time believing the earthquakes, diseases, and loves of the world are spontaneous but rather unexplained. But maybe I just belong back in the 20th Cent.
27. [Mark Primack](http://www.markprimack.com)
2.22.09 / 4pm


“The previous systems of order, which are based on well-understood principles of unity, repetition, variation, and hierarchy….”, apparently include current schools of architecture (in all their supposed variety) and are, I agree, most likely dead men walking. It is important to remember that my alma mater, the Architectural Association, was originally formed as a ‘club' of young architects who hired people to teach them what they were interested in knowing at that moment. No tenure, not repetition, not hierarchy. Ironically then that the emergent (spontaneous) Deconstructivist movement was initially referred to as the ‘AA style.”  

But, in a world of random overload, a new Way of Spontaneity could easily be characterized thusly: “If you put enough monkeys in a room with typewriters, eventually you'll get the compleat works of Shakespeare.” Of course with spell check the time would likely be cut in half.  

And then there is the question which lurks in the background of your wonderful blog: should Architecture free itself from the burden of building. I've personally embraced both building and spontaneity (both my client's and my contractors can attest to that) but I doubt that much of my work is publishable (i.e., Architecture).
29. [lebbeuswoods](http://www.lebbeuswoods.net)
2.22.09 / 5pm


Mark: Your historical perspective about The Architectural Association is correct. I think we need a contemporary version (not a copy) of that school in its early days—60s and 70s—probably making full use of the internet. 


Your spontaneity remarks, however, miss the point. If we were to turn loose the monkeys, why wait for them to reproduce a pre-determined expectation—Shakespeare's plays—when it would be far more interesting and fruitful to look at any outcome they produce and discern the order in it.


The point of cultivating spontaneity, in the self-conscious, socially dependent, and up-tight human camp, is to circumvent the foregone conclusion and make necessary new discoveries. These have to be evaluated to be of much use and that will require a new system of rules, and a new and fresh discourse (which a school and a blog can provide). What I am trying to imagine is the methodology by which we could invent discourse and rules. The drawings and constructions are their imagined outcomes, heuristic guides and not goals, that demonstrate new ways of thinking and working, and not ‘final products.' Under the desired new rules, it will be impossible to determine any final outcome in advance. However, without projections of final outcomes, and critiques of them, we reduce ourselves to the aforementioned monkeys. Not a goal I have in mind.


I must say that I have not succeeded in clarifying this crucial point about my projects from the late 80s, as they are still largely seen as finished design products. There's a lot left for me to do in this regard. Our discussions help.
31. [lebbeuswoods](http://www.lebbeuswoods.net)
2.22.09 / 7pm


rmark: yes…you're still back there. The idea of spontaneity is not new, but has always been part of the designer's process, as you say, which has then been subordinated to the determination-in-advance of a design product. What I am talking about is a whole new process of design. In ‘The Fall' installation in Paris, for example, I designed the rules for shaping the space, but not the final shape of the space. This was determined by collaborators, who were free to determine the space as they chose, in response to conditions on the site, as they developed. The result was far more alive than I—or anyone else—could have designed.
33. [Mark Primack](http://www.markprimack.com)
2.23.09 / 12am


Lebbeus, I'm no stranger to missed points, but let me try to clarify mine.  

 Forgive my ad mass reference here, but I recently watched an early episode of Madmen, in which an advertising executive advises a budding copywriter, “Understand the subject as deeply as you can, and then forget about it.” I think that clearly outlines a foundation of spontaneity, along with Robert Irwin's “seeing is forgetting the name of the thing one sees”. Take away the personal striving for a deep understanding of the world, or any part that exemplifies the world (a building, a lover, a child, a farm, a city), and you are indeed just a monkey in a room with a typewriter. Except these days you're alone on a laptop in MySpace.  

 The world cannot persist without spontaneity. Nor can it survive without discretion or love. We are here not to exploit these things for our careers, but to absorb them into our consciousness, and then- as architects- to protect them, and so save the world.  

 The hardest part of that is knowing and accepting that saving the world rarely gets you published.
35. ysgs
2.23.09 / 1pm


rmark: I guess I share your feeling that the world around us does not necessarily operates randomly,  

That's probably why I'm concerned about random design methods.  

Yet again I would love to see more projects which enable a later spontaneous usage, or Projects which can react spontaneously to changes in their environment.  

I guess the main difference I was trying to point out was between a spontaneous design process and a spontaneous outcome, meaning the built project itself would be able to show “spontaneity” to some extent.  

If I understood them correctly, I find the fall installation example given by LW a good and inspiring example of how an architect can leave his project free to the user's (as well as its own) “spontaneity”, allowing it to spread its wings and fly to a direction which cannot be predetermined by linier planning, whereas the System Wien project is a good example of the representation and embodying of such complex and “spontaneous” systems within a closed environment.  

I guess now we could just wait until the industry enables us to take these ideas out of the laboratory and transform them into large scale structures, letting human behavior and site surrounding's conditions be the vectors to the creation of a new always changing architecture.
37. river
2.25.09 / 9am


The media, the homework, the tests, the financial obligations, are actually barriers to education. Real education is about just getting it, neurally.


Quests are a better measure. Either you get it, or you don't.


my Big Audacious Goals?  

Hmm.. I don't share them with just anyone. (Mark's Primack's comments are gerrmain)  

Ok = GOKU, er, GOTO er, [Totoro](http://en.wikipedia.org/wiki/My_Neighbor_Totoro), whatever!..


1. Wildernets, and global environmental reform (aka. de-sub-urbanizing, re-concentrating)  

2. mmods – massively multiplayer online design software  

3. creative businesses that move like web XO from home  

4. financial success without borrowing time and vital resources from the home pantry  

5. debt free and self-sustaining in all-weather


memory is a model  

the future is a model  

only the present .. is.


we should become facile in metrics like  

 the rest of the world, and  

 the Western cabinet industry did  

 the games industry has


7.5cm = 2.95″ (or retro ‘Anti-tank killer' and 3″ FILLER, field cut)  

space, form, and shadow  

with variance, surface, and acceptable error


means study:


intricate details that turn into black blobs at wider scales if they are outlined by algorithms.  

Yet, they disappear upon close inspection if they are not.


Actually Teaching the intricacies of 3d to real students reminds me of Language, Math, Art, Poetry, Music, and Dance all-at-once.


You have to outline the structure of your model for peer to peer communication  

You have to calculate your geometries precisely to scale  

You have to get the compelling look  

You have to move it like poetry in motion  

Then, you sync it with fresh Music  

In the broad context of the Game's rules, which you interpret for yourself.


It reminds me of being a chef more than a chief.  

The world needs chiefs that are also chefs. (without the capital I, just a Capitol i)


Principles before Principals. There is a whole lot of multi-disciplinary education underwriting the sharing of one's light nowadays. If I could afford to access it all, I would be better. In this brief segment of history I CAN, almost!


Obama spoke Nationally to our Congress earlier tonight, and as always, I just can't hold my private tear of unbelief back.


Confidence in our kitchens will be restored, eventually, at home.


(not least, for the returning soldier market..
39. [Arthur McGoey](http://amcgoey.net/)
3.8.09 / 3am


Lebbeus,


Your comments about designing the process of construction, i.e. setting forth the rules through which those constructing the space can use their judgment and intuition is probably the clearest and most effective example I have heard to bring true spontaneity to architecture. As others have said, too often these days, emergent design is thought of as the design process only, not the process of construction. Architects are coordinators and facilitators of a complex set of processes that can but don't necessarily result in a construction of some sort. The number of people and resources needed is often such that the organizers are unwilling to risk acts of spontaneity. So though an architect might introduce “randomness” and “emergence” into their design process, it still fails to address the much more significant act, the act of construction itself, in fact it often needs much more rigid control of the act of construction, losing any resemblance to the ideas that it claimed to embody in the act of design. True emergence would result in the complete process from construing to constructing and beyond to habitation.


As Architects, we often think of ourselves as being in charge of the design of a building, when in fact we should think of ourselves more as process engineers. Despite what we like to of ourselves, Architects are not really artists, but facilitators and coordinators of the process, not the design. In my opinion this is a richer role and one filled with opportunities.
41. [Bojana Vuksanovic](http://www.bojanavuksanovic.blogspot.com)
3.22.09 / 2pm


I can see the approach in organization and subsequent execution of your project to be very relevant/interesting, especially since it is moving beyond the theory.


Following your discussion. I add a few notes, (copy of clarification notes, ”Planned spontaneity' paper <http://www.plannedsp.blogspot.com> ) with a few additional observations on the subject. I can see ‘spontaneity' to be very much identified with ‘random', and this I think needs more consideration.


The intention is to work more on these questions, to find actual/material ways of implementing it, devising concrete strategies.


————————————–


/notes/


The spontaneous processes are not necessarily random. There seem to be forces that, looking at mathematics of their interrelationship with other occurrences, appear to be following a very definite sets of rules. For many processes, there seem to be a structured path along which the change is happening (e.g. growth of organism, ruled by the DNA information code), yet, they do have reactive and adaptive capabilities.


The term ‘spontaneous' is not referring exclusively to organic processes. There are chemical and physical processes of dead matter going through spontaneous changes. Organic is not truly opposed to the orderly, there are countless examples of spontaneous processes (in organic and also non organic world) that follow a very predictable path.


The liquid states are the systems with rules governing their overall ‘form' and behavior, but within it there is an individual freedom of movements of particles. Solution suggested (refers to quoted article) a neutral configuration (which for that matter, could have very simple/or complex formative rules) but of such an order, that its major characteristic would include the ability to act as ‘cast' of the existing conditions and change/therefore to act as liquids. In this sense, suggesting indeterminacy at the initial stages.


It seems that there are some misinterpretations of terms ‘random', ‘spontaneous' and ‘organic'. Equating ‘spontaneous' with ‘organic', likewise equating the meanings of ‘spontaneous' with that of ‘random' and also of terms ‘random' and ‘organic'.


Organic, in my view, involves no planning. The next level of planning should aim for the level of technology and the knowledge, where we could consciously (rationally) use these processes of spontaneous change, to our benefit. Perhaps through giving the design in such a configuration, where this ‘flow' of change, that is evident, could be used to ‘run it' for us – in the direction, and taking it to the destination that we envisage, in other words, plan.


Bojana Vuksanovic
43. [The fragmenting world around us: On Randomness, Error, & Ego « IDEA:OKAY](http://katparr.wordpress.com/2009/03/01/the-fragmenting-world-around-us/)
3.26.09 / 7pm


[…] “We cannot be random in the same ways as the world fragmenting around us, but we can engage the randomness, not merely react to it, with our powers of spontaneity.” – Lebbeus Woods […]
45. [RANDOMNESS, ERROR, & EGO | The Narrative Beware](http://narrativebeware.wordpress.com/2009/03/01/the-fragmenting-world/)
10.17.10 / 6pm


[…] “We cannot be random in the same ways as the world fragmenting around us, but we can engage the randomness, not merely react to it, with our powers of spontaneity.” – Lebbeus Woods […]
