
---
title: REAL TIME
date: 2009-02-12 00:00:00 
tags: 
    - architecture
    - research
---

# REAL TIME



![hejduk1](media/hejduk1.jpg)


*Above: John Hejduk, BERLIN MASQUE (1983), architectural typologies*


Recently, a journalist writing an article on how the present economic situation might affect the practice of architecture interviewed me. His particular question was: “Now that building commissions were getting more scarce, will architects turn to making ‘paper architecture?'” By that term he meant speculative and theoretical projects that explore the possibilities of architecture outside the strictures imposed by clients, budgets, codes, and municipal building departments. The underlying question was, “With more time to reflect, will architects return to a more basic questioning of what architecture is and can be, even what it should be?”


My answer was: “No.”


Architects who have not already been thinking about the deeper nature of architecture and speculating about it in drawings, writings, and models, either within their building practices or in independent research, are simply not able to pick it up because they find themselves with spare time. As the old saying goes, “It ain't that easy.” To imagine that it is simply denigrates the field of architecture. Would someone ask the same of a physician: “Now that your patient load is diminished because of the escalating costs of health care and so many people losing their jobs and benefits, will you take up cancer research?”


Hardly. Research is a serious, lifelong vocation, not a sideline taken up when there is nothing better to do, then abandoned when there is. Perhaps the question speaks of a certain frivolousness architects have unwittingly promoted about what they do: creating new styles, new shapes, repackaging the old, the already known, but little else. If that is the case, architects cannot blame journalists, who are ‘the messengers,' but only themselves.


 LW



#architecture #research
 ## Comments 
1. [Vive la crise!… « shrapnel contemporary](http://shrapnelcontemporary.wordpress.com/2009/02/12/vive-la-crise/)
2.13.09 / 12am


[…] Not that I didn't welcome the crisis earlier. I did. I always thought crises not only provide an unexpected hair of the dog sort of effect, but they also have a weird capacity to redistribute a commodity that is normally very unevenly allocated to different peoples and geographies: time. Now we all have more time. (Even if that doesn't come to much more of anything.) […]
3. Pablo Garcia
2.13.09 / 12am


I wonder if economics do in fact play a role in architectural research for our youngest architects. Those about to graduate, those most vulnerable to economic downturn may choose to pursue their academic research leading to architectural innovation. Not because of default, but through transposition of academic paradigms into burgeoning “practices” that speculate, innovate, and draw from non-traditional sources. Those already inclined to research will craft their own version of “research practice” instead of following extant professional forms.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
2.13.09 / 3am


Pablo Garcia: Yes, you're right. It is up to the young, still idealistic, still courageous, generation to lift architecture up from its troubled sleep. May it endure, and triumph!
7. Jiri K.
2.14.09 / 1am


I think the crisis will definitely have some influence. As Rem Koolhaas writes about the architects and the crisis in the 1930th – the ones in NY “had time” to think. The problem of conjuncture is that buildings are being build like crazy without any try to reach something more than the dedline.  

We can see that in Dubai, East Europe or China. There ist just no time to use brain. Therefore I hope that the crisis bring some slow down and critical self-reflection of architects´ work.
9. [wazeone](http://www.wazeone.com)
2.14.09 / 6am


Architecture isn't asleep; it died long ago I am, as an about to be freed (or should I say, lost) graduate student who long ago devoted himself to the struggles and perils of our disciplinary survival as a professional-artistic body.
11. [htce](http://htcexperiments.org)
2.15.09 / 4pm


I agree with the post; and I also think to equate levels of cash flow with levels of research intensity is simply to be an economic determinist. And now a relevant but self-prom. link: <http://htcexperiments.org/2008/11/06/economies-of-architectural-thought/>
13. Firas
2.15.09 / 9pm


I would actually say that not only is architecture dead, but in fact, has completely ceased to exist. I think that the causality of the phrase ““Now that building commissions were getting more scarce, will architects turn to making ‘paper architecture?' is an invalid relationship. The only effect of building commissions getting scarce is that less projects will be realized.Full stop. It is as meaningless as its reciprocal, it is highly unfortunate that most architects, even those in training, are up to their foreheads with delusions about “architecture the savior” or the capacity for architecture to induce change or to affect environments, and proceed to tackle social and anthropological issues that have been dealt with by vastly superior minds. 


Architecture is as momentary as a stranger's face on a street, if not more so. 


Achievements exist in the confines of the architectural community, and so urban monuments remain to be simply forms in the city? Maybe a new architecture requires a new user, the “future human” so to speak, maybe architecture is the provoker, maybe not.
15. miran
2.15.09 / 10pm


it is as in all things. true awareness, creation and responsibility come from oneself.
17. river
2.16.09 / 8am


Real Time is unfrozen music. Architects need to practice their scales on vibrating string instruments.
19. chelko
2.17.09 / 6pm


This post addresses a fundamental struggle between theory and practice in architecture. What is becoming exposed is the assumption that one individual can achieve in both realms. The architect as a singular force is an image that will soon die. The theorist however remains with the writers, watching and alone and it is there were architecture waits.
21. harmann
2.23.09 / 4pm


The city scape as we build will continue to effect us, singular buildings much less, as we move on to experiences, sharing,giving away the walls will come down figuratively and literally. I believe architecture had much more influence than a stranger's face but that time is past. Its easy to relive it in buildings, have felt it in temples often, old cities everywhere, the walls the arches doors and windows shape the way we look out, the sense of security we feel, but today as we live connecting to things, people, views walls become less important.
23. [Arthur McGoey](http://amcgoey.net/)
3.2.09 / 2am


It seems to me that there is an argument for an increase in research during this economic crisis. Though I agree with the premise that architectural research requires dedication and should considered a separate form of architectural practice from the many other forms that require years of experience to pursue, that does not mean that those looking for something different to do now that the commissions are scarce won't pursue more research. As Lebbeus implies, it won't be a casual decision, but one that requires careful thought and determined action to pursue, just like any other major change in one's career. For that matter many architect's will probably change careers entirely largely due to the pressures of the economic crisis. The decisions are very similar. The journalist was defining the problem wrong. Instead of asking whether there would be an increase in “paper architecture,” the real question is what kind of innovations, both in the design & construction of structures and in research that the changes caused by economic crisis might encourage. Changes to people's lives such as these can be the engine for change in architecture.
25. [Caleb Crawford](http://www.formpoetrypolitics.blogspot.com)
8.27.10 / 2pm


Amen.  

I actually found myself most recently the most deeply into speculative drawing when I was the most busy with my practice. There is a reason we call architecture a “discipline.” To do things that don't have an immediate pragmatic return takes immense discipline. It doesn't come easy, and it is immensely difficult to make it a habit.
