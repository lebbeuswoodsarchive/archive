
---
title: FLUID SPACE
date: 2009-06-28 00:00:00
---

# FLUID SPACE


![LWblog-FLUID16](media/LWblog-FLUID16.jpg)


Above: A performance by Yves Klein, the inventor of “Air Architecture.”


The surface space of this planet is filled with fluids—air and water—which we humans have adapted to by both natural and technological means. We not only live within veritable oceans of these fluids, with their ebbs and flows, their flux and interactions, but have become entirely dependent upon them. Their lighter density relative to that of our own bodies, allows us to move about with relative ease. We can walk, ride, fly, float, swim, sail, and dive, for the most part safely and increasingly with speed, even while the fluids themselves continuously swirl around us according to their own necessity.


Not only is our physical existence intertwined with what we might call the life of the fluids we inhabit, but our mental existence as well. It is telling that people will often begin a conversation by talking about the weather—how hot it is, how cold, or the rain or the snow or the coming storm. While this may be a way of keeping an emotional distance, it can also be understood as a common concern for the fluid space we share with all others, whoever they might be. This concern is also about our ability to move effectively, to travel, to get work and other important tasks accomplished. It is not mere coincidence that the arts and literature spend a fair amount of time concerned with human movement and its consequences, whether in the historical past or an imagined future, and the states of the fluids which enable it all. In this regard, architecture, among the plastic arts, stands apart.


Architecture, traditionally, is the anti-fluid, or rather it is a primary form of resistance to the flux and flow of air and water, creating fixed points in their turbulence. In a similar way, architecture has always aimed at providing a refuge—‘shelter from the storm'—from a sea of changes continuously occurring in the intertwined human and natural worlds.


Exceptions include the tent structures of nomadic people, recreational vehicles, automobiles and mobile homes, but these are rarely considered architecture, precisely because they do not fit the standard model of resistance to the essential fluidity of space, but instead celebrate it. Science fiction has proposed the occasional flying city, as did the Russian Constructivists when they speculated about the brave new worlds of the Socialist adventure. Archigram's Ron Herron proposed ‘walking cities,' and Michael Webb designed “suitaloons”—clothing that becomes a personal-scale living environment—but these have all been ignored by architects and even theorists as too far from architecture's essential mission: creating resistance to the natural flows of space.


There are, however, some benefits to be gained by speculating about architecture that moves. Such architecture does not simply have moving parts—from doors to moving sun-screens and the like—but wholly moves from place to place. The main idea at stake is that of social stability. If everything is moving all the time, and not even as predictably as the weather, how can a coherent, cohesive community of people to formed and maintained? It is fine if we have some wandering vacationers, itinerant poets, migrant workers, and (for most) not so fine if we have roving bands of political insurgents and rebels, but in any event these nomads and their various modes of portable environment cannot serve as models for a viable human community. Or can they?


If we think about the quickly changing nature of contemporary society, its increasing mobility and dependence on ephemeral electronic systems of communication as the connection between diverse people and places, it is useful to understand the spatial morphologies of ‘lateral,' non-hierarchical systems of order. The internet is such a system, a network of constantly shifting centers occupied by individuals who come and go unpredictably. It does not work according to an overall, hierarchical ‘chain of command,' but rather by the autonomous, uncoordinated actions of individuals following an agreed upon (or at least universally accepted) set of relatively simple largely technical rules or protocols. Beyond these, the overall form of the network is indeterminate, changing from moment to moment. Still, it works as a coherent whole, indeed as a community of people who do not otherwise know each other, much like a city. One architectural counterpart is, then, a community of autonomous structures moving freely in fluid space.


 


ICEBERGS: a community of inhabited structures floating outside the San Diego harbor. Much of their form—and interior space—is beneath the surface of the sea:


![LWblog-FLUID1](media/LWblog-FLUID1.jpg)


![LWblog-FLUID5](media/LWblog-FLUID5.jpg)


![LWblog-FLUID2](media/LWblog-FLUID2.jpg)


![LWblog-FLUID4](media/LWblog-FLUID4.jpg)


![LWblog-FLUID3](media/LWblog-FLUID3.jpg)


 


AEROLIVING LABS: A community of heavier-than-air, experimentally inhabited structures in the air over Paris. The moving structures drag immense sheets of light-weight material through the earth's magnetosphere, generating static electricity in sufficient quantities to levitate them silently, much like a mag-lev train, but freely in the open sky.


![LWblog-FLUID11](media/LWblog-FLUID11.jpg)


![LWblog-FLUID13](media/LWblog-FLUID13.jpg)


![LWblog-FLUID12](media/LWblog-FLUID12.jpg)


![LWblog-FLUID10](media/LWblog-FLUID10.jpg)


LW



 ## Comments 
1. [m(odm)](http://www.markodomstudio.com)
7.2.09 / 3am


What an interesting concept; how to define the fluidity of space based on the social realms of our internet mindset. Would it contain “non-hierarchical systems of order” or would it be a random mix of interest floating about creating a hierarchy of common interest; the fluidity of space could morph based on the governing consensus. Individuals would have the freedom to move freely through each zone, sometimes multiple zones at once, as interests morphed; interesting…Thanks for posing the question and blurring the lines.
3. martin
7.2.09 / 7pm


what really fascinates me about this type of concept is that while you touch on the space that we occupy (air/water), the construction techniques we use in our ‘stable' architecture are truly far from solid. floating foundations, caulk and sealant, mortar, concrete (!), weep holes (!!), vapor barriers, glass (liquid or solid?), warp and weft of wood, expansion joints (!), lateral deflection in skyscrapers, etc etc etc. 


what an illusory world we are really living in. 


calrification by way of analogy: the Endosymbiotic Theory. This basically states that mitochondria were at some point in our evolutionary development, separate organisms that transcribed themselves to our DNA and never let go. Which essentially makes us human shaped beehives for billions of individual organisms.


i would be curious to know what mr. de landa thinks about this, considering his discussion of the changes of states of matter in ‘a thousand years of non-linear history.'





	1. manuel delanda
	7.3.09 / 5pm
	
	
	mr. delanda here. Three ideas come to mind when thinking about the property of “solidity”, one having to do with time scales, the other with spatial structure,and the final one with the difference between properties and capacities. The first is illustrated by two classes of solids, crystals and glasses. Unlike the crystals glasses do not have a sharp phase transition and may be viewed as “arrested liquids”. That is, they are solid only at our time scale but liquid at longer ones, as can be seen in some old church glass windows in which a thickening at the bottom shows that the glass has flowed down over many centuries. The second is illustrated by sponges and solid foams which, unlike dense solids, are full of empty spaces giving them special properties, such as a much higher surface to bulk ratio. Considering that the physics and chemistry of surfaces is different from that of the bulk material (fractures for instance are pure surface entities) this makes for interesting effects. Finally, the property of “solidity” should always be discussed in relation to the capacities of a solid body to affect and be affected by other bodies. A solid metal object like a sword or knife needs to have the capacity to hold on to a shape (so the triangular cross-section of its edge is maintained over time) but its body must be capable of yielding without breaking, to carry the loads produced by, say, hand to hand combat. The rigidity of the edge must be accompanied by the toughness of the sword's body. Thus, solidity is a complex property once we add time, space, and affects.
5. [bdpog](http://bdpoq.wordpress.com)
7.2.09 / 10pm


my comment is about the picture of Yves' jump into the void…what I like in this is the fact that one who has managed to climb high and feel fresh air and nice view decides to dive into uncertainty and pain… what I want to point to here is the fact that this inspiring blog could be a formal letter to the U,S government, giving them some fluid dynamic as to how repair/share their productivity in ways that help all environment to share/create elsewhere than venus/jupiter or whatever they spend their time on and postpone this hilarious universal conquering of reality they are after… some people think seriously on how to build a cheap wind turbine, and that's not easy….
7. river
7.4.09 / 7am


sometimes, silence is the only compliment to sublimity, how to offer it over the internets without breaking it? grr.
9. thais
7.5.09 / 2pm


I had a similar perception of the fluidity of old windows, until one day when the truth was revealed…  

Whether or not, I still have the same perception.  

Glass was spun around, making the centrifugal force pull the fluid glass towards eternity. This leaves the core, at any time, more dense, than the outer point. The circular solid was then cut into squares, exposing the making/behavior of the material.


Still i believe in the fluidity of most, especially glass.
11. Josh V
7.7.09 / 3pm


When I began reading, I instantly thought of the high-speed trains and how they have become mobile hotels and offices complete with living quarters, restaurants, internet & phone access, and even meeting rooms. Occupiable living and working space as you travel from one distant location to another. Granted, not at the whim of nature, but indeed in motion.


Then, as I read on and analyzed your Iceberg sketches I was reminded of an article I read about a man off the coast of Mexico who built his own island on thousands of water bottles complete with his house and beaches… <http://tinyurl.com/27rfd5>  

And a quick google search brought me to the fantastic concept behind Project Habakkuk in Britain. Look it up.  

<http://tinyurl.com/hf9ew>


The flying structures are interesting, but much more fantastic than realistic. Regardless, your sketches are as always beautiful and inspiring.
13. river
7.11.09 / 9am


I have a question. Is this totally bold brand-new stuff? or older sketchbooks posted? i.e. when did you make that? Where were you then? Were you in New York City, or [Kosovo](https://lebbeuswoods.wordpress.com/2008/02/06/the-reality-of-theory/)? I totally understand that it is all intertwined and inseparable. But what was the astrologically speaking, exact date-time of them?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.11.09 / 4pm
	
	
	river: Aerial Paris was made in 1989. Icebergs dated from 1993. I usually don't post dates because it puts the work in a too-easy category, e.g., old stuff. Hopefully it still resonates with the present. However, because you asked….
15. river
7.13.09 / 11pm


Thanks. “Was-will-be” is actually my favorite verb.
17. student
12.24.09 / 11am


I really like the whole concept of fluidity, air, water, and I cannot but think of it in the context of flight. Similarily, if we want to acknowledge it in the architectural realms, we might think of it as a flying structure on the verge of collapsing, not stable, denying the everlasting gravity effect and the bitter reality of it being destroyed by the Newtonian facts. However, I pause and ask myself whether such a vision can actually echo the human desire of flying; this is what will happen if one tries to jump off a cliff…But after all, this is what will elevate us to whole new level of perception. It is this feeling of letting go that produces such a Rush. Then my question to you would be, how could we, as architects, tie all of these bits and pieces to produce flying/dangerous architecture?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.24.09 / 3pm
	
	
	student: One answer to your question is the ‘aero-living labs' of the Aerial Paris project, shown in this post.
19. student
1.12.10 / 11am


What if the ‘aero-living labs' are not the ‘futuristic' flying machines floating in space over Paris City, then what would they be? How could they exist in the fluid space? But then the most principle question would be, should flying architecture be always a machine in motion or can it be perceived as static architecture in space that is still be acting dynamically to weather conditions,perhaps wind or other factors?
21. [material, immaterial, virtual [1] « Blog Archive « Daytime Astronomy](http://daytimeastronomy.wordpress.com/2010/03/10/material-immaterial-virtual/)
4.21.10 / 7am


[…] This reminds me of the Air Architecture work of Yves Klein. Making architecture immaterial, but was it purely about the creation of atmospheres? Read about it on Lebbeus Woods' blog here. […]
23. [Sketchbook Three « The Architect (Almost)](http://architectalmost.wordpress.com/2010/05/05/sketchbook-three/)
5.5.10 / 12am


[…] is an inside joke about Professor Mooney. We were studying the work of Lebbeus Woods, and Mooney had commented about how awesome it would be to move along so freely in the air (the […]
