
---
title: ARCHITECTURE OF ENERGY
date: 2009-06-05 00:00:00 
tags: 
    - Christoph_a__Kumpusch
---

# ARCHITECTURE OF ENERGY


![LWblog-SWIEN2](media/LWblog-SWIEN2.jpg)


**Any city is comprised of many systems—economic, technological, social, cultural—which overlay and interact with one another in complex ways. Each system is different, but from one point of view all share a common purpose–the organization of energy—and a common goal—giving the cumulative energy of the city a coherent form.**


**According to Maxwell's second law of thermodynamics, the entropy in a system will increase (it will lose energy) unless new energy is put in.**


**According to Newton's law of inertia, a system will stay at rest unless it is disturbed by an external force.**


**Energy exists in two states: kinetic and potential. A brick sits on top of a wall—potential (it could fall). A brick is pushed from the top of the wall—kinetic (its potential is released).**


**Energy takes many forms, each created by a system that contains it for a particular purpose. Architecture is one such system that contains energy by establishing stable boundaries, limits, edges. New energy—in the form of maintenance—must continually be added to the system of materials, or they will decay. Metaphysically speaking, new energy—in the form of human thought, emotion, activity—must continually be added to the system of boundaries, or they will lose their purpose and meaning.**


The group of drawings, prints, models, and installations comprising the *System Wien* project develop an idea that the making of architecture can be understood as *the organization of energy*. The project explores how energy relations in public and private city spaces might be represented tectonically in the form of drawings and models; how existing energy relations in the city can be changed by the input of new energy in the form of highly temporary spatial interventions; how the future of the city need not depend for creative energy input on the development of building projects requiring large capital investments and institutional approval, but rather on the redistribution of energy at the human scale of the street and the room.


A portion of a city's potential energy is contained in its architectural constructions. A vast quantity of mechanical energy is stored in the materials used to make buildings, the energy it took to lift them into place above the surface of the earth and its center of gravity. The distance between the materials and the earth's surface determines the quantity of their potential energy, which can only be released when, responding to the gravitational attraction of the earth's mass, they fall.


The larger part of the city's potential energy is contained in its people. Each person has energy that can be activated, in thoughts and in actions, anytime in the future. In theory, just how and when this energy will be released and where it will be directed is unpredictable. Governments, businesses, and other institutions organizing the collective energy of a people, work hard to ensure that the energy of individuals will conform to accepted standards, and maintain a predictable continuity. In the fast-changing contemporary world, with its radical technological developments, political and economic crises, abrupt cultural shifts, institutions often become more protective of the status quo, more resistant to changes in the established systems of organization. Like the build-up of energy in the earth's tectonic plates, institutional resistance to change inevitably brings sudden and violent releases of energy, such as revolution and war. At the same time, institutional exploitation of the fear of violent change will polarize people of different opinions and beliefs. Energy that might otherwise be directed to collectively constructive projects is spent in attacking adversaries, a project that, while usually bent on destruction, is nevertheless part of the processes of creative change.


It is difficult to determine in advance the magnitude of any human being's energy, potential or kinetic. We can know the quantity of energy it takes to walk a given distance, but what is the energy of a person who assassinates a world leader? Do we measure it by cause or effect? Or the person who invents a new technology? Or the person who designs a housing project? Or the person who devotes themselves to helping the poor? The energy of such persons' thoughts and actions cannot be measured quantitatively or qualitatively by applying any known formula. Their input is nominal—they think, move, speak in more or less normal ways—but the effects are vast. If we measure their input in nominal terms, it is not radically different from that of ordinary people, going about their daily lives. The where, when, and how of the energy of such persons' acts have much to do with their effects. But that cannot be all. If it were, then any persons thoughts and words and actions would sooner or later—under the right circumstances–produce vast effects. But they do not. The energy of persons who ‘change the world'—even the local world—must be extraordinary in ways that can be identified and measured. How, then, can that be done? How can it be represented in any analysis of the present and future organization of a city's energy? And, more importantly for our purposes here, how does the extraordinary inform the ordinary? How might we think of and portray the everyday differently as a result of knowing that at any human point it might become extraordinary?


If we could answer this question, we could immediately begin the establishment of a social and political order that recognizes the fact that any human life can and in some cases will, change the course of human events, at a larger than a personal scale. Steps have been made in that direction, in the form of legal and political instruments that guarantee fundamental human rights, including the right to be educated, the right to be employed within the institutional structure, in order that any person's potential energies—physical, intellectual, moral, philosophical—can be applied to human conditions at the right place and time. It has taken some many thousands of years for these first steps to be made, so we might imagine it will take considerably more time for steps that take us beyond the institutional, to the direct application of human energies, without institutional mediation, foreseen by utopians of various kinds. Meanwhile, life goes on, struggling under imperfect conditions and circumstances. The city, the *polis*, is struggling to grow, and to change, perhaps even toward that day when the idea of the human is recognized in the energy, the life impulse and actions of each human being.


The ethical implications of such a ‘toward' are enormous. If human society were to value each human life equally, then what about the murderers, thieves, pedophiles? How about the small-time cheaters, the dullards, the bigots and the bores? Are they to be given the same value as the bright, the tolerant, the compassionate, the innovative and the persons of ‘genius,' who bestow gifts of wide value to the human world? The answer given by utopians-is ‘no,' because the human world will consist only of the latter and not at all of the former. All the disgusting and disagreeable human behaviors will have been eliminated by universal education and opportunity. But, as critic Aldous Huxley asks in his “Brave New World,” what if the perfect society is—against all odds and predictions–disturbed by savagery, that is, by atavistic urges and actions? It could be in the form of incest, or the belief in magic, or in rituals of racial superiority leading to genocide, or, in “my right to do whatever I want, regardless of what anyone else thinks.” The very presence of these possibilities, indeed these potentialities that only wait to be released, brings us back to square one. Human perfectibility, we are forced to acknowledge, in either social or individual terms, cannot be defined by any sort fixed or universal standard of rectitude.


If that is the case, and history bristles with examples—governments of the left and the right that tried to impose universal standards of thought and behavior—then some new and better system for the organization of human energy waits to be devised. The premise of the *System Wien* project is that the social, political, cultural, and ethical can be formulated in spatial terms, as indeed they already are. It opens a speculation on the possibility that this formulation can facilitate change more creatively—at once more wisely and spontaneously—than existing spatial systems allow.


**“What exactly do the vectors represent?”**


**“They don't represent anything. They are just themselves—embodied energy.”**


**“They contain energy?”**


**“Yes. Can't you see it?”**


**“I see white lines on a black surface.”**


**“Tell me, what do you see when you look at that building? Bricks, windows, metal, glass?”**


**“Yes.”**


**“That's all?”**


**“Yes.”**


**“Ah, then that's the problem. You can't see energy, just its effects.”**


**……..**


**“The vectors contain the energy that it took to make them. It is a measurable amount of energy, but it has not yet been measured. It consists of physical energy, intellectual and emotional energy. Certainly we will be able to measure it by its effects, if and when there are any.”**


**“So, then, the vectors are a form of energy?”**


**“Yes, that is what they are.”**


**“Well, there's nothing new there. Any drawing, any word or act has the potential to have an effect. All you're doing is seeing it differently.”**


**“Exactly!”**


Architecture, we hope, is first of all a field of knowledge, and only then of action. Our hope is rooted in the judgement that actions are most constructive when informed by an idea that fits into a larger understanding of ourselves and the world. When we design and build, we demand that they embody such an idea of human experience and how it is enabled by the conception, design, and construction of space. Our existing knowledge is important, because it is the structure of what is already here. Architecture, like other fields, reveals the structure of the familiar. It remains only for us to see this structure as though it has not been seen before, freshly, as though for the first time. This is, I believe, the task of architects.


It is true that most architects will continue to be kept busy designing buildings and spaces serving existing interests and points of view. It is entirely proper that there will be only a few who have the inclination, or feel the necessity, to invent new points of view, or who are willing to accept the risk, if not the probability, that their ideas will come to nothing. The truth is that in the day-to-day practices of architects building and rebuilding the familiar, a substantial part of the invention of new viewpoints will emerge, one building at a time. We might call the cumulative effect of these small steps a social-historical process, one that moves ahead inexorably but surely, because it is intimately connected to everyday conditions and their own subtle and incremental transformations. Still, some leaps are needed, if architecture is to not only keep up with today's accelerated changes, but to get enough ahead to help lead them.


It is crucial that we invent strategies for seeing the familiar differently. If we rely solely on seeing it in familiar ways, we will only be able to re-enact what we have already done and confirm what we already know. As changes occur to the familiar systems, either as a result of entropy or disturbances from outside forces, we will be poorly prepared with entrenched attitudes to control their transformations, the ways the energies they contain are released and to what ends they are employed. In order to adapt creatively to changing conditions, we must adapt our existing knowledge and skills. Accustomed though we might be to finding a new pill or product to solve critical problems, we cannot count on new knowledge alone to save us from becoming relics of our own history.


**The first task of experimental works of architecture and art is to stake out new points of view on what already exists. The second task is to test them.**



Street in Vienna's First District:


![LWblog-SWpic1](media/LWblog-SWpic1.jpg)


Visualization of human movement in the First District, in terms of vectors:


![LWblog-SWIEN6](media/LWblog-SWIEN6.jpg)



*[System Wien](http://www.amazon.com/Lebbeus-Woods-System-Manuel-DeLanda/dp/3775716645/ref=sr_1_1?ie=UTF8&s=books&qid=1244201917&sr=8-1)—*a project made in collaboration with Christoph Kumpusch*—*begins with the existing system of spatial organization, as embodied in Vienna's buildings, streets, open spaces, and how they are presently inhabited. It sees them not as organized matter, but rather as organized energy. The visual language through which this understanding is expressed is one comprised of lines—constructed in two, three, and four dimensions–which we will call *vectors*.


Vectors are mathematical symbols for expressing the direction and magnitude of forces active within or upon a system. In this project, the vector is expanded in meaning and application. It still retains its expressive function, only now including not only magnitude and direction of mechanical forces, but also the intensity and extensity of cognitive and affective forces both active and latent in the city. There is another aspect of the role vectors play in this project and in its projection of the present and future energy patterns of the city. The vectors not only express energy, *they embody energy*. Like other constructive elements used to build the city, they are elements of a system organizing the mechanical, cognitive and affective energy it took to make them, palpable energy that remains potential in their residual forms. If we can see vectors as forms of potential and kinetic energy, then we can see buildings that way, too, and the city itself. If we can see these things not simply as objects, but as embodied energy, then we can see ourselves and others not as material objects, but as living systems interacting continuously with other systems, both animate and inanimate.


This is a new way of seeing the familiar, at least for architects. Ecologists and theorists from fields as diverse as cybernetics and the life sciences have adopted similar points of view long ago. Architects retain a mechanical, materialist worldview, no doubt in part because of the nature of the people they work for, their clients, who see architecture as a product that relates only incidentally to other products, designed and paid for by others. The boundaries of a product are rigid, fitting into clients' ideas of property, and consumers' ideas of buying and owning. Systems, on the other hand, have flexible, often porous or fluid boundaries, depending on their interactions with other systems.


The energy-systems view of the city and its life have strong political implications, in particular, regarding prevailing ideas of identity and, its corollary, property. Individuals, in such a view, are identified not so much by what they ‘own' or who they ‘are,' according the social roles they play, but by what they ‘do,' how they interact with others, including the inanimate systems in their environment. In the same way, buildings, public spaces, and other forms of property can no longer be identified according to building types set by pre-determined economic and functional categories, but by how they perform in a landscape shaped by complex interactions. What architects do with their own initiatives or those of others seriously impacts networks of interacting human and other energy flows, as well as the energies latent in the city. Their ways of thinking and working need to integrate this reality more than they do at present. In doing so, the role of the architect will be transformed into a more expansive and more complex one in the evolution of the urban landscape. The identity of architects—like that for whom they design—will be based on the depth of their mastery of particular skills and knowledge, but at the same time, on their agility in engaging an urban field of continually changing conditions. *System Wien* explores what the production of such architects might be.


LW


One of the aluminum tube vector rods:


![LWblog-SWIEN5](media/LWblog-SWIEN5.jpg)


The vector rod placement team in July, going into the First District:


![LWblog-SWIEN7](media/LWblog-SWIEN7.jpg)


One of several drawings used as guides to the team for placing the vector rods in particular city spaces:


![LWblog-SWIEN4](media/LWblog-SWIEN4.jpg)


Vector rods placed for an hour or so in several of many cty spaces:


![LWblog-SWIEN8](media/LWblog-SWIEN8.jpg)


![LWblog-SWIEN9](media/LWblog-SWIEN9.jpg)


![LWblog-SWIEN1](media/LWblog-SWIEN1.jpg)


An early October return to the vector rod repository:


![LWblog-SWIEN15](media/LWblog-SWIEN15.jpg)


The museum gallery (open to the public) is the repository of the vector rods between interventions in the city:


![LWblog-SWIEN13](media/LWblog-SWIEN13.jpg)


![LWblog-SWIEN14](media/LWblog-SWIEN14.jpg)


#Christoph_a__Kumpusch
 ## Comments 
1. mchart929
6.5.09 / 11pm


Fascinating explorations. It seems to me that the act of laying the rods down also has potential as a performance – the shots of the placement team carrying their rods in the air has a certain striking visual effect, and I assume the fact that they are all wearing black was a decision calculated beforehand. Perhaps my next thought is somewhat of a tangent, but it would be interesting to see what sort of spacial constructions could be made out of the literal movement of people. With the right props, people moving in a line could appear to be a moving wall. Perhaps with the development of robotic technology buildings could be designed to dynamically respond to the movement of the people around and inside them and the changes in energy due to varying patterns of use throughout the day.  

It would be also interesting to observe and analyze the reactions of those who witnessed the placement team and the installation – the creation of the installation intended to reflect the city's energy flow would also go on to affect the energy flow of the city itself due to people's responses to it.
3. [GENERAL](http://general.la)
6.7.09 / 7pm


Human Electromagnetic Generators Whose Reflections Cast the Shadows of Architecture in Quantum Space. Humans are to Architecture /as/ ‘Minds' Bio-Generative Foci are to the HumanBody's Electromagnetic Meridian Grid .  

((As humans consciously evolve, our reflective statement of the built environment may more closely resemble our true nature))  

THE LAST ATLANTIANS
5. Gulliver
6.8.09 / 3am


Nice images to drive home your point, Lebbeus. Out of curiosity, how do you manage to draw perfectly straight jagged lines? Is it from having a b.a.c. of 0.75?


BTW, this is not architecture.  

Just call it art, and call it a day.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.8.09 / 2pm
	
	
	Gulliver: BTW, why is it important for you to give this work a label? What is the relevance of the concepts for architecture as we know it and as it might become?
7. Josh V
6.8.09 / 8pm


At the conclusion of these experiments, how do you assess the work? How do you deam it “good” or “successful”? You do a good job explaining what you intend, but don't explain whether this experiment was successful or not. And if it was, how was it successful? I've seen some of your other work similar to this of the past few years and this article helped me understand it, but what is different about this from the others? If it is simply another attempt at the same idea, how have you refined it?
9. [lebbeuswoods](http://www.lebbeuswoods.net)
6.8.09 / 9pm


Josh V: I am grateful for your questions, even though they are difficult to answer. But that's as it should be—the difficult ones are the best.


The primary way I would measure the success of these experiments is their impact on people's thinking. If there is little or no impact, then I would consider them failures, though I am aware that it takes time for new ideas to sink in and have any effect. If one or two projects by serious architects or artists take up the ideas or the ways we worked and pushes them further, then I would call the project a success. Or, if it would change at all—even in a small way—how people think and speak about Vienna, or any city, or architecture, then I would consider these experiments a success.


In the case of System Wien, I would have to say that the failure or success in these terms has yet to be determined—it is still to early to say. I hope you'll forgive the grandiose comparison, but it took nearly twenty years for the Michelson-Morley experiment in measuring the speed of light to reach fruition in Einstein's Special Theory of Relativity. If some future Einstein of architecture were to find similar support for his or her ideas in the System Wien project, I would indeed consider the project a resounding success!


Or perhaps it will come to nothing. That's the risk one takes in the pursuit of a previously untested idea.


On the other hand, whether the project has any impact at all, it did crystallize one very particular moment in time, and left a trace—and that is worth something.


Referring to your second question, this project (I like your term 'experiments') is concerned with the central idea of all of my work from the beginning: the ways we and the world change and how architecture is not only affected by this change, but how architecture—the design of space that embodies specific ideas—can participate in change, for the better.





	1. Josh V
	6.9.09 / 12am
	
	
	Well, I think that's how any architect could view a work as truly successful, but you could put up any old crap and shrug as to its relevancy because you may never see it (I don't mean to insinuate your work is crap). Surely you have some sort of personal criteria for the success of your projects. I mean, I don't know if you do these for a commission or have deadlines, but at what point do you put down your pencil and decide it's finished? And at that point are you fully satisfied? Are you fully satisfied with System Wien? Or do you shrug and say “We'll see what happens with this one” and move on to another city?
	
	
	I apologize if I'm oversimplifying what you do, but I'm very curious about how you view projects differently from one another. I don't believe it's simply the same idea applied to a different location. Would you have designed this project the same had you done it 5 years ago? If not, what's changed since then? What have you learned to influence that change? If you do not learn something from your own projects, what is the point of doing them?
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		6.9.09 / 2pm
		
		
		Josh V: I've answered your questions as well as I can. Where you want me to go is into my personal feelings about the work, but this is not the appropriate place for that kind of exploration. 
		
		
		As for your comment about shrugging off the relevancy of the work, I can only say that you've grossly misunderstood what I've said. 
		
		
		As for your questions about how this type of project gets commissioned, I can say that such commissions are rare, though I wish there were more. In the case of System Wien, it was made at the invitation of Peter Noever, director of the Museum for Applied Art (MAK) in Vienna, in 2005.
11. Josh V
6.9.09 / 4pm


I apologize if I've insulted or misunderstood you, I'm just interested in your personal views about your own work. If that's too personal, I won't pry any further. But if you like referring to these as “experiments” then for a true experimentat you've left out a vital part: the conclusion! You've very clearly explained your theory and execution and end it there. Would Einstein have achieved his Special Theory of Reletivity had the Michelson-Morley experiment published only their work and not their findings and conclusions? Possibly, but isn't that silly?


I did not experience System Wien first-hand, so I can't determine for myself whether this project was successful or not. I have to rely on your conclusions or the conclusions of others who did experience it, or I have to take faith in your theory and guess or assume the outcome of the execution based on stimulating images and words (which is never enough with architecture). If this is not the appropriate place for that kind of exploration, then please post it elsewhere. Or post that of another project from theory to execution to conclusion. I value your feelings about architecture in general, it's emotional and not cliche like so many other architects I reluctantly read about. I would love more so to hear your feelings about your own work than that of others'. I am intrigued by theory, but theory does not inspire me the way emotion does.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.9.09 / 8pm
	
	
	Josh V: There's a relevant Wiki entry on [Michelson-Morley](http://en.wikipedia.org/wiki/Michelson-Morley_experiment) that shows the difficulty of drawing conclusions from a single experiment.
	
	
	Still, it is true that my work is not searching for scientific answers, but rather for useful concepts.
	
	
	From a personal point of view, I view the project as a success, largely because it brought a number of creative people together and resulted in events and forms we had not ‘seen' before, and which are rich with implications for future work.
13. [Link love for June 18th — Sympathy for the Robots](http://www.sympathyfortherobots.com/2009/06/18/link-love-for-june-12th/)
6.18.09 / 5pm


[…] Architecture of energy, by Lebbeus Woods […]
15. ian
6.25.09 / 5pm


Recently I have been asking myself how is knowledge created? Where is meaning generated? What is the nature of representation? and how do we explore the present?


(here we can begin to consider spaces of representation)  

For the drawing it is the sheet of paper. (a surface)  

For the installation it is the gallery. (a volume)  

For the expansion of the installation it is the streets, the city, which is not really the same idea of a space of representation as the surface of the paper or the volume of the gallery.  

Through each translation of the project the space of representation becomes more complex.  

The space of the drawing (the surface of the paper) is a space of representation. It occupies space (it has a mass and a volume), delineates space (it has a front), and then also represents space (employing the rules of perspective and atmosphere). The vectors in the drawing are then said to represent energy, something that must be conceived of when one perceives a material.  

The volume of the gallery is a space for representation. It exists first as a three dimensional volume in which your installation created numerous other dimensions (one of which is time, in terms of geometrical or Euclidean space)


The space of the drawing as well as the space of the gallery are both resolved. By this I mean, the work of each is built in their respective spaces of representation. Meaning is derived in the design of each from the relationships built between the meanings of the elements (vectors in this case) to the space of representation. The drawing addresses the edges of the paper and the installation addresses the volume of the gallery.


When the vectors become poles in the street something is missing. I will say this has much to do with gravity, scale and the horizon. 


The space of representation of the drawing is less complex (exists in fewer dimensions) than the space of representation of the gallery, which is less complex than trying to use the streets as a space for representation. The interesting thing is that the space of the streets has the potential to require fewer systems of notation. What is the system boundary of the project in the streets. In the drawing, the system boundary is the space of the paper. The installation has the boundary of the gallery. A person in the streets has gravity (which determines their orientation, the vertical line), scale, and their conception of the horizon.


In the AS401 the system boundary for the project is the steel cube. In another student installation constructed at Cooper Union the system boundary was a series of arcs that defined a volume. How does one conceive of a greater system boundary in the space of the streets? It is the horizon. As far as impact of individuals, I believe it is those who engage in the extent of their gaze and the length of their shadow who begin to initiate change.


The vectors are aggregated into assemblages beginning at a small scale, a bottom up approach, but at he same time there could be a top down component, beginning with the large scale so that a full spectrum of scales can be achieved. I believe addressing the horizon will allow for the generation of the large-scale logic.


There is often a difference between what we can perceive and what we must conceive of. The notion of the horizon emerges between these differences. Devices allow us to extend our perception to an understanding of a greater whole. . What can be added to this project is the idea of inertial reference frames (the principle of Galilean Relativity).


The idea that the vectors not only express energy but also embody them is a great realization. 


The further development of this project has the potential to begin to describe much of what is debated in the series of previous postings. To quickly reference the post “open questions”; if we concern ourselves with studying space then the question of what qualifies as architecture becomes a bit less significant. We are not searching for what is good or bad, successful or not, or to make a definite distinction between architecture and art. Instead by asking how is knowledge created? Where is meaning generated? What is the nature of representation? And where does belief emerge? we can begin to reveal the substance of architecture and art.
17. [Arch. of Energy | AMNP](http://architecture.myninjaplease.com/?p=4755)
7.3.09 / 4am


[…] Woods writes on the ‘Architecture of Energy‘. Share this […]
19. [System Wien – Architecture of Energy « Republish](http://www.republish.eu/art/system-wien-architecture-of-energy/)
7.5.09 / 9pm


[…] Project by Lebbeus Woods and Christoph Kampusch that is quite a read on his site. A site which i may strongly recommend among architecture blogs, that offers written pieces (yes my […]
21. Napolexander Mina
7.6.09 / 10am


Interesting discussion.


However one calls it, architecture is ultimately a medium through which culture (the organizations, the artifacts, the rituals, the songs, the aspirations, the ideas, and yes the energy), is allowed to transpire. And evolve.
23. [Igor](http://tecno-macchine.it/id-6-level-0-presse_piegatrici.html)
9.17.09 / 4pm


WOW, really amazing! Is like architecture is alive and walks around by herself… Really great!
25. [Design Culture » Digital Analog](http://www.haynesarch.com/blog/2010/04/06/digital-analog/)
4.6.10 / 4am


[…] I'm reminded of the System Wien/”Architecture of Energy” project by Lebbeus Woods and Christoph Kumpusch, which translates a series of seemingly […]
27. [certificazione energetica](http://www.qualificazioneenergetica.it)
9.2.10 / 2pm


does exist an italian version of the book? please let me know…





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.2.10 / 3pm
	
	
	certificazione energetica: No, I am sorry to say. Only in English.
29. [GESAMTKUNSTWERK « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/09/07/gesamtkunstwerk-2/)
9.7.10 / 11am


[…] all tendencies and trends. Vestiges of this have remained throughout the succeeding years—in System Wien, for example—but never in such an ambitious and hopeful a form as these drawings. Auf […]
31. [Carlos Alberto León González](http://www.facebook.com/CarlosARQdesign)
3.5.12 / 3am


Hi there Im studying architecture and I thinks this is very educative I'm going to read and discuss more about this interesting topic!.I sure do appreciate all you smart people taking the time to set up these types of forums! … If you want to post more information links I'll appreciate it too. THANKS!
