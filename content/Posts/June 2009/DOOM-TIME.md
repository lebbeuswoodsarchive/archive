
---
title: DOOM TIME
date: 2009-06-08 00:00:00
---

# DOOM TIME


![LWblog-DOOM3](media/LWblog-DOOM3.jpg)


(photo of World Trade Center tower; photographer not yet known.)


Flashed around the world in September 2001, the pictures of the World Trade Center towers lying in ruins were both horrifying and—though few would openly admit it—strangely stimulating. The former because we instantly realized, with despair, that many people had died in the towers' collapse, and that many others would suffer as a result of it for the rest of their lives. The latter because such a grand scale of destruction evoked an essential truth about human existence, a truth so disturbing that it is usually cloaked in denial: *we are all going to die.* 


Not only will we die, but so will all our works. The great buildings, the great works of art, the great books, the great ideas, on which so many have spent the genius of human invention, will all fall to ruins and disappear in time. And not only will all traces of the human as we know it vanish, but the human itself will, too, as it continues an evolutionary trajectory accelerated by bioengineering and future technological advances. What all of this means is that we cannot take comfort in any form of earthly immortality that might mitigate the suffering caused by the certainty of our personal extinction.


It is true that through works of art, artists can live on in the thoughts and actions of others. This, however, is more of a comfort to the living than to the dead, and while it may help a living artist maintain a denial of death effective enough to keep believing that working and striving is somehow lasting, it is an illusion, and a pretty thin one at that. In contrast, the solidarity that develops between people who acceptthe inevitability of oblivion is more substantial and sustainable. When we witness an accident or disaster, we are drawn to it not because of ‘prurient interest,' or an attraction to the pornography of violence, but rather to an event that strips away the illusions of denial and reveals the common denominator of the human condition. For the moment of our witnessing we feel, however uncomfortably, part of a much larger scheme of things, closer to what is true about our existence than we allow ourselves to feel in the normal course of living.


Religions have promised immortality and certainty in afterlives of various kinds, but for many today this is an inadequate antidote to despair. There are people who want to focus on the present and in it to feel a sense of exultation in being alive here and now, not in a postponed ‘later.' This desire cuts across all class, race, gender, political and economic lines. In some religious lore, the ruins of human forms will be restored to their original states, protected and enhanced by the omniscient, enduring power of a divine entity. But for those who feel this is too late, the postponement of a full existence is less than ideal. For them, the present—always both decaying and coming into being, certain only in its uncertainty, perfect only in its imperfection—must be a kind of existential ideal. The ruins of something once useful or beautiful or symbolic of human achievement, speaks of the cycles of growth and decay that animate our lives and give them particular meaning relative to time and place. This is the way existence goes, and therefore we must find our exultation in confronting its ambiguity, even its confusion of losses and gains.


The role of art in all this has varied historically and is very much open to question from the viewpoint of the present. The painting and poetry of the Romantic era made extensive use of ruins to symbolize what was called The Sublime, a kind of exalted state of knowing and experience very similar to religious transcendence, lacking only the trappings of the church and overt references to God. Hovering close to religion, Romantic ruins were old, even ancient, venerable. They were cleansed of the sudden violence or slow decay that created them. There was something Edenic about them—Piranesi's Rome, Shelley's “Ozymandias,” Wordsworth's “Tintern Abbey,” Friedrich's “Wreck of the Hope.” The best of such works are unsentimental but highly idealized, located intellectually and emotionally between the programmed horror of Medieval charnel houses and the affected nostalgia for a lost innocence of much architecture and painting of the late nineteenth century.


Taken together, these earlier conceptions are a long way from the fresh ruins of the fallen Twin Towers, the wreckage of Sarajevo, the blasted towns of Iraq, which are still bleeding, open wounds in our personal and collective psyches. Having witnessed these wounds—and in a palpable sense having received them—gives us no comfortable distance in which to rest and reflect on their meaning in a detached way. Hence, works of art that in some way allude to or employ these contemporary ruins cannot rely on mere depictions or representations—today that is the sober role of journalism, which must report what has happened without interpretation, aesthetic or otherwise. Rather it is for art to interpret, from highly personal points of view, what has happened and is still happening. In the narrow time-frame of the present, with its extremes of urgency and uncertainty, art can only do this by forms of direct engagement with the events and sites of conflict. In doing so, it gives up all claims to objectivity and neutrality. It gets involved. By getting involved, it becomes entangled in the events and contributes—for good and ill—to their shaping.  


Thinking of Goya, Dix, Köllwitz, and so many others who bore witness and gave immediacy to conflict and the ruins of its aftermath, we realize that today the situation is very different. Because of instantaneous, world-wide reportage through electronic media,  there no longer exists a space of time between the ruining of places, towns, cities, peoples, cultures and our affective awareness of them. Artists who address thesesituations are obliged to work almost simultaneously with them. Those ambitious to make masterpieces for posterity would do well to stay away, as no one of sensibility has the stomach for merely aestheticizing today's tragic ruins. Imagine calling in Piranesi to make a series of etchings of the ruins of the Twin Towers. They would probably be powerful and original, but only for a future generation caring more for the artist's intellectual and aesthetic mastery of his medium than for the immediacy of his work's insights and interpretations. Contemporary artists cannot assume a safe aesthetic distance from the ruins of the present, or, if they do, they risk becoming exploitative.


How might the ruins of today, still fresh with human suffering, be misused by artists? The main way is using them for making money. This is a tough one, because artists live by the sale of their works. Even if a work of art addressing ruins is self-commissioned and donated, some money still comes as a result of publicity, book sales, lectures, teaching offers and the like. Authors of such works are morally tainted from the start. All they can do is admit that fact and hope that the damage they do is outweighed by some good. It is a very tricky position to occupy, and I would imagine that no artists today could or should make a career out of ruins and the human tragedies to which they testify.


Adorno stated that there can be no poetry after Auschwitz. His argument rested on the fact that the Holocaust could not be dealt with by the formal means of poetry, owing to poetry's limits in dealing with extremes of reality. Judging by the dearth of poetry about the Holocaust, we are inclined to believe he was right. Looking at a similar dearth of painting, sculpture and architecture that engage more contemporary holocausts, we are inclined to extend his judgement into the present. Still, if we concede the impotence of plastic art in interpreting horrific events so close to the core of modern existence, we in effect say goodbye to them as vital instruments of human understanding. If we concede that, because of their immediacy, film and theater have been more effective, then we consign them to the limits of their own traditions. And so, we must ask, how have the arts dealt with the ruins of Sarajevo and Srebrenica, of Rwanda and Beirut and Iraq, of the Twin Towers' site? How will they deal with the new ruins to come? Time itself has collapsed. The need is urgent. Can art help us here in the white heat of human struggle for the human, or must we surrender our hope for comprehension to the political and commercial interests that have never trusted art?


Today's ruins challenge artists to redefine both their roles and their arts. People need works of art to mediate between themselves and the often incomprehensible conditions they live with, especially those resulting from catastrophic human violence. While not all works of art are universal, they share a universal quality, namely, the need to be perceived as the authentic expression of the artists' experience. Without the perception of authenticity and the trust it inspires, art becomes rhetorical, commercial, and, by omission, destructive. What are the authentic forms of interpreting ruins—the death of the human, indeed, ultimately, of everything— today?


LW


Drawings/digital prints, by Lebbeus Woods and Kiki Smith:


“Serenade:”


![LWblog-DOOM5](media/LWblog-DOOM5.jpg)


“The Kiss:”


![LWblog-DOOM6](media/LWblog-DOOM6.jpg)


“The Garden:”


![LWblog-DOOM4](media/LWblog-DOOM4.jpg)


“Firmament:”


![LWblog-DOOM2](media/LWblog-DOOM2.jpg)


“Firmament” ceiling installation in the Henry Urbach Architecture gallery, New York; backlit digital print (lighting by Linnaea Tillett)


![FirmInstall](media/FirmInstall.jpg)


![LWblog-DOOM1](media/LWblog-DOOM1.jpg)



 ## Comments 
1. [generall](http://General.la)
6.9.09 / 1am


The techno-obsession of A new world forged from the ruins of an old world order. the violent mutation of food geneology. The integration of human and technology, The act of taxation in commerce and the legal designation of humans as property defined by their straw-man ‘corporate' identity. Basically the BS posing as progression, and the oblivion of the soul, lost.  

The Human and Nature as ruin. 


This collaboration with Kiki Smith is surprisingly refreshing/ entirely unexpected. And remarkable strong in contrast. An act of singularity/ guidingly inspired.
3. Stephen Korbich
6.9.09 / 5pm


I was wondering what I would see after your diatribe on the futility of human existance and art.  

Of course what I found were pretty paintings, dark, but pretty. These will no doubt look lovely in some rich patrons living room or foyer.  

Are those who can afford fine art and have time to think about it any better than the rest of us? Are they closer to enlightenment and the sublime, to God?  

Art can remind us of our mortality and how precious life is. The search for the sublime begot art which begot religion and now art has destroyed religion and we are all adrift in this lifeboat together.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.9.09 / 9pm
	
	
	Stephen Korbich: You've hit on the very problem Adorno was speaking of: aestheticizing the horrific, thereby giving it some appeal and worse, making it somehow acceptable. But maybe that, too, has its necessary place in the human scheme of things.
5. Vico
6.9.09 / 7pm


This post was a refreshing reminder of when I saw the work first-hand at Urbach a couple years ago. I remember I was pleasantly lured into the paintings' struggle of gentleness and horror. I still am. Maybe it is the collaboration and the interplay of two hands. Or, maybe it is their magnification/compression–being digital prints, I assume they were made on a smaller, personal level–I could be wrong and it would be interesting to understand the process. Anyways, they have a “zoom factor” that gives them a feeling of erosion. Like they were made some other time and dug up. Or, perhaps, they are photographs of tiny graffiti on cave paintings. 


The text, on the other hand, reminds me of Hannah Arendt's comments on the ancient Greek dichotomy of fabrication versus action. While action is futile and concerned with process in-itself, fabrication dealt with immortality–something that could be integrated into the cycle of history–something that would outlast its maker. 


I like to hope that some acts of artistic creation are eternal–or at least that they mirror the cosmos and that it is.
7. Quadrus Penseroso
6.10.09 / 1am


Wondering what Raimund Abraham was up too, I came across your blog, thank you for such fascinating and serious work.


If you don't mind I would like to share my thoughts on this entry.


The closing remarks have brought into tighter focus – an opportunity for criminal activity.


Writing – push it over the cliff, a stampede, a heap of broken thoughts, more artists cutting their ears, Parisian bath tubs filled with drowned poets, suicides in limbo.


Go ahead, “twin-tower” 500 years of written logos, dominant tool for shaping thought no more, no less.


Eggrio Sig. Buonarotti Simoni, we have restored your honour, the plastic will return. The moment before death which memory cannot record we will see, touch, here and smell.


Writing, whilst in the lead, the centre stage, academicians rubbing coconut oil on a monkey's ass in search of paired aphorisms – words have failed to interpret the end, merely shape a new beginning to excuse the fear.


We cancelled television privileges and taken away certain toys this summer, it's amazing how an old MOMA catalogue and trace paper, penciling over the masters work, then bursting desire to create their own art – have opened a passionate side I never considered children could have. They did manage to sneak something called Sponge Bob by me at the rental store, much to my wife's chagrin! 


Oh and stashed away, for some time now, my entire library, I shall find an early oversized Lebbeus Woods to challenge them, because I have seen a reason to do so.
9. Josh V
6.10.09 / 2pm


I think Banksy is one of my favorite forward-thinkingsocio-political artists out there today. Mainly because most of his art is free and on public property. No one can own it. It's everyone's to appreciate. His NYC Pet Store is a brilliant attack on modern society.  

<http://tinyurl.com/4742yj>  

You can see some great work from New Orleans and around the world here:  

<http://tinyurl.com/2xbnpr>





	1. Quadrus P
	6.11.09 / 4pm
	
	
	Josh, I enjoyed the links to Banksy's [bank-sy –folk-sy – hello-ski –dollar-ski], thanks for posting them. He's rather at the top of his game and commercially successful.
	
	
	Katrina tested New Orleans in as much as 911 did New York. It looks like more celebrities are gaining profitable exposure down there than Rudy Giuliani did with 911. I mean, the East Bloc approach would be to suspect Banksy is hooked up a-la secret business angel with Brad Pitt who was advised by Frank Ghery to open up shop in Manhattan. Man, it's like a chain of wealthy mongering scenesters and sceneographers are cashing in – all the while feeling they are controlling the inside track to social redemption.
	
	
	Piss on them. I'm tired of the jig being set from the top down.
	
	
	Okay sure they are legitimately trying to re-build but I suspect more is taken than returned. 
	
	
	I want to see, hopefully soon, how the Rural Studio Mockbee is doing in that region – it looks much more authentic than what the celebrity machine is cooking up.
11. Alex L
6.12.09 / 10pm


Perhaps what I've always liked about your work is its suggestion of… how would you put it, “timefullness”? An innate quality of your architecture is that is allows for ingenuity, and in fact requires it. One must learn to inhabit your spaces. Diane Lewis once described you as a modern-day Piranesi, especially in regards to the desire your drawings inspire to inhabit the spaces you suggest. I think, though, that this doesn't quite fit because I never really saw your architecture as ruins. It is architecture that exists for as long as it needs to. 


I mention this because it is such a fitting contrast to the story of the World Trade Center reconstruction. The site is an illustration of “Now and Forever,” concepts both opposite to each other and to any idea of “timefullness.” Think what you will about the original towers, they were a powerful statement; about capitalism; about optimism. Good or bad, that statement was literally struck down. It is said that many post-modernists imitated modernism without knowing the forces that drove it. The buildings going up now are hollow imitations of those forces that attempt to perpetuate them ad infinitum. After a mere modicum of time had passed, public officials annouced that they would “build something as quickly as possible” (as if that would erase what had happened), something that in all its qualities is like an architectural zombie.


Shortly after seeing the Bodies exhibit, I asked my roommate if he had had the chance to see it. He hadn't, he said he didn't want to. I assumed it was because of the ethical controversy surrounding the exhibit's acquisition of the bodies, which is understandable. The real reason, though, was that it reminded him of his own mortality. His exact words. It bothers me to this day, and it reminds me of the mentality of the political forces in control of the site. You see, I was one of those people who was fascinated by the destruction of the towers. Hidden amongst all the debris was the knowledge that, one way or another, one day, that would have happened anyway, albeit with very different costs. No time was allowed to reflect on that. I think if it had, or if we lived in a culture more exploratory of death, the site might have lived, or been reborn. As it is, it will always be dead to me, and cruelly strung up.


Architecture has always been for me about the passage of time and the gathering of experiences. Loss and darkness and death is part of this, and is as necessary to architecture as gravity. It's a shame no one could acknowledge that. Admittedly I don't know how long is appropriate to wait. Maybe the length of time doesn't matter as much as the conclusions reached in that time.


This post touched a taut chord within me. The drawings are lovely, haunting, and ultimately comforting. Wish I could have seen them in person.


PS  

There was a program on the discovery channel a little while back called “Life After People,” which posited what would happen to everything people had created if everyone just suddenly disappeared. A fascinating watch if you can rent it.
13. [Alex Bowles](http://www.otofog.net)
6.18.09 / 1am


Something in this post seems dated. At a time – say, a century ago, or slightly more – art and aesthetics were inseparable. At that time, it would have been impossible to respond to a real tragedy or its indirect social repercussions without beautifying the subject to some unsettling degree. 


But after the success of darkened discord from Bacon, Ernst, and others whose works are manifestly not easy to live with, it's much harder to equate compelling aesthetic order with Romantic conceptions of beauty. Considered along with people like Hopper, whose more palatable aesthetic achievements frame a deeper and otherwise invisible hollowness that defies the beautification of surface, is it really fair to say that aesthetically creative (i.e. non-journalistic) responses to destructive moments are still at odds with their subject in in a way that's hopelessly subjective, inherently dishonest, or at least insurmountably unrepresentative? 


The very striking photo at the top of this post is, to my eye at least, *de facto* evidence of the artistic progress we've made, and the way we've moved past the early to mid-20th century fascination with the pure grammar of image. Yes, it's eye-catching, but it seems unfair to dismiss a work simply because its author is skilled in engagement – especially not when we're at a point where formal devices like composition and color are no longer worthwhile subjects by themselves, but simply the means to evoke a contemplative state in which we can reflect in a way that the otherwise frenetic pace of first-world life discourages. 


The charges of corruption through financial opportunism seem more serious, but here too, the idea that the needs of commerce automatically render the work unethical seems rather harsh. Perhaps there would be cause for queasiness if the work were done by someone like Koons or Hirst; artists who have made the absurdity of commerce as performance a central tenant of their work as they [very openly](http://en.wikipedia.org/wiki/For_the_Love_of_God) bank of the value of the object as a fetish. 


But I got no hint of that delirious cynicism from, say, a film as sober, evenhanded, and exceptionally well-made as *United 93* by Paul Greengrass, which was presented by that most commercial of enterprises, Universal Pictures (a unit of GE). In fact, the clear danger of justified public indignation seemed to have provided an extraordinary discipline to which Greengrass responded admirably by making a film that was, appropriately, stripped of all sensationalism and cliche, and as such a major accomplishment in the otherwise sentimental and often crassly manipulative medium of big-budget Hollywood production. 


For me, the thrust of the film was actually made more powerful *by* the context of its presentation, and the keen awareness of what the work could have been in less sensitive and capable hands. So I'm unconvinced that the simple presence of profit is automatic cause for dismissal. As Greengrass demonstrated, it's possible (though admittedly uncommon) for profit to precipitate clarity. After all, here's a case in which a failure to be suitably deferential to the rawness and solemnity of the event could have had very negative results for the project's underwriters. I would suggest that the need for profit *gave* the film the deliberate realism and gravitas its subject deserved. 


For Greengrass, this was a one-off. But even an artist who'd made a well-paying career from wreckage and ruins in general wouldn't necessarily be damned by addressing fresh destruction. After all, it's unlikely that the artist would have achieved any stature in today's milieu were it not for a Hopperesque ability to indicate something entirely real otherwise invisible within ruins. Far from being put off by the success of such an individual, I'd turn to them with the expectation of some well distilled insight. Assuming the artist wasn't in the habit of *causing* the destruction he responded to, I suspect their work would be an asset, not a liability. 


So here again, the material relationship of the artist to the subject isn't (usually) the real cause for concern. The core of the creative issue seems to be that the ruins themselves are the products of political and economic failure, and as such, they tend to implicate to some degree *everyone* who surrounds them. They're everybody's problem. 


Artists, as you note, have long had a very uneasy relationship with politics and economics alike, not to mention their more mindless agents. But if an artist's response is to rise above the level of “you're all idiots and I told you so”, he'll need to achieve something with the type of universal appeal that will allow others to develop a suitably clarity about the capacities and necessities of politics and trade, along with the requirements for handling them safely, and the destructive potential that comes with getting them wrong. To do so it's likely that the artist – as with any figure of enlightenment – will need to participate in the dynamics that shape their subjects at the same time they're observing them remotely. 


I'd go so far as to say that, in the thoroughly networked age, the Classical divide between the forms of the contemplative and active lives is swiftly becoming an anachronism. The Platonic ideal of the Philosopher King – both active and aware – is something that will be called for not only in artists, but in exponentially greater numbers of people whose choices and judgements can now be magnified to extraordinary effect for both good and ill. 


Accordingly, the real creative challenge falls outside the work, or definitions of art and the artist's role. Instead, it's found in the artist's transition between two states of being. Authentic commitment to a subject or theme matters, but so does the cultivation of mindful attention that allows one to watch one's self from a distance while fully participating in the present. And given that cultivation of this inherently dynamic yet peaceful state is, perhaps, the most pressing need for sociable humans in general, it's important that the artist reveal the existence of that simultaneously detached and invested outlook in the work itself. 


The real benefit of this state is that the artist, and by extension the audience, can come to terms with mortality – in themselves, their families, their work, and their culture. After all, as focus turns away from the maddeningly transitory nature of life and creation, and towards the astonishing splendor of the universe that we're moving through, however briefly, the thing that becomes clear is that the manner in which we order our material affairs has a significant role to play in the way we regard the life we've been given, and the degree to which we can appreciate the layers of extraordinary fortune implicit it the statement ‘we are here'.


Ruins, ultimately, are about the failures of domination, and the desire to look down forever eclipsing the desire to look up for a moment. We're all in the middle, immersed in politics, commerce, hope, fear, hunger, joy (if we're lucky) and the relentless demands of physical satisfaction. Artists are no different from anyone else in this regard. Nor are they distinguished by their basic capacity for transcendental experience and the peaceful acceptance of our mortality that this state can precipitate. What's remarkable about the good ones is their ability to look forward, back, in, up and out with an eye to sharing the view with others. 


Sites that are still raw, bleeding, and traumatized present an especially acute need for inductive works that both demand and reward leaps of faith. And artists that involve themselves in the historic events surrounding these places, the daily life that limps on within them, or in the definition of the reactions to these crises – both short and long term – shouldn't be dismissed because they derive their own sustenance from what they do here. Instead, they should be judged by their ability to help others transcend the ill effect of circumstances that were only marginally within their control, and transform that margin into a recovery for which new patterns of interaction are central.
15. [System Wien – Architecture of Energy « Republish](http://www.republish.eu/art/system-wien-architecture-of-energy/)
7.5.09 / 9pm


[…] good start seems to be the posts Doom Time and Dead Words. Architecture, Art Name (*) Mail (will not be published) (*) […]
17. Gary He
7.26.09 / 1am


LW, 


If contemporary society at large has come to accept this inevitability of death as a greater truth or wholeness of life than a “postponed later” as you suggest it might, does our position as architects change in regard to the future which we project through our work? 


It seems to me like ruins of the past are simultaneously representative of the relentless corruption of nature and yet also man's will to live forever through his work. The denial of such a will for better or worse seems like a fundamental change in the psyche of an architect. 


I'm not so certain that ruins must always be associated with tragedy or despair – even if we are to come to terms with our eventual demise, that does not rule out the ritual of mourning, recovery or new life that follows. There may even be life after the human race, and our architecture may in fact become a fossil, a physical link between this past and that future. For this reason I think there is optimism in ruin. 


I find this article particularly relevant for me because of my recently completed [thesis](http://garyhe33.livejournal.com/68590.html) at Cornell (you can ask Kent about it, as he was engaged in the final discussion). Thank you for sharing.
