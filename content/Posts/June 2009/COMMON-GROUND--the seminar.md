
---
title: COMMON GROUND  the seminar
date: 2009-06-17 00:00:00
---

# COMMON GROUND: the seminar


**Cornell University, M2 Summer 2009 Program-NYC, Seminar Description, Professors Lebbeus Woods and Christoph a. Kumpusch. Kent Kleinman, Dean of the School of Architecture, Art, and Planning (AAP). Professor Mark Morris, Director of the M-ARCH 2 Program.**


**COMMON GROUND**


**Overview**


The city is often thought of as an ensemble of buildings and the people who inhabit them, establishing a particular way of living that combines the private and the communal. It is useful to remember that a city is also an ensemble of open spaces that are largely public, where private and communal life are openly shared. In our brief time together, we will focus on the not-so-obvious evidence of human activity and thought in these public spaces, and to build a dossier of such evidence, accompanied by our critical reflections on it. The goal of our seminar is the critical exploration of specific physical conditions highly relevant to the actual life of the city.


The specific focus of our study is New York City's ‘common ground.' This is the ground surface of streets, sidewalks, parks, plazas and other (even informal) public spaces, comprising the ground-plane we share with others we do not personally know. This is the ground-plane inscribed with evidence of people's movements, activities, and intentions, but also the accidental events, the unintentional and the unpredicted. This ground-plane informs us, if we are willing to analyze and interpret it, better than official plans and policies, about the actual life of those who live in New York. If we can interpret this common ground, we can interpret relevant but previously hidden dimensions of living in the city.


It might be possible, for example, to interpret the life and culture of New York from the scuff marks of shoes outside Grand Central Station, or the spray-painted marks on streets made by surveyors to indicate water mains or buried telephone lines for repair teams. It might be possible to interpret the cultural practices and priorities of the city government from the unrepaired cracks in a sidewalk, especially if they occur in a busy, central part of the city.


There is much evidence, but little documentation or analysis. People find it easier to follow official pronouncements and newspaper and TV reports. Architects are well suited for both documentation and analysis because of their visual acuity and critical imagination about relationships between material conditions and human activity, desire, and aspiration. Our task in the seminar will be to document and creatively analyze aspects of our ‘common ground' in New York City.


**Assignments**


Assignment 1 is for each student to find and document, through photography, drawing, or other graphic meamns, a particular piece of common ground that seems rich with implications for an understanding of life in New York City. Due Wednesday, June 17.


Assignment 2 is for each student to write a brief commentary about the piece of common ground and/or the documentation of it that explores its material conditions and draws implications about specific aspects or urban living or experience. Due Wednesday, June 17.


Assignment 3 is for each student to make a graphic representation or analog of his or her analysis. Due Monday, June 22.


Exhibition of all work, with discussion between students and invited critics and discussants. Wednesday, June 24.


 


**Outline of INTRODUCTORY LECTURE by LW**


I. drawing, writing, interpreting


Drawing was invented before writing, though spoken language predated both. Cave painting in Lascaux, France, c. 14,000 BCE, shows highly refined formal and technical understanding and technique, as well as an almost ‘modern' conception of montage and perspective. Interpretation relies on understanding the painters were members of nomadic hunting tribes who had a special identification with the animal world of which they were so intimately a part:


![LWblog-Lascaux1a](media/LWblog-Lascaux1a.jpg)


The earliest known writing system used an alphabet of highly abstract characters, and and was invented in Sumer, c. 4,000 BCE. The Sumerians, living in fertile southern Mesopotamia were among the first to develop year-round agriculture, settled communities, and regular trade, making written records a necessity. This sample on a clay tablet has eluded definitive interpretation:


![LWblog-CGRND-1](media/LWblog-CGRND-1.jpg)


Egyptian hieroglyphic writing, seen cut into stone around 2,500 BCE, employs pictographs or ideograms to communicate a thought or narrative, instead of a demotic or abstract alphabet, and was indecipherable until the discovery of the Rosetta Stone in 1800 CE:


![LWblog-CGRND15](media/LWblog-CGRND15.jpg)


The Rosetta Stone, made around 200 BCE, but not discovered until Napoleon Bonaparte's invasion of Egypt in 1799, contains the same text written in hieroglyphics, demotic Egyptian, and ancient Greek, opened the way to a full interpretation of hieroglyphics:


![LWblog-CGRND6b](media/LWblog-CGRND6b.jpg)


 


**II. chance, accident, spontaneity**


“Automatic writing” is made by a person in a trance or deep reverie, who turns willed thought over to presumed spirits or supernatural communicators. Its popularity reached its peak in the late-19th and early-20th centuries, at exactly the time that industrialization and large cities came to dominate European and American cultures. Its interpretation relied on the insights of various seers and mystics:


![LWblog-CGRND18](media/LWblog-CGRND18.jpg)


A close cousin to automatic writing is a type of drawing known as ‘the exquisite corpse,” so called according to the nonsense name given to it by “dada” artists who used it as a way to trick rationality by the introduction of chance and accident. Typically, a piece of plain paper is folded into three or more panels. The first artist/player draws something on the first panel, allowing only the lines drawn to the top of the panel to extend slightly into the next, still blank, panel. Then the paper is refolded so that only this blank panel is visible and it is passed to the next artist/player, who makes a drawing starting with the slightly extended lines from the first panel, extending the lines of his or her drawing slightly into the third, still blank panel. The process continues, with two or more artist/players, until the last panel is completed. Even the simplest finished drawing suggests multiple interpretations:


![LWblog-CGRND20a](media/LWblog-CGRND20a.jpg)


Dada art, which flourished between 1915 and the early 1920s—and was the precursor of Surrealism—was conspicuously based on chance and accident, as well as traditional design and composition, to create not only a new aesthetic, but a new type of aesthetic that challenges traditional methods of rational interpretation:


![LWblog-CGRND11](media/LWblog-CGRND11.jpg)


 


IIa. **INTERLUDE**


In Europe, World War I (1914-18)—with its destruction of human lives in the tens of millions, and attacks on cities, and its aftermath of economic depression—had created a new feeling of uncertainty about the future. The monarchies that had ruled for centuries had fallen and were replaced by shaky democracies or unpredictable proto-Fascist regimes. Not only that, but hard, physical science had for the first time theorized a highly uncertain picture of nature that generally increased people's anxiety about the stability of their own lives and, indeed, of matter itself. In medicine, Freudian psychoanalysis drew a picture of human behavior driven more by primitive emotions than reason. Dada and Surrealism were clearly responses to all these events, which transpired in an interlocking way in the years from 1900 to 1930. Given the realities of urban life in this period and beyond, Modernism in architecture, with its emphasis on rational planning and design, can only be seen as a form of ‘swimming upstream,' going against the tides of uncertainty, unpredictability, and ambiguity of meaning overtaking the human world.


 


**II. (continued)**


A dada-inspired painting from Robert Rauchenberg from the late-1950s:


![LWblog-CGRND23](media/LWblog-CGRND23.jpg)


Chance, accident, and design are all apparent in this complexly layered graffiti, made by many people over a period of time, and comprised of abstract and largely indecipherable characters. Though the whole has no literal intended meaning, the work creates a strong image that  speaks most clearly of the process that brought it into being:


![LWblog-CGRND14](media/LWblog-CGRND14.jpg)


The painting by Willem de Kooning, from the 1950s, has much in common formally with the graffiti, in that its meaning lies in the process of its making. Called ‘abstract expressionism' and ‘action painting' by its expert interpreters, art crtics and historians, this way of working involved only a single artist and therefore lacked graffiti's collaborative, social dimension, and its conceptual complexity:


![LWblog-CGRND12](media/LWblog-CGRND12.jpg)


Cy Twombly's paintings from the 1970s emulate graffiti and naive or children's art, but with a more self-conscious spontaneity, bounded by the limits of a marketable canvas:


![LWblog-CGRND7](media/LWblog-CGRND7.jpg)


![LWblog-CGRND24](media/LWblog-CGRND24.jpg)


Frank Gehry has made no secret of his debt to contemporary painters. His scribble-like sketches for buildings are inspired by dada and surrealist concepts and methods and require extensive interpretation by his collaborators to become building designs. His spontaneous method of working has resulted in unique architectural forms that have been realized only with the aid of highly sophisticated computer software. In a demonstrable sense, he has fused the accidental and spontaneous with advanced technology:


![LWblog-CGRND17](media/LWblog-CGRND17.jpg)


The architect Hermann Finsterlin, who worked with methods similar to those of Gehry from the 1920s through the 1970s, and certainly was known to Gehry, never built any of his building designs because he lacked the computer-aided technical means to do so. He was an architect too far ahead of his time:


![LWblog-CGRND16](media/LWblog-CGRND16.jpg)


 


**III. COMMON GROUND**


From the System Wien project of 2005. Vector analysis in plan of people's movements on the streets and in private spaces in the First District of Vienna, in a five-minute span of time:


![LWblog-CGRND21](media/LWblog-CGRND21.jpg)


The vectorial spaces of the System Wien project crystallize the complex interactions of different energy systems in the city, using only hand-methods that embody human energy at the scale of the street and the room. It imagines that urban space can be transformed not through large architectural projects, but an accumulation of small, spontaneous  energy shifts, additions, and exchanges, involving many people over time:


![LWblog-CGRND22](media/LWblog-CGRND22.jpg)


A piece of common ground—a sidewalk in Manhattan shared by many people—with accumulated marks and patterns of their activities, some of which are by design, others of which are accidental. Even this small piece of sidewalk is encoded with a visual record of the life of people and the city, not unlike the tablets of an ancient civilization, and which await interpretation: 


![LWBLOG-NYSW12](media/LWBLOG-NYSW12.jpg)


Interpretation might come through a self-conscious personal intervention in the common ground:


![LWblog-NYSW12a](media/LWblog-NYSW12a.jpg)


Or it might come in an interpretation of its component traces, such as the mysterious black spots:


![LWblog-CGRND10](media/LWblog-CGRND10.jpg)


Or, when we scratch the surface of this present civilization and find yet another—exploring in imagination the continuum of human history:


![LWblog-NYSW12b](media/LWblog-NYSW12b.jpg)


## COMING SOON:


The results of the seminar.


Poster by Landon Robinson:


![Common Ground](media/Common_Ground.jpg)


LW



 ## Comments 
1. paulo miyada
6.18.09 / 1am


would be great to see what people will produce during this seminar. hope you can publish it somehow.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.18.09 / 2am
	
	
	paulo miyada: I will certainly post some of the work as addenda to this post. It is a brief, intense seminar, which concludes next Wednesday, June 24th.
3. Pedro Esteban Galindo Landeira
6.20.09 / 3pm


I will made this seminar in Havana City. Is quite interesting how to find by means of use of his layers the history of Havana.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.20.09 / 3pm
	
	
	Pedro Esteban Galindo Landeira: Kindly let me know the results of the seminar in Havana, as I would like to post them.
5. Pedro Esteban Galindo Landeira
6.20.09 / 3pm


Thanks a lot. I will do it. I'm thinking on the ruins of this city, because we have ruins from 1700's to 1980's. I'm trying to say our society is in ruins too. I saw the architecture as an image of society. I have written about it but just little things with this seminar I'll expand those concepts.
7. [CP](http://maisoncolumba.blogspot.com/)
6.21.09 / 10am


hi -> I'm a regular reader -> this is my 1st post -> I have been surveying urban lint for the last six months -> its not extensive (more recreational really) and starting to find material correlation -> looking forward to seeing the assignment results.
9. Pedro Esteban Galindo Landeira
6.22.09 / 1am


LW:


I have two ideas.


One, is work with ruins but this idea is not ground, is space, but is in the ground of public spaces and shared for everyone in this city, and is a mark of what this was. Here I have more meanings and lectures. 


The other is work with a program of the goverment, the instalation of pipes for liquified gas who leaves in the ground( properly) importants cracks who in ocassions needs to close some streets and the public space change dramatically. With this I can talk about the program and the fluids in some streets.


Please let me know the best idea depending the lines of the seminar.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.23.09 / 9pm
	
	
	Pedro Esteban Galindo Landeira: The whole idea of a seminar or workshop like this is to have a tight focus and very precise boundaries. In that sense your ‘ruin' idea is too generalized, too imprecise, and the students will be ‘all over the place.' In that case, there will be no basis for dialogues between their projects, both in the working phase and the critical evaluation phase. However, the idea of “fluids in some streets,” and documenting all aspects of how it happened and what it does to the streets and the people who inhabit them, strikes me as a very fine idea for a seminar or workshop of a brief but intense kind.
11. egrec
6.25.09 / 7am


I'm getting shades of Italo Calvino from this… esp. “Invisible Cities” and a late chapter of “Marcovaldo”…
13. [Elsewhere « Visualingual](http://visualingual.wordpress.com/2009/06/26/elsewhere-63/)
6.26.09 / 12pm


[…] Common Ground: The Seminar: interesting summer course at Cronell, co-taught by Lebbeus Woods; via Mockitecture. […]
15. river
6.28.09 / 8am


An unassumingly simple, clear, and exciting boundary for future projects might be ‘Water Quality,' and all that it entrains.
17. Christopher Lauriat
7.1.09 / 1pm


LW: I've updated my own blog with my work from the seminar as well as a few photographs of the exhibition. I look forward to seeing your reflection on Common Ground. Our ten days of collaborative discovery were quite unique and served as a reminder that architecture is ultimately about people, not buildings.
19. egrec
7.7.09 / 5pm


 [— seems apropos to this discussion.](http://newsfeed.kosmograd.com/kosmograd/2009/06/the-past-bleeds-into-the-future.html "The past bleeds into the future.")
21. [COMMON GROUND: the seminar work « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2009/07/14/common-ground-the-seminar-work/)
7.14.09 / 11am


[…] COMMON GROUND: the seminar work A selection of works by participants in the Cornell University COMMON GROUND Seminar: […]
