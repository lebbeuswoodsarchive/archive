
---
title: TOTAL DESIGN
date: 2009-06-13 00:00:00
---

# TOTAL DESIGN


![LWblog-Mies3](media/LWblog-Mies3.jpg)


![LWblog-MIES4](media/LWblog-MIES4.jpg)


![LWBLOG-Mies1](media/LWBLOG-Mies1.jpg)


[above] The Farnsworth House, in Plano, Illinois, by Mies van der Rohe, 1950.


The post-World War II world was one of hope and of fear. Hope for a future in which all people would benefit from the fruits of human progress. Fear that the rival global superpowers would destroy each other, or, worse, that one would destroy the other, prevail, and force its way of life on all.


 It was in this ‘age of anxiety' after 1945, that American design professions—building design, urban planning, furniture and interior design—reconstituted themselves for a postwar world. This involved experimentation with new, synthetic materials; a new degree of coordination between industrial manufacture and the design of everything from building panels to furniture; and, not least, a new degree of coordination of all the elements in a room, a building, or public spaces that became known as “total design.” This was an important trend through the 1950s.


 Total Design, as exemplified by Mies van der Rohe's Farnsworth House of 1950, and as conceived and practiced by Florence Knoll, Eero Saarinen, Charles and Ray Eames, and others in the 1940s and 50s was arguably the fulfillment of a Modernist vision of totality possible in a highly industrialized mass society, codified by the Bauhaus school and workshops in Weimar and Dessau, Germany in the post-World War I era. This same vision had also flourished briefly in another defeated nation, Russia, just following the October Revolution in 1917. The promise of this vision was a society, democratic or egalitarian, that would live and work in buildings and cities that were both efficient, by modern industrial standards, and beautiful. It was an almost utopian vision that was in effect thwarted by the rise of totalitarianisms of an autocratic, reactionary nature during the 1930s. The military defeat of these regimes in World War II, encouraged some designers to think that this idealistic Modernism could be picked up where it had been left, and carried forward with industrial technologies vastly expanded and improved by the war effort. Many believed that America with its newfound dominance, had a moral obligation to show the way forward, and the advocates of Total Design went about their innovative work with messianic fervor, driven by a sense of moral rectitude. Tacitly or not, they aimed at creating a benevolent totality that would improve the conditions of living for the many. And America, still under the influence of a soft form of ‘New Deal' socialism, but with rapidly growing power and wealth, seemed to them the natural, perhaps inevitable, place for this to happen.


 It should be noted that, in the simplest equation, design equals control. When a building or a chair are designed they are not yet made, The designs are instructions to the makers to make them in particular ways. No room is left for the makers' improvisations; rather the end product is controlled by the designer, in coordination with builders and manufacturers, as well as those who will market, distribute and sell the products. While there is risk involved, the effort is made to eliminate chance as, for example, in using hand-craft methods which will vary unpredictably. For a designer who designs, in a coordinated way, all the products in a working or living environment, the goal is to eliminate chance altogether. Total Design equals total control.


 The design of private houses, office buildings and their working spaces were prime subjects for Total Design in the 1950s. Not only were the buildings thoroughly designed and specified by the architect, but also the interiors, their furnishings and decoration, usually in league with more specialized designers. In some extreme cases, custom-designed desks were arranged according to plan, then screwed to the floor, to insure they would not be moved. Carpets, draperies, paintings and other decorations were selected by the designers, as were all colors used throughout. In company cafeterias, the kitchens and serving equipment were designed, and also the uniforms of the servers, and the menus, and the flatware and the china. Legally binding contracts were written between some more famous designers and their clients, that none of these things could be altered without the designers' permission.


 While the designers did not control the dress of the people using these places, dress codes were rigorously enforced according to corporate standards. The 50s was the era of The Man in The Grey Flannel Suit, and women office workers never wore slacks. As for the behavior of the office workers, it, too, was controlled by corporate policies, all of which aimed ultimately at producing a predictable product that would be predictably consumed by a suitably ‘educated public.' It was a time of moral prigishness: anti-gay, repressive of minorities, intolerant of any non-conformity. Not surprisingly, the era came to an abrupt and—in the social, political sphere—a sometimes violent end in the 1960s.


 Total Design was expensive. As it had been with the Bauhaus, the ideal of the best furniture, fabrics, and household goods being mass-produced and available to all never came close to being realized, and remained the exclusive domain of the well-off. Modernist buildings remained the option only for those who could afford unique ‘designer' houses or corporate buildings. What began as a movement that could inspire industry to provide efficiency and beauty to the many, ended as a ‘luxury goods' business for a relative few. Total Design failed.


 The reasons it failed are several. Certainly one reason was the inability of designers and architects to convince business and industry that its egalitarian mission would be profitable enough. Some attempts were made, such as the prefabricated metal Lustron Houses, Frank Lloyd Wright's Usonian Houses, and the Eames' Case Study Houses, which employed standardized materials and methods, but none of these found a positive reception with the broader public. The American public, it seems, was not ready for totality of design and control. Hopelessly insensitive to the virtues of innovative modern design, especially at the price of relinquishing their taste for the traditional, they appeared ready to accept less-than-total versions of modern design in the workplace, then retreat to a suburb of off-the-shelf, hand-built wooden houses decorated inside and out with references to the past.


 Another reason Total Design failed is that, ultimately, it was abandoned by designers themselves. By the mid-1960s a chorus of leading architects and designers, led by Robert Venturi and Denise Scott-Brown, as well as Reyner Banham and the Archigram group in England, rejected—for various reasons—Modernist ideals of totality, and argued for more eclectic or otherwise less controlled methods of design. This corresponded with widespread political attitudes of the time, as well as radical changes in fields of philosophy and the humanities that became the position generally known as post-Modernism.


LW



 ## Comments 
1. Marc K
6.15.09 / 11pm


It seems that in such a way the act of design becomes an extension of the body of the architect, where one becomes hesitant to trust others with their own work, because they feel a physical connection to what they have created. But design is meant to be a collaborative process, which forces many to either peel away from such thought of cooperation in lieu of their ego or beliefs of how one is supposed to make.


For myself, it is unfortunately easy to see how the innovative makers of the 1950's saw the ways to make and present their work. They were given opportunity, funds, and control beyond belief. I know in my own heart given such a chance I would implement a similar totality in design, because it feels as if I have created the entire spirit of the work.


However, it stands that collaborative efforts produce much better results…
3. iheartmies
6.24.09 / 8pm


isnt paper architecture total (paper) design?


one gets to control the environment and the theory behind how it will be used.


no fear of collaboration, just an idea with out implementation.


in the same sense, isnt the design build model becoming a total design? but a more cost effective model.  

so many architects are becoming developers these days.
5. [Bauhaus Design](http://www.bauhaus-total.de)
2.11.11 / 3pm


What a genius architecture. Really cool 😉
