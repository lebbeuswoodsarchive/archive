
---
title: AS401  MU Analog
date: 2009-06-01 00:00:00
---

# AS401: MU Analog


**COLLABORATIVE ANALOGICAL WORKSHOP, MU Art Foundation, Eindhoven, The Netherlands, 24 September-1 October, 1998**


**PARTICIPANTS**


A dozen or so architecture and design students in Eindhoven, including Sander Boer, Gijs Joosen, Leni Niegemeers [the full register of names is actively being sought, and will be posted here when it is located]. Lebbeus Woods, Jos Bosman, Faculty. Ton van Gool, Director, MU Art Foundation.


**PROGRAM**


In the week we have to work together, our goal will be to construct a free-standing wall comprised of separate elements each participant will make. To make the elements, we will begin with flat aluminum sheets (one by three meters) found in a local scrapyard. Each sheet is unique, in that its surface has been marked—often subtly—by weathering, rough handling, and other accidental factors. Beginning with a reading of these marks, each participant will make a drawing on his or her sheet to indicate lines of cutting, bending, and folding that will enable the sheet to stand upright on its own. After completing the individual elements, we will work together to assemble them into a wall.


**Methodological sketch by LW:**


![MUtext1a+b](media/MUtext1a+b.jpg)


Laying out an aluminum sheet:


![MUconst4](media/MUconst4.jpg)


Drawing on an aluminum sheet, following ts characteristics:


![MUconst7](media/MUconst7.jpg)


Wall elements: creating strength through shape—cutting, bending, folding:


![MUconst6](media/MUconst6.jpg)


Note paper models on the foreground floor:


![MUconst3](media/MUconst3.jpg)


![Muconst5](media/Muconst5.jpg)


![MUconst9](media/MUconst9.jpg)


![MUconst1](media/MUconst1.jpg)


![MUconst8](media/MUconst8.jpg)


![MUfinal10](media/MUfinal10.jpg)


Constructing the wall:


![MUfinal1](media/MUfinal1.jpg)


![MUfinal2](media/MUfinal2.jpg)


![MUfinal3](media/MUfinal3.jpg)


![MUfinal4](media/MUfinal4.jpg)


![MUfinal5](media/MUfinal5.jpg)


![MUfinal8](media/MUfinal8.jpg)


**RETROSPECTIVE COMMENTS**


As with all analogical projects, results and process are equally important. If a design is realized by any means whatsoever, then authenticity is lost and this inevitably diminishes the result. If method and process are emphasized at the expense of results, then they become meaningless. One need not, nor should, sacrifice one for the other.


 In the analogical project, process assumes a greater degree of importance than in the simulated actual project in the normal design studio, because it is more than a means to an end. It is, indeed, a part of the desired end result, as it creates the community served by the collaborative design work, and is unique for every project. To clarify: the analogical process from one project cannot be applied directly to another project because each new situation and its conditions are different. However, one analogical process can inform another one, by inspiring the general structure of its formulation.


 The product of an analogical project will not, in most cases, resemble what we normally think of as architecture. It will probably seem like an ‘artwork' of some kind, a sculpture, a painting, a performance. In the case of the wall, it certainly turned out to be more of a collaborative, painterly sculpture than tectonic in the architectural sense. This was due to three factors. First, the abbreviated time of the workshop left little time for analysis and critique—the participants had to begin construction of their wall elements on day one. Second, the students were very inventive and energetic, and brought their diverse design backgrounds to bear. Third, and perhaps most importantly, the question that was put before them by their faculty (LW and Jos Bosman) was framed in a way that encouraged, if not guaranteed, the resulting analog of the idea of ‘wall' would be as playful and free as it turned out to be. Moreover, it succeeded in being the instrument bringing this small, transient community together. The result is a visually coherent testament of the creative intensity of both the community and the individuals within it. 


LW



 ## Comments 
1. ben
6.1.09 / 4pm


fantastic.
3. sander boer
6.2.09 / 9am


With regard to the registry of names, I can help you out with two:  

Gijs Joosen,  

Sander Boer


I recognise the others, but I forgot their names.


PS, A while back, Jos told me my work (the red painted one) is still at the university, is that true ?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.2.09 / 11am
	
	
	Sander Boer: Thank you for these and any other names you can confirm. As for your piece, I don't know, but trust Jos' report.
5. river
6.5.09 / 8am


Excellence! Aluminum?


deer (local ungulate) pemmican  

architectural dog and bird houses  

kayaks  

knives and other elegant weapons of hunting  

bows, arrows, bolas, dartguns  

decorated leather bags, pants, and shoes soled with the latest nanomaterials  

pant belts and buckles, various clever knots  

shiny metal things, like faucets and bathspouts, frying pans  

—


forgotten ways of survival in the wide, bluegreen world


blue, red, yellow, green, grey, brown, black, white


camcorder analysis reveals environmental metadata color histograms:


grocery stores – bright colors, high contrast  

video games – increasingly natural colors, high contrast  

city streets – dim greys w/ outstanding flecks, alternating low-high contrast, dim-plural shadows, realtime penumbras  

lake paddling – reflective animated complexities, up-down contrast in major and minor key  

deep green woods – green grey brown envelopes, low contrast (depending on sun angle), atmospheric moisture, plethoras of novel critters to track


—


as a modern architect, i have been diligent, and fortunate enough to accidentally discover the roots of shelter.


it has made me a rather lame duck in the previous economy. 


behind the skins of our drywalls, however, and far far away beyond the source of our plumbing, (now by Nestlé ™), there is still a primal urge dimly waiting. Not just to parade and show perfectly!, but to survive hard winters, to work less hard next time, to decorate meaningfully, and to reach out boldly for sublimity through the poles of perception and creativity in everyday life.


What the elders warned us about is true. We are not respecting the (w)holeness of our quality, and it is damaging everything.
