
---
title: OUTSIDER ARCHITECTURE
date: 2007-10-01 00:00:00
---

# OUTSIDER ARCHITECTURE


Over the past few years, I've had occasion to think about architects who have produced work of various kinds outside the mainstream certified by the historians and critics who make up architectural academia, or otherwise influence the climate of opinion. To some degree, my thinking was prompted by my own work and where it might be placed in today's critical categories. Yet, what interests me here is other architects whose work has had enough impact on the mainstream to be visible, but is kept by the critics at arms (or barge pole) length from the historical canon.  

Part of my interest comes from a conversation I had in 1997 with Toshio Nakamura, the legendary editor of Japan's A+U (Architecture and Urbanism), who wanted to commission from me an ‘alternative history' of modern architecture to be published serially in his journal. I, of course, accepted immediately. What an exciting prospect it was! However well done most histories of modern architecture are, one gets tired reading about the same buildings, the same architects, tired of shuffling time and again along the same worn path. Maybe the conventional histories get it right–maybe contemporary architecture is influenced most by a score of canonical buildings about which we have all read countless times. Or, maybe the repetition creates an influence at least equal to that of the buildings themselves. Maybe we become so influenced by the historical certification that we gullibly follow along, drawing on what we've ‘learned' in school and beyond.  

In any event, there have been a number of buildings, and their architects, whose influence has more or less seeped through the filters of architectural education–and mainstream architectural journalism–and yet has impacted architectural thought and design, despite the inattention of critics and historians. What a history that would make!  

Well, Mr. Nakamura left A+U shortly after our conversation. Otherwise, I would have been obliged (following the principle of kiri) to write the history I had promised to him. Maybe I'm still obliged. If so, I'm taking my time, and I haven't lost my interest, and bits and pieces of progress are visible—at least in my files.  

Who are these ‘outsider' architects? Which of their buildings, or un-built designs, are significant? What is important to say about them? How do we bring them into our thinking today? Or, should we? Maybe they should stay “outside, in the cold, a little longer.” Here's several at the top of my list:  

Rudolf Steiner (Goetheanum I and Goetheanum II).  

Hermann Finsterlin (speculative projects)  

Frederick Kiesler (Endless House)  

John Hejduk (Masques, Cathedral)  

Surely there are others, and it would be very interesting to hear who they are, and to discuss and debate their ideas, buildings, and their place in the ongoing flow of modern architecture.


[![Hermann Finsterlin, Landscape](media/Hermann_Finsterlin,_Landscape.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-fin1.jpg "Hermann Finsterlin, Landscape")[![Hermann Finsterlin, building design](media/Hermann_Finsterlin,_building_design.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-fin2.jpg "Hermann Finsterlin, building design")[![Hermann Finsterlin, building plans](media/Hermann_Finsterlin,_building_plans.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-fin3.jpg "Hermann Finsterlin, building plans")  

[![Rudolf Steiner, Goetheanum I](media/Rudolf_Steiner,_Goetheanum_I.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-g8.jpg "Rudolf Steiner, Goetheanum I")[![Rudolf Steiner, Goetheanum II](media/Rudolf_Steiner,_Goetheanum_II.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/steiner-g1.jpg "Rudolf Steiner, Goetheanum II")[![Rudolf Steiner, Sculpture, “The Representative of Humanity”](media/Rudolf_Steiner,_Sculpture,_“The_Representative_of_Humanity”.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-sculpt1.jpg "Rudolf Steiner, Sculpture, “The Representative of Humanity”")  

[![st-kies2b.jpg](media/st-kies2b.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-kies2b.jpg "st-kies2b.jpg")[![Frederick Kiesler, Endless House model](media/Frederick_Kiesler,_Endless_House_model.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-kies1.jpg "Frederick Kiesler, Endless House model")  

[![John Hejduk, angel](media/John_Hejduk,_angel.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/hejduk1.jpg "John Hejduk, angel")[![John Hejduk, Cathedral](media/John_Hejduk,_Cathedral.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/hejduk3.jpg "John Hejduk, Cathedral")[![John Hejduk, Oslo masque](media/John_Hejduk,_Oslo_masque.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/st-hejduk6.jpg "John Hejduk, Oslo masque")



 ## Comments 
1. [miss representation](http://www.missrepresentation.com)
10.4.07 / 9pm


I think it's fair to say your list is a result of a large degree of personal interest/approval (that's an awkward term, I know), and I would second most of the list. But isn't the word ‘outsider' a little kludgy when applied to at the very least to Hedjuk? Is there anyone who doesn't venerate his work inside the profession (and, rightly so, in my estimation)? How many insider architects get shows at the Whitney? Doesn't this sort of project run the risk of an analogous process of identifying ‘outsider art' and then starting with Harold Finster?


I really would welcome a layer of insight you have in your role as an educator and critic to talk about the social relations that build firms into ‘names' and see if there is a correlation there to those who managed a modicum of fame as unbuilt architects/designers. 


And it would be great to get Kevin Lippert to weigh in on this. Taste-making of a purely “critical” sort relies on exiting communities (albeit far smaller than professional circles) and even more suspect, since we have far fewer yardsticks with which we can qualify, aside from, say, “Mark Wigley likes them, so they must be good.”


That's not to say people don't operate without critical rigor, either as outsider architects or their critics and fans, but are there externalities that enable some but not others — in graphic design, Stefan Sagmeister's reputed a priori personal wealth gives him far more latitude when selecting clients, against Tibor Kalman always needing to pay the bills always; clearly distinct critical paths emerged because of this. How relevant is a fact like that in the discussion. That Sagmeister focuses obessively on himself and by the end of his career Kalman became increasinly iconclastic about the inequities in global economies seems to indicate this is true.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
10.6.07 / 8pm


One point of the post is to open the subject of what place works of intense originality, involving personal exposure and risk, have in a field dominated by fashion and the global consumption of architectural products. Are they to be grist for that mill? If so, it can only be done by reducing them to imagery, rather in the way that Philip Johnson stripped away the social program of early modernist architects to make his ‘International Style' show in 1932. A bad idea, in my opinion.


John Hejduk was an insider who became an outsider, because his work took him in that direction. His work was what mattered to him, and the ideas he intended it to embody. His work was shown at the Whitney after his death. While he was alive he was never invited to have exhibitions at either the Whitney or at MoMA, even though younger members of his Cooper faculty of the time did. The reason, I believe, was that he and his work (especially from the later period) were ‘out.' Also because, he refused to play the game of celebrity, by wooing the favor of people like Johnson, and showing up at the right cocktail parties. 


I think the works of Steiner, Kiesler, and Hejduk (Finsterlin is a more complex matter) have been hugely influential in contemporary architecture, but this has largely been uncredited, so far, by historians and critics.
5. Moody834
10.7.07 / 4am


One of the reasons I love BLDGBLOG is that it introduces me to people like you. After reading Geoff's interview with you I had to come peruse your Web site, and I am very glad that I did. Your work is fascinating to me from an artistic viewpoint, and what you have to say about it is refreshing, intelligible and insightful.


Thank you.


I look forward to reading more of your blog and further learning your thoughts.
7. [plemeljr](http://www.smogr.com)
10.9.07 / 5pm


Regarding Steiner: I agree that in the larger profession his works are slighted or relegated to a minor role vastly disproportionate to his influence. While we studied him at school (Cincinnati) I think Steiner presents a difficult square peg for historians to fit in their round holes; his expressionist work is hard to place – is he like Corbusier or like Saarinen (???) – and he had the disadvantage of building between two major wars and then, as you point out, the Modern movement shifted into the International Style.


As for other outsider architects Christopher Alexander comes to mind, only because his work is met with almost universal disdain when I bridge his name and work. His [debate with Peter Eisenman](http://www.katarxis3.com/Alexander_Eisenman_Debate.htm) while legendary certainly cemented his status as an outsider.
9. [lebbeuswoods](http://www.lebbeuswoods.net)
10.10.07 / 6am


The Christopher Alexander reference is very appropriate. I remember his work from the 60s, in my first years out of the school of architecture at the University of Illinois. His “Notes on the Synthesis of Form “and especially “Community and Privacy: Toward a new architecture of humanism” single-handedly created the Design Methodology movement that engulfed many schools in the late 60s and early 70s. Its main features were the rejection of the architect's creativity as a basis for design and a new emphasis on a design method following Alexander's theories. Students were required to produce endless diagrams and charts, instead of plans, elevations, and sections. As one consequence, a mini-generation of future teachers of architecture was deprived of a design education–the impact of that is still echoing in the present. In the Eisenman-Alexander debate of '82, Eisenman should have said it was Alexander who was “fucking up the world,” not the other way around.


Design Methodology was a perfect fit for the counter-culture of ‘68–the rejection of authority in favor of some allegedly neutral process that seemed more egalitarian. Hence, it helped spawn and sustain another parallel movement of the time: Design Advocacy–but that's another story. By the time Alexander published in the later 70s “A Timeless Way of Building” and the first volumes of “A Pattern Language,” those movements had burnt out–along with the counter-culture–leaving his ideas with only a cult following. In the end, they produced, as far as I know, no architecture of consequence. At least Steiner created the Goetheanum II. You can leave behind the doctrines of his “anthroposophy,” and still have a great building that inspires many still today.
11. [miss representation](http://www.missrepresentation.com)
10.11.07 / 4am


A show at the Whitney while alive is a really small circle, don't you think? 


Isn't it more an issue of operating with integrity and rigor withing the type of practice you set out for? The issue of gaming and striving applies withing overlapping circles of accomplishment. Plenty of people get to this town with the sole intent of become noted within, say, the ‘October set', and apply the same amount of self promotion and manipulation as someone who wants to work for Rockwell.


Perhaps it only the term I'm struggling with. I would place Sam Mockbee well on the outside fringe (he too espoused a great deal of personal animus towards the architecture establishment). Hell, Steve Badanes does (though I never particularly liked his work). Since both of them incorporated a critique of design centered to some degree in the process of realization (meaning, who actually builds and how), I think they represent a different wing of your list.


I daresay by the end of his life Mockbee was so much an outsider he no longer even cared about that measure, which might be the most critical qualifier of all.
13. [lebbeuswoods](http://www.lebbeuswoods.net)
10.11.07 / 5am


Sam Mockbee was, indeed, an outsider architect, and I'm glad you brought his name into the story.


I'm travelling today, and will add a comment on him and his work later.
15. [progressive reactionary](http://progressivereactionary.blogspot.com)
10.11.07 / 2pm


Right on with the Mockbee comment. I would also suggest Paolo Soleri for the category of “outsider architect” (although I must say my knowledge of him and his work is rather limited).
17. [lebbeuswoods](http://www.lebbeuswoods.net)
10.13.07 / 2pm


One interesting aspect some outsider architects have in common is their success at creating personal havens, where they can be surrounded by sympathetic spirits, even in the face of the mainstream's indifference. Mockbee created The Rural Studio. Hejduk was dean at Cooper Union for almost thirty years. Steiner created the Anthroposophical Society, headquartered in the Goetheanum. Paolo Soleri created the Arcosanti Foundation in the Arizona desert. It was in that same desert that Frank Lloyd Wright created the western branch of the Taliesin Foundation, which is a model for all outsider havens. In every one of these cases there is a certain withdrawal from the wider world, and a limiting of ties with it.


Perhaps this cloistering is necessary for certain modes of thought to survive, even thrive. When ancient Rome collapsed, Western civilization was saved by monks in remote monasteries, who copied and recopied Roman and Greek texts, which would otherwise have vanished in the barbarian ravaging of Europe that lasted for centuries. But our present situation is not the same, unless we consider the institutions of capital (and their military enforcers) the barbarians. I haven't gotten there, yet, and can't help thinking that the more links we establish and sustain with the wider world, the more chance there is that good ideas will prevail. This means living among sympathetic and hostile spirits alike.
19. [miss representation](http://www.missrepresentation.com)
10.17.07 / 3am


It's an interesting model you posit. I don't doubt that most everyone here doesn't share a generalized humanist outlook, but the cloistered model, while not necessarily elitist, being withdrawn, does speak to an amount of cynicism. Or, rather, the hopelessness of active engagement with hegemony.


My liberal arts creds are weak enough that I wouldn't want to construct a comprehensive theory of universal benefit — there are certainly social theories such as socialism (and, to a degree, some religious notions) that assert individual value without qualification. Maybe I've just gotten cynical. So how to address the issue of creating the greatest good when the evidence we have is that minority seclusion or withdrawal is often the best model of long-term resistance?


In harshest terms, is individualism inherently compromised by capitalism? We seem to lack the tools or arguments of personal sacrifice without promise of return in absence of an overtly religious model. 


A non-sequiter: are we taking the best tack by viewing this through the prism of ‘architecture'. I qualify this because I don't think anyone here is focuses solely on individual structures. Nonetheless, the terminology has some loaded associations. At the same time, manipulating the jargon has its own history of perhaps facile dissimulation. We all see the problem. We are all tired of the conventional arguments. As Deleuze said, we only need to look for new weapons. 


Apologies for the unfocused thoughts. But it's a great pleasure to have this exchange. I've never believed comments would allow for it.
21. smoghat
10.17.07 / 1pm


Fascinating and incredible to see a real set of comments on an architectural blog. Maybe because few people have discovered it, it hasn't devolved yet (as so many of the archinect conversations do)…


I also agree that it's hard to include Hejduk on a list. After all, Education of an Architect was a bible not for us, but for our teacher's teachers. 


And back to LW's question… who would be on the next list of outsider architects? Who are the crazy, the misunderstood, the just plain weird. 


A couple of L. A. names that come to mind. 


form(u)la  

michael fox


Sometimes I think NY's aranda/lasch could be on the list. The camera strapped to pigeons is weird. But then the built work is interesting but not too far out of the mainstream. 


who else?
23. [lebbeuswoods](http://www.lebbeuswoods.net)
10.17.07 / 4pm


The really probing comments are piling up a bit, and I feel as though I'm falling behind! 


Re: Smoghat and Miss R. on the point of Hejduk, I realize I'm sticking my neck out by calling him an outsider architect. The Education of an Architect was a hugely influential book–arguably, it changed the way architects are educated in the US and UK, particularly. But that didn't bring Hejduk's own work back inside–it's still out in the cold today. Compare its status with that of Eisenman's work, or Meier's, or the commercial success of Gwathmey's or Graves'. I do believe that the day will come when it will be embraced as though it had always been recognized as great. But it will take a different time and a different cast of characters to make that happen.
25. [lebbeuswoods](http://www.lebbeuswoods.net)
10.19.07 / 1pm


I want to respond to Miss R's comments carefully—they are complexly intertwined. 


Cloistering is understandable. It creates an environment for thought and protects it—what else is academia? However, links to the perhaps hostile outside need to be maintained, as a reality check. In some of the cases I mention, this was done, in others it was not. For example, I think Wright's work from Broadacre City onward suffered from his insularity; hence, it was never taken seriously by most architects and educators. He never engaged in discussion, just made pronouncements. Compare with his earlier Oak Park work, which was taken by him to Europe and changed the course of western architecture.


On her second point. What is the greater good, and how do architects help achieve it? First of all, by working together. That, however, is anathema to today's architects, who are divided by—dare I say, capitalist?—competition for commissions. Imagine, though, if the best and most inventive architectural minds got together to work on the problem of improving or eliminating slums; or, ways to employ light-weight synthetics in the construction of housing; or, techniques for using digital technology to enable people to democratically be part of the design process; or….. well, there are many challenges that no architect could effectively meet alone, but only in collaboration.


The greater good? It is the good for the greatest number of people. What is good? It's time that architects focus on this ethical question–the most effective way to do that is through communication, exchange, debate. This is not a matter of achieving a consensus, but about bringing the issues to the surface and allowing different ideas of the good to emerge in an open way. 


I think architecture is the perfect prism to view all these questions, because the design (control) of living space engages the fullest range of human concerns. We are not obliged to play the usual games but, rather, the opposite: to struggle for fresh perspectives, understanding, and modes of action. This inevitably requires some stumbling, groping, and ‘unfocused thoughts.' If we only repeat what we already know for sure, we never learn–or create–anything new.
27. [John Young](http://www.cryptome.org)
10.21.07 / 5pm


Ousider architects are inside, it is a brand name of the valiant few who manage to repulse and attract.


“Outsiders” are being coy to allure the mildly deranged, following the lead of ever-seductive Nietzsche. 


Outsider archtitects are hardly unknown, they work diligently to prevent that.


Wright considered himself to be an outsider and forever braffed about it! 


Indeed, what notable architect has not attempted to create distance from competitors with a stance of being out of the mainstream. 


This attention-getting nonsense is taught in schools: the architect as outsider, poor recruits commissioned to defy convention.


To be sure, being outside the collegially-corporate AIA is a laudable virtue but requires no courage at all since professional outsiders are given prizes by the AIA, the more derisive the greater the plaudits, and the miscreants never refuse, for leads to paid work are too irrefusible.


This excludes a handful of architects who use anonymity to hide what they do forever out of sight.
29. [seier+seier](http://www.flickr.com/photos/seier/1244185274/in/set-72157600283667645/)
12.10.07 / 10pm


this may be too obvious, but your reply of 10.19.07 made me think of Der Ring, a collective of individuals who could do siemenstadt together and yet out-finsterlein finsterlein, so to speak, when alone.


when they broke up, some became the new mainstream while others fell from grace to join your outsider architects.
31. youarch\_01
12.17.07 / 11pm


Perry Kulper should be included on any list of current outsider architects. The meticulous depth of his work, and his teachings, have influenced almost everyone to pass through SCIARC in the last 15 years.  

Here is some examples of his work for those who are unfamiliar:


<http://www.1-ab.com/kulper.htm>
33. [ASSASSINATION: Justin Lieberman, Artist Represented by Zach Feuer Gallery and Sutton Lane « the ART ASSASSIN volume 2](http://theartassassin2.wordpress.com/2009/09/16/assassination-justin-lieberman-artist-represented-by-zach-feuer-gallery-and-sutton-lane/)
9.16.09 / 2am


[…] Custom Pre-Fab House. This piece in turn came out of a lot of converging interests. Visionary and Outsider architecture, Tropical Modernism, Jean Prouve, Edutainment programs from children's museums, taxonomic […]
35. [dpr-barcelona](http://www.dpr-barcelona.com)
9.29.09 / 1pm


For those who are still interested in Kiesler project, here is a review of the Endless House:


<http://dprbcn.wordpress.com/2009/09/21/endless-house-frederick-kiesler/>
37. John K
2.5.10 / 4am


Then there's Hundertwasser and his Mould Manifesto! Living and working (as an architect) in Perth,a largely conservative, colorphobic corner of Australia, I find myself yearning for his kind of outsider architecture. Design here is too often shaped by conformity, and by what the owner or developers' letting agent thinks is lettable or saleable. Result? dull waterd down, dumbed down ‘homgenised sameness' (term borrowed from Kim Jones). No wonder we holiday in Asia to fill up on life and colour. With African origins, I am an outsider architect at heart, but haven't yet found clients courageous or eccentric enough…..
39. [bryan cantley](http://form-ula.blogspot.com/)
6.23.10 / 11pm


i think we @ form:uLA fall under the just “plain” weird section, smoghat… 😉


but damn good company  

thanx for the mention
