
---
title: CRITICISM
date: 2007-10-10 00:00:00
---

# CRITICISM


Friedrich Nietzsche said, “I never attack persons, but only ideas, and only after they have become successful.” 


His critics were not so scrupulous about ad hominem attacks. He was labeled a madman (before he actually lost his mind), unscholarly (he never used footnotes), an anti-Semite (his writings condemn anti-Semitism), and a proto-Nazi (the Nazis liberally quoted him out of context), among other choice things. Granted, his concept of the “overman” was easy to parody, as G. B. Shaw did in his play “Man and Superman” (certainly one source of the comic book hero's moniker), or twist into a likeness of Der Fuhrer. His concept of “the eternal recurrence” has the earmarks of a half-digested Hindu precept, though forecasts by modern physicists of the fate of the Universe are not entirely dissimilar. His most often quoted dictum, “God is dead,” is something he never wrote or said. What he said was, “Truly, all the gods are dead. When one old greybeard of a god said ‘I am the only god,' they laughed themselves to death!” Misquoting someone on such an important point of their work is about as ad hominem as it gets. It is certainly not a reasonable form of criticism, by anyone's standards.


What successful ideas did Nietzsche criticize? Well, German nationalism, for one–it was the time of Bismarck and German unification. For another, Christianity. He saw the church as a betrayal of Christ's ethics. And Wagnerism. Wagner's music dramas were the rage of Europe, but Wagner was both a German nationalist and a declared anti-Semite. Nietzsche had been close to Wagner early on, but detested the fawning cult that formed around him later, and the pseudo-Christian stance of his last work, Parsifal. His criticisms didn't win him many friends at the time, in academia or elsewhere, but they laid the foundation for modern thought–and art–to come.


But this isn't a post about Nietzsche. It's about criticism. And especially, criticism in the field of architecture. Nietzsche's standards were very precise, but also very tough to live up to. He asks us to be fearless and, even harder, to be honest–in public, or at least in print, which amounts to the same. He asks us to risk offending people we might like or respect, colleagues who take any criticism of their work very personally. Nietzsche considered honesty the truest sign of respect. Most of us don't feel that way. We just want the good news. 


Criticism as we think of it today didn't emerge until the 19th century, with the rise of the middle class and the newspapers and journals they needed to give them an edge in cultural matters, which had formerly been the province of the aristocracies needing no critics to tell them what to like or reject. The aristocrats, and royal families, had made ‘taste' by simply commissioning the art, music and architecture they liked. It was up to the artists, composers, and architects to invent new ways to attract and enchant their patrons. No critics were needed.


But the merchants and factory owners of the late 18th and the 19th centuries were not so self-assured. Emerging from a lower social stratum solely by virtue of their new wealth, they, as the new aristocracy, needed help in knowing what to like. Enter the critics. The art, music, theater, literature, and architecture critics were the avant-garde of the culture of experts who have come to dominate bourgeois society, right up to the present. The expert critics were, and remain, the middlepersons of culture, in all its aspects. They mediate between the artists and the public. They make art intelligible–and digestible–to a public who considers the arts somehow important, but doesn't have as much leisure time as royals to actually listen and see what's available, and make choices. Hence, the power of the critics, who are paid to have the time, and of their written (for newspapers, magazines, blogs) and spoken (for radio, television, podcasts) pronouncements. They decide what's important. 


But who are the critics? Where do they come from? What is the basis of their expertise? Whatever it might be, it is certainly not from being architects. The critics are a separate class. Architects are never critics. And why not? Because architects do not feel comfortable criticizing their colleagues. “Judge not, lest ye be judged,” goes the old Biblical admonition. So everyone judges the critics, and usually not approvingly, but the critics don't care–they do the dirty work, and have the power to shape opinion and, in effect, write the first page of new history.


There is another reason architects are loathe to criticize other architects. They don't want to appear jealous, if the work they criticize is more esteemed than their own (“Sour grapes!”), or mean-spirited if it is not. So, what's to be gained by writing criticism?


Two things, for sure.


Most importantly, the development of critical thinking that can be useful in one's own work. Analyzing (that's what writing is, one way or another) exactly what it is we like or dislike about a work of architecture inevitably feeds back into evaluating our own efforts. Self-criticism is a discipline vital to creative work.


Secondly, if one chooses to publish criticism of ideas or works of architecture, it creates discussion among architects about what is important in architecture today. What are the important questions to be asked? Which answers work, which don't–and why? Which colleagues do we share crucial interests with, opening possibilities of correspondence and cooperation? So much lip service is given to architecture being a collaborative effort, yet architects are so busy competing with each other for commissions that they isolate from one another. 


One consequence of that isolation is in the broad sense political: the profession of architecture is weakened in the social scheme of things. Architects are thought of not as principled professionals, but as businesspeople, competing like everyone else in the marketplace. That's all right, if we think of architecture as the making of commodities, subject only to the vicissitudes of fashion and taste. But it's a disaster if we think of architecture as a field of knowledge put into practice. The creation of knowledge involves occasional flashes of inspiration, but a lot of slow and steady critical thought between. And it is always a collaborative effort.


I'll close this post with a comment on making criticism of one's own or others' works. There are ways of criticizing that are harsh, brutal, and destructive. These are, needless to say, counterproductive. But also there are ways to criticize that are honest and direct, but emphasize what is valuable in a given work, leaving what is not to simply fall away. Einstein's critique of the “aether' concept in his first Relativity theory was in simply not mentioning it at all. Thereafter, no one else did either. Generally, criticism should show the way forward.


LW



 ## Comments 
1. [sandrine von klot](http://www.strategies-research.ufg.ac.at)
10.10.07 / 9pm


Hi to LW,


I feel glad to have found this blog.  

I will be very curious reading about architecture as a current ! field of knowledge put into practice, about its collaborative aspects and effects…good to know you are there insisting on reasonable grounds.  

Sandrine
3. [miss representation](http://www.missrepresentation.com)
10.11.07 / 4am


Surely this isn't a sidelong dig at Sorkin (and hopefully is read as the joke is was intended as). Perhaps as significant is who *publishes* criticism. Hopefully the extant web efforts can collapse the gap that has prohibited architects from being critics due to some of the other structural limitations that still exist in quarters (journals and magazines dependent on materials and furniture ads being far too timid, newspaper editors facing too much pressure to increase profitability, etc).
5. [lebbeuswoods](http://www.lebbeuswoods.net)
10.11.07 / 5am


Surely not. Michael Sorkin was the greatest architectural critic of my time, no doubt about it. But, when he made a committment to his own architectural and urban planning practice, he turned his critical attentions to wider social issues, in books such as “The Next Jerusalem” and “Against the Wall.” 


I'll have more to say about the distinction when I'm not in transit, as today.
7. [progressive reactionary](http://progressivereactionary.blogspot.com)
10.11.07 / 2pm


Miss R – I too was going to bring up Michael Sorkin, particularly in the context of the previous “Outsider Architecture” post. He seems to be the most effective critic from *within* architecture in recent memory, yet I think in many ways he can be considered an outsider in terms of both critical and professional practice. Who knows, maybe the most effective “outsider” is the one who can actually slip inside and work from the inside-out?
9. [normaldesign](http://www.normaldesign.com)
10.13.07 / 10pm


very promising blog! – thanks lebbeus,  

at the risk of being pedantic: a quick correction on the god-is-dead quote:  

Als Zarathustra aber allein war, sprach er also zu seinem Herzen: “Sollte es denn moeglich sein! Dieser alte Heilige hat in seinem Walde noch Nichts davon gehoert, dass Gott todt ist!” – (thus spoke zarathustra, end of second chapter)
11. [lebbeuswoods](http://www.lebbeuswoods.net)
10.15.07 / 12pm


Thank you, Matthias, for this quote.  

The English translation, I believe, is: “When Zarathustra was alone, however, he said to his heart: “Could it be possible! This old saint in the forest hath not yet heard of it, that GOD IS DEAD!”
13. [rolando rojas](http://www.rolandorojas.blogspot.com)
11.4.07 / 4pm


Dear lebbeus, let me speak to you on a subject that concerns the concepts of complexity and minimalism, as in my country, Chile, the image of minimalism has become very popular, so that ten years ago was only doing simple boxes and luxury after the economic development of capitalism in this country … Today has begun to suggest that the sum of many smaller totalities equals minimalist shapes of different scales.  

The question is: if the purpose of the complexities (as a response to the simple and luxurious) is complete in greater simplicity … Is it a matter of scales?
15. algabus
11.4.07 / 6pm


Open closed, inside outside, It's very clean to think this way but I'm not so convinced. Everything permeates everything: loops through filters feedback permutations of a boundless dimension. God is dead and subjugation is an illusion. It's a local issue, we feed on each other, reflect, refract, and perform all the incredible physical properties of relationships: the social order….Michael Sorkin seems like a great man…and Lebbeus you certainly appear the same.
17. David Bowman
1.9.08 / 5pm


Nietzsche did, in fact, write “God is dead” in at least one place. Here is a quote from *The Gay Science*, taken directly from my copy of *The Portable Nietzsche*:


*“Do we not hear anything yet of the noise of the gravediggers who are burying God? Do we not smell anything yet of God's decomposition? Gods too decompose. God is dead. God remains dead. And we have killed him. How shall we, the murderers of all murderers, comfort ourselves?”*
19. [lígia milagres](http://www.flickr.com/photos/ligiamxm/)
2.14.08 / 6pm


interesting text…i´m thinking about the effects of an expansive architectural criticism, even if it's a negative one.
