
---
title: PITCH BLACK  Hernan Diaz Alonzo
date: 2007-10-10 00:00:00
---

# PITCH BLACK: Hernan Diaz Alonzo


[![Peter Noever, Hernan Diaz Alonzo](media/Peter_Noever,_Hernan_Diaz_Alonzo.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/makopng2.jpg "Peter Noever, Hernan Diaz Alonzo")  

[![Andreas Kristof, Peter Noever, Lebbeus Woods, Hernan Diaz Alonzo](media/Andreas_Kristof,_Peter_Noever,_Lebbeus_Woods,_Hernan_Diaz_Alonzo.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/makopng1.jpg "Andreas Kristof, Peter Noever, Lebbeus Woods, Hernan Diaz Alonzo")  

It is Wednesday, October 10th, and I am in Vienna for another day before returning to New York. The reason I am here is that Peter Noever, director of the MAK (Museum of Applied Art) invited me to make a speech at the opening of the exhibition “Pitch Black: Hernan Diaz Alonzo,” last night. In some parts of Europe, the noble practice of such speeches is still maintained, perhaps now in opposition to Americanization, which means that such an event is uncommenerated by speakers, toasts and the like…it just happens. Peter Noever, avant-gardist though he is, and intent on celebrating young talent, is ‘conservative' in the very best ways.


The opening event was happening in the MAK Gallery, an underground (in many ways) space in the majestic old museum building. It was very crowded, mostly by younger people, who had to carefully thread their way through the matrix of “spiders,” as Hernan calls them–the delicate objects comprising the installation he and his colleagues have made. It was warm in the space, a bit too warm, but everyone was in good spirits. 


Peter Noever, a remarkable looking man with close-cropped gray hair and intense black eyebrows, and who speaks, alternately in German and English, with a sharp, distinct voice, introduced Hernan and then me. Considering the conditions, I made a fairly short speech, only about an hour and a half…..just kidding….about five minutes, if that. Here is the gist of what I said….


I said that in seeing this exhibition we are confronting a leading figure in a younger generation of architects who are using the computer to explore the limits of architecture and create scenarios of where architecture can go, if driven by digital technology. I criticized Peter Noever (to sotto laughter) for mentioning Hernan's better known colleagues in this quest, as influences or reference points, because I believe Hernan stands on his own legs and should be seen as he is.


I said that as we can see in the exhibition that architecture can go very far by these means, to places we like, or don't like. Hernan is fearless, in this respect, because he pushes his ideas to their ultimate consequences.


What we see here is an architecture that at first seems sparkling and pretty, almost decorative. But very soon our perception changes and the objects and forms morphing in the videos begin to seem dark, for all their dazzle, threatening, even sinister. It is not the gallery that is “pitch black,” but the work. We are confronted with architecture as an autonomous life-form, reproducing itself in rapid mutations that are beyond human control, beyond, perhaps, human intention. An architecture without people, that does not need people.


But wait!


Does any architecture–any great architecture–need people, except to lounge about and give it scale? Think here of great Modernist architects, for starters. No. Great architecture (and I am not thinking only of big, monumental buildings) is always the embodiment of a great thought, a great idea, one that is bigger than a single building or architect. Great ideas belong to a time, to a culture. Great architecture has no interest in or obligation to the expectations or demands of people. It is people–ourselves–who must learn how to live in it, if we choose, or dare. That is the reality, and has always been. It is moving forward–living–by leaps, not steps.


I said that I believed Hernan aspires to great architecture, to the embodiment of great ideas. And why should he not? The changing times demand it. Whether the ideas of self-organization and algorithm-driven self-reproduction are great ideas is being discussed even now. We shall see. But we do not have to wait for the outcome to be grateful to Hernan for his unflinching explorations in this work that inspires us to get involved in the debate.


I closed by saying that we should make no mistake: we are here at a leading edge of architecture. It cannot be ignored, or passed over lightly.


LW  

[![09hernandiazalonso.jpg](media/09hernandiazalonso.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/09hernandiazalonso.jpg "09hernandiazalonso.jpg")  

[![11hernandiazalonso.jpg](media/11hernandiazalonso.jpg)](https://lebbeuswoods.files.wordpress.com/2007/10/11hernandiazalonso.jpg "11hernandiazalonso.jpg")



 ## Comments 
1. [progressive reactionary](http://progressivereactionary.blogspot.com)
10.11.07 / 1am


I wonder what your thoughts are on the political dimension of Hernan's work… I too am drawn to Diaz Alonso's formal experimentation as something truly *new*, yet in many ways his work is symptomatic of a much larger a-political ambivalence that characterizes much of the up-and-coming generation to which he belongs. I'm referring specifically to those interested in “ideas of self-organization and algorithm-driven self-reproduction” and how these ideas can translate into architectural form. The work is thrilling for its translation of these ideas into geometry, but in many ways it seems to me an architectural retreat from applying such ideas to reality, to society, to progress.


Maybe the formal experimentation comes first, and progressive or revolutionary (dare I say the word) content will follow. Or maybe the formal acrobatics on their own stand as a political statement just by virtue of their sheer newness. Regardless, I've always found Hernan's work (and that of his comrades) politically noncommittal – I'd be interested to see what you think, since much of your own work has managed to project forward both formally and politically.


PR.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
10.11.07 / 5am


Well, you raise some profound questions, and I will post my reflections on them when I'm not in transit, as now.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
10.13.07 / 1am


I share the observation that the coming generation of architects seems politically detached. Except for the fact that everything, even pure formalism, has political implications and consequences, Hernan–as far as I know–doesn't say who his work is for or where the money comes from to build it. (footnote: his work is rendered ‘as though built,' more than implying that full scale construction is its intended endgame.) One thing is that it is very expensive and the product of the highest technology, so it's probably going to be built by the military as an experimental environment. That's realpolitik, for sure. But here's an antecdote….


When, in the 60s, I brushed up against the Biological Computer Laboratory (an important, free-wheeling think-tank at the University of Illinois in the 50s and 60s, largely responsible for the development of cognitive science, cybernetics, systems theory) I observed that it was extensively funded by U.S. Air Force grants. No immediate application for the Air Force, but someone in their organization knew that what people like Heinz von Foerster, Ross Ashby, Humberto Marturana and others were up to just might have implications for future conflicts, affecting the balance of military and, hence, world power. So they put their money into it–a kind of political gamble. This is still going on today, with DOD (Department of Defense) research grants in far-flung fields like literature and morphology. 


My question is: why is the military establishment so much smarter than the academic establishment, or professional establishments, when it comes to sponsoring new thinking and techniques in diverse fields?


But, back to Hernan, and his self-organizing, computer-generated architecture.


I don't think he is working under DOD grants, but his work amounts to a form of basic research in architecture, in the design of space, in digitally directed transformation of environments. The political dimension is there already, even if he doesn't acknowledge it. Only the military can afford it, for now. And only the military-industrial complex could produce it, as a realized architecture. Basic research, like his, has future applications we–or he–haven't yet dreamed of. 


One question: should part of his presentation be a more explicit statement of what he believes his work should be applied to, what principles it should serve?


I think it should.


That won't prevent the military from co-opting his work for some future battlefield purpose, but it will set the tone for how the rest of us should approach, understand, and critique it.
7. aes
10.15.07 / 4am


It seems to me that what left outsider architects like Mockbee and Hejduk “out in the cold” was to some degree directly related to their insights, in architectural form, into the politics/sociology/urbanity of their time, and the corresponding harshness with which society walled them out. They were brave and original enough to baffle both the developers and the MoMA-marketed “academic” circles. Also it seems to me that Diaz Alonso's silence on these more difficult issues is a deliberate one, designed to lead him directly towards the path of the ultimate ‘insider'. Give the man a MoMA show.


On the other hand: it's not hard to see your influences on his work, Lebbeus, as well as those of Hejduk (and his troupe of creatures), Eisenman, etc; and I'm reminded of Rowe's introduction to Five Architects, where he criticizes the Five for abducting a formal architectural language while abandoning the social program that was once intertwined with it. And certainly as precise as this critique was, it was to a degree irrelevant and blind to newer, unforseeable ideas that were germinating in those early houses.


All this to say: what's germinating here in Diaz Alonso's work that we're missing with our outdated tools of critique?
9. [Arman](http://www.nascentarchitecture.com)
10.16.07 / 1am


I want to take this opportunity to first congratulate Lebbeus on his new website. I am very excited to see it up and running and long may it continue to do so. Once again congratulations.


Back to the matter at hand and I want to start by saying how fascinated I am by the avid interest and lively debates that Hernan Diaz Alonzo's work is inciting and not, if I may add, only in the context of this thread.


So far I am very interested in many of the complex issues raised within this thread and I can see that there is plenty of room for manoeuvre but I am personally left rather perplexed when it comes to Hernan Diaz Alonzo. 


I certainly agree with Lebbeus when he states that “…Hernan stands on his own legs and should be seen as he is”. And one gets the distinctive feeling that Alonzo would not disagree with this statement, especially when he is described on his own website (<http://www.xefirotarch.com/>) as being “…one of the most influential voices of his generation”. A rather bold, and may I say, strident statement when you consider that there are others of Alonzo generation – Marcus Novac and Felix Robbins to mention a couple – whom I consider to be pushing further and harder at the boundaries of the digital revolution while reserving the right to being the ‘voice of a generation'.


Personally I am rather alarmed, if not put-off, by the idea of an architect self-titling oneself as the' voice of a generation'. Not even the likes of Le Corb or Koolhaas exercised this level of bravado when they began their careers. They may have thought it at some point but they certainly did not voice it and I don't think to this day Koolhaas would utter such words, no matter how ironic.


So what does this mean? Is this just harmless banter to make us stand up and take notice? One could of course put forth a valid argument for Alonzo as being the culmination of the best of the rest of the other digital architects that swim the sea of processing possibilities and algorithmic uncertainties. I am very eager to hear this counter argument and be swayed from my current position, but I fear that this may be easier said than done.
11. [lebbeuswoods](http://www.lebbeuswoods.net)
11.2.07 / 2pm


It's been two weeks or more since aes posed the question, “what's germinating here in Diaz Alonso's work that we're missing with our outdated tools of critique?” It's taken me some time to think about it.


Our existing critical tools (honed over a few millennia) are very useful when discussing concepts of ‘the human' and ‘the natural,' but are not so effective when confronted with “the cyborgian.” We are on the threshold of an entirely new species of creatures, who are neither human nor natural, in fact, neither alive nor dead by our present standards. Arthur C. Clarke once called them (the cyborgs) silicon-based life, as opposed to carbon-based life. Given the advent of digital computation, microchips, nanotechnology, implants, bioengineering and even newer technologies, the cyborg revolution is already underway, and Hernan Diaz Alonzo, among others, is a part of it.


Our critical tools, when approaching such work, need to take this seriously into account.
13. yoarch\_01
12.17.07 / 12am


Three quick blubs-


As a young Architect it is hard for me to get a grasp on where Hernan's work could lead.


-The way Hernan speaks about his work is very similar to the work its self. Aloof. I have heard him refer to his work, off handedly, as “amateur porn.” His work has a quality of the unfinishedness which makes it difficult to critique because it can morph into a variety of different conversations. This usually creates an interesting architectural situation but the looseness in his work actually hampers any critical discussion. There is a lack of rigor involved which Hernan uses to his advantage. For example, his installation at SFMOMA consisted of 5 broken stereolythograph models and an amorphous blob with white sticker labels delineating the program. It was a serious breakdown of Program to From methodology. The quality that is making the work relevant, which was pointed out in an above message, is its newness. Quite honestly, anyone who has been to SCIARC, Columbia, of the Bartlett recently (to name a few) have become all too familiar with this typology of work. His work has already been passed over by many of his students who has pushed farther into the realm of creating complex biomorphic structures fitting of Phillip K Dick. 


-It is a hard for me to connect any of his work, or any of the other similar practices, to current experimental science because this is a type of architecture that is directly linked with formal structure governed by a computer program (Maya). There is no creation outside the parameters set up by AutoDesk. Formally this work lies more in line with late 1960's and 1970's scienfiction but it is a big leap to categorize this work in terms of the biological revolution that is occurring now. It is immense to image a true biomorpic structure where single cells can be grown into skins but that's off topic.


– The bottom line of this work is to create forms based on animating a shape over a set path. There is no real play on algorithmic production because the computer can work it our for you. Hernan's work has only formal aspirations based on finessing a computer program. 


I am curious what this early experimentation can become but to me it seems formally stagnant right now.
15. [lebbeuswoods](http://www.lebbeuswoods.net)
12.17.07 / 3am


I'm more than glad that you're continuing the Hernan conversation. Your comment is packed. I have to confess that I'm not competent to respond about the technical and creative points you raise so articulately. That is, I'm out of touch with the most advanced developments in 3-D modeling a la Maya, in the schools or elsewhere. Still, it's obvious that Hernan has had the exhibitions at SFMOMA and the MAK, so he occupies a highly visible, hence authoritative position in the domain of computer experimentation. If he is already behind the creative wave, this makes his position even more compelling and controversial. 


His offhand descriptions of his own work, and his lack of commitment to a social-ethical program, are so very typical of many gifted people presently on the architecture scene. One needs only to think of Zaha Hadid, Frank Gehry, and other leaders of the field. Their apolitical and, we must say, amoral example, and success, set the model. Leave it open. Don't take sides. Be a genius. Get the big job.


For me, the idea of an auto-generated architecture–however advanced or archaic in technological terms–is the central factor in the work Hernan presents. Retro sci-fi or not , we're looking at a new alliance between developer- and computer-driven design and construction that will be shaping our near-future world. Hernan is part of that story. If we're happy with it, let's let it roll. If not, we've got to be very precise about what we don't like.
17. [serapahtar](http://www.youtube.com/watch?v=-Ay8ckbvJXw)
12.19.07 / 10pm


I think that both your efforts, Mr. Woods, and Mr. Hernan Diaz Alonso's efforts to push the boundaries of architecture should be applauded. I only wish that you both would be awarded large architectural commissions in which you would have full creative license and control to build as you see fit. It would be interesting to see also what would be the results of you, Mr. Woods, using latest in 3D computer modelling and rendering programs and what Mr. Alonso Diaz would produce if he were to do use hand rendering and conventional physical models as mediums.


About architecture being apolitical, I was wondering if this is a precise thing to say. Is not all architecture intrinsically political? Polarizing people into two camps- those who like it and those who don't. Polarizing people into societal castes between those who can live, seek recreation and work in affluent surroundings and those who either live in shanty towns or are homeless? The untouchables and human detritus of society.


About both of your work, indeed any work, could it be importantly asked as to whether or not considerations of embodied energy and sustainability were integral to the aesthetic explorations?


How about an architecture that not only expresses physical and natural phenomena such as earthquakes but seeks to ameliorate the damage wrought by human development (and you proposed the idea of an architecture that generates miniquakes to nullify large destructive ones). An architecture that could repair the ozone layer and reverse global warming? An architecture that could reverse erosion and destruction of arable land?


Another article discussed the importance of professionals being ethical and the difficulty that arose once the government ruled that professional services would be priced on a free market basis same as for products. But addressing the conditions of social inequality and the importance of environmental stewardship could should and should be the priorities of an ethical professional.
19. lebbeuswoods
12.20.07 / 1am


I must demur when it comes to the idea of large scale building commissions, but the idea of what I would do with computer 3-D modeling, or Hernan with a pen and paper, is fun to think about.  

Even better is your thought about an architecture that repairs damage to the earth and atmosphere. Sets my imagination working. The idea of a community of people working on the projects you mention, where and how they live, and the instruments they use to actually repair….  

Your comment about the priorities of an ethical professional are very fine. I think most people want to do the right thing–it's not always easy to know how.
21. some guy
1.6.08 / 11pm


i saw Alonzo speak at Rice University last spring. he was just a big kid, like a frat boy, making jokes about porn and rape, while eating up the adulation poured upon him by the pretentious Rice staff. i admit, he made me laugh. but his “architecture” was just a horror movie set. it's a dead end. it doesn't solve any problems whatsoever, except for the “problem” of form, which is sort of like solving the “problem” of your clean apartment by making a big mess.


on a totally different note: Lebbeus Woods is blogging? awesome.
23. [studentwithoutaclue](http://www.uky.edu/cod)
1.16.08 / 2am


As loathe as I am to comment on a thread that is months old and quite probably dead, I feel like I have had something on my chest for quite some time in regards to Hernan.  

I'm a student as the University of Kentucky, and thus my opinion is sophmoric at best, but being in my current situation I am probably more perplexed than most of you advanced architects, theorists, and critics. I have friends who worked on this particular project (Pitch Black) through the UK COD Manufacturing Lab and they have attempted to explain to me Hernan's process, but as of yet it seems evasive and extremely unprofessional.  

He famously equates his work to Salt… A little salt in food makes the food taste better, but alot ruins the taste. Similarly a little Hernan is good I guess. But this seems self effacing for someone who claims to be one of the leading voices of my generation. A professor of mine told me a story of a dinner he had with Hernan where questions regarding the relevance of his architecture were being asked. Hernan responds by removing his ever present cigar pausing for dramatic effect and then saying in the most arrogant, and infuriating voice, “I never said this was architecture.” How can one seriously critique in any intelligent manner this sort of response? I just wonder how it is that Hernan can be taken seriously as an architect/ architectural theorist/ artist… whatever he is… if he can't even really take himself seriously. Maybe I'm just missing something about his work… I also wonder how his work is any more stimulating than many of his computer driven contemporaries (Ali Rahim, Jesse Reiser, Stan Allen, Jeffrey Kipnis, etc.) or if it is even presenting anything different that Greg Lynn, Peter Cook, and Eisenman hadn't already beaten into the ground.  

I conclude by saying that we have been at the leading edge for a number of years and people are slowly making that edge quite dull. Are we all obsessed with Hernan because of his dinosauric pretense and ambiguity?
