
---
title: TAKING A POSITION
date: 2007-10-20 00:00:00
---

# TAKING A POSITION


We do indeed live in a dry time for theories of architecture. It's as though we've reached the ‘end of history,' proclaimed, in 1992, by Francis Fukuyama: “What we may be witnessing, “ he wrote, “is not just the end of the Cold War, or the passing of a particular period of post-war history, but the end of history as such: that is, the end point of mankind's ideological evolution and the universalization of Western liberal democracy as the final form of human government.”


An astonishing prospect!


Architects, often the hand-maidens of politics, today seem more eager than ever to play the main game of liberal democracy, which is the pursuit of clients and their commissions. And who are the clients? Developers, exponents of liberal democracy's main activity: capitalist enterprise. In the age of McLuhan (and Debord) they understand the value of the PR spectacular architecture delivers. Occasionally, governments—who need the same kind of upbeat PR—are clients. The liberal democracies of Dubai (UAE) and Kazakhstan, for example. Or of Bejing, Shanghai, Singapore. That's where the money is. That's where the developers and their architects (including many of the best we have) are. Working feverishly at the end of history.


Theory? Actually, it's excess baggage, even when the architects are flying first class.


Architects are not born theorists, that is true. Most of the world's best architects never wrote a line about their work, let alone proposed a theory—they didn't have to. There were busy critics and professors who followed their works with great attention. Innovative architects were lucky to have their Mumfords, Gideons, and Tafuris, and, more rarely, their Foucaults, Deleuzes and Derridas. The theories that the theoreticians spun around their works enabled a wide discourse to develop, elevating architecture to a form of knowledge, lifting it out of the venal chatter of the marketplace. Sadly, those critics and professors have died, leaving a conceptual—-and critical—-void.


Many of the critics and professors of the present day may be silent about the most recent works for a reason.


The “Bilbao Effect” has dampened critical architectural writing. With its advent, interest shifted from the heady quarrels about Deconstructism and Post-Modernism to a concern with the much less intellectually taxing search for novel forms. Novel forms work so well, from the viewpoint of promoting tourism and other spun-off enterprises. As for the Bilbao Guggenheim, there's not much that can be said about it beyond its great success. It encloses the same old museum programs. If we look behind the curving titanium skin, we find swarms of metal studs holding it up—no innovative construction technology there. It hasn't inspired a new architecture, or a new discourse, other than that of media success. Herbert Muschamp was right—-the building is the resurrection of Marilyn Monroe. Certainly the architect, like Marilyn, hasn't said anything of consequence. Sexiness just speaks for itself, no? Exeunt critics and professors.


I think architects themselves need to take up the task of theory writing, and not wait for rescue from the quarters of academe. That may seem at first an absurd expectation, but I can think of two architects very engaged in building, who have done just that: Rem Koolhaas and Steven Holl. They don't write theory, exactly, but they place their work in the context of ideas, not just opportunities. They take the risk of putting off potential clients. Think what you will of them, their buildings and their books, but they have taken positions vis-à-vis other fields of knowledge, and the contemporary world. That's not only admirable, but imitable. All I can say is, let's have more from others, the architects who, by building, or intending to build, are shaping the world.


LW



 ## Comments 
1. aes
10.21.07 / 3am


Fukuyama's delirious, if brilliant, statements at the dusk of the last century, disguised as a form of optimism, betray to me a stark fear of the future and the unknown (which, obviously, is a neoconservative trademark). This was his own “Mission Accomplished” banner, looking back on the rubble of the Berlin Wall and declaring victory without really looking forward to what lay ahead in Sudan, Pakistan, Afghanistan, and Iraq. Now, of course, he has since somewhat retreated from such delirium (and the neoconservative camp in general) in his more recent, and much more somber, writings in “America at the Crossroads”.


Perhaps architects are so focused today on museums, monuments, and memorials for a related reason: these are classically retrospective programs, usually looking back and furiously praying to be at the end of history, without the courage to look, and theorize, forward towards a new (and more difficult) architecture. This seems to be yet another glimmering facet of the Bilbao effect: political and programmatic convenience, a lucrative alignment between the spectacle and ideological indifference. Perhaps Bilbao is really a spectacular eulogy for architectural optimism, ideas, program, and bravery.
3. [John Young](http://www.cryptome.org)
10.21.07 / 5pm


Putting off potential clients, that has promise as a practical theory for hastening the end of ever-thoughtless AIA, handmaiden of the property hoodlums.


Get the job, get the job: what every architectural client dreams architects will believe, must believe to maintain a practice. What's theory got to do with that kind of practice, well, thanks to the Institue of Urban Studies, it's a marketing gloss, a variation on the pomo theme.


Urban housecleaning of the Philip Johnson era to substitute venal wit for morality, parlaying soothingly shallow intellectualism for dangerously critical suicide, can it be possible? 


Probably not in the corrupt to the maximum big cities but perhaps in the wastelands where architects who came of age under Johnson's baleful influence on Jackie-O-ditsy clientele will need to do penance to purge the toxins of humiliating success at black tiedom.


Fuck Gehry, he brags, as if being vulgar as a property developer means he's not a pussy.
5. [B. Nory](http://www.dustlife.blogspot.com)
10.22.07 / 7am


“It hasn't inspired a new architecture, or a new discourse, other than that of media success. ”  

I believe it did in one way or another. It inspired a new range of possibilities in the realization of architecture. The methods used in buildings such as Bilbao Museum and other similar ones has opened the doors for new experiments in forms that can exist due to the new technologies used. Architects are now “almost” free to imagine the form they want to create and eventually build. Of course, other restrictions apply like budgets…etc. I believe that the discourse of such new frontiers may not be found clear in the texts of theory of architecture but rather in texts and contexts of applications that lead to realizing such complex expressive architecture. We might be in the phase of experimentation of these new field, that is moving fast forward every day. Theories may follow this time not proceed.
7. nucleuswonder
10.23.07 / 10pm


The much used and over used “Bilbao effect” has stripped credibility from those who seek a (any) move forward, to an(other) architecture. unfortunately that is the stage we are at- a pseudo-progression. For now any building that deviates from the ‘contextual' architect's imaginable radius is an ‘iconic' building; a jaw-droppingly poor catagorisation of the thing, pocketed nicely into a box of ‘iconic', a word that i find myself cringing at whilst typing. Paper architects are where I find truth. With these guys who are/ were not plagued by external influences and who Nietzsche could have appreciated for their honesty. There is no commercial compromise here, no underlying economic reasoning, just architecture, which has not been tarred by the world's brush. If we are indeed at the end of history will our nostalgia worsen or will there be a future afterall???
9. James
10.29.07 / 9pm


… architectural schools are largely theory based, its just when we get into the big wide world, we actually have to earn a living…
11. [lebbeuswoods](http://www.lebbeuswoods.net)
10.29.07 / 10pm


James: please go on….surely there's more on your mind.
13. [Christian](http://-)
10.30.07 / 11pm


I have been reading the entries here and I have asked myself under which topic to post. Maybe it is good to take a position for now. I read and recognize a lot of grief and disappointment about architecture and I have to say, since I studied (1997-2005) I have meet just e few architects who were happy with there life / job / passion. I was always wondering why and also tried to understand – also because of being someone who wasn't happy with it also. One major issue that creates unhappiness are outside influences. Contractors, clients, banks, governments etc. Combined of course with the fact, that we can't create a single house without their risky idea to want one. There is a dependence. But also in the real of architecture as culture – as an instance of being human – a position that is of relevance to history can be judged as worth a life of work. A position of a single mind – a single hand. Struggle for a body of work, that people will remember. But is that an aim? I don't know. I a competitive understanding of oneself – “I am the best… ” – maybe. I can't do this. According to that topic I am asking architects where ever I meet them, if they could see them selfs investing there very own money together with 3000 – 5000 others to build as architects. On their own. Maybe it sounds stupid, pragmatic or boring, but the reactions are quite interesting. Mostly all of them think it is not possible. That can't happen. I ask myself why? How in this world, can one fulfill his social and humanistic commitment to the human being if a building costs as much as you earn in a lifetime. Would that independence make a difference? I guess so. In my opinion an architect can be a maker his own ideal surrounding – somewhat like a vision (but understood in a total non mystical way). I have hard times to imagine a better tomorrow. There are a lot of inventions and changes that keep me from that. Summed up in the realization that increasingly a fewer people are able to mess up the lives of a whole lot others. Robotic is just one topic that would belong here. Are we able to take care of that? There is one interesting aspect of our profession that I found unique. By nature the education includes a certain sensibility towards the human and an openness to other cultures. That combined with the exchange of architects around the planet makes “us” a “group” that shares the care for people (in many different variations) across all borders and regulations. I don't know. Maybe that people will be in need of that one day. Somewhat between artist and doctor. Maybe. My vision would be that action to take the responsibility for the human habitat. What would be a six billion city 😉 ? No for real, we just need to figure out how to feed ourself till the end of the month. Give me that and I am free. There was and is a lot argument about the element of competition and survival of the fittest as the best way to make sure great architecture will be created. I do not have an opinion on that, but there are a couple of examples where the architect has died before he saw his later admired work. What a price. And on the other hand does it seem to be much easier to be friends with the latest emperor or bishop or dictator. What a shame is that. What would happen if the architectural history would be written without those who have been build with blood. No pyramids for sure. So the being as an architect till now seems to be the same dependence on the “king” as ever. How may peasants he made suffer for that building doesn't matter. A vision to get away from the king. That would be a position. Thank you for reading. 


PS: Two suggestions for interesting architects –  

Ben Ledbetter (New Haven)  

Toralf Sümmchen (Berlin) <http://analog77.deviantart.com/gallery/>
15. [lebbeuswoods](http://www.lebbeuswoods.net)
11.2.07 / 12pm


Christian has several ideas that respond to James' rather resigned attitude. Architects can take independent action by 1) by forming a community of like-minded people and building for and with them; 2) designing one's own environment according to one's own vision of architecture and living; and 3) simply avoiding entanglement with the “kings” who exploit anyone and everything to increase their own power and wealth, in order to pursue ideas that matter. These are quite practical (though not easy) ways to live and work that enable the architect to act on his or her principles.


However, the hardest part by far is forming one's own principles. It takes time, much thought and self-criticism, a lot of effort to write or otherwise express them clearly enough to stand up to the constant pressure to conform to others' principles. 


It can be done.
17. sergio machado
11.2.07 / 11pm


Dear Mr Woods: initially I didn't intend to post this text here. So it refers to you as “Woods”. Sorry for the impersonality.


“It is only shallow people who do not judge by appearances. The true mystery of the world is the visible, not the invisible….” Oscar Wilde 


Lebbeus Woods took few words to take a position, and this forces us into a temerary and incert job of interpretation. Let´s start from the extremes: Woods denounces the Bilbao Effect and criticizes the architects that may be producing architecture on a less intellectual environment. At the same time, calls on these architects to follow in the steps of Koolhaas and Holl, explaining their theories. If he is so sure about the bad results, why do ideas matter? 


What does Woods intend? Today's production of architecture is forcing critics to talk about form. But this does not imply that there is a detour from the real theory. The opposite would be more reasonable: not discussing the architectural form, is a detour from reality. Anyway, it would be necessary to define better what we are talking about when we use the word theory. A project is a theory too. 


Mentioning Bilbao's Guggenheim, Woods states that “there's not much that can be said about it beyond its great success”, and blames the project for its conventional program. I agree with him that programs are a crucial point of innovation and progress to architecture and I think architects have the proper conditions to contribute in its formulation but, once the program is established, the project must dialogue with it: the reference is posed. It's not fair to justify poor solutions because of program, neither to judge a project only in these terms. 


In another point, the technology is questioned: “If we look behind the curving titanium skin, we find swarms of metal studs holding it up–no innovative construction technology there.” Well, if we look closer to those curving elements, they would have much to say about construction techniques, insulation methods and so on. On the other hand, it seems to me that it is inadequate and old-fashioned to defend the technological innovations as a quality standard. 


So it is old fashioned too to say that Marilyn Monroe limited herself to her sex appeal. Moreover, it reveals poor understanding of movies, photography and women. The diva expressed much more than sensuality through her gestures and looks: she was such a great performer that she is criticized as if she were her characters. 


Woods also points out that architects don't write anymore, leaving this hole to the critics and mentions two, as example of integration between practice and theory: Koolhaas and Steven Holl. We could discuss the work of both, in another moment, but, in advance, we can say that Koolhas is the product of an intelligent and carefully orchestrated marketing strategy. His competence as a designer is, for me, unknown. The propaganda he makes of the hundreds and hundreds of miles traveling, shows he probably has little time to perform such an absorbing activity, like projecting. This reminds me of John Portman, frequently demonized for his proximity to capitalism: we certainly can attribute the conceptions of his buildings directly to his genius. Who conceives the OMA's work? 


Finally, I'd have to say that Woods' position is also a great contribution and sends a warning to architects, calling them to take the lead in the architectural discourse that represents the profession, taking off the hands of critics and opportunists, this inadequate prerogative. 


Sérgio Machado. architect
19. aes
11.3.07 / 2am


When Le Corbusier remarked that he learned more about the Italian Rennaissance from the architecture of New York City than that of Italy, he was, with considerable irony, critiquing the hollowness of form, no matter how splendidly perfected, when emptied of ideas.


Louis Kahn: “Paestum is beautiful to me because it is less beautiful than the Parthenon. It is from it the Parthenon came. Paestum is dumpy– it has unsure, scared proportions. But it is infinitely more beautiful to me because it represents the beginning or architecture. It is a time when the walls parted and the columns became and when music entered architecture.”


And then, of course, there is this:  

<http://www.imagereferencedatabase.com/myadmin/photogallery/photo-37-37-182-0-Yes.html>  

An exact replica of the Parthenon built in Nashville, with all the friezes filled in and the colors restored. It is in the absence of critical ideas that such a thing is built, not in the absence of form.


And when radical manifestos arise, such as Bramante's Tempietto or Corb's Maison Domino or Kahn's Trenton Bath House, they do so as dialectics between idea and form, between theory and practice.


Lastly, I would actually argue that Bilbao was indeed a small manifesto, though a big building. And perhaps it is unfair to blame the Bilbao effect solely on Bilbao. We are told that Mies, towards the end of his life, complained aloud as he looked out his office window overlooking the Chicago cityscape: “We showed them what to do. What the hell happened?”


What would Bramante think of Stanford White? Or Frederic Kiesler of Greg Lynn?
21. sergio machado
11.3.07 / 12pm


Maybe we could talk about what is the Bilbao Small Manifesto: It seems more fecund than “effect”.  

For now I'll add that Gehry is an artist, playng with computers as a tool. Lynn is a boy who doesn´t know exactly what to do with that tool and has no control of it.
23. aes
11.3.07 / 4pm


Somewhere or other Michael Sorkin has written that the genius of Bilbao was Gehry's ability to literally build the squiggle/sketch/doodle. I remember this to be an astute observation, since it speaks both to Gehry's strengths and weaknesses. The strengths are obvious: the compaction of artistry and technology, the uncompromised form. The weaknesses are also plain: what makes a sketch a sketch is the disregard for resolution and rigor in favor of the gesture; similarly, the steel beams crashing through sheetrock and the “swarms of metal studs” illustrate the fact that indeed Bilbao is a physical rendering, an illustration, a sketch, and not a “construct.”


But I think Bilbao, and for that matter the Bilbao effect, has been exhausted. I'd much rather pose the question: what are the issues/ideas that manifestos today need to address? What are the problems that architects' theories need to tackle, in a way that Koolhaas took on capitalism, and Holl on phenomenology?


To retreat for a moment from overtly political issues… for me one as-yet unmined area is literature. I wonder if it's possible to break down what makes great literature and construct from those tropes an architecture.


Thoughts?
25. [lebbeuswoods](http://www.lebbeuswoods.net)
11.3.07 / 7pm


I like the idea of a building as a manifesto. If new ideas are not embodied in built (or drawn, or modeled) form, then no amount of written theory is going to put them there. A notorious example of such an attempt was the use of the theories of Barthes and other post-structuralists to justify 70s kitsch like Charles Moore's Piazza d'Italia. No one is advocating this kind of abuse.


What is important is the relationship between an architect's thoughts–expressed in writing or speech–and their presence in a building. I'm glad aes mentions Kahn, because he was very much a thinking and writing and speaking architect. In fact, Kahn's writings—very much a conscious creation of theory, as in his statements on the relationship of design to form—are very evident in his buildings. It is not merely speculative to say that without his theorizing he would never have created the architecture he did. Certainly he knew that, and so should we.


Bilbao is a manifesto without a theory, or even a comprehensive argument. Sorry, but an architect cannot be only an artist. An architect's work begins in philosophy, and an architect needs to be self-conscious and self-reflective in ways artists do not. This is because architecture embraces the widest possible understanding of the human condition. Even Mies, the taciturn genius, was given to seminal pronouncements that conveyed ideas it took others volumes to express.
27. sergio machado
11.4.07 / 12am


Let's put the building itself, aside for a while.  

What is a manifesto? The act of manifest is to make explicit an idea (in fact, originally is explicit it with the hands; from the Latin manus, mani). It has nothing to do with novelty or originality but with clarity, intelligibility and evidence.  

What is theory? Again, it comes from the Latin and it refers to the act of observe, through the speculative reasoning or a Philosophical investigation. So a Theory is a complex of ideas and notions derived from the observation. As K. Michael Hays pointed in a discussion in Harvard (in HUGSD Studio Works 4), there is a distinction to be made, between architecture that produces theory and theory that produces architecture. Extending the notion of theory, and even removing it from the exclusive fields of academy, Hays agrees with Terry Eagleton, who said that “because we are human, because our existence is significant, in that we make signs in the practice of everyday life, we are theoretical”. Further, Hays affirms that, “because what we do is not governed by biology alone, we need theory to organize the sign systems we use”.  

Well, if Bilbao is an evident manifesto and, as a “form of organization of signs”, is effectively a theory, so what we have to do, in order do understand the event, is ask what about this theory is, and if it is adequately explicit.  

I completely agree with Woods when he says that “architecture embraces the widest possible understanding of the human condition”. So, we would expect that a building will embrace many theories. In the case of Bilbao, the most impressive to me are the ideas about the relationship between the urban tissue and the building itself. There is also, thou not necessary, novelty on this issue: there are little buildings that embraces a viaduct, like Gehry's did, integrating a infrastructure of transportation in the design.  

There is a theory too, about materials, about the choice of titanium cladding: it rains very much in Bilbao and the skies are often cloudy, the building catches and reflects almost all photons on the atmosphere. There is another theory about how to access a building, and I don't think that it is successfully in Bilbo Gug. And so on.  

But, maybe above all, there is a theory on how a public building may have to appear to the public, on how architecture should place herself in the general culture, on what is the adequate image to the space and time in case. If we call the Latinos again, in the concept of decorum, or decency, convenience, Bilbao is a respectable and efficient theory.
29. Mafalda Gamboa
11.7.07 / 7pm


I'm quite new here but I could not keep my hands off the keyboard before pointing out some important ideas that are being discussed here.


I must agree with the way Mr.Woods wrote this, we could name it, manifesto. I appreciate his concern on the theory of things.


I am on my second year of my degree on Architecture. And I surely have two stories to tell on teachers and kings.


Starting with a true episode: once a friend told me about a somewhat famous architect here, in Portugal, that used to work for a particular man, some “Mr. Rich”. The architect decided to hire a partner, whom he took to a very important meeting. During that meeting, “Mr. Rich”, while being presented to the original drawings of the architect, started drawing over them pointing out what he expected from the project. Well, the new partner quickly took the drawings out of Mr.Rich”‘s reach and told him: “That was the last time you sketched over these drawings.”. Point is: “Mr Rich” loved the enthusiastic girl because he wasn't used to being faced off, while the architect was about to dismiss her.  

Well, this brings very simple issues: firstly, why do people hire architects if they actually know precisely what they want? Or either, why don't they simply state the limitations of the project and leave the rest to the architect? Why do Architects bend over money, more easily than over a very good idea or a very good chance for a manifesto?


Second thing is… it's in the learning. I am learning to shut my thoughts off. Everyone around my College says that. Theory doesn't matter for grades, so you actually spend 5 years learning to dismiss the concept. My teachers never read text. They say it's all in the looking. Well, I must disagree but it's just me. And if you think about it… where are you teaching people to criticise Architecture? Nowhere. Nobody follows Architecture for there are no rules. I surely agree with the fact that all Architects should be and feel, themselves, responsible. I do. I am scared to look around and realise we're shaping with no true intention but some fake poetry of light.


I'm sorry if this was completely pointless but I had to take it ouf of my mind.


Thank you for your inspiring words.
31. Helder
11.11.07 / 5pm


I have enjoyed reading the text, and all the comments. If all those curving titanium panels on Gehry's museum were some sort of energy producing gadget, a thermal reflector, or some alien communicating system, so E.T could phone home, I would have been more impressed. I think all this formal parade has got everyone confused. A teacher once said in class: “architects haven't got it yet; buildings have to function like the human body” and good looking. a simple phrase can teach you more than a 100 books.
33. Helder
11.13.07 / 3pm


Correction on my text above, It is a thermal reflector.Thank you
35. [Alexandre Mendes](http://www.sleurope.deviantart.com)
11.13.07 / 7pm


(LW edits in [….] brackets)


“the end point of mankind's ideological evolution and the universalization of Western liberal democracy as the final form of human government.”


the universalization of western democracy will bring a proper world right? WE that are here, I think we all live in western countries, right [?], this is a major problem, the universalization, how can you universalize… all the world, they will copy our way of living, and our way of living exceeds what the Earth can give to us. Imagine China and India consuming like the western [countries].


I do not agree with what was said. We don't know what the future will bring.
37. [blogs and minor manifestos « project modernity](http://projectmodernity.wordpress.com/2007/12/27/hello-world/)
12.27.07 / 5pm


[…] “An Anti-Pragmatic Manifesto” [link]; Lebbeus Woods “Taking a Position” [link]. Whether these minor manifestos become statements of a larger program or are lost within the ebb […]
39. [ArchiTakes» Blog Archive » Architecture Meets Science Fiction at 41 Cooper Square](http://www.architakes.com/?p=4140)
12.4.09 / 4am


[…] commercial success of Frank Gehry's Guggenheim Museum in Bilbao, Spain, Lebbeus Woods has written that ”the ‘Bilbao Effect' has dampened critical architectural writing.  With […]
