
---
title: ENDS AND BEGINNINGS
date: 2009-04-02 00:00:00
---

# ENDS AND BEGINNINGS



When an architect such as Anthony Titus makes an installation in an art gallery, it is not an attempt to break into the art world, but a way of conducting experiments that can feed back into architectural thought. A gallery or a museum offers a controlled, laboratory environment where untested ideas can be tried out within a tight set of limitations and, at the same time, engage the public, getting people involved. Some ideas cannot be plugged directly into a building project but must be developed more modestly, and economically. The temporary nature of installations works advantageously for this sort of research, not so much as a way of erasing in preparation for starting over, but because of the intensity and focus imposed by time on both the architect and the public. Later, when ideas or even principles developed in the heat of the moment become clearer, more distilled, they can find their way into building design. But, at their inception, experiments such as Titus' installation are risky, uncertain as to their outcome or potential for more practical uses—and necessarily so.


The pieces in this installation explore the material and conceptual aspects of designed space. Abstract geometrical elements are set up as spatial markers and boundaries, as is usual in architecture, but here in ways that introduce both surprise and anxiety. The elements are treated not as shapes designed to be together, but rather as found elements, in a sense as idealized elements, that can only be brought together spontaneously, creating compositions and spaces from collisions and chance groupings. If one were to project a built landscape from them, it would be closer to that of a favela than, say, Brasilia. The evocative use of different materials, colors, and textures only underscores the impression that we are seeing the far outposts of another world, the meanings of which we must begin to grasp.


Entitled “The light in the window is a crack in the sky,” this installation existed for a month about a year ago. The sky, as we know, is dark. The prevailing mood of the work is somber. The installation is not meant to entertain or distract us. There are dire problems pressing. We are unsure and sometimes afraid. Design—architecture—of this order intends to give us tools we may need on our journey to the light.


 LW  



![lwblog-titus1](media/lwblog-titus1.jpg)


![lwblog-titus3](media/lwblog-titus3.jpg)


![lwblog-titus4](media/lwblog-titus4.jpg)


![lwblog-titus21](media/lwblog-titus21.jpg)



 ## Comments 
1. [SUPERFRONT daily » Blog Archive » Lebbeus Woods on Installations as Architectural experiment](http://daily.superfront.org/2009/04/lebbeus-woods-on-installations-as-architectural-experiment/)
4.8.09 / 10pm


[…] Lebbeus Woods, in one paragraph, sums up my motivation in relating architecture to installation practices – something I've […]
3. Pedro Esteban
10.28.11 / 6pm


thanks more than a question solved with this post


 this is our ”la rue” to light
