
---
title: WITHIN THE WALLS
date: 2009-04-01 00:00:00
---

# WITHIN THE WALLS



In one sense, the town-within-a-town that grew over many centuries within the Palace of Diocletian in Split, Croatia, is an archetypal gated community. Bounded by the walls of the original Roman palace, built around 305 CE, the town grew slowly and steadily through centuries of political, cultural, and religious change, each of which left its architecture in the town's landscape. The perimeter walls remained constant, as did a few key buildings. It was basically an infill process, with new buildings constructed during the early Christian era, the Middle Ages, the Renaissance and so-called modern times, being added to the ever more dense fabric. There was also the continual remodeling of earlier buildings to meet changing demands for contemporary living. It was an organic form of transformation, like many old cities, but one that that ended in the 20th century, when the town was fixed as an historical artifact and an object of international tourism. There is no contemporary architecture. From the standpoint of the spirit of urbanism that it embodies, this is a great pity, because it has become sacrosanct as a unique work of art and a document of history and can no longer serve as an active laboratory of urban building and living. Considering, however, the propensity of Modernist architecture for erasing the past, and of post-Modernism for fashionable kitsch wherever it can be imposed, we should probably be grateful for the small town's ossification.


Today, walls that separate one community from another are considered to be problematic, or worse. Gated communities, planned as safe havens for the wealthy on the turbulent landscapes of urban diversity and conflicts between social-economic classes are anathema to well-intentioned egalitarians. Still, this bounded town shows us that walls can also serve to concentrate energies, social and architectural; that the dense layering and juxtaposition of differences and the continual rethinking of common space, its premises and purposes, can lead to admirable, even imitable—in principle—urban form.


 LW


**Diocletian's Palace in Split, Croatia**, c. 1970:


![lwblog-dio-1](media/lwblog-dio-1.jpg)


 **Diocletian's Palace in Split**, *reconstruction from known surviving parts* (in black):


![lwblog-dio-3](media/lwblog-dio-3.jpg)


**Diocletian's Palace in Split**, *composite of existing buildings*: Black (upper layer): known surviving parts of the Palace, 3rd century CE; Black (lower layer): 8th-15th centuries; Outline: 16th-17th centuries; Cross-hatched: 18th-19th centuries (additions and remodelings):


![lwblog-dio-4](media/lwblog-dio-4.jpg)


![lwblog-dio-5](media/lwblog-dio-5.jpg)


*Within the walls*:


![lwblog-dio-6](media/lwblog-dio-6.jpg)


![lwblog-dio-7](media/lwblog-dio-7.jpg)


![lwblog-dio-8](media/lwblog-dio-8.jpg)


![lwblog-dio-9](media/lwblog-dio-9.jpg)



 ## Comments 
1. [Stephen Lauf](http://www.quondam.com)
4.1.09 / 3pm


[“Historical analysis within a space-time continuum is more ongoing productivity and less end-product.”](http://www.quondam.com/13/1250.htm)


It's interesting that while Diocletian enjoyed gardening time within his retirement palace, his wife and daughter were living a fatal fugative existence.


Diocletian + [Eurtopia](http://www.quondam.com/25/2404.htm) = Maxentius?
3. [Michael Knutti](http://cafe-warszawa.blogspot.com/)
4.1.09 / 3pm


The problem with gated communities is not (as it might seem) that they are fenced in, rather that they are an autistic enclave. Muted towards the outside, not participating in the vibrant dialog of the city which surrounds them. Mostly mono-culture on the inside, a pastiche of a superior society where all share the same values. A total absence of cultural diversity, and where there is no mix there is no collision/friction and hence no evolution.  

But maybe those shortcomings are only of a short time, and over the next few hundred years we will see a progressive change. The once mono-all settlements will evolve into new hyper-diocletian palimpsests.
5. Pedro Esteban
4.4.09 / 2pm


It's interesting becuase, the boundaries between architecture and urbanism disapeer. By the juxtaposition of architecture, they do urbanism without planning. That sounds good, as a concept for urbanism.  

Sometimes we need to learn from history. How this can be proof in contemporary life?
7. Matthew Allen
4.4.09 / 9pm


Would you mind giving sources for the drawings? I have been looking at Robert Adam's plans 1757-64 and would like to compare them somewhat objectively.
