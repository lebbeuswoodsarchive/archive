
---
title: [hiatus, again]
date: 2009-04-05 00:00:00
---

# [hiatus, again]


For the next few weeks, I will take a break and collect my thoughts. I do hope regular readers of this blog will return about the middle of May, and any time between now and then, if they (you) want to revisit some of the posts. Until then, I hope all will be well, or at least getting better.


LW


![lwblog-hiatus22](media/lwblog-hiatus22.jpg)



 ## Comments 
1. nikkie
4.6.09 / 5pm


will wait for you.
3. [wmwm](http://www.asasku.blogspot.com)
4.8.09 / 4am


Take care LW.


*I'll shall wait eagerly for your upcoming posting.
