
---
title: SIC TRANSIT GLORIA
date: 2008-04-01 00:00:00
---

# SIC TRANSIT GLORIA


[![Nouvel's Cartier Foundation Building, Paris](media/Nouvel's_Cartier_Foundation_Building,_Paris.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-pritz082.jpg "Nouvel's Cartier Foundation Building, Paris")


Jean Nouvel has won the 2008 Pritzker Prize for Architecture. No surprise. The lack of surprise makes it is easy to view the Pritzker as establishment laurels for those who are already well-established. Like the Nobel Prizes, it is conferred on safe, already certified choices. Nouvel's buildings are certainly of a high quality of design. At his best, he designs beautiful buildings. Who can quarrel? The Pritzker tries to remind us that the design of beautiful buildings within, or maybe—on occasion—closer to the limits of the accepted canon of beauty is the ultimate goal of architecture. People need enclosed spaces, and it is up to architects to design them in ways that satisfy the needs of body and soul. Or, at least, in ways that reassure us about what we already know. Nouvel works masterfully within the limits of what we already know. 


So, is there a problem with any of this? Not at all. I say, let the rich bestow upon the famous whatever they like. Let the rituals of power play themselves out as they always have. It is quite a seductive spectacle. We all become part of it, say, by posting comments like this on blogs. 


But I have to question how relevant the Pritzker Prize is for the expanding world of architecture. Whatever its claim to reward innovation and expand discourse, I would say not very. Its focus on buildings, and often expensive buildings, leaves out much of the most innovative work going on in the field today, by younger architects making smaller-scale projects, or experimental ideas that never get off the boards or out of the computer—ideas that get published and change our ways of thinking about what architecture is and can be. The Pritzker sends the message that unless one builds, and in a spectacular way, one will never qualify for “architecture's top honor.” The catch-22 here is that to build you need clients, and to build spectacularly, like Nouvel, very rich clients, and they are seldom willing to risk sponsoring the genuinely new. So, the subliminal message is, don't push the envelope too far. 


The existence of the Pritzker reminds us that the powerful are not as self-assured as they like to appear. They need to engage continually in demonstrations of their power, such as getting on—in an upbeat way—the front page of the New York Times, as well as other major newspapers and magazines around the world. Oddly enough, the Times perennially rails against the Nobel Prizes, not least because its founder was an armaments manufacturer who bought respectability in posterity by creating prizes in his name for intellectual achievement. And it works. When we think of Alfred Nobel, his name becomes synonymous with Albert Einstein, Samuel Beckett, Toni Morrison, Martin Luther King, Jr. The Pritzker is sponsored by the Hyatt Foundation, not exactly “merchants of death.” Still, the formula works. By associating themselves with successful architects and the world of creative thought, they make a significant step up in cultural, and historical, terms. Their domain, and their power to affect the world, is extended and consolidated.


For the recipients of the Pritzker, it's easy enough to understand why they would accept it, often with speeches of praise for the Prize and its sponsors. The money may be relatively paltry ($100,000, compared with the Nobel's $1,500,000), but every bit helps and, after all, why not? What's the harm? Only two have ever declined the Nobel, on principle: Sartre and Le Duc. So far, no architect has declined the Pritzker. If that were to happen, it really would ‘expand the discourse.'


All this we already know. So, why bother to write about it? Perhaps only to step back from the spectacle long enough to see its contour, and its limits. Only then is it possible to see its true place in the order of things, and the wider world that lies beyond.


LW



 ## Comments 
1. Josh V
4.1.08 / 5pm


I do somewhat agree with you. It would be nice to see more theorretical archtiects who have influenced design to receive the award. It was nice to see Zaha Hadid receive it, although only after she finally had something built. But then again, these awards are all just pompous masturbation that furthers little other than the recipient's career.


However, I fail to really see the correlation to the Nobel Prize. Firstly, Nobel didn't invent armaments, he invented dynamite for mining purposes. It was afterwards that his invention was twisted for military use. Calling Nobel a merchant of death would be like calling Einstein the same for discovering nuclear fission.


I honestly can't say much about the recipients of either of these prizes though. I don't really follow any of it. I used to, but awards shows don't really interest me. I seemed to think the Nobel was something of a prestige for those doing exceptional selfless work, but perhaps it's changed. I don't know. I did always assume the Pritzker was an upper echelon crapshoot. This post is actually the first I heard about the Pritzker this year.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
4.1.08 / 5pm


Josh V: Alfred Nobel owned Bofors, a major armaments manufacturer, from which he gained the majority of his wealth. It was his brother Ludvig, who described him as “a merchant of death.”
5. [progressive reactionary](http://progressivereactionary.blogspot.com)
4.1.08 / 7pm


LW – Agreed. It seems like every year the Pritzker announcement has a kind of “going-through-the-motions” feel, like they have some kind of internal list of candidates that is slowly checked off year by year. Nouvel gets checked off this year — and I'm sure we could easily predict likely winners in the years to come. Holl? Tschumi? Eisenman? Or maybe they'll go East next year – Ito? There is, as you say, no surprise.  

I also wonder if the Pritzker folks will ever right their mistake in 1991 of awarding the prize to Robert Venturi but not also to his partner Denise Scott-Brown…
7. [lebbeuswoods](http://www.lebbeuswoods.net)
4.2.08 / 12am


progressive reactionary: The Denise Scott-Brown fiasco and scandal is paralleled by the Nobel's Rosalind Franklin fiasco and scandal. In both cases, the women were decisive to the achievements, but the men —Venturi and Watson and Crick—accepted the prizes with barely a nod to the real authors. So much for authenticity….
9. [fairdkun](http://lomohomes.com/fairdkun)
4.2.08 / 4am


LW: yeay! a common sense. after reading about how pritzker, unlike it's big brother, nobel, awarded individuals [architect] for a team work [building], i see no critic towards the essence of the prize itself.  

i prefer to see this prize thing with an analogy to the movie industry, as it's not only the very popular award giving spectacle, it's also well-known for the conspiracy behind them. ‘the golden globe goes to…' will often followed by ‘the oscar goes to…', but we will always have cannes, toronto, berlin, et al, in which different categories would be awarded. these categorization would give ease to people like me, the audience or the viewer, to choose which dvd i should buy, or just rent, or don't even bother with, just by looking at the award[s] a movie has. i hope you know what i'm talking about.  

the difficulties about architecture though, would anyone see a project [or an architect] who had won ‘progressive architecture award', and his colleague [or another building] who had won ‘pritzker prize', differently? it's a difficult system, i said, because how would you see someone like thom mayne, for instance?  

again, i hope you understand me. tQ.
11. [Dave D](http://intheblackbox.blogspot.com)
4.2.08 / 11pm


As I am young (and still learning about these things) I feel unqualified to comment too strongly on who and why someone deserves to be awarded for their work. However as I study the history of both art and architecture, and practice art myself, I do feel a certain frustration brewing inside me. You speak of how the younger architects are caught in a catch-22, unable to express themselves due to a lack of patronage from the right people (people with cash)… and as I examine the developments and innovations in architecture over the last 200 years it is clear to me that it is at the grass roots level, the level where architects are born, that there is a problem.


——–I wrote a whole lot about the lack of expression allowed to students of architecture just there, but I deleted it because it was lacking in clarity and full of irrational comments born out of frustration. ————-


I simply feel that if I were to follow a career into architecture that my ideas would be restricted by a modern order… one that cannot be broken or bent (as much as Venturi might think he has bent it… to me he hasn't!!)


If studying architecture's development has thought me anything it's that true innovation existed and was promoted when Architecture was still a trade. When master guided apprentice and ideas were encouraged, today this doesn't exist. All I see around me is an architecture that is more about money than expression… Gropius had ideals and they have long been forgotten… what we do remember is how cost effective those ideals are… and that's about it.


…. Every so often there is a beautiful expression, but these are few and far between… all I see dotted across my city nowadays are great big heartless mounds of masonry and wealthy architects feeding on money.


I'm a little disillusioned! Ha


I hope this doesn't just come across as amateur/immature babble, though I'm more than willing to accept that it may very well be!


Thanks, 


Dave
13. Christian
4.3.08 / 7pm


Hi Dave,  

I can assure you that a lot of architects and a lot more people think like you. There are a lot of reasons why architecture is today like it is. But for all of that there are the opposite examples too. You just need to choose. If you want more expression more meaning more thought – then do it. What about amateur musicians for instance – they have their normal job and life and then they train for hours and hours to get better and more perfect in playing. An architect is able to do the same. And in fact that will make you happy – to deal with space with structure with function. At the point when you understood what is important or essential to you in architecture – you will love your job. And thats it. What more does one need.
15. Slam
4.8.08 / 7pm


FYI  

Boris Pasternak is the 3rd person who has declined the nobel in 1958.
17. marik
4.8.08 / 9pm


Although I always appreciate and enjoy reading the sentiments of Mr. Woods in regards to the architect as one limited by the “accepted canon of beauty” as determined by the aformentioned organizations, I would argue that recent architects that have recieved the Pritzker prize, albeit predicatable in their selection, are the designers that have paid their dues and are now flourishing within the limits they have worked to expand. Were Zaha Hadid, Rem Koolhass, and Thom Mayne not the same architects who were at one point “younger architects making smaller-scale projects, or experimental ideas that never get off the boards or out of the computer?” Project by project they have worked to set new precedents in design, and their age should not negate them of the credit they are due. In the end, the architectural award process is as the slow as architecture itself, perhaps Rem Koolhass should have won decades ago, however, at least there is this venue in which we can celebrate the contributions of those architects who, despite the artistic sacrifice inherent within the profession, have, in my opinion, “pushed the envelope.”
19. BP2
4.9.08 / 4am


One other aspect of the Pritzker, beyond the exercise of endowing the establishment with laurels, is that in a subtle way it also operates as narrative on the stasis of Architecture. The safety of the award bestows upon the recipient a legitimacy that they're doing more than “mundane” architecture. 


One can debate the merits of whether or not Glenn Murcutt is more deserving than Gordon Bunshaft or why Peter Zumthor has yet to win while Hadid's nomination was because the Pritzker hadn't awarded a female architect the prize prior to her and felt obligated to do so (as amends for Denise Scott-Brown) despite Hadid's dearth of built works. Which by some critical standards is the measurement by which Architects are measured not only by their peers but also the public at large. ‘What have you built?'


The Pritzker truly is a confirmation of the safe. Albeit some would ask are these laureates the safest of the architectural field because they reaffirm the noblesse oblige of architecture for the powerful or is the Pritzker a confirmation of the achievable by architects that have practiced for decades by pushing a far as they can what is buildable by the powerful? Is this the equivalent of a life-time achievement award? Does the recipient change their process as a result? Doubtful. Murcutt still practices as a singular operative much out of self-preservation as it is a desire to not become a “starchitect” in the sense of jetting off around the world legitimizing questionable architectural projects of absurd egoism. Much of which OMA now finds itself promoting.


I can't say that I disagree with Mr. Woods position that the Pritzker is an affirmation of the expected but I would add that it's also a commentary on the continued stasis of the built world in which the ‘tried and true' in Architecture are celebrated while the new is not. The underlying fact is that, and most especially in America, the architect labors for decades before it is determined they're ready to build significant works of architecture.


The Pritzker only confirms what we architects already know…that to achieve confirmation of one's standing as an architect the act of building is primary…pushing the envelope isn't. But as a momentary way of getting architecture into the realm of public discourse the Nobel of Architecture will have to soon move out of its comfort zone of the buildable and award critical thought as equally as the built iconography if it is to remain relevant.
21. [Conrad](http://www.conradskinner.com)
8.26.08 / 11pm


I'd like to see Vito Acconci recognized for his contribution to architecture. He began as a poet and moved through performance art and participatory sculpture into architecture.  

His early performance, from the “seminal” seedbed when he rebuilt the Sonnabend Gallery floor as a ramp from under which his masturbatory activity was amplified into the space above, to his following pieces where he tracked residents through New York streets, dealt with shared architectural and urban space, the human body and taboo. This was happening during the architecturally moribund late sixties and early seventies when modernism was stagnating, historicism began its fashionable stagger and the only architect who could begin to interest me as a student was Eisenmann. I kept a picture of Loos' Tzara house on my wall. I met Vito in 1982 when, as fabricator for Dennis Oppenheim I was helping when both artists were installing work in a show at the Hirschorn. Vito's piece was a thought-out tent-like structure of radially symmetric banners naming “marginal” social groups to be unreeled by viewers as a human figure rose up a central mast. To fast forward to his current work, the Mur Island in Graz, Austria architecturally expands his earlier exploring of shared space. It is at once an urban floating island ( arkitectur fanstastiche ), a morphed surface (shared topology) a theatre, a resting place and a bar. It's accessed by streets (bridges) from each shore. This piece challenges received categories – nature; its an artificial island – architecture; its a boat – architectural elements; the hull becomes the roof – function; it's at once a kind of park and a theatre – rationalism; its fantasy – abstraction; it's engaged kinesthetically. Vito used the practices of art to inform his architectural practice. This is almost unknown in doctrinaire architectural education, yet he's changed the artform, defying the two bete noirs of architecture, professionalism and commodification.
