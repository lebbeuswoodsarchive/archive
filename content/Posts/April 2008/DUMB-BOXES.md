
---
title: DUMB BOXES
date: 2008-04-08 00:00:00
---

# DUMB BOXES



[![](media/dumbbox11.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/dumbbox11.jpg)[![](media/dumbbox21.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/dumbbox21.jpg)[![](media/manhattan1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/manhattan1.jpg)


Anyone familiar with my work knows that I reserve a special place in my feelings and thoughts for what I call ‘dumb boxes.' These are buildings that  are often little more than rectilinear solids of brick or stone facing with holes punched in them for windows and doors. Sometimes they are all glass, with no holes at all. Most architects today consider them the antithesis of creative design, but I believe they are essential to it. The worst thing I can imagine is an urban world of idiosyncratic buildings that jostle each other for attention with no reference to any deeper form of order. The next worst thing I can imagine, though, is a world of dumb boxes embellished by architects determined to disguise their dumbness with all manner of distracting shapes, colors, materials, or tectonic doodads. <mark>I say, a box is inherently dumb, so let it be dumb, by which I mean, let it be what it is.</mark>


What is dumb about the box? Well, it's actually we, when confronting it, that are able to be dumb. We know what it is. We don't have to think about it. In the same way, we don't have to think about an urban street grid. Thirteen blocks up and two blocks to the right and we're there. What we find when we get to our destination is another matter, and it may shake, though probably not, our comfortably routine world of assumptions. The same with the dumb box. Within it, we will probably find ‘normal' life, totally predictable. But we might find the abnormal, even the world shaking. Serial killers live in dumb boxes, as did Karl Marx and Wassily Kandinsky. Right now, a genius is sitting in a dumb box somewhere, thinking through a knotty problem that, if solved, will transform our ways of thinking, even of living. Hitchcock's “Rear Window” is a concise movie play on dumb boxes and their everyday, sometimes profound, human mysteries. Those of us who live in cities don't need a movie to tell us that we never really know, for sure, what lies around the next corner.


<mark>Architects, however, are today routinely indoctrinated against the dumb box. Even advertising urges us to “think outside the box.” Why? Because it is thought we all hate the box for being too dumb, too boring, and we want to escape it. If we do escape, by buying the advertised product, we usually find ourselves inside another dumb box populated by boring people just like us. It is clearly possible to live an extraordinary life inside a dumb box. Question: is it possible to lead an extraordinary life in anything other than a dumb box?</mark>


The extraordinary can only be measured against the ordinary. If the dumb box, and all the predictability it embodies and symbolizes, is the ordinary, then we need it in order to transcend it, if that is what we choose to do. In the world composed only of the extraordinary, the only extraordinary thing to do would be to design a dumb box.


<mark>Or, there is another way to look at it. In the world of the extraordinary, which becomes, in effect, the ordinary, the only way to transcend it is to design the more extraordinary—to up the ante. This is what seems to be happening in architecture today, in the post-Bilbao era.</mark> It brings to mind the comment by Edward Hanslick, the 19th century music critic, about the operas of Richard Wagner, which their author proclaimed as<mark> ‘The Music of the Future.' “They are all superlative,” Hanslick said, “and superlatives have no future.”</mark>


Perhaps, though, there is another way, a way out of the trap of the extraordinary for its own sake, as a kind of ‘can you top this?' syndrome. <mark>Let us make the extraordinary only when extraordinary conditions demand it.</mark> Radical social and political changes. Recovery from war and natural disasters. The reformation of slums. Cultural ‘paradigm shifts,' such as computerization, or the greening of technology.<mark> Let us refrain from dressing up old building types in extraordinary new forms that do nothing to transform the way we actually inhabit or use or think about them.</mark> Instead, let us deploy the extraordinary in architecture as a way of bringing about changes we believe are important to the improvement of the human condition.


In the meantime, let us inhabit our dumb boxes, striving for the extraordinary when it is necessary, at the same time sustaining as high a standard of the ordinary as we can. After all, it is the common ground—quite deep at that—we all share.


LW 




 ## Comments 
1. Taryn
4.8.08 / 3pm

I would agree with you, but only to a certain extend. I feel that we will live in “dumb boxes” and try to create “extraordinary” architecture only if we keep trying chase technology. 


I feel there has been a huge paradigm shift with the role of technology and architecture. In the Renaissance architects and artists were constructing ideas that were leading to new inventions and new ways of thinking. I do not think this is the case today. Instead of forming innovative ideas, we are waiting for the latest version on Maya to construct new forms. 


Architecture is becoming submissive to technology. Donald Judd challenges formal interventions when he argues that we haven't fully understood space. I think this is true. 


This is a round about what to get to my comment. I feel that to progress architecture one needs to transcend it, and not refer to architecture as a means to construct architecture. Instead have an investigation, which makes architecture becomes something other then architecture. It becomes an experience. Similar to the experience one gets when walking into a Borromini Church. In that time the focus wasn't to make extraordinary architecture, it was about something else.

#Donald_Judd

3. David Bowman
4.9.08 / 3am

Taryn, we are all submissive to technology — the problem is much more broad and is not specific to the field of architecture. Everyone from architects to farmers are involved in the technology game. Some people have justifiably argued that even farming itself is a technology. Jared Diamond argued somewhat more contentiously that farming, as the root of technology, is the root of all evil (search online for “the worst mistake in the history of the human race”).


As Thoreau said, technology is really civilization. And furthermore, architecture is really technology. Just look at the Latin roots “archae” and “techne” (“supreme” and “craft” would be a very loose translation). Prior to the Industrial Revolution, buildings were the “supreme craft” — one of the highest expressions of human technology and the most visible evidence of civilization.


Don't get me wrong though — I agree with the general spirit of your statement. It's possible that people are becoming the tools of their tools. Without technology, people don't know how to communicate… how to travel… how to get food and water… how to build.


Part of the problem is that technology is always an attempt at taking a shortcut — an attempt at trying to cheat a natural system. Look no further than the communications and transportation infrastructure that the so-called First World depends upon. We traverse immense distances in just a few hours, and our commmunications traverse greater distances in even less time. However, it takes a correspondingly immense amount of knowledge, natural resources, and time to build and maintain that infrastructure. More than a few would argue that humanity (and the environment) would be better of if “we” put our energy into other things, and simply accepted that nature always wins. The shortcuts only benefit the people at the receiving end of technology. The people who put all the work into it make things easy for us. The same argument has been made about third-world labor supporting the current First World economies, for example, or cheap immigrant labor in America. Why is the US government so hesitant to make bold steps towards illegal immigration? It's because the economy depends to a great extent on the inexpensive labor that illegal immigrants provide. Looking further into the past, it's easy to see that the United States would not be where it is today without the work of African slaves and hardworking but impoverished European immigrants.

#Henry_David_Thoreau

5. [fairdkun](http://fairdkun.multiply.com)
4.9.08 / 9am


taryn + david bowman: wow, we're concerning ourselves with this ballardian discussion. would it be fair to say that architecture, as a discipline, has let itself to be a tool for greed + consumer culture [the underlying cause of most of the mess we're facing today]?


lw: do you think ‘shocked condition' [since you mentioned war + natural disasters as a vital condition for architectural existence above] a la naomi klein could lead to a collective good?


tQ,


7. Vick
4.9.08 / 12pm


The dumb box is a result of architectural trends, a result of architects being defined by the perimeters around them and confining themselves to a particular constraint or idea.


The same way people are going crazy about green architecture, i think dumb boxes happen to be one of the easiest solutions an architect can muster, a safe design that is neither too boring, nor too wacky / fancy to a client's liking.


But then again, its up to the clients decisions, this is the excuse of most architects. i believe we will eventually grow out of this self imposed notions, and as you said, will transcend this limitations at some point. its just a question of when and why.


9. [lebbeuswoods](http://www.lebbeuswoods.net)
4.9.08 / 2pm


fairdkun: the ‘shock doctrine' that Klein describes can produce an anti-capitalist result, if intellectuals can clearly lay out alternatives, and political leaders can implement them. The whole story of Eastern Europe after the ‘fall of the Wall,' is about a social-political void that capital was able to fill very quickly, while alternative politics were in disarray, unable to do much but watch. The most viable alternatives are going to be in the form of community-based collectives, no doubt. But, at best, it's a David and Goliath story.


As architects, we are part of the intellectual effort to lay out ways that architecture can become part of a wider effort to empower communities, putting, as it were, the stone in David's sling.\


11. [lebbeuswoods](http://www.lebbeuswoods.net)
4.9.08 / 6pm

refrence: <http://en.wikipedia.org/wiki/Naomi_Klein>



13. Taryn
4.10.08 / 1am


What I find interesting about the future of architecture is there will not be a middle ground. The profession I feel will either rise or fall. I think the community-based collective are a start, but I think there need to be a change that will be more powerful. 


lw. Do you think the focus of architecture will have to change in order for it to succeed? For example the architecture, in working with others, focuses on community as a whole, rather then the creation of individual buildings.


15. Vick
4.10.08 / 12pm


Mr. Lebbeus, what is your opinion on mass, collectivist structures? i seem to think that the reason most architects do not transcend an individual building's limitations is owing to the huge scale of a collective community project? It will be extensive and the people in the community would probably reject an unacceptable change to the notions of the urbanist culture.



17. [lebbeuswoods](http://www.lebbeuswoods.net)
4.10.08 / 2pm


Taryn and Vick: Your questions are closely related.<mark> I have argued that serious, creative, innovative architects have to take up the design of spatial fields, networks, addressing transitory conditions, and not concentrate so much on single, monumental buildings. But this is very different from what we have known as ‘collectivist' projects a la Socialism and Communism, which have not worked well anywhere. We must develop entirely new ways of thinking about urban architecture ensembles and constructs, and therefore new approaches to design. The System Wien project of 2005 (see lebbeuswoods.net) is one example of such thinking and design approach. However, it is only the barest beginning.</mark>


The challenge is to embrace, simultaneously, the needs of the community for orderly change, and the free choices of individuals to define themselves. Up to now, collectivist projects have failed to do this, because they have failed to address the complexity and almost paradoxical nature of the challenge.


19. Victor Navarro
4.10.08 / 8pm


is it possible to lead an extraordinary life in anything other than a dumb box?


I find this question to be tricky because it all depends on where one has lived, what one has seen and been exposed to. As LW expressed, the dumb box is simply something we know and we don't have to think about. Is it fair to say, then, that something ‘dumb' falls within the category of familiarization? To be familiar enough with a shape, form or space?  

LW applies this notion to the urban street grid, but can a ‘dumb' box be anything other than a box?…a cylinder perhaps?…a prism? or any other basic shape? Is it the nature of the shape or the mere use of repetition of that shape that holds the classification of ‘dumb'?  

So, my understanding of the question poses as to whether or not we can live an extraordinary life in complexity and unfamiliarity of forms. My first reaction to this is that based on routine and daily use, unfamiliar forms become familiar within time. If we can't say ‘3 blocks straight ahead and turn left', we'll be able to say ‘take the first bifurcating curve to the left and go straight until the first vertical crossing'…and this may be okay….anybody from the US who goes to a European city for the first time will find themselves constantly in the process of becoming familiar enough to not get lost…or vice-versa.


LW comments about an urban world of idiosyncratic buildings that jostle each other for attention with no reference to any deeper form of order. Can the skyscraper fit into this classification? Is the skyscraper a ‘dumb box' or does it jostle with othe buildings for attention? Is the skyline of Manhattan burdened by such dramatic competiton?
21. [lebbeuswoods](http://www.lebbeuswoods.net)
4.11.08 / 3am


Victor Navarro: The ‘deeper form of order' supported by the dumb box I am referring to in this post is the Cartesian. Also the pre-Cartesian, the Euclidian and the Aristotelian. Skyscrapers have traditionally fit into this form of order until recently. The new, Maia-generated skyscrapers, or their kin, pretend to deny this old form of order by twisting, wrinkling, distorting the basic tower form, without really altering the stacked floor-plate spatial system. These moves are what I consider disguises of the ‘dumbness of the box,' and are therefore mere show, mere spectacle, mere re-packaging of the same old ideas. New forms must propose and support new spatial programs. Or so I believe.
23. Taryn
4.11.08 / 6am


Do you think we need to rethink the conventions of architecture? The ways in which we communicate space, through images and drawings, have been the same for hundreds and hundreds of year, and now I think we need to challenge the conventions in order to construct new ones. Even the computer is based on the idea of perspective, which was an invented means to represent space.


How does one reinterpret the “dumb box” in order to make it innovative? LW much of your work is based on reinterpretation, constructing new parameters within the old. This was case in the System Wien, the project you mentioned before. Did you think of the space through something else, in some cases through a different medium, the medium of bodies and movement? This brings me back to a point I discussed earlier about Baroque architecture, their focus. It was not architectural, but something more. 


To a certain extent the “dumb box” wouldn't matter, they shouldn't matter. The form should disappear, and the experience should be left.
25. Diego Penalver
4.11.08 / 2pm


Hi Lebb and all,  

Maybe the mayority of people don´t really look for “an extraordinary life” , but conform to the simple. The box which is the simplest of space schemas (only 3 axis) is the most profitable and economical order in space for all common uses. I like what F.L. Wright did with the box, a kind of dematerialization maybe looking to scape from the prison it can be. He went further, though, towards a free fluidity in space, but in the background you can still see these 3 essential axis of human existence, as in mi front, back, and sides. (mi two pennies)
27. [Mark Primack](http://www.markprimack.com)
4.13.08 / 3am


Victor Hugo wrote that, ‘a wall behind which something is happening is of no small interest to a Parisian.' He was referring to a crowd gathered outside a courthouse in which a famous trial was taking place; a blank stone wall had held a mob enthralled for hours.  

I think it safe to say that very little is happening behind the sensational facades of contemporary architecture. No matter how enthralling their rhetoric, stars like Koolhaas aspire to wealth at worst (Dubai) and the vacuous at best (Prada). Their work reminds me of the old blues song sung by Taj Mahal, ‘I've got a mind to give up living and go shopping instead'.  

So I'm happy to be designing a district of ‘dumb boxes'- 26 on 20 acres- in which 800 people will work and/or live, having completed the interiors of their ‘shells' to suit themselves. When local planning staff voice concerns for ‘architectural continuity' or, worse still, demand constrictive design guidelines, I say, ‘the street will be pleasant and active. Businesses can grow or contract without leaving the neighborhood. Who is going to care what the facades look like? With a little luck, these walls will be draped in edible vines and espaliered fruit trees.  

Of course, community-based architecture pre-supposes the possibility of authentic community. I share Lebbeus' optimism on that count, though I fear mine is mostly faith-based.  

And thank you, Lebbeus, for your continued efforts here.
29. [as](http://alexan.stulc@gmail.com)
4.13.08 / 5pm


I find it troubling in this discourse that no one has yet mentioned the proclivity of the construction industry to the dumb box. Look at the available building materials today; the rectilinear plywood, cmu, lumber, and w sections. I agree with the notion of a ‘dumb' architecture, but is this distinction really formal? It seems that the difference between the work of the present and masters such as Borromini is a fundamental lack of material understanding. Take Gaudi and his use of masonry and ceramic tiles, then look at the detailing for most any (progressive or not) architect dealing with complicated morphology – its pretty shapes dressed in gyp board. Before we address politics we should look towards the white elephant in the corner – architecture.
31. [lebbeuswoods](http://www.lebbeuswoods.net)
4.13.08 / 7pm


as: to repeat, the standardization of construction materials and the standardization of ways of living are both part of the ‘deeper form of order,' which has its roots in the accepted standards of Western logic and mathematics. The challenge to architects today is NOT to make innovative new forms cloaking the old logic and its typologies, but to discern the new logics emerging from changes of all kinds, and to give these spatial and formal utterance. If we can't do that, let's not deceive ourselves by trying to disguise the dumb box OR the old typologies.  

But let's keep trying to discern the new that's trying to be born.
33. [Mark Primack](http://www.markprimack.com)
4.13.08 / 11pm


as: When clients tell me they want a craftsman style home, I ask them if they can support four finish carpenters for five months. If they can, I probably don't want to work for them, or they me. When materials were expensive and labor cheap, we had craft. When materials and labor were abundant, we had tract housing. Now materials and labor are both expensive, and regulation is becoming extravagant; we have people sleeping in overcrowded cars while ‘sophisticated' architects capitalize on vacant rhetoric. I fear the new, community-based craft will all be post-apocalyptic. But then why am I still speaking in the future tense?  

Lebbeus, isn't your ‘free space', by its very definition, a ‘dumb box' at best?
35. [lebbeuswoods](http://www.lebbeuswoods.net)
4.14.08 / 12am


Mark Primack: I have always openly declared that “all space is freespace.” Certainly dumb boxes are prominently included. My definition of ‘free' is, however, a bit rough for most people. Freedom is loss. When I lose my wallet, I am free of my wallet and all within it that defines/constrains me. A changing world (not to mention just getting old) results in many kinds of loss, thus also of freedom. The problem is that freedom is painful before it can be pleasurable, if at all. The dumb box has lost just about everything that gives pleasure, except its potential for transformation. That's what my ‘non-box' freespace structures in Zagreb, Berlin, Sarajevo and elsewhere were designed, under general conditions of loss, to instigate and to do.
37. omaraza
4.14.08 / 7am


I completely agree with your ‘Dumb Box' post LW. I recently wrote a short essay on ‘Rapid Urbanism and The Future Multicity'. What we need to get away from is this icon typology that will only lead to the ‘super-iconic' (a typology that is based on extant systems of form-finding) and towards a more 4-dimensional view of what urbanism actually is, designers need to get creative with the multiplicities of an urban context and how different programs can interweave within a site. Rather than this consumerist billboard syndrome that is overtaking every part of our life.
39. [JIC](http://informationagearchitecture.blogspot.com/)
4.14.08 / 10am


LW,  

You offer a very interesting perspective by explaining that in order to transform the way we inhabit, use, and conceive of architecture, we must at first experience restriction and be surrounded by the tiresome uniformity of typical, dumb boxes. I completely agree that without knowing what is ordinary, there would be no such thing as the extraordinary; the latter is fundamentally a derivative of the former. But while I concur with your opinions, I also have many questions about how what you present can be carried out in our contemporary society. You go on to advise that we should “make the extraordinary only when extraordinary conditions demand it. Radical social and political changes. Recovery from war and natural disasters. The reformation of slums,” and that in the meantime we should sustain “as high a standard of the ordinary as we can.” My question to you is: How do you see this to be possible? How can you expect architects and designers to “create the extraordinary” only on the cue of such situations, as if that process is a practice and ability that can be turned on and off? Even if there are a select few with the potential to produce exceptional designs only when it is necessary, why would you anticipate that those capable would resist using their capacities at all times? If one is talented, I do not see it reasonable to hope for them to apply their skills sparingly, nor do I see it beneficial to “the improvement of the human condition” as you mention. Perhaps then, do you mean that we should be more aware that we depend on what is considered the familiar, and that we should learn to tolerate those who do design dumb boxes?
41. Diego Penalver
4.14.08 / 2pm


LW said: “But let's keep trying to discern the new that's trying to be born”.


Maybe this new logic of form and space is related to new techniques. I am thinking here in the new tools and processes available in the digital world. The box or rectilinear forms are the result of the most suitable techniques, buildings happen to be very expensive, they depend on so many people, things and circumstances, that simplicity in form became the norm, in most cases, with very happy exceptions through history (Borromini and the labor intensive barroque..,for intance:-))  

The development of new processes of manufacturing (machine-factoring) controled by computers offers the architect ways to control material and form like never before. Now comes the story of what he does with these new methodologies.  

In most cases these new techniques are still expensive, but maybe with time they will become readily available, like the concept of PC, or the gourmet cafè.(2.5 cents)
43. [lebbeuswoods](http://www.lebbeuswoods.net)
4.14.08 / 3pm


JIC and Diego: “Sustaining as high a level of the ordinary as we can” suggests that there are different levels of the ordinary, the everyday, the normal. Each of us should exercise as much talent as we have in achieving and sustaining as high a level of the ordinary as we can. It's not a question of turning talent, or convictions, off and on, but rather of having a sense of proportion, of where and when to apply extraordinary effort.


The ‘dumb box' of our Cartesian world demands to be transformed, to a greater or lesser degree. How—and WHY—will we choose to transform it?
45. spencer
4.14.08 / 8pm


Lebbeus,


It seems there is a contridiction to your logic that I can't quite get my mind around. You seem to be posing an oppositional arguement between Ordinary and Extraordinary but attempting to cast these words off through your concept of Freedom..


The trouble I'm having is with the multiplicity that you are making out of these words. Instead of casting them off you seem to be creating more varriations on these words (different levels of ordinary leading to different levels of extraordinary) further entombing us in their meaning rather than raising us to the liberation of both definitions. This is similar, I believe, to the transformation Lars Lerup talks about in Planned Assualts.


Maybe casting them off isn't possible just as casting off standard technology isn't. Maybe you are trying to say that we need to approach our process differently. Say more naturally like the way a plant grows – responding to stimuli from many sources creating many forms (arguably both ordinary and extraordinary) from similar product. Afterall, the building blocks of plants (ie it's 2x4s) are standardized pieces but ones that have the ability to transform to stimulus beyond it's comprehension.


The dumb box responds to one stimuli: humans. It isn't complex nor allows for complexity. Not the complexity of material or form but the complexity of stimulus that leads to complex shape rather than form. I use the word shape because [I can't think of it's opposite] it can be familar without allowing for judgement or opposition (ordinary/extraordinary). We all recognize the shape of a snowflake but no two are alike becuase of the complexity of stimuli each receives. They are both ordinary and extraordinary at the same time while being neither (individual).
47. Godofredo
4.14.08 / 8pm


Lebbeus, if one is to speak of freedom, one should think of a concept of “freedom” in itself, and not as an opposition to non-free. It is strange that you define freedom as loss, as it implies the pre-existence of a state on non-freedom (plenty of similarities with the Christian logic of sin…).There is nothing free in such freedom.  

Remember Jarry and his science of exceptional reasoning: the invention of less exceptional correlations is the death of creativity. The same applies for the “dumb box “as you strangely call it: it is not in the orthogonal of the box that the problem lies, its in the orthogonal imagination unable to make something out of it…
49. [lebbeuswoods](http://www.lebbeuswoods.net)
4.15.08 / 12am


It's wonderful to have so many angles of view on this issue, but—amid much semantic confusion I want to take a step back, take several breaths, and re-read my post. The words were chosen most carefully.
51. spencer
4.15.08 / 3am


Godofredo,


I feel it's difficult to use words without creating an oppositional situation. Most (if not all) language is derrived from oppositional relationships. So, we should try to read past this bias to see the root and intent of what Lebbeus is saying. It seems Lebbeus is using freedom in much the same way John Rajchman uses “lightness” in his book, *Constructions**.* 


Lebbeus, I really hope I did not come across as nit-picking your choice of words. I was only bringing up a few points to help me understand better what you are saying. 


For the sake of dropping the critique of terminology and words…


The notions of ordinary and mundane are interesting and clearly have their place in our world (thanks to Venturi and Brown and others in architecture), but it is your statement, “Let us make the extraordinary only when extraordinary conditions demand it,” that I think is at the root of your arguement. I agree that the media leaders in archtitecture are not doing anything revolutionary and that form is changing [although I feel that is argueable] but the notion of architecture stays the same. It's almost as if architecture hasn't had that moment of clarity that takes it from being representational to becoming something more expressive of itself and revolutionary to itself and others. In short it is not contributing to moving people to act but allows people to act. Maybe it needs something to give it a push in the sameway photography moved painting into something new changing our perceptions?


Right now, we all agree, economics drives the architecture bus not allowing for much freeing from the bonds that hold architecture to the ground. In my mind, with the passing of John Hejduk, you along with Lars Lerup, Jennifer Bloomer and a few others have made the most impact on unloosing architecture from its foundations giving the rest of us a little insight into what architecture could become.
53. Diego Penalver
4.15.08 / 12pm


LW says,


Sometimes I think, and this happens a lot in my experience, that the ordinary becomes extraordinary and viceversa. It seems to me that this terms are qualitative, poetic and many times reflect subjectivity.  

I agree this being important to any creative person, but don´t see a necesary implication towards any particular concept of form in space.  

If I were what they call nowadays a Minimalist, I would not be concern with the “dumbness of the box” nor the suitability of the cartesian field, I would take it or granted, for simplicity sake, and concern myself with the dynamics of space, light and surface, in order to discover new relations-sensations within the world of the box, a reduction to the essential of the built would be my moto.


On the other hand, I don´t restrict the search of the minimalist to a orthogonal construct. To reduce for clarity to the essentials does not necessarely means dumb boxes. Actually a living space of non-euclidean nature could be an interesting pursuit, like we´ve seen in many contemporary architecture, incluiding Leb´s, who has been very influential, i must say.
55. Diego Penalver
4.15.08 / 12pm


Im sorry this phrase was missing:  

LW says: “Sustaining as high a level of the ordinary as we can” suggests that there are different levels of the ordinary, the everyday, the normal.
57. aaronnajera
4.17.08 / 4am


There is some kind of integrity inherent to these “dumb boxes”. The kind of integrity that's not afraid to reveal itself before the world of criticism. The integrity to be just a box and still function. 


And there's Dubai with all the powerful buildings making an statement. No one's worried about a little dumb box just being there.
59. godofredo
4.17.08 / 10am


A few further comments:  

I think architecture should be discussed by what is produces, by what it does (not just in a functionalist way, or maybe in a super-functionalism which stands beyond utility), and in that sense it should never be reduced to a shape. The shape is always part of something bigger and far more complex.  

I would even say that the shape doesn't exist, in the sense that it is an abstraction that we decide to do. Or more correctly, it is an extraction.  

In this way we should avoid falling in the simplistic opposition “box vs. strange shapes” as if it would be an equivalent of “order vs. creativity”, or “ordinary vs. extraordinary”. It is not by shape that we should think architecture. That's only good for glossy paper magazines, and it is due to an exclusive focus on shape that most of contemporary visionaries are so benign and harmless.


But in the main thing I would agree with Lebbeus: there is nothing more annoying than the proliferation of box-like buildings that pretend to be something else. To them I will ever prefer the simple box.
61. [Arthur McGoey](http://amcgoey.net/)
4.19.08 / 9pm


LW: That was a very interesting essay. I have long felt that an architecture of the ordinary is overdue. I don't just mean ordinary in the sense of normal versus extraordinary, though it is certainly related. I am referring more to the ordinary and the singular.


The ordinary encompasses the field in which the singular is but a special point within. It is what is implied by your usage of dumb boxes and saving the extraordinary for extraordinary circumstances. I feel like such “ordinary” architecture as the dumb boxes is the architecture of the field; the fabric from which our day to day lives arises. The singular are just those points of concentration and focus from which the extraordinary can emerge. The singular is the points of influence and persuasion in the greater context of our lives.


For example, surely your knitted together architecture in the scarred and war torn city becomes a singularity of influence for healing in many of the lives of the city's citizens. It is not that a single building becomes the grand monumental focus, but rather the ordinary boxes from which your building grow form a kind of tissue that supports the very change in the city that you propose.


I feel like today's architecture is trying to always produce the singular moment without giving support for the ordinary field that is required for the singular to become affective in people's lives. As you have commented above, the very shallow use of spectacular forms has remained largely impotent to instigate true change.


True change will come by an architecture of the ordinary not just the singular. It will happen by changing the field in which we all live not by giving us branded images of desire and consumerist want. Thinking very carefully about “dumb boxes” and subverting through subtle but deep changes is where, I feel architectural discussion should focus, not on making “interesting” forms.


Thanks.  

Arthur.
63. [lebbeuswoods](http://www.lebbeuswoods.net)
4.20.08 / 2pm


Godofredo: I have spent some effort on this blog to linking aesthetics and ethics–what a ‘shape' is, and what ‘it does.' I don't think it is possible to separate the two, or to devalue one in favor of the other. To say that shape ‘is only good for glossy magazines' is only the flip-side of the coin that elevates shape to the highest priority. I would advise trying to see the ethical dimensions that shape (say, dumb-box Cartesian, or exotic free-form) embodies. You may not agree…..
65. [lebbeuswoods](http://www.lebbeuswoods.net)
4.20.08 / 2pm


Arthur McGoey: Your summary argument is coherent. I fully agree with it. Unfortunately, architects seem to be dividing into two antagonistic camps: one that favors freedom of individual esxpression (the ‘singular,' as you wisely call it), and another that argues for social-ecological efficacy above all. What we need are ways that bring the two together, as you suggest.
67. [oa](http://archinect.com)
4.23.08 / 2am


new cannibalist manifesto of the ordinary;


we want to see extraordinary and extraordinary eat each other. 


and, ordinary and ordinary grow together. 


and, we want architecture of ordinary thought in schools,  

taking the educational institutions back from extraordinarists in developed nations run by so called talent supremes. 


no more senseless fake fluidity, no more of yours.  

no more hand me downs.  

we are not interested in your art.  

they are not for us. we have art of our own.  

guggenheim yourself.  

your operas hurt our ears.  

we sing to rice fields and to our skies.  

we tell our own stories.  

we teach our skills.  

to our youth.  

you stole our art ideas and sold them for millions.  

now they are worthless in your hands.  

we don't want to see them in your extraordinary museums.


you always told us we need you. we don't.  

you need us.  

you'll become us.  

there is no other way you can survive. 


extraordinary architecture is like apartheid.  

there are a lot of extra-o's in five acre choice sites on the lake shores of opportunity class, but they only house sharpest tip of the wealth pyramid.  

the world is different now and will be even more different when the petrolium start to deplete faster and rice becomes rare and farmer only grows for his own community and the people in wall street screams for meal.  

our beans are not segregated.


ordinary will survive because of its simple ways and its vast imaginative pool. but more so by its skills of talking to wheat plants and enjoying bread with onions and drink the cool water from its clay vessel, it's been done for centuries, we can survive without automatic cheese slicer…


extraordinary is alien to us.  

as soon as ordinary will be the way to live, extraordinary will be ordinary and pay its taxes.  

ordinary will make sure of that. 


transformation will come from survival skills not from paper money with your serial numbers. 


we are not transformed by looking at extraordinary lifestyle pictures on tv. 


why should we work there and if we can't afford its product. 


we are not interested in what's yours anymore. 


we are only interested in what's ours.
69. [Lebbeus Woods On Living In And Thinking Outside 'Dumb Boxes' on PSFK](http://www.psfk.com/2008/04/lebbeus-woods-on-living-in-and-thinking-outside-dumb-boxes.html)
4.23.08 / 6pm


[…] Lebbeus Woods recently posted an essay on his blog discussing the effect banal rectangular buildings ('dumb boxes') have had […]
71. amp
4.23.08 / 7pm


Programme is missing from this discussion. In Programme we confront the deadness of all architecture, it is not alive or intelligent but is an objective display of such subjective and, though rare, transcendent things. 


The tumescent ennui which produces a manifesto is libido moving and encountering its current limitations of self, but is not an evolution nor even the catalyst for one. An image is not a Vision, but our naive and eukaryotic psyches are only willing to experience so much pain before we choose comfort over genesis.
73. [Victor Navarro](http://www.myspace.com/vicarch)
4.23.08 / 8pm


oa-


my understanding is that ‘extraordinary' is presented in this article as the ‘extraordinary dumb box'…that notion of dressing up the box and making that dumb box not look like a dumb box. To make the dumb box look like something else other than it's very nature is put into question…should we or should we not make something look extraordinary when, in fact, it is nothing but ordinary.


what you insinuate in your manifesto is that the extraordinary is something of an elite, egotistical, contrived and suprematist nature. I can agree with you only in a few cases… Dubai is a great example.


However, I would not incline to generalize your comments. There is something about human nature that looks beyond our daily habits and seeks a deviation from routine, the simple…the ordinary. As simple as one's life may strive to be, I would not expect it to be linear by any means [linear as equivalent to ordinary]. In fact, extraordinary has always been manifested in some form of celebration or grand event, as has the architecture that houses it.


The question is whether we have the criteria to decide on the ordinary over the extraordinary….
75. Billy Ray Virus
4.24.08 / 5am


To be purely pragmatic, most clients are perfectly happy with dumb boxes, and perfectly happy to pay us to design them.  

Try telling your client that your innovative unorthodox structural design is going to cost a few million dollars more than a dumb box, and watch the reaction on their face. At that point, your credibility goes down the toilet, and you need to have the most incredible line of BS to feed them.  

If it works for you, then more power to you.
77. spencer
4.24.08 / 6pm


Victor,


While I understand your point about serendipity and chance circumstance that can creat the extrodinary, I would also say that it is human nature to have ritual, regularity, familarity, and the expected. All of which I feel are ordinary occurances in life that give stability and allow for concentration, meditation, calming and spirituality giving way to big ideas and extraordinary moments.


To answer Lebbeus' question if “is it possible to lead an extraordinary life in anything other than a dumb box” I would say yes it is considering the many mundane aspects of daily life (including location and spatial) that we go through. And considering we are not attempting to create extraordinary architecture.
79. amp
4.24.08 / 6pm


‘Ritual' is an interesting word to use – does this really imply a direct connotation as habitual or familiar?


Ritual is usually habitualized programme facilitating an extraordinary experience, when the sacred and profane meet. 


Take all the big project architecture renderings for Dubai, the Emeriates, or really any big project by a starchitect, and notice the “Ray of God's Light' beaming into the image like some mystical blessing – implying many things to many people but really doing nothing but producing an image which is supposed to convince the world of the extraordinariness of the architects gift. Truth is, we work in a service industry – its a craft and creative, sure, but architecture cannot happen for architecture's sake, there is no virtue in that. It always costs something.
81. spencer
4.24.08 / 10pm


amp,


I see your point. But your example is a form of marketing and not an actual ritual. 


To me ritual is something done repeditively for a spiritual connection. It certainly may be an extraordinary event, like Catholic taking of the host or ordinary like a baseball player rubbing his socks before going to the plate.
83. amp
4.24.08 / 10pm


I am a cynic: whats the difference any more? Architecture, extraordinary or less than, is a civic operation – the dumb box is not marketable as a dumb box (though it facilitates the secular ritual of neophilia), it needs the Light of God (which suggests to me that our culture is currently bereft of any such Light but unconsciously so) and God's Fantastic Image Production Skill-Master to feign an attempt at the extraordinary. 


The dumb box is far to honest – you want this because you beleive this and that, and thats what, who and how and why you are. Architecture isn't as important as the need to make and have money, and if architecture can get you more, then ye are blessed.
85. Diego Penalver
4.28.08 / 10pm


..Talking about dumb boxes and architecture:  

<http://firmitas.org/>
87. [The Sesquipedalist](http://sesqui.pedali.st)
5.1.08 / 10am


This is an extraordinary essay amidst ordinary dumb box blog posts. Which, to me at least, proves the very point you are making.  

Nice one.
89. [Green Roof Building - [pushpullbar]2](http://www.pushpullbar.com/forums/back-drawing-board/11581-green-roof-building.html#post158422)
10.30.09 / 4pm


[…] made some very specific, atypical formal moves but haven't said what lead to this. Why not just a box? I am not necessarily advocating 'dumb box' design, but you should have a reason for doing […]
91. [SAARINEN'S LAST EXPERIMENT « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/02/27/saarinens-last-experiment/)
2.27.12 / 12am


[…] computer-generated complexities. What about the creativity of everyday life? “Dumb boxes,” as I have written elsewhere, may be the wave of the most creative future. Whether yes or no, the sharp sting of Saarinen's […]
