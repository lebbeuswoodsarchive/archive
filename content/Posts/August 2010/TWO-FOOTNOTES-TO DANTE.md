
---
title: TWO FOOTNOTES TO DANTE
date: 2010-08-19 00:00:00
---

# TWO FOOTNOTES TO DANTE


Two recent photo-essays, in the manner of the long-gone Life Magazine, have appeared in the New York Times. They struck me as being relevant and deeply related.


The first is on [the housing boom in China](http://www.nytimes.com/slideshow/2010/07/15/opinion/20100715_LivingRooms_China.html?scp=1&sq=Christoph%20Gielen&st=cse), photographed by **Christoph Gielen**, “a photographer who specializes in the intersection between art and environmental politics,” with comments by **Tom Doody**, a freelance writer:


[![](media/china-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-11.jpg)


[![](media/china-1a-inv.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-1a-inv.jpg)


[![](media/china-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-2.jpg)


[![](media/china-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-2a.jpg)


[![](media/china-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-31.jpg)


[![](media/china-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-3a.jpg)


[![](media/china-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-5.jpg)


[![](media/china-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-5a.jpg)


[![](media/china-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-6.jpg)


[![](media/china-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/china-6a.jpg)


.


The second photo-essay is about [slum dwellers scavenging in Ghana](http://www.nytimes.com/slideshow/2010/08/04/magazine/20100815-dump.html), photos and comments by **Pieter Hugo**:


[![](media/ghana-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-1.jpg)


[![](media/ghana-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-1a.jpg)


[![](media/ghana-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-2.jpg)


[![](media/ghana-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-2a.jpg)


[![](media/ghana-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-3.jpg)


[![](media/ghana-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-3a.jpg)


[![](media/ghana-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-4.jpg)


[![](media/ghana-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-4a.jpg)


[![](media/ghana-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-5.jpg)


[![](media/ghana-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-5a.jpg)


[![](media/ghana-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-7.jpg)


[![](media/ghana-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-7a.jpg)


[![](media/ghana-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-8.jpg)


[![](media/ghana-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-8a.jpg)


[![](media/ghana-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-9.jpg)


[![](media/ghana-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-9a.jpg)


[![](media/ghana-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-10.jpg)


[![](media/ghana-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ghana-10a.jpg)


.


For LW comments, see [SLUMS: the problem](https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/), a previous post on this blog.



 ## Comments 
1. [SLUMS: The problem « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/)
8.19.10 / 1pm


[…] <https://lebbeuswoods.wordpress.com/2010/08/19/two-footnotes-to-dante/> […]
3. [Tweets that mention TWO FOOTNOTES TO DANTE « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/08/19/two-footnotes-to-dante/?utm_source=pingback&utm_campaign=L2)
8.19.10 / 3pm


[…] This post was mentioned on Twitter by Hungryghoast, Krzysztof Narkowicz and Nick Porcino, alvaro jasso. alvaro jasso said: TWO FOOTNOTES TO DANTE <http://bit.ly/augQon> […]
