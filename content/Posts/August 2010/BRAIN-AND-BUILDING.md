
---
title: BRAIN AND BUILDING
date: 2010-08-28 00:00:00 
tags: 
    - cybernetics
    - experimental_design
    - Kordetzky
---

# BRAIN AND BUILDING


[![](media/lk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/lk-1.jpg)


(above) One of Lars Kordetzky's sketches for his “spaceinvader” project.


.


The last time we encountered Lars Kordetzky on this blog, he was working with mental patients in the doomed Oberwil psychiatric clinic in Zug, Switzerland, visualizing and constructing [the imaginary spaces](https://lebbeuswoods.wordpress.com/2010/01/23/lars-kordetzky-sequences/) that actually existed for them in the barren confines of their setting.  Since then he has continued his research and speculations in a more rigorously structural way, walking the tightrope boundaries between the physical and metaphysical in his search for an architecture grounded in a complex idea of reality.


In his latest project, which he calls “spaceinvader” (a title I like because of its evocation of algorithmic play), he leaves behind the lush climates of “mind” and burrows into the more austere domain of ‘brain,' with its computational networks of neurons and electrical charges and discharges, its quantified physicality so often reduced to charts and other forms of ossified data. Kordetzky's gamble—that herein lies an architecture of fully human dimensions—is based on the obvious fact that the brain is the sponsor of the mind, indeed, its very source. Inspired, perhaps, by pioneering scientists and thinkers like [Heinz von Foerster](http://en.wikipedia.org/wiki/Heinz_von_Foerster) and his colleagues in cybernetics, he sees in the neural structure of the brain not a ‘trivial machine' in which output is a strict function of input, but rather a complex organism capable not only of unpredictable results but of creativity, the very source of our humanity. In the complex interconnections, the synapses and gaps, the ever-incomplete structure of neural space, Kordetzky constructs a model of imagination's exploration and growth, a spatial matrix of both the possible and the improbable, challenging our capabilities and our will to inhabit the world.


The images shown here are a sneak peek at this work, which he is carrying forward to what conclusions we shall have to wait and see. His preliminary work is encouraging—the models, in basswood and chipboard, are especially so, because they are precise yet imperfect, just like organic matter in its struggles of formation. Reality, on more levels than one.


LW


.


[![](media/lk-comp-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/lk-comp-1a.jpg)


#cybernetics #experimental_design #Kordetzky
 ## Comments 
1. [Tweets that mention BRAIN AND BUILDING « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/08/28/brain-and-building/?utm_source=pingback&utm_campaign=L2)
8.30.10 / 11am


[…] This post was mentioned on Twitter by Ethel Baraona Pohl, farid rakun and Cesar Reyes Nájera, KpaxsArchi. KpaxsArchi said: BRAIN AND BUILDING: (above) One of Lars Kordetzky's sketches for his “spaceinvader” project. . The last time we e… <http://bit.ly/9OPmNQ> […]
3. [Chris Teeter](http://www.metamechanics.com)
9.1.10 / 3am


this is interesting, but not sure i get it yet…
5. Thom Brady
9.12.10 / 11pm


Hey Leb,


Why dont you just effing build something(an actual building!) instead of playing around with toothpicks?





	1. Pedro Esteban
	3.27.11 / 11am
	
	
	imbécil!!! (in spanish)
	
	
	Sorry.
7. Gio
9.15.10 / 7pm


Thom,


If there is no room for imagination, creativity and exploration, then what are you building?
9. [Aaron](http://www.amorphica.com)
10.13.10 / 4am


Why does he have to build a building when he is building a complex posture that you don't get.
11. Pedro Esteban
3.27.11 / 11am


How the patients feel about these creations? What they do, because ok, we understand an interesting architecture but what the pacients think of this?  

How this help them?
