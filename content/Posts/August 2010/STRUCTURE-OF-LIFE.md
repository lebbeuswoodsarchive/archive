
---
title: STRUCTURE OF LIFE
date: 2010-08-30 00:00:00 
tags: 
    - creative_science
    - DNA
---

# STRUCTURE OF LIFE


Call it late summer foraging beyond the parched fields of architecture, but I have recently been caught up again in a story about creativity in science. My fascinations with Relativity theory and Einstein, as well as Radical Constructivism and von Foerster, are well displayed on this blog and elsewhere, along with my attempts to tie them back to architecture. Prompted, I suppose, by my recent teaching experience and Lars Kordetzky's research published here, I have reopened the pages of *The Double Helix*, a book about the discovery of the structure of [DNA](http://en.wikipedia.org/wiki/DNA).


It's not only that the very term has become part of our present everyday culture, in newspaper accounts of prisoners being freed after decades of incarceration for crimes they didn't commit, now proven by DNA evidence, but also that genetics has become such an important part of our perceptions of ourselves and of others. When I was a boy, we were taught (in a conservative Midwestern high school) that our human destiny was determined by a combination of ‘heredity' and ‘environment,' greater emphasis on the latter. Our race, class, gender, and even country of birth—our genetic input—were far less important than our social circumstances—family, community and especially education. But all that is changed today. It turns out (so we are told) that there is a gene for everything from being fat, smart, creative; for getting cancer and Alzheimers; for, I suppose, being loyal citizens, criminals, or geniuses—our die is cast at birth. Thanks to the [human genome project](http://en.wikipedia.org/wiki/Human_Genome_Project), which (so we are told) definitively decoded the genetic structure of human heredity, it will only be a matter of time before genetic testing will divide us and our progeny into differing human camps, to be dealt with by society's institutions accordingly. We can only imagine where future genetic engineering, beyond that of tomatoes and the cloning of sheep, will take us. And it all started with the discovery of the precise structure of DNA.


The story of the discovery of the structure of DNA—the complex molecule that carries, literally, the genetic code of living organisms from generation to generation—brings together so many strands appearing recently on this blog. The experimental. The creativity of science at its highest, conceptual levels. The evolution of humankind taken into its own hands. The potential advent of a scientific dictatorship that will ‘perfect' the human race. Even the use of hand-made physical models to work out new concepts of universal phenomena. [The book](http://www.amazon.com/Double-Helix-Personal-Discovery-Structure/dp/074321630X/ref=sr_1_1?ie=UTF8&s=books&qid=1283002807&sr=8-1) written by James Watson, the co-discoverer of DNA, along with Francis Crick, reads like an adventure story of risky exploration guided as much by intuition and chance as by rigorous scientific method, and it all happened in the pre-computer age, using analogue tools, such as, well, the human brain. It is a story with intrigues and a dark side at the outset, in the way the research of Rosalind Franklin was stolen from her and given to Watson and Crick at a crucial moment in their speculations, and for which she was credited only after she had died. Science on this level is human drama at its fullest. A good (or so they say) ‘summer read.'


.


[![](media/watson-crick-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/watson-crick-2.jpg)


(above and below)) Metal working model used by James Watson and Francis Crick to determine the double-helical structure of the DNA molecule in 1953.


[![](media/dna-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/dna-4a.jpg)


(below) 1953 X-ray diffraction photograph of DNA molecule (end-on) by researcher Rosalind Franklin, which directly informed the Watson-Crick model:


[![](media/franklin-dna-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/franklin-dna-1.jpg)


(below) a segment of the very long DNA molecule—the double helix:


[![](media/dna-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/dna-6.jpg)


(below) A caduceus—the staff carried by Hermes, “the messenger of the gods.” Is its double-helical structure an ancient Greek premonition of the most vital ‘message of the gods,' or simply a coincidence?


[![](media/caduceus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/caduceus-2.jpg)


LW


#creative_science #DNA
 ## Comments 
1. R. Y.
8.30.10 / 6pm


Excellent post. However, I think it should be noted that the science of biology is rapidly moving beyond the idea of simple genetic determinism towards a much more complex view of how interactions between the environment and the genome influence developmental processes. While presence or expression of certain elements in the genome may be correlated with observed characteristics (or the phenotype) of an organism, this correlation does not imply the presence of simple causative mechanisms. The emerging field of ecological developmental biology (or eco-devo) encompasses much of the thought that continues to break down or complicate these old dogmas of molecular biology. Of course, the popular press is slow to pick up on these developments and continues to present a much simpler story.  

Ross
3. [Michael Phllip Pearce](http://www.carbon-vudu.com)
8.30.10 / 7pm


Amazing how all the solutions are within us. Great post and sounds like an interesting summer read. 


Another source for those fascinated by bio structure is, D'arcy Wentworth Thompson, “On Growth and Form”, I learned this from reading one of Santiago Calatrava's books long ago on a concept he cited. Anyway, “The Principles of Similitude”, and the gravitation governance of the structure systems about life outside of water and structural systems about life within water are fundamental. 


DNA would fall into this system, a structure protected/supported by fluids within the body. However, to remove it from its original intent and emulate the design but in a larger scale would violate the integrity of why it is. 


As for the caduceus, great analogy and good question because there is such much information within and about us how can we really know? The source of the design[er] perhaps.
5. [River](http://www.routendesign.com/scott)
8.31.10 / 7am


<3!, OMG.. Hello again. Soon we will not need these contraptions, eh!


"continues to present a much simpler story."  

Ross


Ungh hunngh. Michael mentioned water. That's a lot of our nature, and quite mysterious at best!


Most excellent link: <http://video.google.com/videoplay?docid=-5123182744103122879>


I'll admit, that ever since I went to architecture school that I've been on a kind of hell-bent campaign against non-vernacular language. I'm so sorry about Daniel Libeskind and the other folks who introducted me to John Hejduks sacred material? They drive most folks away because they're so super-special!! Lebb, I still can't believe you fought the film industry over the design rights of the chair in the 12 monkeys. That's heroic.


Willy Wonka quoth unto Charlie, "We are the music makers, and we are the dreamers of dreams"


Us are blades of grass on the wind now! Let's party!
7. [River](http://www.routendesign.com/scott)
8.31.10 / 11pm


You can delete my super-silly rambling insurrections at will with my wholehearted permission at any time, lest I reveal TMI. But this just in, as Burning Man is ragin' out in the Black Rock: <http://vimeo.com/13215028>


I feeel your idea of freespace Leb. And I hate to bring you down to reality. But this is what is happening now.
9. [m.byrne](http://byrmartin.wordpress.com)
9.2.10 / 1am


i really enjoy that picture of watson and crick. especially because of whichever one is on the left.


i wonder, considering the content and direction of this and other recent posts, how we as architects might deal with the supposed ‘scientific dictatorship' that may or may not arrive. in the sense that a ‘perfected' human-turned-cyborg might be our future selves, we might then also not need architecture, if say our lungs are genetically modified to breath whatever is in space, or if we dont need to sleep, or to inhabit, or…who knows?


also, how does all this talk about science relate to deleuze and guattari's royal science and nomad science? the dna discovery probably falls under royal science, and to that end, if [royal] scientific dictatorship is on a distant horizon, do we offer more opportunities of [nomad] scientific freespace?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.2.10 / 1pm
	
	
	m.byrne: Watson is on the left.
	
	
	There are always anti-democratic forces at work in any society that believe people need to be ruled, for their own good. Monarchs, oligarchs and dictators of one kind or another have ruled throughout human history and are still very much with us today. As Thomas Carlyle said, “It is the everlasting privilege of the foolish to be ruled by the wise.” Democracy in the present sense of the term—we, the foolish, ruling ourselves—begins in the late 18th century and has a pretty tentative hold today even where it exists. The genius of Huxley's book is that the dictators have learned that the best way to control people is with pleasure, not pain (*Brave New World* vs. *1984*). Through various synthetic means, scientists and technologists create and control the pleasures that pacify people (people get addicted to them)—say, drugs and entertainment—so the scientists either become the dictatorial government or directly serve it.
	
	
	Now we get to “royal” vs. “nomad” science. As I understand Deleuze, royal science is that science sponsored by or co-opted by a ruling state—the science that enhances or enforces the power of the organs of the state, the government. It becomes the “official” truth. Nomad science is ‘outsider' science, something that challenges the official truth, and is either repressed, or tolerated as marginal, or ends up changing the official truth. So, we could say that Relativity started as a far-out nomad idea that became royal and official. DNA was well within the royal—Watson and Crick discovered its precise structure, not its existence. Nomad science today might be about para-normal phenomena, or even psychoanalysis, which is still largely ignored (marginalized) by official medical science.
	
	
	What can architects do to resist the “advent of a scientific dictatorship” or the hegemony of royal science and its architectural cousins? Participate in or encourage risky experimentation in design. A good start is reading or re-reading [Neil Spiller's recently posted article](https://lebbeuswoods.wordpress.com/2010/07/17/neil-spiller-the-great-forgetting/).
	
	
	
	
	
		1. [River](http://www.routendesign.com/scott)
		9.3.10 / 7am
		
		
		Awesome Détournement!
11. [River](http://www.routendesign.com/scott)
9.5.10 / 12am


Did I just hear you thinking about hatT  

<http://video.google.com/videoplay?docid=-5123182744103122879#>
13. [Browsing, looking and waiting « Something Good in Every Day](http://causeofhappiness.wordpress.com/2011/09/14/browsing-looking-and-waiting/)
9.14.11 / 5pm


[…] Metal working model used by James Watson and Francis Crick to determine the double-helical structure of the DNA molecule in 1953. https://lebbeuswoods.wordpress.com/2010/08/30/structure-of-life/ […]
15. [Efectos Espaciales](http://www.efectosespaciales.net/chazhuttonsfsm-this-is-james-watson-and-francis/)
8.8.12 / 12pm


[…] and some interesting words about models / science / experimentation / architecture / and stuff over at LW's blog. Tags: […]
