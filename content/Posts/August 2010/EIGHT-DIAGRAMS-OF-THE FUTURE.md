
---
title: EIGHT DIAGRAMS OF THE FUTURE
date: 2010-08-08 00:00:00
---

# EIGHT DIAGRAMS OF THE FUTURE


*Having seen the many creative responses to the [WHAT IS THIS?](https://lebbeuswoods.wordpress.com/2010/05/28/what-is-this/)* *post, I've decided to take things to another level—to up the ante, as they say—with the following:*


Who says we cannot know the future? We can, but it always a matter of interpretation, that is, of imagination. If that seems obvious, I should point out something not so obvious: that knowing the present is also always an act of imagination. We gather the facts, at least the ones available, or the ones we want, and describe what is happening around and within us. It is always an act of invention. If—following the conventions of our social group—enough people agree, then we have an accurate description of reality, the truth that Voltaire called (referring to history), “the lie commonly agreed upon.”


We can know the future to exactly the same extent that we can know the past or imagine the present—they are not interchangeable but do share the quality—owing to the structure of the human brain—of having been invented or, if you prefer, [constructed](https://lebbeuswoods.wordpress.com/2010/07/17/constructing-a-reality/).


The imagination needs some material to work with—bits and pieces are all we have. The more open to imagination they are, the more they can be interpreted, therefore the ‘truer' they are.


I am putting forward eight diagrams—the very best, most accurately constructed diagrams of the future I am capable of devising—in order to help us know what it might be. Such knowledge may serve us well. Or it may not. Knowledge always cuts both ways.


To some of you, this might seem a variation on the Rorschach test, that is, an essentially psychological exercise. To others, it might seem like the mystical reading of tea leaves, or the entrails of a ritually sacrificed goat. Fair enough, but I should note that in both of those situations, the material is created accidentally, or—if you prefer—randomly. The eight diagrams are the products of conscious design.


Another reference comes to mind, though it, too, may be only distantly related to the eight diagrams: [The Glass Bead Game](http://en.wikipedia.org/wiki/The_Glass_Bead_Game) devised by writer Hermann Hesse. In Hesse's novel, “the exact nature of the game (quoting Wikipedia) remains elusive and (its) devotees occupy a special school…. The rules of the game are only alluded to, and are so sophisticated that they are not easy to imagine. Playing the game well requires years of hard study of music, mathematics, and cultural history. Essentially the game is an abstract synthesis of all arts and scholarship. It proceeds by players making deep connections between seemingly unrelated topics.”


It is only the last sentence that will appeal to anyone today interested in a future formed by “a synthesis of all arts and scholarship…proceed[ing] by players making deep connections between seemingly unrelated topics.” Or is this nothing more than elitist propaganda, a distant echo of Plato's ‘philosopher kings” in *The Republic*? To counter that judgment, perhaps we can substitute “all people” for “players”—isn't universal education, hence universal participation, the goal, indeed the necessity, of political and social democracy? But is democracy in the future at all?


LW


Diagram 1:


[![](media/fut-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-1-2.jpg)


Diagram 2:


[![](media/fut-3-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-3-2.jpg)


Diagram 3:


[![](media/fut-4-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-4-2.jpg)


Diagram 4:


[![](media/fut-5-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-5-2.jpg)


Diagram 5:


[![](media/fut-6-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-6-21.jpg)


Diagram 6:


[![](media/fut-7-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-7-2.jpg)


Diagram 7:


[![](media/fut-8-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-8-21.jpg)


Diagram 8:


[![](media/fut-2-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/fut-2-2.jpg)



 ## Comments 
1. Kiel
8.8.10 / 5pm


The future is greater ease as a coefficient of greater complexity. This applies to all things — most importantly the formation of ideas, the woof and warp of a reality consensus-derived. As complexity liberates our wildest whims, the fixed cosmology hinted at by our continuous scientific soundings of the universe will lose solidity, shrinking in significance to be nearer the equal of our own inner model. “Inner” will itself lose meaning as one reality directly informs the other in a closed, cyclic vice versa. The unknowns and unforseeables which once escaped our imaginative ability will not only be knowable, but common currency — without any loss of density or wonder. The quality we will have gained is total Certainty — the highest kind of freedom.





	1. [korzac](http://kafee.wordpress.com)
	8.9.10 / 7pm
	
	
	Total certainty as freedom means probability one. If it is a universal attribute then ‘freedom' would be the reign of boredom: all things are known..and there is nothing to imagine.  
	
	Anyway for reasons of entropy the future has more choices than the past. So it's much more probable that in terms of entropy certainty is a scarce material so that there remains much to do for imaginative people..
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		8.9.10 / 8pm
		
		
		Korsac: Right, total freedom is (in the words of the song) “nothin' left to lose.” That is, death.  
		
		Your comment about more choices coming from entropy is intriguing. So, as the universe—and each of us—moves toward ‘heat death,' the decay of order makes possible the invention of new forms of order—is that what you have in mind?
		3. Kiel
		8.10.10 / 7am
		
		
		“The fine arts were born in the agony of boredom.” (Asimov). Certainty is not omniscience's synonym — it is an understanding that one may express, confidently, whatever one wishes or tries. The old barriers to consummating imagination are gone, clearing the ground for the creation of new, more interesting ones. This is the difference between a real, self-obtained freedom and the false Orwellian variety. Utopia is not implied by certainty. Entropy is not banished. Problem-solvers are confronted by more and better opportunities than ever before. Their light, before scattered and hectic, has been brought to a focus: now it can burn.
		5. [korzac](http://kafee.wordpress.com)
		8.11.10 / 7am
		
		
		Lebbeus: ‘…the decay of order makes possible the invention of new forms of order…' is what we write or say in everyday prose. As a visual artist I also use this anthropomorphic prose and that was indeed what I had in mind when seeking surprising pictorial forms, forms I was glad to see in your Eight Diagrams of the Future.
		
		
		But entropy is really the spread of energy putting at our disposal more microstates leading to more choices not necessarily made by human artist..
		
		
		 (BTW, entropy has an irreversible arrow of time that cannot be banned if we want to make sense of the Future.)
3. [workspace » Eight Diagrams of the Future](http://www.johndan.com/workspace/?p=1978)
8.8.10 / 6pm


[…] Woods  EIGHT DIAGRAMS OF THE FUTURE ﻿I am putting forward eight diagrams—the very best, most accurately constructed diagrams of the […]
5. [Tweets that mention EIGHT DIAGRAMS OF THE FUTURE « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/08/08/eight-diagrams-of-the-future/?utm_source=pingback&utm_campaign=L2)
8.9.10 / 10am


[…] This post was mentioned on Twitter by dpr-barcelona, KpaxsArchi. KpaxsArchi said: EIGHT DIAGRAMS OF THE FUTURE: Having seen the many creative responses to the WHAT IS THIS? post, I've decided to t… <http://bit.ly/bT8Pdl> […]
7. [Mitch](http://www.superfront.org)
8.9.10 / 2pm


#7 feels like the Enlightenment – if only it were possible to do twice!  

#6 the War on Terror never ends  

#5 nihilism  

#4 the beginning of a mega-autocracy  

#2 and #3 merely opposites of each other – fantasies  

#1 empire (and resistance) continues  

#8 consensus





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.9.10 / 8pm
	
	
	Mitch: I really like your audacity to be literal. Stories (and hi-stories) can be spun off your lines.
	
	
	
	
	
		1. [Mitch](http://www.superfront.org)
		8.13.10 / 7pm
		
		
		Thanks! I just took the drawings seriously as potential territories, bounded political spaces or charts of economic activities, frequencies of events…. There is an abstraction necessary to conceive literal reality at the scale of a whole society or planet, and we are so accustomed to seeing the same charts (time and growth), the same figure ground relationships (land and water) over and over again. And those abstractions that worked for the past 500 years are not working anymore. What does it matter where the shoreline of a continent is after oil toxifies an entire Gulf…. What does it matter the movement of a stock exchange up or down over time when the Federal Reserve has to push interest rates to zero *and* pump quantitative easing into the money supply. None of the cartesian systems are working, even at the level of the literal.
		
		
		Thank you so much for doing what you do. Your blog makes me feel sane. I appreciate it.
		
		
		Mitch
9. [Chris Teeter](http://www.metamechanics.com)
8.10.10 / 4am


1. I looked, and, behold, a door was opened in heaven: and the first voice which I heard was as it were of a trumpet talking with me; which said, We are the angel mutant, the streets for us seduction, our cause unjust and ancient!


2. I took it standing, broke commandments and he tumbled like a toy. Blood like a crimson highway  

spreading out from his forehead to the ground. What hast thou done? The voice of thy brother's blood crieth unto me from the ground.


3. There appeared a great wonder in heaven; a woman clothed with the sun, and the moon under her feet. Do you think we're robot clean? Does this face look almost mean? Is it time to be an android not a man?


4. Black dog's head on the killing bed severed and left to bleed there on fire in the corner of the world there in misery. The fire and wood are here, but where is the lamb for the burnt offering?


5. If you feel alive in a darkened room do you know the name of your solitude? If you ain't got the answer,if you don't know the truth, if you want the power then let it flow through. A casual stroll through the lunatic asylum shows that faith does not prove anything.


6. The “kingdom of Heaven” is a condition of the heart – not something that comes “upon the earth” or “after death.” But someday, somehow, someone may need me  

when my drifting days are done.


7. They had a hi-fi phono, boy, did they let it blast. Seven hundred little records, all blues, rock, rhythm and jazz. But when the sun went down, the volume went down as well, the rapid tempo of the music fell. “C'est la vie,” say the old folks, “It goes to show you never can tell”.


8. The blue bus is calling us. Driver, where you taking us?  

Heard about the old time sailor men? They eat the same thing again and again, warm beer and bread they said could raise the dead. 


lyrics, verses, qoutes – Bible, Misfits, Danzig, Nietsche, Chuck Berry, Jim Morrison, Jimmy Buffet.
11. Elizabeth P.
8.10.10 / 1pm


Sundials:


As our knowledge of the universe (the science) expands so does the complexity of our relationship with the environment (the nature), our interactions with that knowledge (our nature), and the mediating tools we use to interpret our collective rhythms (the sundials). 


After all, isn't a sundial an expression of how advanced the understandings of our surroundings have become, with an arrogance that says: ‘I can set my watch to it'?


In these premonitions I see the workings of eight sundials, at eight distinct initial locations around the globe in the outer stratosphere. From these initial conditions the sundials are set into a free fall of consistent and measurable change. The sundials of tomorrow are not fixed but instead an ephemeral shadow cast on a global scale. 


When I (future man) look into the sky I see a complex alignment with the universe. Because I am knowledgeable I can understand, interpret and say: 


Yes, it is 10:15am.
13. [VonIrminger](http://www.youtube.com/watch?v=skAh-kTF0ck)
8.11.10 / 9pm


the Repetitive and the Differential


“(…) human nature has no dynamism of its own and that psychological changes are to be understood in terms of the development of new “habits” as adoptions to new cultural patterns. Though there is no fixed human nature (…)”


…only the fixed mind…


“(…) reasons that the present is indefinite, that the future as no reality other tan as a present memory, supposes that the planet as created a few minutes ago, furnished with a humanity that ´remembers` an illusory past.”


– Fromm / – Borges


1-3 / 2-4 / 5-7 / 6-8
15. [Stratis Mortakis](http://arcsickocean.blogspot.com/)
1.29.12 / 7pm


I think they all talk about the interaction of lines, which reminds me the constructivist ideas of forming through a ‘construction' process. I could say that maybe it relates to the politics and their connection to architecture. Politics are mainly space and time driven, so that these 8 diagrams express several alternatives, or maybe the same view through several ‘viewfinders'. What i really appreciate in them is the dialogue, spatial discourse, expressing the energy of politics in space.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.29.12 / 11pm
	
	
	Stratis Mortakis: I appreciate your serious analysis of the diagrams. Some have considered them as ‘ironic' images. I consider them as revelatory, though I certainly am no prophet, nor desire to be. Time and space, the elements composing the future, as well as the present and the past, are precisely expressed as geometry, the more rudimentary the better. The evolution of the geometry in these diagrams, as well as its affective coloring, reaffirms the endurance of the human mind, but even more so the interaction of the differences between the diagrams. These are indeed political, in that they address the structure of relationships entirely within human choice. My fond wish would be that a story might be written about each diagram, and that these stories might form a sequence about our humanity and its ‘progress.' Or, we might say, what he hope its progress might be. Anyone (other than me) up to this?
17. [Stratis Mort](http://arcsickocean.blogspot.com/)
6.2.12 / 10pm


Although my comment is late, i find the work of Augustine Kofie ( <http://knowngallery.com/blog/post/augustine-kofie-%E2%80%93-working-an-angle-opening-night-photos-by-carlos-gonzalez/> ) quite intriguing.  

Are these architectural spaces rather than ‘abstract' geometry? Can one experience them in real scale?  

Quite fascinating the ruler he adds on the side of the canvas.
