
---
title: AS401  VERTICAL MANHATTAN 1
date: 2010-08-15 00:00:00
---

# AS401: VERTICAL MANHATTAN 1


*The following is part of the continuing series of posts ARCHITECTURE SCHOOL. It is the first of a three-part post about the experimental studio conducted this summer as part of the Cornell University MArch II program.*


[![](media/earthcore-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/earthcore-1a.jpg)


(above) The world deep below Manhattan.


**CORNELL MArch II, June 7-25, 2010, “Scales of the City.”** Professor Dagmar Richter, Chair of Architecture; Professor Mark Morris, Director of the MArch 2 Program; Professor Lebbeus Woods and Professor Christoph a. Kumpusch, Studio Critics. MArch II STUDENTS: Moritz Schoendorf, Leanna Brugh, Cem Kayatekin, Alkisti Douka, Zachary Emmingham, Ryan Drummond, Irina Igolnikov, Sarah Haubner, Anahita Rouzbeh, Jose Revah, Pingchuan Fu, Matthew Luck, Roger Mainor, Konrad Scheffer, Siddharth Soni, Gillard Rex Yau, Xiaoben Dai. **STUDIO PROGRAM:**


**VERTICAL MANHATTAN: between the earth and the stars**


It is convenient to think of Manhattan as a city spread out across an island, made up of streets lined with buildings, some extending upward hundreds of feet and more, with subway tunnels and other subterranean spaces coursing below. While this is not an inaccurate way of thinking about the place, it is certainly incomplete. Manhattan is a city related to the earth deep below and the sky high above, and in ways that impact those domains just as the city is impacted by them. In the present age of enlightened ecological consciousness, architects are struggling to make new connections between the design of living space and a much wider world than they have ever dealt with before. In our studio, we will work to investigate what these connections might be in relation to the urban construction called Manhattan.


Our approach will be through a focus on the vertical plane—the vertical section—simply because this connects the vastly differing scales of nature that impact or are impacted by the city.


For example, Manhattan is in a seismically active region of the earth's surface and earthquakes—which are caused by forces active deep in the earth—have the potential to damage not only buildings, but also infrastructure, such as water and power supply networks. On the other hand, decisions made in corporate boardrooms high above the city can result in deep-water drilling for oil that, when it goes wrong, can result in massive oil spills that foul oceans and their teeming life. Every day the city emits pollution that contributes to fouling even the upper levels of the planet's atmosphere. And we shouldn't forget that the project for the first atomic bomb was called the Manhattan Project, because it began in the city's advanced universities and research labs. Then there are the thousands of earth satellites that, while they have not originated in Manhattan, exist to service it and other cities like it.


On the positive side is Manhattan's potential, as a center of learning and innovation—of which our studio is one example—to participate in solving problems, and of fostering concepts and ideas that might improve the human condition in ways small and large, and thereby contribute to the well-being of nature as a whole. In short, Manhattan will continue to be a source of good and ill: it should be our task to invent ideas that contribute as much as possible to the good.


In our brief time together, we should not be intimidated by the grand scale of the context of which Manhattan is a part—between the earth and the stars—but remember that creativity itself has no scale, but radiates from the smallest source throughout the universe. That is what modern science teaches us, and what each of us instinctively knows when we are in the presence of the most original poetry and art.


(below) The world high above Manhattan:


[![](media/hub-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/hub-7.jpg)


.


**Week One**


Introductory lecture.


The studio will divide into four (4) working teams.


Each team will research and draw a vertical section through Manhattan, extending from the center of the earth to outer space, identifying the range of different regions below and above Manhattan.


Each team will then create a vertical montage of their vertical analysis through Manhattan.


**Week Two**


The studio will divide into working pairs. [Pairs will be self-chosen. If one cannot find an appropriate working partner, then one can work alone.]


Each two-person team will analyze the vertical section/montage and determine where Manhattan is significant, i.e., where it vertically ‘begins' and ‘ends.'


Each two-person team will then identify a site in which to develop a designed intervention considered important to Manhattan and/or the regions above or below that Manhattan affects or is affected by.


**Week Three**


Each two-person team will develop its intervention in the identified site.


This design will be developed in drawings, a model, and a text, emphasizing the vertical section.


Review, exhibition, and celebration of work.


.


**CALENDAR**:


June


7            m            introduction of program—-research vertical section—evening lecture


9            w            research and draw vertical section—-develop vertical montage


11            f             present vertical montage


14            m           present site and intervention concept


16            w           develop intervention design


18            f            PIN-UP


21            m           develop intervention design


23            w           develop intervention design


25            f             present intervention design—exhibition


(May 26, 2010)


.


**GLOSSARY** of terms in relation to our studio:


**research**


Gather data, in analogue and/or digital form, especially data visually represented. Note that most existing/available charts, maps, and diagrams are not only aesthetically inexpressive but also show information you may not consider relevant to your interests and estimation of importance to your thinking and understanding.


**vertical section**


A composite section cut beginning deep within the earth, passing through Manhattan, and extending to outer space above the city. This should be a drawing or sketch, made not to actual scale, but to a scale that shows your idea of relative importance and relationships. Your reference should be the city of Manhattan. It is preliminary to making the **vertical montage**.


**vertical montage**


A photo-montage based on the **vertical section**. It should show the same relative scale between earth, Manhattan, and outer space, except with photos of the relevant spaces, places, domains. The photo-montage should show potential sites for an **intervention** that relates in some way to Manhattan.



**site**


A place in the **vertical section** and **vertical montage** that you consider appropriate for a projected architectural**intervention**.


**intervention**


A projected space for human inhabitation. It should be represented by drawings and models, and supported by a programmatic statement or brief text relevant to its purpose. Because of our time limitations, but even more so the studio's focus on the vertical layering and the interactions of spaces and purposes in the vertical plane,emphasis should be placed on the vertical sectional aspect of your project. Don't lose sight of the need to relate to Manhattan, even if the intervention is elsewhere.


**develop**


This is what you have spent your previous studio time learning how to do. Work and rework your ideas and designs until they approach your aspirations.


**pin-up**


A preliminary **presentation** for the studio members and critics of in-progress work that allows for discussion and critique of your work, with the goal of helping you proceed in the most productive and creative way.


**presentation**


Your work shown in drawings, models, videos and other forms of projection that convey your concepts and their architectural/spatial development. Powerpoint presentations are discouraged, because they leave no physical presence for contemplation and discussion. Physical drawings and models are encouraged. These may be supplemented by forms of electronic projection.


The following are images excerpted from the introductory lecture by LW:


Manhattan, 2010:


[![](media/manhaerial-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/manhaerial-1.jpg)


Lower Manhattan, 2010:


[![](media/manst-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/manst-2.jpg)


Diagrammatic section of the earth's tectonic plates:


[![](media/plates-1-2ainv.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/plates-1-2ainv.jpg)


Pangaea—the original continent, two hundred fifty million years BCE:


[![](media/tec-comp-1a-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/tec-comp-1a-1.jpg)


The Blue Planet, two hundred fifty million years CE, after the continents have drifted, dispersed and the oceans have risen 1000 feet:


[![](media/tec-comp-7a-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/tec-comp-7a-2.jpg)


The earth's magnetic fields, resulting from the planet's geo-dynamism:


[![](media/magnetic-3b-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/magnetic-3b-1.jpg)


Present positions of artificial satellites and related artificial debris, 2010:


[![](media/earth-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/earth-11.jpg)


Moon, earth, sun, and beyond:


[![](media/moon-5b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/moon-5b.jpg)


LW


.



 ## Comments 
1. [AS401: VERTICAL MANHATTAN 2 « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/17/as401-vertical-manhattan-2/)
8.18.10 / 7am


[…] 08.15 AS401: VERTICAL MANHATTAN 1 […]
3. [AS401: VERTICAL MANHATTAN 1 by Lebbeus Woods | Architecture Lab](http://architecturelab.net/2010/08/18/as401-vertical-manhattan-1-by-lebbeus-woods/)
8.18.10 / 8am


[…] Check the rest of this entry here […]
5. voidinbetween
9.8.10 / 3pm


Dear LW, Could you maybe share your knowledge about planet's geo-dynamism and its relation to our built environment? perhaps books or articles to recommend? Thanks always for your post!
