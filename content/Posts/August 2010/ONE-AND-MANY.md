
---
title: ONE AND MANY
date: 2010-08-26 00:00:00 
tags: 
    - Aldous_Huxley
    - Arthur_C__Clarke
    - future
    - human_evolution
---

# ONE AND MANY


There are two stories related to the ideas of the one—the individual—and the many—groups of individuals—that are relevant to sorting out what the they might mean today.


The first is by the writer Arthur C. Clarke who in his classic science-fiction novel, “Childhood's End” writes of the evolution of humankind from individual consciousness to group consciousness. What he means by the latter is that groups will come to possess a mind, as only individuals can today. For him this will mean that humanity reaches a higher state, moving towards what the heretic Jesuit Teilhard de Chardin called the “omega point,” the endpoint of evolution when the human becomes the divine—Man becomes God, possessed of timeless and universal comprehension.


Part of Clarke's vision has to do with the long-term—really long-term—survival of the human race. Eventually, when the sun begins to expand in some millions of years, moving from a yellow to a red giant star, making the earth uninhabitable, humans will have to leave their home planet, travel to the stars and colonize other planets. The distances involved, and physical limitations of velocity—as mass approaches the speed of light, mass approaches infinity—means that if we want to reach the stars, and their earth-like planets, hundreds or thousands of light-years away, it will take many human generations to do so. Or, the more likely alternative, humans will have to travel in a different form. The form that Clarke foresees is what he calls silicon-based life, rather than carbon-based life, which is our present form. Silicon-based life is cyborgian life, the life of the computer chip as we now know it, which is immortal. Our silicon-based progeny, intelligent machines that we have created, will traverse the immense distances to other plants and, encoded with our humanity, will carry the human seed to the stars. Or so he imagines.


The second story is by Aldous Huxley who, in his classic novel of utopia, “Brave New Word,” depicts a perfect world, or at least a world perfected by science, but at the cost of human individuality. Unlike Clake, Huxley has no desire to see the eclipse of individuality by group consciousness and, indeed ends his novel with the death of “the Savage”—the lone, remaining, rebellious individual. His reflections in “Brave New World Revisited,” which considers the state of the world in the late 1950s, ends with his call to resist the advent of a “scientific dictatorship” in any and all of its forms, including coporate conformity.


Part of Huxley's vision has to do with the near-term—really near-term—survival of the human race. In “Brave New World,” he projected the end of our humanity that he describes in his novel would be some six-hundred years A.F.—After Ford. Henry Ford invented the assembly line and the singularly efficient and, if you will, in Marxian terms, demonic form of industrial production in which the individual is alienated from the product he or she makes—their individuality is subsumed in a group or collective effort.


But only thirty years after his novel, he saw it coming true. People were seduced by the promises of science to improve the human lot, even if it meant genetic manipulation and the wide use of psycho-pharmaceuticals—he called them “soma”—that would be necessary to pacify and control the human masses, to homogenize them, make them—willingly—the same. His point in “Brave New World Revisited” was chilling. This new breed of beings would not miss their individuality, with all its struggles and travails, but would be happy, content, pacified. The dictatorship would succeed, ultimately and forever.


Socialism. Capitalism. The individual. The collective. These and other terms are tossed around today in discussions, pronouncements, polemics, proposals, and projections without much of an understanding of their basis in any idea of what it means to be human within the spectrum of all living things. What is human, of course, is not so easy to pin down. Philosophers, scientists, mystics, poets and the rest of us, in our own short lives, have been working on that question, with no final definition in sight. But it has something to do with our loneness and its struggles, and thus with our individuality. At least so far.


*To be continued.*


LW


Footnote: a report on the active, ongoing search for earth-like planets:


<http://www.nytimes.com/2010/08/27/science/space/27planet.html?ref=science>


[![](media/earth-like-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/earth-like-1.jpg)


#Aldous_Huxley #Arthur_C__Clarke #future #human_evolution
 ## Comments 
1. [chaz](http://chazhuttonsfsm.tumblr.com/)
8.26.10 / 6am


Possibly another relevant story: ‘The Last Question' by Isaac Asimov.  

<http://www.multivax.com/last_question.html>
3. [Ian Christopher Thomas](http://www.ianchristopher.us)
8.27.10 / 5pm


Perhaps Margaret Atwood's novel Oryx and Crake is also relevant in that it thematically examines the social, economic, scientific, and ethical consequences of these topics related to technology.
5. [Diego](http://opacos.wordpress.com)
9.19.10 / 4pm


Reading this post I remember “The invincible”, an enigmatic book of Stanislav Lem where the individual consciousness confronts the group consciousness.
7. Ben Dronsick
9.23.10 / 3pm


Taken from the NY Times article provided, I found this process as quoted interesting: “…looking for wobbles in the wavelength of light from the.., gravitational tugging of unseen planets.”
