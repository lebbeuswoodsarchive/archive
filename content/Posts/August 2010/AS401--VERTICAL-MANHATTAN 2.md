
---
title: AS401  VERTICAL MANHATTAN 2
date: 2010-08-17 00:00:00
---

# AS401: VERTICAL MANHATTAN 2


*The following is part of the continuing series of posts ARCHITECTURE SCHOOL. It is the second of a three-part post about the experimental studio conducted this summer as part of the Cornell University MArchII program.*


**FINAL REVIEW, VERTICAL MANHATTAN DESIGN STUDIO, June 25, 2010. Visiting Critics:** Steven Holl, Professor, Columbia University; Michael Bell, Professor, Columbia University; Anthony Titus, Professor, The Cooper Union; Eunjeong Seong, Professor, Pratt Institute and Rensselaer Polytechnic Institute. **Critics from Cornell University:** Dagmar Richter, Professor and Chair of Architecture; Mark Morris, Professor and Director of the MArchII Program. **Studio Critics:** Lebbeus Woods, Professor, The Cooper Union; Christoph a. Kumpusch, Professor, Cornell University. **MArchII Students:** Moritz Schoendorf, Leanna Brugh, Cem Kayatekin, Alkisti Douka, Zachary Emmingham, Ryan Drummond, Irina Igolnikov, Sarah Haubner, Anahita Rouzbeh, Jose Revah, Pingchuan Fu, Matthew Luck, Roger Mainor, Konrad Scheffer, Siddharth Soni, Gillard Rex Yau, Xiaoben Dai.


(below) Studio Critics Christoph a. Kumpusch and Lebbeus Woods at a midpoint table review:


[![](media/finrev-0.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-0.jpg)


Studio Critic Lebbeus Woods introducing the Final Review:


[![](media/finrev-1a.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-1a.jpg)



The review was organized as an exhibition of the seven collaborative projects made in the studio, with brief spoken presentations—given without a break—by the project teams to the critics on the review and their studio colleagues. This was followed by a discussion among all present of the work and concepts presented, as well as the general premises of the studio. The idea was not to dissect in detail projects on a one-by-one basis—a method deemed inappropriate for exploratory, speculative work—but to identify guiding principles for expanding architecture thought and design technique, using the specific projects as examples. This approach was very much aligned with the basic goal of the studio, which was to conduct graduate level research. Of course, it put a heavy burden on the critics to interpret the projects in terms of broader aims and goals they considered important to architecture. The students, for their part, had—without exception—worked hard in the compressed time-frame of the studio, and created highly imaginative and provocative projects in response to their mandate to consider ‘the scales of the city” defined in the Vertical Manhattan [program](https://lebbeuswoods.wordpress.com/2010/08/15/as401-vertical-manhattan-1/). Altogether, it made for a lively few hours of often contentious discussion.


Scenes from the event:


[![](media/finrev-16.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-16.jpg)


.


[![](media/finrev-15.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-15.jpg)


.


[![](media/finrev-141.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-141.jpg)


(below) BRUGH and YAU, *Satellite City:*


[![](media/finrev-17.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-17.jpg)


[![](media/rl-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rl-1.jpg)


[![](media/rl-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rl-3.jpg)


(below) HAUBNER and SCHEFFER, *Times Square Machine:*


[![](media/finrev-18.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-18.jpg)


[![](media/sk-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/sk-3.jpg)


[![](media/sk-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/sk-1.jpg)


(below) ROUZBEH, MAINOR, and SONI, *Vertical Manhattan:*


[![](media/finrev-13a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/finrev-13a.jpg)


[![](media/ars-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ars-1.jpg)


[![](media/ars-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ars-2.jpg)


[![](media/ars-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/ars-3.jpg)


(below) IGOLNIKOV and LUCK,*Instruments of Many Folds:*


[![](media/finrev-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/finrev-2.jpg)


[![](media/il-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/il-1.jpg)


[![](media/il-2det.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/il-2det.jpg)


[![](media/il-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/il-3.jpg)


[![](media/il-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/il-5.jpg)


(below) DRUMMOND, REVAH, and EMMINGHAM, *Folded City:*


[![](media/finrev-19.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-19.jpg)


[![](media/rjz-5adet.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rjz-5adet.jpg)


[![](media/rzj-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rzj-3a.jpg)


[![](media/rjz-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rjz-4a.jpg)


[![](media/rzj-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/rzj-2a.jpg)


[![](media/finrev-5a.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-5a.jpg)


(below) DAI, SCHOENDORF, and FU, *Planet Manhattan (after Dante's Divine Comedy)*:


[![](media/finrev-6.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-6.jpg)


[![](media/pxm-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/pxm-2.jpg)


[![](media/finrev-7.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-7.jpg)


[![](media/pxm-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/pxm-11.jpg)


[![](media/finrev-9.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-9.jpg)


(below) DOUKA and KAYATEKIN, *Soft Architecture:*


[![](media/finrev-27.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-27.jpg)


[![](media/alkisti-cem-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/alkisti-cem-1a.jpg)


[![](media/alkisti-cem-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/alkisti-cem-4a.jpg)


[![](media/alkisti-cem-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/alkisti-cem-6a.jpg)


[![](media/finrev-25a.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-25a.jpg)


(below, b. to f.) Michael Bell, Eunjeong Seong, Steven Holl, Mark Morris, Dagmar Richter:


[![](media/finrev-10a.jpg)](http://cornellm2sm10.files.wordpress.com/2010/07/finrev-10a.jpg)



 ## Comments 
1. [Chris Teeter](http://www.metamechanics.com)
8.18.10 / 2am


the graphics for the Soft Architecture project are incredible.





	1. [Steve Pitera](http://augmentation.tumblr.com/)
	8.18.10 / 4pm
	
	
	I was going to mention that the soft architecture images look amazing and in-depth!
	
	
	And I will add that the 3rd graphic reminds me of the guide bearing for a plunge router bit. Except multiplied by two and attached is an arm that rotated 360 degrees that lets the piece run along the walls mapping out places to cut.
3. [Tweets that mention AS401: VERTICAL MANHATTAN 2: The following is part of the continuing series of posts ARCHITECTURE SCHOOL. It is th... -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/08/17/as401-vertical-manhattan-2/?utm_source=pingback&utm_campaign=L2)
8.18.10 / 6am


[…] This post was mentioned on Twitter by miha bot, kpaxsarchi. kpaxsarchi said: AS401: VERTICAL MANHATTAN 2: The following is part of the continuing series of posts ARCHITECTURE SCHOOL. It is th… <http://bit.ly/azhDD8> […]
5. Stephen
8.18.10 / 5pm


As an architect and an artist, I love this kind of studio project. I wish I could have been there for the discussion.  

While seeing this kind of exploration lifts my spirits, I am ufortunately brought back to earth with the reality that the world of architectural practice is all too mundane.  

I do believe that there is always room for exploration and that there is a great need to push the limits of what architecture can be.  

My advice to these bright students is to embrace the mundane and make something extraordinary.
7. TMU
8.20.10 / 6am


It's so strange.  

Why everybody wear black and white?  

Is there dress code in this school?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.20.10 / 4pm
	
	
	TMU: Architects are famous for preferring black. In this case black and white clothing didn't draw attention away from the powerful images exhibited on the walls. No, there was no dress code—just personal, aesthetic choices.
9. [Diego](http://opacos.wordpress.com/)
8.27.10 / 3pm


I believe that this kind of approach to the arquitectonic process is the only one that can improve the reality, because is poetic person in charge, also in the case of residential projects, ideas can survive in a latent way that users can feel.  

apologize my english.


Kind regards


Diego Germade
11. spencer
9.30.10 / 6pm


Lebbeus,


Did any of the projects focus downward? It looks like all shown project upward in some manner.
13. Pedro Esteban
4.24.11 / 12am


no idea what all those things are, but my open mouth will remain for days. I came often to this post trying to decoded this but anything jajaajaajajaj
