
---
title: THE EXPERIMENTAL
date: 2010-08-12 00:00:00
---

# THE EXPERIMENTAL


[![](media/tm-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/tm-12.jpg)


(above) Experimental architecture hand-drawing by Thom Mayne of Morphosis, (1991).


.


The task of the experimental architect is to take us to places and spaces we haven't been before. That is more difficult than it sounds, particularly in this age of hyper-rendering by computer that can also look back over, and exploit ad infinitum, a long history of imaginative and speculative architectural design. It is also an age when many social problems—such as the rapid growth of urban slums and the need of low-cost housing for what used to be called the ‘working class'—remain not only unsolved but unaddressed. So, we might ask, why should we even care to make, let alone support with our interest, more or less abstract speculations about new and unfamiliar kinds of spaces?


This question evokes the common argument against the exploration of outer space—to the moon, Mars, and beyond. With so many problems here on Earth, why should we devote precious financial and intellectual resources to off-planet exploration? What's to be gained, in terms of our terrestrial concerns? Better that we devote ourselves to ‘real-world' problems. It's a hard argument to counter. Indeed, it's clear that we should put the problems of our home planet first. Then, when we have those solved or at least have them on the right track, we can turn our attention to the stars.


This, in turn, is something like the argument against government funding of art. We understand that paintings, sculptures, poetry and the like are luxury goods—only the well-off can afford to take the time for them. Most of us are working too hard to make ends meet to afford the luxury of time, and price of tickets, to go to museums and concerts where art is displayed in plush settings. Better that the taxpayers' money go to solving urgent problems like poverty and sub-standard education for our children. Well, yes.


The problem with these arguments is twofold. First, if we have to wait until the world is made right before we can afford the satisfaction of beauty (in whatever terms), we will never have it, because the world will never be made right enough. Second—and this is the more subtle point—it may be that the apprehension of beauty in art, music, poetry, even architecture, is necessary to solve the grittier real-world problems. The experience of beauty–especially difficult or [‘terrible'](https://lebbeuswoods.wordpress.com/2010/07/24/terrible-beauty-2-the-ineffable-2/) beauty—is one that gives us a sense of personal connection to a wider world. No doubt this sense of belonging to a world inhabited by a complex multiplicity of people and things inspires us and gives us the desire to concretize our relationships beyond the fleeting moments given by music and art, or, say an experimental architectural drawing. Without art to broaden our world-view we might well stay mired in our narrow personal problems, isolated and apathetic.


If this seems esoteric, then we should consider the ‘spin-off' effects of many forms of research and experimental projects. The Apollo program that put men on the moon in the late1960s is a good example. A decade or so before the first moon landing in 1969, the very idea of rockets and the exploration of the moon and other planets was the stuff of pulp science-fiction. However, a handful of serious scientists, led by Willey Ley and Werner von Braun, began to speculate on practical ways to go to the moon in the early 1950s, long before the vicissitudes of Cold War competition with the USSR spurred the US government to actually make it happen. The Apollo and the other programs that led up to it were very expensive, and there were many who questioned their value, though criticism was muted in the general atmosphere of patriotic fervor. Americans were inspired by visions of ‘manifest destiny,' that it was they who were destined to conquer a new frontier in the name of “all mankind.” Interestingly, the 60s was also a period of political and social upheaval and change, giving us another, malevolent upsurge of manifest destiny, the Vietnam War, but also the enactment of revolutionary civil rights laws.


In the end, as we know, the moon was ‘conquered,' not once but numerous times. With ‘mission accomplished,' and home-world problems pushing to the forefront, the desire to continue planetary exploration faded, the rockets and technologies supporting the moon flights languished, and fell into obsolescence. While it is hard to argue with critics who say that nothing much was accomplished by the act of landing men on the moon, there is plenty of evidence that the technological spin-offs of the space programs have been significant, impacting society particularly in the development of telecommunications, computers and earth-satellites.


There is a more critical reason for experimental projects of different kinds than the practical benefits that may—or may not—result from them. The truth is that most experiments lead nowhere and judged from a strict cost-benefit viewpoint are a waste. However, learning and invention are notoriously inefficient, requiring many failed attempts and dead-ended explorations to find one that is fertile enough to open out onto a rich new landscape of possibilities. If a society is unwilling to tolerate such waste it will stagnate. In today's world, which is under tremendous pressures of change, a vital and growing society not only tolerates but actively supports experimentation as the only way to transform the difficulties created by change into creative opportunities to enhance and deepen human experience. This is doubly true for the field of architecture which, charged with continuously remaking the world, is at the forefront of this struggle.


LW


.


(below) Drawing/construction by LW from the *San Francisco: Inhabiting the Quake* series (1995):


[![](media/sf4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/sf4.jpg)


.


See and hear Larry Rinder's commentary on this project:


<http://blog.sfmoma.org/2010/07/75-reasons-larry-rinder/>



 ## Comments 
1. [chris teeter](http://www.metamechanics.com)
8.13.10 / 12am


Only one crit do I remember from undergrad – good architecture is experimental.


There was a recent news article on this bendable flexible thin glass that made the inventor little money in the 60's, now its one of the hottest materials for visual media (like an ipad)


Morphosis architecture looks like that drawing
3. [dessalles](http://dessalles.com/2010/08/13/lebbeus-wood-the-experimental/)
8.13.10 / 4pm


[…] read the entire essay, The Experimental » […]
5. [River](http://www.routendesign.com/scott)
8.14.10 / 8am


Blah, blah, blah, Coalition of the willing, blah, blah , blah:  

<http://coalitionofthewilling.org.uk/>
7. [River](http://www.routendesign.com/scott)
8.14.10 / 11pm


They always quoth that architecture is frozen music. Well, what happens when it all thaws out? Hold your ears!
9. [Michael Phllip Pearce](http://www.carbon-vudu.com)
8.15.10 / 9pm


“The Journey is the Destination” Dan Eldon's story, which I hear resonating in my mind as I read it is not so much the experiment itself but the byproducts of it–in my words of course. 


[SELF] We are living experiments, reflecting back on our actions, tuning our work ethics, our behavior patterns, remolding our molds…a living conscious experiment. 


[ARCHITECTURE]Then there is design build where you can see the product go down the assembly line, critique it, judge the process, and modify the design before, during and post construction–it evolves and becomes better, more efficient. 


Although I think it is rare that Architecture makes that experimental loop. Perhaps the wiser architects can manage the loop and manage the design into building by understanding all the consultants and builders feedback.


But, not as nearly as well as the one who does the design and builds it first hand, having their hands in every step of the process, watching it evolve and tuning the efficiency by listening to how it becomes. To me this is the experiment, this is the first prototype. The byproducts reside in within person's precept, or team's summation for those who can afford the overhead and its consequences. 


The byproducts always fascinated me, all the sketches, models, and concepts that did not make it into the design–fascinating. Experimentation is the insightful journey.


As for cost and production from outer space to our own abandoned architecture is that even controllable? Maybe that is the point for discussion. Enjoy Tom Maynes composite PSE paintings and how they reflect his models, very similar to you LW…it is like it as if it were a experiment, of self expression. Thanks for the posts. mp
11. [Oliver](http://dronelab.net)
8.22.10 / 12pm


“Its [modernism's] arts have been promoted and accepted as ‘experimental.' The word stands for endless efforts to be different; it is one of the many misnomers of our time. An experiment is conducted under rigorous conditions; it follows a method, relies on others' most recent research, and is subject to review by peers. The artist's effort is entirely individual and uncontrolled. It is barely trial and error, since there exist no standards by which error can be gauged and a better trial made.” –Jacques Barzun


I will, however, admit that architecture operates on a level quite different from the other arts in this respect.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.22.10 / 1pm
	
	
	Oliver: In this quotation, Barzun is not only wrong but has an idea of scientific ‘rigor' that he gets wrong, too. On a routine level, scientific experiments are plodding and dull and serve only to build up a database—somewhat akin to the design methodology of designing known typologies, such as office buildings, which get many ‘peer reviews' (building departments, for one). But at the higher levels, scientific experiments are much more ‘individual' and ‘trial and error'—say, Einstein's General Relativity theory and proof, or the discovery of DNA's structure by Crick and Watson—and at first get few peer reviews because geniuses have few peers.  
	
	Also, it's part of Barzun's misconception that artists are not methodical—in fact, any good artist, in whatever medium, invents methods and techniques that are developed in work after work, experiment after experiment—think of Picasso, DeKooning, Hitchcock, et al.—they don't just make one-off works—and by working methodically they create the very standards by which their works can be judged.  
	
	Jacques must have been having a bad day when he wrote these lines!
	
	
	
	
	
		1. [Oliver](http://dronelab.net)
		8.23.10 / 11am
		
		
		Yes, I was reluctant about posting that quote too as the definition of experimental architecture put forward in your post escapes his criticism. However, I'm always fond of injecting a bit of noise into all interesting discussions I stumble upon. Ideas tend to develop the most when faced with an opponent.
		
		
		I will still defend my stance that most architecture today dubbed as experimental is too capricious to deserve the title. To challenge the existing is easy; to propose a road forward is harder. Too few attempt at the latter.
		
		
		I will admit that I am still tossing around with the fundamental question here, which is whether experimental architecture shall be considered one who puts forward anwers or asks questions. I recall faintly that you once argued the merit of architecture from the questions and issues that arise from it. Well, this separates it distinctly from experimental science, whose mission is to suggest solutions to problems and anomalies present in the current understanding of the universe. Since the very word “experiment” is one of scientific origin, the incompatibility begs me the question whether architecture even needs it in order to be visionary.
		
		
		I do like the thought though that beauty in art can be a catalyst to fight the horrors and injustices of reality. Beauty as attaching us to the world and teaching us to dwell in it, rather than an escape out of or away from it. (Forgive me for putting words in your mouth, it is simply my interpretation of your post.)
		
		
		Well, this didn't make much sense. Keep writing, mr Woods, always an interesting read.
13. [THE DREAMS THAT STUFF IS MADE OF « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/04/the-dreams-that-stuff-is-made-of/)
1.4.11 / 1am


[…] going to trot out the arguments for experimentation that I've made in many posts and indeed quite recently. Instead, I'd like to address some specifics of the Conflict Space series of drawings, shown […]
