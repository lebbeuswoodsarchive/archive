
---
title: BY HAND
date: 2010-08-22 00:00:00
---

# BY HAND


It is very difficult to imagine, in this age of word-processing, that there was a time when people wrote anything and everything by hand. Yet it is so. All the great works of Western thought—from Aristotle to Shakespeare to Locke to Einstein—were formulated by putting a hand-held stylus to parchment or, later, to paper. The keyword here is ‘formulated,' because the act of writing is the act of thinking. We cannot think everything out in the mind and then simply copy it down in written form. Without the act of writing, thoughts remain unformed and incomplete. So—as Marshall McLuhan noted—the way we write is part of the content of what we write—“the medium is the message” or at least a part of it.


This is a reality shaped by nuance. After all, is not the word ‘air” the same in meaning regardless of whether it is written by hand or word-processor or, for that matter, set in lead type on a linotype press or on a modern digital press and printed on a book page? Yes, air means air, in whatever medium it appears. If all we are interested in is getting the conventional meaning of a word, it matters little how it appears to us, and we can believe that nothing of consequence is lost in the translation from handwriting to printing. If we consider, though, how different typefaces affect the meaning of a word, say, **AIR** instead of ***air***, we have to grant that the meaning is subtly affected by the form in which it appears, especially in relation to its context. If that is the case, then the handwritten word is subtly different from the typeset word—perhaps because of the imprint of the author's personality or, even more subtly, the effect of different shapes or colors on our understanding of the precise meanings of words.


As we know, though, the impulse of Western civilization is towards standardization, in the same spirit as industrialization, mass-production, and consumerist conformity, which through franchises and global advertising, presents the same products to people everywhere, however different they may be or aspire to be. Their acceptance of (buying and living with) the product, or the meaning of the word (read in standardized, typeset form) enforces a homogeneity of understanding, a social bond that transcends individual idiosyncrasies in favor of commonality and community. The examples given below emphasize the difficulty of reading  the handwritten, idiosyncratic form of text and indeed cry out for a common, accessible form that we can share and discuss and in some practical, cultural way, use.


Still, we must wonder, what do we lose in the process of homogenization and standardization? Is it anything important for us to know, to experience? Also, in this age of digitalization, which can present us with the idiosyncratic as easily as the standardized and the mass-produced, do we any more need to sacrifice the personal nuance to achieve accessibility and common clarity? Can we not have both?


.


(below) The three beginning manuscript pages, with multiple corrections by the author, of *A Christmas Carol* by Charles Dickens:


[![](media/dickens-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/dickens-1a.jpg)


[![](media/dicken-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/dicken-2a.jpg)


[![](media/dickens-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/dickens-3a.jpg)


For the other sixty-six pages of *A Christmas Carol*, visit [this website](http://documents.nytimes.com/looking-over-the-shoulder-of-charles-dickens-the-man-who-wrote-of-a-christmas-carol#p=1).


.


(below) A manuscript page of Albert Einstein's paper on *The General Theory of Relativity*, with corrections by the author:


[![](media/einstein-1b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/einstein-1b.jpg)


.


(below) Typescript by T. S. Eliot of  a manuscript page of his poem *The Wasteland*, with critical comments by Ezra Pound requested by Eliot:


[![](media/eliot1b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/eliot1b.jpg)


(below) Typeset version of the above manuscript page, including Pound's comments:


[![](media/eliot2b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/08/eliot2b.jpg)



 ## Comments 
1. Jeremy Ashkenas
8.22.10 / 11pm


To contribute one, here's the longhand of a Mark Twain essay about being interviewed, unpublished for over a century:


<http://www.pbs.org/newshour/rundown/documents/mark-twain-concerning-the-interview.html>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.23.10 / 12am
	
	
	Jeremy Ashkenas: Thank you—a wonderful contribution!
3. [Chris Teeter](http://www.metamechanics.com)
8.23.10 / 4am


I am replacing “writing” with “drawing”


“The keyword here is ‘formulated,' because the act of drawing is the act of thinking. We cannot think everything out in the mind and then simply copy it down in drawn form.”


first a disclaimer: T.S. Eliot and Ezra Pound made poetry so damn intellectual you would have to read about 6 volumes of Phd commentary on one poem like “Prufrock and other Observations” just to get the gist; and at that point if you get it, you're a young T.S. Eliot for a moment…


(maybe this isn't your point Lebbeus)


I couldn't disagree more with the modified statement “We cannot think everything out in the mind and then simply copy it down in drawn form.”  

— Matter fact, if I had to draw everything to figure it out I'd be embarrased to call myself an architect. I think that's called being a draftsman, you know the low rank member of the profession.


I'm just wondering when will the obsession with drawings finally be abandoned and we go on to thinking about architecture?


Drawing is a skill just like writing, which every architect and writer should master, but it's not the point, just like 3D modeling and speech are important skills.


Just waiting for the day when the mind can be connected right up to the computer and imaginations of architecture are presented virtually as quick as the mind can envision them…


Have you ever worked with an architect who just talks the contractor thru the job? is he bullshitting or is drawing just a waste of time? (besides legal oblications…)


How much of today's architecture is a direct result of the process of drawing and not a result of planned architecture?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	8.23.10 / 3pm
	
	
	Chris: I didn't change “writing” for “drawing” for some of the reasons you give in your comment, though it is tempting. In the post I'm thinking of how nuances affect a commonly held vocabulary, such as we find in an established verbal language. No such vocabulary exists, at least not as clearly, in the language(s) of drawing.
	
	
	The idea that architecture can exist without drawing, as a direct verbal communication between architect and builder, has been working for a few thousand years—vernacular architecture. Refer again to Bernard Rudolfsky's book “Architecture Without Architects.” While I agree that the fantasy that ideas can go directly from our brains to bricks and mortar (or carbon-fiber) has some appeal, I worry about all the bad ideas architects have running around in their heads that I would hate to see realized. Also, concerning the good ideas that don't get built, I'm wondering what happens to them without drawing as a means of documentation.
	
	
	Finally, I guess you and Frank Lloyd Wright share the same remarkable ability to work a design out fully in your minds before making a drawing. For most of us, drawing is a necessary ‘crutch' in working out our thoughts.
5. Stephen
8.23.10 / 5pm


I strongly agree that the act writing (or drawing) is an act of thinking. It seems odd to even argue that it is not always an act of thinking. Even if one were “jacked” into a computer, it would be likely that thoughts would still need to be worked out on a virtual sketchpad. Even when one is thinking things out in their head, they are “writing” or “drawing” on a virtual medium.  

I don't have any intellectual argument for why this is so, just personal experience and observation.  

Writing or drawing by hands feels much different than entering information into the machine. This has been a great thing for me, being able to switch back and forth to see things in different ways.  

In addition to drawing, I paint with oils to force myself to slow down and feel the image and the idea through direct contact with the canvas.  

As long as humans are born with at least one arm and hand we will continue to think through writing and drawing.
7. [River](http://www.routendesign.com/scott)
8.24.10 / 6am


?. Let me be blunt. I'm good at that by accident occasionally. I hated computers until I realized that they were introducing me to time. Now I'm onto the next thing. I have a lot of super special old sketches to share, but they are water under the bridge. What are you doing this second? Are you… ? The languages I was brought up to understand in school are obsolete. Kids these days are on a different wavelength. MUCH more musical. I hope my observations don't piss anyone off. I respect writing and drawing, and I believe they will never lose place. But thoughts in realtime, preferably hanging out by the fire with drums.. lead more into cymatics. TV knows this too, and they use it religiously.





	1. [River](http://www.routendesign.com/scott)
	8.24.10 / 6am
	
	
	Break the mold:  
	
	<http://imaginingthetenthdimension.blogspot.com/2010/08/flow-and-now.html>
9. Stephen
8.24.10 / 3pm


Hey kids, slow down and observe the world around you. Sketching is a great way to do this as is writing thoughts in a journal.  

The internet, TV and other electronic media are roasting your brains. Leave your phone at home, take the ear buds out and listen and look.  

We are loosing our ability to be truely creative and I can't help but think that depending too much on computers or mass media is distracting us from what is real.  

I sound like an old guy…is 42 old?
11. [chris teeter](http://www.metamechanics.com)
8.25.10 / 12am


Can't find reply option on blackberry.


Lebbeus thank you for letting me ramble on your site. I see your nuanced point and funny thing is-nuances in the computer can only be achieved at levels of complex logic and not really at ontological or intuitive levels, or at least my experience would suggest so. At 1-2 am I read Kant then Husserl and back and forth I go. 


Those pages you show are like a worn wooden tread in a bar on the way to the bathroom…too many factors to compute the immediate ontological fact. So I apologize for spinning off into drawings (I replaced the words)


FLW and I used to think a lot of contractors I had worked for or met. I now realize many contractors just talk a good game, but I am pretty sure the framer I worked for in Colorado knew it before someone drew it. Maybe he had many memories of framing, enough to re-organize the images like in dreams or was it the acid he would drop, seemingly having no affect on his standard human functions. Guys I know in film or comics or even computer guys seem very capable of imagining architecture, they often do one of those, well I can't draw very well but you should get what they are saying.


River brings up a very good point of which I am fully convinced of, the amount of media shot to the back of our eyes (literally), hearing a live music performance perfectly at a request, the internet allowing extremely quick history to be written, the amount of electromagnetic waves flowing through the atmosphere…


Does that make us smarter and less aware or more aware and less smart, or both more aware and smarter?


But architecure even if you envisioned it, when finally built is extremely nuanced, and I don't mean form and space, but the ultimate material outcome of many social, politcal, economical, physical, and design issues all altering the final state of an architecture.


Thanks again..lebbeus two epiphanies
13. [River](http://www.routendesign.com/scott)
8.27.10 / 3am


Epiphanies, yes.


There are always one or two when I pay attention to your blog. I hate to bring the conversation down to what's real, but time seems extremely short to me. And I'm up in arms. It seems this is ridiculously simpler to me from my point:  

<http://planetark.org/wen/59271>
15. matei23
8.28.10 / 6am


I wanted to add a new way of writing air.  

The hyperlinked version, linking to wikipedia or to a certain article. In this way the meaning of the word is clearly changed. The jump in medium not only from the typewriter to the computer, but to writing for the web creates a new type of writing and a new type of reading. And while TS Eliot got Pound's comments on his writing, you, Professor Woods, got all of ours (for better or worse).
17. [Diego](http://opacos.wordpress.com)
8.31.10 / 5pm


Curiously I am reading “Thechnics and civilization” of Lewis Mumford and in chapter ” War and invention” explains the following question: The press of printing went a powerful agent to produce uniformity in the language and with it, gradually, in the thought. The estandarzación, the production in great scale, and the capitalist company appeared with the press of printing.


Kind regards,


Diego
19. spencer
9.29.10 / 5pm


anyone think this is a similar question that was asked with pencils were invented from charcoal and berries or when ink was invented from pencils, or the printing press yet later?


Although I am an ardent supported of knowing how to write [draw] by hand I am still convinced that the computer is an evolution of the pencil. As Lebbeus points out, the same words are very similar when written, and, actually, they are closer to being the same now than ever before. What was once expression in how we created individual letters and combined them into words is what is lost to gain of a little more clarity (both in form and meaning).


I still do many of the same activities on the computer that I once did in a journal. I feel no loss in content nor skill. I can always pick up my pencil or pen and do the things I used to do. The difference is a I embrace both in the same methodology. 


I have a friend who is a gifted renderer. Unlike anyone I have ever known. When I met him he was teaching himself to draw with both hands at the same time because each hand gave a different result to what he drew. Now, he combines technology into his process. He draws both with the computer and by his hand(s). He scans the things he draws to use the computer as a digital third hand for added technique and speed.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	9.29.10 / 10pm
	
	
	spencer: Your friend's approach seems like a good one to me. It's never a case of either/or. Let's use all the tools, and possibilities, we have. The only question is, to what purpose?
21. [Ο μυς της γραφής | the book paper](http://thebookpaper.com/?p=916)
11.29.11 / 10pm


[…] Lebbeus Woods   If you enjoyed this article, please consider sharing […]
