
---
title: RIEA  the back story
date: 2011-06-25 00:00:00 
tags: 
    - architectural_research
    - experimental_architecture
    - RIEA
---

# RIEA: the back story


[Heinz von Foerster](http://en.wikipedia.org/wiki/Heinz_von_Foerster) once told me, “You are an epistemologist.”


His opinion, in this case and others, meant a lot to me. On the other hand, I wasn't sure what an epistemologist was. I did know that it was someone interested in knowledge, and the nature of knowledge. What is knowledge? How do we know it or anything else? These are the questions an epistemologist asks. But, what good are the answers? How do we know the answers are true? More questions for the epistemologist. It seems that the answers form a kind of circular riddle: I know because I know, I think, therefore I am. Oh, yes, Descartes and all those long ago ponderers who wanted to understand understanding and place the human being squarely at the center of the world-labyrinth. Theseus was not an epistemologist, because Ariadne gave him the answer to the problem he had to solve—how to escape from the labyrinth, that is, how to solve its spatial riddle. Ariadne was the epistemologist, because she figured out the *method* of understanding how to escape from Minos' diabolical trap.


Epistemologists are concerned not only with the answers to a question, but how you get to the answers, and once we get there, how we know they are true. We learn little from a raw answer to a given problem, because every problem is different in its details. But we learn much from the method of finding an answer, because it anticipates variables in similar problems.


Nearly twenty-three years ago, when Olive Brown and I founded the Research Institute for Experimental Architecture (RIEA), it was an epistemological approach to architecture that we had foremost in mind. Architecture is, before anything else, a field of knowledge. Olive, a psychologist, and I were married at the time, and she had a strong commitment to the idea of experimental architecture. She was instrumental in fund-raising and organizing the Institute's events. The founding event was a two-day meeting of experimental architects in upstate Oneonta, New York. We had rented a cottage and a barn on an old rural estate and thought it perfect as the place of what later came to be called the First Conference of RIEA. Motels in the local villages provided shelter for a select few invited architects coming from New York, Los Angeles, London—architects who had not built much if anything but had made ground-breaking conceptual projects. The idea of the meeting was sort out what experimental architecture might be. Were their ideas we had in common? Did we share any principles that guided the topics of our experimental projects? Did it make sense to somehow collaborate, or was it better if we remained solo workers, pursuing our separate, idiosyncratic paths? And of course, did it make sense to establish an “institute of experimental architecture?” Would institutionalizing the idea of experimentation stiffen or even kill the free spirit considered necessary for experimenting?


In science, there are many institutes for research and experimentation. If the development of science were left up to lone and isolated individuals, it would not be getting very far. Today, in science, there are still solo innovators, but their ideas must be taken up and tested by others to be considered valid, true, even useful. Scientists dedicated to exploration know that they need each other and must share information about concepts and techniques with one another, often through the auspices of institutes. At this stage, architects do not. They model themselves on the traditional figure of the artist as an inspired loner. The main task of the artist is to produce uniquely original, inimitable masterpieces, not contributions to a body of knowledge. Even when the gifted artist is backed up by a production team, the goal is the same. The founding idea of RIEA was to challenge that model, to create a collaborative spirit that could advance architecture by meeting together many challenges facing it today that cannot be met by inspired loners.


[RIEA still exists.](http://www.riea.ch/) Olive Brown and I are no longer married, or active in it. The founding questions still remain, largely unanswered. The challenges are being addressed by a new generation of highly dedicated, collaborating architects. Still, they remain largely unmet. It was a much more difficult task than I ever imagined. Yet, the potential remains.


LW


[![](media/oneonta-first-conf-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/oneonta-first-conf-2.jpg)


(above) Image taken from *[RIEA: The First Conference](http://www.amazon.com/RIEA-Conference-Princeton-Arch-Staff/dp/1878271008/ref=sr_1_4?ie=UTF8&qid=1309014373&sr=8-4)*[, Princeton Architectural Press, New York, 1989](http://www.amazon.com/RIEA-Conference-Princeton-Arch-Staff/dp/1878271008/ref=sr_1_4?ie=UTF8&qid=1309014373&sr=8-4). (Seated around the table, counterclockwise: Gordon Gilbert, Michael Webb, Hani Rashid, Michael Sorkin, Lebbeus Woods, Ken Kaplan, Ted Krueger, Peter Cook, Neil Denari.



#architectural_research #experimental_architecture #RIEA
 ## Comments 
1. tom
6.25.11 / 7pm


The story of Theseus can be looked at differently than the conventionally heroic way it's usually looked at- ironically, as a send up of Euclidian space; maybe how it was originally intended. The minotaur, standing for Euclid; the bull, the essentially earth bound.


You can relate the irony in Kafka to Descartes; as a criticism of.


>>Would institutionalizing the idea of experimentation stiffen or even kill the free spirit considered necessary for experimenting?<<  

This made me think of how much more I enjoyed visiting Storefront before its renovation.
3. [David Alan Ross](http://www.code-a.com)
6.28.11 / 4pm


This session—the first of many RIEA synaptic pulse bombs—is a rare look inside the (original) outside that rocked academia full-tilt. A direct-feed: no smiling for the camera, no John Hancock's, no Magna Carta—just raw spatial pursuit. Of course the door is wide open, both then and now. To new models and future barnstorming!
