
---
title: DRAWINGS, STORIES 4
date: 2011-06-24 00:00:00 
tags: 
    - buildings
    - new_cities
    - story-telling
---

# DRAWINGS, STORIES 4


[![](media/1-4simlandscapes-1-2-det-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/1-4simlandscapes-1-2-det-1a.jpg)


[![](media/1-4simlandscapes-1-2-det-2b1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/1-4simlandscapes-1-2-det-2b1.jpg)


 


This is what the urban dweller said to me: “Sure, the new ways of building created new kinds of cities. Instead of streets and sidewalks to get around, we were forced to move endlessly in three dimensions to get from building to building, from one part of the city to the next. For most of us it was not a better way of living, just different, uncomfortably different. The city slowly became what they called a network, something like a complicated organization chart, but built in real space. That's not entirely accurate, because organization charts usually have the most important places clearly marked, if you know what I mean, and less important spaces related to them, so there some kind of order to guide you where to go, or where it was important to be, or at least how to find your way home—“I live three blocks north of the Capitol Building,” or, “You can find me at the end of Harmony Street,' and things like that. The new city had no order like a grid, and it didn't help that buildings would change faster than in the old days. There are parts of the city built in the old ways, from materials, not energy, so we have been able to go there over the years and see them slowly decay in the rain, wind, sunlight. We see the old signposts, with street and building names, and they only remind us of a world we lost, or was lost for us by the great minds who engineered the new ways of building. They say that it was necessary to save the planet from us, and to save us from what pop-poets are calling the planet's revenge. The great minds tell us that it's all for the better. Living in a network rather than a neighborhood will make us, eventually, smarter, more independent, more secure. They say that the old ways made us dependent on the people in charge of everything—the politicians and CEOs, the bosses. Now we've been liberated, and we're free to shape our own worlds, not just as fantasy but as reality. The only problem is that you're not really free if you have to do something. And the other problem is that we all have to live together, and because there are no supervisors to tell us how to bring our fantasies together and shape a reality, we have to spend most of our time and energy talking—excuse me, networking—with others about how to do it. Yes, I know about the newest Negotiation machines and how to use them, and about the Architects, who translate their results into the actual spaces of the city, my house for example. But it's very difficult and demanding. They say the next generation will find it all very easy and natural, but there are times when I wish I could say “I live at the end of Harmony Street,” or had a boss at my work to tell me what to do. But the streets and the bosses, which were somehow linked, are long gone.”


[![](media/4simlandscapes-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/4simlandscapes-1-2.jpg)


**LW**


#buildings #new_cities #story-telling
 ## Comments 
1. zo
7.21.11 / 3pm


Mr Woods


I have always seen a network in your work. I have always admired since I ever saw your work for the first time, an incredible sense of your truth. You speak so clearly. The color composition is just as good as everything you have to offer in a network. I wish I could say I could keep up with you. Hopefully someday I will. But you are creating something for me. It's always good to see your mind at work..





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.22.11 / 12am
	
	
	zo: Your remarks are encouraging and deeply appreciated.
3. youssefnabil
4.13.12 / 6pm


I live in cairo and you should pay us a visit there, the notion of vehicle only street does no exist we live in a very hybrid network people cars vendors buildings and objects which always provide more safety more interactive experience but of course has its own downside which is the flow of traffic is always bad and uncontrollable but there should be some kind of theory that regulates that chaos and use for new kind of overlapping hybrid networking .





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.13.12 / 8pm
	
	
	youssefnabil: I very much like your term “hybrid network.” It really does evoke a more subtle complexity than theory has dealt with so far. Thank you.
