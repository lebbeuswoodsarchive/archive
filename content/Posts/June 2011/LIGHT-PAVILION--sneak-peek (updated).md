
---
title: LIGHT PAVILION  sneak peek (updated)
date: 2011-06-03 00:00:00 
tags: 
    - architecture_exhibition
    - Christoph_a__Kumpusch
    - Light_Pavilion
---

# LIGHT PAVILION: sneak peek (updated)


## *UPDATE*….Photos of installation at the end of this post.


## 


[![](media/wall-a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/wall-a.jpg)


## .


## **MAK CENTER PRESENTS the**


## 


# **[“LIGHT PAVILION”](https://lebbeuswoods.wordpress.com/2011/02/15/a-space-of-light-2/) by**



**LEBBEUS WOODS and CHRISTOPH a. KUMPUSCH**



**Construction Drawings & In-Progress Photographs**


**at the Mackey Garage Top**


**June 17 – August 6, 2011**


(West Hollywood) The MAK Center for Art & Architecture presents Lebbeus Woods's and Christoph a. Kumpusch's most recent project, the *[Light Pavilion](https://lebbeuswoods.wordpress.com/2011/02/15/a-space-of-light-2/)*, designed for Steven Holl's building currently under construction in Chengdu, China. This singular opportunity to experience the work of the internationally-respected experimentalist is offered at the Garage Top at the Mackey Apartments. The *Light Pavilion* will be on view Fridays and Saturdays from June 17 – August 6, 2011.  A free public reception will be held on Thursday, June 16 from 7 – 9 p.m.


Created in collaboration with Christoph a. Kumpusch, the *Light Pavilion* will be Woods's first built piece of architecture. Inserted mid-level in the Steven Holl building, the Pavilion will function as a beacon of light and provide focus for the community. Intended as an entirely experimental space, the structure employs a dynamic geometry that contrasts with the more regular rectilinear lines surrounding it. The angular columns supporting stairs and viewing platforms set the spaces in motion, encouraging visitors to explore. These columns articulate the interior spaces and are illuminated from within, glowing with subtly changing color. Mirrored surfaces enclose the Pavilion, visually expanding the spaces infinitely.


Known for “paper architecture” projects that have challenged the architecture profession, Lebbeus Woods trained at University of Illinois and Purdue University and worked in the offices of Eero Saarinen before devoting himself exclusively to theory and experimental projects in 1976. Among Woods's best known work are provocative proposals for San Francisco, Sarajevo and Havana, each created when those cities were experiencing crises. In 1988, Woods co-founded the non-profit Research Institute for Experimental Architecture. He currently teaches architecture at The Cooper Union in New York City and the European Graduate School in Saas-Fee, Switzerland.


Christoph a. Kumpusch is a New York-based architect and a PhD candidate in architecture at the University of Applied Arts in Vienna. He is a Leonardo da Vinci Fellow, co-author of *System Wien*, editor of *IDEA(u)topsy* and consultant to the Architectural Forum and Association for Urban Development and Research in Shanghai and Guangzhou. He was a MAK Center Architect in Residence in 2003, and currently teaches architecture at Columbia University Graduate School of Architecture and Pratt Institute.


At the Garage Top, the *Light Pavilion* will be represented by construction drawings, in-process photographs, and a model. On view from June 17 – August 6, visiting hours will be Fridays and Saturdays from 11 a.m. to 6 p.m. and by appointment. The Mackey Apartments are located in the Mid-City district, at 1137 South Cochran Avenue, Los Angeles 90019. Admission is free.


**The MAK Center for Art & Architecture at the Schindler House** is located at 835 N. Kings Road in West Hollywood. Public hours are Wednesday through Sunday, 11 a.m. to 6 p.m. Regular admission is $7/$17 with the guidebook, *Schindler By MAK*; students and seniors, $6/$16 with book; free for Friends of the MAK Center and on Fridays, 4 to 6 p.m. Parking is available at the public structure at the northeast corner of Kings Road and Santa Monica Boulevard. For further information, the public may contact [www.MAKcenter.org](http://www.MAKcenter.org/) or call (323) 651-1510.


.


[![](media/mak-la-poster-blog.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/mak-la-poster-blog.jpg)


*(above) Poster design by Won. S. Choi.*


## Exhibition installation photos:


(below) Installing one of several photo-murals:


[![](media/photo-1bw.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/photo-1bw.jpg)


[![](media/mak-la-install-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/mak-la-install-2.jpg)


(above) Installing the photo-mural on the gallery's sliding glass doors.


[![](media/makla-install-15.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/makla-install-15.jpg)


(above) Table with construction documents and model of the Light Pavilion.


[![](media/makla-install-20.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/makla-install-20.jpg)


(above) At the end of the evening, the sliding glass doors are closed, except for the last. There are always a few diehards remaining….


[![](media/makla-211.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/makla-211.jpg)


(above) The diehards open the sliding glass doors. Ah, the exotic LA evening…. Special thanks to Kimberli Meyer for her generous hospitality, to Steven Holl, for his commission of us to design the Light Pavilion within his Raffles City project, and to CapitaLand, his client, for their courageous support of experimental architecture.


**LW and CaK**


#architecture_exhibition #Christoph_a__Kumpusch #Light_Pavilion
 ## Comments 
1. Eric Chin
6.9.11 / 3am


Will Mr Woods and Mr Kumpusch be gracing us with their presences?
3. Mark Morris
6.17.11 / 5pm


I want to congratulate Lebbeus and Christoph on this remarkable exhibition with the MAK Center in Los Angeles. Apart from the built work, which deserves its own critical space, the idea for the show – its venue, scale of representation and timing – is a remarkable thing in and of itself. Exhibitions are their own design projects, particularly their posters. I think the one for the Light Pavilion is exceptional in this regard. Kimberli Meyer should also be applauded for seeing the potential here. Great bold Work!
5. [“LIGHT PAVILION” @ MAK center, L.A. poster design 116 «](http://jiboong.wordpress.com/2011/06/27/%e2%80%9clight-pavilion%e2%80%9d-mak-center-l-a-poster-design-116/)
6.27.11 / 8pm


[…] <https://lebbeuswoods.wordpress.com/2011/06/03/light-pavilion-exhibition/> This entry was written by jiboong, posted on 2011/06/27 at 16:23, filed under graphic design, wosoch. Bookmark the permalink. Follow any comments here with the RSS feed for this post. Post a comment or leave a trackback: Trackback URL. « É Preciso Perdoar – Versions by João Gilberto / Ryuichi Sakamoto & Cesaria Evora & Caetano Veloso & Jaques Morelenbaum 115 LikeBe the first to like this post. […]
