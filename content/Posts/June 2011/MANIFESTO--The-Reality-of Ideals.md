
---
title: MANIFESTO  The Reality of Ideals
date: 2011-06-06 00:00:00 
tags: 
    - architectural_education
    - manifestos
    - The_Cooper_Union
---

# MANIFESTO: The Reality of Ideals


*The following has emerged from the innovative First Year [program](https://lebbeuswoods.wordpress.com/2011/01/26/four-ideal-houses-first-year-studio-2/) and [work](https://lebbeuswoods.wordpress.com/2011/05/17/four-ideal-houses/) made this past semester. It has been posted on the wall of The Cooper Union End of Year Show and is being printed as a pamphlet, with photographs of the students' projects, which will be distributed to all faculty, students and administrators in the School of Architecture:*


.


## 


## **The Reality of Ideals**


## **A Manifesto of Architecture and Education**


## 


## **First Year Architectonics Studio**


## The Irwin S. Chanin School of Architecture


## of The Cooper Union for the Advancement of Science and Art


## 


## Professors Woods, Titus, Wegman, and Miron


## 


## **Spring, 2011**


**1.**


**This work is a Manifesto of Architecture and Education**. It is dedicated to the present and future faculty of the Irwin S. Chanin School of Architecture of The Cooper Union for the Advancement of Science and Art, and to the students of the school, from the present and into the future. In the venerable tradition of Manifestos it is a critique of the present situation in the school; a remedy through a new statement of principles; and a lucid example of them. Its undisguised purpose is to redirect the school for the better: encouraging the best instincts of its faculty, the idealism of its students, and in this way contributing affirmative new energy to the field of architecture in its present fragile state.


Professors Woods, Titus, Wegman and Miron


**2.**


**FOUR IDEAL HOUSES**


**Project**


This semester we will focus on the design of **four Houses**, which we term ‘ideal' because each occupies a different elemental volume—*cube, cylinder, cone,* or *pyramid*—and each embodies a program of habitation based on a different natural element—*earth, air, fire,* or *water.* Furthermore, the **inhabitants** of each House are assumed to be ‘ideal,' in the sense that they embody, for our purposes, only certain universal human characteristics, such as physical size, capabilities of movement, perception of their environment, and interaction with it. The **site** of each of the four Houses will also be ideal, meaning sloped or flat, horizontal or vertical, and will disregard any inherently idiosyncratic features. In the design of each House, equal emphasis will be placed on the **interior and exterior** of its volume. In taking this approach, we realize that these ideal types exist only as ideas, yet find these ideas useful in the laboratory of the design studio as a means of **understanding the fundamental elements of architecture.**


 **3.**


 **Background**


There is considerable historical precedent for our project. We find ideal architecture—of exactly the sort we are engaging—in examples from Vitruvius, through Alberti and Da Vinci, Ledoux, Semper, Taut and Le Corbusier, Archigram, up to the present in ideal projects by Holl and Abraham. These and other architects have found it important to define their personal principles of design, as well as to set a standard against which to measure their more reality-based work. Ideal architecture has been essential to defining building typologies, which serve the purpose of bringing a level of order to the diversity of urban landscapes. Each student is encouraged to look up and into historical examples, in order to better understand the broader context of our work.


**4.**


**Method**


We will arrive at the designs of the Four Ideal Houses by a series of steps or **stages**, working both individually and in four teams, one for each House. As the design of each House progresses, it will evolve from the ideal forms of its beginnings to the particular forms of its development and conclusion. If we assume, for example, that the House of earth has the form of a cube, we can expect that its ‘earth-like' material stability will resist any changes made to the volume; yet, human inhabitation requires changes, for example in the need for openings for going in and out of the cube, and letting in light and air. Consequently, these openings will be determined by the rules inherent in material stability, say, the regularity of its cubic geometry. This transformation will, in itself, be considered a higher level of the ideal, in that it embodies a fundamental aspect—a continual evolution in time— of both the human and natural worlds.


**5.**


 **Goals**


**Collaboration and teamwork**


Each of us will approach this project with our own aspirations, our own ideals of architecture. It is crucial that, even when we work in a tightly knit team, we keep our own personal ideals and goals in mind. Teamwork is at its best when individuals who are clear about what they want to achieve collaborate. The key to their successful cooperation is for them to emphasize what they have in common, not their differences [of course, if they have no important ideas in common, they should not be on the same team]. In that way, collaboration is never a compromise of what each believes, but rather a reinforcement of the most important aspects of it by the similar ideas of other team members.


When working on our projects, we should keep in mind that the making of architecture is always a team effort. At the same time, we should recall that successful teamwork bringing together the work of strong individuals, requires **leadership**. It is never a committee effort, with decisions made by voting. Instead, at each stage of the design work and, later, the work of building, a leader must guide the collaborative effort; it may well be that each stage a team's work could have a different leader. This leader usually emerges quite naturally, as he or she is the one who has the best idea about how to accomplish a particular stage of the work. **Achieving successful design collaborations is one of our goals this semester.**


**6.**


**Human scale**


We will emphasize in our work this semester the attainment of human scale for our projects. ‘Human scale' is a term used freely by architects, but often with very little understanding of what it means or how it can be achieved in their designs. All too often, they indicate human scale in their drawings by drawing human figures in or next to their buildings, or, in models, little figures and model cars. These are actually poor techniques because they do not attain human scale in the architecture, but merely indicate it graphically. Human scale in even uninhabited architecture is attained in two basic ways:


1) **the presence of tectonic elements required by human use**—stairs, windows, doors, and other elements that facilitate human use of spaces. Their size in proportion to a building's overall form, and their relationship to each other do not merely indicate the relative size of people, but are necessary for people to inhabit the building and are therefore integral with it. Most of these elements are not arbitrarily sized, but confirm to the dimensional limits of the human body and its capacities.


2) **the presence of tectonic elements used to construct a building**—its walls, ceilings, floors, and other elements defining and articulating spaces. Buildings are not made of a single, solid material, but are constructed of many parts and pieces put together by human beings, and the pieces are sized accordingly. Whether they are assembled directly by hand—bricks or wood panels—or by the hand operation of construction equipment—steel beams, pre-cast concrete slabs—when these parts are visible in the completed building, they immediately establish the relative size of a person (the builder) and, thus, human scale. [Note that even the most seemingly monolithic of materials—reinforced concrete—is poured in parts and thus bears the marks of form panels and construction joints.] **Achieving human scale in our projects is one of our main goals this semester.**


**7.**


**Ideal Houses**


The conception and design of ideal houses realizes the highest hopes of their designers, giving form and structure to their aspirations for themselves, architecture and through its place in the broader scheme of things, the many people engaged by it. The truth is that ideal architecture in the sense that we speak of it here CAN be constructed in the real world and with real materials—indeed, it MUST be constructed. The final drawings and models of the four Houses will—if made with intelligence, passion, and courage—**achieve the reality of ideals. This is our most important goal.**


**8.**


*Through precise questions (and affirmative constraints) students are able to engage language and tectonics, re-framing the possibility of habitation through construction and scale, while being/working together, as an energy, with an ethics of work that engages drawing, dialogue, thinking and building; translating ideas into tectonic plays of dynamic potentialities and new directions, concepts, positions, situations, feelings and risks, engaging raw materialities with full scales; tapping into histories, myths, precedents, unknowns and differences, through details and assemblages (even erasures and subtractions), while transforming concepts and materials into new forms and structures. This as an expression that, poetically, Beings can create as a group, in a school, as a political act to begin or continue resisting marginalization, exclusions, totalities, forgetting. To become….become open to new thoughts, ways of being-together-apart as affirmative action that can influence others ethically and creatively.*


*Professor Aida Miron*


*.*


*In every act of great architecture the whole is greater than the sum of its parts. The great task of putting things together is also the challenge of putting different minds together—it calls for an understanding of architecture as a creative assemblage and demands an education for collaboration, clarity and construction. The work in first year Architectonics studio is an assembly and collision of materials, personalities, moods and minds. It is a place where the fragility and risk involved in the making of architecture should be celebrated and encouraged and where a clear structure should be provided for the assembly to happen. The Four Ideal Houses are also a search for an ideal studio—where a wild mix of poetics and politics takes place and where ideas create personal commitment and a passionate public debate.*


*Professor Uri Wegman*


*.*


*It can be said that architecture seeks to structure the thoughts, needs and desires of a given society. As four educators, we seek to provide an exact and precise set of limits that will allow our students to explore their own thoughts, needs and desires. This process is seen as a point of departure, a beginning, an exploration of principles, which will be carried into the future, by both the students and faculty alike. The Four Ideal Houses come on the heels of two previous years of rigorous research and making by two other groups of first year students within The Cooper Union. The Ideal Houses are a cut in time, revealing the  moment of a growing transformation within our community.*


*Professor Anthony Titus*


.


*Each in their own way, my colleagues have here addressed the issues of society and community that our field of knowledge and practice must begin to face directly. The era of the solitary genius must come to an end. The era of the collaborative genius must begin. Our school has always stood for and nurtured not only creativity, but also authenticity, the quality of originality emerging from the inner struggle of individuals to give form to their most inspired thoughts. That should never change, and must be defended against all pressures to make the curriculum conform to standardized teaching and to homogenize the thinking of students and faculty to the competitiveness of corporate practice. It is now time to turn the inner struggles for the soul of architecture to the realization of common purposes. It is worth repeating: **The era of the solitary genius must come to an end. The era of the collaborative genius must begin.***


*Professor Lebbeus Woods*


.


#architectural_education #manifestos #The_Cooper_Union
 ## Comments 
1. [lebbeuswoods](http://www.lebbeuswoods.net)
6.8.11 / 11pm


Must say I'm surprised that no one has taken issue with any of the points of this manifesto.





	1. Pedro Esteban
	6.8.11 / 11pm
	
	
	are you shure of that?  
	
	Inspiring!
	
	
	
	
	
		1. Pedro Esteban
		8.13.11 / 3pm
		
		
		Are you sure of that?
	3. Peter Kneiber
	6.13.11 / 11am
	
	
	Lebbeus , why do you think that happened?
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		6.13.11 / 1pm
		
		
		Peter Knelber: What is the “that” you refer to?
	5. Peter Kneiber
	6.13.11 / 3pm
	
	
	I was asking why do u think no one had taken issue with the points of manifesto ? … or can u make it little bit clearer : taken issue , because i think this might be as much interesting as the manifesto itself.
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		6.14.11 / 12pm
		
		
		Peter: I hope not. Still, I could make some guesses:  
		
		1. No one is reading the Manifesto.  
		
		2. No one believes in manifestos enough to take them seriously, hence comment.  
		
		3. There is nothing controversial in this Manifesto—it's a yawn.  
		
		4. Readers don't care much about architectural education—there's nothing important at stake.  
		
		5. The points made in this Manifesto are badly written, unclear, and the like.  
		
		6. The readers of this blog are not as smart and engaged as I think they are.  
		
		7. The readers of this blog are too busy with summer plans to be serious about anything.
3. tom
6.10.11 / 3am


I guess manifestos are currency after being absent from post modernism. Is there a recently published book of architectural manifestos? the statement about the end of solitary genius' seems contradictory. who are the solitary genius' being referred to; Hadid and Gehry? They hardly seem solitary, but hooked into immense cultural machinery. Colaboratives like Bow Wow relatively seem to be working more on the margins, although the BMW Guggenheim contradicts this.  

talking about genius' opens up a can of worms to what end/ Why any sort of?  

Personally I can resd some humor into proclamation of genius. After not hearing the word much and in the current cultural landscape. it has the ring of a super hero name, something from xmen. has the ring of manifold, an engine part, something useful, but also the ring of past or pesto, something entirely superfluous.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.10.11 / 2pm
	
	
	Tom: Manifestos are enjoying something of a revival. There are a couple of books recently published and though I have contributions in them I can't recall their names! Try ‘manifestos' in google or Amazon.  
	
	The advantage of the manifesto form is that it simply states its points, without a lot of tendentious academic argumentation. It is clear and concise, or should be.  
	
	Your point about ‘genius' is of course right on. I do use it was some irony, but also seriously. Even though the term is out of fashion (for good reasons) our society still runs on the idea of the unique, one-of-a-kind creator—the ‘starchitect' and the ‘celebrity' command the most attention. At the Cooper Union, where I have taught for more than twenty years, the students have been encouraged to produce unique, entirely solo works—the proto-genius is the model lurking not far beneath the surface of this and most other schools. This manifesto points in a radically different direction—creative collaboration.
5. tom
6.10.11 / 4pm


Thanks. Not that it that important., but I meant that because of the recent resurrection of the word “manifesto”, it has an odd ring it, like a combination of manifold and pesto.
7. [chris teeter](http://www.metamechanics.com)
6.10.11 / 11pm


#4 is the best one I think. Its a fraction of what happens in practice but hints at where you will develop your language and is a very good introduction. I say fraction because form and material are only part of the overall detail design determination with typical jobs and clients – the additional aspects that influence the design are codes, clients desires, economy, logistics, environmental considerations, social politics, time, etc…details are important since space planning is so pre determined these days even clients can do it, so what's left right?


Which brings me to anthony titus paragraph, on point and should be noted regarding limits. Anyone can dream, everyone does it when they sleep. No limits requires little creativity on anyone's part, but establishing limits makes one develop their creativity while discovering ideals that are real. So maybe you can push the pre determined space planning limits… 


Teamwork – always thought this was a funny one when professors talked about it, but it may also be a generation thing. After working for 4 years and returning to grad school with kieran timberlake as profs for studio one semester emphasizing teamwork and research etc…I actually was tired of hearing about teamwork. In practice teamwork tends to be passing the buck. The lone genius is a myth(we all know that), but KT would talk about guys they worked for and other bosses I have worked for as well..about these architects who stomped their feet and yelled at everyone and would say “figure it out!”. These guys don't exist even if geniuses, I have never met any who have a job or clients, because no one thinks architects actually know anything anyway…well no one that knows anything about building.


Architect and genius can only be used in the same sentence when the word architect is used to describe a politcal mastermind or something.


Anyway a teamwork suggestion, architects mainly agree on the same things, it'd be interesting if a later studio at cooper required architects to work with engineers, that requires real teamwork in my opinion. People who have very other limits to design.
9. [Richard Joon Yoo](http://www.h-a-h-a.us)
6.19.11 / 9pm


Solitary Genius vs Collaborative Genius —I do not see either as beginning or ending.  Instead the two are dependent on each other.  Our potential for progress resonates between individual spontaneity and collaborative wisdom.  


This reverberation exists in multiple spheres: politics, society, technology, music, physics, and architecture. In the context of this studio, each collaboration was so potent and immersed in a rich and fertile environment, that the work was capable of reaching “the highest hopes of their designers… the reality of their ideals” [Lebbeus], indeed with a “wild mix of poetics and politics” that appeals greatly to my sensibility.  Without the walls of the studio, the “celebration and encourage[ment]” of architecture's “fragility and risk” [Uri] almost always utilizes the mechanisms of capitalism, which complicates the distinction between idealistic collaboration (as exemplified in this fantastic studio) and the profit-driven corporation (which Lebbeus so rightly holds at arms length).  The interplay between the refinement of ideals and the sustainability of profit exists in shades of grey and is multi-dimensional.  


Regardless of the direction in which architecture's momentum is headed, I consider it our highest obligation to subvert cultural passivity, apathy, boredom, and blindness.  (Perhaps this says more of my own personal distrust of the comfortable then it does the role of architecture!)  This motivating ethic Aida phrases well as “… work that engages drawing, dialogue, thinking and building … tapping into histories, myths, precedents, unknowns, and differences … as a group, in a school, as a political act to begin or continue resisting marginalization, exclusions, totalities, forgetting.”  


Without these traits I fear an architecture that remains, while seductive, at the level of surface and style.  This studio has aimed squarely for durability in content and form, in such a way that is not naïve, nor slavish to trends.  The boldness and sophistication of architecture at its best arises, as I see it, from speaking to and challenging social, political, and aesthetic assumptions, as well as taking opportunities to mesh the pragmatic with the poetic and whimsical.  In our current fractured field, this studio is a brave and bold expression of humanistic architectural principles.
11. joão antunes
6.26.11 / 10am


first time here.  

and I will be back. cause I know your work – both theoric and practice.  

and I will be back… cause you said – “readers of this blog are not as smart and engaged as you think they are”  

now that is not true at all… I am here.


and I don´t like violet…
13. joão antunes
6.26.11 / 10am


and of course I haven´t read the Manifesto… yet.
15. Pedro Esteban
9.30.11 / 3am


I am not interested in the death of that lone genius/no irony/  

The genius have a quite crucial and hard work: the genius guides…


 In what I am interested is in collaboration, do not in who make collaboration…  

This is an interesting approach but are many factors who can be working against that collaboration.
17. [JOHN SZOT: Interview with LW, ‘the ineffable' « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/02/26/john-szot-interview-with-lw-the-ineffable/)
2.26.12 / 4pm


[…] Of course, they don't do this in isolation, but working with others. That's what I meant in a recent manifesto of education when I declared that “the era of the collaborative genius must begin.” The architect must be […]
