
---
title: AI WEIWEI  What is to be done?
date: 2011-06-07 00:00:00 
tags: 
    - 'disappeared'
    - Ai_Weiwei
    - Charlie_Finch
---

# AI WEIWEI: What is to be done?


## **by Charlie Finch**


That the Chinese government is holding artist [Ai Weiwei](https://lebbeuswoods.wordpress.com/2011/04/03/7669/) for tax fraud, related to his company Fake Design, is especially ominous for a number of reasons.


First is that until recently tax fraud was a significant trigger of the death penalty in China. According to Xinhua, the Chinese news agency, Ai is accused of not only “massive tax evasions,” but also “destruction of documents,” code for a charge of systematic fraud that could yield the ultimate penalty for Ai and his associates. The complexity of the Chinese tax system, which includes a Value Added Tax and a wide range of Stamp Taxes, makes proof of fraud, if only for inadvertent negligence, especially easy.


Secondly, charging Ai is an easy way to divide and conquer his associates into testifying against him or engage in special pleading on their own, or his, behalf. Already, Weiwei's sister Gao Ge has publicly stated that because Ai was not an officer of Fake Design that he has no accountability for the tax charges, however bogus. This is a dangerous gambit, as the Chinese authorities have already “disappeared” four of Weiwei's associates, including journalist Wen Tao, whose family is arguing that he had no formal connection to Fake Design and thus should not be charged.


Thirdly, tax charges are an easy way for the ChiComs to scapegoat Ai and his associates to the Chinese people. Already, the Chinese Foreign Ministry has issued a statement that “the Chinese people do not like Ai Weiwei,” an oddly personal yet deadly expression of the prized conformity with which the Chinese government unjustly characterizes its own people.


Finally tax fraud offers the Chinese government an easy way to denounce those abroad calling for Weiwei's release, as it already has vociferously, for “meddling in internal affairs,” and to put companies doing business with China on notice that they too could be targeted on tax charges for speaking out on human rights in China. There is grumbling about Weiwei among the art elites, exemplified by curator Francesco Bonami's statement last December in Italian *Il Reformista* (before the current charges) the Chinese government had “finally” taken action against Ai.


This exasperation with Weiwei (called by a friend of mine “Oy Vey Vey”) is far more prevalent among the art elites, whose business is inherently antidemocratic, than one might first suspect. Therefore, it is incumbent that a leader of art commerce should speak out and take concrete actions, tailored to the trumped up tax accusations against Ai. I respectfully call on Pace Gallery chairman Arne Glimcher, a man quite familiar, through his family history, of the lethal actions promulgated by fascist governments, whose art operations have been at the vanguard of the Chinese contemporary art scene, to direct Pace not to pay any taxes to the Chinese government until Ai Weiwei and his associates are freed and the charges against them permanently dropped.


**CHARLIE FINCH** is a writer on the contemporary art world and co-author of *Most Art Sucks: Five Years of Coagula* (Smart Art Press). This article recently appeared on the artnet.com website. Thanks to Gerri Davis for finding it.


#'disappeared' #Ai_Weiwei #Charlie_Finch
 ## Comments 
1. [THIS CANNOT PASS (updated June 7, 2011) « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/03/7669/)
6.7.11 / 9pm


[…] JUNE 7: The art writer Charlie Finch has written a short but insightful article ob Ai's situation, which gives up information as well as proposing a plan of action: https://lebbeuswoods.wordpress.com/2011/06/07/ai-weiwei-what-is-to-be-done/ […]
3. [J.p. Sara](http://www.facebook.com/profile.php?id=605615109)
6.11.11 / 6am


Hello Charlie, Professor Woods,


 What is to be done? Well, what is this situation a situation of? Weiwei is an artist who has grown beyond the limits of his corporeality, whose various works engage simultaneously with consciousness and the unconscious, and who is able to create, and locate himself at, syntheses within the unconscious. This can only be devastating to a political apparatus that requires its populace to repress the unconscious as much as possible or, ideally, not have one at all. Weiwei's reputation is not at stake. It's busy at war. And it's winning.


Any government is only a structure, possessing necessarily the same basic logical foundations as any mathematical or architectural structure, and is also subject to high levels of scrutiny. China's government, as you know, does not hold up very well to outside scrutiny. And the ethological nature of architecture overly prepares it to fearlessly confront the irrationality of something like a governmental structure by conceptualizing, and engineering, objects of dissent, objects which necessarily originate in the unconscious. These objects, I would argue, are examples of architecture at its purest. They do not replace or destroy any existing foundations but rather make known the reality of an excess, or surplus burden, a burden whose weight can threaten existing foundations by perverting their forms and demanding extreme performances on the part of engineering, either physical or social. Without a monopoly, few governments can withstand the burden of these excesses and often require apparatuses for adaptation and change in order to survive. This is Ai Weiwei territory.


The mere existence of Weiwei threatens to take over responsibility of these surpluses from their existing (in this case governmental) foundations, and render an awareness (in this case among the Chinese populace) of their obsolescence and redundancy. Now this is precisely what the Chinese government will not have and this is why Ai Weiwei is in jail if not for tax evasion. The Chinese government does not care about how other countries and cultures distribute the surpluses they generate nor whether or not they think Weiwei is a stable support of them. It cares only about how the excesses that it actively generates, and uses to police itself within its one-party walls, are supported and managed. Weiwei is not appreciated for making known the excesses within Chinese culture nor is he invited to compete to support them. Nonetheless, if Weiwei weren't winning at this competition he wouldn't be in jail.


How do we save Weiwei's life? Persuading others to avoid their tax responsibilities could only validate a system of punishment for this form of civil disobedience. Weiwei, then, must appear insignificant in order to secure his safety. Rendering futile the Chinese government's efforts to incarcerate the affects Weiwei has over the unconscious is a matter of improving upon this particular aspect of his work, a rapid propagation and dissemination, that will overwhelm China and the world with the reality of imminent power shifts that are destined to occur. Destined because this is the history the unconscious demands to look back upon.
5. aes
6.22.11 / 3pm


released. note the terms: ‘good attitude', ‘confession', poor health.  

<http://content.usatoday.com/communities/ondeadline/post/2011/06/china-releases-outspoken-artist-ai-weiwei-on-bail-/1>
