
---
title: STEVEN HOLL’S HORIZONTAL SKYSCRAPER
date: 2011-06-28 00:00:00 
tags: 
    - architecture
    - China
    - mixed-use_development
    - Steven_Holl
---

# STEVEN HOLL'S HORIZONTAL SKYSCRAPER


[![](media/sh-shen-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/sh-shen-3.jpg)


*(above) A part of the Horizontal Skyscraper project by Steven Holl Architects, when it was under construction.*


Regular readers of this blog know from past posts that Steven Holl and I have been close friends for many years, and also that he commissioned me to design The Light Pavilion in his mixed-use project, Raffles City, in Chengdu, China. I mention this in the spirit of “full disclosure” concerning this post. Still, I do not believe these factors have clouded my critical judgment about the Vanke Center project in Shenzhen, China, though you have to decide this for yourselves. After all, I have several prominent architects with whom I've been close for many years and in past posts have not hesitated to express my critical judgments in quite different ways than those written below.


LW


[![](media/vanke-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/vanke-1.jpg)


*[![](media/vanke-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/vanke-2.jpg)*


*(above) The Horizontal Skyscraper in recent photographs, when construction was completed.*


The following commentary is an expanded version of one that appeared in [a previous post](https://lebbeuswoods.wordpress.com/2009/07/10/steven-holl-in-shenzhen/). It attempts to give a summary historical background for the Horizontal Skyscraper project:


As he did with his Linked Hybrid Towers in Beijing, Steven Holl—in his Shenzhen Horizontal Skyscraper—shows that he is the master of the large-scale, multi-use building. These projects are new building types, as their metaphoric titles proclaim, in sharp and challenging contrast to the usual developer typologies. In Shenzhen, the large buildings hover above what is to be a large, open garden, freeing—to a remarkable degree—the ground-plane for public use. This concern for making open space for people to enjoy is a primary goal of Holl's urban designs, and sets them apart. The architectural forms are straightforward, even modest by today's flashy standards, but their presence on the landscape is extraordinary. In Shenzhen, it took some special engineering to make the long buildings span between a small number of vertical cores, creating a new type of public space, but Holl's team achieved this, and within the developer's budget. Now completed, this project will hopefully become a model for others, not to be literally copied, but one that shows the decisive effect imagination and innovation can have. In a world where the number of large-scale commercial projects is certain to increase, and steadily, it offers inspiration, and hope that architecture can make a difference after all.


[![](media/photomontage_of_the_wolkenbugel_by_el_lissitzky_1925.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/photomontage_of_the_wolkenbugel_by_el_lissitzky_1925.jpg)


*(above) El Lissitzky's “Horizontal Skyscrapers,” c.1925.*


The architect Henri Ciriani once remarked that Modern architecture allowed people, for the first time in history, to not only walk around, on, and through buildings, but under them. Lighter building materials, stronger structural systems, but most all a desire to be free of gravity, or rather to overcome it, made the levitation of large building masses possible and inevitable. The advent of airplanes—heavier-than-air inhabitations—was a great stimulus to human imagination and not only showed that buildings could fly—in a particular sense—but also created a new concept of space, one fully three dimensional, in which the ideas of over, on, and under gained a new significance. It was the hubris of Modernism to believe that ‘man' had finally ‘conquered nature.' Overcoming gravity was the final, decisive battle to be won. El Lissitzky's “horizontal skyscrapers” led the way in architecture. Le Coubusier's buildings lifted high on columns freed the ground for human use, and also liberated it from the domination of gravity. Over the decades since these revolutionary works, many buildings have been designed and built to be inhabited under raised building masses, but it was not until the design and construction of Holl's Shenzhen project that the concept has been realized with the boldness and idealism envisioned by early Modernists.


*Steven Holl's Gymnasium Bridge across the Harlem River, New York, c.1979:*


[![](media/tumblr_l3v4vnsnqw1qcp8t5o1_1280.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/tumblr_l3v4vnsnqw1qcp8t5o1_1280.jpg)


Steven Holl's American Library expansion, Berlin, 1984:


[![](media/sha-berlin.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/sha-berlin.jpg)


*Steven Holl's Spatial Retaining Bars, Phoenix, 1994:*


[![](media/holledge1c.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/holledge1c.jpg)


[![](media/holledge2b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/holledge2b.jpg)


The Horizontal Skyscraper project in Shenzhen does not emerge from Holl's knowledge of history, or a desire “to continue the Modernist project,” as some architects have proclaimed for their work, but from his own deep interest in phenomenology and the nature of human experience. His published books are filled with references to and speculations on light, density, fluidity, gravity and many other phenomena, and their impact on human perception, cognition and, ultimately, the design of architecture as a primary source of human experience. His thoughts and conclusions have taken their most potent form in his projects. The earliest precursor to Shenzhen is the Gymnasium Bridge in New York, designed in the late 70s, a building bridge-like only in its spanning of the Harlem River, significant not as a means of transit but rather for its cultural connection of the South Bronx and Manhattan through the activities and events it houses. Ten years later, Holl designed a competition-winning addition to the American Library in Berlin with a portion of the building raised high on piloti and connected to other portions by a translucent, inhabited bridge. Most important, though, was his design for the Retaining Bar buildings in his Phoenix “Edge of the City” project, in which slender vertical housing blocks turn sharply to the horizontal, creating in effect a porous urban wall, within which public space is more ‘under' than ‘on.' These explorations reach their culmination in the Shenzhen Horizontal Skyscraper. With its realization, the hopeful prospect of new kinds of urban space and experience has been fully opened.


LW


Nicolai Ouroussoff has written a review of the Horizontal Skyscraper project in the New York Times. It addresses its urban and cultural, as well as its architectural aspect and is, in my opion, well worth reading:


<http://www.nytimes.com/2011/06/28/arts/design/steven-holls-design-for-the-vanke-center-in-china-review.html?ref=arts>


#architecture #China #mixed-use_development #Steven_Holl
 ## Comments 
1. [Gio](http://gravatar.com/gb427)
6.30.11 / 5pm


Mr. Woods…..I am glad you carved sometime to post again…Thanks


What I admire about Mr. Hall's work is the precision of his architectural studies about light, materials, color, form, concept, structure and the translation of these studies into an expression of architectural space. Sure, in this current state of bio-morphic structures and fancy curves, a project like the Vanke center in China, puts us back into considering what is really important about the creation of architectural space and possibly the re-examination of why in our conscious mind the modernist movement seems to fit so right for the expression of space.


I think re-examining the modernist movement is not necessarily copying their forms but to analyze the phenomena and story that makes the sensation of space…


Gio
3. [Lebbeus Woods sobre Steven Holl (El futuro es hoy 04) « Skfandra](http://skfandra.wordpress.com/2011/07/10/lebbeus-woods-sobre-steven-holl-el-futuro-es-hoy-04/)
7.10.11 / 9am


[…] Articulo original en inglés, en el Blog de Lebbeus Woods […]
5. [Steven Holl's Horizontal Skyscraper](http://rchitectur.com/archives/507)
7.17.11 / 9pm


[…] Woods' most recent blog post got me […]
7. [Fellows in the News: Arad, Chin, Dattner, Dixon, Fowle, Greenberg, Holl, Jahn, Orff, & Venturi « Institute for Urban Design](http://www.ifud.org/institute-news/fellows-in-the-news-arad-chin-dattner-dixon-fowle-greenberg-holl-jahn-orff-venturi/)
7.20.11 / 2pm


[…] recommendations for the future of a busy stretch of Toronto's Yonge Street; Lebbeus Woods wrote an enthusiastic piece on Patron Steven Holl's Vanke Center in Shenzhen; Chicago […]
9. Luke
8.22.11 / 9am


These horizontal sky scrapers are fascinating to me. I was just wondering how building such as these respond to seismic events (the designs seem like they would behave poorly). It seems that modern highrises built on pilotis/ soft floors (inspired by corbu's unite'd habitation) have a bad reputation for failing during e.q. If I were to design something of this nature, my structures professor would critique the shit out of me.
11. [EXUBERANT SPACE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/09/13/exuberant-space/)
9.13.11 / 11pm


[…] that many have pronounced dead. Looking at some of Steven Holl's later projects, such as the Horizontal Skyscraper, and the Edge of the City project for Phoenix—as well as Dujardin's designs—it is […]
13. [Steven Holl , El Lissitzky and the HORIZONTAL SKYSCRAPERS « Someone Has Built It Before](http://archidialog.com/2012/01/17/steven-holl-el-lissitzky-and-the-horizontal-skyscrapers/)
1.17.12 / 11am


[…] I recommend everyone who is interested in the work of Holl, to read Lebbeus Woods post dealing with Steven Holl's Horizontal […]
