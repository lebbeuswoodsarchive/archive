
---
title: ANTI—Journey to Architecture  PRELUDE
date: 2011-06-29 00:00:00 
tags: 
    - architecture
    - Lebbeus_Woods
    - Raimund_Abraham
    - road_trip
---

# ANTI—Journey to Architecture: PRELUDE


[![](media/front-cover.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/front-cover.jpg)


Raimund Abraham and I were working on a book together when he was killed in an auto accident over a year ago. Since then I've collected the incomplete parts of the book and transcribed the discussions we had during our seven-day adventure to France in 2007. It is extremely difficult to finish the book without Raimund. We had a general plan, but very little of the writing he intended to do. Partly for this reason, and partly because of the politics and economics of the publishing world, I have yet to find a publisher who will commit to making the book. In my work I've had the complete cooperation of Una Abraham, Raimund's daughter who now controls his estate. I am grateful to her for that. She knows how important the completion of the book was to him.


I have decided to publish as much of the completed book as I have on this blog, in regular installments over the coming summer. My main reason is to fulfill my commtment to Raimund, myself, and the ideas he and I have devoted ourselves to, different as they may be from one another and from the current trends of thought in the field of architecture. An equally important reason is to afford you what I hope will be the pleasure of sharing Raimund's and my experiences.


The blog format allows only for relatively short installments, so I ask for you, dear readers, to be patient as the story—which gets more interesting—unfolds.


LW


[![](media/ra-mugshot.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/ra-mugshot.jpg)


**Raimund Abraham INTRODUCTION**


On Saturday, October 20, 2007, we embarked on a journey, an odyssey as it progressed, but not a pilgrimage, as we both proclaimed, to visit the monastery St-Marie de La Tourette, an icon of modern architecture and considered by many the masterpiece of Le Corbusier. Our purpose was to retreat for a week of solitude to discuss architecture in a time of senseless wars, incomprehensible politics and an unrelenting triumph of rapidly changing fashions in architecture—to probe our own positions—and, if successful, to publish our discourse under the title “The La Tourette Talks.” Longing for the unpredictable, for unknown obstacles, we left unconcerned about the outcome, unprepared geographically and historically for the journey. But unlike Ulysses, who successfully out-maneuvered the Sirens, we staggered through the unassuming landscape of France, lamenting the decline of French cuisine, cursing the banality of towns we passed through, and romanticizing the memory of our own past journeys, long gone. Stations at Ronchamp, Ledoux at Arc-et-Senans, and Courbet's house in Omans failed our expectations, while other unplanned deviations delayed our arrival at La Tourette.


When we finally arrived on a rainy day, October 24, in the mid-afternoon, we both felt a rare moment of inspiration in the overwhelming presence of architecture.


Entering the monastery the next day with a key, obtained under unexpectedly delightful circumstances, we were drawn immediately to the abandoned monk-cells, heart and soul of the edifice, final destination of our journey, locus solus, space of familiar tranquility but unexpected spatial freedom despite its severely limited physical dimensions. After few futile attempts to conduct our discourse we both realized the quixotic aspect of our planned endeavor and it crossed our mind that a diner in Brooklyn, in the midst of banal normality, might have been the authentic site to discuss the fate of architecture in our time. After the final station at the church of Firmany-Vert, recently completed, but many years after Le Corbusier's death, and deprived of his hand and spirit, we returned to Zurich and departed for New York on Sunday, October 27.


After one year had passed, the idea of a book about the “anti-journey” emerged: two parallel worlds, parallel memories, text against text and ultimately image against image of our own work, probing conflict against affinity, silently witnessing our continuous passion to imagine a new architecture.


RA


[![](media/lw-mugshot1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/lw-mugshot1.jpg)


**Lebbeus Woods INTRODUCTION**


It was Raimund who suggested when we met for lunch one day that we spend a week in La Tourette, devoting ourselves to a discussion of architecture. The idea of living in tiny monk's cells, deprived of creature comforts, living only for the sort of tough discourse I knew he had in mind, did not have much appeal to me. Yet, here I was, saying yes, without any show of hesitation. When I mentioned it to my wife that night, she said that I must do it, with such enthusiasm that I didn't bother to ask why. Carried along on a momentum that seemed self-generating, plans for the trip began to formulate. Before too long, dates had been set, schedules cleared, tickets bought. Remarkably, Raimund and I never talked about what we were going to discuss all those days in the monastery, trusting, I think, that our mutual interests and disparities would inspire us when the time came. And it worked out this way, more or less.


For my part, the instinct to follow through was driven by several realities, and not least that the field of architecture is today in a sorry state, owing to a lack of fertile ideas about what architecture is and what it ought to be. It has not been so many years ago that fervent debates about the role of architectural history, the proper impact of technology, the nature of innovation, as well as the role of architects in effecting social changes, shook up the comfortable complacency of practitioners. True, these debates didn't yield as much of value in the way of new concepts or buildings such as Modernism had done, but they did hold out the hope for them, the hope that architecture could be a vital force for change, and not simply the confirmation of a status quo in a time of tremendous social change. What is called for is no mere search for novelty, which is always superficial and often a disguise for maintaining a status quo—as much current architecture demonstrates—but instead a questioning, examination, and, if necessary, a reformulation of principles and founding ideas. Only tough discourse can get to these roots of what we know and do, and the sorry lack of it in the field today explains the superficiality of so much new work. I will take this assertion one step further, and say that only in the active and sometimes stressful, difficult conflict of differences is there the energy for creativity of any kind.


Discourse, however, does not necessarily lead to creative work in architecture, or anything else. Its usefulness is in fertilizing the soil of the imaginations of individuals whose passion for architecture is tempered only by a cold knowledge of its limitations. Indeed, it is to the establishment of these limitations that creativity always needs, that the most profound debates and the toughest discourses lead. Conflicts between conceptions of the ideal and the material, the finite and the infinite, the real and the actual, the deliberate and the accidental, the planned and the emergent, are vital to setting the boundaries of the tasks at hand, as well as architecture's limits in accomplishing them. While the ultimate synthesis of a work of architecture can only happen within an individual mind, the discursive ferment that leads up to it can only be accomplished among individuals willing to talk openly with one another.


There was another factor behind making this journey. While neither Raimund nor I imagine ourselves as other than practitioners, the fact is we have both been teachers for much of our lives. I have faith in the free discussion of both concepts and works of architecture. The studio critic is not an academic, speaking primarily to a community of other academics, but a participant in the practice of architecture, which is firmly grounded in the physical construction of space, in whatever forms it might take. As a teacher, I have faith in the crucial role of language in conceiving of architecture, not only for myself but also for the community of those who believe in architecture as a consequential endeavor. The seven days we spent on what we have come to call our anti-journey—for reasons that will become clear—created more new questions than answers, and for that I am grateful.


LW


*to be continued*


**The book ANTI was being developed with the assistance and collaboration of architect DORIT AVIV.**


#architecture #Lebbeus_Woods #Raimund_Abraham #road_trip
 ## Comments 
1. chaos海
7.1.11 / 12pm


Really look forward to your work !


PS, thanks for the efforts about AI WEI WEI
3. [Gio](http://gravatar.com/gb427)
7.6.11 / 1am


This sounds interesting!  

I ll be waiting.
5. ryanadoyle
7.6.11 / 3am


this is my first post but i have been an avid reader for quite some time… thank you for continuing to write this blog as it is one of the few true and honest discussions of architecture available today


i just had to write in that we must have nearly missed each other at la tourette… i was then (fall 2007) in a study-abroad program in london and had decided for my fall break to spend a week in france travelling on my own, to paris, lyon, la tourette, and finally to ronchamp


i have in my records that i was staying there october 22nd and 23rd… i also was graciously allowed individual access to the entire complex by the friars (and even dined with them, whereby i managed to befriend one who took me up to the roof! – otherwise off-limits!)


the whole experience was, though it may sound trite, quite life-changing (especially traveling on one's own)


thank you, and keep at it
7. [Kevin Rhowbotham](http://www.kevin-rhowbotham.co.uk)
7.6.11 / 10pm


Architecture will not be Idolised


Yo! brother;


Architecture will not be idolised!


Architecture will not make your teeth whiter than white.


It will not give you a six-pack, cure alopecia, or make your dick two inches longer.


Architecture will not give you the body of 19 year old, reduce facial wrinkles or remove unsightly body hair.


Architecture will not make you a Hollywood starlet, improve your fornication technique, or make you attractive to your business partner's mistress.


Architecture will not give head.


Architecture will not reduce the size of your butt.


Architecture will not make you rich, wise, reflective, socially magnetic or sober.


Architecture will not free your mind, free your body or cure halitosis.


Architectural will not be idolised!


Architecture will not be brought to you by DuPont, Disney or Metal Box, in four parts without commercial interruption.


Architecture will not show you pictures of politicians shedding crocodile tears over desolation and indifference in the streets of neglected cities.


Architecture will not be photographed holding babies of the poor whilst taking money from speculating landlords, breaking lives for meagre profits.


It will not lead the charge to conquer evil in all its forms.


Architecture will not bring you to the kingdom of heaven or to the seven tiers of hell.


Architecture will not be the next reality tv sensation.


Architecture will not win Big Brother, X factor, I'm a Celebrity, Survivor, Wife Swap, America's Next Top Model, the Apprentice or American Idol.


Architecture will not make you live longer, relieve stress, make you wet, give you a hard on or replace masturbation.


Architectural will not be idolised!


Architecture will not be screened by the international news desk of Al Jazeera, Fox News, ITN or the Today programme.


Architecture will not make you famous, notorious, talented; an artist, a genius or a freak. 


Architecture will not give you sex appeal.


Architecture will not make you five pounds thinner, two inches taller or ten times stronger.


Architecture will not clean your shoes, clean your windows or clear your conscience.


Architecture will not raise your children in a benign society.


Architecture will not make you morally acceptable, corporately commendable or socially desirable.


Architecture will not make you plug in turn on or kop out.


Architecture will not make you gay, heterosexual, metro sexual, bisexual or co-incident with the sexual interests of Katherine the Great.


Architecture will not be iterated, replicated, donated, eviscerated, confiscated, castigated, consecrated, obliterated or denigrated.


Architecture will not overturn the tables of the usurers and moneychangers in the temple of Solomon.


Architecture will not be crucified, plagiarised, pacified or glorified.


Architecture will not give you the head of John the Baptist.


Architecture will not be idolised!


Architecture will be live.


After ‘The Revolution will not be Televised' and in fond memory of, the late great Gill Scott Heron. died May 27th 2011.
9. zk
7.9.11 / 8am


please continue
11. [Anne Romme](http://gravatar.com/anneromme)
7.18.11 / 9pm


Dear Lebbeus, thank you so much for having made this important book and now publishing it. Anne
13. [chlee19](http://hapticthoughts.wordpress.com)
10.19.11 / 9pm


Thank you for your time and passion. I always look forward to posts in heavy discussions such as this.
15. [Pesce, Woods, and Abraham | Joshua Wilson](http://www.joshuadwilson.com/2011/11/20/pesce-woods-and-abraham/)
11.26.11 / 10pm


[…] came across this article from Lebbeus Woods, describing his trip with Raimund Abraham to La Tourette.  Lebbeus Woods (and […]
