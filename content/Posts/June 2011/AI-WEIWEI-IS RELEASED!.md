
---
title: AI WEIWEI IS RELEASED!
date: 2011-06-22 00:00:00
---

# AI WEIWEI IS RELEASED!


The terms of [Ai's release](http://www.nytimes.com/2011/06/23/world/asia/23artist.html?hp) are stringent, but the important thing is that he is free. We should celebrate that, whatever the terms, and hope the best for his future.


LW


[![](media/aiweiwei-released-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/06/aiweiwei-released-1.jpg)


For previous posts on Ai Weiwei, see: <https://lebbeuswoods.wordpress.com/2011/04/03/7669/>


Postscript: Refer to slothglut's comment below. While he is certainly correct in saying that the restrictions on Ai's freedom are severe, I believe we must not assume the worst but must realize Ai needs time to figure out his way of dealing with them. I have no doubt he will find ways to express himself as he always has.


LW



 ## Comments 
1. [THIS CANNOT PASS (updated June 22, 2011) « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/03/7669/)
6.24.11 / 9pm


[…] 06.22 AI WEIWEI IS RELEASED! […]
3. slothglut
6.25.11 / 3am


Please take a look why “imprisonment” is no longer confined into a certain detaining space:


<http://www.theatlanticwire.com/global/2011/06/ai-weiweis-freedom-does-not-involve-much-freedom/39256/>


Stringent, is to put it lightly.
5. underscore
7.23.11 / 6pm


Lebbeus, I thought this would touch some thread within you…..






	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.24.11 / 11am
	
	
	underscore: Thanks for this. It's not to be taken lightly, either in its pros and cons, its promises and dangers. For me, this seems to be the ultimate Anarchist movement, long overdue. Still, the mass murder in Oslo shows the dark side of paranoid distrust of government. Not everyone can stay on the non-violent side, once people's fears have been aroused. The bombing of the Federal Building in Kansas City in 1995 by an American right-winger is yet one more example. 
	
	
	
	
	
		1. underscore
		7.26.11 / 5am
		
		
		Lebbeus: You speak truth and wisdom. There seem to be many pros within the plan and yet many things left open. There are tremendous promises offered within the plan, but the basis of the plan is completely reliant upon the people, that being either the weakest or strongest part of the plan. WIth that said, people have began to gather, online, and within only a few weeks thousands have joined. Keep an eye on the streets of New York upon the evening of the 30th and the truth of the people may be revealed.
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		7.26.11 / 4pm
		
		
		underscore: You've given us all a lot to think through. People demonstrating in the streets—what are the consequences, or, what does Anonymous hope they might be? For me, the covert activity of WikiLeaks might be the more effective way of forcing government to change. Freedom Square in Cairo was one thing, but Main Street in Des Moines quite another. As the old adage goes: “Revolutions are most often achieved by well-organized minorities.”
7. underscore
7.26.11 / 9pm


Lebbeus: Where to begin with this? The covert activity of WikiLeaks seemed to be a powerful beginning to what Anonymous hopes for. Alas, you are correct again, with your comparison of Cairo to Des Moines, but this is merely the beginning of The Plan. Anonymous is working through phase I, which calls for Assembly of the people. Egypt had the advantage of the prior knowledge of their oppressors, the people as a whole understood and knew who they had to revolt against. Demonstrating in the streets of Des Moines is nothing on the grand scheme of things, but, as stated, this is only the beginning, this is phase I; The Plan is a newborn and unorganized, small-scale demonstration is it attempting to scream, to get its voice out there, to let the people know.
