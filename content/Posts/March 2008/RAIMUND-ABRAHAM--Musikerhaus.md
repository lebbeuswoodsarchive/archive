
---
title: RAIMUND ABRAHAM  Musikerhaus
date: 2008-03-26 00:00:00
---

# RAIMUND ABRAHAM: Musikerhaus


[![Musikerhaus, Dusseldorf, under construction](media/Musikerhaus,_Dusseldorf,_under_construction.jpg)](https://lebbeuswoods.files.wordpress.com/2008/03/lwblog-ra1a-2.jpg "Musikerhaus, Dusseldorf, under construction")[![](media/lwblog-ra4greg1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-ra4greg1.jpg)[![](media/lwblog-ra3greg1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/lwblog-ra3greg1.jpg)[![](media/ramhaus5greg4.jpg)](https://lebbeuswoods.files.wordpress.com/2008/04/ramhaus5greg4.jpg)


Musicians' House. Four musicians will live here every year, one in each quadrant. Occasionally, they will meet in the center space and make music together, under the triangular opening to the sky. Music and mathematics and architecture. Rarely has geometry been considered so seriously in architecture, or taken to such poetic lengths. Clarity is not a conclusion, but the threshold to a mystery that is archaic and prophetic, belonging at once to the first questions ever asked, and the last ones that ever can be asked. An epic cycle of time is evoked here, and it both exhilarates and threatens.


How accustomed we are to architecture entertaining us with its novelty, or its richness, or its melodrama! This building challenges us with its sober, still, enigmatic presence. We are discomfited, unprepared. And then, we hear within it the sounds of music being played, crisp, equally clear and abstract, yet elusive, ephemeral….and we understand. This is what philosophers meant by “the dialectic.” This is what the architect, Raimund Abraham, means by “architecture is a collision between ideas and matter.”


Raimund Abraham's Musikerhaus in Dusseldorf is under construction and due to be completed this year.


interview with Raimund Abraham:  

<http://www.nyc-architecture.com/ARCH/ARCH-RaimundAbraham.htm>



 ## Comments 
1. [buzzcut » Blog Archive » Taking the Fun Out the “Play” House](http://buzzcut.com/?p=35)
3.27.08 / 10pm


[…] Musikerhaus « LEBBEUS WOODS How accustomed we are to architecture entertaining us with its novelty, or its richness, or its melodrama! This building challenges us with its sober, still, enigmatic presence. We are discomfited, unprepared. And then, we hear within it the sounds of music being played, crisp, equally clear and abstract, yet elusive, ephemeral….and we understand. […]
3. aes
4.6.08 / 10pm


the additional photos continue to astonish. thank you for the update of a project i had long admired and wondered about.  

any word on the other hombroich project, which cuts an axis towards this musikerhaus through an L-shaped housing piece?
