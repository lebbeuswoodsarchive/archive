
---
title: INTEGRITY
date: 2008-03-20 00:00:00
---

# INTEGRITY


(This is the fourth in a series of posts on the relationship between ethics and aesthetics.)


Hard as it is to believe, it was once thought that architecture had integrity. Not just the architects who practiced their profession with a principled, uncompromising sense of purpose or mission, but buildings themselves. Certainly, the attribution of human qualities to inanimate things like buildings is and always was what the literary critics call a ‘pathetic fallacy.' A ‘sad moon,' may work as a metaphor of our own emotions when we, on occasion, look at the moon, but no one would really believe that the moon itself experiences sadness, or anything else. And yet the belief that buildings had an uncompromising, principled reality about them was once widely held. As a student of architecture, nearly fifty years ago, I was taught that all good buildings had, in fact, three integrities, without which they could not be considered architecture.


The first was integrity of function. This meant that a building's form and spaces had to be be designed to serve their intended purpose. Fifty years ago, everyone knew this meant its form had to accommodate a known building typology: school, hospital, house, museum….all of which had been established by exemplary precedents. 


The second was integrity of materials. This meant that a material was always used according to its inherent properties. Wood, for example, was a ‘beautiful' natural material, so it should never be painted. The same with concrete, even though it was not beautiful or natural, it's roughness and ugliness were integral with its ‘character.' So, it should always be left exposed.


The third was integrity of structure. This meant that whatever held a building up had to be clearly visible as part of its form. Structure was not to be disguised or hidden, as in Beaux Arts buildings, burying, say, the steel frame structure of a building in masonry walls to achieve the effect of an ancient Roman or Gothic building. To do that was to betray the reality of the building, to turn it into something false, a lie, a mere stage set—the antithesis of architecture.


This idea holds that a building is—like a human body or a machine—an assemblage of parts having diverse purposes that must be integrated to form a coherent, working whole. In order to be integrated, each of the parts—function, materials, structure—has to be integral within itself, within its own, intrinsic nature, to join coherently with the others. It is rather like the old saying, “Be true to yourself, and you cannot be false to another.” Each part has to have integrity before the whole can have it, too. In this way, architecture is the embodiment of an ethical, as well as aesthetic, ideal.


Such thoughts were once taken very seriously. We still see their residue in the buildings of some prominent architects in the over-60 generation, who grew up with the idea that architecture had integrity. For the generations following them, however, a building's integrity–or lack of it–is no longer an issue. The idea is, in fact, never mentioned. 


Newer ideas hold that a building—like any product of manufacture—is a unit serving a prescribed purpose, within which its constituent parts are entirely subordinate to the whole. This means that the make-up of any part can vary, so long as the integrity of the unit is not violated. Perhaps the better word is efficacy–the effectiveness of the unit. The ease and speed with which the computer can integrate different systems has taken the emphasis off the different parts and placed it on the whole. In a practical as well as philosophical sense, the goal of design today is an overall synthesis of the elements of building—a form—that is achieved not as a result of assembling parts, but established at the beginning of the design process. Architects who work with any 3-D modeling program know that structure and materials are now subordinate to form. When concrete can be easily substituted for steel framing, or plastic for glass, or stressed-skin carbon fiber for riveted aluminum, without sacrificing the coherence of the form, then the lines between the former distinctions are sufficiently blurred to be insignificant. 


There is another factor. The global marketing of brand-name consumer goods has coincided with a social trend towards an effacement of differences based on racial, gender, economic and other stereotypes. It has created a new type of diversity based, ironically enough, on sameness. In the products we buy, the places we go, the ideas we have, we all share the same differences. In other words, we can have different designer clothing, or buildings by different well-known architects, but the differences between these brand-name products are not antithetical or opposed, like the old stereotypes (black/white, male/female, gay/straight, socialist/capitalist, modern/classical…), but are only variations on themes and trends dominating the marketplace at the moment. Ethical distinctions (right/wrong, good/bad…), which are seldom marketable themes, have— like integrity—become all but irrelevant.


LW


The Critic gets it (almost) right:  

<http://www.nytimes.com/2008/03/23/arts/design/23ouro.html?ref=design>



 ## Comments 
1. [fairdkun](http://lomohomes.com/fairdkun)
3.21.08 / 8am


a clear logic you mentioned over here.  

what's the case, in your opinion, with the brad pitt's ‘make it RIGHT' campaign then?
3. [lebbeuswoods](http://www.lebbeuswoods.net)
3.21.08 / 3pm


fairdkun: I think Pitt's initiative is great. All too rare. One of the reasons I put ‘seldom' in the last sentence of the post.
5. [Joseph Clarke](http://www.josephclarke.net)
3.21.08 / 4pm


It's interesting that integrity appears to have vanished as an architectural criterion around the same time as utopia disappeared as an architectural ideal.


Generally speaking, codes of ethics can be either personal or social; they can envision either an individually upright life or a well-functioning social body. Although the measures of “integrity” that you mention, Mr. Woods, are aesthetic judgments that apply to individual buildings, they do seem to be linked to social values, in the sense that a building that failed to express its materiality or its function would be “unreadable” by the public and thus an affront to the commons. When postwar architecture turned its attention from the ideal of “the good society” to that of “the good life” and private enclaves replaced public space as the principal domain of architectural thinking, it became OK and even praiseworthy to create illegible buildings. (Today we have buildings that are somewhere in between–what Charles Jencks calls “enigmatic signifiers”).


One shouldn't necessarily lament the passing of these old categories. It does seem a little silly to pronounce ethical judgments on buildings based on how they deploy materials, structure, and formal elements. But as arbitrary as these categories may have been (at least they seem so in retrospect), they did, at least, give us something we now lack: a common framework for thinking about the architecture we inhabit.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
3.21.08 / 7pm


Joseph Clarke: I agree that the passing of interest in integrity and ethical distinctions marks the end of utopian thought and aspirations, meaning a belief in progress and improving the human condition. These were the ideals of modernism, in response to the deplorable living conditions for many created by industrialization in the 19th and early 20th centuries. Sadly, these ideals were never fully applied by modernists themselves, who got sidetracked in the 20s and 30s by unsympathetic political events, the rise of Fascism and Communism, and World War II, among other things. Modernism failed, not only in architecture (becoming, in the end, elitist), but as an intellectual, political, and ethical movement. Post-modernist thinkers and architects, in reaction, had no belief in progress or ‘making a better world' but preferred to keep a distance—usually ironic— from such thinking. Perhaps they felt betrayed by the failures of the generation before them, and rejected not only their methods and forms, but also their goals.
9. Josh V
3.24.08 / 3pm


It seems to me that very often architects are falling prey to pleasing the client. Few clients put faith in an architect to do it all and few architects demand that faith. Their dreams, even if they have them, never have the opportunity to see light. The public demand for cheap crap has taken precedent over the architects control over the supply. To hear architects make statements like “Our goal on this project is to not be embarassed” makes me cringe.


Why are so many of us “playing it safe”?
11. [Joseph Clarke](http://www.josephclarke.net)
3.24.08 / 3pm


JV: Yes, I wish that architects had the will and the credibility to be more ardent stewards of the built environment!


LW: Even though the dominant architectural paradigms have rejected the idea of progress, many still seem to be animated by a faith in novelty. Techniques aimed at generating new forms, such as diagrams and scripting, are so easily instrumentalized by the self-propelling engine of global development with its fantasy of unending growth that, despite frequent glosses of Deleuze, they rarely seem to have much critical value. The results of all this “innovation” can be seen in the nightmarish designs for the Dubais of the world, where the total disappearance of public space has enabled an all-out competition among buildings to outdo each other in spectacle.
13. Penley Chiang
3.24.08 / 5pm


1. The subordination of parts to the whole has indeed allowed architects to amplify the ‘gesture' of their buildings. The paradox of this extreme iconicity is that architecture has actually become much less ‘articulate' despite being infinitely more technologically complex than, say, a mere generation before. When the parts of the building are incapable of asserting an identity with respect to, or give resistance to, the overall form, the game is up as soon as you've absorbed the punch line, rendering it subsequently ‘mute.' (About a decade ago Cacciari published an article called ‘Shards of the All' in Casabella depicting this precise phenomenon. Despite being purely metaphysical in its overt content – platonism, et. al. – its preoccupation was intensely tectonic.)


2. In another sense the current situation could be seen as the apotheoisis of the modern. In his ‘Details of Modern Architecture,' Ed Ford chronicles, example by example, how the ‘dream' of the modern held up monolithic construction as an ideal while at the same time being inexorably pulled toward the tectonic, an assemblage of increasingly intricate and varied parts. Is not the ‘integral' nature of contemporary building culture the (perhaps unintended) unity of the two?


3. It may be somewhat disingenous to portray ‘integrity' with respect to form and structure as timeless principles that have only recently been subverted. Has there not always been a (generally fruitful) tension between integrity and its other, ‘effect'? For every example of clarity one can find a scholium of smoke and mirrors. The optical ‘corrections' in Greek temples; he bombasticism of the Romans; Renaissance discourse of column vs. wall; Semper and his principle of cladding; the spatial theatrics of Loos; Mies' decorative I-beams; even and most especially Corb, for whom ‘free plan' meant ‘freedom to create whatever spatio/temporal/plastic composition I damn well please.'
