
---
title: DELIRIOUS DUBAI
date: 2008-03-05 00:00:00
---

# DELIRIOUS DUBAI


[![Koolhaas' vision for Dubai](media/Koolhaas'_vision_for_Dubai.jpg)](https://lebbeuswoods.files.wordpress.com/2008/03/lwblog-dubai3.jpg "Koolhaas' vision for Dubai")  

[![Koolhaas' vision for Dubai–model](media/Koolhaas'_vision_for_Dubai–model.jpg)](https://lebbeuswoods.files.wordpress.com/2008/03/lwblog-dubai1.jpg "Koolhaas' vision for Dubai–model")


One thing for sure, Rem Koolhaas doesn't hedge his bets. He also knows how to stick his neck out and not lose his head. He has perfected the old debating trick of disarming his critics in advance. Philip Johnson was also a master at this. Before anyone could criticize the pandering commercialism of his office tower designs, he would say, “I'm a whore.” Rem Koolhaas gives this tactic a European sophistication, a rhetorically polished upgrade. He says that he is trying to “find optimism in the inevitable.” The “inevitable” sounds like fate, something beyond human control, and has an ominous ring to it. Death, of course, is the ultimate inevitable, and who could criticize someone who is defiantly optimistic in the face of that? It's a heroic position, no doubt, if, that is, the inevitable is as certain as it is made to seem. The inevitable in Koolhaas' discourse is the ultimate world domination by ‘liberal democracy' and unfettered, free-market, capitalist economics at the expense of other modes of human exchange. He has set out to put an optimistic face on this inexorable process. 


Koolhaas has been plying this idea for quite a few years now. In the 80s and 90s, it took the form of “in early Modernism, the heroic thing for architects to do was fight the mainstream; today it is to go along with it.” Then came “Bigness,” followed by the “Generic City,” and let us not forget “Shopping.” Now comes his big Dubai proposal, which amounts to transplanting a chunk of the Manhattan he celebrated for its “culture of congestion,” thirty years ago, in “Delirious New York,” onto an artificial, offshore extension of the city of Dubai. 


Dubai is certainly the inevitable place for the realization of Koolhaas' ideas. It is by now the capital of an economic and political New World Order. A city-state without income taxes, labor laws, or elections, it is ruled by a corporate oligarchy of hereditary rulers, accountable only to themselves and their investors. Quite a model for the global future. Built up rapidly over the past few years on the wealth gotten from the world's greed for oil—and more recently as an unregulated sanctuary for cash—it has no depth of history or indigenous culture, no complexity, no conflicts, no questions about itself, no doubts, in short, nothing to stand in the way of its being shaped into the ultimate neo-liberal Utopia. 


Unlike Manhattan, which grew incrementally on its grid over two centuries and is laden with everything from history to conflicts and self-doubt, Dubai is a kind of frontier boom-town that has to import everything to be anything, from workers to investors, from ideas to architects, from high culture to low. Now, apparently, it is importing…congestion? Sitting between two deserts, one of water and one of sand, congestion does not come naturally to Dubai. The dynamic compression of space and activity that creates a critical mass of imploding human energy called a city has to be imposed there as an idea and somehow generated as a reality over the next few years, without genuinely urban conditions. The strategy seems unlikely to succeed, except as another attraction in the high-end theme park Dubai has become. But that is not the proposal's most disturbing aspect. Given the tabula rasa the site offers, and the apparently unlimited finances its owners possess, we might ask: is this the best vision for the future that the architect could come up with?—a gratuitious look backward at the ultimate 20th century city, rather than an imaginative look forward to the possibilities of the 21st century city. 


What, for example, are the space-organization possibilities of networks of information exchange, rather than streets? What are the architectural design possibilities of synthetics, rather than steel or concrete building frames typical of high-rise construction? What are the possibilities for increasing choices in non-hierarchically organized urban spaces, rather than classical, Cartesian systems? And so on—the list of new possibilities is long. 


Maybe Koolhaas doesn't believe that Dubai is the place for a forward-looking vision. Or maybe he believes, true to his post-Modernist roots, that the past offers the best model for the future, if it is leavened with irony, and garnished with a dash of the surreal. Or maybe he simply doesn't have a vision for the future. Who knows? We should care, however, because the world's attention is focused on Dubai, and on Koolhaas and other architecture stars, and because—like it or not—what they do is taken as a model for the future, even when it is, how shall I say, not nearly good enough. 


LW


New York Times article on Koolhaas' proposal for Dubai:  

<http://www.nytimes.com/2008/03/03/arts/design/03kool.html?ref=design>


[Mike Davis article on Dubai](https://lebbeuswoods.files.wordpress.com/2008/03/mike-davis-on-dubai.pdf "Mike Davis article on Dubai")



 ## Comments 
1. Sayem (rajit) Khan
3.6.08 / 4am


If history does repeat itself “Dubai” has all the symptoms of a modern day “Babylon”, monoculture, oppressed labor force building the tallest tower in the world. No need for any God, Prophet or genius environmentalist like Al Gore to tell us that there is something fundamentally wrong with creating an artificial habitat that strictly depends on buying water for it's occupants, because that is all they are, occupying a piece of desert where life simply was not meant to sustain itself. My grandfather once said to me, “Where there are no trees, there are no humans, because there is no water.”





	1. Desert dweller
	5.14.09 / 6pm
	
	
	Well, your grandfather is wrong. life exist in the deserts as in any place in the world.As for the opressed workers, they are not there against their well. They can leave whenever they want. Plus they get ten times what they are normally paid in their home countries. Another thing, Dubai is far from being monocultural society, there are more than 170 nationalities that live and work in that city and the local population make up only 8%.
3. Godofredo
3.6.08 / 12pm


I think you've mentioned a very important issue here: there is a lack of thinking on what creating and inventing really mean. 


Architecture is progressively being reduced to the mere role of a graphic illustration of urban and political decisions taken by others.


This mania of being contemporary (behind Koolhaas support of neo-liberal politics) is a result of the same disease in the origin of historicism – it constitutes in fact an historicism of the present…No surprise that the same architect attempts both at the same time. 


But it's also a way of ignoring any attempt of a critical and productive discourse based on architecture as a truly creative act.





	1. Feroz
	10.16.11 / 12pm
	
	
	Its true that you say. Besides creativity they are progressing to a state of anonymity (have no unique or indigenous style); they all lost it to the ‘modernism'. In fact creativity has become an another international style rather that an instinct to respond to the indigenous conditions.
5. [Stephen Lauf](http://www.quondam.com)
3.6.08 / 4pm


In 2001, the finished *Ichnographia Campi Martii* will be 240 years old, yet Piranesi's truly unique urban paradigm — a city “reenacting” itself through all its physical, sociopolitical, and even metaphysical layers — may well become the most *real* urban paradigm of the next millennium.  

—[1999.11.21](http://www.museumpeace.com/15/1437.htm)
7. Josh V
3.6.08 / 6pm


I worry for when the rebel army discovers the 2 meter exhaust hatch…


But seriously, this seems to be a continuation of the Manifest Destiny concept of humanity to expand and conquer everything. When the land was all taken, we built up. Now that this “new” piece of land is exploding into the mainstream, it's a rush to build there. The new Manifest Destiny. Can you imagine what the United States would look like if these archtiects and technology were around 250 years ago?


Innovation at its roots is just problem solving and unfortunately the only problem most architects/clients see is not being well-recognized enough. Everything is just a show, attention-getters, distractions from the competition and from their actual quality of business. Maybe if the attention of the problem-solving was directed more towards pressing problems (such as economic and ecological catastrophes) we might see more amazing applications of money and technology. I have high hopes for the Masdar City.
9. Pop-Rouge
3.6.08 / 6pm


This critique is as poignant as the project is shockingly banal and the renderings eerily pastoral. Thank you!  

It seems as if Rem's strategy of pushing the normal to abnormal extremes has found a place where the extreme has already been normalized in Dubai. He might do well to put his thinking cap back on before tackling this problem with the usual bag of tricks – maybe revisit what it means to create a master plan for a nonexistent city in 2007/8, when the idea of even what urbanism is, is controversial. To be honest this proposal seems shockingly lazy, on the order of a second year student project with access to a render-farm and CNC machine.
11. aitraaz
3.6.08 / 10pm


Bravo Lebbeus! An excellent critique – while i have always appreciated Koolhaas's sense of wit and irony, i am still a fervent believer (as no doubt you are) that architectural concepts of authenticity and substance have become of utmost importance, albeit increasingly complex and difficult, facing the realities that contemporary urbanism presents (Dubai being a prime example), but vital nonetheless…
13. Elan
3.7.08 / 9am


Have you noticed that Koolhaas' master plan is an ancient Mesopotamian city? I think it may be the plan of Ur… There is a city wall, and a trading center (sacred because it is square on earth), a ziggurat and most importantly this spherical (temple/palace) that occupies the edge between the sacred square and the exterior. (Only one such building holds this tangency). I wonder if this is the ‘pre-emptive critique' that Lebbeus mentions, in which Koolhaas drives Dubai all the way forward to its ancient primeval structure.
15. Godofredo
3.7.08 / 10am


I found this wonderful quote from Koolhaas: “The world is running out of places where it can start over.” 


Unfortunately it seems that K. is looking to the desert as some sort of “tabula rasa” where he can be free to explore whatever comes to his mind – and obviously with no regard to social or political aspects.  

Worst, it also shows that he does not believe in the possibility to start over where things already exist: revolutions are no longer possible, long live the capital. 


Isn't Koolhaas (and several others) giving up while roaming the globe in search of places where to start again?


In the end it seems Koolhaas wants to play God.  

And that, coincidently, is very fortunate because Dubai wants to build some sort of Paradise Lost…


Anyway, two more articles on the other side of Dubai:


<http://findarticles.com/p/articles/mi_qn4158/is_20060328/ai_n16180501/pg_1>


<http://www.guardian.co.uk/business/2006/feb/13/unitedarabemirates.travel>
17. [David](http://www.buzzcut.com)
3.8.08 / 4am


I wonder about Dubai. Do we blame the capitalist process that Debord nailed so long ago? The spectacular society? Or do we blame archi-theory itself?


Koolhaas, like all the master archi-salemen, understands his milieu and has mastered its rhetorics.


That is to say, Archigram thought cheeky fantasy was great criticism. All these years later, how could they have predicted that their pointy irony would have provided the blueprint for serious architecture?


Delirious Dubai? Brilliant analogy. Inevitable Dubai? Perhaps a statement of fact!


— David
19. Keith
3.8.08 / 1pm


This is an absolutely cogent critique. I think Koolhaas has overplayed the rhetorical and the sense of irony that have been consistent features of his discourse. Woods commented with ample sarcasm that the strategy employed may be traced to Koolhaas' post-modernist roots and hence the borrowing from the past. I would however argue the motivation is not so much the desire to look to the past, but to appeal to the surrealist aesthetics of the unexpected, namely Lautréamont's phrase, “beautiful as the chance encounter on a dissecting-table of a sewing-machine and an umbrella”. An identical strategy was in fact employed in the landscape design of CCTV, where Piranesi's drawing of the Roman city was deployed as the base texture. Given this, what disturbed me about this proposal is that tens of billions of dollars will be spent in concretizing rhetorical speculations. A delirious state of mind is perhaps indeed conducive to surrealist visions.
21. aes
3.8.08 / 4pm


i'm also interested in the notion of rhetoric, built or otherwise, situated in a tabula rasa. if not dialectically pitted against the friction of site, specificity, or cultural context, the critique sets up its own straw man. even in china, where the cultural context seems to have been obliterated, and where one might argue that the CCTV building is sited less in a place than in a geopolitical/economic context, the delirium that china is experiencing provides another kind of a-temporal, a-spatial tabula rasa. in which case: isn't koolhaas proposing a series of isolated sculptures, both in an urban and architectural scale, detached from the very ground it proposes to critique?
23. [progressive reactionary](http://progressivereactionary.blogspot.com)
3.8.08 / 9pm


LW- your critique is timely and — particularly in its comparison of Koolhaas to Johnson — quite provocative. Much has been made of the resemblance of the Koolhaas persona to that of Corb or Mies, but the Johnson reference is very prescient and deserves further exploration…  

I completely agree with your argument, but the one thing I would personally add (and which I've written about previously) is a lament that someone as intelligent, skilled, and fundamentally *critical* in his approach to architecture is not attempting — even if just a little — to try and insert some kind of progressive twist into these megaprojects. It is regrettable that Koolhaas, with these many opportunities in the UAE, China, and elsewhere to implement such large-scale architectural change, does not further question the political, social, or cultural status quo. Indeed, it seems that the latent critical edge, so present in some of his earlier work and writings, diminishes with each new, spectacular project.


–[PR](http://progressivereactionary.blogspot.com)
25. Sayem (rajit) Khan
3.8.08 / 10pm


At what point can we say to this Architect, whom I believe is conscious of the injustice that places such as Dubai is harvesting and say enough is enough we are taking you to “court”. The Architectural Justice Court where architects will be tried for environmental degradation, labor injustice, and creating socio-economic dilemmas that were result of their egotistical intellectual pursuit.
27. DesignerGuy
3.8.08 / 11pm


I am new to this forum. I wanted to raise the question of the apparent lack of local desert vernacular (if that exists) on the urban topography of Dubai. Where is the history? I understand Dubai was just a small town until very recently but one has to question the long-term consequences of such rapid development and ponder the future goal of the stakeholders involved in this frenzied growth.
29. [Mark Primack](http://www.markprimack.com)
3.12.08 / 3am


I was a student at the AA in London when Koolhaas was completing ‘Delirious New York' there. I'm sure he was as familiar as I with the line from a punk anthem of that moment, ‘We are the future- no future.'  

Dubai is the future only if there is no future, for the reasons you all have posited here. I keep being reminded of that archaic phrase, ‘an embarrassment of riches'. In Las Vegas it manifests in a desperate attempt to spend it all quickly and be done with it, so that we can perhaps get on with a life that has some meaning or purpose. Dubai appears to be burying their oil wealth back under the sand where no one will ever see or use it again. Serves us right.
31. Danny
3.16.08 / 6pm


Rem is not only importing Manhattan, but Coney Island, too. 


It seems the unbuilt [Globe Tower](http://www.westland.net/coneyisland/articles/globetower.htm), which he writes about in Delirious New York, may finally happen.
33. [on koolhaas at ecological urbanism - mammoth](http://m.ammoth.us/blog/2009/04/on-koolhaas-at-ecological-urbanism/)
4.10.09 / 2pm


[…] Urbanism conference was a joke (on the attendees?). But, then, a very cynical person might say that this is typical, rather than atypical, of his recent work. Fortunately some of the other presentations were much […]
35. [Superordinaire.ca » Blog Archive » ICONAUTS MAKE IT IN THE N.Y. TIMES!](http://superordinaire.ca/?p=6)
4.22.09 / 9pm


[…] Woods gives the project a harsh critic… Tags: dubai, […]
37. [Delirious Dubai « Interruptions](http://interruptionsblog.wordpress.com/2009/10/10/delirious-dubai/)
10.10.09 / 8pm


[…] Delirious Dubai Lebbeus Woods on Koolhaas' proposal for Dubai […]
39. [die fragen, die wir meinen. und vermissen « KONNTAINER](http://konntainer.wordpress.com/2010/07/25/die-fragen-die-wir-meinen-und-vermissen/)
7.26.10 / 2pm


[…] wir (ArchitektInnen) nicht verantwortlich seien und somit verantwortungslos(!) handeln könnten (Rem Koolhaas) kann keinen aufrichtigen Menschen überzeugen. Soll sie auch nicht. Sie soll vielmehr unser […]
41. [ANOTHER ONE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/13/another-one/)
12.13.10 / 11pm


[…] the case below, the place is Saudi Arabia, looking very like Ordos, Inner Mongolia, and Dubai's displaced Manhattan by Koolhaas. A formula that works, so it seems. New versions are just over the […]
43. Cesar Cheng
12.19.10 / 7pm


<http://www.eikongraphia.com/?p=2348>
45. [famous footwear coupons » 41. Take a lesson from Lebbeus Woods](http://www.famousfootwearcoupons.tk/41-take-a-lesson-from-lebbeus-woods/)
5.2.12 / 11am


[…] This brings me to Lebbeus Woods. I happened to look at his website the other day and was really surprised in a pleasant way. You see, for me Woods was old school in my mind. I associated him with a generation of architects from yesteryear. So when I saw what he was doing today, (like I mean now) I was impressed. His was not an old HTML site that you would expect. It was flash based, it had sound and video clips, it had graphics that were consistent with the architecture that we have come to know as distinctly Lebbeus Woods. It was like the old guy was reborn in the 21st century. He even has a regularly updated blog with some pretty interesting stuff that you all should read.(be sure to check out his wickedly sharp critique of the notorious Rem “Delirious Dubai“) […]
47. [famous footwear coupons » 45. Warning #2: Beware of the absurd (John Silber)](http://www.famousfootwearcoupons.tk/45-warning-2-beware-of-the-absurd-john-silber/)
6.9.12 / 5am


[…] in starchitecture today. It is well due for a serious critique. I have previously pointed out Delirious Dubai as a good start. Architecture of the Absurd is far from […]
49. eco3
9.24.12 / 9pm


As a former architecture student disillusioned with the designer architectural practice today, I do agree with you in criticizing Rem's “embrace of the inevitable”. Rem Koolhaus is actually coming to speak at my former school, Rice University SOA, this semester. While in school there, I often found myself questioning why I was building this art center or an exhibit for a nature preserve, when clearly it was more of a monument or sculpture than something that was actually environmentally conscious or sensitive to the inhabiting people. We did studies and this and that to “prove” that we were considering the environment and inhabitants, but they were more theoretical and not actually realistic. 


In reality my school upholds architects, like Rem Koolhaus, for “embracing” artificiality in architecture and egotistically viewing architecture as an environment which renders the natural environment unnecessary. Think about Rem's proposal for Delirious New York, where people would not have to leave the skyscraper in order to find everything they needed to live. I suppose they will find a way for the buildings to create oxygen when all the trees are cut down to make way for our cities, or find a way for the buildings to inject us with vitamin D when we no longer have to go out to soak up the sunlight?


Humans need the natural environment. We need ecosystems to ensure the environmental balance of our world. See what happens when we are immersed in unnatural environments. Stress in urban areas is higher than rural areas. Crime is more prevalent in urban areas. We are not supposed to exist in artificial environments. Unconsciously, we need to feel connected. Artificial environments do not connect us to anything- they are closed systems which do not rely on symbiotic relationships to function.
