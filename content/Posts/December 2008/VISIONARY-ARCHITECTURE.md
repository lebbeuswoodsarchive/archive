
---
title: VISIONARY ARCHITECTURE
date: 2008-12-11 00:00:00
---

# VISIONARY ARCHITECTURE



“They'll tear down your building in twenty-five years. All that will remain are its ideas.”


            Louis Sullivan


 “….but what if it never gets built?”


            anonymous


 The term ‘visionary' generally refers to a person (and things he or she does) who exists somehow apart from the real world—as in ‘having visions'—and it is usually reserved for saints and the brilliant-but-unbalanced. It is a term used to marginalize certain architects and their works, and in that sense, is both pejorative and highly political. Still, it is almost instantly understood by most as having to do with the innovative and the new and on that limited basis serves present purposes.


 The two examples of visionary architecture posted here each exhibit at least three essential attributes.


 First: the projects propose new principles by which to design for the urban conditions they address. In the case of Steven Holl's 1991 project for Phoenix, Arizona, he gives the city a distinct edge, rather than allowing it to continue sprawling helter-skelter into the desert. His argues for the establishment of the city's physical identity, and for the desert's—each is to be spatially defined, though porously, so that they are not cut off from one another. In Le Corbusier's 1924 Plan Voisin for Paris, the historically overburdened center is to be replaced with a rational matrix of widely-spaced modern buildings heavily interspersed with lush parks and gardens. Both offer radical new conceptions of these cities and their transformations.


Second: the designs are total in scope. All scales—from the individual dwelling unit to the overall urban field are addressed and integrated. Also considered are the physical relationships proposed between architecture and nature, the sky and the ground, as well as the conceptual relationships between the past and the present, the individual and the society, and other such weighty philosophical issues. A vision sees the world whole.


Third:  the designs invent new types of buildings. Existing typologies are not useful to implement the new urban principles. Holl's “retaining bar” buildings and Le Corbusier's “cruciform” skyscrapers were new building types, creating new relationships between people, and between people and the landscape they inhabit.


Is or was it ever likely that either Holl's or Le Corbusier's visionary projects would be realized? No. Did that matter to the architects? Not likely. So, why did they make them? To unequivocally articulate a set of architectural principles and design ideas —and to test them. Were the drawings and models made by the architects sufficient to accomplish these goals? Yes. In both cases, the drawings and models were made to be clear and understandable, to any intelligent person, but especially to critically minded architects.


There is an instructive story about Plato, who was an architect only in the sense that he envisioned the ideal social architecture of a human society—The Republic—in his Dialogues. Upon hearing about it, a certain Dionysos, the tyrant of Syracuse, was so enamored of Plato's ideas that he invited him to come to his city and establish the ideal society in reality. Plato, for reasons that are not recorded, decided to accept the tyrant's offer, so off he sailed. After only a month, the intrigues of Dionysos and his court—who felt threatened by the radical new ideas—became so intense that Plato had to flee, and was lucky to escape with his life.


Did Plato fail to ‘realize' his vision of The Republic? Yes. Did that prevent the ideas of The Republic from having further influence? Hardly—they have been some of the most influential political ideas in history. Indeed, their not being realized has undoubtedly helped keep them alive for so long.


LW 


Steven Holl's “Edge of a City” project for Phoenix, Arizona (c.1991):


![holledge2b1](media/holledge2b1.jpg)


![holledge1c](media/holledge1c.jpg)


![holledge1b](media/holledge1b.jpg)


![holledge2a](media/holledge2a.jpg)


 


Le Corbusier's “Plan Voisin” for the center of Paris, France (c.1924):


![voisin11](media/voisin11.jpg)


![voisin3](media/voisin3.jpg)


![voisin2](media/voisin2.jpg)



 ## Comments 
1. [Woods on Visionaries « ubiwar . conflict in n dimensions](http://ubiwar.com/2008/12/11/woods-on-visionaries/)
12.11.08 / 10pm


[…] in ubiwar by Tim Stevens on December 11th, 2008 It's typical of Lebbeus Woods that he can write the following today about ‘visionary' […]
3. [infopollen » di-visionairies](http://infopollen.net/di-visionairies/)
12.12.08 / 12am


[…] The full post, very much worth reading, is here. […]
5. [paulo](http://cosmopista.wordpress.com)
12.15.08 / 3pm


and how would you evaluate architectural propositions equally visionary, but concerned with the articulation and testing of distopian ideas? i'm thinking about superstudio, young koolhaas, archizoom…
7. [lebbeuswoods](http://www.lebbeuswoods.net)
12.15.08 / 7pm


Paulo: As i strongly suggest in my recent post, TIMESQUARE, only a ‘dystopian' vision can evoke a new ‘utopia.' The dystopian is at once a critique of the status quo—with all its faults—and at the same time a model of how to go forward from here and now. The dystopias of the architects you mention were all very important in their time, and remain so, to the extent that they can inspire present efforts. My question: where are the present—I mean NOW—efforts?
9. [rob](http://eatingbark.covblogs.com)
12.15.08 / 9pm


Is it possible that, as the modernist moment passes, so too the possibility of the lone ego (noting that this is a descriptive, not pejorative, label) bravely reinventing according to his (her) spectacular vision, also passes?


In other words, is the project of visionary architecture, defined as an architecture which so transcends the present as to inexorably (though perhaps subtly) drag the present along with it towards utopia, dependent on an acceptance of the philosophical project of modernism (probably extremely broadly defined, to include the first wave of postmodernism — Nietzsche and those who have extrapolated from Nietzsche)?


If the passing of modernism impacts the plausibility of any of the three aspects of these projects you highlighted, I would think it is most likely the second aspect, totality of scope. Is it peculiarly modern to think that attempting to address and integrate the whole of a city (or City) in a single project is wise or helpful? 


Though someone could also probably make a good case that the notion of utopia itself is suspect (and dependent on an acceptance of unacceptable aspects of modernism). And a counter-case made that to do so would misunderstand (too narrowly construe) utopia. Which case is stronger, I don't know.


I'm not going to pretend I know the answer to these questions, but they seem worth asking, particularly in response to the question “where are the present [visionary] efforts?” (and if you answered these questions one way, the answer might be that that's the wrong question to ask, though I'm not at all sure that my questions should be answered in that fashion).
11. [lebbeuswoods](http://www.lebbeuswoods.net)
12.15.08 / 11pm


rob: Modernism, in it's original form (1910-1940+) was driven more by socialist ideals than by individualism. Individualism is what drives the present era in architecture, in that there are no widely shared principles or ideals, but only individual, idiosyncratic utterances and gestures,


In the post, I don't criticize the two examples or otherwise analyze or evaluate them. I'm more interested in what constitutes the visionary in them. Other visions are possible and no doubt needed. They may not share the three attributes I mention. So, what then constitutes the visionary today?


You are right about the issue of ‘totality' of scales being a rather outmoded concept. Yet, without addressing the micro and the macro together, how do we define, let alone embody new principles (rules of the game)? That, it seems to me, is the ‘visionary' task to be accomplished.


I, myself, have made some moves in that direction.
13. [rob](http://eatingbark.covblogs.com)
12.16.08 / 5pm


1. I suppose I should have specified (since this is an architecture blog) that I was referring to the broader philosophical movement of modernism (roughly Descartes-Nietzsche, and certainly including the incarnations of socialism which informed architectural modernism), not the shorter-lived architectural movement by the same name; I think my questions make more sense understood in that context.


2. That said, if I had a point beyond asking questions which I don't know the answer to, it was probably something like “I expect that a present visionary architecture would be barely (if at all) recognizable as akin to Corbusier or Holl”, which by no means implies disagreement with the idea that such an architecture is absent in the present (or with the broader point of your post, which I took to be something like “*but what if it never gets built?* is a misleading question”).


But now that I've re-read what you wrote a couple times, I think I have learned something and answered some of my own questions; in particular, I think that the first characteristic you identified — “the projects propose new principles by which to design for the urban conditions they address” — might be truly essential to visionary architecture, and might incorporate the less obsolete portions of the notion of ‘totality' while leaving out the mistakes of (philosophical) modernism (which believed it possible and desirable to build a new world from nothing, or to achieve such effective and comprehensive control of this one as to be indistinguishable from building a new world).


3. *Yet, without addressing the micro and the macro together, how do we define, let alone embody new principles (rules of the game)?*


Is part of the answer that we accept that we are limited in our ability to define the rules of the game?


I like, for instance, what you had to say in [this post](https://lebbeuswoods.wordpress.com/2008/02/09/slums-one-idea/) proposing a capsule to be inserted into slums:


*“It is especially important, then, that the transformative capsule enables the slum-dwellers to achieve their goals, serving their values, and does not reduce them to subjects of its designers' and makers' will. Inevitably, the values, prejudices, perspectives and aspirations of the designers and makers will be imbedded in the capsule and what it does. Therefore the slum-dwellers should, in the first place, have the right of refusal. Also, they must have the right to modify the capsule and its effects as they see fit. It cannot be a locked system, capable of producing only a predetermined outcome. The implication of these freedoms is that the capsule, whatever its capabilities, could be used to work against the intentions of its designers and makers.”*


I don't think that a (philosophical or architectural) modernist would have allowed for this (though such a subversion of their intentions is in fact what happened). Maybe you could say something like “here the rules of the game are not redefined; they are skirted, which subverts the rules about how the rules of the game are to be defined.” Sure, in one sense, the ‘rules about how the rules of the game are to be defined' are just another part of the ‘rules of the game', but they're an entirely different set of rules than the rules the modernists would have seen fit to operate on — and they're exactly the set of rules that holding to a (philosophical) modernism would make us blind (by focusing on trying to control the wrong set of rules) to the importance of. I'm not sure that formulation captures the difference between your capsule suggestion and, say, the Corbusian plan for Paris very well, but I think there is an esssential difference there, even if I'm not doing a very good job of articulating it.
15. [lebbeuswoods](http://www.lebbeuswoods.net)
12.16.08 / 7pm


rob: Thanks for the clarification, and for the close reading of my other, related posts.


The ‘moves' i have made are, indeed, reflected in the quote from the SLUMS: One idea post. My work has (explicitly, from the late 80s) been concerned with strategies and tactics of the unpredictable and the spontaneous, even the uncontrolled—but within a set of rules or boundaries that avoid the pitfalls of ‘chaos' or “anything is OK.” The designs I've made are intended as “heuristic aids,” not as a means of controlling a final result.


My purposes in showing the examples by Holl and Le Corbusier are 1) they are outstanding expressions of imaginative vision; 2) they demonstrate the architects' feelings of personal responsibility for the human condition; and 3) they are each works demonstrating the architects' aspirations to integrate different fields of knowledge, which I believe architecture—as a comprehensive field of knowledge—should always strive for.
17. martin
12.21.08 / 11pm


in regards to your question of ‘where and/or what is visionary architecture today?'


certainly projects by mitchell joachim could be considered visionary today, and i think that he signifies a specific type of visionary in today's dialgoue. specifically that of the computer derived visionary, which is directly related to formal play, rather than affecting societal issues as Corbusier or Holl attempted.


i am currently about 2 yrs out of an undergraduate architecture degree, and i must say that during my school years, any “visionary” designs were put down frequently and strongly. after being frustrated by this, i attempted to make my 5th year thesis as much outside the norm as possible to try to at least foster discussions on the visionary and was only met with silence or derision.


so…i think that early fostering of visionary attitudes is whats causing the lack of current visionary work. it has somehow developed into a sort of mimicry and then becomes trite so it is eschewed in favor of designs that resemble late modernism (mies van der rohe, for example)


i feel as the vast majority of contemporary architecture has a terrible blandness and modern slickness that leaves you feeling extremely unsatisfied, even if there are moments of cleverness about the designs. 


i have to wonder if the current economic situation will provide another upswing in the unbuidables. what do you think?
19. [matt bua](http://www.bhomepark.blogspot.com)
12.24.08 / 3am


Nice to run across this dialogue,  

Here's a call for a book project looking for visionary drawings…


<http://www.incidentreport.info/id92.html>
21. [Buamai » VISIONARY ARCHITECTURE « LEBBEUS WOODS](http://www.buamai.com/image/4800)
2.10.09 / 10am


[…] black and white, housing, phoenix, stephen holl Posted by will patera on February 10th, 2009 Original Source Post Comment Click here to cancel […]
23. [EXUBERANT SPACE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/09/13/exuberant-space/)
9.13.11 / 11pm


[…] dead. Looking at some of Steven Holl's later projects, such as the Horizontal Skyscraper, and the Edge of the City project for Phoenix—as well as Dujardin's designs—it is clear that they are very much […]
