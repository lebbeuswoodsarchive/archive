
---
title: TIMESQUARE
date: 2008-12-08 00:00:00
---

# TIMESQUARE



![tsq49a-blog71](media/tsq49a-blog71.jpg)


The abandoned New York Central railyards, between 57th and 72nd Streets on the Hudson River, were—in 1987—one of the last remaining Manhattan sites ripe for large-scale commercial and residential development. Unlike the 34th Street railyards, which were and remain active to the present day, the vast Upper West Side property had been unused for many years. Or almost. At the northern end, at the entrance to the train tunnels that run under Riverside Park, a shanty-town had been built and occupied by squatters, including the “Mole People,” who lived in the tunnel. A peaceable lot, who made money by selling books and other collectible detritus a few blocks away on the sidewalks of Broadway, they had built, in some cases, rather substantial shacks in the time-honored way of people with little money but a determination to settle down. They earned, but not enough to afford Manhattan's high rents, so they moved into spaces that no one else wanted. They were the first to go, in a single police sweep, along with their bulldozed constructions, when a major developer bought the property and prepared to build.


One has to resist pitying those squatters. Pity is a treacherous emotion, for everyone involved. Better to respect them. Their way of life, as chosen as any in the capitalist jungle (don't imagine that the rich are really free), included the certainty that they would one day have to move on, probably very quickly. They were prepared and no doubt found other ‘undeveloped' spaces to settle down in for the next timeframe, whatever that would work out to be. On the other hand, their scattering was traumatizing and unnecessarily brutal. And another thing: their little community had a spirit of invention impossible to achieve in the emotionally arid and highly regimented skyscraper landscape that was soon to come.


 In anticipation of what was to come, Michael Sorkin, John Young, and I began to meet once a week to discuss strategies for the development of the site that would be a clear alternative to the developer's predictable, profit-oriented thinking. Sorkin was writing scenarios, Young was doing research, and I was making design sketches that attempted to bring together the different strands. What evolved was a ‘duty-free zone'—‘duty' understood in at least two meanings of the word. What we envisioned was an urban utopia of sorts, which meant an urban dystopia, from the standpoint of the usual developer mega-projects. Individual improvisation and consciousness of community were the reigning ideals. The site development was intended to be an experimental living zone outside the legally regulated norms, a vast collective enterprise created by its inhabitants.


Sorkin's writings were the genesis of his landmark manifesto on an urban dwellers' bill of rights, “Local Code: Constitution of a City at 42 Degrees North Latitude.” Young's stalwart advocacy of housing rights in New York, leavened by a wicked sense of ironic humor, and an outlook deeply antithetical to hypocritical authority, contributed a mass of relevant images. My own urban studies up to that time, embodied in the “Centricity” project, were pushed into new directions that reached their apotheosis in the “Berlin Free-Zone” project of three years later. In short, “Timesquare” was an important step in each of our ways of thinking. However—perhaps because of a certain discomfort we felt with the visual outcome—the project was published, in part, only once. This post presents just a selection of the thirty-eight plates, but enough to expose the project's underlying ideas, as well as its flaws. Regardless, it seems like an appropriate moment to inject it—warts and all—into present urban discussions.


LW




![tsq46-blog61](media/tsq46-blog61.jpg)


![tsq22-blog1](media/tsq22-blog1.jpg)


![tsq47-blog6](media/tsq47-blog6.jpg)


![tsq1-blog101](media/tsq1-blog101.jpg)


![tsq1-blog10a](media/tsq1-blog10a.jpg)


![tsq7-blog12](media/tsq7-blog12.jpg)


![tsq6-blog11](media/tsq6-blog11.jpg)


![tsq32-blog3](media/tsq32-blog3.jpg)


![tsq28-blog2](media/tsq28-blog2.jpg)


![tsq42-blog5](media/tsq42-blog5.jpg)


![tsq43-blog13](media/tsq43-blog13.jpg)


![tsq51-blog81](media/tsq51-blog81.jpg)


![tsq48-blog11](media/tsq48-blog11.jpg)


![tsq38-blog4](media/tsq38-blog4.jpg)


 


More on Michael Sorkin:


<http://en.wikipedia.org/wiki/Michael_Sorkin>


<http://www.amazon.com/Local-Code-Constitution-degrees-Latitude/dp/1878271792>


More on John Young and his partner, Deborah Natsios:


<http://www.cryptome.org/>



<http://www.jya.com>


<http://www.cartome.org/nss/Natsios-NSS.htm>



 


 




 ## Comments 
1. [Adam Smith](http://www.thisishatch.com)
12.8.08 / 12pm


Dear, Mr Woods,


Thank you so much for putting time into this blog. I've followed it from the start and it keeps getting better.


What I've appreciated the most about it is that it's given me the opportunity to revisit your work, but in a seemingly more temporally relevant way. 


Maybe it was just my immaturity, but I don't think I previously realized how much your work is rooted in pragmatism.


Thanks again,


Adam Smith
3. Alex Bowles
12.9.08 / 9am


I second Adam's remarks. I've been quietly reading everything that posts here since you returned from your hiatus. 


I haven't had much occasion to comment. Just listening really, and constantly amazed at how many dimly considered, vaguely felt, and loosely defined ideas of my own I see reflected here with clarity, depth and precision. 


So thank you. 


Also, something about this collection of images in particular prompted me to revisit my copy of *The New City*. I was struck by what you wrote one page in particular (54), where that resonance between that project and this one seemed especially strong. 



> At the City's edge, energy is released from its bondage, flowing into the sky and its imminent, necessary storms. 
> 
> 
> Outlines of things are the visible limits of energy.
> 
> 
> Fabric of inner space, matrix of interweaving forces, unseen views of moments not repeated.
> 
>
5. [Local Code « Strange Quotidian](http://strangequotidian.com/2011/12/09/local-code/)
12.9.11 / 5pm


[…] an urban condition ideal for its citizens. It has been described by Lebbus Woods as a ” landmark manifesto on an urban dwellers' bill of rights“. That part of the book is what is striking about this writing (please see image […]
