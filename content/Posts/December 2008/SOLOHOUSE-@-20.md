
---
title: SOLOHOUSE @ 20
date: 2008-12-01 00:00:00
---

# SOLOHOUSE @ 20


[![sh4](media/sh4.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/sh4.jpg)[![sh2](media/sh2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/sh2.jpg)[![sh3](media/sh3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/sh3.jpg)[![sh11](media/sh11.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/sh11.jpg)




The Solohouse is twenty years old. At my age, twenty years does not seem so long, except when I think that it is the length of time between 1940 and 1960, the period when I was growing up, and I can remember with amazement all the changes, to myself and the world, that took place over that span of years. And then I also remember how much has changed since 1988, the year of this project's birth. Considering that, I think the Solohouse has aged reasonably well. Of course, it no longer exists, except in photos and a few drawings, having been destroyed some years ago.


The Solohouse, I should explain, is as close as I have come (so far) to building a structure that could be called, in the usual sense, a building. It was built of steel and wood, about one-sixteenth full size. At five feet tall, it was not big enough to enter or physically inhabit, but nevertheless addressed the basic construction problems of an actual building—such as the selection and joining of materials, and constructional stability—in a highly tectonic sense. The architect and master builder Christopher Otterbine was my contractor and, as in all creative building projects, my design collaborator.


The concept of the Solohouse was, ostensibly, that it was a house for one person. But, even more so, I conceived it as an ‘atom' of architecture, one that embodied the essential properties of architecture that were fundamental to building up ‘molecules' and ‘compound substances,' like building groups, even towns and cities. The single dwelling is the fundamental space of human habitation, and there must be a clear idea in an architect's thought about it, before he or she can talk about larger human settlements. The Solohouse was the embodiment of mine. Later, in projects for Berlin, Paris, Sarajevo, Havana and San Francisco, the Solohouse mutated and combined many times, responding to different cultural, political, and geographical situations, but its underlying principles remained.


In a previous post—INTEGRITY—I spell out basic ideas about materials, structure, and function in more or less orthodox Modernist terms, that guided the design of the Solohouse. And yet, it does not “look like” a Modernist building, and indeed it is not. The thin steel shell structure gains its strength though shape, not mass or an internal skeleton. The materials are allowed to age and have no imperative to remain eternally new. The function is ambiguity itself, the burdensome idea of ‘freespace'—one must invent the way to inhabit the house, because it is not pre-determined. That's the way principles work, assuming many variations, shapes and forms over time and its changes to the way we understand materials, structure, and function. Still, the human condition remains the same, which is what makes it ‘human,' joining us to the past, and also giving us not only inspiration, but a guide for creating the future.


LW





 ## Comments 
1. Sander Boer
12.1.08 / 9am


I am proud to be the first to comment and to go on record saying that I *Love* the Solo House.


I am happy that I was able to see it in person in Eindhoven when it was exhibited.  

For me personally it is an important piece.
3. ekkehard rehfeld
12.1.08 / 1pm


the solohouse, a masterpiece.


i mourn its physical loss; mind you, sandrine might still have her version of it.


to be a lighthouseattendant on a remote island.  

to make sense without almost anybody ever knowing of it. 


utson did.
5. Penelope
12.1.08 / 2pm


Dear Lebbeus,


One could only respect your choice in not resorting to the conventional building industry. However, it is very difficult not to tempt asking you or at least to hope that you will build at some point. It would be so challenging and thought provoking to have the opportunity to physically inhabit your spaces. 


Your projects could inspire scenarios for films or stories because of their ambiguity in terms of function (this has already happened actually!). They trigger the imagination for one to think of different actions that could take place in each space. Have you ever considered giving your students a project like the Sololhouse, and ask them to draw how they would inhabit it? In other words to add ‘bodies' in the spaces? 


The Solohouse has a very strong presence but at the same time it is vulnerable. It is vulnerable to time and this makes it even stronger because of this sensibility. Could this vulnerability to time (in terms of allowing the materials to age) be considered another interpretation of the notion of integrity?


Penelope
7. [jgrzinich](http://maaheli.ee/main/)
12.5.08 / 8pm


It was also just over 20 years ago that I entered architecture school. Not long after, you came to speak at our school (deep in the mid-west). The questions then seem similar to now, focusing on the practical issues of physicality and construction of such a structure (along with your other projects). But there is a paradoxical edge to this and your work in general. 


We must accept that from the moment architectural constructions are “completed” they ultimately face their inherent, almost linear, decay toward ruin by all the forces that be. As something built we would eventually have to contend with the notion of Solohouse as an aged structure of say, 100 years, and its subsequent “preservation”, at which point it would cease to be Solohouse.


Solohouse stands for something else, a dwelling in domains of the immaterial, inhabiting the turbulent non-linear world of human ideas and imagination. It is in this turbulence the structure survives and weathers the test of time. I know, because it is this and other conceptual projects of yours (how do you define them?) that remain an influence. There is a Solohouse replicated from indeterminate materials, grown out of the constrains of architectural school (and schools of thought), outward into seemingly unrelated fields of practice.
