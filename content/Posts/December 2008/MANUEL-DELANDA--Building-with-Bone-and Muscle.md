
---
title: MANUEL DELANDA  Building with Bone and Muscle
date: 2008-12-03 00:00:00
---

# MANUEL DELANDA: Building with Bone and Muscle


*Over the next few months, I will be posting a series of articles by Manuel DeLanda, the influential philosopher. Created for the design magazine Domus, under the rubric Matter Matters, they are lucidly written and very relevant to creative thinking in architecture. They are posted here with his generous permission.*









**Building with Bone and Muscle**


The field of biomimetics, a subfield of the science of materials, examines biological creatures in an attempt to derive design principles that may be utilized in an industrial context. The goal is not to simply replicate a specific material that is already used in nature. The silk produced by spiders as a structural foundation for their webs, for example, is a very desirable material with a tensile strength approximating that of Kevlar, the stuff used in bulletproof vests. Spiders, being predators, cannot be domesticated like silkworms, so their secret has to be literally extracted from them: the proteins that in long chains make a strand of silk are identified; the genes that code for the proteins are isolated and then inserted into a domesticated animal (a goat, in this case); and finally, the silk is recovered from the milk of the goat as a thick paste and extruded through small orifices to yield separate strands. As it happens, something like this is already taking place in some industrial settings. But it is not biomimetics, since no abstract principles are being learned that may be applied to other materials. On the other hand, if we studied vertebrates as load-bearing structures to learn from their unique combination of bone (bearing loads in compression) and muscle (bearing them in tension), in order to apply this combination in architectural structures using entirely different materials, this would constitute a valid example of biomimetics.


Bones are interesting for a variety of reasons. Like fiberglass and other composites, they are a hybrid of glue and grit, with collagen playing the role that epoxy resin plays in human-made composites, and tiny mineral crystals playing the role of the thin glass fibers. To the extent that humans have been using composites since they first used straw brick, the principle of combining glue and grit to get novel emergent properties was already known to us. But bones have other secrets to yield, particularly when considered in combination with cartilage, a material that also contains collagen and that may be converted to bone. The biomimetic principle here, combine two materials one of which may be transformed into the other, is simple but powerful. In biology it implies that the skeleton of embryos may be laid out with more flexible cartilage before it becomes rigidified into bone, with the exception of the external ear, the tip of the nose, and the ends of the ribs. Moreover, given that the bones of a new born infant are already articulated, the problem arises as to how to preserve these functioning joints while the bones grow in length several times their original size. Adding fresh cells and moving outwards at the edges, a solution used by other tissues, would destroy the articulations. The answer is to increase the length from within: the cartilage serving as interface between bones becomes the locus of growth, and it is later replaced by bone, thereby preserving the functionality of the articulations. Cartilage also helps bone be self-healing: after a fracture, a blood clot forms that seals bleeding vessels and holds together the broken ends; cartilage then replaces the clot, and it is, in turn, replaced by bone. Finally, the spongy nature of cartilage allows it to store an organic lubricating substance which is then squeezed out of it when one bone in the joint bears on the other. This allows bones to be self-lubricating.


*Muscles are an even more interesting material. They are basically, ropes made out of jelly, but in which the rope is designed like a collapsible telescope, that is, in which layers of muscle can slide under each other (like the successive tubes of decreasing radius in a telescope) allowing the muscle to contract in size. The ability to contract, in turn, means that muscles can pull on external objects, in other words, that they can not only passively bear loads but they can actively exert their own loads. Since the bodies of animals must be able to push as well as to pull, many muscles in the body are used in pairs oriented in opposite directions. To say that muscles can exert loads is, in effect, to say that they are engines or motors, a typical human body having about six hundred of these engines. Material scientists have recently developed rudimentary forms of artificial muscle. There are, for example, “smart gels”, consisting of a liquid solvent mixed with solid chains of polymers that tangle with one another making the composite viscoelastic. A variety of stimuli (heat, electricity, chemicals) can be applied to these gels to make them contract or stretch. These smart gels are already replacing certain moving mechanical parts, such as valves.*


From the point of view of learning from vertebrate technology, the principle here is that the variety of mobile load-bearing structures may be increased by linking a passive composite (bone) with an active one. This yields structures that are dynamic, capable, for example, of switching from one gait to another (from walking, to trotting, to galloping, in the case of quadrupeds) each gait involving different sets of muscles, the transitions from one gait to another occurring at specific critical points in speed. But just as in the case of bone a key role was played by the interface material in joints (cartilage) the interface between bone and muscle itself is also crucial. If soft muscles were linked directly with hard bone, the combination would be clumsy and imprecise. Muscles have to control bones at a distance, via a material of intermediate stiffness: tendon. The muscles operating our hands, for example, are located up our arms, connected to our hand bones through long and thin tendon cords. Thus, the final biomimetic lesson for structural engineering is that special materials must be used for the interface between passive and active materials. Apparently, the human body and its vertebrate kin are full of lessons for today's builders of load-bearing structures.


**Manuel DeLanda**


More on Manuel DeLanda and his work:


<http://en.wikipedia.org/wiki/Manuel_de_Landa>  





 


 


 


 


 




 ## Comments 
1. [jgrzinich](http://maaheli.ee/main/)
12.5.08 / 7pm


Thank you kindly Lebbeus. Manuel DeLanda's writing continues to inspire me over the years, particularly, [Deleuze and the Open-ended Becoming of the World](http://www.diss.sense.uni-konstanz.de/virtualitaet/delanda.htm "Manuel DeLanda").
3. ekkehard rehfeld
12.6.08 / 10pm


a truely inspiring thinker, manuel. and one of the very few who knows time. never did i meet anyone who threw LW off ballance. manuel did. chapeau to you, LW, for celebrating him.
5. [Celebrity Brainmatch « ubiwar . conflict in n dimensions](http://ubiwar.com/2008/12/06/celebrity-brainmatch/)
8.28.09 / 1pm


[…] guys in a room together. Lebbeus' first offering in the series is DeLanda's essay, Building with Bone and Muscle, a primer to the qualities of various forms of matter. I'm not going to quote from it because […]
7. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] 2007: M. DeLanda, ‘Building with Bone and Muscle'in Domus, No 884, September 2005, pp. 208-09 <https://lebbeuswoods.wordpress.com/2008/12/03/manuel-delanda-matters-1/> M. DeLanda, ‘One Dimension Lower'in Domus, No 886, November 2005, pp. 136-37 […]
