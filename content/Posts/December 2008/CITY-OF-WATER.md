
---
title: CITY OF WATER
date: 2008-12-31 00:00:00
---

# CITY OF WATER



I stayed for some time in the forests bordering an inland sea, resting and considering the unusual places I had visited and recorded in my drawings. Determined to understand better the underlying ideas that had informed the different constructions, I began to puzzle together a theory of sorts. First of all, I decided that the two ‘cities,' as I called them, were not built by two different communities of people, but rather by the same community, living with the same philosophy under two differing sets of conditions. It struck me that ‘earth' and ‘fire' are different manifestations of the same vast pool of matter and energy comprising the planet, and two stages of a cycle of their necessary exchanges and transmutations.


The community, I reasoned, had as its motive force an active and continual engagement with some larger natural cycle. Its people were ‘nature worshipers,' of a peculiar kind, considering the types of activities and structures they built. In my own community, now far away, we too had become sensitive to nature, which we referred to as ‘the environment,' or the ‘ecosphere,' but the activities we chose as participation with it were benign by comparison. We considered the idea of mining, of cutting into the earth, a violation of nature, something violent, exploitative, and wrong. Similarly, the idea of capturing the earth's latent heat and retaining it in strangely fragmented or fluid forms seemed an aggressive and almost pointless act of hubris. We preferred ‘passive' modes of energy exchange, and gently left issues of architectural form to techniques of achieving what we called ‘sustainability,' the maintenace of a perpetual equilibrium between human and natural forces. I decided that this community, in which I was a stranger, was a relatively primitive one compared with my own. What I encountered next confirmed, but then confounded, this estimation.


Near the shorelines I restlessly walked, I had come across large, almost architectural-scaled fragments amongst groves of trees, their geometrical forms overgrown and seemingly abandoned. Stranger than that, they seemed never to have belonged where I found them, but rather to have been brought there and left. This puzzled me, until I noticed one day, offshore, in the sea, some large structures. Thick plumes of white steam rose from some into the sky. Hurriedly taking a small boat, and making sure I had paper and pencils, I set out across the water to explore.


The artificial islands on which the massive constructions stood were actually the upper parts of underwater structures I entered only after some time and with difficulty. Once inside, in immense chambers surpassing in grandeur anything I had seen before, the detritus of a civilization had been accumulated. The pieces scattered near the shore were mere bits and parts compared to whole buildings and other monumental shards that I saw casually thrown together here. The great assemblages comprised a chaotic tectonic landscape, inhabited by workers and others busy arranging—almost composing—the more manageable elements, dismantling them into precise, if convoluted, arrays of debris. All this activity was carried on with a sense of urgency. It was not long before the water came.


Falling in cataracts that inundated the complex landscape, the waters from the inland sea had been let in to transform the exotic terrain, not so much by force, I thought, as by erosion of the kind that water patiently produces on inert matter. Following the cataracts, I discovered older tectonic landscapes in progressive stages of erosion and decay. Some had been fused over time by the water into unexpected formations: great reefs of iron, glass, wood, and other materials formerly shaped by human craft and ingenuity for highly specific purposes, now melded into one with no apparent purpose whatsoever. It was then I remembered that water is the universal solvent, and that everything dissolves in water, eventually—everything returns to the sea. This, I decided, was a community devoted to the return to the earth of minerals taken from it—the completion of an epic cycle of human and natural struggle, interaction, and evolution. I called this place, accordingly, the City of Water.


 Thinking of my own community and its disdain for polluting rivers, lakes, and seas, I felt that my sense of superiority over this culture and its practices was confirmed. But then I wondered, where were the plastics and toxic chemicals we used to make them; where was the industrial waste from the manufacture of countless consumer products; where, indeed, were the discarded consumer products, their discarded wrappers and containers; where was the garbage? Was I witnessing only a ritual of waste disposal? Where was waste of the kind that polluted our waters? Was it dealt with here in a different way? Did it exist at all? I was left with many questions, and little sense of certainty.


 *To be continued.*


 LW



![fc24](media/fc24.jpg)


![fc23](media/fc23.jpg)


![fc22](media/fc22.jpg)


![fc16](media/fc16.jpg)


![fc171](media/fc171.jpg)


![fc31](media/fc31.jpg)


![fc19](media/fc19.jpg)


![fc18](media/fc18.jpg)


![fc18a](media/fc18a.jpg)


![fc34b1](media/fc34b1.jpg)


![fc34a](media/fc34a.jpg)


![fc34](media/fc34.jpg)



 ## Comments 
1. Marc K
12.31.08 / 5pm


The City of Water, in my opinion, speaks of the idea of determinate temporality: what is will no longer exist, and that this “return to the sea” becomes inevitable. One must cope with the truth, being that one day all of the greatest works of architecture we cherish will no longer exist. This, however, is not necessarily a product of ourselves, but the processes we commit to and the excrement of manufacturing only help to increase the speed of this deterioration.


In addition, scientists have predicted the death of our sun; and thus the death of humankind. But one must then think of the human life cycle, and understand that he himself will only live for so long. It becomes then our duty to construct and to make, knowing that we must effect and show what we have found to as many people as we can. For naught, all will be lost and humanity would perish not knowing that which we have to offer.
3. peridotlogism
1.13.09 / 5pm


“Near the shorelines I restlessly walked, I had come across large, almost architectural-scaled fragments amongst ”


what do you mean by large, almost architectural-scaled?
