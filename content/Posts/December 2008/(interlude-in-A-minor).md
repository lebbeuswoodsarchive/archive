
---
title: (interlude in A-minor)
date: 2008-12-10 00:00:00
---

# (interlude in A-minor)



![lwblog-interlude](media/lwblog-interlude.jpg)


(notes from a hermitage)


 


Alone and not,


in the space shared


 by disallowed,


 forgotten


voices.


 


Lucid lines or lies


predict a just decay


of time. Marks wander


freely on presences'


dazzled terrain.


 


One, two, six, eight


four. The signals rest.


The eye of God numbers


 all steps to Orthogany.


Get, get it straight.


 


Rooms without


boundaries so frail


as significant waking,


residues of originality


forgiven, ignored.


 


Sentiment fades;


Rationalized actions


and affectations fade;


Memories fade last


in vagrant noise.


 


How long it has been!


Paths unnumbered,


confused or free,


steps counted,


unerringly.


 


A boy or a girl,


or a cat, or a bird,


or the swift lizard,


fooling the mole–


 find a home.


 


Steel is cold


because it drains


the body's heat,


better, say,


than air.


 


On the bed of steel,


my body must get hotter,


racing against the death


of the Universe. So all


depends now on dreams.


 


Rain at a zenith softens


more than confuses edges,


making space palpably bare,


limiting some probabilities


as assurance expires.


 


Architecture, I heard, is near.


The third voice said so, and I,


distracted by memories of you,


pretended to hear, imagine,


and begin to believe.


 


Is that, I asked,


the promised storm?


Or, more indifferent rain


dulling the open sky,


our frail horizons–


 


Is that, I cried,


an equation of one?


with no solution,


unbalanced,


effete.


 


Yes, and no.


Yes, no,


are halves


of a struggle


 resolving nothing.


 


Geometry endangers,


Abstract thought is a threat.


Color my desires with sense


and sentiment and limits


to what can be seen.


 


Hours limit, unless


others intrude, obsessed,


demanding and secure,


like monks who scold,


extending all of time.


 


Above your street,


emptiness of movement


confirms or conforms,


enforcing the datum


commonly known.


 


My blood passes


across and through,


flooding the chamber


wherein passions


promise more.


 


 


![lwblog-interlude2](media/lwblog-interlude2.jpg)


LW




 ## Comments 
1. Bill M
12.10.08 / 6am


a stirring read during the late night / early morning. it would go well with some minimal organ muffled behind doors & sounds of weather.  

I ran across [this](http://www.bartlett.ucl.ac.uk/otherhostedsites/avatar/intro.html) today. it's difficult to find texts that embrace architecture in full awareness, but minifesto comes close to many ideas I appreciate from your writings. it won me over early with the mention of ‘pataphysical clinamen, but I would love to hear your reaction to their attempts to understand architecture through this notion of “plectics”.
3. Marc K
12.11.08 / 1am


I wanted to question the existence of “Abstract thought is a threat” and “Architecture is near” in the poem. It seems odd, being that Mr. woods is a man of abstractivity, and being that he has stated in the past that Architecture is the most abstract of the arts, does the threat of the abstract mean the threat of Architecture?


Clearly, this work explores the idea of a person that exists within specific material and spatial conditions, and it seems that the nature of this poem is abstract and representational in itself. To communicate that “Architecture is near” seems to communicate to me that the struggle this person finds him or herself in is one of distinct narrative experience, and that the idea of a coming event- and that there exists some architecture or device that responds specifically to this event and this event only.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
12.11.08 / 3am


Marc K: I like your interpretations. This is apparently a hermit who wants to be alone in public—-already a paradoxical condition.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
12.11.08 / 3am


Further: abstract thought, embodied in architecture's mathematical ‘coolness' is not very comforting–hence the desire for sense and sentiment which—because they are limiting—can comfort, even though they also bring pain.
9. Marc K
12.11.08 / 5am


This pain, does it relate to the narrative of the person? To me it becomes an emotion invested within the spaces that this man exists. Is this pain existential? I feel as if the idea of this man within a position of pain is somehow a vision into the spaces he finds himself. That the conditions of these spces become his own conditions. 


I feel that “My blood passes across and through, flooding the chamber wherein passions promise more” speaks to an experiential force that embodies this room. However, this pain that you speak of also fits in with this idea of a datum: “Above your street, emptiness of movement confirms or conforms, enforcing the datum commonly known.” I think that the force that exists in this space causes pain, and its absence is a sign of relief.


Hypothetically, how would an architecture be constructed that not only embodies this paradox, but also talks about these present spatial conditions?
