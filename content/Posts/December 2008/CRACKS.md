
---
title: CRACKS
date: 2008-12-30 00:00:00
---

# CRACKS



I live in Lower Manhattan, one of the spookiest of places. This is so not just because it was settled by the Dutch—-from whose language the word ‘spook' is derived (it means ‘spirit')—and not only because its Medieval-scaled street plan is lined with skyscrapers, creating New York's only actual ‘canyons;' and not only because in winter, when the sun is low on the horizon, the street spaces are dark even on the sunniest days. No, the spookiness comes from something else. It is found in the spaces that exist in the most crowded and densely built of places, but which people enter only reluctantly.


It is well known that Manhattan is a city without many alleys. Cities like Chicago planned for service ways by which horse-drawn carts, then trucks, could deliver goods to businesses and houses and pick up refuse, without clogging the streets, which were deemed to be for people and their vehicles. In New York, as the saying goes, “Everything goes through the front door.” People and garbage alike. Whether this is because of an extreme sense of democracy (people and garbage being, in this wise-cracking city, more or less the same); or because the 1811 Commissioners' Plan, which established the Manhattan grid, never included them; or because the value of land made them financially impractical, is difficult to say. Alleys almost never existed. However, it is true that Madison Avenue and Lexingrton Avenue began as rather grand alleys, providing access to the stables and carriage houses that served the mansions once lining Fifth and Park Avenues, but they have long since become lively shopping streets. Anyway, they were Uptown, and always active, and never spooky in the sense that concerns me here. Downtown, in the oldest part of New York, there are a few accidental alleys, but they are like cracks and fissures in the tectonic geology of the city, speaking more of discomfort than any affirmative  human presence. 


In the 80s and early 90s such urban spaces were a hot topic in architecture. It was believed—or hoped, by some—that they would provide the ground for a radical kind of urbanism, starting with the small-scale and spreading to the large. There was much talk of ‘parasites' and ‘viruses' that would begin or breed in such spaces, then spread out to ‘infect' the more polite, stable, controlled, and socially stratified spaces of the city, transforming them from within into something more complexly human. And who were to be the instigators of such hopeful changes? Well, it was to be the socially dispossessed, the politically unempowered, the homeless underclass of urban squatters led by the existentially homeless artists, intellectuals, and architects who fervently believed that society needed changing. The mass revolutions of the 60s had failed, having run up against formidable police states. The ‘flower power' of the 70s had wilted, finally, in its own sun. All that was left was the ‘small revolution' of cracks and crevices, of slipping unseen into the big system, growing quietly ‘until it was too late.' Alas, that failed, too. The real estate boom of the 80s and the neo-liberalism of the 90s simply sapped whatever energy that movement ever had. Eventually, everyone got on board. Everyone got comfortable. Those who did not became invisible.


The  cracks and crevices are still in Lower Manhattan, and they are more empty, more spooky, than ever. And did I mention that where they do not exist, we should create them, at least in our thoughts? After all, where else can those viruses of transformation secretly grow?


LW


![lwblog-5a](media/lwblog-5a.jpg) 


![lwblog-crk-6a](media/lwblog-crk-6a.jpg)


![lwblog-crk-2a](media/lwblog-crk-2a.jpg)


![crk-7](media/crk-7.jpg)


 


 




 ## Comments 
1. [slothglut](http://fairdkun.multiply.com)
12.30.08 / 7am


LW,


i've only been in manhattan once, but just for comparison to jakarta/denpasar/phnom penh, the places i've been living in, i have some questions about this ‘cracks.'


do they have decaying [the popular term must be neglected] atmosphere to them? or is there a vitality imbued inside them instead?


do architects still pay close attention to them? or as you stated, we have failed these spaces to meet their full potential, because of being comfortable?


great photos by the way.


tQ,
3. peridotlogism
1.13.09 / 5pm


Interestingly enough, these Lower Manhattan spook-spaces crop up on the UWS and in the boros as well. They are belligerent and neglected children-specters on the UWS and tend to be stealthier in the boros, as though they are observing and would prefer to acquiesce and be ignored.
5. [Trennlinien « Stein auf Stein](http://walkingstones.wordpress.com/2012/08/31/trennlinien/)
8.31.12 / 8am


[…]  City Cracks […]
