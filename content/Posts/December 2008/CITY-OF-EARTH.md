
---
title: CITY OF EARTH
date: 2008-12-20 00:00:00
---

# CITY OF EARTH



Some many years ago, I found myself—by accident or coincidence—in a strange country, of which I knew nothing of its customs, culture, or language. As I was free to travel around for a while and was very taken with the rugged landscape and especially the unusual (yet uncannily familiar, in some aspects) buildings and their groupings, I decided to record in drawings what I saw.


As an architect, therefore one who believes that much can be learned about a people from what they build, I wanted to get a deeper understanding, from the architecture, of what sort of community I found myself in. The drawings I made are therefore to be seen as documentary, providing as much detail as I could render, but requiring a high degree of interpretation as to what uses the buildings were put to, and what larger meanings they had within the community's way of life.


The particularly mountainous region I first encountered was certainly inhospitable to building. Jagged, rocky ridges were separated by steep chasms rather than welcoming valleys. Yet many large structures had been built. Clinging to the cliff faces, or cut into them, or set into almost inaccessible promontories, the structures were built largely of concrete with much the same color as the rocks. Adorning them were more delicate armatures of iron that were rusty, in much the same manner as industrial buildings I knew well from my own country. From these aspects I reasoned that this was a mining community, one in which the people were primarily engaged in cutting and digging into the earth to extract its hidden mineral riches. As I was to learn, this reasoning was only partially correct.


Of all the various structures and their activities I saw and drew—apartment buildings, temple-like structures, in which groups gathered to perform quietly mysterious rituals, various factories and technical facilities, which seemed like astronomical observatories, but focused inward, not outward, as well as other not easily typified buildings—I found none devoted to commerce, such as markets, warehouses, or, most especially, to shipping, whereby merchants and manufacturers would pack off—at a profit—their ores, raw materials, or manufactured products to distant locations. It became clear, but only after a considerable time, that this was a community organized around the activity of mining, of cutting and digging into the earth, *for its own sake*. The goal of its efforts was not to take something from the earth in order to sell it to others, but rather to establish a kind of reciprocity with the earth, a dialogue between human ingenuity and inventiveness and natural geological process, which were co-equally creative. What these people took from the earth they used only to build their buildings and apparatus, which, when these decayed, fell back to the earth, and into it, engaging in a natural cycle that embraced the human and the natural. The ultimate purpose of their activity was *the activity itself*.


This was difficult for me to understand. In my own, very different, culture, activities—even the playful ones—had to have a purpose beyond the mere doing: to make something useful, to make a profit, to be fun or be entertaining, to win, or to at least learn something from losing. My insight into this strange culture gave me pause for thought.


Without any knowledge of what the people called themselves, or their built community, I named this place the City of Earth.


As I was leaving the region—in certain ways gratefully, but in others with regret—I heard of other communities, not too distant, that I determined to visit.


 *To be continued.*


*LW*



![fc0](media/fc0.jpg)


![fc13](media/fc13.jpg)


![fc2](media/fc2.jpg)


![fc3](media/fc3.jpg)


![fc4](media/fc4.jpg)


![fc32](media/fc32.jpg)


 


![fc5](media/fc5.jpg)


![fc6](media/fc6.jpg)


![fc9](media/fc9.jpg)


![fc10](media/fc10.jpg)


![fc121](media/fc121.jpg)


![fc111](media/fc111.jpg)



 ## Comments 
1. [Miller Taylor](http://www.flickr.com/photos/millertaylor/)
12.21.08 / 4pm


Excellent entry in both prose and imagery. I await the next stop. It reminds me very favorably of Italo Calvino's Invisible Cities, a text that has influenced me architecturally for years.
3. [mary](http://www.startingfromscratch-mary.blogspot.com)
12.21.08 / 5pm


this project perfectly hyperbolizes what I think is Kenneth Frampton's most compelling idea…the essential connection between the earth(work) and the tectonic…The more ambiguous the relationship the more inevitable the architecture…A friend of mine asked him why this mattered at all (because at the time, being inexperienced grad students, we thought this focus on the ground seemed rather arbitrary)…and in front of several hundred straight-laced midwesterners his face turned an angry red…he took a deep breath and said, “so the building won't be just another goddamn object in the lanscape”…The earth is undifferentiated being and the tectonic is individuation, naming, discerning etc…The earth is the irrational…and without it a project has no unconscious…
5. [lebbeuswoods](http://www.lebbeuswoods.net)
12.21.08 / 11pm


Miller Taylor: The Calvino reference is apt, but don't forget Borges of many years earlier, particularly “The Immortal” and “Tlon, Uqbar, and Orbis Tertius.” I think my tone is also related to that of Edgar Allan Poe, whom Calvino and Borges had certainly read.
7. [slothglut](http://fairdkun.multiply.com)
12.22.08 / 1pm


LW,


with all due respect, my question to you:


why don't you pursue film-making/script-writing/comic-creating more [all of those are my dream jobs]? or you are doing it, it's just me that's not informed enough?


very very nice post, by the way.


tQ,
9. [lebbeuswoods](http://www.lebbeuswoods.net)
12.23.08 / 1am


slothglut: Movies? Comics? In all due respect to those genre, they never occurred to me.
11. [lebbeuswoods](http://www.lebbeuswoods.net)
12.23.08 / 1am


mary: I love your story about Frampton, and I love his passion for architecture, too, even though I'm not so sure he would welcome this work as anywhere near his thought. I, however, believe it is.
13. [rb.log» Blog Archive » Lebbeus Woods' fantasy world](http://www.richardbanks.com/?p=1624)
1.6.09 / 5pm


[…] on his blog, tied to his beautiful sketches. The first is about a city built into rocky crags, the City of Earth. His prose style reminds me of H.P. Lovecraft, for some reason, creating a slight atmosphere of […]
15. Del. Irium
1.17.09 / 12pm


I am also reminded of pynchon writing of the underworld in Against the Day
17. [Buamai » CITY OF EARTH « LEBBEUS WOODS](http://www.buamai.com/image/4806)
2.10.09 / 10am


[…] city, dystopia, lebbeus woods, pyramid, utopia Posted by will patera on February 10th, 2009 Original Source Post Comment Click here to cancel […]
19. [Buamai » CITY OF EARTH « LEBBEUS WOODS](http://www.buamai.com/image/4809)
2.10.09 / 10am


[…] sphere, sphereology, structure, Technology, truss Posted by will patera on February 10th, 2009 Original Source Post Comment Click here to cancel […]
21. [As Above « The Zero Of Form](http://thezeroofform.wordpress.com/2009/02/15/as-above/)
2.15.09 / 4am


[…] Any way, these literal Land Marks need to be preserved, we need to be reminded of the devastation of our values. The damage already done, we should reclaim the space. And conceptually, these spaces allow for activities previously unrealizable. It became clear, but only after a considerable time, that this was a community organized around the … […]
