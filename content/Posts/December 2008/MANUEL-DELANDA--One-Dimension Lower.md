
---
title: MANUEL DELANDA  One Dimension Lower
date: 2008-12-23 00:00:00
---

# MANUEL DELANDA: One Dimension Lower



*Another in the continuing series of articles on materiality by philosopher Manuel DeLanda.*


**ONE DIMENSION LOWER**


Some of the most interesting material phenomena occur at critical points of intensity. The familiar sequence of states of matter, Gas-Liquid-Solid, illustrates in its own humble way the spontaneous micro-architectural changes that matter undergoes at these critical points. These sequences of so-called “phase transitions” exemplify matter's capacities for autonomous change, capacities that may lay dormant as long as a critical point is not reached. But why should these phase transitions take place at a point? The answer is that they do not always do. Water will change from liquid to solid at a speciﬁc point in temperature (at exactly zero degrees centigrade) as long as we keep other parameters (such as pressure or volume) constant. If we change pressure as well as temperature, the critical values at which this phase transition takes place will now form a line, and if we in addition change the volume of water, these critical values will form a surface. 


One way of describing the situation is to say that the spontaneous changes that animate matter from within always take place one dimension lower than the space formed by the parameters triggering the changes. The variable “N” is traditionally used in mathematics to designate the number of dimensions of a given space. In these terms we can say that critical phenomena always take place at N-1 dimensions. Although in the case of phase transitions the N-1 rule refers to the dimensions of the abstract mathematical space formed by the values of some parameter, similar ideas apply to physical space. Since our familiar spatial surroundings are three dimensional, the rule would make us expect that exotic material behavior should occur in two dimensions, that is, in all physical surfaces, and this is indeed the case. That all physical surfaces are special places is clear from the fact that the physics and chemistry of the monomolecular layers binding a piece of matter are different that those that govern the bulk material contained within those exterior layers. 


The reason for this difference derives from the special situation in which molecules at the surface ﬁnd themselves. Those in the interior of a piece of matter are surrounded on all sides by similar molecules, while those at the surface are only bound to similar molecules on one side, the other side being exposed to another material, such as air. In this sense the surface of any material object is the site at which one type of matter interfaces with another, and therefore the place where interfacial phenomena, such as rusting and corrosion, take place. Surfaces also tend to possess a certain amount of free energy (referred to as “surface tension”) that is absent from the material in bulk. The same asymmetry that makes surfaces into interfaces is at work here: the forces acting on the molecules by other molecules inside a piece of material tend to cancel each other out; those same forces, however, affect the molecules at the surface in some directions but not in others, so that unlike those inside, surface molecules are not at equilibrium. In liquids this free energy manifests itself by forcing droplets to adopt a quasi-spherical form, the form that will minimize surface energy.The surfaces of solid objects also possess this free energy but the bonds among their component molecules are too strong for the energy at the surface to bend them into a droplet. Nevertheless, the free energy is there sometimes in quantities surpassing that of liquid surfaces. 


For builders of load-bearing structures the free energy of solid surfaces is important because the cracks or fractures whose uncontrolled propagation can endanger the structural integrity of a building are in fact nothing but surface. Cracks are prototypical N-1 entities. To say that the surface of a liquid has free energy means that work has to be performed, or outside energy spent, to move molecules from the interior of the liquid to the surface. To put this differently, work must be performed to enlarge the surface. Similarly for solids. The expansion of a crack in a structure under tension, for example, demands the creation of fresh surface, indeed of two new surfaces since a fracture always has two sides. The minimum amount of energy needed to cause a tensile fracture in a given structure is therefore twice the surface energy of the material of which the structure is made. Where does the energy needed for crack propagation come from?


Any structure carrying a load is for that very reason a reservoir of energy, of strain energy to be exact. When structures bearing loads in tension have a small crack the probability that it will propagate will depend on an energy budget: as a crack grows it releases some of the strain energy stored in the structure, this being the “credit” side of its budget, while at the same time it will have to spend energy creating fresh surface, this being the “debit” side of the budget. There is, for any given material, a critical crack length below which the credit side is lower than the debit side so the fracture does not grow. But if the critical length is exceeded, the credit side will now show an energy surplus that will allow the crack to propagate spontaneously, sometimes explosively. Interestingly, the critical length is independent of scale: in a structure made of mild steel, for example, bearing a load of 11,000 pounds per square inch, the critical length is about six feet, regardless of the size of a structure. This means that for relatively small objects (swords, machine parts) there is not enough space for the critical length to be reached; small cracks will form but they will never get to the point where they can wildly propagate. Mild steel is, therefore, a safe material for their construction. But in larger load-bearing structures there is plenty of space for accidental fractures to reach the critical length and, more importantly, many deliberate discontinuities (doors, hatchways, panels) will from the start be larger than the critical size. These discontinuities, from the point of view of fracture dynamics, are nothing but incipient cracks. Thus, it is not only necessary to go one dimension lower (from volumes to surfaces) to understand what cracks are, we need to go one even lower (to critical lengths) to understand their behavior. Either way, it would seem, it pays to think to the N-1. 


**Manuel DeLanda**




 ## Comments 
1. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] M. DeLanda, ‘One Dimension Lower'in Domus, No 886, November 2005, pp. 136-37 <https://lebbeuswoods.wordpress.com/2008/12/23/manuel-delanda-matters-3/> M. DeLanda, ‘The Importance of Imperfections'in Domus, No 888, January 2006, pp. 136-37 M. […]
