
---
title: MANUEL DELANDA  Events Producing Events
date: 2008-12-13 00:00:00
---

# MANUEL DELANDA: Events Producing Events



*Another in the continuing series of articles on materiality by philosopher Manuel DeLanda.*


**EVENTS PRODUCING EVENTS**


In the material world there is hardly a more important relation than that between causes and their effects. Nevertheless, our understanding of just what causes and effects are is strangely primitive. Part of the blame for this state of affairs should go to philosophers, at least to those who have traditionally confused an analysis of how the link between causes and effects is perceived by us humans with an analysis of what the causal link itself is. The philosopher David Hume, for example, argued long ago that all that our senses reveal about causality is that there is a constant conjunction between an event acting as a cause, such as a collision between billiard balls, and an event acting as an effect, a change in the state of motion of the balls. Many of his followers took this to mean that objective causality can indeed be reduced to the subjective perception of this constant conjunction.


The problem is that our senses can reveal only the existence of an invariable coincidence between two events, but not of any process through which one event may be regularly produced by another. Two successive events may always be experiencedinassociation,butnonecessarygeneticlinkbetween the two is ever directly sensed, and it is precisely this genetic link, this production of one event by another event, that constitutes the objectivity of causality. There is, of course, no question that in order to study some causal relations we must use the constant conjunction or regular succession of events as evidence for the possible existence of a causal link. But to reduce the objective production of one event by another to their observed invariable correlation is to confuse causation with the tests used to establish its existence. 


Another problem with the traditional view is that while constant conjunction may sometimes be evidence of causality, this criterion works only for linear causes, the simplest and most mechanistic form of causality. The formula for linear causality is “Same cause, same effect, always.” The impoverished view of causal productivity that linearity implies has, in turn, made a perfect target for critics who argue that such a simple relation cannot account for the richness of pattern we observe in the material world. We can imagine two different ways in which this formula may be enriched, one by breaking with the “same” part, the other by challenging the “always” part. The ﬁrst departure yields nonlinear causality, at least when the word “same” is taken to refer to the intensity of the cause. Let me give an example relevant to builders of load-bearing structures, that is, an example in which the event “changing the amount of weight supported by a structure” is the cause, while the event “becoming deformed” is the resulting effect. 


Some materials, such as the mild steel so often used in modern buildings, do behave linearly under loads, a fact that was already established in the seventeenth century by Robert Hooke. These materials will stretch or contract by a given amount which is always proportional to the load. Hooke's law may be presented in graphic form as a plot of load versus deformation, a plot that has the form of a straight line (explaining one source of the term “linear”). On the other hand, many organic materials display a J-shaped curve when load is plotted against deformation. A slight tug of one's lip, for example, produces considerable extension, but once the straight part of the “J” is reached any additional pull results in relatively little additional extension, although it may indeed cause quite a bit of pain. This exempliﬁes a nonlinear relation between the intensity of the cause and the intensity of the effect: a cause of low intensity produces a relatively high intensity effect up to a point (the inﬂection point of the “J”) after which increasing the intensity of the cause produces only a low intensity effect. Other materials, such as rubber, display a S-shaped curve, which translates into a more complex relation between intensities. If we imagine trying to deform an automobile's tires, pulling very hard produces at ﬁrst almost no effect at all. As the intensity increases, however, a threshold is reached at which the rubber-made structure does stretch but only up to a second point (the second arc of the “S” shape) at which it again stops responding to the load. Neither one of these two examples exhibits a constant conjunction relative to intensity. 


The second challenge to linearity comes from the fact that causes seldom act in isolation, but are usually accompanied by other causes that may interfere with them. In some cases this interference may lead the cancellation of the effect that is supposed to “always” occur. But even if the interference is not so drastic, it may still reduce the probability that the effect will be produced to less than a 100%. In other words, causality becomes statistical, as when one says that “Smoking causes cancer in 70% of smokers”. In statistical causality one event simply increases the probability that another event may be produced. Much as in the case of cancer it is events internal to a person, such as metabolic events related to inherited predispositions, that interfere with the primary cause, so in the case of materials under load, it is events related to their internal microstructure that make their deforming behavior less than a 100% predictable. Dissolved gases, nonmetallic intrusions, variations of chemical composition, even details of their process history (such as the shaping process), make materials not isotropic, that is, not likely to exhibit identical properties in all directions. In these conditions observing a constant conjunction of cause and effect is not possible. Manufacturers of materials, for example, must deal with questions of quality control in a statistical way, testing entire populations of samples of a material to arrive at the average probability that a given deformation event will take place. Designers of load bearing structures must, in turn, take into account not only the magnitude of the load but its direction, since the material may respond in different ways depending on orientation. 


Nonlinear and statistical causality bring back to the philosophical conception of the causal link some of the complexity that was taken away by the notion of constant conjunction. Additional complexity may come from an analysis of catalysis, and extreme form of non linear causality in which an external cause produces an event that acts merely as a trigger for an entire sequence of further events. But while restoring the richness of causal relations does make them more likely to explain complex material behavior, it is a restoration of their objectivity that will have more profound philosophical consequences: material events producing other material events in ever more intricate series, whether there are humans around to observe them or not.


**Manuel DeLanda** 




 ## Comments 
1. [slothglut](http://fairdkun.multiply.com)
12.22.08 / 1pm


the last paragraph is my favorite of your delanda series, just because it shows just how philosophy failed themselves: by putting human beings in the central of discourse too much.


as the saying goes, the world doesn't revolve around us.


or does it?
3. [lebbeuswoods](http://www.lebbeuswoods.net)
12.22.08 / 3pm


slothglut: Revolve around us? DeLanda has his view, while Heinz von Foerster has quite another, if you look at his essay, “On Constructing a Reality,” attached to my post ZEROES AND ONES. From my viewpoint, both are correct, in that they offer ideas we can interpret as aspects of a larger picture of the world embracing both. In other words, Copernicus was right, from a scientific standpoint, but wrong from a psychological one. The ‘truth' depends on your angle of view.
5. [Recommended links | A Synthetic Architecture](http://amcgoey.net/general/recommended-links/)
1.2.09 / 7pm


[…] MANUEL DELANDA: Matter(s) 2 EVENTS PRODUCING EVENTS […]
7. [Cluster | City - Design - Innovation » Material(ism) for Architects: a Conversation with Manuel DeLanda](http://www.cluster.eu/2010/10/08/materialism-for-architects-a-conversation-with-manuel-delanda/)
10.8.10 / 8am


[…] pp. 136-37 M. DeLanda, ‘Events Producing Events'in Domus, No 889, February 2006, pp. 100-01 <https://lebbeuswoods.wordpress.com/2008/12/13/manuel-delanda-matters-2/> M. DeLanda, ‘Evolvable Materials'in Domus, No 890, March 2006, pp. 164-65 M. DeLanda, […]
