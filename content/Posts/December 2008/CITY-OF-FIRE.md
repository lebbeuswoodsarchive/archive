
---
title: CITY OF FIRE
date: 2008-12-24 00:00:00
---

# CITY OF FIRE



As I descended into one of the steep crevasses in the rocky terrain, I felt an updraft of warm air. Knowing this way down led to some of the mines, I remembered stories of how workers in the deepest mines are subjected to heat that rises upward from the earth's inner core of still-molten iron and nickel. At even three thousand feet below the earth's surface, miners suffer almost unbearable temperatures of a hundred and thirty degrees and more. I recalled as fact that geo-thermal wells, drilled downward from the surface to tap the heat within the body of the planet, provide virtually limitless sources of energy for various mechanical systems operating above. So my experience of the warm winds from below, in a darkness far from the sun, came not entirely as a surprise.


What did surprise, though, was what I found as I descended. Here, in strata upon strata of  volcanic rock, were spaces inhabited by a community of people engaged in some sort of industry. There were dwelling spaces, hollowed out from the dark earth masses. There were machines that captured an eerie form of light different from that far above. There were vast caverns in which I saw entangled, monumental forms that were like fragments of the lucid geometric volumes I had seen in the city above, but here broken into disparate parts and linked together by tendril-like passageways and conduits, creating a vast, indeterminate network, rather than a geometrically coherent form. All of this was revealed by an inner-earth glow, amid a constant, throbbing heat. It soon became clear that the harnessing of heat energy, as a source of motive power, I imagined, was the industry of this underground community.


To some extent, the existential philosophy of the community far above, which exalted as an end in itself its interaction with the physical body of the earth, was evident in the constructions I found, though in markedly different form. The architecture here was fragmented and tenuously connected. Lower down, though, in a region where the heat's maximum of human tolerance was reached, the rocks and buildings (if they can be called such) seemed to flow together. The resulting forms were, as on the airy ridges above, a fusion of the human and the natural, but here so much so that one could not be distinguished from the other—indeed, they were one, captured flows of energy, celebrated, I now believe, for their own sake.


I called this place the City of Fire.


How I managed to ascend from this fearsome, strangely exciting domain I cannot say. Perhaps it was like waking from a dream. Or, more likely, I was taken upward by some of the inhabitants, who saw my extreme discomfort and confusion and took it upon themselves to rescue me. In either case, or both, I left without understanding the ultimate purpose of engaging the heat and light, the quietly violent energy of the underworld. To drive the machines of the upper world? To, more simply, power their own machines and light their own dwellings? Or, another possibility exists. I have since thought of the story told about the bull-dancers of ancient Crete. In their sacred rituals, the point was neither to conquer the dangerous bull, nor to spiritually merge with it. Rather, they danced with the bull as a form of artful play. It was all, and only, a very serious game.


*To be continued.*


LW



![fc12a1](media/fc12a1.jpg)


![fc14a](media/fc14a.jpg)


![fc27](media/fc27.jpg)


![fc15](media/fc15.jpg)


![fc26](media/fc26.jpg)


![fc26a](media/fc26a.jpg)


![fc26b](media/fc26b.jpg)


![fc281](media/fc281.jpg)


![fc29](media/fc29.jpg)


![fc212](media/fc212.jpg)


![fc201](media/fc201.jpg)



 ## Comments 
1. [Paul M. Rodriguez](http://ruricolist.blogspot.com)
12.27.08 / 12am


These illustrations are nearly Piranesian. (If I recall correctly, Piranesi's engravings inspired the author of *Vathek* with his own architectural underworld).


After the comments to the previous entry in this series, surely it must annoy you to hear comparisons multiplied, but I must say that these remind me of nothing so strongly as H. P. Lovecraft's Dreamlands stories, which also mingled the scale of Dunsany with the menace of Poe.
3. Marc K
12.29.08 / 6am


As one may delve into such spaces of phenomenological discourse and the ravaging of the mind, and to see what you have seen and to felt what you have felt becomes a part of the dichotomy of the darkness: the architecture of the beneath. To simplify such an experience into words may reduce its value; for such spaces withheld in the darkness and the heat seem to provoke from one an almost unrelenting, indescribable desire. But having delved; the spatiality of the surface bears not to the possibilities of the internal. And so through a monumental blast from within are we to understand these concepts of the interior. Beneath the heat and the sweat may we find ourselves closer to the answer, yet farther away from the truth.
5. [lebbeuswoods](http://www.lebbeuswoods.net)
12.30.08 / 1am


Marc K: I am very taken with your use of language to express the ‘ineffable.' “…the ravaging of the mind” stands out as an idea about the terrible and the wonderful that gets close to what I believe architecture should enable us to experience. Of course, we also need architecture to mediate our experience with that of others—total strangers—we share the world with. But we have to be stripped down, reduced to the essentials, ‘ravaged,' if you will, to experience and communicate our experience, to even a single ‘other,'
7. Marc K
12.30.08 / 7am


Thank you. I also believe that this mediation becomes about the always important question of representation…  

It always seems that we as creators of spaces have to “reduce” ourselves in order to extrapolate our concepts to others. But once being a member of said “other” world, one must realize that there is a certain weight to compelling work that either subliminally or subconsciously communicates exactly what one intends to his audience. People, “total strangers,” become in touch with the work and the experience, but may not always find the words to describe it.
