
---
title: THREE OLD MEN
date: 2009-05-21 00:00:00
---

# THREE OLD MEN


That is no country for old men. The young


In one another's arms, birds in the trees


—Those dying generations—at their song,


The salmon-falls, the mackerel-crowded seas,


Fish, flesh, or fowl, commend all summer long


Whatever is begotten, born, and dies.


Caught in that sensual music all neglect


Monuments of unageing intellect.


 


An aged man is but a paltry thing,


A tattered coat upon a stick, unless


Soul clap its hands and sing, and louder sing


For every tatter in its mortal dress,


Nor is there singing school but studying


Monuments of its own magnificence;


And therefore I have sailed the seas and come


To the holy city of Byzantium.


 


            from *Sailing to Byzantium*, William Butler Yeats


 


![LWblog-OldMan1b](media/LWblog-OldMan1b.jpg)


Albert Einstein


74 years old.


Eyes: compassionate, alert.


Hair: Uncut, uncombed.


Dressed: old sweatshirt, pen clipped on collar—indifferently.


Wrinkles: softly etched, gently modeled.


Countenance: grandfatherly, benign, accepting, vaguely sad.


Work at time of photograph: continuing thirty-year quest for a theory that would unify all the physical forces—electromagnetism, gravity, strong and weak nuclear forces—but which  he never succeeded in formulating.


Earlier works: theories of Relativity and particle-wave duality that transformed physics and the modern world (solid-state physics, Atomic Bomb).


 


![Beckett1a](media/Beckett1a.jpg)


Samuel Beckett


77 years old.


Eyes: critical, intense.


Hair: Badly cut, combed.


Dressed: turtle-neck sweater, Bohemian—self-consciously.


Wrinkles: deeply cut, sharply modeled.


Countenance: fatherly, defiant, rather dangerous.


Work at time of photograph: increasingly austere plays for stage and television that sought for essentials in human situations.


Earlier works: plays such as “Waiting for Godot,” “Krapp's Last Tape,” and “Eh, Joe?” that challenged prevailing conventions of drama, gained wide fame but remain on the artistic edge.


 


![Wright1B](media/Wright1B.jpg)


Frank Lloyd Wright


86 years old


Eyes: patient, dreaming.


Hair: Well-cut and combed.


Dressed: White collar and tie, jacket—elegantly.


Wrinkles: relatively few, soft and deep.


Countenance: like a rich uncle, relaxed and aloof.


Work at time of photograph: Guggenheim Museum, Price Tower.


Earlier works: Prairie Houses and Unity Temple—which greatly influenced the founders of European Modernism—Fallingwater, Usonian Houses, and many others, as well as books and articles on architecture that have had wide influence.


 


LW



 ## Comments 
1. river
5.22.09 / 8am


Those three, in great particular, wave the [semaphore](http://www.winterbourne.freeuk.com/semaphore.html) of [silence](http://www.youtube.com/watch?v=zgvuzQHm3zc). That is at least, what I think they have in [common](http://www.youtube.com/watch?v=gvfgLBMmtVs). But take my thought with a grain of salt. I grew up here with second-hand, and pre-recorded, newly broadcast stories about the nature of my native, personal silence. I suspect there were translation errors regarding the minute details of turtle shells, salamander behavior, and sea conchs. Especially in school.
3. Marc K
5.24.09 / 4am


I found this past semester, in my schooling, that there began to exist some cultural generational differences between my studio professor and myself. It seems that there still exists the ‘old guard' in architecture schools. Those professors and professionals who learned during a time where the vast majority learned to detail sections and the few among them wrote and drew books on theory.


The studio became an interesting clash of minds… and I sensed that there existed a shifting focus between him and myself, where he was concerned with the pragmatism of architecture, rather than its poetic meaning. Is this generational shift one of theoretical exploration? Or do the young and brash just seek to play on their computers and contemplate mappings/diagrams and other formalist exercises…


Regardless of this, it seems that the wise ‘old' men among us cannot keep up with how fast the world is changing.


I can not help but think that these wise men once stated similar claims about their teachers, and that our own students will one day say the same about us.
5. river
5.24.09 / 9am


River remembers his high school geometry teacher best of all. For that creature had hair all over his body. It erupted from under his neckties in front and back. We used to jokingly call him the ‘missing link'. It was as black as pitch. That teacher did not accept anything but excellence. I had failed several of his class assignments previously. Then he took me into the hall for a private conversation. I have excelled in proving physically based theories ever since! That was a long time ago.


I am now strangely interested in mud-sand-straw circle dwellings, after having kids, against the Zeitgeist of all developed marketing, at my sore loss of funding, ever since!


Fancy that.


psst. Google “zeitgeist.” [ssh](http://en.wikipedia.org/wiki/Secure_Shell). Be lithe, or He'll hear you!
7. Marc K
5.24.09 / 5pm


What did he say to you?
9. river
5.26.09 / 5am


“I expect more from you.”
11. river
5.29.09 / 6am


In the interests of full disclosure, he was actually my “Trigonometry” teacher/ I was merely abstracting absentmindedly again.
