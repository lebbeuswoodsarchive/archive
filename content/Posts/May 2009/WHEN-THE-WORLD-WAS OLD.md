
---
title: WHEN THE WORLD WAS OLD
date: 2009-05-26 00:00:00
---

# WHEN THE WORLD WAS OLD


When the world was old, all things became possible. Opposites remained opposed, yet found a unity in being so. Sameness was no longer the same, but different at each turn. Time remained infinite, but closed in finite cycles with distinct variations. There was reconciliation without compromise or surrender. Each thing remained true to itself, but joined with many others. Conflict became a form of harmony. Accordingly, the human and the natural embraced one another, almost by accident. It happened one day on the way to autonomy, when people finally saw and understood their reflections in ‘the mirror.' That which can be controlled by them became reconciled with that which cannot.


 Alas, the world was still young, too. When the world was young, nothing seemed possible, because possibility did not yet exist. There was only the immediate exchange between need and needed, between what was felt and hungered for and what could be grasped close at hand. Possibility came into being only when need could be delayed, when one could be hungry and wait to get the food one wanted to eat. Possibility is the acceptance of delay, acceptance of gratification's postponement, for a reason. When the world was young, there was no reason, as it was not needed, as long as gratification was easy. Reasoning became a need only when gratification became difficult, and only after the resulting wave of panic. Reason is the result of panic, in that it is the only way to keep panic from spinning out of control and consuming entirely our vital energy. Reason enables us to delay desire, to wait, to dream, and to plan.


![LWblog-City1](media/LWblog-City1.jpg)


![LWblog-City2](media/LWblog-City2.jpg)


![LWblog-City3](media/LWblog-City3.jpg)


![LWblog-City4](media/LWblog-City4.jpg)


LW



 ## Comments 
1. [bojana v.](http://www.bojanavuksanovic.blogspot.com)
5.28.09 / 12pm


really beautiful peace of writing/thoughts….
3. arun sawant.
5.28.09 / 3pm


Really a master piece, everyday is just an illusion……
5. [pedrogadanho](http://shrapnelcontemporary.wordpress.com)
6.27.09 / 2pm


It is because of pieces like this or “Doom Time” or so many others texts in which a long-term philosophical thought does emerge, that I would like to invite you for a special challenge regarding the forthcoming volume of Beyond, Short Stories on the Post-Contemporary…  

I expect I can develop on this via email…
