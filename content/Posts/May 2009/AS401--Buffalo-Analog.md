
---
title: AS401  Buffalo Analog
date: 2009-05-15 00:00:00
---

# AS401: Buffalo Analog














**ANALOGICAL COLLABORATIVE DESIGN STUDIO, State University of New York at Buffalo, January 14–April 30, 2004**






**PARTICIPANTS**


Timothy Beebe, Jose Chang, Keith Johnson, Brandon Lauricella, Tuan Luong, Zhiwei Liao, Peter Mendola, Derek Morgan, Caterina Onorati, Alexandria Rabuffo, Chris Romano. Lebbeus Woods, Dennis Maher, faculty. Brian Carter, Dean, School of Architecture. Kent Kleinman, Chair of the Graduate School of Architecture.


**PROGRAM**


*Upper Level Studio. ARCH 608. Professor Lebbeus Woods. SPACE OF CONFLICT.*


**Buffalo Sites**


 We're going to spend the first week looking for a site in Buffalo, which will be the focus of our analysis and the starting point for our making of an analogical architectural construction, which we will complete by the end of the semester.


 Working individually, or in small groups (if you prefer), I am asking you to identify several sites in the city which are marked by some sort of conflict. The conflict can be social, architectural, functional, or of some other type that you consider significant. Remember that this site will be the starting point for work we will do all semester, so it—and its conflictual nature—must be rich and various enough to allow in-depth study and multiple interpretations. It may be an outdoor or indoor space and will probably be public in some way.


 Be prepared to present the chosen sites next Wednesday, 21 January, 2 p.m., in the studio. Your presentations should include photos, drawings, videos or other visual material that you find necessary to describe the site and the nature of the conflict that marks the site. It should also include a written statement describing the conflict and saying why you consider it significant.


 After the presentations, we will discuss the presented sites, and choose the one that we will be working with as a starting point of our work.


*Upper Level Studio. ARCH 608. Professor Lebbeus Woods. SPACE OF CONFLICT.*


**Buffalo Analog**


After our reviews yesterday of spaces of conflict that each of you has identified and given spatial form to, we are going to shift our focus. It seems important now to give some boundaries and some rules to our common work, so that our individual research can find some common ground.


 What we are going to do next is create a common territory for our individual ideas about spaces of conflict to engage (and conflict?) with each other. We are going to create an analog urban space, that is, one that—like actual urban space—contains complex interactions of people and their ideas. These interactions may involve the confrontation of differences in ideas, the contention of ideas for space that is limited by urban boundaries, and/or their simultaneity and overlap in the same spaces.


 Our goal is to create an analog of urban processes and formations, a parallel with the actual city, the better to inform us about the city itself.


 We are the members of a community—our studio. Our differences (of every kind) that take the form of our individual interests and concerns and most particularly our projects-in-progress set the stage and give substance to our community. From these manifest differences and also our common desire to create a coherent expression of our common belief in the significance of architecture in human affairs, we will build a kind of model city, an ideal city of architectural ideas.


 Toward that goal, I am asking that you:


 1)    Construct an urban analog space that is a cube, eight feet on each side;


2)    Negotiate with each other the space within that cube you need to construct a model of your project;


3)    Construct a model of your project within the cube.


*L. W. 19 February 04*



 


The analog at the end of April:


![Final2](media/Final2.jpg)


Design and construction process:


![lwb-const1](media/lwb-const1.jpg)


![lwb-const2](media/lwb-const2.jpg)


![lwb-const7](media/lwb-const7.jpg)


![lwb-const14](media/lwb-const14.jpg)


![Const15](media/Const15.jpg)


![lwb-const9](media/lwb-const9.jpg)


![lwb-const11](media/lwb-const11.jpg)


![lwb-const101](media/lwb-const101.jpg)


Study model: negotiating the spaces of individual projects within the community space:


![Model1](media/Model1.jpg)


Installing the individual projects:


![lwb-const13](media/lwb-const13.jpg)


![final11](media/final11.jpg)


![final13](media/final13.jpg)


![final17](media/final17.jpg)


![final19](media/final19.jpg)


![final15](media/final15.jpg)


![final20](media/final20.jpg)


![final18](media/final18.jpg)


![Final1](media/Final1.jpg)


![final10](media/final10.jpg)


Not shown in this synopsis are 1) the many developmental drawings and models for both the community space and the individual projects; and 2) the programs for the individual projects that inspired their particular forms. Taken altogether they comprise a book. Still, it is hoped that, like a city, we can infer the most important ideas from a close reading of the relationships, and the differences, in the analog construction.


**RETROSPECTIVE TEXT**


The idea of an analog is to be like something else in some ways but not in others. If the something else is part of a city, therefore too complex for any definitive form of analysis, the analog can make manifest and analyzable some essential characteristics, while leaving others, less important, aside. The analog is not an abstraction, though it uses abstraction as a tool. It is not a reduction or a simplification, for it remains complex in its own terms. Rather it is a shift in the angle of viewing and understanding a situation or complex set of conditions, one that gives the opportunity to see the familiar in new ways. This is extremely important when the familiar is, like a part of a city, overburdened with historical interpretations that inhibit the creation of new ones. By creating a parallel reality, the analog circumvents this historical over-determination and  liberates the imagination in ways that can impact the primary reality under consideration. In today's world of rapid changes, where history is less and less reliable as a guide to the future, intellectual freedom and inventiveness of the type enabled by the analog are increasingly important.


It is true that architecture, as a practice and a form of production, is bounded by precise practical considerations—technical, economic, legal, cultural—that restrict imagination and invention. But as culture, technology, law, and socio-economic factors themselves undergo change, the boundaries of architecture require adjustment or even redefinition that cannot be devised by the simple extrapolation of old ones. This is where analogical thinking and the analog-—as a model of constructed reality—-become useful. This is especially the case in schools of architecture, where the next generation of practitioners, theorists, and teachers is being educated. The aim of their education must be less in terms of understanding existing typologies and techniques and more in the forging of new ways of approaching old and new problems alike, that is, in ways of thinking about what architecture is, how it works, and what it may yet become.


The Upper Level Design Studio focused on spaces marked by conflict, beginning with particular sites in the city of Buffalo. Spatial manifestations of conflict were given the highest priority among the spectrum of urban conditions and became the subject of our analogical analysis and projects. The idea was that conflict signals some sort of change that is trying to occur in the spatial structure of a particular site. This change requires an alteration of the existing spatial order and how it is used, and therefore calls for new spatial designs and constructions. Architecture, according to this thesis, arises as a creative engagement with conflict. Without conflict, there is no need for change, no need for architecture. The exception to this is when architecture must be introduced to create conflict where none exists, fomenting change in a social or spatial situation.


After the initial phase of identifying and analyzing spaces of conflict in existing urban situations, the work of the studio took a decisive turn. Rather than hypothesize about actual conditions in Buffalo, it was decided that we would treat the studio itself as a community, an analog of the wider community of Buffalo. Where it is the architect's usual practice to grasp the needs and motivations of groups and individuals from fragmentary and indirect sources, we would be able to work directly with people for whom the designs were intended–ourselves. We were, after all, a community in every sense of the word, having common interests, values, goals, problems, and at the same time diverse, even conflicting, ideas, perceptions, ways of working toward our common ends. Cannot we say that in one sense this was an ideal community, in that we would be able to work directly and together, without the need for intermediaries like developers, clients, institutional representatives? Would not the ideal be that every community could design its own spaces, according to its uniqueness and demands? Politically, we might say this is the anarchist view, an extreme form of democracy, in which liberty and choice are ordered not by imposed laws and ideology-based rules but rather by dialogue and critique within a community of individuals who understand from the beginning that their fate as individuals is tied up with the fate of the community. Enlightened self-interest is thus the operating frame of political and social reference.


Once the analyses of different urban conflicts were complete and codified in visual terms (drawings and models), it was decided that we would build a construction, a spatial and conceptual analog of a community—ours—in continuous self-definition and formation. It was decided that this construction would be within an eight-foot cube.


The apparent arbitrariness of this decision is important to consider. As an analog, the spatial frame of reference of our work could have been any number of other configurations: a pyramid, a sphere, an irregular volume. Or, it could have been in a volume more related to the origins of our work, the city of Buffalo. The latter option was rejected because it would be more representational than analogical, and it would be hard to escape from all the literal implications of that representation. Having to cope with the cube had implications that were more philosophical, typological, conventional. The cube is the building block of the spatial reference system that is dominant in our society and culture. It is also the visible evidence that the complexity of human experience can be made coherent and practicable. The cube gives us coordinates in space, but also in time. Any form, any eccentricity, any accident or unpredictable occurrence can be contained within its system of coordinates and made intelligible, logical. So, somewhat less than purely arbitrary, it was chosen as a boundary, a limit, a structural frame, but also as a convention and critique. Our ideal community, we determined, would not abandon the known and accepted, but embrace and expand it.


The dimensions of the cube were determined by the available space in the studio. As such, it had no inherent scale. By tilting the cube up on one corner, shifting it off the ordinary Cartesian reference system of the room, the building, the campus, the city, its three-dimensionality was fully liberated. But this created a problem of how to support the cube, to transfer its weight back to the Cartesian structure that would carry it to the ground. This required a compromise of the cube's autonomy that was the subject of heated debate. In what was probably the only consensus-driven decision of the project, the cube, a welded steel frame, was supported on a steel bracket at the corner and a short wall, constructed of plywood, at two raised points. The project began it's move toward a tectonic hybridity that continued through its construction.


It was then decided that the space of the cube would be divided according to prevailing social practice as a series of volumetric properties. Each property, staked-out by a member of the studio, was to embody a spatial construction related to the conflict originally identified in the analyses of urban spaces in Buffalo. Because these properties–irregular volumes estimated to best serve individual purposes–interpenetrated each other in the animated three-dimensional space of the cube, a set of new conflicts arose, the actual conflicts of our community, which had to be resolved through a long and difficult process of negotiation and design. Tempers flared. Factions formed, and reformed. Mock-ups were tried, rejected, modified, retried. There were moments when it seemed that the project could not go forward. The studio itself became the space of conflict, with the spaces of the cube at its conceptual center.


The designs for each of the properties within the cube were entirely determined by the individual architect occupying the spatial volume of a property. Adjacencies with other volumes and designs were matters of negotiation where abrasions demanded some resolution. Intersections of the volumes required active collaboration, as differing, even opposing, tectonic systems met or collided. Each design had its unique narrative, each abrasion and confrontation its story. The final stage is only a moment in a living process incidentally truncated by schedule. It could have continued. It should continue, in some form or other, until it reaches the horizon. What we see is not a utopia, but a heterotopia—the spatial form of a community of differences, of conflicts, of personally determined spaces that work together in a complexly cohesive and communal way. It could be an analog of the most desirable city of tomorrow, one whose form and purpose arise not from the imposition of pre-determined means and ends, but from a creative interplay of personal and communal means and ends.


**Thoughts on the methodology**


As the teacher in this design studio, I adopted the role of the senior member of the community. Having asked the initial questions, I became the the arbiter, the counselor within an effort that I perceived to be the independent and collective work of the individuals in the studio. I preferred not to call them students, and myself teacher, because these terms put the wrong hierarchical emphasis on the generation of a project that could not be taught, in any traditional sense, but only created by the members of the class. This resulted in consternation and frustration among some members of the studio/community who wanted my direction as to what to do, and in their complaints and appeals to the hierarchy of the school, which I understood and had to seriously address.


Certainly there are aspects of architecture that need to be ‘taught,' particularly at the beginning levels of architectural education. At the upper levels, when students have acquired skills of architectural drawing, model-making, design, and face the prospect of leaving school and working in the architectural profession, it is time to ‘teach' individual initiative, thinking, and action. Authority figures, such as teachers, must at this stage become mentors. Students not prepared for the degree of independence required by this work will not be prepared to practice architecture with independent conscience and sense of responsibility.


It is hoped that the design studio also imparts the idea that architecture is, and can be more, a collaborative art. Buildings often are but should not be conceived in a social and political vacuum. At the same time, the best architecture cannot be designed by committee or consensus. New ways of approaching individuality and collectivity need to be developed in design, if complex human living environments are not to be reduced to standards, or to accumulations of randomly eccentric architectural gestures. The Buffalo Analog was and remains an exploration of the aesthetical and ethical possibilities of a methodology of collaboration that synthesizes community from conflict, and an analogical urban architecture from strongly articulated, highly individual differences. 


LW



 ## Comments 
1. ysgs
5.16.09 / 1pm


Apart of the benefits of the students taking part in the design studio, getting to learn and understand in quite a unique way about the complexity of the environment and the role architecture plays within it, what I find mostly interesting in this line of work is the emergent results of such a design process.  

I think some of the main critiques towards contemporary city planning and urban design are regarding the basic fact that an architect or any single designer cannot fully comprehend (or some might even say perceive) the full complexity of a given situation in the city, especially not through empirical order or experiments, and are therefore (on a theoretical level) unable to design alone a proper solution for a certain problem within it, or to integrate their designs in it. This might be seen as the “catch 22” of the profession, but can also be treated as one of its most interesting aspects.  

The amazing thing about such collaborative design process is the unexpected outcome it has, as it to my opinion grows mainly from the interactions of the individuals with each other and not from the individual itself.  

If I'd take for example the “The Proto-Urban Condition” entry you've published last year regarding a similar design process, the design results were very interesting because they included a different level of complexity which is rare to see in urban planning designs.  

It can be that the loss of individual control over a certain design creates new priorities, hierarchies, organisation and even values systems within a given project, and might help it to loss some of its (maybe less wanted) aspects such as designing Style, individual recognition values etc.  

It can be interesting now to try and create a platform, which can be used to design projects using different architects from around the world working simultaneously on a given site, maybe trough the internet, or with a special computer program. The interesting part then would be the rules and regulations of the platform, which would reflect simultaneously our views of architecture as well as our basic human behavioural norms. Would you be able to ‘delete' someone else's work, to which extend can you react to it, transform it, confront it or support it.
3. [PEJA](http://www.piliaemmanuele.wordpress.com)
5.17.09 / 12pm


really interesting!
5. [David Burns](http://so-ad.com)
5.18.09 / 6am


Come to Sydney!
7. martin
5.18.09 / 8pm


catch-22 indeed. what seems to be promoted in this studio is “user-driven design,” in a certain sense. The studio is the community and they are building for themselves. Doesn't the logical extrapolation of this negate the need for architects? Perhaps not directly, but there seems to be some sort of contradiction in there somewhere.


also, this studio seems to be more about the process, and less about what is actually produced. The content itself is visually engaging, but its meaning seems to be mostly derived from the process of producing it, not the ideas producing it. This approach may be fine, assuming that those involved are relaying previously developed ideas about architecture. Isn't school about exploring ideas, refining ideas, and testing them? This seems to only be the testing stage. Is this intentional?


frankly, im surprised no one punctured the cube.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	5.19.09 / 1am
	
	
	Martin: In a world where people control their own worlds, the professional architect has an important role: creating the models, the hueristic guides that non-professionals can follow, Yes, you have gone to the radical heart of the matter. We (in architecture) can either cling to our traditional role of master artist, creating singular works that become the ‘jewels in the crown' of our society, or we can become leaders and teachers of the whole society that is more democratic, networked, open and, yes, risky. It is a choice we can, even need to, make.
	
	
	
	
	
		1. Amin
		11.16.10 / 5pm
		
		
		hey lebbueus woods 😀  
		
		how do you think of ideas like this????  
		
		and what peice of your model represents what????  
		
		btw loving your work and hope you make much more great things 🙂
		
		
		Amin
		3. [geraldine](http://homepage.mac.com/geraldineliquidano/)
		3.11.11 / 2pm
		
		
		what will you do if you are teaching interior design to first year studio students?We have chosen an 1912's neoclassical building, all gutted, notw we are doing loft projects in two upper floors? will you let them destroy the columns and floors for the sake of design, or will you preserve the whole shell and only intervene with the already given spaces?
9. [Zhiwei liao](http://www.flickr.com/photos/zliao/sets/72157606026344637/show/)
5.19.09 / 4am


Thanks, Lebbeus! I missed the days!  

-Zhiwei Liao
11. slothglut
5.24.09 / 4am


‘Students not prepared for the degree of independence required by this work will not be prepared to practice architecture with independent conscience and sense of responsibility.'


this statement alone brings out the conflicts architecture graduates have to face upon diving into the real world.  

who we should be responsible for? architecture is a very broad discipline, with numerous stakeholders with their own agenda. me myself, after 4 years graduating, still got caught in the middle of this dilemma:


who should i listen to?  

myself?  

the hand that feeds?  

the greater good? who is this greater anyway? if it's the community, which part of the community?  

the chief? the bottom of the pyramid?  

do they really know what they need, instead of what they think they want?  

the list could go on…


to address + deal with conflicts [be it analogical or not], it is a very trusting [albeit, in your own words, risky] mode of operation you're proposing here. i wish the ex-community members could bring out something interesting + virtuous to their pre-existing, or future communities they already or will be a part of. very interesting, to say the least.


tQ for sharing.
13. [AS401: LA Analog « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2009/12/07/as401-la-analog/)
12.7.09 / 2pm


[…] and conceptual basis the LA Analog is similar to other such studio projects—such as the Buffalo Analog. In form, however, and its design particulars, it is […]
