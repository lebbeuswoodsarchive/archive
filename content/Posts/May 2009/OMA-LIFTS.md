
---
title: OMA LIFTS
date: 2009-05-27 00:00:00
---

# OMA LIFTS


FROM THIS:


“In thinking about how to employ seismic forces in the making of architecture (and not simply trying to resist them), it occurred to me that the horizon determines how we perceive space. We always stand perpendicular to the horizon (a fact, it is said, that inspired Pythagoras to formulate his theory of the right-angled triangle). If, however, we were to rotate an asymmetrical space, without altering it otherwise, it would appear very different to us. A long space, rotated from a horizontal to a vertical position, would appear to us as a tall space, with corresponding aesthetical and psychological impact.


 The Horizon Houses are spatial structures that turn, or are turned, either continuously (the Wheel House) or from/to fixed positions (the Star and Block Houses). They are structures experimenting with our perception of spatial transformations, accomplished without any material changes to the structures themselves. In these projects, my concern was the question of space. The engineering questions of how to turn the houses could be answered by conventional mechanical means—cranes and the like—but these seem clumsy and inelegant. The mechanical solution may lie in the idea of self-propelling structures, using hydraulics. But of more immediate concern: how would the changing spaces impact the ways we might inhabit them? But that is another story.”


LW sketches of the concept from 1996:


![LWblog-OMALIFT](media/LWblog-OMALIFT.jpg)


![LWblog-OMALIFT2](media/LWblog-OMALIFT2.jpg)


 


*above project published in**Radical Reconstruction**, Lebbeus Woods, Princeton Architectural Press, 1996 and the* *Lebbeus Woods website, Work/Projects/Horizon Houses [(lebbeuswoods.net](http://www.lebbeuswoods.net)**).*


TO THIS:


 ![ONAROLLa](media/ONAROLLa.jpg)


 ![ONAROLLb](media/ONAROLLb.jpg)


*as published in The Architect's Newspaper, 05.20.2009.*


Ah, such is the fate of an idea in our Prada world.


LW



 ## Comments 
1. [saurabh](http://www.urbanfloop.blogspot.com)
5.28.09 / 9am


Commodification/appropriation of typologies to turn them into ‘a matter of taste' devoid of any architectural history or criticism is a prerequisite for having a practise that so eagerly engages with the Global capital. But maybe that's just the nature of capitalism and not necessarily the practise…
