
---
title: LOCUS OF MEMORY
date: 2009-05-11 00:00:00
---

# LOCUS OF MEMORY


The building is called the World Center. It covers the former World Trade Center site and rises, for the present, to twice the height of the former WTC Towers. It remains perpetually under construction, and its ultimate height is not yet known. It is the tallest building in the world, and will, as it grows, always be the tallest building in the world. It is a symbol of regeneration and continual change. It is a project with a precise beginning—September 11, 2001—but no ending. Like the city, it has a finite history, but a future that can be defined only in terms of potential.


The World Center contains vast amounts of leasable office space, though at any one time much of it is empty. There are equal amounts of public and private housing, as well as shopping malls, commercial facilities, sports and recreational facilities, automobile parking, as well as several interlocking systems of vertical mass transit, which connect with the new Grand Central Station Downtown, which connects the site horizontally with the city and the region.



**The main feature of the new structure is a vertical memorial park called The Ascent, which reconstitutes in various ways experiences issuing from 9/11.**


There are four ways to make The Ascent.


The Pilgrimage (one month) is for the devout, and consists of traversing a difficult vertical path through a series of Stations, ordered by a narrative of the events and aftermath of 9/11.


The Quest (one week) is for the ambitious, and consists of a series of climbs up near-vertical faces, ledges, resting places and camps, ordered by a narrative of the events and aftermath of 9/11.


The Trip (two or three days) is for the vacationer, with or without family, and consists of a series of platforms, lifts, escalators, interactive displays, hotels, restaurants, vistas, and educational entertainment ordered by the story of 9/11, the histories of New York City, the skyscraper, and metropolitan life.


 The Tour (half a day) is for day-tourists and consists of a rapid elevator ride to the summit of the park, with intermittent pauses at displays commemorating the events of 9/11 in the context of New York City history.


All the ways occupy the same spaces of The Ascent. They interact yet remain separate, even opposed.


At the top of The Ascent is The Summit, a community dedicated to reflection, study and contemplative action, all related, directly or indirectly, to the events of 9/11.


**The Summit is a community made up of transients—pilgrims, climbers, vacationers, tourists, as well as workers in the World Center—who have made The Ascent, and of permanent residents—scholars, students, philosophers, artists, and others who have devoted themselves to the study of, reflection upon and production of works concerned with the causes, events, and effects of 9/11, which are understood to be far-reaching and wide-ranging. It is a community that crowns the World Center with a continuously evolving network of interior and exterior spaces and serves as a window into old, present and future worlds. At the same time, it is a community that brings together diverse social classes—a new egalitarian realm rising above the competitive tumult of the city below, a place where contentions can be informed by new perspectives and possibilities.**


 LW


![lwblog-w-c82](media/lwblog-w-c82.jpg)


Building section, showing part of The Ascent:


![lwblog-w-c3](media/lwblog-w-c3.jpg)


Perspective views of The Ascent:


![lwblog-w-c6](media/lwblog-w-c6.jpg)


![lwblog-w-c7](media/lwblog-w-c7.jpg)


![LWblog-W-C2](media/LWblog-W-C2.jpg)


Perspective view of The Summit:


![lwblog-w-c5](media/lwblog-w-c5.jpg)


Museum at The Summit:


![lwblog-w-c4](media/lwblog-w-c4.jpg)


Hudson River perspective, looking east:


![lwblog-w-c11](media/lwblog-w-c11.jpg)



 ## Comments 
1. martin
5.11.09 / 4pm


bravo. out of curiosity, what do you use as a base for your drawings? is it some type of ink wash on paper?


also, i was wondering if you could give any support to the claim that the white on black drawing technique started with El Lissitzky and other constructivists working on competitions with blackboards and chalk.
3. Pedro Esteban
5.12.09 / 1am


Without words, as ussually.  

Philosophicaly how do you think this building will be seeing for the world?
5. [Encapsulations. » Blog Archive » He's back!](http://so-and.org/encapsulations/blog/?p=103)
5.13.09 / 5am


[…] newest post, “Locus of Memory” references a project that I have mentioned to several of you: Woods' proposal for a new […]
7. [Memorialising 9/11: World Trade Center II « ubiwar . conflict in n dimensions](http://ubiwar.com/2009/05/13/memorialising-911-world-trade-center-ii/)
5.13.09 / 6am


[…] Trade Center II Posted in ubiwar by Tim Stevens on 13 May 2009 Lebbeus Woods unveils his plans for a new World Trade Center, a perpetually under-construction edifice rising above Ground […]
9. river
5.14.09 / 8am


Doh! [Vista](http://www.flickr.com/photos/32655769@N06/3530681970/sizes/o/)
11. ben
5.22.09 / 5am


so good!!! way better than any of the other proposed memorial buildings….
13. arun sawant.
5.28.09 / 3pm


Get ready for a nomadic trip….back to the roots…..
15. matei23
6.8.09 / 4am


excellent, especially considering the current state of the WTC site.  

i would add one more type of ascent:  

the addition, of indeterminate length, where the climber constructs his own path, adding to the building as he climbs, eventauly reaching the construction crew at the top, which he joins





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	6.8.09 / 6pm
	
	
	matel23: lovely idea! This is indeed for the Adventurous.
17. katerina
7.12.09 / 4pm


dear lebbeus,what do you use to make them?(what kind of ink or pencil).these are all analog?or a combinationof analog and autocad?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	7.12.09 / 4pm
	
	
	katerina: they are all analog. My exact technique remains my own little ‘trade secret,' but I assure you the materials are all very common. With some experimentation, anyone can figure it out and maybe improve on it—better than simply being told the formula!
19. Tee
8.11.09 / 6am


You're few of my favorite when it comes to drawings. How did you create the red lines? Did you use color pencil? How did you create to get bright yellow lines? I tried before but it turns into greenish yellow. White on black is always amazing!  

Black charcoal paper (Black Paper) Do you have any preferences?  

Color pencils  

Pastel  

What else did you use?  

Thanks LB.
21. [Screen Machine » The moralising of Doctor Parnassus:](http://www.screenmachine.tv/2009/11/24/the-moralising-of-doctor-parnassus/)
11.24.09 / 7am


[…] designing the reconstruction of the World Trade Centre site, envisioned an enormous structure under perpetual construction which would grow continually to remain the world's largest building? This guy is crazy […]
23. maude orban
12.13.10 / 3am


Does it mean that the summit constantly grows?
25. [Project 01: Portfolio Template « blawg](http://backinarch352.wordpress.com/2011/01/07/01-portfolio-template/)
2.11.11 / 11am


[…] The example project is Lebbeus Woods's project for Ground Zero. […]
