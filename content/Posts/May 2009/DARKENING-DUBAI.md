
---
title: DARKENING DUBAI
date: 2009-05-31 00:00:00
---

# DARKENING DUBAI


# Architects named in human rights row


28 May, 2009 | By Christopher Sell


[as reported in [The Architects Journal](http://www.architectsjournal.co.uk/news/daily-news/architects-named-in-human-rights-row/5202585.article) (UK) online.]


[![](https://i0.wp.com/www.architectsjournal.co.uk/pictures/595xAny/2/5/3/1203253_AJ28_AbuDhabi_Performing_Arts_Center.jpg)](http://www.architectsjournal.co.uk/attachments.aspx?js=yes&height=auto&width=572&storycode=5202585&attype=P&atcode=1203253)


**Abu Dhabi projects by Zaha Hadid and Frank Gehry accused of ‘exploitation' by human rights group**


Zaha Hadid, Frank Gehry and Jean Nouvel are among architects singled out by US-based organisation Human Rights Watch for the ‘abuse and severe exploitation' of construction labourers occurring on their projects at Abu Dhabi's luxury Saadiyat Island development.


In [an 80-page report](http://www.hrw.org/en/reports/2009/05/18/island-happiness-0) published last week, entitled *The Island of Happiness: Exploitation of Migrant Workers on Saadiyat Island, Abu Dhabi*, Human Rights Watch found that, despite slow improvements in timely payment of wages and labour conditions, abuses such as passport withholding and fines are still occurring.


Under government developer the Tourism Development and Investment Company (TDIC), Saadiyat Island (‘Happiness Island' in Arabic) is being developed into a £17 billion tourism and cultural centre, comprising a Nouvel-designed Louvre, a Guggenheim by Gehry, Foster + Partners' Sheikh Zayed National Museum and Hadid's Performing Arts Centre, all yet to be completed.


Human Rights Watch called on the architects, and institutions such as the Guggenheim and the Louvre, to obtain enforceable contractual guarantees that construction companies will protect workers' fundamental human rights.


‘These international institutions need to show that they will not tolerate or benefit from the gross exploitation of these migrant workers,' said Sarah Leah Whitson, Middle East and North Africa director at Human Rights Watch.


A source close to the Saadiyat Island project said: ‘Neither Gehry nor Hadid have shown concern for the issues. It is quite surprising if they didn't know what is going on in Abu Dhabi.'


Julian Carlson, a director at Pascall + Watson, which was recently appointed by Ateliers Jean Nouvel to work on the Louvre project, said: ‘We haven't reached the stage yet where we engage with the construction industry, but we are committed to supporting our client in achieving equitable working conditions.'


A statement from Zaha Hadid Architects said: ‘We take the Human Rights Watch report very seriously… As a partner in the Abu Dhabi Performing Arts Centre project, Zaha Hadid Architects is confident the implementation of policies included in the TDIC CSR [Corporate Social Responsibility] Report 2009 address the issues raised by Human Rights Watch.'


In response to the report, TDIC issued a statement saying that the report ‘not only neglects TDIC's policies, procedures and actions relating to worker welfare, but also makes misleading assertions and false assumptions'.


Meanwhile a spokesman for Foster + Partners said: ‘As architects appointed by TDIC for the Zayed National Museum, we fully support every effort that TDIC have outlined in their CSR Report, and indeed the considerable work they have already done to ensure the workers employed on all of their projects enjoy their core labour rights. 


‘In particular we were pleased that TDIC have established an in-house Department of Employment Practices Compliance – a step which we think will further protect workers on their projects.  TDIC have promised regular updates on their values in areas of sustanability, health and safety, labour practices and philanthropy and Foster + Partners are looking forward to working with them in order to acheive those value*s.*'


 


About Human Rights Watch:


<http://en.wikipedia.org/wiki/Human_Rights_Watch>


Full Human Rights Watch report on “The Island of Happiness”:


<http://www.hrw.org/en/reports/2009/05/18/island-happiness-0>


Article about conditions in Dubai in The Independent (UK) newspaper:


<http://www.independent.co.uk/opinion/commentators/johann-hari/the-dark-side-of-dubai-1664368.html>



 ## Comments 
1. [Daily News About Wikis : A few links about Wikis - Sunday, 31 May 2009 09:05](http://wiki.dailynewsabout.com/archives/469)
5.31.09 / 4pm


[…] DARKENING DUBAI […]
3. Nico
5.31.09 / 8pm


Lebbeus,


Firstly, it is probably important to differentiate between Dubai and Abu Dhabi, while “close cousins” they have different economic basis (financial trading vs. material oil wealth, respectively) and a differing attitude towards development.


More importantly, I can't help but think that the real crime that architects are perpetrating here is one of illusion. The allowance of the sheiks to hide behind the formations of fantasy. A search of images related to these countries produces countless visages of impeccably rendered fantasies of paradisaical development. However the reality on the ground is one of a disastrous failure of foresight into the urban structure, a construction industry based on southeast Asian indentured servitude, and the kind of abuses that can only occur in absolute monarchies (see below). The visualizations of graphical sophistication and architectural delirium obscure the world's vision, and by extension moral judgment. 


<http://mideast.blogs.time.com/2009/04/28/abu-dhabi-torture-and-the-sheikh/>


<http://network.nationalpost.com/np/blogs/fullcomment/archive/2009/04/29/abu-dhabi-torture-tape-elicits-global-shrug.aspx>
5. [The Hackenblog » Does Frank Gehry care about this?](http://hackenblog.hackenbush.org/2009/05/31/does-frank-gehry-care-about-this/)
5.31.09 / 9pm


[…] guarantees that construction companies will protect workers' fundamental human rights.” Darkening Dubai. Architects named in human rights row, by Christopher Sell, Lebbeus Woods, 28 May, 2009 addthis\_url = […]
7. [Simon's World » Blog Archive » Dubai fantasy continues to crash](http://cygielski.com/blog/2009/06/04/dubai-fantasy-continues-to-crash/)
6.4.09 / 5pm


[…] <https://lebbeuswoods.wordpress.com/2009/05/31/darkening-dubai/> […]
9. [Артем](http://lesstpierre.com)
6.11.09 / 11am


thanks this post. I made some adjustments
