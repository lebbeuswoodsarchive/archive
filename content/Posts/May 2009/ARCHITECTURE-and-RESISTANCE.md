
---
title: ARCHITECTURE and RESISTANCE
date: 2009-05-09 00:00:00
---

# ARCHITECTURE and RESISTANCE


Apropos of nothing in particular—unless it is the general spirit of acquiescence pervading the field of architecture today—I have been thinking about the idea of architecture and resistance. Although many people might judge that my work in architecture has been nothing if not a form of resistance, I have never considered it as such. To say that you are resisting something means that you have to spend a lot of time and energy saying what that something is, in order for your resistance to make sense. Too much energy flows in the wrong direction, and you usually end up strengthening the thing you want to resist.


It seems to me that if architects really want to resist, then neither the idea nor the rhetoric of resistance has a place in it. These architects must take the initiative, beginning from a point of origin that precedes anything to be resisted, one deep within an idea of architecture itself. They can never think of themselves as resisters, or join resistance movements, or preach resistance. Rather (and this is the hard part of resistance) they must create an independent idea of both architecture and the world. It is not something that can be improvised at the barricades. It takes time and a lot of trial and error. This is only just, because the things to be resisted have not come from nowhere. They have a history built over periods of time, a kind of seriousness and weight that makes them a threat to begin with. They can only be resisted by ideas and actions of equivalent substance and momentum.


The word resist is interestingly equivocal. It is not synonymous with words of ultimate negation like ‘dismiss' or ‘ reject.' Instead, it implies a measured struggle that is more tactical than strategic. Living changes us, in ways we cannot predict, for the better and the worse. One looks for principles, but we are better off if we control them, not the other way around. Principles can become tyrants, foreclosing on our ability to learn. When they do, they, too, must be resisted.


**RESISTANCE CHECKLIST:**


Resist whatever seems inevitable.


Resist people who seem invincible.


Resist the embrace of those who have lost.


Resist the flattery of those who have won.


Resist any idea that contains the word *algorithm*.


Resist the idea that architecture is a building.


Resist the idea that architecture can save the world.


Resist the hope that you'll get that big job.


Resist getting big jobs.


Resist the suggestion that you can only read Derrida in French.


Resist taking the path of least resistance.


Resist the influence of the appealing.


Resist the desire to make a design based on a piece of music.


Resist the growing conviction that They are right.


Resist the nagging feeling that They will win.


Resist the idea that you need a client to make architecture.


Resist the temptation to talk fast.


Resist anyone who asks you to design only the visible part.


Resist the idea that drawing by hand is passé.


Resist any assertion that the work of Frederick Kiesler is passé.


Resist buying an automobile of any kind.


Resist the impulse to open an office.


Resist believing that there is an answer to every question.


Resist believing that the result is the most important thing.


Resist the demand that you prove your ideas by building them.


Resist people who are satisfied.


Resist the idea that architects are master builders.


Resist accepting honors from those you do not respect.


Resist the panicky feeling that you are alone.


Resist hoping that next year will be better.


Resist the assertion that architecture is a service profession.


Resist the foregone conclusion that They have already won.


Resist the impulse to go back to square one.


Resist believing that there can be architecture without architects.


Resist accepting your fate.


Resist people who tell you to resist.


Resist the suggestion that you can do what you really want later.


Resist any idea that contains the word *interface.*


Resist the idea that architecture is an investment.


Resist the feeling that you should explain.


Resist the claim that history is concerned with the past.


Resist the innuendo that you must be cautious.


Resist the illusion that it is complete.


Resist the opinion that it was an accident.


Resist the judgement that it is only valid if you can do it again.


Resist believing that architecture is about designing things.


Resist the implications of security.


Resist writing what They wish you would write.


Resist assuming that the locus of power is elsewhere.


Resist believing that anyone knows what will actually happen.


Resist the accusation that you have missed the point.


Resist all claims on your autonomy.


Resist the indifference of adversaries.


Resist the ready acceptance of friends.


Resist the thought that life is simple, after all.


Resist the belated feeling that you should seek forgiveness.


Resist the desire to move to a different city.


Resist the notion that you should never compromise.


Resist any thought that contains the word *should.*


Resist the lessons of architecture that has already succeeded.


Resist the idea that architecture expresses something.


Resist the temptation to do it just one more time.


Resist the belief that architecture influences behavior.


Resist any idea that equates architecture and ownership.


Resist the tendency to repeat yourself.


Resist that feeling of utter exhaustion.


………..


 


LW



 ## Comments 
1. Pedro Esteban
5.9.09 / 5pm


It's a placer reads your artciles again and I'm a big resistant, that's my objective.
3. [workspace » ARCHITECTURE and RESISTANCE](http://www.johndan.com/workspace/?p=1167)
5.9.09 / 8pm


[…] Woods' little/big manifesto on resistance in architecture would work for a lot of other professions. Here a short selection of few of his […]
5. namhenderson
5.9.09 / 10pm


“Resist the impulse to open an office”  

Interesting given the times.
7. river
5.10.09 / 7am


Welcome back! And happy [Mother's](http://www.youtube.com/watch?v=sA2ro_3TCes) day.
9. [palace](http://www.palacepalace.com)
5.10.09 / 3pm


resist writing lists about what to resist, internalise them
11. [ARCHITECTURE and RESISTANCE ›](http://www.bianucci.net/journal/?p=26)
5.10.09 / 7pm


[…] ARCHITECTURE and RESISTANCE « LEBBEUS WOODS. […]
13. Kimberli Meyer
5.11.09 / 3am


Well-put, through and through  

Lebbeus it's great to see this list here and now, I believe it is the same or similar to one you put into the MAK Center publication in 2003. Each imperative is as true now as ever.  

Especially topical is “Resist believing that anyone knows what will actually happen.”
15. [lebbeuswoods](http://www.lebbeuswoods.net)
5.11.09 / 12pm


Kimberli: Right. but the text and list have been a bit revised. Still, one hopes that one's thoughts have some endurance….
17. martin
5.11.09 / 5pm


Resist the idea that architecture is a building.


Have you ever heard of Second Life? If not, I strongly recommend that you to look into it. Its theoretical implications on architecture seem boundless to me, in very real terms. Here is an article that might get the ball rolling:


<http://www.arcspace.com/sl/sl-architects/scope-clever/maxmoney/maxmoney.html>


Also, I suggest that one does not dismiss this as an internet fad or some waste of time. It's possibilities are both bizarre and fascinating.
19. martin
5.11.09 / 5pm


<http://www.nytimes.com/2009/03/08/magazine/08fluno-t.html?_r=1&scp=21&sq=second%20life&st=cse>


this will explain it more clearly, if not concisely.
21. Charlie Brown
5.12.09 / 7pm


Resist reading endless lists that add up to little more than bumper sticker philosophizing.
23. [Encapsulations. » Blog Archive » He's back!](http://so-and.org/encapsulations/blog/?p=103)
5.13.09 / 5am


[…] again and his first two entries are outstanding and very appropriate for the studio. The first, “Architecture and Resistance”, articulates the act of resisting and then lists dozens of examples of how we should be living. […]
25. [orhan ayyuce](http://archinect.com)
5.15.09 / 1am


right on mr. woods!  

long live bumper sticker nation! the list goes on.  

indeed resistance…
27. kevin Rhowbotham
5.20.09 / 4pm


A resistance


Come you masters of form  

You that build the big blocks  

You that build the death cities  

You that build all the tombs  

You that hide behind walls  

You that hide behind desks  

I just want you to know  

I can see through your masks


You that never decline  

But build to destroy  

You play with our world  

Like it's your little toy  

You take the roof from my head  

And you dazzle my eyes  

And you turn and run fast  

When the poor start to cry


Like Judas of old  

You lie and deceive  

Perfect lives can be won  

You want me to believe  

But I see through your eyes  

And I see through your spoofs  

Like I see through the water  

That leaks through my roof


You make all the plans  

For others to make  

Then you set back and watch  

While they discover the fake  

Then you hide in your mansion  

While the poor people's soul  

Drains out of their bodies  

And falls down your hole


You've thrown the worst fear  

That can ever be hurled  

Fear to bring children  

Into this world  

For bullying my joy  

Unborn and unclaimed  

You ain't worth the blood  

That runs in your veins


How much do I know  

To talk out of turn  

You might say that I'm old  

You might say I'm unlearned  

But there's one thing I know  

Though I'm older than you  

Even Jesus would never  

Forgive what you do


Let me ask you one question  

Is your money that good  

Will it buy you forgiveness  

Do you think that it could  

I think you will find  

When your death takes its toll  

All the money you made  

Won't buy back your soul


And I hope that you die  

And your death'll come soon  

I will follow your casket  

In the pale afternoon  

And I'll watch while you're lowered  

Below what you create  

And I'll stand on your grave  

‘Til I've settled my hate 


after Bobby Dylan Masters of War 1963
29. Charlie Brown
5.22.09 / 4am


Who asked you, kevin? Your quote is almost as stupid as this long list of ‘resist's.
31. Greg
5.22.09 / 4am


Resist people who tell you not to read Lebbeus Woods.


Resist starting a blog.
33. kevin Rhowbotham
5.22.09 / 2pm


A Reader in Resistance


Adorno  

The Jargon of Authenticity


Kashmir Malevich  

• The Non-Objective World


Bob Dylan  

• It's all right Ma (I'm Only Bleedin')  

• Gates of Eden


Frank Zappa  

• Bobby Brown


Eminem  

• Mosh


John Cooper Clarke  

• Beasley Street


Leonard Cohen  

• Everybody Knows


Percy Bysshe Shelley  

• Ozymandias


Herbert Marcuse  

• One Dimensional Man


Pierre Bourdieu  

• The essence of neo-liberalism


Frederic Nietzsche  

• Twilight of the Idols


Oswald Spengler  

• The Decline of the West


Schopenhauer  

• The World As Will and Representation


Spinoza  

• Tractatus Theologico-Politicus.


Ludwig Wittgenstein  

• Tractatus Logico-Philosophicus


David Hume  

• The problem of induction


Claude Levi-Strauss  

• Totemism


Marcel Mauss  

• De quelques formes primitives de classification.


Foucault  

• The Birth of the Clinic


Roland Barthes  

• Mythologies


Lautreamont (Isidore Lucien Ducasse)  

• Les Chants de Maldoror


Yevgeny Ivanovich Zamyatin  

• We  

Dostoyevsky  

• Notes from the underground


Herbert Marshall McLuhan  

• The Medium is the Massage


Ivan Illich  

• Deschooling Society


Philippe Lacoue-Labarthe  

• Heidegger Art and Politics


Arthur Oncken Lovejoy  

• Great Chain of Being


Hayden White  

• The Tropics of Discourse


John Berger  

• Ways of Seeing


Susan Sontag  

• On Photography


W. Somerset Maugham  

• The Razor's Edge


Peter Sloterdijk  

• Critique of Cynical Reason


Ferdinand de Saussure  

• Le Cours de linguistique generale


Karl Mannheim  

• Ideologie und Utopie


Max Weber  

• The Protestant Ethic and the Spirit of Capitalism


Georges Bataille  

• Story of the Eye


Octave Mirbeau  

• Le Jardin des supplices


Guy Debord  

• The Society of the Spectacle 


Raoul Vaneigem  

• The Revolution of Everyday Life, (Traité du savoir-vivre à l'usage des jeunes générations)  

• Le livre des plaisirs, (The Book of Pleasures)


Leopold von Sacher Masoch  

• Venus in Furs
35. [Resistance | Andrew Venell](http://andrewvenell.com/2009/05/27/resistance/)
5.27.09 / 7am


[…] Resistance To say that you are resisting something means that you have to spend a lot of time and energy saying what that something is, in order for your resistance to make sense. Too much energy flows in the wrong direction, and you usually end up strengthening the thing you want to resist. It seems to me that if architects really want to resist, then neither the idea nor the rhetoric of resistance has a place in it. These architects must take the initiative, beginning from a point of origin that precedes anything to be resisted, one deep within an idea of architecture itself. They can never think of themselves as resisters, or join resistance movements, or preach resistance. Rather…they must create an independent idea…. Lebbeus Woods […]
37. Facundo
5.28.09 / 4am


Mr Woods, i really enjoy your works, specially the Sarajavo one. I made a work about it and it was the most enjoyable i did. Now im teacher assistant in that subject (architectural theory) in my university, Universidad de Buenos Aires, Argentina.  

The resisting issue, here is really interesting, and is one of the things i have been thinking about when i was working on this work i made. The destruction of cities, specially this kind of cities, like caothic and infraestructural poor, is really visible, and, what i find incredible, is that you gave shape to that caos and destruction. Shapes that in an ironic way looks like the newest language of architecture of the developed countries.  

Thank you for resist and help us resist  

We would love to have you here sometime
39. Dale Nason
6.3.09 / 4am


How about Resistant Plumbing!


My uncle Bill died recently. He designed the South Eastern recycled sewage outflow for  

Melbourne, Australia.  

In the 80's he was on TV, interviewed and  

shown drinking a clear glass of recycled sewage water.


^..^  

My mission currently is to resist the theories of learning  

I am being taught as part of my teacher training.  

By resist I mean that I am eating them, digesting the  

nutritive, and allowing the discarded remainder to act  

like fibre in my diet. My practice (now 12 years p/t) has  

been via crises of structural reforms, financial collapse's  

and hierachical suppression. I teach in the vocational  

sector. So my theory/s will be situated there – initially.


Later!


D
41. pete wenger
6.3.09 / 8pm


Resist reverence.  

Resist contempt.  

Resist thinking yourself an inventor.
43. [generall](http://General.la)
6.7.09 / 10pm


Resisting Resistance Resisted.
45. [On resistance: a confession… « shrapnel contemporary](http://shrapnelcontemporary.wordpress.com/2009/07/06/on-resistance-a-confession/)
7.6.09 / 11am


[…] the same vein, I was quite glad to find out that also Lebbeus Wood recently posted on architecture and resistance, showing his usual concise depth in dealing with the notion. In his serious, yet delicious, […]
47. [MISCmedia » Blog Archive » THE ESSAY'S CALLED…](http://www.miscmedia.netarcadia.com/?p=2848)
9.9.09 / 6pm


[…] …“Architecture and Resistance,” but Leebus Woods offers advice suited to all. Examples: Resist whatever seems inevitable.Resist people who seem invincible. […]
49. [Architecture of Resistance « M[e]ande\_R](http://rmande.wordpress.com/2011/02/18/labyrinth-wall-for-bosnia/)
2.19.11 / 3am


[…] via ARCHITECTURE and RESISTANCE « LEBBEUS WOODS. […]
51. [Negarchitecture « Robert M. Delap](http://robertmdelap.wordpress.com/2011/11/07/negarch/)
11.13.11 / 9am


[…] Lebbeus Woods had some wonderful insights and steps towards resolution, my favorite bits in red: Apropos of nothing in particular—unless it is the general spirit of acquiescence pervading the field of architecture today—I have been thinking about the idea of architecture and resistance. Although many people might judge that my work in architecture has been nothing if not a form of resistance, I have never considered it as such. To say that you are resisting something means that you have to spend a lot of time and energy saying what that something is, in order for your resistance to make sense. Too much energy flows in the wrong direction, and you usually end up strengthening the thing you want to resist. […]
