
---
title: ARCHITECTURE SCHOOL 401
date: 2009-05-15 00:00:00
---

# ARCHITECTURE SCHOOL 401


The use of *analogy* is the most radical and controversial aspect of the collaborative design studios mentioned here. The current ‘rehearsal' model for design studios in schools of architecture uses presumed actualities—actual sites and situations—and requires students to come up with pretend-to-be-actual transformations of them by means of architectural design. This approach has the advantage of walking would-be architects through a process resembling what they will encounter in practice as it is usually conducted, rather like a guided tour, with a teacher or design critic acting as a tour guide, but also as a client: giving the commission, assessing the architect's work, and ‘paying' the architect's fee with a grade. The fact that students in such design studios will have more freedom than in actual situations of practice does not diminish the psychological power of such a ‘real world' fantasy. It might, however, account for the disillusionment many students feel when they enter practice.


An analogical design studio operates very differently than this model. Its emphasis is placed on the design studio itself, as a community analogous to communities anywhere. What binds together the studio community is a shared situation, at the center of which is first of all an interest in architecture and all the ideas in orbit around it, and the very act of designing. Then, of course, there is a problem or question that they agree to work on together, and to answer through design.


The most important thing about the question placed before the collaborative analogical design studio is that it is one that can be answered by the members of the studio working together. This is not so simple as it seems. We are accustomed to thinking that students who address the design of a single building or an urban space, such as a streetscape or a park, do so in a way that is plausibly complete, in actual-world terms. The fact is, however, that the given buildings and public spaces are far more complex than can be dealt with in a single semester. The students, then, must be satisfied with creating the sketch of an answer or solution, and cannot discover or be engaged with the completion of their designs. A central idea behind the analogical approach is that it is best to work with tangibles the students can grasp in the studio and its given time-frame and, therefore, are able to see their designs through.


There are two other ideas central to the analogical design studio. The first is that the design process be an active encounter between the individual and the collaborative, with the inevitable conflicts resolved—if at all—by negotiation. The second is that individual ideas and designs be legible within the ultimate result. The idea, again, is to create a collaborative design result that differs radically from what we know as a corporate product, in which individual work is subsumed in a unified result. To the contrary, the form and content of the studio's work arises from diverse and expressive individuals. The result, therefore, is unified on a different plane of order.


A number of collaborative analogical studios have been conducted in different schools of architecture, at my instigation and under my leadership. In following posts, these will be presented in some detail, under headings beginning with “AS401:—–.” Hopefully, they will clarify the general ideas presented here and provide a tangible basis for criticism and a refinement of their method and outcomes.


LW



 ## Comments 
1. Stephen Korbich
5.15.09 / 11pm


It will be great to see how this studio evolves. My experience with collaboration with other designers has been mixed. In school it was very difficult and I found myself in the position of backing off my idea and helping others bring their ideas into the fold. If a student of design can understand that responding to anothers creative ideas then your ideas will be that much stronger.  

Please give updates on the studio progress (with images?).  

Cheers
3. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.19.10 / 10pm


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
5. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.21.10 / 10am


[…] lebbeuswoods.wordpress.com/2009/01/28/architecture-school-101 lebbeuswoods.wordpress.com/2009/02/06/architecture-school-102 lebbeuswoods.wordpress.com/2009/02/16/architecture-school-201 lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401 […]
7. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
