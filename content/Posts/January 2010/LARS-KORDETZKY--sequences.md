
---
title: LARS KORDETZKY  sequences
date: 2010-01-23 00:00:00
---

# LARS KORDETZKY: sequences


[![](media/lwb-kord-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kord-1.jpg)



**elevated site, woods in back, a yellow structure that seems solid and unshakeably rooted stands in the midst of a sprawling complex of buildings, bars in front of the windows bespeak confinement, the building of a psychiatric clinic shortly before demolition, made superfluous by new construction,**


**the lake below, spread out, and a railroad line usually from north to south, less often the reverse in people's heads, gotthard line, amsteg, church of wassen several times, ticino and then italy, promising warmth and freedom, part of a network linking other places and other worlds,**


***“…sometimes when the föhn spins soft threads of fog, it's a person sitting at my bed whispering and holding forth, long are the green glass nails on his fingers, and they shimmer when he cuts the air with his hands…sometimes he sits up on the bell tower and then he tosses threads, colourful threads, far out into the countryside, over villages and cities and houses standing lonely on the edge of the hill…far-reaching is his might and his glory, and no one eludes him…”***


**(Friedrich Glauser,** ***Matto regiert*****, Zurich 1991, p. 54)**


18 november, 1997, psychiatric clinic oberwil near zug,


yellow building, empty and deserted, receptive to an artistic event, psyche and its worlds, breakdown and breakthrough are the subject of investigations in the rooms of the yellow building, co-operation between artists and residents of the clinic is an uncertain, unpredictable requisite to exploring the world of the psyche, to finding answers to questions that allow conclusions and kindle further questions,


duration of work fifty-one days, no conclusive outcome targeted, a process to try and study other worlds,


a dark corridor and single cells lined up at the end, weight tangible despite the lighting, doors that admit no illusions, scored by physical attempts to break out of reality,


isolation room, place of confrontation with psyche, with architecture,


there would be a garden adjoining the door of the cell, another one further up, the dining room slightly elevated looking onto the garden, to the left a view through the window of star-studded skies, a self-contained body above empty plains, a bedroom adapted to the body, offering protection at night, accessible only at certain times and not site-specific,


being free within a larger structure, ‘being at home' means ‘settling down' inside and outside of oneself, in many places, with changing scenery and different perceptions,


floor space two metres twenty by three metres seventy-five, room height three metres sixty, walls plastered, washable paint, next to the door a built-in toilet, flushable from outside, rounded corners, thick window sill of black stone, a two-metre tall window above it,


specifications as if for other beings, for bigger people,


de-ranged dimensions, a different scale for different mental dimensions,


built a tunnel but nobody knows about it, it has to be somewhere, don't know exactly where anymore, maybe here, maybe over there,


walks uncertainly into the cell, since the age of thirteen, over and over again in this place, locked in, months days and saw only the moon,


isolation room, empty and yet charged, site for ideas, thoughts, visions,


a spatial structure breaks up a given world, scale unknown, infinite extension, direction unclear, possibly related to northsoutheastwest, or other points of reference apply,


mental building washed white, model of an invisible reality, a coexistent world to counter the visible outside world, to see the true world through confrontation, everything spread out in mutual penetration, a set of rules influenced by space and time, that takes circumstances into account, emerging out of them and open to possibility, possibilities of ‘settling down' in a world that is detached and fixed at once,


primal shelter,


breakdown breakthrough,


places change their age, grow old and new through people moving in, appropriating them, occupying shaping them according to their will, within given possibilities,


changing domicile life situation, inworld and outworld, entails giving up custom, revives the eternal search for a home, means creating structures of one's own, systems of order within a given context,


underneath, the foundation, a platform, a cellar supply unit, giving support and protection below, fragmented levels provide access, a ramp exits through a window, a tunnel underneath, with a shaft connecting it to the platform, breaks through the walls, forks off into a distant landscape, amsteg silenen hairpin turn church in wassen several times gotthard airolo ticino, additional turnoffs follow, into different landscapes and different housing,


bars in front of the windows, obvious to those who have been there once, others see landscape,


fragments of a spatial structure ready to be processed, scale unknown, building materials on site, thin wood veneer, wooden struts sawed out of larger pieces of timber, usable as beams, found objects left behind in a past that is still present, adhesive tape for the joints,


a table two chairs in the middle of an isolation room, an announcement posted on the door, drawing building houses, three days a week,


*quotation:*


***“…matto! he is mighty, he adopts all different shapes, sometimes he's short and fat, sometimes tall and thin, and the world is a puppet theatre, they don't know, the people, that he is pulling their strings the way a puppeteer pulls those of his marionettes…”***


***Friedrich Glauser,** ***Matto regiert*****, Zurich 1991, p. 82)***


residents build draw work at their spatial architectural ideas of home, of being in the world, now here or in a dream future,


spatial diagonals concentrated in the centre of the space to a beginning end or a node within the network of an invisible reality, what follows obeys the given order, other orders follow, planes spreading out and collapsing into each other,


psyche caving in,


residents determine surroundings their in- and outworlds, their outlooks and their situations, define their position within the whole, cultivate unrestricted contact with other cells,


a clear spatial structure emerges, the edges of the isolation room seem to have been shifted towards the interior, define an inner body, open and free, that constantly confronts the outside body, influenced by a different system of co-ordinates,


the static of the space suspended as if shifted into a state of anticipated shock, difference is exposed and reflection made possible by systems that are simultaneously ‘within each other',


horizontal vertical top bottom left right, orientation suspended, physically and mentally, the familiar guidelines, reality, the norms are questioned,


interim world in a state of precarious equilibrium,


several superimposed layers crop up, an endless number of conceivable storeys, without dividing walls, partitioned only by a central shaft,


we are building a skyscraper in the isolation room,


a ramp leads from the children's room to the living area, supply unit underneath, fixed component, foundation for variable buildings above the ground, linked with other units via a system of tunnels,


tunnels lead from cell to cell, to free level bodies within an overarching structure, to places of the psyche,


infinitely long lines intersect, pierce the isolation, free the enclosing areas from their mass, from the weight of their mental and physical load, explode the space,


a new layer is superimposed on prevailing memories to create new possibilities,


liberated space as the point of departure for ‘settling down', the door is open, we go in and out of the window, three days a week, conversations are conducted, the constricting surfaces lose their sense of confinement, turn into space-giving countryside, see-through, a new space emerges, the isolation room loses its name,


9 january, 1998, in front of the isolation room, three constructed fragments of a spatial structure, hung up free-floating, viewable from outside, mentally accessible inside, representing different ideas on home on architecture,


door of the isolation room open, propped open by a beam, in the cell the same spatial structure, uncluttered free open, one can walk in, experience it from within, look at the view outside, parts of the landscape on the walls, in the heads of the viewers their own constructive thoughts on settling in, unburdened, in a world of their own …



**Once a resident of the clinic came to the isolation room in the old building. Standing in front of the open door, he recounted his memories of this and other places. He also mentioned a tunnel that was probably still in the cell. At my request, he drew a map on the lateral wall of the cell by way of explanation. The map, which spread out over the entire wall, showed a landscape with towns, streets and tunnels. The streets and tunnels were like connections to others cells and scenes within an over-riding structure. Behind the wall lay the enclosed world, and in the isolation room there was a structure – part of a larger concept, like a second system of co-ordinates or a counter-world. It offered a setting for ideas, thoughts and dreams.**


**Exorcising the burden of this place and the physical and mental constraints associated with it were important aspects of producing a work in the isolation room of a psychiatric clinic. Architecture and the psyche are confronted with each other in a place situated in the border regions of perception: a springboard for further thoughts and questions.**


**Is there spatial composition that can do justice to the various states, even extreme states, of the human psyche and the resulting perception of the world? How is architectural space actually perceived when insides turn out and outsides turn in? Is there a room of the psyche?**


**Faced with the impossibility of finding tangible answers, I studied the border regions of sensation and perception to design a spatial structure of the psyche, fully aware that knowledge of these regions is necessarily vague. The term “psyche” and its inaccessibility became the potential that fed into the design process and its implementation.**


**Currents – landscapes, colours, light, … – flow around the “rooms of the psyche”. Freed from the constraint of surrounding surfaces, interiors seem to be turned inside out and vice versa, depending on the resident's state of mind. The rooms are meant to be like particles in unrestricted space. Users should be able to move about in their own realities.**


**……..**


(Text translated from German by Catherine Schelbert)


[![](media/lwb-kordet-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-2.jpg)


[![](media/lwb-kordet-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-8.jpg)


[![](media/lwb-kordet-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-4.jpg)


[![](media/lwb-kordet-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-9.jpg)


[![](media/lwb-kordet-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-6.jpg)


[![](media/lwb-kordet-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-12.jpg)


[![](media/lwb-kordet-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-5.jpg)


[![](media/lwb-kordet-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-10.jpg)


[This post is excerpted from the book “SEQUENCES: saw only the moon” showing the entire project by Lars Kordetzky:


<http://www.amazon.com/Sequences-Only-Moon-Lars-Kordetzky/dp/321183642X/ref=sr_1_3?ie=UTF8&s=books&qid=1264262104&sr=8-3>]


[![](media/lwb-kordet-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-kordet-11.jpg)


**Lars Kordetzky**



 ## Comments 
1. [BRAIN AND BUILDING « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/08/28/brain-and-building/)
8.28.10 / 4am


[…] patients in the doomed Oberwil psychiatric clinic in Zug, Switzerland, visualizing and constructing the imaginary spaces that actually existed for them in the barren confines of their setting.  Since then he has […]
3. Pedro Esteban
3.27.11 / 11am


I don't understand anything, but I get everything…….great work, outstanding, they are the best architects ever!  

Please could you put a post updating his work?
