
---
title: BOULEZ IN SPACE
date: 2010-01-12 00:00:00
---

# BOULEZ IN SPACE


[![](media/boulez-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/boulez-2.jpg)


*(above) spatial layout of musicians and audience for Répons (drawn by and courtesy of Michael Singer).*


Mixing the audience with the players in a musical performance is by now a well known avant-garde tactic, though—like most things avant-garde—it has hardly budged the convention of audience on one side and musicians on the other. In that arrangement, the music is played ‘to' or ‘for' or ‘at' the listeners; moreover, it expresses and enforces social divisions active in the world outside the concert hall, between workers and patrons, between art and ‘real' life. As has been noted elsewhere, and many times, any arrangement of space is inherently political; however, the political and social origins of any traditional arrangement are often forgotten, which is to say, obscured by habit and nostalgia. We forget that the hierarchies of the societies Haydn and Wagner composed their music for are really antithetical to modern ideals of participatory democracy. This does not mean that we should not listen to their works, and in the spatial arrangements they were composed for—quite the contrary. What it means is that the new music of our time needs to be performed in spatial arrangements that reflect our most critical experiences and values, those that engage a new level of complexity in relationships between people and between events. The same can also be said of the new architecture of our time.


The spatial designs of Pierre Boulez, composer, conductor, and critic, who sees the necessity of composing new music, but also the wisdom of writing critical essays presenting both its musical and social bases, are radical departures from tradition and convention. His works—by nature collaborative and interactive—give form to the anxieties, tensions and strange, unexpected harmonies that emerge from the discordant mélange of an international community assembling itself from the shards of old societies and the raw forms of new ones. His music is not meant to comfort or distract us, but to give us experiences that aid us in joining the struggle.


LW


New Tork Times article on Boulez:


<http://www.nytimes.com/2010/01/10/arts/music/10boulez.html>


Boulez Répons:


<http://www.youtube.com/watch?v=nfSfYiGsZMU>


Boulez biography:


<http://en.wikipedia.org/wiki/Pierre_Boulez>


[![](media/boulez-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/boulez-1.jpg)


The following is a comment in response to this post by composer Svjetlana Bukvich-Nichols that I think is of interest:


Boulez was much needed in one historical moment (namely post-war Germany and Darmstadt), and he rode the wave of that moment well. However, I believe that his tongue did more for his legacy than his compositional work. Boulez did not create a new language (such as Scriabin or Reich did), but used an existing one which eventually self-imploded. Then moved to improvisation, chance (what Cage is known for, among his other contributions), electronics, and world music…?!? I see him more as a proponent of a system than a creator.


Much of his ideas come from a kind of thinking (euro-centrism, anyone?) which is essentially dated. “Performers aren't audacious enough today,” Mr. Boulez said in a recent New York Times interview. “They think audiences won't respond to what's unfamiliar. But to provoke — in the good sense — is the performer's role. It's not just to give one more concert.” “That's not culture,” he said. “That's marketing.” This type of generalizing is not applicable to music outside of the confines of mainstream European concert practice. Perhaps he should have tea with John Zorn.


Boulez is very right about the importance of tools, though, and the element of danger and risk all truly new music possesses: “Tools are important,” Mr. Boulez repeated. “Mallarmé chastised Degas for writing poems. He said, ‘You can't just have an idea that you want to write poems. Poems are made out of words.' ” The only problem for me here is that Boulez likes to tell the world which tools should be used for the evolution of communication with sound. But, it is not all dire. In a true historical rubber-band moment, Boulez yielded people like me. Between larger-than-life serial and avant-garde orthodoxy on one side and Russian romantic orthodoxy on the other, I fled from Sarajevo to the U.S. Minimalism seemed—back in the 80s—to be creating a new thing.



 ## Comments 
1. [Greg](http://www.talaske.com)
1.12.10 / 7pm


While the integration of audience and orchestra in space is an interesting and democratizing visual/spatial experience, it can actually be very stratifying and prescriptive acoustic/spatial experience. Whereas I follow the concern about having music played “to” listeners, there is a certain spatial arrangement between large ensembles and listeners that is required if the listener is to experience the totality of the music at once. Any moderately-sized ensemble (such as indicated on the sketch) creates sound at different positions in space and, for them to blend together at all, there must be some distance for the sound to travel betore it becomes a coherent summation. 


Placing the listeners in the midst of the ensemble means that each “zone” of listeners will hear only a portion of the performance. A listener's seating location predetermines the particular emphasis of the listening experience they will have and, conversely, deprives them of the rest of the emsemble's musical contribution. 


I suppose it could be argued that the listener is free to choose their seating position, and thereby their experience is not dictated to them. However, this presupposes an educated listener who has researched their listening position preferences ahead of buying their tickets. This is a rare listener.


As one who works on the acoustic design of performance spaces, I am deeply committed to exploring ways for musicians and listeners to develop new means of interacting with one another. It is crucial, however, that any visual/spatial forms also account for their acoustic/spatial implications.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.12.10 / 10pm
	
	
	Greg: You are right. As a professional I'm sure you're aware of similar problems in a traditional concert hall—it takes a ‘magical' space like Carnegie Hall to meld the sounds equally for all listeners, at least as much as possible. Even then, people seated in orchestra left will hear more violins than those in orchestra right. Anyway, a piece like Boulez' Respons, I am pretty sure, is meant to be heard differently by different listeners. Imbalance and in a sense fragmentation are designed into the listening experience. Everyone comes away from the performance with a different perspective. Philosophically speaking, it's the difference between a quest for Classical unity and totality and a quest for Existential, personal authenticity. Your last paragraph seems to me to be in this spirit.
3. [larry rinder](http://www.bampfa.berkeley.edu)
1.17.10 / 4pm


At the Berkeley Art Museum we have been experimenting with musical performance in the vast central atrium of our Mario Ciampi-designed building. Two performances thus far–both programmed by Sarah Cahill– have taken advantage of the unique setting and sonic properties of this resonant space to present remakrable musicians in unusual spatial relation to their audiences. Last November the pianist Terry Riley played in the very center of the space surrounded by nearly 800 visitors who formed an expanding circle around him. Many people lay on the floor on pillows, while others watches from overhead balconies (from one vantage point, one could look directly down on the keyboard and Riley's hands) and others explored the galleries themselves, still able to hear the sound flowing through the open space. A simlarly dynamic experience took place for those attending Ellen Fullman's performance of her long stringed instrument. The programmer, Sarah Cahill, has really done a lot to develop the possibilities of dynamic audience/performer engagement through her annual summer solstice event at the Oakland's Chapel of the Chimes. In this event, visitors are encoruaged to move about the vast Julia Morgan-designed mortuary listening to performers playing in crypts large and small. Lastly, I'd like to mention the strikingly innovative Tower, designed by Ann Hamilton, located in Geyserville, California. This eight storey structure incorporates a double helix internal stair, so that the audiences can move up and down in relation to the performers who are situated on the opposite stair. A project done last summer by Shahrokh Yadegari combined moving singers and musicians as well as a dynamic computer-programmed sound sysytem. Musical performance in non-traditional spaces certainly offers a wealth of artistic and social opportunities!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.17.10 / 10pm
	
	
	larry: these are wonderful explorations and I appreciate hearing about them. Two questions: can we find any traces of these works online? Then, it would be great to know the music that was performed—classical, or contemporary, or perhaps improvised or composed for just these performances? I realize that like so many performance works, they are difficult—if not impossible—to document in any literal way. One simply has to be there. Still, all of us who were not would want to participate in some way, and in this age of electronic mediation would wish for the creative artist who would bring us closer to these events and experiences, through video, film, recording, or a combination of these and other media. 
	
	
	
	
	
		1. [Larry Rinder](http://lrinder@berkeley.edu)
		1.20.10 / 10pm
		
		
		Here is a link to Yadegari's Tower Sounds (excerpt): <http://www.youtube.com/watch?v=PGaJrUsqTfA>
		
		
		Here is a video on Ellen Fullman's project: <http://www.youtube.com/watch?v=Uq9AFRMWLTU>
		
		
		Unfortunately, there is no video of Riley at BAM, but here's a nice pic (you can find more on Google): <http://www.dailycal.org/photos/20091109/107419-11.09.composer.MCDERMUT-01.jpg>
5. Mclng
10.13.12 / 2pm


I find the comments of Svjeltana Bukvich incredibly self-serving. I suppose she would consider any composer not using a Synthesizer, like she does, to be out of touch and dated. I find her words to be more of a promotional announcement for the superiority of her own work (which I will refrain from commenting on here) than anything else. Let's see who is remembered in fifty years, and then we'll have a better discussion.
