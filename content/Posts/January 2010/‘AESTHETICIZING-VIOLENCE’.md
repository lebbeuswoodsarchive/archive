
---
title: ‘AESTHETICIZING VIOLENCE’
date: 2010-01-15 00:00:00
---

# ‘AESTHETICIZING VIOLENCE'


Architects love to take on the easy problems—the ones already solved—and make them difficult. This way they can have it both ways: operating safely within the boundaries of the known and, at the same time, being daring innovators—but with a minimum of actual risk. For example, the high-rise office tower. This problem was solved by Sullivan and Adler, in 1894, with their Guaranty Building in Buffalo, New York. You can make an office building taller, wider, twist it, sculpt it into all kinds of shapes or give it a variety of new skins and it will still be essentially the same as Guaranty: a vertical stacking of level floors with grouped stairs and elevators, with public access and service at the ground floor and mechanical services on top. Knowing this, one has even more respect for Mies van der Rohe's Seagram Building, because it tacitly acknowledges the precedent by earnestly recasting it and nothing more. There are other examples, but the point here is different: architects are reluctant to take on the difficult problems—the ones not yet solved—such as the restoration of slums; the reconstruction of buildings and cities damaged by war; those devastated by earthquake, hurricane and other natural disasters; and especially, the invention of architecture that mitigates the tragic effects of such catastrophes by, in effect, anticipating them.


The reason, I believe, is not the obviously daunting difficulty of such problems, but the risk of being stigmatized by taking them on. Architects who do so—and they are few—are often accused of preying on human misery for their own gain, or at least to advance their careers. Architects are particularly susceptible to this accusation—doctors, for example, who search for a cure for cancer are not. Perhaps it is because architects present themselves as following a ‘higher cause,' meaning a higher ‘class' of clients and projects. The best architects prefer to be thought of as great artists, not as benefactors of the great numbers of common people, and so ignore the problems that most affect these people. Hence, their problems are unaddressed and remain unsolved.


One of the most common charges leveled at architects who address these unsolved problems is that they are “aestheticizing violence.” In other words, they make violence, or at least its effects, look good. The violence of poverty. The violence of war. The violence of nature. Not unlike Hollywood action movies, which glamorize violence by making it seem heroic or visually exciting, architectural designs incorporating the effects of violence—in order to transform them into something at the least non-violent—are seen by many as an acceptance of the causes of the violence. I do not believe this is the case. Architects are by nature pragmatists who want to deal with real conditions—even the most idealistic ground their designs in the actual. So, why would it be surprising that they want to deal with the effects of violence that has already occurred? Perhaps many do—but they are afraid.


It is important to distinguish here between causes and effects. No architect would wish for the violent destruction of human communities just to enhance his or her career, just as no doctor would wish for the creation of cancer just to win a Nobel Prize. But once cancer exists, its destructive effects have to be treated, and—by anticipating them—its cause eliminated or ‘cured.' The task of the few architects who dare to engage in their work destructive forces and their effects in our time must not only struggle with them but also with the stigma of doing so.


LW



 ## Comments 
1. Josh
1.15.10 / 6am


LW


I recently sent you an email on your Lebbeus Woods . net site in regards to the efforts in Haiti. Would you have the time to read it and pose a response?


Thank you





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.15.10 / 5pm
	
	
	josh: I will track down your email…
3. ehud rostoker
1.15.10 / 6am


Could you give some examples?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.15.10 / 5pm
	
	
	ehud rostoker: I don't feel comfortable implicating architects by name, other than myself. My projects for Sarajevo, Bosnia during and after the siege of that city (1992-96) were widely condemned for being expoitative and ‘aestheticizing violence.' This is not the place to argue the pros and con of the projects themselves, but only to note that the critics object to the very idea of considering ‘war and architecture'—on ‘moral' grounds. The same with my studies for the San Francisco Bay Area earthquakes and later my Terrain projects, which visualize ‘earthquake architecture.' I believe that the reception this work received (and still does) discourages others from pursuing design research in these areas. How it might affect anyone who wants to do exploratory work for the reconstruction of Port-au-Prince is an open question. If they are not Haitian, they will almost certainly be condemned as ‘neo-colonialists,' and the like….
5. [Easy versus Hard » Life](http://areadeefecto.com/life/archives/67)
1.15.10 / 6pm


[…] easy for Lebbeus Woods to talk about the Hard Life.  Here is a part of his post “Aestheticizing Violence” .  Here he argues for designers to venture with their work into the financially riskier fields of […]
7. [Stephen Korbich](http://www.ffadesign.com)
1.15.10 / 7pm


I agree that architects can do more. But then, so can everybody else. Unfortunately the high death tolls happen in places with a lot of poverty and corupt governments. Architects can only do so much to influence these countries, but it doesn't hurt to offer up ideas.  

As far as aestheticizing violence goes…  

This is done in art all the time. Architects walk the line between art and the real and so when they propose designs which react to violence or destruction they are under more scrutiny.  

Right now I am working on a grade school that was ravaged by fire. No one was hurt, which can be attributed to numerous exits and good planning.  

It seems like we got the job because we were first on the scene. Did we take advantage of an opportunity? Sure, but we do want to help in addition to getting a fee.  

I also have to admit that there is a beauty in the ruins of the building, with rooms floors and walls mostly in tack yet open to the sky. I may even do some paintings of what I have observed.  

So Lebbeus, if you are getting hasseled for your work, don't fear, it just means that you are doing it right. More of us should be so bold.
9. [Chris Teeter](http://www.metamechanics.com)
1.16.10 / 5am


in short: people are narcissictic self centered pricks who give their identity away when they reflect on others and humanity.


why did i decide to become an architect? to change the world…no one told me i was a powerless servant to the rich; damn this fact of the profession.


i don't even know the guys name anymore who started Architecture For Humanity… I remember people would harass him on Archinect for being as you suggest above others. if he had an ego, his ego has transferred funds to the poor, so what?!? why does anyone have a problem with this?


 i understand this cynicalism, personally. my parents were chrisitian missionaries and still poor people who give their hearts to their students and anyone who needs some care; retirement has never been an option. i used to say in theological debates with my father (as my mother was seriously distraught by my objections)..i used to say Jesus did it to be famous, to be immortal to be like Socrates who was made up by Aristotle (some say). Jesus did what he did for himself. Charity can make the rich more powerful. Preaching hope can make you money.


so you harass say Brad Pitt, rich loaded guy trying to make a difference…believe it or not he was cursed with talent and riches and he may just be trying to give a shit…but it's a lot harder for many to understand why anyone with that much money would give a shit, because they probably couldn't.


i have a suggestion to architects: we don't make a lot of money and in principle we love what we do over the money we recieve, so why not start non-profits in all of our companies to offer free design to people who need it, at least be smart with the hours we never get paid for, make them charitable hours of tax deduction donations.


do people need design? 


AFH should come up with a competition on how to import ample amounts of wood lumber to Haiti with Simpson-Strong Tie connections to rebuild their cities, or another scenario in which re-bar could be cheap, to properly engineer their buildings…


this catastrophe could have been reduced to a mere annoyance had an engineer or architect convinced those with money to build something proper for the ihabitants of Port-du-Prince.


tax write off: architect design earthquake proof generic builing in Haiti.


how hard could this be?
11. D. Coulombe
1.16.10 / 7am


It seems that in other design fields (namely graphic design) the aesthetisation of violence has become an accepted and common practice. Is architecture far behind? Perhaps this beautiful violence often depicted in fashion ads is linked to an emerging sense of nihilism aimed at our late capitalist society. If this is the case, would a violent architecture merely be a symptom of this?


On a final note, I would argue that the erasure of violence is a far more violent act than it's aesthetisation.
13. [Tweets that mention ‘AESTHETICIZING VIOLENCE' « LEBBEUS WOODS -- Topsy.com](http://topsy.com/tb/is.gd/6kTYQ)
1.16.10 / 7pm


[…] This post was mentioned on Twitter by farid rakun, Christian Schmidt. Christian Schmidt said: Lebbeus Woods: "Architects love to take on the easy problems—the ones already solved—and make them difficult." <http://bit.ly/4tnOCD> […]
15. Brett Holverstott
1.17.10 / 11am


It is interesting that the technology for tempering the effects of natural disasters is very well known. It is not “dauntingly difficult” to reinforce a building to withstand lateral loads. Should Calatrava stop designing the spire and go add reinforcement to Haitian schools? It would be a misuse of engineering talent.


The difficulty is in making people realize the risks and mustering the resources to prepare for those risks. We live in an age in which scientists are getting very good at evaluating the risks. Two years ago scientists warned the Haitian prime minister of a potential 7.2 magnitude earthquake. But the resources were simply not available, and the timetable for such a disaster so uncertain. What needs to happen is to continue to strengthen third-world economies and to have leadership that makes building safety a priority. Perhaps help from the first-world should also focus on disaster preparedness in addition to disaster relief.


Skyscraper design is at the cutting edge of engineering, and is a proper place for structural engineers. It is not a solved problem, as evidenced by the new discoveries being made with regard to mitigating wind forces. But I also admit that the significance of building taller has been inflated. The pyramids held the record for 3000 years. The Lincoln Cathedral for 230 years. Now, you might hope to hold the record for 20 years.
17. Flavin J
1.19.10 / 11am


great post.
19. Flavin
1.19.10 / 12pm


“Should Calatrava stop designing the spire and go add reinforcement to Haitian schools? It would be a misuse of engineering talent.”


Actually it might be the reverse: Caltrava's work on symoblic, overwrought ego projects is the waste of his talent.


The burj khalifa is a disgusting dick measuring contest, not an interesting building or a good use of talent. The “aestheticizing violence” argument is way of not recognizing that a lot of architecture is complicit in environmental destruction. Going into Haiti or Sarajevo is not where the violence lies, it is the reverse of aestheticizing violence
21. [chris teeter](http://www.metamechanics.com)
1.19.10 / 1pm


You know as far as mother nature is concerned,architecure is the very aesthetic of human violence….but that wasn't your point.


Half the reason architects don't take on the ‘hard problems' is due to education. What you would theoretically need would be a dual major in social or political science and a profession that didn't require you to produce to make a living (capital, capital). Doctors heal, architects get buildings designed and built.


Kieran Timberlake had this studio at UPENN which is surely still going on as it was a 5 year theme, which from what I remember was to solve third world problems, etc… I participated in its first semester, a semester to come up with the right questions. Having worked a couple years it seemed absurd to spend 5 years on this theme, as the solutions only had to be given theoretically. Studio seemed like this long stalling at getting nowheree  

, like when working with a designer who claims they can design but can't and use all types of procedures to delay the expression of a design solution. Should architects be solving these problems this way? If the building technology envelope were pushed in such a way that new products for earthquake cheap construction were developed wouldn't the architect be a lot more useful than asking flat out how an architect can save the world?


Traditional architect training prepares you to solve built environment issues of space, form, and cost. Politics such as sustainability or shelter for the poor are presented as personal options of preference.


Its a lot to ask architects to do anymore than that, but if they do they should be at the very least applauded.
23. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.19.10 / 3pm


A great colleague of mine, Spencer, sent me this link based upon a premise I have been pondering for some time. Pondering in the sense of not the theoretic aspect as to why, but contemplating a strong business plan that can execute helping people as a reality–post disaster certainly and pre disaster planning ideally. Why, because it is a huge problem and I have yet to see a viable solution. Sure Brad Pitt and his Make It Right Foundation are a huge step in the right direction. But, the costs, rate of production, amount of manpower compared to the deliverables do not equate. Habitat for Humanity is a great idea, but then again this is what I call old school construction.


I have been studying architectural fabrication since I came across architecture, from modules from the macro [office, house, entertainment] to the micro [the constitution of components] and how these all fit together as systems–like that of an automobile or ship. My solution to this problem is what I call Rapid Systematic Design + Build. Imagine a car factory converted into a architecture factory, run mainly by machines governed and tuned by people. My vision is turning a LHA, USS Ship into a architecture mobile fabrication plant and these designs are not throw backs to the modern movement, or manufactured housing. But, to a movement of our changing patterns thoughts, socialization and interaction with our peaceful yet hostile environments. I apologize for I am not willing to share specifics, but the point is we can offer a lot better and should do so.


So, why do this emergency architecture? I agree with Lebbeus Woods, in a indirect way, this is our responsibility, this is what we do. Bandwagon sounding perhaps, but help is help. 


Lebbeus Woods your writings and work have been great guides, thank you. 


m|pearce





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.21.10 / 4pm
	
	
	Michael Phillip Pearce: Your idea of a mobile fabrication plant for architectural components (arriving by ship or, possibly also, by air) in emergency conditions seems like an inspired one. With the funding to actually do it, this could be a practical solution. The question is, then, what are the components? How do they come together to make camps or to immediately fit into damaged cities or towns? You may not have the answers already, but they are well worth pursuing as a design project. I hope you, or someone inspired by your comment here, will do so.
	
	
	
	
	
		1. [Chris Teeter](http://www.metamechanics.com)
		1.23.10 / 12am
		
		
		see I-Beam Design's Palette House. ship in the material on wood palette's, then build house.
25. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.21.10 / 7pm


Thank you Lebbeus Woods. I have much work at stake here, and am fearful, similar to the concept of your chair design acquired by the film, 12 Monkeys.


But, sharing concepts to better our world is a must. I foresee a mobilized design build operations unit, first a converted ship, I previously mentioned a decommissioned LHA or LPD, but for my business model a fishing trawler is better suited.


Finding funding and investors is my weakest link thus far. The building systems are a defined kit of parts, archaic at first, retro-fitted shipping containers, of different units for living, eating, living, etc. Later, these units would evolve into streamlined systems of fabricated modules along with components to retrofit the existing architecture. Vague I understand, and apologize.


Anyone can simply take shipping containers and turn it into what I refer to as Emergency Bunkers–pods to protect the living and pods to ship materials, tools, soils, seeds, etc., to aid now and tomorrow. Although, this is the quick fix, it certainly is not the best solution that I foresee, but a immediate component nonetheless. 


I look forward to hearing other visions of what is and can be.
27. [Josh](http://www.unitebydesign.tumblr.com)
1.27.10 / 12am


Michael,


Our newly formed initiative has taken Lebbeus' thoughts and visions and is trying to make something real. The events in Haiti have become the impetus for our reasoning and our Manifesto or life-stance is one that focuses on the greater good and a collaboration of ideas from a multiplicity of disciplines. Your ideas are valued within our group here at Kansas State University and we are actually hosting a symposium or meeting of the minds that is an experiment of sorts in this idea of collaboration and the sharing of knowledge between fields that are normally not found in connection.


If you, or anyone else is interested in participating or offering their thoughts please do not hesitate. Our forum is the 27th of January and we will post updates on our process and see where this takes us…


We are all professional amateurs, the age of the renaissance man has come to an end… it is our duty to  

educate ourselves and others so that we can at least say we tried. We have ideas, we want to help, so we are proving it…
29. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.27.10 / 4pm


Josh have your team locate and make contacts with your sources at KSU. This might be your ticket because the University could help. First, target churches, and political science groups, recruit people for diversity and funds to aid. Coordinate with the ROTC, perhaps you could get free transportation and mobilization to and from the site. Set up a preliminary budget and quantify your numbers, perhaps make this a class on the art of re-building for mass production—not 1 house in 6 months but be aggressive and shoot for a 1 month/100 people have comfortable dwellings. Get some professors involved, architecture and other disciplines to make this a course so that it may get properly funded. Get community involved how do they help re-build, make them you habitat for humanity. Talk to habitat of humanity, red cross and organizations of the like, have them help you help them. 


Indigenous Materials: get an assessment of what is abundant in Haiti, use these as the main ingredients and so forth, set up the palette for re-build using what is there. Start setting up a volunteer camp, free food and medical to those who help re-build along with training to what you build and how you build it.


Materials from the outside: Get a solar companies involved, and alternate means to generate energy, see if they will fund/donate products. Talk to the sciences at KSU get them on board, again, make this a University project. Contact rainwater hogs and see if they would be interested in helping for secure water storage. In general, set up distribution list of the products you intend on using, and generate a systems list of components to put in your architecture/shelters. 


This is just a rain of thought, good luck to you.  

Michael
31. [UNREAL RUINS « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/05/unreal-ruins/)
4.5.11 / 11pm


[…] on this blog, I have written about “aestheticizng violence” and “terrible beauty,” discussing artists'—and occasionally an […]
33. [blog wunderlust: 25 Jaunary 2010 | Architecture Design & Trends](http://architecture-trends.com/?p=406)
6.8.11 / 11am


[…] or whatever | plug & play hospital | imaginary landscapes | Gehry does context… sort of | anaesthetizing violence | the last picture show | the ultimate Trottoir Roulant guide | transform cities into lush jungles […]
35. chris
11.10.11 / 2am


LW


i'm in the process of compiling my masters thesis, and would like your  

permission to use this particular blog entry as a reference for my  

argument. 


thanks,


chris





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.10.11 / 12pm
	
	
	chris: By all means do. Like to know more about the thesis.
