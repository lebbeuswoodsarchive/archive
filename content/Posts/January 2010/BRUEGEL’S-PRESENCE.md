
---
title: BRUEGEL’S PRESENCE
date: 2010-01-28 00:00:00
---

# BRUEGEL'S PRESENCE


[![](media/bruegel-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-2a.jpg)


[![](media/bruegel-3a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-3a1.jpg)


(above) *The two* ***Tower of Babel*** *paintings by Pieter Bruegel. In the earliest version, the Tower rises optimistically skyward, still under construction at the top. In the later version, it rises no more, but has already begun to fall into ruin, not because of Divine wrath, but, it seems, because of  human neglect or error.*


Speaking about influences on my work (weren't we?), I have to say that the only architect who moved me as a young man was Mies van der Rohe. Oh, I liked some of the work of other architects, but it was the Mies of the Barcelona Pavilion and Crown Hall at IIT that got my blood flowing. I also have to say that it was painters who had more of an impact than architects, and among a handful that did, Pieter Bruegel stands out in my memory. Looking back, I can see very well why this was so, and also what a great influence he ultimately had on my visual imagination.


Bruegel lived and worked in Flanders, in the southern part of The Netherlands, in the middle of the 16th century. He traveled to Rome as a younger man and brought back his experience of the great, revolutionary Renaissance art he saw there, but remained for all his working life a northern painter, that is, with one foot still firmly in the Middle Ages. What this means is that while he incorporated a measure of Renaissance humanism, he did not adopt with it the lofty idealism embodied in a “rebirth of classical knowledge” that so inspired Italian artists, but instead remained rooted for his truths in the realities of everyday life. His older, near-contemporary Erasmus embodied the skeptical, always questioning intelligence that characterized down-to-earth northerners, and so, in his own way, did Bruegel.


Bruegel's paintings celebrate the informal complexity of the everyday in villages and towns that lacked the spatial and social hierarchies characterizing their Medieval counterparts to the south. They had about them a sometimes abrasive melding of classes and their activities that was proto-democratic. Most of his paintings retain a Medieval fascination with details and the complicated narrative they create. Their compositions lack any single focal point, but are what we would today call ‘fields,' made up of many competing figures and objects, all rendered more or less equally, and requiring the viewer—not the artist—to select what is important to focus on. The themes of the paintings range from the everyday to the allegorical, the mundane to the metaphysical. Even these differences, however, are treated with equanimity, as though they were all in the same measure simply parts of life.


Umberto Eco, the theorist, novelist, and Medievalist, has claimed that globalization and the sometimes abrasive melding of peoples and cultures in our time have not yielded a new Classical age in the Enlightenment mode, but rather a new Medieval age, negotiating between chaos and order, seeking a complex, ever-changing balance of competing forces. The emergence of the internet, with its anti-hierarchical form, certainly bears out this point of view. Pieter Bruegel's paintings are more alive today than at any time since he made them. In many ways, he was one of us.


*(below)* ***The*** ***Fight between Carnival and Lent.*** *In this Netherlands town scene, people of all kinds and classes mix casually, un-self-consciously. In the foreground, a somber procession emerging from a church in honor of Lent (when Christians are supposed to sacrifice some pleasure in anticipation of Easter) encounters a band of drunken revelers immersed in their pleasures. The painting is not so much an illustration as an allegory of human diversity and tolerance of it.*


[![](media/bruegel-13.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-13.jpg)


(below)***The******Fall of the Rebel Angels.*** *According to Biblical accounts, Lucifer and the other angels who have rebelled against the authority of God are being driven out of Heaven into the lowest world of the damned. Here Lucifer proclaims, according to the poet John Milton, that he would “rather reign in Hell, than serve in Heaven.” The anti-hero is born….*


[![](media/bruegel-12a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-12a.jpg)


*In no other Bruegel painting is the Medieval fascination with the invention of the grotesque, in obsessive detail, so freely expressed.*


[![](media/bruegel-12b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-12b.jpg)


[![](media/bruegel-12c.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-12c.jpg)


(below) ***The******Triumph of Death.*** *In this apocalyptic vision—which has no Biblical counterpart—death comes to a vast landscape. The legions of death march with symbols of Christian ideology, mercilessly destroying all they find—no social class is spared. Over the horizon, immense fires are raging—an anticipation of the entire world at war.*


[![](media/bruegel-11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-11a.jpg)


[![](media/bruegel-11c.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-11c.jpg)


(below) *The soldiers of death herd their victims into a large, box-like object—a prefiguration of Nazi gas chambers?*


[![](media/bruegel-11b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-11b.jpg)


(below) ***The******Parable of the Blind****. The Biblical parable states that “when the blind lead the blind, they will fall into the ditch.” Two unique aspects of this late painting are that each of ‘the blind' has lost their sight from a different eye disease, rendered with scientific precision, causing speculation that Bruegel may have been a medical doctor; and also, the painting shows (left to right) the blind men in progressive stages of balance or losing their balance—like a Muybridge photographic sequence of movement, or a stop-motion animation.*


[![](media/bruegel-10a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-10a1.jpg)


[![](media/bruegel-10b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/bruegel-10b.jpg)


LW


*See [this New York Times article](http://www.nytimes.com/interactive/2010/12/13/arts/design/abroad-the-wine-of-the-feast-of-st-martins-day.html?ref=design) for a close look at a newly attributed painting by Pieter Bruegel.*



 ## Comments 
1. [Lebbeus Woods on Bruegel's Tower of Babel. « thousands of rhizomating blossoms](http://athousandrhizomes.wordpress.com/2010/01/28/lebbeus-woods-on-bruegels-tower-of-babel/)
1.28.10 / 1pm


[…] Lebbeus Woods on Bruegel's Tower of Babel. BRUEGEL'S PRESENCE « LEBBEUS WOODS. […]
3. [BRUEGEL'S PRESENCE by Lebbeus Woods – thoughtfox](http://www.cocreate.ca/thoughtfox/?p=34)
1.30.10 / 3am


[…] …/more […]
5. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.31.10 / 6pm


I remember seeing Bruegel's art work on the cover of a Black Sabbath album as a teenager, it was the “Triumph of Death”. Upon seeing it is was more like a charismatic invitation to get lost in it, reading it over and over. Interesting how we read—perceive information. 


Thanks for sharing your insight and influences. Although, Mies is a surprise. I would have guessed the Futurists, specifically the work of Antonio Sant'Elia. I was fascinated by that movement similar to Bruegel's art work, which are more like visuals of his internal insight.
7. [Dave Irwin](http://organicmobility.com)
1.31.10 / 6pm


I am currently studying in Rome. This post reminds me of the layers that are present here. The medevil time being one of these layers seems to be a moment in history where fortfication was necessary for several reasons. The first being a strategic military move and the second being that the infastructure within rome was too vast to maintain. I love what you have written about The Fall of the Rebel Angels. I feel like at this time the people began to look into themselves very critically. Later of course came the renassiance. Incubation as it were to give rise to the likes of rafael micaelAngelo etc. Thanks Lebbeus
9. [Kenneth Howe Jones](http://www.poulsbohemian.com)
2.11.10 / 5pm


Thank you for this reminder of Bruegel's marvelous visons of the stage of human drama.  

Oppressive myths of globilation are indeed medieval in their nature. Let us muster the courage to allow anti-hierarchy to blossom on our own dismal stage.
