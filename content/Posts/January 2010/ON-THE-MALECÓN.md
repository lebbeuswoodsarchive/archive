
---
title: ON THE MALECÓN
date: 2010-01-01 00:00:00
---

# ON THE MALECÓN


[![](media/malecon-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/malecon-1.jpg)


(above) *El Malecón de la Habana*—The Malecón—in good weather. The sea-wall on The Malecón on a typical day:


[![](media/malecon-2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/malecon-2.jpg)


Along the north side of Havana, facing the Caribbean and the often-cited ninety miles to the barren Florida keys, is a long road bordering the sea. This is The Malecón. It is both a lively and forbidding public space—the former because the sea sends waves crashing against a sheer seawall footed on black rocks below, and people come there to walk or sit on the wall to watch and feel and taste the sea—the latter because it is primarily a space for automobiles. Granted that there are not so many because of a perennial shortage of gasoline, but this doesn't alter the character of the space: it is a bare, inhospitable roadway separating the city and its people from the pleasures of the warm tropical waters. Granted, as well, that there is no natural beach along this stretch and the land itself drops abruptly to some depth beneath the surging water; and also, as a result, that the water is too turbulent for swimming. Still, it seems that something could be done to make The Malecón a more hospitable space next to the sea for the people of the city.


There is another feature of The Malecón to be considered. When hurricanes move across the island or the sea, the sea-level rises and the waves become larger and more violent, often flooding The Malecón and its neighborhoods. Every fifty years or so, there is a particularly large storm with winds that drive the sea in a massive storm-surge that floods many lower-lying parts of the city, including those such as *la Habana Vieja*, the old Spanish colonial city, housing poorer people—a situation similar to the Ninth Ward in New Orleans, which was inundated by a storm surge from a fifty-year storm—Hurricane Katrina—in 2005. The Cuban government has called for engineering proposals to protect against such catastrophic events. However, because of the nation's depressed economy—the result of following Soviet Bloc models as well as the fifty-year trade embargo by the United States—the money to pay for solutions on the scale of, say, The Netherlands [Delta Works](http://en.wikipedia.org/wiki/Delta_Works) is not available nor likely to be in the near future. Havana remains vulnerable.


In spite of the economic barriers to realizing at the present time any large-scale projects, new proposals need to be made. One cannot simply shrug at the impossibility and walk away from the problems. Political situations are changing, and with them the prospects for international cooperation in solving Cuba's urgent human needs, including flood control for Havana.


It has seemed to me that the problem of making public space on The Malecón and the construction of a barrier to destructive storm-surges could be solved by a single project, which I have proposed and which is shown in schematic form below. Constructed on the land—instead of in the sea, as many proposals have envisioned—it could be realized as a labor-intensive rather than capital-intensive project. Even so, it would require a determined commitment not only by the government, but also by the people of the city. The problems to be solved are large and extensive and can only be addressed by large and extensive proposals. Halfway measures and symbolic gestures are worse than inadequate—they waste both resources and the hope of actually improving conditions.


In brief, the proposal calls for the construction of a terrace along the six-kilometers of The Malecón, which is dedicated to public recreation, a type of urban park or abstract beach cantilevered from The Malecón over the sea. Shelter from the sun is built into the layers of each structurally independent terrace segment, which rests on heavy steel hinges such as those used for modern drawbridges. The independent segments can be built one at a time, and also shaped to the curving line of the roadway, so that their ensemble is continuous. Most importantly the segments can be designed to accommodate differing recreational needs along its six-kilometer extent; for example, a set of terrace segments serving one neighborhood could be very open, while another, serving a different neighborhood, could provide more layers of shelter and semi-enclosed spaces. The schematic design shown here only begins to suggest the possible variations.


When the weather turns bad and a passing hurricane raises the sea-level and sends a storm surge of water onto the shore, the force of this surge activates a ballast system within the terrace segments and rotates them on their hinges into a near-vertical position, creating a strong, high seawall that holds back the surging water. When the storm passes and the water recedes, the segments rotate back into their original positions. The idea is that the sea itself initiates and provides the energy for operating the system—no added mechanical systems to install and maintain—a sustainable system, in the contemporary sense.


Section through the six-kilometer long terrace, cantilevered over the sea—an artificial beach dedicated, in good weather, to public play and recreation:


[![](media/lwb-havmal-2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-2.jpg)


Model section showing the steel structure of a terrace segment and bridge-type steel hinges:


[![](media/lwb-havmal-7.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-7.jpg)


Plan of several terrace segments:


[![](media/lwb-havmal-3.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-3.jpg)


[![](media/lwb-havmal-111-e1262280271907.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-111.jpg)


Section through terrace segments at normal sea levels:


[![](media/lwb-havmal-62.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-62.jpg)


Typical terrace (looking back at the city):


[![](media/lwb-havmal-11.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-11.jpg)


[![](media/lwb-havmal-63.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-63.jpg)


The Malecón during a hurricane—not yet the fifty-year storm surge. Note the height of the sea, and the beginning of flooding:


[![](media/malecon-31.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/malecon-31.jpg)


The force of the waves rotates the terrace segments all along The Malecón into a seawall position:


[![](media/lwb-havmal-4.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-4.jpg)


[![](media/lwb-havmal-82-e1262283461478.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-82.jpg)


Section through terrace segments in seawall position during a storm surge:


[![](media/lwb-havmal-10.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-10.jpg)


[![](media/lwb-havmal-5.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-5.jpg)


View of terrace segments in seawall position (looking back to the city):


[![](media/lwb-havmal-12.jpg)](https://lebbeuswoods.files.wordpress.com/2009/12/lwb-havmal-12.jpg)


Note: Model designed and built in collaboration with Ekkehard Rehfeld.


LW



 ## Comments 
1. [vanderleun](http://americandigest.org)
1.1.10 / 11pm


“In spite of the economic barriers to realizing at the present time any large-scale projects, ”


Decades lost never to be recovered. What sort of “economic barrier” do you suppose was responsible for that? Care to give it a name?
3. [Joshua Ray](http://www.thursdaycity.com)
1.2.10 / 12am


This is a truly brilliant and elegant solution, which means it is virtually doomed to never happen. 


Of course, as you say in your post, the inevitable is no reason to stymie morale. The inevitability of death, for instance, to one with a true ethos of courage and action, only enriches the holy and sacred nature of life. 


May fortune smile on this worthy effort.
5. pedro
1.2.10 / 9am


Will be interesting to this second barrier in the urban landscape of the city and the implication of the people with the project. 


Your works for this city are really good, thanks a lot for those projects to the city. Is a shame that the cuban architects do not understand them. 


What was the inspiration for meta-institute? Any comment?


Is necesary for the development of the architecture in this country another “meeting” like Havana Project.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.3.10 / 3pm
	
	
	pedro: You should look at the post [META-INSTITUTES](https://lebbeuswoods.wordpress.com/2009/08/01/meta-institutes/) for an answer to your first question. The state of Cuban architecture has been affected by the the type of government that has ruled for fifty years and its policies. But even in the U.S. where architects have much more freedom, the state of architecture is far from ideal, or even as good as it might be. I'm not sure American and European architects have much to inspire Cuban architects with today, or that they set an especially bright example. The fault (to paraphrase Shakespeare) is not with governments, but with architects, ourselves. [See my comment to Paul on [STORM WATCH](https://lebbeuswoods.wordpress.com/2009/12/19/storm-watch/).]
7. Alex
1.3.10 / 11pm


Its really enjoyable to see a project focused on a real need, both social and physical, and which genuinely wants to improve peoples lives. Through it you have not only brought attention to an issue I was not aware of but provided a solution which is sensative and beautiful. I feel we need more projects like this to reach realisation, this is what architecture should be about.


As always I find your project highly captivating, however the question I want to ask is more mundane, and not associated with the theory of your work. The model you have made is beatifully crafted and I was wondering what materials you used to make it?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.4.10 / 2am
	
	
	Alex: The model is made entirely of chipboard (fine cardboard), spray-painted.
9. namhenderson
1.7.10 / 3am


Ah Lebbeus. Thanks for sharing. I first saw these (at least some of these) images in the book The Havana Project: Architecture Again, which I read for the first time about a year ago and loved. Particularly this project. Mainly because of it's infrastructural and multi-use nature.


Plus, such a large hinge…
11. [re:place Magazine](http://regardingplace.com/?p=6955)
1.7.10 / 3pm


[…] Mining legacy a challenge for Ladysmith, British Columbia [Journal of Commerce] INTERNATIONAL On the Malecon [Lebbeus Woods] Remnants of the Biosphere [BLDG BLOG] More Underground Art [Price Tags] For Cape […]
13. pedro
1.8.10 / 9am


Today I was speaking with a friend about your project for Old Havana, is incredible, at the end your project open the eyes to all this proarchitecture we have here, and we was comparing your work to some “stararchitects”. One project by Lebbeus Wood do more than 3 buildings by any of the “stars”, in ours days is more imporatant say something to do something. Is incredible how a building builted and finished(good architecture of course) can do less than a word, the language control us, and lebbeus thanks for convert my work into other even more outstanding thing. Others can inspire us, the point is who inspire us and how we let inspire by. Even Shakespeare can.
15. Gustavo
1.11.10 / 8pm


I must say that the proposition was a delightful experience. Its wonderful to see ideas like the one above in a world where architects must be concern of environments such as The Malecon. The first time I read this blog I instantly fell in love with it (in matter of fact I even starred at the pictures for hours memorized by its brilliant idea).  

Though I would like to know if other propositions like yours Lebbeus were ever proposed and if there are any books in which you can recommend on similar topics like the above?
17. Steven Harris
1.16.10 / 1am


One point wasn't clear to me: It looks as though people can walk onto the terrace from the city side when they are in the level, unencumbered position. As the seawater flows over the far edge of the lever, the edge on the city side raises, like a seesaw.


Any people standing on or near that edge on the city side would be in danger, either of being lifted up or dropped down suddenly as the lever flaps, or, worse, being crushed under its edge as it slams back down.


Does you design consider some way to block access to the terraces when there is some chance of them lifting up — or coming back down?


If it wasn't an amazing idea, I wouldn't bother to ask.
19. [On the Malecón « Techne](http://jstall.wordpress.com/2010/01/19/on-the-malecon/)
1.19.10 / 8pm


[…] the full blog go to: ON THE MALECÓN « LEBBEUS WOODS. El Malecón de la Habana—The Malecón Heavy waves crash against the sheer […]
21. [Nick](http://www.archinect.com/schoolblog/entry.php?id=84723_0_39_0_C)
1.20.10 / 12am


I was on the Malecon about a year ago today. While I did not witness any storms, I can see how this proposal would be a great infrastructural amenity to the Habaneros. And since it wasn't stormy, there was a constant stream of people out there at night, though it was fairly desolate in the late afternoon. This desolation made it my favorite place in the city–a place to escape the throbbing madness within.


There are a number of places where I saw people fishing and swimming by the rocks–so I suppose this wall wouldn't go up uninterrupted along the length of the Malecon because you wouldn't want to inhibit that access.


Great stuff, I'll be showing it to my 1st year architecture students tomorrow!


Regards.
23. anette brunsvig
3.5.10 / 12pm


Dear Lebbeus Woods  

I am so happy very soon to go to Havana with your project in mind and with the memory of Svein Tønsager, who so much wanted to go there but never reached it.  

The way you show what architecture can be about and with the relations to our loved ones in mind what more can you……….  

I hope that you and your family is well and give you all my best.  

Anette Brunsvig
25. [alejandrodiazbarrios](http://alejandrodiazbarrios.wordpress.com)
3.7.10 / 3pm


Yes, it gets terrifying along the malecón in Havana when the sea gets angry. I have experienced it as I have lived there for fifteen years.  

Do you truly wish this project was considered to be built and the government would allow it in Havana? Have you attempted to show Cuban government your ideal proposal?  

Support could be found; example: Miami, Florida. Benevolent intents could also be arranged with Cuba. there's been many changes in Cuba's government lately, things are rather a changing status. The United Nations also help many poor countries improve in what they can. Also Spain has been very helpful to Cuba and so has China, they might be willing to contribute with their help.  

Regardless, this project is very fascinating to not be constructed, no matter what the political restrictions are, it can be seen beyond that. The people in Havana would be grateful to have such great architect solve a priority problem as this one.


Let's solve the problem even more now by taking it to a next level.


A





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.8.10 / 12pm
	
	
	A: I have had no direct contact with the Cuban government—in fact, as a citizen of the USA i am prohibited by law from doing so (the embargo on all ‘trade' by the US government). This government has already charged me with ‘trading with the enemy,' for going to Havana for the 1994 conference, entirely sponsored by the Austrian government. The case is still pending.
	
	
	Anyone can present the idea to the Cubans, so long as I am given due credit for the ideas and initial designs represented in my models and drawings—which is one reason I have published them, that is, put them into the public domain.
	
	
	
	
	
		1. Alejandro
		3.10.10 / 1am
		
		
		Clarification question; the US charged you for ‘trading with the enemy,' for going to Havana for the 1994 conference, or was it the Cubans?
		
		
		If existing, would you recommend me any other documents related to this project, the cuban government call for engineering help or any further information that supports or influences this proposal?
		
		
		very interested,
		
		
		Alejandro
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		3.10.10 / 1am
		
		
		Alejandro: I am charged by the US government. I will look into the documents you mention, with the Austrians.
27. [waterproof public space « WeWasteTime](http://wewastetime.wordpress.com/2010/07/16/waterproof-public-space/)
7.16.10 / 5pm


[…] public space/water barrier proposal by Lebbeus Woods in the area of  Malecón in Habana. The proposal consist of a series of independent terrace […]
29. Bong
10.27.10 / 11pm


Hi Lebbeus,


I am really obsessed with your drawings and models … particularly the greyboard models. Are you just trying to stick on a single material? Though it seems quite complicated to realized some of your detailed mechanism on drawing by using cards.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.28.10 / 4am
	
	
	Bong: I'm not quite sure what you are asking me. Grey chipboard (as we call it) is a wonderful, strangely precise material. I built these models in tandem with my colleague, Ekkehard Rehfeld, with me doing the finely articulated parts of the ‘beach' landscape—the model was definitely a design tool. I also did the photography, so—as you can see—I was as fully engaged with the models as I was with the drawings.
31. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thought: a Dialogue with Lebbeus Woods - Part II](http://www.cluster.eu/2010/12/03/architecture-as-the-solid-state-of-thought-a-dialogue-with-lebbeus-woods-part-ii/)
12.3.10 / 8am


[…] city of La Habana (Model by Lebbeus Woods in collaboration with Ekkerard Refeldt), image taken from lebbeuswoods.wordpress.com/ I have always been driven in my work by a few questions, a handful of ideas, having to do with the […]
