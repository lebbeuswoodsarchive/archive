
---
title: EARTHQUAKE (again)!
date: 2010-01-13 00:00:00
---

# EARTHQUAKE (again)!


[![](media/lwb-eq-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-eq-1a.jpg)


*In the wake of the earthquake in Port-au-Prince, Haiti, I feel obliged to post this brief essay, written nearly fifteen years ago. When are we going to get seriously to work on this problem?* 


Earthquakes are the result of natural, tectonic changes in the solid crust of the earth and, as such, are not inherently catastrophic. Their bad reputation comes from the destruction to human settlements that accompanies them, when buildings collapse under the stress of forces produced by earthquakes. This destruction is not the ‘fault' of earthquakes, but rather of the buildings, which, even in regions regularly visited by earthquakes, are not designed to work harmoniously with the violent forces periodically released. So buildings collapse, usually with considerable loss of life and injuries. The earthquakes are blamed, as though the purpose of these sublimely unself-conscious phenomena was to damage and destroy the human. “Earthquake Kills Thousands!” “Killer Quake Strikes!” “Earthquake Levels Town!” are typical aftermath headlines. What they should say is “Falling Buildings Kill Thousands!” “Killer Buildings Strike!” “Inadequately Designed Town Leveled!”


Such headlines will not, of course, appear. If they did, architects, town-planners, engineers, and the entire army of professionals responsible for the design, construction, and maintenance of the affected buildings would be called to account. If that were to happen, they would certainly implicate politicians, developers, banks and the entire coterie of private and public officials controlling what gets built and where, the financial/economic community that finds it more profitable to rebuild what has been destroyed than to commission the development of architectures that would work with earthquakes and thus survive, even benefit from them. If this profit-driven community were called to account by public outcry, it would almost certainly turn the blame back on the public itself. After all, corporations and government are under constant pressure to give the public what it wants, which today means the same products, the same lifestyles, the same buildings and types of buildings to be found anywhere on the planet, regardless of the planet's extremely varied processes of transformation. If all these individuals and social institutions were held responsible for the destruction caused by earthquakes, then the public in earthquake regions would have no choice but to demand radical changes of them. But this would be an expensive revolution, one that all the interests involved could afford only at great cost to their reputations, knowledge and technical expertise, and to their present economic prosperity.


There is, however, a deeper structure of resistance to investing in the invention of new architectures of and for earthquake, and this is formed by the most venerable beliefs about the relationship between the human and natural worlds, which are considered to be hostile to one another. The ‘Man versus Nature' attitude begins in the founding stories of some of the world's dominant religions. Christianity, Judaism and Islam share the Biblical account of the expulsion of Adam and Eve from the Garden of Eden, which came as a result of their desire for self-knowledge and, thereby, for independence from the rest of the unself-aware, wholly interdependent world. Many philosophers over the past epochs have rationalized this belief, but it was Rene Descartes who best codified it for the modern era. His philosophy postulates an essential duality of the world, comprised of the human and the Divine, which cannot be bridged. Not coincidentally, he also invented a mathematical system—analytical geometry—that organizes the spatial and temporal properties of the human domain with great efficiency. Cartesian logic and geometry offer a pragmatic usefulness that shows no signs of diminishing, more than three hundred years after their inception, and in spite of immense cultural and technological changes to the society they serve. But, while Cartesian thought and method succeeded in freeing science, and therefore technology, from the grip of religion per se, it maintained the adversarial Biblical relationship between the domain of the human and the realm of ‘Divine' nature. Nowhere is its fragility in this regard more clearly demonstrated than in earthquake regions. There, not only has the idea of the Cartesian ‘grid' as a symbol of rational efficiency and stability been overturned (literally) by the nature of earthquake forces, but the civilizational cornerstone of human independence from Nature (a conceit, however transparent, that has propelled the notion of human progress) has been broken to bits. In light of the consistent failure of leading societies such as the United States and Japan to build in ways that work with earthquake, it is reasonable to begin to reconsider the dominant philosophies, techniques and goals of building and urban design in earthquake regions.


While today much attention is paid to ideas of ‘green architecture' and ‘sustainability,' a reconsideration by architects and planners of the forms and methods of architecture in seismic regions has hardly begun. Most needed now are new ideas and approaches that go beyond the defensive reinforcement of existing conceptual and physical structures and open up genuinely new possibilities for architecture integrating earth's continuing processes of transformation. Once new spatial models that extend the forces of earthquake into the dynamics of private and social life have been proposed, we can look forward to a post-Biblical reconciliation of the human and the natural. That will be a real beginning.


LW


From the New York Times, a sign of hope that attitudes can and are beginning to change:


<http://www.nytimes.com/2010/01/14/world/americas/14construction.html?hp>



 ## Comments 
1. [cinco dias » O que mata? Terramotos ou edifícios? A acção da Natureza ou a acção do Homem?](http://5dias.net/2010/01/14/o-que-mata-terramotos-ou-edificios-a-accao-da-natureza-ou-a-accao-do-homem/)
1.14.10 / 1pm


[…] O que mata? Terramotos ou edifícios? A acção da Natureza ou a acção do Homem? 14 de Janeiro de 2010 por Tiago Mota Saraiva Earthquakes are the result of natural, tectonic changes in the solid crust of the earth and, as such… […]
3. David
1.14.10 / 7pm


What about the fact that in countries that actually have an “entire army of professionals responsible for the design, construction, and maintenance” injury and death from earthquakes is minimal?  

The problem isn't that architects and engineers aren't blamed – it's that there aren't architects or engineers in many cases. To be honest, I can't think of any way to shift blame for drastically to the architects and engineers than getting hit with a career-destroying lawsuit!  

“…consistent failure of leading societies such as the United States and Japan to build in ways that work with earthquake …”  

The 1989 Loma Prieta earthquake (6.9 on the Richter scale) killed 63 people out of a population of over 7 million. That means less than 1 in a million people were killed. The 2003 Bam, Iran earthquake (6.5 Richter) killed 26,000 of the 43,000 residents. That is 3 out of five.  

I think it is somewhat ingenuous (and dangerous) to claim that rejecting Cartesian thinking will somehow lead to greater safety and humanity.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.14.10 / 10pm
	
	
	David: So what are you saying? Everything is OK? Architects have done all they can? 
	
	
	If so, then I disagree. Rich countries like the U.S. and Japan can afford to do much more research into advanced forms of design for earthquakes that will benefit poor countries like Haiti, and they have a moral obligation to do so. If, by comparison, there is going to be a cure for AIDS, then it will most likely come from one of the rich countries, not from the poorer countries of Africa, where the disease has taken its most terrible toll.
	
	
	It is clear that in our ‘free-market' social system, responsibility for much of the needed architectural research must be accepted by ‘private' architects. Until they recognize their responsibility—and they haven't so far—not much will happen. Like all forms of research into difficult problems, much collaboration will be needed. Architects are responsible for that, too.
	
	
	I don't propose rejecting Cartesian thinking. What I am saying is that Cartesian ‘duality'—the absolute division between mind and body, the Human and the Divine—causes a no longer useful conceptual division of the human and the natural. Also, the rectilinear Cartesian grid and right-angled structural connections fail too easily under the oscillating lateral forces produced by earthquakes. It is therefore necessary to consider, in seismically active regions, other spatial and structural configurations. 
	
	
	A comment on your facts: The epicenter of the 7.0 (Richter Scale) Loma Prieta earthquake happened far south of San Francisco, in an unpopulated area near Santa Cruz. This mitigated the effects in the Bay Area.
5. [regarding Haiti… « laurence turner](http://laurenceturner.wordpress.com/2010/01/14/regarding-haiti/)
1.14.10 / 8pm


[…] If you would like to prevent things of this nature happening in the future, then please vote for people who will not foster colonialism around the globe, nor buy products from companies which exploit people and the environment. It would also be helpful if you could press for changes in building codes so as to prevent the construction of inadequate and unsafe buildings in zones subject to seismic activity. […]
7. aitraaz
1.18.10 / 10am


Convincing…From a DeLanda piece, Mondo 2000, a few years back:


“We truly need a complete new thing, and A Thousand Plateaus is the direction. Those guys are 50 or 60 years ahead of everybody else.


You read it at first and you think you're reading poetry: “Metals are the conciousness of the planet.” Get out of here, what the fuck is that?


The you read about metallic catalysts, how in a way they are like probing heads that unconciously accelerate certain reactions and decelerate certain others. They allow the exploration of an abstract chemical space by probing and groping in the dark. And you realize those two are right.


They're already going into future technology: how would you put together a bird song with a wind with an Indian chant and make a machine out of those three? I don't bother with their disciplines, who think that all you have to do is repeat words like ‘deterritorialization' and ‘binary machines' like little mantras, without really understanding that those guys are engineers – engineers of the year 2035. I don't know how they derive their knowledge. They must trip.'
9. Eimear
2.8.10 / 12pm


“But, while Cartesian thought and method succeeded in freeing science, and therefore technology, from the grip of religion per se, it maintained the adversarial Biblical relationship between the domain of the human and the realm of ‘Divine' nature.”


-could you elaborate on this please? Could you explain why you think Cartesian thought definitely maintained this adverse relationship.


I am not sure whether I agree or disagree, and would like to hear your thoughts
11. [Kenneth Howe Jones](http://www.poulsbohemian.com)
2.10.10 / 9pm


Leb: I remember reading this. Didn't you also include it or something much like it in one of your monographs? Every word resonates today. Is there a competent dialogue of truly creative architectural thinking going on anywhere … let alone in the schools? Is there any excuse for architects not to design structures suitable to celebrate the forces of nature … earthquake included? I think not. The talent exists but where is the will?
13. [don't let force pile « Thought Shop](http://thoughtstop.wordpress.com/2010/01/26/dont-let-force-pile/)
3.6.10 / 7pm


[…] Mason White at Wired's Haiti initiative notices Lebbeus Woods, an experimental architect based in New York, revealing that earthquakes are not at fault in the destruction of settlements: […]
15. [don't let force pile « syncwpmu](http://syncwpmu.wordpress.com/2010/01/26/dont-let-force-pile/)
3.7.10 / 6am


[…] Mason White at Wired's Haiti initiative notices Lebbeus Woods, an experimental architect based in New York, revealing that earthquakes are not at fault in the destruction of settlements: […]
