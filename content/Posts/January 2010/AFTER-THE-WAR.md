
---
title: AFTER THE WAR
date: 2010-01-20 00:00:00
---

# AFTER THE WAR


[![](media/lwblog-wa5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa5.jpg)


“Even the Great Cathedral suffered some damage, but was so important that it was restored to its remembered form. This gave many people comfort, because it was a symbol of continuity in a city that had been so transformed.


[![](media/lwblog-wa2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa2.jpg)


Everywhere along the streets, buildings that after the war had gaping holes in them had been rebuilt, but in a strange new way. First of all, people could not always remember how the buildings once looked—they were just ordinary buildings. Also, people realized that their city had changed in other ways, too. They decided the new parts of the surviving buildings should belong to the present, not the past. And to the future, which could not be remembered.


[![](media/lwblog-wa7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa7.jpg)


The present, people realized, and therefore the future, had to be invented and made from things as they actually were in the city and in themselves. They were not the same people they once had been. Anyway, they could not afford to go back. They didn't have the resources to do so—they had lost too many things during the war.



[![](media/lwblog-wa22.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa22.jpg)


The Mayor of the city wanted to set a good example, so he rebuilt his house on some broken walls that were still strong. He was richer than most, so it stood alone near the center of the city.


[![](media/lwblog-wa20.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa20.jpg)


Most people could not afford new materials for rebuilding, so they decided to use whatever materials they could find. At first they thought their constructions looked primitive, like accumulations of junk. But, after a time, they taught themselves to build well in a new way.


[![](media/lwblog-wa21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa21.jpg)


Some structures simply covered up damaged parts of the older buildings, keeping the weather out while rebuilding was carried on within.


[![](media/lwblog-wa41.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa41.jpg)


Other structures, when they were complete, were placed so carefully within the damaged sections of the old buildings that they looked like they had always been there. This was reassuring to people who looked to their buildings for a sense of permanence and stability.


[![](media/lwblog-wa24.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa24.jpg)


Before the war, all the buildings had been similar. After the war, those that had to be partially rebuilt were each different. People said this was so because each building had had been damaged in unique ways and its residents had unique stories to tell of what they had experienced.


[![](media/lwblog-wa8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa8.jpg)


Rebuilding was a slow process. First the gaping holes had to be covered and then the space between new and old carefully filled in.


[![](media/lwblog-wa9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa9.jpg)


The materials people found were carefully shaped or reshaped, and precisely fitted together to form new walls and openings in them. When it was finished, a wall did not look like something old or haphazard, but brand new. It took a lot of hand labor, unlike before the war, when machines had done most of the hard work.


[![](media/lwblog-wa12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa12.jpg)


Inside the rebuilt structures were new and at first unfamiliar spaces. People could not move their old furniture—if they could find it—into them. But very soon they found new ways to inhabit the unfamiliar spaces that some thought were even better than before.


[![](media/lwblog-wa13.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwblog-wa13.jpg)


Of course, some things remained almost the same as they had always been.”


LW





 ## Comments 
1. river
1.20.10 / 7am


Good. Sorry to say. good just so. A children's story about a yesterday that has not yet been. “That!” is the exact nature of urbanization without latitude of forests. Oh! forests. you forests. Who planned such efficacy?


“People could not move their old furniture — if they could find it — into them.”


No. They had all went prolly to the good schoolz fur. Then gots 2 talking via lingua them (prolly enchante).
3. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2010/01/20/after-the-war/)
1.20.10 / 3pm


**Social comments and analytics for this post…**


This post was mentioned on Twitter by jackscuache: Lebbeus Woods tells a story about a post-war culture, through its architecture. Reads like a very short graphic novel. <http://bit.ly/8U750E>…
5. student
1.20.10 / 4pm


If you don't mind sharing, what kind of pens/paper did you use for these? 


Thanks





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.21.10 / 4pm
	
	
	student: very ordinary: “precise V7” and “flair” pens—regular bond paper.
7. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.20.10 / 4pm


Pragmatic soaked in romanticism…love the analogy, war, disasters, Haiti and Nature cleansing itself, ourselves.


Analogy Haiti: For the Haitian culture to thrive on the existing disarray of building materials, abundantly waiting, to be assessed and defined into a new kit of parts. The art of sequencing such an event that could provide immediate construction—gabions constructed of what is, organized in such ways to define and accept new programs of being, all the while accepting future shear forces. The creative challenges for a new beginning excitingly unfold.


Although, in the midst after such a cleanse and before any act can just be, it seems a massive congregation of leadership [governments intermixed with organizations] consuming all the remaining energy to plan, negotiate, re-plan, assess, to plan again—the overhead is endless—the art of entropy consumes…
9. Josh
1.21.10 / 1am


Instead of entropy, in terms of inevitable and steady deterioration of a system or society, which has obviously already happened in Haiti, would it be more appropriate to assimilate the potential energy used in this situation with something embryonic?


This sort of situation will happen again and again unless we try to understand what has happened and why, and search for new dialogues that may hold truly valued propositions. If we ignore this situation and treat it as any other, then and only then might we attach an idea of entropy…
11. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.21.10 / 2am


Well said Josh. However, I was referring to entropy as an analogy to the infinite number of solutions contrived and discussed by governments/organizations/planners…before any action would ever take place. Take Katrina for example. Whereas, “After the War”, poetically embraces people rebuilding their community with the remains from war. 


How many sketches, working drawings, and models would it take to put an addition on a residence? Perhaps just a living sketch, perhaps more to convince thyself that this is it, the one, the plan. Then onto working drawings, BIM for permits and of course fees for approval and inspectors to inspect. Life safety is a huge expensive hurdle. How with all of these rules, regulations, and I didn't even mention the “design review board” or an HOA approval. HA. Fiftyfold that process for a disaster or war situation. Lebbeus Woods has illustrated a graceful story board of what can be, but I ask how can it happen without the approval of the world? Unless all the powers to be were destroyed.


Granted Haiti does not have the luxury of this red tape, and I am sure some would say well if they did none of this would have happened as bad as it did. Disaster nonetheless and it will happen again.


Josh, I am not trying to ignore the situation, I am just comparing bureaucracy to an event that Lebbeus Woods so finely illustrated and merging it with Haiti.
13. Josh
1.21.10 / 4am


Michael\_


I absolutely agree with your thoughts.. 


I was in no way attempting to decipher your reasoning or undermine your thoughts… though it is a fact that these types of institutions and regulations are put in place for the safety and welfare of the general public. 


I can only hope to imagine a world that exists purely for the betterment of mankind. One that would focus its collaborative efforts on the importance of the connectivity between architecture, our environment and our life values. Lebbeus has only aided in my ability to reorient my thinking which provides me with at least the faintest glimmer of hope. Thank you





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.21.10 / 4pm
	
	
	Josh and Michael Phillip Pearce: Your exchange of thoughts is very important and I'm glad for it. 
	
	
	After the war “people realized that they needed to reorganize their government, because it had—one way or another—brought the war upon them. They wanted a better government, so they got together, in the same way they did to rebuild, and made a new government, one that served their interests in the inventive construction of their city and lives together. Of course, the old politicians resisted their efforts, but the people insisted.”
	
	
	From a fairy-tale or bedtime story? Usually—but not necessarily.
15. [Michael Phillip Pearce](http://www.carbon-vudu.us)
1.21.10 / 5pm


Thanks Josh, and I understand what you are saying/implying. 


To Lebbeus Woods I am grateful for your insight[s] and enjoy the scenario and how it unfolds.


To capitalize on your vision with current war[s] and disaster zones, construction could begin immediately if it already has not.


I was thinking last night with all the wars going on, one could easily skirt the governing. The people living in war is one thing, but to have leaders, perhaps militia leaders actually living, surviving and fighting in it is another.


Maybe the catalyst other than “After the War” is within the war, a creative engineer, a fighter/survivor team seeking protection—designing pure functionality contrived from the context—as Woods illustrated. Code: principles of similitude, improvise, adapt and overcome conditions, making this architectural style, again, as Woods depicts. Experimentation would be the governing force for safety. 


I am just stating the obvious but it is very interesting read…it seems as if war and nature are abundant in our times.
17. [Wall House « PROJECTIONS](http://prjctns.wordpress.com/2010/01/21/wall-house/)
1.21.10 / 6pm


[…] obviously, Mr. Woods always has something to say about the idea, an idea he's been studying for a long […]
19. [Justin](http://justinoaksford.blogspot.com)
1.25.10 / 7pm


incredibly graphic and simple, elegant and dark. Beautiful!
21. Christian Molick
1.25.10 / 11pm


I wonder if this specific quote might have been in some way an inspiration for this, either conscious or unconscious. In any case this is interesting to contrast with arguments against modernity in favor of traditional methodologies.


” … You have, ladies and gentlemen, to give this much to the Luftwaffe: when it knocked down our buildings, it didn't replace them with anything more offensive than rubble. We did that. … ”


[Speech by Charles, Prince of Wales before London Planning Committee](http://www.princeofwales.gov.uk/speechesandarticles/a_speech_by_hrh_the_prince_of_wales_at_the_corporation_of_lo_161336965.html)
23. [# HETEROTOPIC ARCHITECTURES /// After the war by Lebbeus Woods | The Funambulist](http://thefunambulist.net/2010/12/22/heterotopic-architectures-after-the-war-by-lebbeus-woods/)
12.22.10 / 11pm


[…] Lebbeus Woods. After the war This entry was posted in Architectural Projects, Heterotopic Architectures. Bookmark the permalink. ← # GREAT SPECULATIONS /// Une architecture des humeurs. Exhibition by R&Sie(n) LikeBe the first to like this post. […]
