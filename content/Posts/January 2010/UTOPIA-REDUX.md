
---
title: UTOPIA REDUX
date: 2010-01-09 00:00:00
---

# UTOPIA REDUX


[![](media/lwb-utop-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-utop-11.jpg)


Daniel Meridor's montages place us somewhere between hope and despair. Paused in time, we cannot be sure whether events are moving from a troubled world towards a more harmonious, serene one or the other way around. Or, whether he is depicting a world in which progress and decay, as well as technology and nature, coexist in a fragile, uncertain balance. Is this latter case the world of the present? We can hope it is, but despair at the thought that perhaps our civilization—now embracing the globe— cannot maintain its delicate balance.


Meridor's generation—a younger one—has no faith in grand architectural plans to make a better world and especially not a best of all possible worlds. Yet a certain idealism remains. For some it takes the form of environmental consciousness and sometimes grand plans for healing the damage done to the planet by humankind—or, for more modest, ecologically sustainable designs. For others it takes the form of technological innovation using computers, the internet, and other electronic wonders. For only a relative few, however, does their idealism take the forms of isolated buildings that are the focus of the present generation of innovators—radical ‘new forms' fail to inspire as they once did. The idealistic architects of Meridor's generation are more critical and reflective than any in recent memory, which leads them to an emphasis on process rather than product, and into either direct engagement with communities in need or into teaching careers, which are not altogether unrelated activities.


Still, at the end of the day, architecture must concern itself with the design of spaces for people to live in. Important as criticism and reflectivity may be, they are not in themselves enough. Rather, they need to inform action, or they become academic. It is easy to see Meridor's montages as social critiques—the detritus of industrial civilization encroaching on nature—or, as harbingers of technological salvation—the windmills as alternate, non-polluting sources of energy. However, they become much more interesting, and fruitful, if we see them as architectural proposals, that is, as designs for the present and perhaps, for a better future.


First of all, thinking about process, we have not seen much use of photomontage as a design tool since the work of the Russian Constructivists. the Bauhaus and, somewhat more recently, Archigram. It has the immediate advantage of employing the familiar and, by selection and rearrangement, transforming it into the new. At the very least, this enables us to see new potential in the existing and obviates the need to begin—in the usual utopian sense—from scratch. As a design technique, its weakness is that, in actuality, it is clearly easier, if not always better, to begin from a tabula rasa than to somehow work with what is already in place. On the other hand, if we think of a design drawing heuristically, as a guide and not an instruction, even the most descriptive drawing can be seen as only one example of what a particular way of thinking would produce in reality. Other examples remain to be invented in response to particular conditions and by different architects. The design drawing seen this way indicates a process, a way of thinking, and not a product.


So, for example, the piles of debris in Meridor's montages indicate the importance of stochastic domains, governed by chance and probability, within well understood—or at least in presumably well understood—natural and designed landscapes. Another example: the floating technological objects indicate the need for the ubiquitous intervention of elements of unknown purpose, encouraging discovery, change, and the invention of new and experimental modes of living. Another: the treehouse invokes the times when, as children, many of us loved to climb trees. It was not an aggressive act, an attempt to conquer the tree or to control it, but rather a cooperation with a chosen tree, an acceptance of its offering of a ladder for us to reach new heights. In doing so, we did not destroy the tree, but fulfilled one aspect of its presence within the world we both inhabit. Is there a more poetic—or finally practical—idea of our creative interaction with nature?


The whole design of the montage and the landscape it envisions is governed by a complex visual sensibility, evoking—even celebrating—a tolerance for clear differences uncompromised by coexistence with one another. The mission of architecture, these designs suggest, cannot dispense with the aesthetic, for it is inseparable from the ethical, the way we choose to live with all other things. And it extends far beyond property lines and the enclosure walls of buildings. This is almost, well, utopian.


LW


[![](media/lwb-utop-51.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-utop-51.jpg)


[![](media/lwb-utop-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-utop-31.jpg)


[![](media/lwb-utop-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-utop-21.jpg)


[![](media/lwb-utop-41.jpg)](https://lebbeuswoods.files.wordpress.com/2010/01/lwb-utop-41.jpg)


Daniel Meridor is an architect who has worked closely with Bedouins in Israel, helping to negotiate the Bedouins' nomadic ways of living with contemporary Israeli society. He is currently studying in the Graduate School of Architecture of The Cooper Union, in New York City.



 ## Comments 
1. [mu](http://muwang.wordpress.com)
1.10.10 / 10am


Dear Lebbeus Woods


I'm an architecture student in UK.


Just want to say your work and articles have been so inspiring and they really kept my faith when disapproving eye shows around.


Here sending my best regards from icy snowing london.


Thank you so much.
3. [c.smith](http://pureaesthetic.tumblr.com/)
1.10.10 / 7pm


*almost* utopian. *almost*. The fragile equilibrium evoked echoes scarcity and disaster. There's more οὐτόπος than εὖτόπος to what I see in these. 


Still the photomontage technique affords a honesty to a cultural and human based vision. There's care and compassion. I'd like to understand this kind of idealism and find an honest process to any vision.


Also:  

*into either direct engagement with communities in need or into teaching careers, which are not altogether unrelated activities.*  

i second that sentiment.





	1. Sophie
	1.10.10 / 8pm
	
	
	the realization of a Utopia is that it is never complete
	
	
	
	
	
		1. [c. smith](http://pureaethetic.tumblr.com)
		1.13.10 / 12am
		
		
		well put though i think the ideas are a bit more complex.
5. Sophie
1.10.10 / 8pm


What a beautiful and insightful reading of these microcosms of openings!  

I especially enjoyed your comment on the tree house and the realization of our co-existance with nature and the mutual ability to inspire new heights.


Meridor's captivating moments evoke a sense that one is looking into an alternate present – tense with an unknown yet perturbing nostalgic familiarity.  

As if a window has been wiped of its years of grime, and through its new vantage point, a viewer can see a mirror image of their own world, that they may have forgotten along the way, put together in piles of amounting ideas, ideals, imaginations, imitations and investigations, that have combusted and returned to a state of genesis.  

Meridor has captured that split-second moment between destruction and creation, shrouded in a vast silence, yet with an opening for a flickering of endless possibilities to be tangible once again, comforting.  

In their state of delicate imbalances, the images ASK A QUESTION. They do not aim to dictate a definite answer.  

This shifts the assumed control the creator of these montages has (in the very act of making them) on the state or outcome of the proposed landscapes, to the viewer, who can choose one of the infinite strings that are attached to the future. A new montage is then created by the collective visions of those who approach these erased canvases.


There is a humility in the choice of medium in that it takes from the existing, respecting the role the used images have played in creating the new. As a starting point of the process you talk of, here is no attempt to boast of a newer, more advanced way of thinking or creating, which in itself is refreshing, and allows for real, productive growth.


Thank you for a succulent visual and intellectual experience!


Sophie
7. Francesco.F.
1.10.10 / 10pm


These images resonate with a specific moment: The moment of peace after human destruction. On a personal level it might be a fight with close ones, or collectively – it might be a war. Each image contains within it an uncovered potential after these accelerated peaks have been reached, it seems that each is hovering quietly, ambushing for this moment, in order to allow a new balance to occur, this time hoping that it will not be broken easily.  

I do agree that the choice of montage is evocative and allows  

an immediate portal into these places whether physically or ideologically, and though it is ambiguous whether this is a moment of decadence or reordering of the old, there is a strong atmosphere of peace. A quiet equilibrium which does not impose one's dream but allows any individual a search within it: technology, nature and human dwelling together – a peaceful dream.


my questions would be (at the risk of being literal) how will these montages look like in a densely populated zones (if they still exist in this Utopian world) or have these zones already disappear in such a world? who are the inhabitants of these montages? are they survivors of some kind?
9. [organicmobb](http://organicmobb.wordpress.com)
1.11.10 / 6am


Lebbeus, thank you.
11. [signe schmidt](http://justflowernotfruit.blogspot.com)
1.11.10 / 9pm


Dear Lebbeus Woods.  

I read you text about Daniel Meridor with great interest.  

I'm a student at the royal danish academy of art, working in the field of architecture and theory.  

In the end of february, me and a friend will travel to israel, on a holiday and study-trip.  

it would be of interest for us, to know more about Daniel Meridors project. Could you provide contact to him?  

Thanks for your perspective, it is needed.  

Best,  

[Signeschmidthansen@gmail.com](mailto:Signeschmidthansen@gmail.com)
13. [“utopia redux” - mammoth // building nothing out of something](http://m.ammoth.us/blog/2010/01/utopia-redux/)
1.11.10 / 9pm


[…] Woods has a fantastic piece, “Utopia Redux”, on the collages of Daniel Meridor, a student at the Cooper Union; the second paragraph, in […]
15. ArchForm
1.13.10 / 10pm


extremly sensitive and beautiful!!
17. [Collaging Idealism » Life](http://areadeefecto.com/life/archives/70)
1.15.10 / 6pm


[…] UTOPIA REDUX […]
19. J Ingold
1.16.10 / 4am


These images use infrastructure to date the content. Even in his almost post-apocalyptic projections, Meridor seems bound to(or maybe in question of?) this modernist/futurist paradigm of infrastructure as the dominant built landscape. At what point does this “more modest, ecologically sustainable design”  

reverse the momentum of Sant'Elia's model of infrastructure-as-exclamation-point to one of infrastructural silence through a pragmatic and multifarious meeting of needs?  

 Second, Meridor seems keenly aware of the transition from (quite literally) a concrete hub of anthropocentric activity to a more transitory/migratory condition that appears to be represented through the use of mechanisms that signal the ever-present fluid reality of our environment (i.e. airborne bouys, windmills, streams, bridges over oceans of sand, erosion) We seem to be fighting that tide(pun intended) as object makers when posed with the problem of information technology and the immateriality of an increasing portion of what is needed to meet the needs/means of production as understood by the standards of the last century. Ideas of permanence run headlong into problems of obsolescence when we consider architecture as interface and, therefore, hardware. How do we build-in fluidity in an environment where, while resources are scarce and lifestyles are increasingly less dependent on physical proximity to resources, the need for permanent provision of resources through mechanical and electrical means remains constant?
21. P. Garcon
1.31.10 / 1am


Meridor's photomontage is quite interesting. Thank you for posting this. It's as if past, present, and future are collapsed into a point. Not ignoring the devastation and pain caused by the past. And seeded with a future, with a color, and a hope not yet born. Maybe utopian in the form of humanity achieving some kind of viable balance point between its own development and the natural world. As Lebbeus writes, between despair and hope. (And between ground and sky).  

These do seem post-apocalyptic at first glance. But I suppose a time after chaos can present enormous opportunities for change on a large scale level. Maybe utopia cannot come out of our landscapes without a giant wrecking ball leaving much of what we have built in ruins. Maybe true hope cannot come until we have the feeling that a new way of integrating our existence into the natural world becomes possible. And our footprint in the natural world becomes smaller…  

Meridor's visions here are at first glance unsettling, but also provocative. After looking at these, there a sense of beauty and peace that is there…perhaps of harmony…that is unfolding beyond the initial reaction to the starkness of the landscape.  

Thank you again for posting this. Meridor's montages not only challenge our aesthetic notions of what utopia could look like. They also seems to challenge our ideas of what our contribution to a utopian landscape might look like in relation to nature and the ruins we leave behind, some of which we may not want to, or be able to, sweep under the rug.
23. [Black and White « Plastic Futures](http://liveness.org/plasticfutures/?p=2345)
2.3.10 / 5am


[…] and White posted by: Girish Sagaram on February 3, 2010, 4:22 pm Lebbeus Woods features a series of photomontages by student Daniel Meriador. They might depict a strange and […]
