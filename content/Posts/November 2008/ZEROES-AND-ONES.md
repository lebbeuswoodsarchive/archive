
---
title: ZEROES AND ONES
date: 2008-11-09 00:00:00
---

# ZEROES AND ONES


There is much talk today about computation in architecture, not only its implications for the design and production of tectonic objects—from chairs to buildings and cities—but also its inescapable philosophical consequences. Understandably, most of this talk, by a few theorists and many practitioners, centers on the digital computer and its capacity for rendering complexity and simulating reality. I say ‘understandably,' not because this is a proper focus for issues raised by computation, but because very powerful digital computers have become handy and accessible tools for everyone, so…why not use them?  Actually it is not the proper focus, especially if our interest is in philosophical domains such as aesthetics and ethics, and how architecture both embodies and enables them. An even more powerful and accessible computing tool—the human brain—should be our primary subject, and object, of understanding the nature and consequences of computation, for architecture and the world.


There are precise, historical reasons for this. It was advances in neuroscience, during the 30s and 40s of the last century, in understanding how the brain works as an electrical machine, a “biological computer,” that led to the rapid development of artificial, electronic computers. This advance—a leap, really—was prompted by dramatic discoveries, in the teens and 20s, in physics and mathematics, especially the invention of quantum mechanics and quantum theory. Niels Bohr's Copenhagen Interpretation had radical and profound consequences not only for epistemology, but also for every branch of inquiry and practice. It states that when we describe with scientific precision any phenomenon, we must include in the description the manner in which we observed the phenomenon. The human brain and wider nature were thenceforth intertwined and inseparable; the old barriers between the ‘subjective' and the ‘objective' were shattered and a new era, the present one, began. 


Architectural theorists and experimental practitioners would do well to give more attention to cognition theory, its origins and contemporary forms, when considering concepts of computation. One of the key concepts to come out of cognition theory is ‘self-referentiality,' having to do with the paradoxes created by the brain studying itself. Concepts such as ‘recursion' and ‘feedback,' ‘self-organization' and ‘autopoesis' are secondary consequences. Technological application, such as software design, communications networks, and their relevance for architecture—comes further down the line.


Indeed, anyone who wishes to understand the role of computation in human thought and activity must study the developments in this recent history, particularly in the field of cybernetics, which in the 50s and 60s, laid the theoretical foundations for contemporary cognition theory, general systems and information theories, which, in turn, underlay the rapid advances of computer technology. I close this post with a succinct essay by Heinz von Foerster, one of the founders of the transdisciplinary field of cybernetics. Hopefully, it whets your appetites for more.


 LW


“On Constructing A Reality” by Heinz von Foerster:


[![hvf20](media/hvf20.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf20.jpg)[![hvf211](media/hvf211.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf211.jpg)[![hvf221](media/hvf221.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf221.jpg)[![hvf23a](media/hvf23a.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf23a.jpg)[![hvf23b](media/hvf23b.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf23b.jpg)[![hvf24](media/hvf24.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf24.jpg)[![hvf25](media/hvf25.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf25.jpg)[![hvf261](media/hvf261.jpg)](https://lebbeuswoods.files.wordpress.com/2008/11/hvf261.jpg)


More on Heinz von Foerster and his colleagues:


<http://en.wikipedia.org/wiki/Heinz_von_Foerster>



 ## Comments 
1. [General](http://underconst.)
11.11.08 / 10am


During the spring and summer of 2008 I had been working on series of images that were termed ‘enatiadromic solipsistic'– so I find this post particularly interesting to this year's conscious discourse.  

As a rule, I have tried to keep the computer as far away from the original creative process as possible, and save it for polish and presentation when necessary. For one reason- the computer implies your title's digital logic– to where human creativity is exhaustingly/exponentially more advanced than its algorithmically coded brain-emulation. In context to the current of human civilization at both present and future, this notion is both reactive, rebellious, and in some ways archaic in nature– however, it is more natural- and thus closer to the creative source. I tend to think of the human brain not as the origin, and more of the conduit, of the creative human consciousness– a place of reflection, per se– for referential learning. The computer being humanity's way of materializing what we reflectively know about a fraction of ourselves. What really bothers me about computers is the interface with our anatomy, and the unhealthy circumference of physical interaction, as well as the ordained quasi-limitation of its creative possibilites (one must learn the ‘logic' of say, maya, before creating what could be second-nature with clay… however industrial efficiency being the primary argument against this… and the value of a slower pace of life and the couture object its retort). With paper and pen, free of language or any predisposed method of formal interpolation ( the keyboard, at worst), the brain is allowed to create rather than interpret, thus consciously expand and focus on the intentionality of creative manifestation rather than means of designing a translation. This is all a result of familiarity- my preference being able to feel what I am creating as opposed to think what I am designing. Freedom!  

Perhaps any technology- pen or keyboard (or Cintiq!), written or spoken word- is still not thoroughly advanced or efficient enough to communicate the highest sensibilities of our imaginative capacity.  

And, as we become more dependent upon the computer interface and w/w/w as the source of real-time information, what happens in the architecture of our experiences, and our communities and intentionality of civilization beyond such technologies..?  

To have taken the inescapable philosphical route–I would much rather have intellegence than artificial intellegence, or anything, for that matter.  

In closing, and in response to Heinz von Foersters' notion of solipsism defeated in the presence of the community or the autonomous other, what of a community of solipsistics… or the ‘one' in multiplicitus individual forms merely separated by the unconsciousness of being varied psychological experience in physical bodies…? The ‘god-head' condition repeated: As Jungian dream analysis proposes the characters in our dreams are versions of the theatre of our psyche… I propose the characters of our waking experience are merely versions of our true ‘self' for which we are as inadept at recognising as ourself as we are the characters of our dreams, as it is happening.  

And to add another layer: How does the computer then place another screen of illusion through which we must navigate to look in the eyes of another, to see the total realization of what it means to be a ‘human' being//?  

….(Realized humans are one with themselves/the collective unconscious and those unaware in a void of the zero..)  

((The egyptians believed that the entire universe was counted between zero and one. 3 would be an example of moving between zero and one thrice, etc))…
3. river
11.16.08 / 5am


File, Edit, and Help  

Undo


[For instance,](http://www.youtube.com/watch?v=BEDx5oxG4c8) a common denomination of Darwin's post-atomic [Metaverse](http://web.mac.com/nealstephenson/Neal_Stephensons_Site/Home.html) might be Web 3.0.


The experience of helping real-people in real-time with real-world 3d problems has led me to a strange, new position as naturally as I once walked in the gradually, inexorably subdivided, forest of my irretrievable youth.


As an architect today, I find a disproportionate amount of my attention is devoted to helping clients attach a picture to their email. As a Dad, I struggle to learn enough just-in-time to keep up with the radical creativities of socializing children. The stories I myself internalized as a child on my [Grand](http://richard.ferriere.free.fr/3vues/a26invader_3v.jpg)[pa's](http://en.wikipedia.org/wiki/1940_Cincinnati_Reds_season) [knee](http://www.google.com/search?hl=en&q=dr+seuss+mulberry&btnG=Search) are not adequate to explain today's world, but they retain a smoky relevance beyond nostalgia. Facing Moore's Law in a clearer and clearer projection of [asymptotic](http://en.wikipedia.org/wiki/Deep_time) quantum intelligence, the Native mythopoetic [process](http://en.wikipedia.org/wiki/Joseph_Campbell) seems more relevant than I can possibly explain, from a feeling level, the level I [fight](http://www.youtube.com/watch?v=Xz-hO6nEjBM) to design from.


In 3d class yesterday, my students advised me that [Wrath of the Lich King](http://www.youtube.com/watch?v=KiP8fAZ4lZk) was finally released. They proposed that attendance would probably be off. But it was actually not. Does it matter to the Global Internet that my left cappuccino colored Converse sneaker is untied?


I think the Web is suddenly getting quite real after 15 years and more folks are evaluating it's risks against it's scintillating promise and, of course, their values. It is a trend I can see unfolding in daily and nightly practice everywhere, from my limited perspective, of which your blog is a helpful, and expanding part. Maybe dark forests are still somehow… [Nawh](http://johanssonprojects.com/default.htm). Yet, [may](http://www.oblong.com/article/085zBpRSY9JeLv2z.html) [be](http://www.smallpieces.com/).


I really enjoyed the additional layers of General's post, btw.
5. river
11.18.08 / 8am


Writ better'n I <a href=”http://www.nytimes.com/2008/11/16/magazine/16hyde-t.html?pagewanted=1&\_r=2″coulda writ by a longshot. I politely defer to the authenticity of the WWWeb for your future informations. I linked it up to your audience from a local Facebook friend's page I have not yet met, but a lot of my friends (four or ten good ones) seem go have in common locally.
7. river
11.18.08 / 8am


Whoops! Typo! [coulda](//www.nytimes.com/2008/11/16/magazine/16hyde-t.html?pagewanted=1&_r=2″)
9. river
11.19.08 / 6am


I want to apologize for communicating “out-of-form.” I certainly don't want to interfere with the emerging process of your efforts. Posting spam is not my intention, even though I've quoted it in the past, tongue-in-cheek. I'm merely exploring effects of much more radical languages whispered to my inbox in realtime. I can actually write like a person!


I watched a long [video](http://www.handheldlearning2008.com/handheld-learning-conference-and-exhibition/video/905-video/123-danah-boyd-social-media-scientist) a few minutes ago that explains a great deal about the underlying context for my weird missives. This is a new, and undefined space. How should we learn to act?
11. [NEIL SPILLER: The Great Forgetting « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/07/17/neil-spiller-the-great-forgetting/)
7.17.10 / 8pm


[…] <https://lebbeuswoods.wordpress.com/2008/11/09/zeroes-and-ones/> […]
13. [CONSTRUCTING A REALITY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/07/17/constructing-a-reality/)
7.18.10 / 2pm


[…] on Neil Spiller's reference to radical Constructivism, I thought it important to re-post (ZEROES AND ONES, November, 2008, in a more readable form) one of its seminal texts by Heinz von Foerster, the […]
