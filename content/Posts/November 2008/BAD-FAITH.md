
---
title: BAD FAITH
date: 2008-11-23 00:00:00
---

# BAD FAITH



We seem poised to enter a new era of the control and regulation of private life by the public institutions of government. The reason, it is becoming clearer every day, is that too many individuals in key positions of authority and responsibility  have abused their privilege of freely choosing their courses of action, by serving their own interests at the expense of the interests of the wider public. This is equally true in politics (where ideological fantasies trumped reality), finance (where greed for short-term profits did the same), industry (felled by sloth and stupidity), and commerce (debt-financed consumerism and damn the consequences—won't the party go on forever?). So, new leaders are being brought in and must use their authority to try to repair the damage done and set the society on a new and healthier course. It is an extraordinarily difficult task that will require—given the complex interconnectedness of everything today—unprecedented degrees of centralized control. Regardless of anyone's political philosophy, there is no other choice. The SOS has been sent. What we're talking about now is not the best of all possible worlds, but survival.


Architects, particularly many of the more recognized and celebrated ones today, share part of the blame for this unhappy state of affairs, along with leaders in other fields entrusted with the public's interests. For too long they've set the model for professional behavior by serving the narrow interests of clients who are primarily commercial developers, and who are famously interested in making quick returns on their money. Spectacular new building wrappers for tried-and-true (presumably unrisky) types of buildings have produced some amazing new shapes and surfaces, but little else. Developers will not risk their money on really new building types. Meanwhile, questions of new types of affordable living space in cities and of public spaces in an age of exploding population and diminishing direct contact go begging for some imaginative invention. And now, against all previously safe assumptions, we can begin to ask, who is going to buy all those luxury condos and lease all that corporate office space? We should care less if developers get burned in the current catastrophe, but we should care that some of the best and brightest of an innovative generation are going to suffer in the crises presently underway, and deepening.


On the other hand, it is difficult to feel badly about all the projects for skyscrapers, condos, resorts, luxury hotels and shopping malls that are currently going on ‘hold,' and will certainly die altogether. But is not hard to feel sorry that so many gifted architects have devoted so much of their talents and energies to them. The ancient Greek conception of tragedy looms here, as even the greatest heroes are undone by fate. At the same time, Existentialist voices keep whispering in our ears that we each choose our own fate.


For too long, architects have justified their servile attitudes (often accompanied by ostentatious displays of pride and independence, cleverly indulged by clients) with their belief that clients have more social agency than they do. In other words, by having lots of money or access to it, and therefore the capability to commission large and expensive buildings, which use massive amounts of human and natural resources, developers prove that they speak for the public—the ultimate source of all wealth—and are acting in its best interests. As recent events show, this is far from true. Then again, no one expects developers to be moral or social philosophers, while we do hope that architects—the stewards of constructed human space—exhibit a wider perspective of and concern for the human condition. When they do not, especially at the top of the profession, it sets a bad example for the whole field.


Each of us, regardless of our social or professional status, has not only agency within our own lives, empowering us to achieve our personal goals, but social agency, in that what we do affects others. Immanuel Kant's “categorical imperative” states that to be both rational and moral, each person must “act only according to that maxim whereby you can at the same time will that it should become a universal law.” Jean-Paul Sartre brought this idea into modern life by stating that each of us “is responsible for all mankind.” Our actions become the model for the actions of others. So, for example, if we decide to throw our used chewing gum wrapper on the street, we should not imagine that we are the exception, but the rule: if everyone threw their trash on the street, we would not be able to wade through it. Sartre went further with his idea of “bad faith.” We act in bad faith when we base our decisions on a socially assigned role, rather than our own inner sense of right and wrong. He had in mind the Nazi extermination camp commanders who defended themselves by saying that they were only following the orders of a legally constituted Nazi government, even though they did not personally believe in the murder of innocent people. A less dramatic example would be the judge who orders the execution of a prisoner “because it is the law,” even though he or she does not personally believe in capital punishment. A much more common example is the architect who accepts the commission for a project that he or she feels may be superfluous from a social standpoint, or perhaps redundant or even damaging, because “architecture is a service profession,” displacing—in bad faith—the responsibility to the client. By way of contrast, the great architect Cedric Price always asked potential clients, “Do we really need this building?” He inevitably tried to talk them out of building, saying “ the problem is not that we don't have enough buildings—rather, we have too many of them. The problem is, we don't know how to use the ones we have.” He devoted his practice to addressing, in the most ingeniously innovative and inventive ways, how to use, and re-use, what we have.


We should lament that Price's sense of his social agency was not widely influential. Of course, he had few clients and commissions. Hardly a model for today's globe-trotting, international superstars, or those who dream of following their examples. Still, we might imagine that the current economic downturn and financial shakeout will give all of us in the field of architecture pause, and some time to reflect on what we are—and should be—about.


LW   




 ## Comments 
1. Ryan
11.24.08 / 4pm


Thanks for that post, it is timely and I am sure many of us are pondering these very same questions that you have presented.


Like yourself, it saddens me that the convictions of Cedric Price and others seemed to have fallen upon deaf ears, limited to occasional discussions within academic circles (if that!). Though of course, telling people they shouldn't build, or what they are trying to build is wrong, is a bad way to get business. We are Architects after all, aren't we?


But why is our primary desire to get another commission? Why do we let ourselves be ruled by this insatiable thirst for more and more money? I don't think we have enough faith in our own feelings of what is “right”. In the end, what would you rather leave behind? One small, good thing, which benefits society in a positive way–or as Mr. Woods put it–hundreds of gum wrappers?


Architects should be thankful for this economic downturn. Thankful that we have been jolted from our comfortable lives of serving ultra-rich clients, and we can now see, clearer than ever before, how greed is effecting the environment in which we live.


INNOVATION.


Oh how we have lost sight of this single concept. I question how many architects today even understand what true innovation is–would they even recognize it if they saw it? We've been stuck in the same tired cycles and patterns of “Architecture”, that we've lost sight of the very things we started out to do in the first place.


What is innovation? Innovation is the light bulb. Innovation is putting people on the moon. But innovation is also stuffing newspaper inside your jacket to keep from freezing or figuring out how to support your children on minimum wage. Innovation is SURVIVAL. And now, architects have been placed in a position where they must survive. So to live, they must innovate.


The beauty, however, is that innovation is not only survival, but progress. Innovation can not only save lives, but enhance and enrich lives already being lived. In our darkest times, we can overcome, we can rise above…but only together.


We must look outside of our offices, and groups of designer friends.


Architects, designers, even many artists are blind. Blind to the truth that everyone outside of our little circle knows:


We are NOT special.


We are NOT the arbiters of what is creative.


We mock creativity and innovation thinking a select group of people can know or even understand the depth of the ingenuity of the mind. The human mind, which ALL possess.


We cannot be the arbiters of such a thing, because in fact,


We are the LEAST creative.


We have turned up our nose upon the truly creative individuals in our society–the houseless, the immigrant, the widow–and we have ridiculed (sorry, ‘critiqued') to death the few truly innovative designers we are fortunate enough to still have.


Instead, we have chosen to reward and worship those who only know how to make the most money, and who know how to become the most popular. But that is not innovation. Greed and Egotism do not bring creation…but only destruction.


My message is not only condemnation but a plea. A call to architects and designers, who for so long have been given power over others (only to use it to oppress, and promote their own ideologies) to put aside their egos and preconceptions of others, and begin listening to those around them. The ordinary man or woman, citizens of our globe.


Remember,


Your neighbor is likely more creative, more artistic, and more innovative than you are. Stop ignoring them, but watch them, listen to them, talk to them…and re-learn what it means to be creative.


Only through returning to true innovation and creativity, can we ever hope to move forward. Anything less is just more wrappers.
3. Timothy Mendez
11.24.08 / 11pm


I work for a small, private residential design/build firm that produces large estates for extremely wealthy clients. Having had great professors and visiting lecturers-LW spent a week with us at the University of Oklahoma some time ago-this is not the architecture that I envisioned for myself. 


This essay resonated deeply within as it exposed my own cowardice and laziness. I forwarded it amongst my remaining coworkers including the principles, (we've cut 70% of our staff in the past 6 months.


I was damn near fired.


It seems the principles skimmed over all the big words and ended up with me calling them Nazis.


That being said, I'm making up for lost time, apologizing to my own betrayed ideals and am leaving this firm. There will be an opening. 


If anyone else feels the need to disseminate this essay to your officemates, I suggest replacing ‘Nazis' with ‘gummy bears' and ‘concentration camps' with ‘golden Haribo wrappers'. 


Never saw it coming.


Thank you, LW.
5. ekkehard rehfeld
11.25.08 / 11am


two thousand years of chistrian history today culminate in what we see as the biggest crisis ever. two thousand years of crisis following crisis, war following war. little has changed, why should it now?


has the age of aquarius finally dawned because “this worlds new president” will take office on the first day of aquarius 2009?


will there finally be harmony and understanding? will the ideas of solidarity, justness, sustainability advance beyond propaganda?


i doubt it. little will change as long as the vast majority of this worlds population believes in the concept of divine salvation. as long as they do, they will believe in someone (something) else to take care of the chewing gum wrapper. as long as they do, this worlds leaders, including those in the field of architecture, will not need to pay much attention to necessity or true sustainability. they are allowed to continue their operations unchecked, as long as the vast majority seeks salvation not in mankind (=itself), but in some abstract concept, such as god, culture, design or lifestyle.


the age of aquarius will be one of rationality. LW is right, it is time to reflect.
7. will
11.26.08 / 2pm


I will admit I have been tempted to apply the categorical imperative to architecture. It seems there are very questionable values at play within our profession, at least in a moral sense. The trouble with architecture is that it is usually too damn big for one person to perform in their head. Few are those fountainheads that fully grasp and wield their work in a way that is completely authentic and selfless. What I see in Kant and Sartre are typically maxims who's effects become obscure beyond a certain scale. Architecture begins with the individual, in all his or her moral virtue, and grows into edifice. This Building, this house or tower or car park, must it not also then be judged? Can we possibly expect to accurately relay the blame for a faulty design back to its architect?  

In principle, we can. We have, however, systematically managed to redirect the responsibility for misbehaving Buildings from ever affecting the moral character of the individuals that create them.  

One might argue rightly so. Can we count on every individual to bear this responsibility? Or is it at all realistic to believe this to be possible?
9. [progressive reactionary](http://www.progressivereactionary.com)
11.27.08 / 4pm


LW – While I wholeheartedly agree with your sentiments, I find myself ever conflicted on *how* exactly an architect is supposed to navigate the terrain of a culture and an industry rife with opportunities for “bad faith.” Indeed, the strategy of resistance which you espouse and to which I too am drawn ultimately seems to offer two equally undesirable endgames. The first would be a profession that consists almost entirely of independently wealthy architects, in the game for the right reasons, but nonetheless preselected and prequalified based on their ability to survive financially without the patronage of wealthy clients. The second alternative would be the opposite: the “starving artist” model where architects, shunning corporate and developer work, are left to scavenge for those dwindling public and non-profit “good” projects.


Now obviously I speak of two extreme conditions here, but you get my point. For the young generation of architects who largely have not known any other reality than that of graduating school and going to work for some starchitect or developer architect, you can imagine that it's difficult to envision an alternate way forward. Difficulty, though, can be overcome — so I remain hopeful. My question remains, though: if we eschew the developers and the corporate capitalism that's so driven the profession in recent years, how do we avoid becoming either a profession of aristocrats or a profession of paupers?


Maybe the answer lies in the kind of massive public investment that many are hoping new leadership will bring in the coming months….
11. Penelope
11.29.08 / 8pm


Dear Lebbeus and all, 


I think architectural education can really make a change and we should count on it.  

Unfortunately many architectural schools build their courses in order to ‘prepare' the students (by learning them how to perform sophisticated softwares) to work for the star-architects, as if this reassures the success of their courses. 


Education has the power to do much more than promoting existing situations and I believe that the paradigm-shift could be initiated from the place where young people start shaping their minds as architects.
13. [herman](http://dayswithoutarchitecture.blogspot.com/)
12.6.08 / 8am


architects have this blind faith in receiving data from a developer, believing this is what the future needs… in the stock market, a smart investor will read behind the data knowing that this is the future the big player wants you to believe in.
15. [AUTHENTICITY « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/02/06/authenticity/)
2.8.10 / 2am


[…] <https://lebbeuswoods.wordpress.com/2008/11/23/bad-faith/> […]
17. [Piano Supersizes the Kimbell « Trouble in Xanadu](http://troubleinxanadu.com/2010/05/piano-supersizes-the-kimbell/)
5.28.10 / 10pm


[…] to handle. But ultimately, the situation reminds of a quote by the architect Cedric Price. As recounted by Lebbeus Woods, Price would ask his clients: “Do we really need this building?” If the new […]
