
---
title: BIG BANG  “Doctor Atomic”
date: 2008-11-13 00:00:00 
tags: 
    - atomic_bomb
    - stage_design
    - Theater
---

# BIG BANG: “Doctor Atomic”




In late 1944, I was living with my mother at a U. S. military installation outside the small desert town of Roswell, New Mexico. My memories of that time and place are few. The little wooden house we lived in at the edge of the desert, raised up on blocks a few feet, to allow the sand to blow under and not accumulate in drifts against it. The treeless, empty desert vistas. My stays in the base hospital, to treat chronic urinary-tract infections. The funny petroleum smell of my father's clear plastic raincoat, which he would wear over his uniform on intermittent visits from up north, where he spent most of his time, in Los Alamos, working on the Manhattan Project. Plastics were something new then, really unperfected, but so too was the atomic bomb. All of that was soon to change.


Recently, my wife and I attended a performance of “Doctor Atomic” at the Metropolitan Opera in New York. John Adams composed the music, with a libretto by Peter Sellars, and the production—on a stage four times the volume of the usual opera house—could indeed be called “grand.” The story, which focuses on the few days leading up to the first atomic bomb test in July of 1945, at the “Trinity” site in New Mexico, portrays the complex emotions of the people who conceived and built the bomb, and those who were close witnesses to its development and the test. Of the latter, Kitty, Robert Oppenheimer's wife, who lived with him in Los Alamos, and Native Americans from that region, are given leading roles in the drama. The personal and the mythic in contrast with the political and scientific is a profound, deeply affecting dimension left out of official histories. It stirs in us the recognition that, regardless of our roles, we are not passive witnesses to history, but active participants. We can either resist or give in to the imperatives of the moment. Resistance may not be in the form of barricades, but only in poetic or visionary dissent, as in Kitty's alcoholic ramblings or, more potently, in the recasting of the event in traditional Native American rituals that try to absorb it into a broader, more spiritual reality. After all, bystanders are powerless to stop events already taking place. They can only raise our consciousness in the hope of getting beyond them to a wiser future. Still, what happens is our choice, and responsibility. The last scene of the opera, with the whole company standing awestruck as the big blast of the test occurs, is the most moving of the performance. The final curtain goes down on a vast landscape frozen in immobility and silence. No one really believed that it would come to this. A new and terrifying world had been born.


Adams' music is strongly evocative of the difficult emotions involved—it speaks to conflicted, if not tormented, souls. There is little of the sonorous or harmonious. In contemporary opera generally, the absence of hummable passages is the rule, as ‘tunes' are relegated to popular Broadway shows. Mostly choral cascades, fleetingly lyrical horn and woodwind solos and, occasionally, rhythmic cacophonies of sounds, the Adams' work is more a great film score than stand-alone music. Lovers of Mozart and Puccini beware. The libretto, by Sellars, is largely made up of extracts from official transcripts of documents and letters—very verité and purposely prosaic. There are a handful of lyrical arias, but most of what we hear is sung speech.


The visual design of “Doctor Atomic” is problematic. The Met, with plenty of money to spend on its own production (the opera premiered in San Francisco three years ago), has introduced some wonderfully abstract elements: the enormous rolling grid-walls that are used for the chorus members, to project images and maps and even stormy weather on, and later the Native Americans, in traditional costumes, who in a way are the ultimate witnesses to what is happening on their ancient land. Representing nothing in particular, the grid-walls can be interpreted as stand-ins for Western civilization, as Cartesian-mathematical systems of order; as the compartmentalization of modern knowledge; as the Existential isolation of individuals from one another. These elements are a brilliant stroke. However, the designer and director chose to be more literal in the representation of the mountains in the background/distance, choosing to build them onstage from tarps laid over a gigantic wooden framework that more befits a high-school production. The shape is wrong (they are too peaked) and the scale is wrong (they appear too close), and, worst of all, they are visually dominating throughout, even though they really play no role in the actual narrative, and could be omitted altogether. The San Francisco production showed them as a gently jagged flat shape in the deep background—which is interesting because that is the way they actually appear, yet, in the theater, it made them seem more abstract.


Opera, in general, and grand opera, especially—with its tremendous costs of production resulting in ultra-expensive tickets and, therefore, limited audiences—has been under attack for decades. I remember a conversation I had some years ago with Brian Eno, the great contemporary composer, who thinks all the opera houses should be closed, and the tens of millions of dollars it takes to run them transferred to better uses. Given the many urgent problems in the world, it is tempting to agree with him. Do we really need to see another grand production of Donizetti's “Lucia di Lammermoor?” Or, if there must be one more, can't we just put it and all the other operas on DVDs and shut down the opera houses forever?


The emergence of “Doctor Atomic” argues against this idea. It demonstrates that opera can be a vital art-form that speaks to contemporary people across their usual social divisions. The Met performance we attended was being transmitted live, by satellite, to movie theaters around the world. It was also simulcast on radio and will appear in its entirety, and for free, on public television stations everywhere. And there is another thing to consider: live performances have to happen somewhere. In movies they happen on sound stages or ‘on location.' In opera, they happen in theaters, where not only the performers are live, but also their audiences. This is interactivity at its most intimate, and its best.


Nuclear weapons are still with us in the tens of thousands. For now, they are dormant, like sleeping devils. “Doctor Atomic” reminds us that the ecstasy of creation can very easily lead to a lust for destruction. The devils can be awakened at any time.


LW


ps. I never returned to New Mexico. My mother died at 79 in 1986. My father died at 52, in 1953, from radiation-induced cancer.



Trailer of “Doctor Atomic” at the Metropolitan Opera production (2008):


[**[**](http://www.youtube.com/watch?v=Uy9vx_lNxYg)**removed by youtube.com for copyrights violation)**


Final scene (partial) of “Doctor Atomic” from the De Nederlandse Opera version (2007) of the San Francisco Opera production:


[**(**](http://www.youtube.com/watch?v=6SSijYptknc)**removed by youtube.com for copyrights violation)**


Historical references:



<http://en.wikipedia.org/wiki/Nuclear_weapon>


<http://en.wikipedia.org/wiki/Manhattan_Project>


A recent NY Times article:


<http://www.nytimes.com/2008/12/09/science/09bomb.html?_r=1&8dpc>







#atomic_bomb #stage_design #Theater
 ## Comments 
1. Adam
11.14.08 / 6pm


I had a similar and recent experience with theater in NYC. It completely blew my mind away that there is something beyond money-making entertainment happening on stage – that one can be brought to THINK by ‘performers'. I've often thought that stage productions are now mostly irrelevant – either they are huge brand name spectacles meant to accommodate a movie-franchise, or they are complete and utter unknown, amateur crap done with the ‘idea' of theater being a great and valiant enterprise – much like architects within the profession/industry. 


But after sitting through ‘Fifty Words' in the East Village with a small house and great pacing, it was actually cathartic, far more than entertaining and ultimately more valuable than whatever un-rated director's-cut indie film on dvd could be.
3. [TERRIBLE BEAUTY 3: “I am become Death” « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/09/14/terrible-beauty-3-i-am-become-death/)
9.14.10 / 9pm


[…] down to earth,” and the lines from the Bhagavad Gita quoted by the head of the Manhattan Project, J. Robert Oppenheimer, as he witnessed the first test of an atomic bomb in New Mexico, in 1945, “Now I am become […]
5. [ORIGINS « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/01/02/origins/)
1.3.12 / 12pm


[…] <https://lebbeuswoods.wordpress.com/2008/11/13/big-bang/> […]
