
---
title: BAD FAITH 2
date: 2008-11-29 00:00:00
---

# BAD FAITH 2



I am deeply appreciative of the seriousness and directness of those who have commented on the BAD FAITH post so far. Each deserves an individual response. Still, one underlying question is clear: if architects are not to be the mere servants of developers, what are they to do? Because I have raised that question, I feel obliged to attempt an answer.


 I mention Cedric Price in my post. His is certainly a fine model of practice to follow. He was not independently wealthy, nor did he live like a poor man. He enjoyed excellent cognac and fine cigars, and never wanted for them. He had a fine marriage. His circle of friends and colleagues included the best and the brightest. And he turned down work he didn't believe in. He chose carefully, not making more than enough money to go forward with his work and his life. Oh, and he also had a sense of humor, and some perspective on himself, which enabled him to see things more fully the round. However, he is far from the only architect to uphold high standards in an often venal profession.


 If you look at the lives of the best architects of the preceding generation, you'll discover that the esteem in which they are held had little to do with blockbuster developer projects, though they were often approached to design them, and just as often refused, because they believed that the critical issues of architecture lay elsewhere. Wright was always broke (by his own admission), because he devoted a large proportion of his time to questions of houses and housing and cities and researching the relationships between architecture, technology and building crafts. Le Corbusier, the same. While the “plan Voisin” was commissioned by a developer, it was his program, and not the developer's, that prevailed. The number of urban studies he did for minimal or no fees is astounding. His research into typologies brought him little money, but just enough to carry on. Mies fared rather better financially, but not so much. His fees for the Seagram Building and 860 Lake Shore Drive no doubt financed his research into public housing projects and his conceptions of ideal civic space. We could go on—Louis Kahn focused on public buildings of symbolic importance, where the integrity of the architecture resonated with ethical ideals of civic life. He wore rumpled suits and dreamed of a better city enabled by architecture. One can agree or not with the viewpoints or the architecture of each of these architects, but their devotion to the principle of architecture as a primary instrument of human agency is indisputable. These were architects who took personal responsibility for the place of architecture in the complex human world. And they were the role models for my generation.


 Something happened in the 1970s. These and a few other exemplars of architectural thought and practice had died. The brief flowering of political populism of the 60s was being translated increasingly into commercial projects. Robert Venturi and Denise Scott-Brown wanted architects to “learn from Las Vegas.” Colin Rowe and Fred Koetter advocated “collage” cities, made up of implicitly commercial fragments of different, even conflicting parts. It was an intellectually exciting time, as ‘paradigm shifts' always are. The marketplace of competing capitalism was equated with freedom of choice, hence social equality. Architectural thought, indeed its ideals, began to shift from singular visions of urban space to more commercially viable projects. The real-estate developer emerged as the best social agent for a burgeoning consumer society. For younger architects the architectural role models were not the ‘old masters' like Mies or Wright or Corbusier, but corporate firms like Skidmore Owings and Merrill (SOM), offices with mainly commercial projects, who were servicing the rapid expansion of consumerism. Not only architects got on board, but municipal governments, as well, with the establishment of various public ‘development' agencies and commissions, and giving ‘tax breaks' and other financial inducements to private developers. The real-estate boom of the 80s was on and, with some stumbles, has continued up to the recent crash.


The point here is not that the good old days were better and we should somehow go back to them. The world has changed too much and ‘going back' never works, even if it were possible. But there are things to be learned from the past about the practice of architecture, ways of thinking and acting that might inspire us, bring a new sense of purpose, and give us renewed energy to go forward. As much as I dislike lists of dos and don'ts (they have a musty, superego-ish sniff to them), in this case it may help me get to the point. So, for architects, a short list:


 Don't accept a commission (or a job) unless you believe that it makes a positive contribution to your community—however you define ‘community,' and however you may define ‘positive.'


 Don't convince yourself that taking on a project you have no such belief in is just a ‘stepping stone' to projects you can believe in. It won't work, and never has.


 Do devote as large a portion of your time as you can to independent research and experimentation about problems that you think are important to architecture and your community. Don't wait for a client to ask for it—you are the only agent for such work. Be prepared to finance it from your own pocket. Then publish the results so they can be available to as many people as possible.


 Do keep your office as small as possible. Creating a large ‘overhead' is a sure road to taking on projects and clients you don't want, but ‘must' accept to pay your employees.


 Do remember that your responsibility as an architect is first to the wider community you inhabit, and only then to your clients. Don't accept clients who do not share this understanding.


 Do remember that what you do as an architect—at whatever level you practice—is vitally important to the field as a whole and to your community. Don't imagine that what you do doesn't matter, or is too small to make a difference.


 There is one thing I failed to mention about the best architects of the former generation: they were all teachers. Wright had the Taliesin Fellowship; Mies the Bauhaus and the Illinois Institute of Technology; Kahn the University of Pennsylvania. Price had no fixed academic affiliation, but taught constantly at various schools in the UK and Europe. These architects believed in the coming generation, devoted much energy to it, earning a modicum of money with which they could carry on. They taught by the example of their work, but also by their direct engagement with young people aspiring to be architects. I cannot think of a happier form of practice.


LW




 ## Comments 
1. Sander boer
11.29.08 / 12pm


dear Lebbeus,  

I have always admired the engagement of our preceding generation, sadly I have also come to the conclusion that the market has been capitalizing on our willingness to invest in quality. See the increasing demands put forth in competition briefs and tenders. It is considered normal that we make sketch designs for free.  

This is I think the reason why we are the worst paid academics.


It is how we deal with this demand that is important.  

I feel we are giving up our expert advice in favor of being servile.  

I feel that we do not try enough trying to be servile by the means of our expertise.  

Too many times our work is limited to the facade or furniture. Exactly denying us to the core of our business, the plan.  

So, dear Lebbeus, I have another advice for us:  

Know better and prove it.
3. [slothglut](http://fairdkun.multiply.com)
11.30.08 / 7am


LW,


tQ for giving back to hope to my generation. i've tried to write an essay with a similar reminding tone back when i was still a student back in jakarta, but as anyone's guess, i got more angry letters than actually successfully shift the paradigm.


this second part of your writing makes it clearer for me about what i'm striving for. as for teaching, i'm still questioning myself about my own agenda. it's an ideal scene you described above, but if it's based on gaining political power towards the young + the naive, i should not do it, since its tendency towards corruption. would you agree?


tQ again,
5. aes
11.30.08 / 9pm


The architect's voice and role in the larger community are indeed treasured in the universities, and I wish I could see it on a larger scale in the public realm, namely in mainstream politics. Architects, I think, would have a uniquely qualified point of view in government, especially at a time when there is a renewed focus in infrastructure, urban renewal, and sustainability.  

I think as architects, we need to redefine our role in the community; if we want a different contractual and professional relationship with developers, or the public, or especially politicians, we need to do so by expanding our presence in the public sphere. The political arena itself is one, though certainly not the only, place where we can both contribute our expertise and raise the public's expectations of that expertise.
7. david
11.30.08 / 9pm


In my experience, these values are alive and well within the academy. The hard part is implementing them upon graduation. Why do young architects work on projects that they don't think are Good? Our generation has something that Mies and Kahn and Venturi never had: student debt.
9. [Mark Primack](http://www.markprimack.com)
12.7.08 / 9pm


David's point is well taken. Lebbeus, you're observing the profession, and history, from a tall tower indeed. As someone who has struggled with each of the constructs you describe above, I must say that this entry has more than just the aroma of the peanut gallery.  

 Academics pontificate for wages to students who are mostly supported by their parents. Architects like myself pay your former students while teaching them, even as they are becoming parents themselves. So yes I do take on projects to keep your students employed, and they have never complained about that.  

 The ‘academy' was built on the failure to distinguish the advancement of society from the advancement of individual careers. As one writer once quipped, it is the only institution in history that sees no conflict in calling for world revolution, while demanding tenure.  

 As Einstein could ‘learn from any man (sic)', so I have struggled to learn from any project, provided it served the health of my community.  

 Writing about the painter Giorgio Morandi, John Berger reflects on the way obscurity can liberate the artist from the desire to please, which is measured in our age by ‘fame', hiding though it sometimes does behind ‘revolution'. Academics long to be famous revolutionaries, but usually settle for tenure. I've been in both worlds, so I know that the ‘threat' obscurity requires more personal courage and discipline than the ‘promise' of fame.  

 I'm surprised that your history in this entry has so similar gloss as your dos and don'ts:  

There are a lot of reasons Wright was broke all the time, and they are not all pretty. But only together can they help us understand what drives creation and revolution. Edit out the less romantic or idealistic and you're left with Palin-esque pap.  

Louis Kahn was guilty of repeated sexual harassment of his students and interns, as chronicled in the latest documentaries. That needn't diminish the beauty of his buildings, but it should inform their context.  

I knew Cedric Price. He was very clever, very articulate, very confident. Back in the day (mid-seventies), I stole his office manager off to Amerikay. He was the last to leave our farewell party, there still being some cognac in the bottle. His last words to me, as I gave up my privileged spot before the AA bar, were……  

Actually I can't remember what he said. After all, he was just another guy trying to keep up with his own reputation.


 Academics, professionals and students would do well to consider Hugo's pronouncement, in ‘Notre Dame de Paris', that ‘time is the architect, the people are but masons.' In times of turbulence and uncertainty, a little humility can go a very long way.  

 I remain, as ever, a grateful admirer of that which you do so well.
11. [lebbeuswoods](http://www.lebbeuswoods.net)
12.8.08 / 12am


Mark Primack: My, my….I guess I did hit a nerve.


Your viewpoint would be more convincing if you based it on ideas, rather than stereotyping (in the all-too-familiar way) ‘academics,' and belittling some fine architects. 


Also, I would advise that you look into your problem with ‘fame.' “Yes,” Peter Cook once said, “Cedric is famous…to 135 people around the world.”
13. [Mark Primack](http://www.markprimack.com)
12.8.08 / 3am


Lebbeus, my viewpoint is based upon what I know. For instance, Reyner Banham told me that Frank Newby of Ove Arup (the engineer of Price's aviary) was the originator of that quote, and had said it of Banham. An incestuous group, those English.  

 Peter Cook was a great supporter of my work, and I owe him a lot. But we met under somewhat contentious conditions, similar to this exchange of ours. He was encouraging AA students to feel comfortable in the pursuit of more mundane, technical avenues of architectural inquiry than the ones he was exploring. Window details, for instance. I challenged him to stick with what he knew, and to promote what he loved and invested himself in; anything else would be insincere at best, presumptuous at worst. I was twenty-one at the time.  

 In speaking of the greats, did I belittle anyone by acknowledging their humanity? Of course not. The pedestal creates the problem, and always has. In responding to your stereotyping of working architects as ‘mere servants of developers', I was only observing that you were calling the kettle black.  

 So many of your entries on this site have addressed the issues and pitfalls of fame that I'm not sure I'm introducing a new ‘problem' here at all, except for my inclusion of the Academy from which you speak. My comments from the field only echo yours from the tower. I keep touching that nerve, I know, so let me acknowledge the obvious, that people who have never built architecture out in the world still have plenty to teach. They can and should teach what they know and love, not what they fear or loath, or suspect or resent.  

 But here's an idea, as per your suggestion. My daughter, a musician not an architect, recently sent me this link. She thought I might find it amusing. 


<http://villageofjoy.com/50-strange-buildings-of-the-world/>


 What struck me about this personal collection of ‘strange' or ‘unusual' buildings was that the works of some of the world's most famous architects were scattered among those created by ‘unknown' architects, non-architects, admen and, arguably, madmen. The names and pedigrees of the designers were immaterial to the maker of the list. Can we say which are ‘destructive' and which are ‘positive'? After all, they all constitute ‘research'.  

 We can look at the construct of this collection as uninformed, ignorant, crass or whatever makes us feel righteous about our own efforts as academics, historians, theorists or architects, or we can appreciate that the postman Cheval (4.) may ultimately be the most positive and influential among them all. His ‘masterpiece' was built from whatever caught his magical eye as he walked on the road. He worked with what he knew, and that was universe enough.


ps. Might future generations of academics confuse the Wooden Gagster House (31.) as your largest built work, or even ‘of the school of Woods'? How would you feel about that?
15. [lebbeuswoods](http://www.lebbeuswoods.net)
12.8.08 / 1pm


Mark Primack: I take your point about my bit of stereotyping. Please observe, however, that I hold the most famous architects responsible for failing to set the ‘right' moral example not in their personal lives (no doubt as flawed as the rest of us), but in their practices. If that is a ‘righteous' judgment, so be it.


A recent exception—a strong step in the ‘right' direction—was a recent public speech by Thom Mayne, in which he condemns the overdevelopment in Dubai. I will be really encouraged when an architect of his stature actually turns down a big commission on the basis of principle, and makes it public.


<http://www.bdonline.co.uk/story.asp?sectioncode=426&storycode=3124669&c=1&encCode=0000000001841ac5>


<http://de51gn.com/2008/10/11/is-thom-mayne-skeptical-of-dubais-sustainable-building-ambitions/>
17. Tomek Gancarczyk
12.22.08 / 12am


Dear Lebbeus  

“Don't convince yourself that taking on a project you have no such belief in is just a ‘stepping stone' to projects you can believe in. It won't work, and never has.”


there is one thing common to this which bothers me: 


sometimes the problem is not to take on project having no belief, but when entering the situation so hard that You are unable to face and as a result repress it. Convince yourself that its positive, someway trick yourself in order to accept the reality and defend your position and action in it, enter the beautiful and happy dreamland. Such unreal world once entered cant be left easily, because leaving it no matter what time ruins everything that was built there. The later you leave the more you have to destroy, the more you have to destroy the more difficult to leave.
