
---
title: DEAD WORDS
date: 2008-09-18 00:00:00
---

# DEAD WORDS



There are words and terms that once had currency in architecture but have become, in effect, dead. This short, annotated list contains a few, but I'm sure there are more, and I invite readers to submit their own in the comments section. The point here is not merely academic, but rather to note the shifts in thinking that impact the nature of our field's development. The words we use—and don't use—are important.


 **radical**


**This term used to refer to paradigm shifts and other important changes in thinking and practice that contributed to human progress [see below]. But today, it is associated with ‘extreme.' In the era of terrorism and the so-called ‘war on terrorism,' radicals are seen as the enemies of the currently hunkered-down system of social order—in short, as terrorists. They are to be shunned, especially in the application of the penultimate instrument of social order, architecture. It is certainly acceptable to propose extreme forms, now and then, but only in the service of already known and familiar programs of use, and therefore as a reaffirmation of the status quo. Proposing radical forms that implement radical programs is unacceptable. Indeed, radical programs of use are more unacceptable than they ever have been.**


 **new**


**Advertising and media hype have used this word to death. But that, in itself, is not the reason for its demise. The application of the word—and concept—to many things that are not really new has effectively destroyed its credibility. The rapidity of change has made everything seem new, even if it is not. The ‘new' model of car, the ‘new' skyscraper concept are of the same ilk: new forms of what we already know and have. We embrace the contradiction, so we can have the illusion of newness, while clinging to the old.**


 **original**


In the present time of appropriation in art, as well as the mass-merchandizing of brand name products, including those of famous architects, the idea of originality is not only of minimal interest, but, being a form of the radical [see above], rather dangerous. Of far greater interest is the recycling of ideas, products, and modes. Appropriation acquired legitimacy in the post-Modernism of the 70s and 80s, when the recycling of historical styles—including Modernism—was in vogue. Today, it continues in the guise of architectural populism and social realism, where low art, such as squatter architecture, is elevated to high, and presented as avant-garde.


 **principles**


Today, everything is about technique. ‘How' a building is conceived and made is of great interest, but not the ‘why.' Principles are concerned with the ‘why.' Principles are philosophical—they define basic, inflexible reasons to do a particular thing and not just anything. Today, principles only get in the way of architects who want to do as they are told by their clients, or be free to adopt new styles and modes.


**progress**


Considered a hopelessly old-fashioned idea, progress means that things get better, that they somehow advance, reach a higher level. Developments in technology, political thinking, and architecture were once thought to be instruments of progress, that is, change for the better in the human condition. Today, it's difficult to say in any general way what ‘better' is—in the cacaphony of the marketplace, there are so many different voices, options, demands. Hence, we surmise that things pretty much stay the same, changing in form, not in content. Architecture valorizes wealth and power and the egos of architects, as it always has. Architecture is for an elite who can afford to commission expensive buildings, and the architects willing to design them.


**experimental**


While this word is bandied about in architecture, its meaning is all but dead. There is little architecture, or design, that truly experiments, that is, plays with the unknown. The single defining characteristic of an experiment is that no one knows at the outset how it will turn out. The experimenter is looking for something, has a hypothesis to prove, but has no idea if the experiment will verify the hypothesis, or prove it wrong, or result in something entirely unexpected. Experiments are risky. Architecture is today, and generally has been, averse to this kind of risk.


**critical**


This word has two meanings for architecture, both of which have to do with time. There are critical moments in architecture, when profound ideas are at stake, and the outcome of debates and discourse about them will impact the future [see below] of architectural ideals and practices. At present, there are no great debates on which the course of architectural thinking seems to hinge. And no ideals. The second meaning of the term is found in the idea of criticism. Criticism was once thought to be essential to high-stakes debates about architectural principles [see above], but, lacking those, has today become, at best, a matter of personal opinion, and, at worst, the stuff of careerist maneuverings.  


**housing**


This word refers to large-scale developments, usually sponsored by governments, that provide living units massed into large building groups. These mass-dwelling projects were the products of ‘socialistic' thinking, that is, governance committed to the fair redistribution of a community's wealth and resources. Today, socialism in all its forms is dead, having been soundly defeated by globalized capitalism. Further, the idea of class has been flattened out to a quotidian middle by credit-cards, retail franchises, tourism—in short, consumerism. The middle class does not live in housing, but in houses and condos.


**genius**


Like the word ‘new,' genius appears to have lost its meaning. If everyone will be famous for fifteen minutes, then everyone will be a genius for about the same period of time. However, the main reason the word no longer applies is that it is too blatantly elitist. Today, the rich wear blue-jeans, not top hats. In the age of consumerism triumphant, everyone is supposed to be, or at least to look, the same—somewhere in a ‘middle' class. The words ‘celebrity' and ‘starchitect' are as derogatory as they are flattering or honoring. But also, maybe the age of geniuses, of people who discover or invent great new principles [see above] about nature, science, or art—and architecture—has, for the present, passed.


 **future**


Once upon a time, the future was where wondrous and terrible things were going to happen, where the present would be transformed, for better or worse, and in a sense reach fruition. The idea of the future has all but vanished from architectural conversation and discussion. Perhaps because the present is one of self-satisfaction—there is nothing to ripen and mature—and no great chances being taken that can succeed, or fail. Perhaps the future has become just another place we already know, or hope we know.


 


LW












 ## Comments 
1. Severn Clay-Youman
9.18.08 / 5pm


It seems that most of these terms have only had this particular resonance for the last fifty years or so – maybe the more interesting question is what we might be returning to? This is a lament often heard in the performing arts – as the baby-boom generation becomes the dominant taste-makers (I think you could add the term “emerging” to the list, by the way). Have we entered a sort of avant-Beaux-Arts period, where we've just become very good at producing forms that look new?


SCY
3. Nenad A Stjepanovich
9.18.08 / 9pm


Architect: While this seems to be basic and most important denominator whether we are talking about building or planning, this basic subject is increasingly fragmented and it's becoming obsolete in historical sense. I would say the word only preserved a meaning metonymically. What do I mean?  

If you read about the definition from several sources such as wikipedia, or from widely accepted historical writings which investigate context of social order, culture and spirit of time within profession (M. Tafuri, Francesco Dal Co, Rayner Banham ) – Zeitgeist, you would come to understanding that there is a radical departure from the old normative definition. Today, I would say an architect is someone akin to being a talk-show lawyer. On one hand he is an expert in a legal landscape of terminologies that define a size and a scope of program, a person well versed to push the project to poetic realization in economical terms. And he completes this task with his talent by preventing political rancors (as much as possible, see WTC memorial), legal battles, or simply bureaucratic bog. On other hand, we see maybe 2-4% of professional architects who engage in a kind of holistic approach to architecture – ‘this building provides better future', ‘it's an “intelligent” building (KPF)', and ‘we saved millions to our clients' (Vingnoly Architects) or ‘this building represents school's commitment to the future' (Frank O. Gehry) which are slogans reminiscent of cooking channels that provide immediate indulgence and easy access to satiating content. I am not saying that these as they are called values of one's economic part of endevour are not important. However, they are simply a part of the process not the goal itself. In increasingly globalised society, which is constantly agitated with more imagery, prevaricating technology and hyperactive media, profession itself is slowly relegated to a subservient role whereas architect acts as a facilitator for developer. He enables him to achieve his business goals by putting humanistic (LEED) and expert's seal (codes & regulations) to an otherwise brutally plain marketing salesmanship. In short both architect and developer radically transformed their position from the time of Renaissance, when likes of Uffizis, Pittis, Sforzas and even Borgias commissioned great public and private works. I would call these changes comodification of the profession – everything is measurable, billable, punch-out, copied, listed etc. Unfortunately critical thinking and invention can not be broken down by equally abstract derivatives of media infused markets or there is place and time for it in the practice. Recently, I attended a ‘networking meeting' where one can be introduced to the whole spectrum of different professionals in the building & real estate industry that just bursted. After meeting very variegated crowd with completely different roots and educational backgrounds, I came across a very smarting insight – ‘all my friends that started as architects are in development now.' Obviously it was message that this middle age broker/loan-shark dealer from the open wide country was trying to convey to me. However, he was not even capable of explaining in what capacity he is involved in the industry. He did not even posses a basic aesthetic vocabulary although he was talking about big changes and grand buildings that will change the life as we know it. In the club scenery where this event was taking place he appeared more like well dresses hyped-up drug dealer, than anything else. It's unfortunate but we protagonists, architects, who only operate outside academic world, face this visionless reality every day.
5. [slothglut](http://fairdkun.multiply.com)
9.20.08 / 11am


.following your logic on ‘radical', i propose the next dead word is ‘revolution', it's like a taboo to use the word today, + those who disregard this condemnation, often fails miserably in restoring the original meaning  

.either cases, it's dead
7. aes
9.21.08 / 4am


ideology–


a charged word, certainly, and not without dangerous implications, as abundantly demonstrated when applied to politics, religion, art, economics, and all other things that truly matter. ideology carries real and tangible risks. 


still, i would posit that the only thing as dangerous as one reigning ideology is the poverty of having none at all. and indeed, it seems that is exactly the state of architecture; the norm is to remain fashionably non-ideological. the failure or imperfections of some ideologies has proven to be the perfect excuse to avoid it altogether.


if architecture is to last longer than the media used to sell it, or if architecture is to withstand the politics used to subvert it, or if architecture is to progress against the status quo established to curb it, then it must learn to thrive in the friction of an overabundance of ideologies.
9. [CW](http://www.studioshift.com)
9.21.08 / 11pm


Dead word: CONTEXT – seemingly replaced some time ago by SYNTAX which, in guise of such ongoing placemaking absurdities as Dubai or various cities in Asia, has just fallen into the grave next door. On the bright side there exists a grand sense of creationism and possibility, albeit now unchecked and propelled by consumerism as well as greed, that may some day turn into something GOOD (is this dead, too?)…one can only hope.


On GENIUS: The term suggests the individual as the generator of ideas. Along with what was written above, its demise may also stem from the air of collaborativism that has been implemented by many offices in order to purposefully undermine the word. Sadly, the media is intent on focusing the public on one person, one event or one thing which can fit easily into a headline. But, given our new means of connection, communication and idea sharing, we have thus far fallen absolutely short of meeting our collaborative potential.


On FUTURE: I'll admit that I wasn't yet born when architects were fervently inventing their visions of the future and what could be. But, from what I know about that time (the good old days) and what I know now, it seems that we used to be in a good spot, able to look ahead confidently. Now, it seems like we're simply trying to keep up.


Thanks for a stimulating blog.
11. [CW](http://www.studioshift.com)
9.21.08 / 11pm


…and what are the new words?
13. aes
9.22.08 / 1am


i think another dead word would be ‘history.'


the disenchantment of the future is mirrored by a disengagement with the past, and for me the loss of architectural history is parallel to, indeed tethered to, the erosion of architectural literacy.


there are those who see architectural history as a series of neatly concluded narratives relevant only to the time to which they belong; or worse, there are those who see it as a mine of stylistic resources ripe for fetishistic misappropriation. yet an examination of any significant piece of architecture of any era shows that there are questions that haven't been answered, and disruptions that haven't been quieted. i want to see history not as beautiful dusty books valuable only as relics, but rather as books whose contents still prove radical, and worth trying to address. 


and for me, it seems that in response to the heavy volumes left for us by those who contributed to the culture of architecture in the past, all we've been offering are magazines.
15. Josh V
9.23.08 / 1pm


One dead word widely used throughout the media has been “Extreme” for the past 10-15 years or so. Everything has been extreme from sports to deoderant. And there has even been classified extreme architecture. I remember when I first heard of it I thought, “How interesting! Is this to mean architecture that has pushed theoretical, political, or social boundaries?” Unfortunately, no. It was in reference to people who built bizarre houses in strange places. Nothing very extreme about it. Fortunately, I've noticed this word is beginning to die out, although I see it being replaced on the horizon by “Turbo”. A different word with exatly the same meaning…


Another pop word replacing some of these words is definitely “Green”. Although I am a huge proponent of sustainable design, I feel as the green is being attatched to anything that can be BSed into being so. Greenwashing is so prevalent, I'm beginning to roll my eyes when I hear the word. It is good to see the movement becoming popular, but at what cost? I suppose most good things must be ruined by the greedy eventually…
17. Spencer
9.23.08 / 3pm


In the context of dead words I'd like to bring to the groups attention the living words of politics and architecture.


Monday, September 22nd 2008, in Seattle WA, USA, a collective of people made a political statement about inadequate housing, limited shelter space and the inhumane treatment of the city's poor by creating a permanent encampment at the south end of the city. This is an interesting political act (arguably) utilizing architecture as a statement.


Here are two links about the encampment.  

<http://www.nickelsvilleseattle.org/>  

<http://apesmaslament.blogspot.com/2008/09/nickelsville-in-pink.html>


Although the blogger has stated this is not a protest it is difficult to not see it as one. The tents are all one color (pink – donated by the Girl Scouts of America) contrasting strongly with its surrounding “context” (ha!), and the adhoc organization has strongly spoke out in opposition to the Mayor's policies regarding the homeless. The site is populated only by people without homes so it is unlike the few protests this year in which people camped one night at city hall in protest of the Mayor's policies.


If this event is successful the word ‘revolution' might have a little breath back in it.
19. joeyjoseph
9.23.08 / 7pm


It's nice to be reading you again. Hope your summer was enjoyable.
21. joseph
9.25.08 / 5am


Excellent list. I suspect, however, that none of these words have lost their meanings quite as quickly – or conspicuously – as *sustainability*.
23. [MDA DOCUMENTS / “Dead Words”](http://mda-docs.net/?p=60)
10.3.08 / 4am


[…] Click here for full article. Post a comment — Trackback URI RSS 2.0 feed for these comments This entry (permalink) was posted on Friday, October 3, 2008, at 2:15 pm by David Burns. Filed in News, Research. […]
25. Alex Bowles
10.6.08 / 6am


Mr. Woods,


I was struck by how many of these terms seemed to encapsulate the work of Frank Lloyd Wright. I didn't have time to comment at the time, but was interested to see him referred to again, in your post about the Fountainhead. 


I'm also wondering if these words aren't really dead, but have simply gone out of fashion for the time being, which can be the same thing, for all intents and purposes. 


Until, of course, fashion changes.
27. archinthebay
11.22.08 / 5am


“SPATIAL EXPERIENCE”


“DIAGRAM”


These are words commonly heard in architecture school. They have one thing in common: they both have no clear definition. It just depends on who is saying them.
29. [Dead Words « DEL LUMANTA : GREATEST PITS](http://dellumanta.wordpress.com/2010/11/09/dead-words/)
11.9.10 / 5pm


[…] ‘Dead Words' by Lebbeus Woods <https://lebbeuswoods.wordpress.com/2008/09/18/dead-words> Tagged with: language, lebbeus […]
