
---
title: THE FOUNTAINHEAD
date: 2008-09-22 00:00:00
---

# THE FOUNTAINHEAD



*This is the fifth in a series of posts on aesthetics and ethics in architecture.*


The Fountainhead (1949) is a very complex and layered Hollywood movie. It is the only movie I know that is *about* architecture, that is, in which architecture is not merely a background, but is the central issue in the narrative, around which the lives of the characters revolve. Further, it is the only movie that ties architectural aesthetics to social ethics, much in the way Nietzsche did, writing of theater, in his The Birth of Tragedy. The fate of human relationships rises and falls on the way architecture visually portrays concepts of integrity and moral value. Arguably, this movie has had an immense impact on the public perception of architects and architecture, and also on architects themselves, for better and for worse. Given its age, and the fact that most people today have never seen the movie, this influence is largely indirect. Nevertheless, it persists. 


 These distinctive features are due to the author of the screenplay and the 1942 novel on which the movie is based, Ayn Rand. A Russian who immigrated to the United States in the 30s, Rand was a vehement anti-Communist, who embraced American ‘rugged individualism' and free-market Capitalism as the ultimate alternative. She chose as the hero of her novel and screenplay—a morality play of the individual vs. the collective—an architect, naming him Howard Roark. Her model for the character was Frank Lloyd Wright, an independent American soul if ever there was one. Indeed, she asked Wright if he would design the architecture for Roark, but he refused. So, the buildings we see from Roark in the movie are 40s Modernist, a mish-mash of Neutra and whomever else Hollywood set designers found fashionable at the time. This creates some serious contradictions in the film, in terms of the architecture and therefore the narrative. Not the least of these is that the struggle between Roark's style of modernism and the literal historicism of the corporate architects in the movie was effectively over by the 30s.


However, this lack of historical accuracy does not affect the main theme: the architect, a man of intense personal integrity, will not compromise his designs for any reason. They must be built exactly as he has made them, or he walks away from the commission. In one case, at the climax of the film, when his design for a low-cost housing project is aesthetically compromised by others during construction, he dynamites the unfinished buildings.


Rand had aspirations to philosophy, and its shows here, and not only in the linking of aesthetics to ethics. Later, she would write of ‘the virtue of selfishness,' which is the supreme right of any individual to pursue his (in the 40s it was assumed to be ‘his') own interests. Rand argued for the rights of the creative individual over groups of any kind, including established social institutions such as private corporations. Her architect in The Fountainhead claims the right to his own ideas and designs “against the whole world.” In the movie, architecture is explicitly referred to as an art. An artist of worth cannot care what others think. However, the implication of the movie's conclusion is that society is ultimately served by such individualism, that is, Roark is vindicated (after an idealistic speech in court, no less), and the virtue of enlightened self-interest is affirmed.


It is hard not to think about a remake. Would it be possible, or in any way valuable to remake this ‘classic' movie today? Are the issues it raises still relevant–or, were they ever relevant? The ‘artist against the world?' In todays's climate of artistic ‘appropriation' and ‘social responsibility,' this position seems out of date, hopelessly reactionary and right-wing, for most people. How do the young artists feel? The young architects? The Fountainhead poses the struggle between the individual architect and the corporate architects? Is that still of interest today?


And what would Roark's architecture be like? In the movie, Roark designs luxury apartment buildings, banks, and private houses for the wealthy. He also designs gas stations, farms, and factories. What building types would the remake have Roark design? Whose architecture would serve as the model?


My opinion is that stubbornly creative individuals have always been scarce and have become increasingly so. The triumph of global corporatism appears irreversible. Reading Aldous Huxley's Brave New World (1932), we find that The Savage (the stubborn non-conformist in a technologically controlled society) is not only destroyed in the end, but also that no one mourns his passing. The World works perfectly well without him.


 LW




 ## Comments 
1. [Adam Koogler](http://web.mac.com/adamkoogler)
9.23.08 / 12pm


Lebbeus, thank you for bringing up The Fountainhead at a time when many architects scoff at the mentioning of Ayn Rand. It seems that more often than not her idea are misinterpreted and often by those only with second hand knowledge of the writings. With today's competition briefs all explicitly requiring an icon and architecture students all dreaming of “starchitect” status, there could be no better time to rethink the individual. In Atlas Shrugged, Rand shows that through one's pursuit of their own greatest potential, society benefits as a whole. It is difficult to see how society or the discipline of architecture has benefitted from the abundance of empty wrappers. Sadly if the film were to be remade, Frank Gehry would probably be picked as the role model and his character would be designing wineries, museums and luxury condo towers. So perhaps this fiction has become more fact.
3. David
9.24.08 / 5pm


“With today's competition briefs all explicitly requiring an icon and architecture students all dreaming of “starchitect” status, there could be no better time to rethink the individual.”


First of all, it makes sense that competitions usually explicitly demand iconic buildings. This is because the commissions for “background” buildings are not typically awarded through competition. Competitions make up a tiny percentage of total architecture work (<1%). Further, what the iconography of a building has to do with the discussion of it being designed by an individual or a team is debatable.


I especially take issue with the notion that all architecture students want to become starchitects. Who told you this? Perhaps some do. Perhaps even most. Is college basketball harmed by most players wanting to eventually become NBA stars? 


Eventually, young architects, realize that teamwork is a key component at every firm, even ones where the public image is dominated by an imposing personality. 


Here is how society has benefited from the “abundance of empty wrappers”: they provide shelter. Architects need to realize that, for most of the world, the first-level functional aspect of architecture will always be the most important part. Bad buildings are almost always more useful than no buildings.


Of course, I say almost, because this relationship is reversed when the buildings are put up with their second-level functionality (symbol) is more highly valued. Dubai, Vegas, etc.


But, notwithstanding the abundance of writing on and photography of these places, they are exceptions.


I don't understand why it is sad that Gehry is a real version of Roark to you. Who cares?
5. aes
9.26.08 / 2am


According to Ayn Rand, though, the World doesn't work at all without its Howard Roarks; in fact, in ‘Atlas Shrugged,' which I would argue simply re-presents her arguments in ‘The Fountainhead' in a vastly more cartoonish and nihilistic way, she bases the very plot of the book with the idea that when the Roarks and the John Galts and the Hank Reardens of the world walk away, the little people deservingly die (literally, as described in the train tunnel scene).


What I find disturbing about Ayn Rand is not some notion of the heroic individual, even if that individual is cynically labeled as a ‘starchitect'; but rather the thing first hinted at in The Fountainhead, and made explicit in Atlas Shrugged: fascism. Not only political fascism, which in itself is obviously abhorrent, but cultural fascism.


I think that Rand's novel was always more about the architect than it was about architecture, about heroic personalities than heroic acts. Her heroes were always above or against the world, but never in it. Her ideas feed the notion that architects should worry about the world crushing their good ideas, and never worry whether their ideas are any good.
7. [Dag](http://www.studarch.com)
9.26.08 / 9pm


Adam Koogler  

This reminds me of the dumb boxes discussion.. if everything is special then nothing is. I think an important issue here also is the motivation behind the “individual”.. >”In Atlas Shrugged, Rand shows that through one's pursuit of their own greatest potential, society benefits as a whole.”  

If the motivation is merely stardom, i dont think society would “benefit as a whole”..
9. [lebbeuswoods](http://www.lebbeuswoods.net)
9.26.08 / 10pm


aes: As I suggest, The Fountainhead is complex and layered. I certainly agree that Rand's philosophy becomes fascistic in Atlas Shrugged and beyond. However, I think you err when you separate heroic individuals from heroic acts. There is no heroic act without a hero. Words do mean something. No one would call Chartres cathedral heroic, for the reason that it was created by anonymous collaborative groups. Great, yes. Heroic, no. Fascism requires a hero, but so do extraordinary acts of courage in war, or in a disaster. It all depends on how we view heroes–whether we idolize, and idealize them, or, less grandiosely, hold them up as examples for anyone to follow in similar circumstances. The Fountainhead, unfortunately, has it both ways. When Roark is defending the integrity of his work against the middling opinions of others, or, when he speaks plainly about his reward being in the doing of the work, he is a more down-to-earth, admirable, and inspiring hero. When he stands on top of the world's tallest building, designed by him, and the movie camera zooms in on his figure in an Il Duce pose, well….I for one am repelled.


It's not always possible to know if one's ideas are good or not, at the time when they are offered. It's taken four hundred years for Giordano Bruno's ideas to be validated by modern science. He—heroically—went to the stake rather than recant them. On the other hand, not every seemingly crazy idea needs to wait so long. That's where individual judgement—not public opinion—becomes crucial.
11. [P'lala](http://none)
9.29.08 / 12am


Despite Rand's fascisistic tendencies, the discussion of individuals — of heroes — is especially relevant given the fact that agency is so often attributed to a group, rather than the individual. 


At the same time, however, individuals have more avenues for the proliferation of their own ideas/art than ever before (making it more likely also that individuals remain anonymous, perhaps even posthumously):


With the world wide web at our fingertips and in our pockets giving rise to wonderful cross-disciplinary discussions that engage ‘space and time' — the ephemera that make for ‘architecture' vs. construction/production — as their topic…


then the question for me really becomes: what makes someone an architect? 


With so many building projects, too, headed by designers (and then stamped off on by architects who have been granted official status), thus created by a team, in which individual and specialized knowledge/skill sets demand deferral and compromise to the different strengths within the team — are our chances of witnessing/bearing true acts of heroism ever-declining?


The pace of life today makes it all the more evident that the world will keep turning without each of us; and yet, it really does matter, for example, who will be our next president.


Or who it is that might generate the ideas that can truly keep us “current” — a term that bears little longevity today.


So…even if — no — especially because ideas are today so quickly proliferated, appropriated, commodifed, bastardized, reinterpreted, the onus is even greater for individual and critical judgment — and expression.


I might argue, therefore, that any individual who has the capacity to plan for human existence within some space they create — whether it be music or poetry or buildings — could be called architect?
13. [Pyracantha](http://www.pyracantha.com)
10.2.08 / 9am


Lebbeus Woods


I have always wondered what an architect would think of Rand's book/movie and characters. Thanks for speaking about it so eloquently and without sarcasm.


I wrote about “Fountainhead” on my own blog some time ago, with an attempt at an illustration. It can be seen at:  

<http://pyracanthasketch.blogspot.com/2008/07/architect-hero.html>


I have loved your work for a long time, thanks for being so brave and persevering.
15. Alex Bowles
10.6.08 / 7am


aes,


I think you may be right about Rand's own interests, and her desire to interpret Wright through this lens. This isn't surprising, given that the Wright family motto was ‘Truth Against the World'.


But I can also see why Wright would decline to participate in Rand's project, especially when he considered his own work to be a matter of public service, and a means to establish a more harmonious relationship between man and his world through Architecture. 


More importantly, I doubt he regarded his ideas as ‘his' in the first place. Instead, I believe he considered them as natural principles – a bit like the law of gravity – which he had merely discovered. Accordingly, he was confident that these principles had intrinsic and everlasting value, regardless of whether they were understood, or even recognized, let alone explored and applied to actual design. 


And I think Wright was well aware that the world would continue to get by just fine without him. But his aim was to demonstrate that the world could do much better than simply ‘get by'. Obviously, he took great personal and professional risks to make this point, and in that regard, he is a truly heroic individual. Again, I can see why Rand would be attracted to him. 


But I sincerely doubt he shared Rand's idea that the heroic individual has an intrinsic moral value that elevates him above the ‘common' man, or that commitment to selfless acts of heroism could permit a man to become a law unto himself. I fact, I think his commitment to external principle, derived as they were from nature, would make him feel that egotism of Rand's kind was truly abhorrent, and that one was safe only to the extent that one was in harmony with something greater, and external. 


This attitude was, perhaps, encapsulated after his own home (Taliesin) caught fire and burned for the third time. The damage, though severe, had stopped short of his drafting room. “It's as though God were reproaching me for my character, but never my work” he remarked.


I'm not sure that this particular blend of humility and pride is one that Rand ever understood, let alone appreciated. And I think that Wright would be the first to say that even heroic architecture (if there can still be such a thing) demands the elevation of principle, not the man practicing it.
17. Jamin
6.6.09 / 5pm


In response to Adam Koogler, I would agree that Frank Gehry would be today's personification of the “heroic” individual, the Roark character. 


As an architecture student, I have seen many of my peers becoming dissatisfied with this representation of the profession. Gehry, Hadid, Koolhaas, et al, characterize our profession as a luxury service for a fraction of the population, not as a necessity with the capacity for real social impact. 


While Roark was designing luxury apartments and private houses for the wealthy, his side project was a housing project for the poor. This was one of the projects that intrigued him the most, that he spent his time on after his responsibilities with actual clients were complete. Frank Lloyd Wright had Broadacre City, Corbu had the Unite and the Radiant City. Unfortunately, these projects and aspirations that actually have social importance often take backseat to the commissions of the people with the deepest pockets. 


My idealistic, change the world, college aged self asks then why can't these projects with social importance be the primary focus of our work, and maybe in our free time (ha!) we'll do some work for wealthy Manhattanites.
19. Liza
6.30.09 / 6pm


In response to the idea that the Howard Roark of *The Fountainhead* by be portrayed as a caricature of Frank Gehry in a remake of the movie, there are a few comments that I would like to bring forth as an architecture student currently reading the book.


When reading *The Fountainhead*, one doesn't need to look at Roark's drawings or models to know that the buildings he has designed architecture that is beautiful because it is reasonable. The plans optimize a person's path, unlike Peter Keating's original plans that are convoluted. If there was a facade that was added on to a building that Roark had designed, the facade would destroy the central unity of the building and would reveal the cowardice of the client. Howard Roark stripped projects of frivolous ornamentation, with the exception of the statue of Dominique Francon that was sculpted by Steve Mallory for the Stoddard Temple which, as the text suggested, pulled the building together because the statue was a representation of the human spirit.


In the sense that Howard Roark would strip his buildings of ornamentation and work his floor plans to optimize circulation, Frank Gehry cannot be the caricature of Howard Roark. Gehry's buildings leak from the visual explosion of his steel deconstructivist facades. As seen in *The Sketches of Frank Gehry*, Gehry's buildings are also first considered from the outside, and then the inside. In fact, Gehry has an entire team devoted to the details whereas he focuses on the sculptural exterior. (Speaking of which, does Walt Disney Concert Hall have the acoustics correct, yet? Are the acoustics of a concert hall the most important aspect of such architecture?) From the standpoint that the caricature of Howard Roark would be an architect of buildings that optimized circulation, stripped of frivolous ornamentation, and preformed well as a shelter (without rain), Frank Gehry does not fit the description. 


Perhaps a figure more like William McDonough would be this generation's Howard Roark. He described in his TED lecture (<http://www.ted.com/talks/william_mcdonough_on_cradle_to_cradle_design.html>) a time from when he was a graduate student under Richard Meier at Yale during an energy crisis. William McDonough was designing a building that would absorb solar energy, which he later built in Iceland. He said, “Richard Meier my professor kept coming over to my desk and saying ‘Bill, you've got to understand. Solar energy has nothing to do with architecture.'…” Today, William McDonough's perseverance to erect an energy efficient building would be considered a Roark act, especially because of the current “green” trend, and LEED certifications.


On the other hand, Frank Gehry has designed architecture that has shocked, even disturbed, the public upon their first encounter with his style. In fact, we should not limits the architects with styles that my professors at Carnegie Mellon would refer to in slang as “blob” or formally as “freeform” to only Gehry. All have shocked the public upon their first encounter with each architect's style. If a person was not shocked, he or she was no doubt aware of the tension and uncomfortable feelings that others felt about the form. Similar to the reactions to Howard Roark's proposals. The commissioners were nothing without their safety blanket of corinthian columns.


However, with regard to the news from Beijing, it does not seem that buildings being erected by international architects in China have been accepted for their design. Rather, these buildings have been reject by the the shiny capitalism that they come with. Not to say that capitalism is good or bad, in fact, as Lebbeus Woods described, “Rand was a vehement anti-Communist, who embraced American ‘rugged individualism' and free-market Capitalism as the ultimate alternative.” The arson against Rem Koolhaas' CCTV or Zaha Hadid's Guangzhou Opera House has, in my opinion, a direct result to the Chinese government's ill conduct preceding the Beijing Olympics in which hundreds families were given short notice of to leave their Beijing homes that their families had been residing in for hundreds of generations so that Beijing would appear as a clean tourist attraction whose focal point would be the Birdsnest. The fact that the Chinese government prioritizes the tourism and profits from the landmarks these architects create over the well being of their citizens leads me to believe that the only social gain of freeform starchitecture is from the free form. In contrast, William McDonough, as he described in his lecture, was designing a project that would reactivate a Chinese town's agricultural and energy self sufficiency. 


Interesting discussion, thanks.
