
---
title: ARCHITECTURE SCHOOL 301
date: 2009-03-18 00:00:00
---

# ARCHITECTURE SCHOOL 301


Students in the RIEA collaborative urban workshop, Taormina, Sicily, Italy:


![lwblog-archschl301-1](media/lwblog-archschl301-1.jpg)



If there were to be a new and independent school of architecture formed today, what would be its philosophy, program, and course contents? The answers are as diverse as the faculties who would want to join them. But also, the direction of the school—and any school needs a clear direction—would be as precise as its dean and chairs could make it. In other words, there could be many schools, each offering a distinct approach to architecture.


It must be said that this prospect is radically different from today's situation, in which schools are homogenized by imposed professional standards, enforced by accreditation boards and the like. Of course, the accreditation boards and their touring committees like to believe that they are encouraging diversity, not homogeneity. And of course, each school likes to believe that it is unique—this is a very poignant human need: to believe in one's uniqueness. It is the same with architects, and students in schools of architecture. But the question remains, how best to achieve individuality in a field that is both interdisciplinary and collaborative, and at the present time largely dominated by conformity?


The current design studio system in schools encourages students to work independently and to come up with their own ideas. These are then put up in reviews next to one another and judged in isolation. It is the perfect model of ‘free-market' competitiveness. The best students stand out and are praised. The less-than-best (by current standards) are less praised or criticized for not being good enough, which tacitly translates as losing in the competitive marketplace.


This approach, which emphasizes (at least for the present) uniqueness and originality, at the expense of camaraderie and the sharing of knowledge, does not work well after students graduate and enter the world of professional practice. Many graduates, entering practice and finding that their individuality is subordinated, become disillusioned, discouraged and, considering their experiences in school, understandably so. Of course, the occasional genius emerges and struggles to break out of the given hierarchy and establish himself or herself—but the genius will emerge from any sort of system, even the most rigidly conformist, and find a way. Our concern in education should not be the nurturing of geniuses but of encouraging and helping aspiring young people, who want to contribute their idealism and talents—of whatever magnitude—to a common good, and to do it through the design of architecture. This way of thinking should be encouraged in their schools, but as yet it is not.


It is clear that the corporate model as we know it in business and architectural practice today does not nurture individuality in this sense. It subsumes it in the process of attaining a corporate product. Thus, in effect, it discourages it, if in a different way than contemporary schools.


I am convinced that a more embracive and ultimately more effective way to nurture individuality is the establishment of design studios that operate, paradoxically, on the basis of collaboration. I am not thinking of the sort of corporate collaboration where everyone's effort is blended anonymously into one result, but, rather, a type of collaboration where each individual work is still clearly legible within the collaborative whole, even while contributing to a collective effort, a common end result. This is an important distinction, though today not well understood because it is so seldom practiced.


We might think of a city as a model of individuality and collaboration. A city is an accumulation of many buildings, built over time. Most follow the prescribed rules and are rather similar and I suppose we would say, ordinary. A few bend the rules and stand out as singular achievements of thought and skill. On rare occasion, a building will break the rules and become literally ‘the exception that makes the rule,' establishing new rules that amount to possible changes for the future. The point is, though, that each building, from the most ordinary to the most innovative, makes its contribution to the landscape of the city and the ways of living in it. In a city, at least a relatively democratic one, differences are accepted, and their terms of engagement, the conflicts between them, are negotiated. The ideal aim is not a homogenized whole, but a complexly evolving, dynamic balance of differences.


A new type of design studio and perhaps an entire school could be organized and conducted beginning with this type of urban condition as a model. However, being a school—in effect a laboratory for learning—there could be considerable experimentation with the ways individual projects might be combined in collaborative landscapes. Some preliminary explorations with this type of design education have already been made, with promising results.


 *(To be continued)*


*LW*


 




 ## Comments 
1. [Mark Primack](http://www.markprimack.com)
3.18.09 / 12pm


I have not been around an architecture school for a long time. I had applied to three and, being a lousy high school student, was fortunate to have gotten into the one I did. Years later I came to realize how different my life in architecture would have been had I attended either of the other schools. For the student, education is often a toss of the coin.  

 Samuel Mockbee's Rural Studio hovers over this topic like a ‘special child', the cousin unlike the others at family gatherings, content or resigned to play or read alone in the garden while the other children shun or ridicule him, as occasion allows. It's only years later that the family learns of the patents or the Pulitzer or the beatification.  

 Would an ‘Urban Studio' need to distinguish itself within the rhetorical context of cosmopolitan architectural academia to be believed?
3. Tomek Gancarczyk
3.18.09 / 2pm


LW : You probably seen many of architectural studios, for example being guest critic. Which one of the existing studios do you consider as having those kind of qualities as you mentioned above?
5. [lebbeuswoods](http://www.lebbeuswoods.net)
3.18.09 / 2pm


Mark Primack: The Rural Studio had (has?) the right spirit, but it was of an older type of collective work, in which individual work is subsumed in the common effort. I am speaking a ‘new' kind of collective effort, where individual works remain legible. 


I don't think a new type of ‘urban studio,' as you might call it, needs the approval or legitimization of the current academic regimes. It could only establish itself independently, then grow, if at all, on its own merits.  

I do think that some strong students would be attracted to it, even at the cost of not obtaining a professional degree from such a school.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
3.18.09 / 2pm


Tomek Gancarczyk: I don't know all that is going on, but there are none that I know of at the moment.


As you might imagine, I have taught a number of such studios at various existing schools. See, for example, my previous posts MATRIX and PROTO-URBAN CONDITION on this blog. However, within the existing academic programs, which emphasize a different approach, such studios tend to be short-lived, and their effect is not supported when students return to ‘normal' design studios.


In my next post on the issue, I intend to spell out principles and strategies that can be used to set up studios of the collaborative type I mention here.
9. Joel
3.18.09 / 7pm


I'm not quite sure if it qualifies as the “collaborative studio” you describe, but my final studio in my undergraduate education held some of the qualities you mentioned. It was a topical studio based around improving conditions in slums. Each student chose a different city to research and based upon that research, developed a project to deal with certain conditions in the slums. Projects varied widely, from the designs of urban farming carts to water containment systems, to large urban infrastructure projects. The variety of projects meant that competition between each other was low, yet because we were all dealing with the same issues, cross-pollination of ideas was rampant. It was a great experience. Unfortunately some in the school apparently didn't like the looseness of the studio and future iterations have featured a far more structured and narrow course plan. My guess is some were afraid we weren't doing “architecture”, which to an extent we were not, we were doing something far more worthwhile.
11. Pedro Esteban
3.19.09 / 3am


This article sounds exciting. 


Actually, I prefer knowledge more than my graduation certificate from Havana.  

I see in this school the solution for one of my constant questions.  

What could happen to cities where architects design with concepts like chaos, random or those “new ways”? Today exists a pluralism in design, we can find minimal, regional, ecodesign, and chaotic design, but when there only exists chaos, is the citizens psychological perception of their cities entirely chaotic? If the architects have the qualities for working together, all design emerges homogeneous, of course with they own point of view, but unlike today when everyone has his very particular perception.
13. ytiffanie
3.20.09 / 7pm


YES – isn't this a continuation of the argument over the extreme disconnect between design education and the design profession?  

many students in my school lament and have simply relegated ourselves to accept that the traditional school ‘studio' environment we know and love so much disappears after college and grad school. 


with regards to collaboration….in my studio at MIT's undergrad program, one of my favorite projects was when we each designed a hanging house in Boston's north end, each of them at a different site along one street, and it culminated in a sort of [‘community in the sky'](http://ytiffanie.wordpress.com/2008/12/29/elegy/) at the end. there was extreme individuality, but there evolved a sense of common urban fabric that we had to maintain. 


i do think that infusing collaboration into the studio institution one way that we can ‘improve' design education to become more analagous to the profession. but…do we really need to do that? is ‘real world' something that education must always strive to fit?
15. [lebbeuswoods](http://www.lebbeuswoods.net)
3.20.09 / 8pm


ytiffanie: The irony is that schools today believe they are preparing students for the competitive marketplace of professional practice. That is the sole justification for the imposed standards of education and the resulting homogenization of schools. The following are some thoughts I wrote, but, for reasons of clarity, left out of the post:


“Under the current accreditation standards, a school is expected to educate the student architect in history, technology, structures, theory, and other technical and academic topics deemed necessary for an understanding of architecture and its role in society. This diverse knowledge is intended to be brought together in the design studios, where it is synthesized in students' design projects, supervised by design teachers and various critics. In theory, it should work, because it is in fact a **rehearsal** [emphasis added] of what architects are expected to do in practice, when they work with clients and others in the design of buildings. So, after a series of in-depth rehearsals over several years, the students graduate and move into practice, first under the supervision of already licensed architects, and then, with their own licenses in hand, independently. 


The accreditation process is preparing students to practice architecture in a particular way, with the goal of creating a particular kind of architecture—the buildings we see going up everywhere today. Most of these buildings are structurally sound, and cost effective, in that they earn a return on the money invested. They are uniformly mediocre in all their aspects, reflecting the effectiveness of standardized education. They serve their intended uses well enough, at least those defined by accepted models of living and working. It is hard, as the saying goes, to argue with success, even of this limited kind.”


I would only add that the prevailing educational system emphasizes individuality, but only as a way of identifying future leaders in the existing corporate structure of architectural practice, not as a way of forming creative alliances and collaborations. Most graduates, even talented ones, but who lack the mentality to climb the corporate ladder, have to downsize their aspirations and expectations, or get out.


One more thought. K. Marx wisely noted that ‘philosophers try to understand reality. The point, however, is to change it.” It is time, I think, to change the reality of architectural practice, and we should begin by changing architectural education.
17. Pedro Esteban
3.21.09 / 1am


K. Marx wisely noted that ‘philosophers try to understand reality. The point, however, is to change it.” It is time, I think, to change the reality of architectural practice, and we should begin by changing architectural education.  

Good point of Marx. 


How long we need to wait for that?  

The show must began.  

I need facts, in all the globe, luckily are people thinking on that.
19. penelope
3.21.09 / 10am


ytiffanie: I agree with you that students of architecture after graduating they/we really miss the design studio. And I think what we miss is the space of dialogue around ideas. But, I think this space of dialogue established in design studios (most of the times into a one-to one criticism/praise/disapproval), could be structured in such way as to urge students into collaboration within each other, and therefore this space of dialogue would need to be created by them. 


This might give us hints on how to perpetuate our research interests and the ‘design studio' energy even after we leave the school of architecture. And nowadays this is more in need, because the expansion of knowledge and specialisation, the advancement of technology demand creative collaborations in the field of architecture. It is impossible for an architect to know everything and be everything.
21. [Michael](http://www.nebulousideas.com)
3.25.09 / 8pm


It seems as though the lack of organization in educational “milestones” would produce a more individualist process. My own personal thesis studio professor approached the final a year by not establishing a set of goals and requirements…instead developing a process (to each his own) and discovering how that exploratory work evolved over the year. Some produced details down to the final wall sections, while others had 17 conceptual study models of colliding circulation. 


Its in the hands of the architectural educator, prospective young architects be warned.
23. [joehagedorn](http://www.visual-results.com)
3.27.09 / 8am


Since the notion of corporate business models and competitive free market response has been made in your argument for collaborative learning, you may indeed be able to further your cause from another subject. Namely the general public. I'm always surprised by the divide in how the general public perceives the end product or presentation of the finished stages of design versus how designers see their work at the same moment. Perhaps the rural studio did not promote solely individual movements leading to collective cause but simply taught effective dialogue between eager and energetic designers and the people that designed products serve. Now we are hearing of such divide in NYC WTC development. Why?
25. [Miguel Jaime](http://theblogofarchitecturaleducation.blogspot.com)
3.27.09 / 10am


I am glad to see the subject “collaborative studio” raises. I have been introducing some elements of Vigotsky pedagogical philosophy in my design studio (3º year) for the last three years now. I can tell you that the results has been astononishing. We have presented the experience on a benchmarking congress on education and other faculty were less enthusiastic than my students that live the experience. The implementation of the Construccion of Common Knowledge techniques is what characterizes my “Reflexive Architecture Laboratory” in Segovia, Spain at the IE University. You are welcome to follow in the blog <http://theblogofarchitectural> education. I am interested to know new experiences and waiting for the “principles” Woods is going to publish in the next post.
27. acatects
3.27.09 / 3pm


some ideas are hatched in the island of a single mind  

others from sea between  

one need not pressure aloneness to conform nor collaborate


thinking knows few bounds  

it alights today in one mind  

tomorrow in many


we only claim it for the briefest of time
29. v
3.28.09 / 1am


Mr. Woods,  

I don't know of any of your buildings? Wait, you have not built anything…so you preach.  

Shut up! Perhaps all architects should learn to tone down the bs and focus on actually doing their work…build buildings!
31. acatects
3.28.09 / 3am


maybe the world has enough buildings  

but very much needs some thinkers
33. [Harald Brynlund-Lima](http://wwww.flickr.com/people/brynlund)
3.28.09 / 7pm


I have completed 3 of 5 years at the Bergen School of Architecture. I understand it as an alternative school that has challenged many of the traditions and norms of Northern European architecture. Please have a look at its website: <http://www.bergenarkitektskole.no>
35. [Rex Thomas](http://www.poolsidestudios.cc)
4.2.09 / 11am


In the beforetimes, architects and artists studied in the same classes, worked together on creative projects, and were two foci on the same ellipse. The two disciplines have since drifted apart, as evidenced by our current built environment; a reconvergence is badly needed. At the University of Florida's Graduate School of Architecture, art and architecture are allowed to reconmingle, as expressed by the current crop of Master's Research Projects being completed this year. The students going out into today's profession bring with them the strength of creativity and ideas to change our cities.


Graduate school has traditionally been thesis-driven, with the culmination being a defense of the thesis before committee. In the Graduate School of Architecture, this is preceded by an architecture project. Some students this year chose projects in Florida, but most travelled in search of the strange and exotic, bringing with them brick kilns from Tanzania, earthquake rubble from Sichuan, ice from near Vladivostok, and pieces of the West Bank wall. Each personal experience caused some big questions to be asked, and most students tackled these questions with the spirit of inquiry and desire to dig as deeply as possible into design, as well as broader issues relevant to our time.  

Faculty encouragement of more collaboration with fine arts could, if it is a reaction to the computer, perhaps spark a reconvergence of art and architecture. The profession and academia maintain an uneasy relationship, and such an trend could potentially widen the gulf. However, it has greater potential to close the gap and provide a new path for integrating more design into our future spaces.


For the full text click <http://www.examiner.com/x-3237-Winter-Park-Examiner~y2009m4d2-Art-and-architecture-reunite-at-the-University-of-Florida>.


Rex Thomas
37. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.19.10 / 10pm


[…] lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 […]
39. [Cluster | City - Design - Innovation » Architecture as the Solid State of Thoughts: a Dialogue with Lebbeus Woods - Part 1](http://www.cluster.eu/2010/11/18/architecture-as-the-solid-state-of-thoughts-a-dialogue-with-lebbeus-woods-part-1/)
11.21.10 / 10am


[…] lebbeuswoods.wordpress.com/2009/02/27/architecture-school-202 lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 […]
41. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] In the Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
43. [Lebbeus Woods on Architecture School | Architecture EV](http://architectureev.wordpress.com/2011/01/02/lebbeus-woods-on-architecture-school/)
1.2.11 / 6pm


[…] School 101 Architecture School 102 Architecture School 201 Architecture School 202 Architecture School 301 Architecture School […]
45. [blog wunderlust: 27 March 2009 | Architecture Design & Trends](http://architecture-trends.com/?p=857)
6.8.11 / 11am


[…] floor plans | the Florence Institute | Stern comments on Women in Architecture | Lebbeus musings on architectural education | ditch the architect […]
