
---
title: DIVIDED CITIES
date: 2009-03-16 00:00:00
---

# DIVIDED CITIES


*An important new book,* ***Divided Cities,*** *by Jon Calame and Esther* *Ruth**Charlesworth, has just been published.*


Below: The Israeli ‘security fence' in East Jerusalem; sectarian division line in Belfast, Northern Ireland; and the Green Line in Nicosia, Cyprus.


![lwblog-jerusalem1](media/lwblog-jerusalem1.jpg)


![lwblog-belfast11](media/lwblog-belfast11.jpg)


![lwblog-nicosia1](media/lwblog-nicosia1.jpg)


 


Foreward to **Divided Cities**:



The five cities under study in this book [Jerusalem, Nicosia, Beirut, Mostar, and Belfast] are vitally important to an understanding of the contemporary world. Each is different, in that each emerges from a unique historical background, belonging to a quite particular and localized set of cultural conditions. Yet, each shares with the others a common set of existential factors, belonging to what we might call an emerging global condition. Prominent among these is sectarianism—a confrontation of differing, though not necessarily opposed, religious beliefs, leading to widespread violence—and a stopgap solution focused on the physical separation of conflicting parties and communities. The other feature shared by the cities under study is that the stopgap solution of separation, intended as an emergency measure to prevent bloodshed and disorder, turns into more or less persistent, if not permanent, division. No one intends to create divided cities as a long-term solution to sectarian violence; rather, such cities emerge from the seeming intractability of the conflicts and their causes.


The story of the present is increasingly being written in terms of religious conflicts. The secularism of the West is exposed through globalization to the sectarian quarrels that bedevil many regions of the world. Western institutions of government and commerce, which operate according to democratic processes or market-savvy principles, are no match in single-minded determination for those elsewhere driven by religious fervor.  On the defensive, they have hardened their own positions accordingly. At the same time, Western politicians are not above exploiting religious differences to disguise neo-colonial ambitions.


Going against two centuries of growing liberalism in the West, there are many new walls—physical, legal, psychological—being hastily thrown up in the interests of ‘security'to separate ‘us' from ‘them.' This goes beyond realities of gated communities for the rich, and restrictive, ethnically biased immigration laws, extending to attempts to seal entire national borders. If the perceived threat of religious warfare increases dramatically, it is conceivable that, not only in the West, ghettoes and internment camps may be deemed necessary. Looking to the future, this is the worst-case scenario. Can things possibly get that bad, in this information-enlightened age?


To insure they will not, it is necessary to understand not only the tragic mistakes of the past, but also the dynamics of the present in terms of the polarization of peoples and their communities. Critical aspects of these dynamics are revealed in this book, for in the physical divisions of Beirut, Nicosia, Belfast, Mostar, and Jerusalem the means by which fear and misunderstanding are given physical form are revealed in all their dimensions. Once in place, the barriers separating disputing groups become the mechanisms for sustaining the urban pathology of communities at war with themselves. The right thing, as this book inspires us to imagine, is to remove the barriers and replace them with new openings for dialogue and exchange. The best thing, however, is never to build the barriers at all. It is to that distant, but attainable, goal that I believe this book is dedicated.


LW


Where to find the book:


<http://books.google.com/books?id=PkvROgAACAAJ&dq=Divided+Cities,+Calame>




 ## Comments 
1. marzipan
3.17.09 / 5am


Too bad they forgot one other place: Ambon, Indonesia. The sectarianism is truly unique… You don't have physical walls but there every block of houses is a wall in itself.


Will be looking out for this book.
3. [Marcello](http://marcellodicintio.blogspot.com)
3.17.09 / 6pm


This is a topic close to my heart and I am glad I found this blog. I'm working on a book about communities in the world that live in the shadows of walls, fences and other kinds of ‘hard' barriers. Not quite ‘divided cities', but divisions nonetheless.


I am finishing my research in the West Bank right now, and will fly to Cyprus tomorrow morning. If anyone is interested in following my project, you can check out my blog: marcellodicintio.blogspot.com.


Thanks for posting this. I look forward to reading ‘Divided Cities.'


M
5. Spencer
3.21.09 / 9pm


I'm glad you mentioned these barriers exist not only as physical ones but also take other forms (or non-forms) that influence our spaces through representation. Hopefully, understanding and dismantling these physical walls will provide us with a path to remove the other unseen walls that are equally impeding to our cultures.


When considering that these walls are only meant to put something between the disputing and not really considered a method of resolution I have to wonder if its all really worth the expense and does the wall builders really care about finding common ground?
7. [Worldtravel » Blog Archive » Peripheral Milit\_Urb 29](http://tourist.bl0x.info/peripheral-milit_urb-29/)
6.7.09 / 11am


[…] City of walls // Divided Cities // A Year in Iraq and Afghanistan // Pentagon Rethinks Old Doctrine on 2 Wars // The Economic Cost […]
9. [Eric Kahn](http://ideaoffice.net)
5.3.10 / 3am


Hi Leb,


Just read this and ordered the book. SCI-Arc is doing a vertical studio in israel this summer…I'd like to speak to you about it some time…


thanks for this post/intro
11. [Peripheral Milit\_Urb 29 - D+STREAM](http://www.duranarquitectos.cl/stream/?p=43)
6.28.10 / 2am


[…] City of walls // Divided Cities // A Year in Iraq and Afghanistan // Pentagon Rethinks Old Doctrine on 2 Wars // The Economic Cost […]
