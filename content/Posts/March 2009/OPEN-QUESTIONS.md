
---
title: OPEN QUESTIONS
date: 2009-03-10 00:00:00
---

# OPEN QUESTIONS



*Church on the Berlin Wall*, Raimund Abraham, architect, model and early sketch:


![lwblog-arch-3a](media/lwblog-arch-3a.jpg)![lwblog-arch-3b](media/lwblog-arch-3b.jpg)


An issue that continues to come up on this blog is whether only an actually constructed project is architecture, or whether drawings and models of unbuilt designs can be architecture, too. There are good arguments on both sides of the question, so, in a sense, we cannot expect a definitive answer. The debate, however, remains useful in establishing different philosophical and practical positions within the field, and so we carry it on.


 Not surprisingly, my own position is that drawings and models are architecture in their own right. If they are not, I ask, when does the architecture appear? Architects do not build buildings; rather, they design them. Others do the building, following the architect's designs, realized in drawings and models. This position is based on the assumption that not all buildings are architecture, but only those that embody the knowledge and understanding that only architects can give them. I do not consider a simple utility shed built by even skilled craftspeople architecture, unless it embodies conscious ideas about human habitation and its meanings. For those who consider any constructed building architecture, then not only are drawings and models not architecture, but architects themselves are, at best, superfluous. Most buildings actually constructed in the world are not designed by architects. Yet if there is no architect, I believe, there cannot be architecture.


 Now, I want to define better what I mean by ‘architect.' An architect is one who has an active knowledge of ethics and aesthetics, as well as technical matters, and is skilled in embodying these in precise forms and spaces. Further, an architect is able to make designs that integrate these into the fabric of a city or town or landscape, harmoniously or not, but in any event according to a critical position taken by the architect. To do this, an architect must have a developed understanding about the way a city or town or landscape works and also how it *should* work, given the right conditions. An architect works to maximize these conditions. Therefore, an architect must have a wide and comprehensive knowledge of the world he or she inhabits, however large or small its extent, in order to have a clear idea of the best conditions to enable and encourage.


 I should emphasize that an architect need not be licensed by the state, or even be a graduate of a school of architecture. Mies van der Rohe and Frank Lloyd Wright were basically self-taught architects, as were countless unknown architects throughout history, and in many parts of the world (see Bernard Rudofsky's [“Architecture Without Architects”](http://books.google.com/books?id=L8pPAAAAMAAJ&dq=Bernard+Rudofsky&source=an&hl=en&ei=LJu1SeXpCorEMduMlbAO&sa=X&oi=book_result&resnum=4&ct=result)—note that I dispute the book's title). Whether self-taught or the inheritors of a coherent cultural tradition, the definition of architect given above applies. Architecture does not ‘just happen.' It is designed by someone with comprehensive knowledge and skills, and that someone is an architect, by whatever name they, or we, choose to give.


 The question comes down to this: is architecture only an end-product that people can physically inhabit, or is it also the matrix of ideas, concepts, and designs that serve as inspirations for constructions that can be inhabited, but lack the financial support, or the technical capacity to be physically built? Or, perhaps, architecture can exist only as forms and spaces that can be inhabited mentally, until such time—if ever—when they can or need to be physically constructed. It seems to me that if we exclude these latter modes of architecture, we narrow the field to what is built at the moment, limiting as well its potential to fully engage the human condition in a time of tumultuous change.


This is an important issue. The views about it that prevail will greatly influence not only the direction of architectural education but also the priorities architects, clients, and public policy-makers set for the field in the future.


Let the debate continue.


LW


*Monastery Simon Petra*, on Mount Athos, Greece:


![lwblog-arch-21](media/lwblog-arch-21.jpg)




 ## Comments 
1. [Michael Knutti](http://cafe-warszawa.blogspot.com/)
3.10.09 / 4pm


I would like to agree to your position. It is even so that sometimes what is on paper has more architectural qualities than the built outcome.  

you could actually expand the above mentioned terms of architecture without architect by inverting and saying that sometimes buildings by architects are devoid of any sense of architecture. They have become complex machines, but are not, as in the definition given, anymore responding to the questions of space and aesthetics etc.
3. David Pearson
3.10.09 / 9pm


As a soon to graduate thesis student of a B.Arch program, a good friend and I have been asking this question a lot. As students, we have been asking when does one become an architect? We know the AIA stipulations and the general public's notion of architect but as the above argument suggests it is not always so and we agree.  

As students, we see ourselves coming from a “world we did not create” (LW) but have been engaged in and developed the curiosity to educate (not schooling, but education; Mark Twain) ourselves toward becoming architects. We chose to go to university, so we are learning to bring order to matter through drawings (which models, paintings, collage, etc. have been lumped into). Through these we develop aspirations to produce architecture. Where we get stuck reconciling the above argument with our own views is where not built architecture comes into play. When our shadows meet the shadow of a piece of architecture we have designed, we are becoming architects. This is important because matter has been given order through drawings and then exercised and manifested through craftspeople. At that point one can better understand that organization of matter (like the blacksmith learning through sensation, DeLanda). How does the unbuilt fit in here or have we neglected key parts? Perhaps we are neglecting the drawing's use as the blacksmith like material informer.  

Any discussion on this would be appreciated.
5. Pedro Esteban
3.10.09 / 11pm


In Havana the people have become more architects than the architects themselves. Because here the profession is so unrecognized (and other problems too) the people have developed their own architecture( of course whitout any aesthetic sense and charged by kistch) according to the economic problem and their own thought of comfort, and they resolve their own problems whitout any architects, resolving problems from function to structural topics.
7. [lebbeuswoods](http://www.lebbeuswoods.net)
3.11.09 / 12am


Pedro Esteban: For centuries, people have been their own architects, and often very good ones, when there was a local tradition of building to inherit and emulate. Today, so many local traditions have vanished, due to economic pressures, and therefore can no longer provide guidance to local builders. People improvise as best they can. Sadly, architects give no guidance, because they are more concerned with getting prestige commissions, in whatever form they are available. So the older fabric of cities deteriorates.


Kindly look at my proposals for La Habana vieja, made fifteen years ago, after my visits to the city (lebbeuswoods.net—under Work, Projects, Havana). They attempt to provide some guidance to local builders, enabling new spaces of transformation for the Old City.
9. Marc K
3.11.09 / 12am


An interesting duality exists here. Where the architect's ownership of his or her own work meets the built environment. Credit is always given to the architect, when a project is completed, having those who actually constructed it to fend for themselves. And when the problems occur, it was the fault of the builders. Fundamentally, one's own work is what one produces himself, and therefore the work of others in lieu of the architect's direction is in some ways taking the credit for work one did not do himself But as we may say, the built environment could not exist without some form of architect.


The true hand of the architect then exists in the realm of representation. And if one chooses to forgo the built in order to discuss, teach, and question, then so be it. Que sera sera.
11. Marc K
3.11.09 / 12am


Pedro, my friend, that is the true nature of the vernacular.
13. Jorge Javier
3.11.09 / 3am


The idea of architecture as art, and therefore as a human expresion follows the constant need of improving ourselves as human beings, however, the word ARCHITECTURE that comes from the greek (i think) is higher construction, no more no less, anyway, in recent times the proposals that has been invading and amazing us are the true soul of things to come. Beside self-building with a sense of space, architecture follows nowadays a path toward perfection, a true pressence of what we feel, imagine and create. I consider that models, 3d-models, drawings and any kind of sketch is the proper way to understand architecture, no one can visualize a building or any kind of 1:1 object if it wasn't observed and mistaken within the paper board.
15. Pedro Esteban
3.11.09 / 6am


We gad a problem whit the text(Oh!Derrida, rather my English).


 I refer to the bad things who people are doing. That's “new architecture” don't will be never vernacular architecture, that creations are solutions for problems, resulting of the sociologycal realm. Studies done here demostrate ostentation from the owners, of course, responding to a necessity on they family structure. They sense about aesthetic is completely wrong( another result of sociology). This is a big trouble here cause we don't have a big vernacular heritage( apart from Cuban architects think). It's true than people are resolving his troubles, “por sus propios medios”(persons who build it's houses whitout any monetary help of the state,-by his owns means-) whit more cuality than an architect, but that dosen't means quality, aspect who have the architecture whitout architects before '59.


Lebbeus I'm really sorry for tould you this, and I need to apologize for them, but Project Havana is a laught to the Cuban architecs, who dosen't want to see the problem. And what could think a student of second year, watching their teachers so blinds? 


Proyecto Habana Arquitectura otra vez, it's an important book if we want to save this city, is the vision who dosen't have the Cuban architects, those who live whit the problem, those who think: “that don't could be constructed”, those who know but don't says anything.  

Your project it's amazing for me Lebbeus, my favorite is the MetaInstitute, it's really good the concept around that building ( It make smile about other things who I prefer don't discuss here)
17. slothglut
3.11.09 / 6am


dear lw,


another great entry, as usual. i really enjoyed your education series + was left speechless. for this post, i finally found my voice back. 


first, i like how you applied the argumentation logic ‘unless you prove me wrong, i'm not' [not unlike ‘thank you for smoking' style], in the beginning of this post. second, by putting self-positioning of an architect as a ruling parameter to consider whether a piece of work could be put under the category of architecture or not, you somehow render a political aspect of being an architect as an individual. it is a liberating point of view, but on the other hand, it could also push ‘it's the singer, not the song' attitude. don't you think?


in indonesia, where i come from, the profession and this exact debate are under an enormous spotlight, because finally it's put under the position it deserves. in front of the larger public audience, a lot of questions regarding what should be considered architecture, and therefore its practical implications, are posed as i write. from practical law, the importance [and also the lack of vision, of course] of the professional association, up to debate between ‘paper' vs. ‘verbal' vs. ‘built' architecture are still a very heated debate. needless to say, it is a crucial time. reading your opinion on this issue is definitely helpful. tQ.
19. [serraglia](http://www.serraglia.com/)
3.11.09 / 9am


Yes,  

not all buildings are architecture (unfortunately),  

because  

to build is just another step.  

It is a temporary results from the mental/physical process done by one person or more (architect, worker, etc…)  

Later  

only people and time,  

living,  

using that place,  

they will un/consciously “define” if that building is  

or  

not  

an Architecture.


Sincerely
21. ysgs
3.11.09 / 1pm


Very interesting questions indeed,  

My main concern regarding the definition of Architecture as one that can only be achieved by an architect is that it leaves some of our greatest cities architecturless.  

Vernacular cities, such as Delhi, the old city of Jerusalem, the medieval European city etc have, in my opinion, supplied us, through time based processes of selection, (destruction, re-modification, reconstruction etc), with meaningful knowledge of architectural ethics and aesthetics. It would thus just be a shame to take these off the architecture list.


I would therefore most likely go with the ‘everything is architecture' point of view. Or in different words, every spatial barrier which nears or exceeds the scale of a human being could be considered as architecture when it is applied to change our spatial perception.


Under this definition, the architect would be the expert in the art of applying such barriers (or not) with an “active knowledge of ethics and aesthetics, as well as technical matters”  

Working from within the physical surrounding to enable a (maybe better) physiological perception of the space


The architect does not need to actually construct the barriers. His job would be to determine how to build, and what needs to be built, and to provide a better spatial (architectural) organisation of a certain situation, or under the words of this definition; a better architecture.  

If it would then be built or not, is out of his control, and exceeds his line of specialities, as it is well known that often not the best architecture gets realized.
23. Nathan Bishop
3.12.09 / 2am


I have a significant concern with the separation of architecture and building. Perhaps it is the egalitarian in me, but to define architecture as separate from a building is to raise architecture to the level of elitism. “Noblesse Oblige” was a brilliant essay, but does not convince me as much as I would like. It sounds like a cop-out. ARCHITECTURE is much too controlling for “Noblesse Oblige” to be accepted outright. I would define architecture as anything human created/crafted which defines or alters space. The suggestion that an architect needs to embody a knowledge of “ethics and aesthetics, as well as technical matters, and is skilled in embodying these in precise forms and spaces” suggests that any building which does not embody these traits is not architecture. However, all building do embody these traits (most defacto, but all consider them), and some fail miserably because they do, in fact, feature these traits. One of the significant problems of architecture is that precision of form and space of which you speak. Architecture, in the formal sense implied by LW's essay, is a game of control, and thus must fail if its control is lost. Michael Benedikt, in “For an an Architecture of Reality,” suggested:


“The dumb and inexplicable features of old and/or vernacular buildings, otherwise so straightforwardly organised are often precisely those that attract us to inhabit them. Offering opportunity rather than giving us direction, they are indifferent to our designs on them. They were here before us, they are “wrong” in a way that challenges us to possess them creatively.”


It sounds like a ‘dumb-box' argument, but it is a great example of ARCHITECTURE which, although not expressly informing ideas of inhabitation, results in an effect of our inhabitation of space. 


This whole argument is predicated on the belief that ARCHITECTURE, in the LW definition of it, has value. Architecture does not have value. I think of beautiful comments like Charles Jenck's famous statement about Pruitt-Igoe: “the day modern architecture died.” Architects love that comment because it reinforces the belief that architecture has value. But it does not. That's why this argument is so elitist. I was once depressed by the fact that architecture has no value, but now I am free. I consider myself to be one the luckiest, and yet, one of the most useless people around. I watched a video of Liz Diller on TED, and I felt like dropping out of school and moving to the hinterland just so people would never remember me and my path down THAT road. She makes me depressed to be pursuing architecture. Architecture of a “matrix of ideas, concepts, and designs” is not architecture, they inform buildings.
25. [Mark Primack](http://www.markprimack.com)
3.12.09 / 2am


Let's try to simplify things. I think we can accept that an unexecuted building design of Frank Lloyd Wright qualifies as ‘architecture'. Someone might even deem one of those to be his greatest work.  

 But let's not forget that when Adolf Loos pointed to a fresh grave and declared it ‘architecture', he wasn't declaring the gravedigger an architect.  

From LW's and YSGS's excellent references, we should conclude that not all architecture- as they define it- is the work of architects, and there's overwhelming evidence that not all architects create architecture. So let's complete the circle and admit that many people can and should draw architecture, we can all learn from their efforts, and drawing alone does not make someone an architect. Why is that such a hard thing to swallow?
27. [lebbeuswoods](http://www.lebbeuswoods.net)
3.12.09 / 3pm


There are a number of intelligent and strongly felt opinions here. Before I make some specific replies, I want to say this: one of the main reasons I insist on tying architecture to architects is that I want them (us) to take personal responsibility for the state of the field. It is an ethical issue. As soon as we detach architects, who are empowered by the state and the society, from what gets built, we relieve them of the responsibility for it. Saying that our built world is the product of anonymous cultural forces leaves no one accountable, and no one who can change it for the better. It will just (continue to) ‘happen.'
29. [Mark Primack](http://www.markprimack.com)
3.13.09 / 7am


It is indeed helpful to define the field. A person freely drawing architecture is free of responsibility to state and society. When Corbu said of the Parthenon, “It's easy. No plumbing!”, he was by default including drawn architecture. Free of client, bank, ordinance, payroll, or liability (responsibility); free of code, collapse, mildew, inaccessibility, trespass or leaks (plumbing). Was anyone ever arrested, sued or bankrupted for simply drawing a picture of a building? The human body, certainly, but never a building. Drawing may be an expression of ideals, fantasy, rebellion, narcissism, inspiration or reverence. Drawing can open eyes and inspire change, but drawing alone is no true test of one's ethics or responsibility, any more than words alone.  

 Frank Lloyd Wright began a speech to the Architectural Association of London with these words, “Talk is cheap.” Articulate as he was, and as talented a limner as he was, he made buildings, lived, worked and walked in them. Apparently that's what made him an architect. And that's what made him a credible- and relentless- critic of his peers in the profession.  

There is no hierarchy proposed here. I am as awed by Lebbeus' drawings as I am by the monastery at Mt. Athos. It is important to make the distinction between artist and architect, so as not to denigrate or diminish either.
31. [lebbeuswoods](http://www.lebbeuswoods.net)
3.13.09 / 2pm


Mark Primack: You state your case very clearly. There is one point where you are very wrong and that is when you say, “A person freely drawing architecture is free of responsibility to state and society.”


When an architect designing a building follows building codes and other regulations, he or she is not being particularly responsible, because there is no other choice. Anyway, these sorts of restrictions only establish minimum standards for the public health, safety, and welfare—many mediocre and incompetent architects hide behind claims of this type of responsibility. 


No, I believe that an architect must be guided by an inner, personal sense of responsibility, to the public, to the field of knowledge we call architecture, and to his or her own sense of right and wrong, and what is the best thing to do in a given situation. These may well go beyond the minimums upheld by legal or even customary restrictions on design, and often should. The few innovative architects whose designs are built, find ways to get around or expand the interpretations of codes and regulations. 


An even fewer number of architects, confronting new conditions and feeling a strong sense of personal responsibility, make designs (you would call it ‘drawing freely') that go far beyond the existing rules or even what can be built, in the interest of finding a way forward for architecture in a changing world. They are so few because they do not attract paying clients and must accustom themselves to criticism from all sides, including accusations from colleagues that they lack responsibility, and that what they do is easy—the really hard stuff is following all those codes and ordinances….


In this debate, I am not at all interested in defending my own position or that of a great ‘free drawer' like Peter Cook. My most exploratory work is probably behind me (though I may still offer a few surprises), and people will make of it what they will. 


Rather, I am concerned about young architects with high aspirations and fertile imaginations and a strong sense that the status quo is not good enough. If all they see and hear is they must work within the known boundaries established by laws and standards; that they must be good soldiers and follow the rules in order to be respected; and that making ‘visionary' drawings is really only ‘art' and not architecture, they will either flee the field of architecture, or stifle their best instincts. In either case, the future of architecture becomes increasingly bleak.
33. Pedro Esteban
3.13.09 / 5pm


Mark Primack.  

I'm not agreeing with your comment, you remind me some of my teachers, who don't believe in new ways, and always are crushing students with new ideas. Architecture must change because the world and society are changing. Today the society doesn't need one more pragmatic.
35. Firas
3.14.09 / 8am


Another(and possibly more critical) question evolves from this line of dispute, namely: when does drawing or sculpture cease to be a “representational” tool? It is critical to identify that all tools of the architectural trade are attempting to represent, and it is important to question the validity, or futility, of the representation. A section or a plan is not architecture but the embodiment of something (many lengthy discussions can emerge on the precise nature of the word inserted instead of “something” but this post is precisely not about those discussions.)


What I am trying to elucidate here is that is we consider architecture to encompass even the unbuilt, then we run into the problem that architectural representation ultimately represents the “potentially built”. The built is key in this equation, and perhaps as architectural representation ceases to be representational, architecture can become a spatial practice free of connotations of “built” inherent in the present definition of the word.


Fertile imaginations and a genuine concern for the linguistic syntax that spreads like wild fire in the architectural community, might be the right direction: to critically evaluate the abstract etymological notions of “space”, “movement” and , ultimately, “architecture”, those words that are used immeasurably on a daily basis with no intellectual discourse to support their usage.
37. [Mark Primack](http://www.markprimack.com)
3.14.09 / 8am


I'm obviously not making myself clear enough, and I apologize for that.  

 ‘Free drawers' should need no defense (though I'm grateful that Peter Cook once took it upon himself to defend me). Having lived the life of both the ‘pragmatic' reformer (few) and the visionary (fewer) to which you refer above. I don't imagine one more stalwart, courageous, principled, righteous (or opportunistic, ingratiating, parasitic, self-righteous) than the other. Those characteristics can all be found on either side of the equation ( Raimund Abraham would make a great petri dish for that analysis). Perhaps the real test of the mettle you speak of, Lebbeus, is one's willingness to face obscurity, to continue to work in the absence of recognition and rewards- publications, fat commissions or tenure? Our society brands such an absence of credentials as failure, but it may actually be a gateway.  

 How many students have been encouraged to commit to obscurity for the sake of their art in the spirit of, say, idealistic elementary school teachers? I wasn't looking for obscurity when I left the big city, but it allowed me to live deeply within a particular place and community, and so come to understand the meaning of place and community. I'm grateful for that. Contrary to what feels to my like a very naïve assumption, my ‘pragmatic' architecture doesn't involve obeying rules, but rather challenging and reforming them, or at the very least renegotiating them, endlessly. My buildings don't support elitism and snobbery, they stir some movement toward economic and social justice and engagement. I'm not talking Tatlin here; this is late capitalist America after all, where bold statements are generally aimed at rapid co-optation, commodification and globalization (Harvard, Rizzoli, Hollywood, Dubai, Oh my!). My built work, though hard-fought, won't be coming to a computer screen near you anytime soon. But I've got some good-looking road rash, I've been elected to local office and my grown children miss the house they grew up in.  

 I neither know nor care whether drawn or built architecture is ‘better' or more responsible; that's not my point. I do know in my hands, my head and heart, and definitely in my back, that they are very different, and that drawing, as demanding and challenging and richly inspiring as it can be, is a much safer activity than the built architecture that I and many more than a few others struggle to practice in the world today.
39. David
3.14.09 / 12pm


I would take it the other way.  

“What I'm really interested in is not Architecture. It's buildings.The problem with architecture is that it's allergic to time, because architect keep being asked to create lasting monuments, frozen in time. But buildings have no such presumption. Buildings live in time, the same way we do.In time, we learn. In time buildings learn.”  

-Stewart Brand


What is it about “architects” that you you so badly want to be a part of that club? I mean, who cares if you are called an artist, architect, or pogo stick? Isn't what you produce what really matters?


My take on it is that even architects who build real buildings their entire careers end up having little to no individual impact compared to the people who use the building, or decide to change it later. How much less of an impact do those who never build have?


If you see architecture as a type of drawing, that's fine too! Richard Rorty's classic essay “Philosophy as a kind of writing” tackles the relevant issues much better than I could. Basically, the central fiction is that there are any “problems” that architecture can solve, or that there is even such a thing as architectural “research.”
41. David
3.14.09 / 12pm


PS here is a link to the essay  

<http://www.jstor.org/pss/468309>


If you are on a school campus you should have access to Jstor.
43. [lebbeuswoods](http://www.lebbeuswoods.net)
3.14.09 / 2pm


David: You're right in saying that it's not what you're called that counts, but what you do. However, if you are called a ‘pogo stick' (or an ‘artist') your opportunities to design buildings intended to be built will be very limited. 


I don't think that being considered an architect is about membership in a club. It is a form of political empowerment by a society to take responsibility for the built environment. That's what is at stake here.
45. [lebbeuswoods](http://www.lebbeuswoods.net)
3.14.09 / 3pm


Mark Primack: Deeply felt. Your position is clear.
47. David
3.14.09 / 5pm


LW


But that is exactly the point. The political empowerment that society bequeaths to “architects” goes to those individuals that embody society's *generally accepted* definition of “architect.” Most civilians would say that is “someone who conceptualizes, designs, coordinates, and oversees the construction of built places.” 


“It's in the doing that the idea comes,” said Edmund Bacon. 


Most civilians would call non-building architects “artists.” I believe that explains why there is NOT a great deal of political empowerment placed in their hands. I'm not quite sure what the political empowerment of non-building architects would look like, actually.


I think the standard response is that non-building architects create intellectual capital that the practicing architects draw upon. But anyone who has gone through the building design process knows that ideas are a dime a dozen. It's the so-called pragmatic aspects of design that usually serve to trip up the grand scheme. The process is so practically difficult that most architects give up the idea of articulating a critical position from the very beginning. 


It seems to me that whatever lack of criticality exists in the contemporary architecture world springs not from a lack of thought on the part of architects, but from the vertiginous position of the architect within the chaos of the building process. I fear that the modern academy's focus on “concept” has encouraged generations of architects who don't have many practical skills to offer to the building process. Thus they are unable to articulate their (social or otherwise) critical ideas in built form since they are forever playing catchup with the other parties in the dance.


I think we can agree that the virtuosic practicing architects, conceptually speaking, were ALSO virtuosic practically. I think of Corb, Kahn, Sullivan, Cret. The education that encourages this type of professional flexibility and inventiveness is a technical one. We are born thinking about the people around us and how they act, and how we want them to act. We are not born – and often today not even taught – how to build.


If all the greatest thinking architects devote themselves to NOT building, who will we be left to design buildings necessary for civilization? I maintain that the highest goal in the profession should ALWAYS be to design and carry through to and past construction. Only then will the great ideas conceived in the architect's mind's eye develop with the nourishment of interaction with reality. The tough part is not coming up with a critical idea, but finding a way to adapt it to a specific place and time.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.14.09 / 9pm
	
	
	David: Your emphasis on the technical makes sense only when the technical is put in the service of really good ideas. Contrary to your belief, really good or great ideas are not a dime-a-dozen—they are all-too-rare. 
	
	
	Your general position is an ‘arts and crafts' one, which I allude to in my post Architecture School 202 by way of the Bauhaus and Vkhutemas. It certainly has a lot of virtue in relatively stable societies, but as the failure of those two great attempts to unite the technical, social, political, and the aesthetical proves, in unstable societies such as theirs and the present one, this approach does not work because of society-wide fragmentation and ideological polarization. Nevertheless, I respect the position, and fully understand why one would wish it could apply.
	
	
	As for your question “If all the greatest thinking architects devote themselves to NOT building, who will we be left to design buildings necessary for civilization?” I can only answer that there are rough times ahead. As you know, most buildings being built in the world today are not designed by architects, but by developers, bankers, corporate boards and other money people who use architects and engineers as building technicians, at best, and legal rubber-stamps at worst. The influence of architects is shrinking rapidly and there is nothing in sight to reverse this trend. Nothing, that is, but architects themselves, who in many places are still legally and, shall we say, morally empowered, even if many have abdicated their actual authority over what is built. I ask, what would happen if “the greatest thinking architects” banded together and refused to be the mere handmaidens to capital interests, and publicly declared their resistance and why? What would happen to our civilization then? My expectation is that such a stance, if informed by great ideas, would have an effect, that it would be a strong step in the direction of changing ‘civilization' for the better. It seems to me that this is what our discussion is really about.
49. [shyarc](http://shyarc.blogspot.com)
3.14.09 / 11pm


architect/architecture = knowing the guy next-door
51. [Mark Primack](http://www.markprimack.com)
3.15.09 / 12am


Vkhutemas, the golden age of starving students. How I wish I could have been there. Melnikov, my hero, had the indiscretion to build a pavilion in Paris. He paid dearly for his hubris, didn't he? House arrest, but such a beautiful house! Still, it's a cautionary tale about revolution's rapid plunge into back-biting institutionalism.  

 I can't think of a single great idea an architect ever had. What am I forgetting? I think democracy is a great idea. So is communism (I'm a sucker for ‘from each according to his ability'). Broadacre City is not a great idea, nor is the Plan Voisin. Had either been universally applied, it would have destroyed the world. But those are still two great architects.  

I agree that the current state of ready-to-wear built architecture outside the major fashion houses is more dismal than ever. In the face of developer- or contractor-owned in-house architects, the quaint checks and balances of an AIA contract seem like rosy vestiges of a gold plated era. Now, as Sanders notes (Commodification and Spectacle in Architecture), the best an architect can hope for is to act sensational enough (‘great ideas', a la Diller and Scofidio?) to get branded, and then to sell.


But credit where credit is due. The sincere people you have left behind in Sarajevo, Havana, Berlin, Zagreb and maybe even San Francisco, the ones who didn't manage to parlay their resumes into tenured positions, will enter the workforce either as seat-of-the-pants, self-employed, catch-as-catch-can, learn-as-you-go practitioners like me or as observant yet invisible short-term functionaries in firms or bureaucracies.  

Those obscure young practitioners, stigmatized by you and others of your ilk, will continue to read and question, and throw their thoughts at whatever small commissions or home improvement projects they can get. They will avoid rules and expenses in brilliant black-market ways. That collective consciousness will define the next zeitgeist, the next great ideas.  

My nephew-in-law is an art school welder in the foothills of the Sierra Nevadas. He graduated from a state university. He and his wife built their own house, and have no mortgage. She drew the plans and got the permits. They raise goats and chickens, and he owns your books. Congratulation Lebbeus. You can never know your own strength.
53. [lebbeuswoods](http://www.lebbeuswoods.net)
3.15.09 / 4pm


Mark Primack: If I have appeared to stigmatize what you refer to as obscure young practitioners trying to do good work wherever they are, then I regret it, as that is not how I feel. In a happier world, it would be enough for the health of architecture “to let a thousand flowers bloom.”


I'm not in a position to speak for the rest of my ‘ilk,' whoever they might be. 


I can think of one great idea Frank Lloyd Wright had, and that is the ‘open plan' first seen in his Prairie Houses. It was really his invention and had a great influence on the development of Modernist architecture and thinking. The idea of freer movement in space liberated architecture and those who inhabit it from the tyranny of the box and of history.
55. [Mark Primack](http://www.markprimack.com)
3.15.09 / 6pm


Bravo. Saint Francis is said to have been the first stigmatic.  

This from wikepedia:  

“The blood from the wounds (of a person receiving the stigmata) is said, in some cases, to have a pleasant, perfumed odor, known as the Odour of Sanctity.  

Individuals who have obtained the stigmata are many times described as ecstatics. At the time of receiving the stigmata they are overwhelmed with emotions.”


Not to embarrass you with praise, Lebbeus, but you have voluntarily borne the pain of the world on your shoulders and through your hand, undoubtedly inspiring others to think beyond blind acceptance toward action. There aren't many of your ilk, but I hold you all in great esteem.


Having said that, I doubt that the chief executives of the Larkin Company, a precursor of corporate American consumerism, were ‘liberated..from the tyranny of the box and history' by inhabiting Wright's homes, though I appreciate your point.
57. [lebbeuswoods](http://www.lebbeuswoods.net)
3.16.09 / 12am


Mark Primack: For once, I am (almost) speechless…..
59. Kyle Schroeder
3.16.09 / 9am


In the words of Raimund Abraham, “architecture is an act of desire…an anticipation of materials expressed in a drawing or a text”. I agree with Abraham. Architecture is not necessary. Architecture is excess.
61. [Church on the Berlin Wall | Locus Iste Building Database](http://locusiste.org/buildings/?p=555)
8.20.12 / 2am


[…] IMAGE SOURCE: lebbeus woods blog […]
