
---
title: ZAHA HADID’S DRAWINGS 3
date: 2009-03-30 00:00:00
---

# ZAHA HADID'S DRAWINGS 3



The change in Zaha Hadid's drawings—fragmentation giving way to fluid form—was already evident in her drawings analyzing the farming landscape around the Vitra factory site for the Firestation project. These impart a linear dynamic that comes together in the powerful thrust of the little building. The vision here is no longer about breaking up and scattering. Rather, it is about gathering together and directing. It is also about the making of unified, and unifying, forms. As with other questions about which she has given no explanation or insight, we are left to puzzle out the reasons for this change in her drawings and the designs they describe.


It seems to me that there are two separate sets of possible reasons, not entirely conflicting. The first is that over the ten years between The Peak and the Vitra firestation, Hadid's thinking had evolved in a natural way, following a trajectory from utopian visions of an ideal Hadidian world to Hadidian interpretations of the world as it actually is. The second is her gradual acceptance of the fact that, if she wanted to build, her designs would have to work within property lines and other limitations—material and conceptual—imposed by clients. Her buildings, in other words, would have to become more compact and contained, more unified and more buildable in the conventional sense of the term.


However, there was another factor that, as fate would have it, impacted her development in the 90s, and that was the growing pre-eminence of the computer in architectural design and construction. Not only architects, but clients, engineers, and contractors, expected to see computer drawings that could be readily exchanged between them and directly translated into the computer-aided manufacture of building elements. This development coincided with the dramatic shift in Zaha's designs, and clearly gave it impetus.


From the mid-90s on, she was increasing interested in complexly curvilinear, fluid dynamic forms that can be drawn by conventional hand methods—with French curves and the like—but are much more quickly and accurately drawn using computer programs. She, and her team—now grown from a small cadre to more than a hundred—were still producing hand-paintings for projects such as the Centers for Contemporary Art in Cincinnati and in Rome, but these have the feel of being made for exhibition purposes and lack the pictorial vibrancy and conceptual urgency of earlier, exploratory paintings and drawings. The real action had moved to the computer. In one sense, it liberated Zaha, enabling her to create the unprecedented forms that have, by the present day, become her signature. In another, it brought an end to a certain intimacy and feel of tentative, almost hesitant expectancy, in her drawings and designs, that was part of the intense excitement they generated.


Whether she works on the computer in the direct way she once did with foam brushes I have no idea. Certainly, the new method of drawing has done nothing to diminish the intensity of her architecture, though it has changed it as a visual presence and a philosophical proposition. The forms gather energies around them and retain them. The contained energy contorts simple forms into complex ones. They are tightly wound, or bundled, and seem ready to explode—but do not.


Today, Zaha's global vision includes urban planning projects for Singapore and Bilbao and, no doubt, others to come. The urban landscape, reformed by her architecture was always a basic theme of her work.  In her earlier drawings and speculative urban projects, swarms of tectonic fragments accreted into Hadidian buildings, either among existing buildings—as in *The Peak*'s Hong Kong—or on an abstract field of city blocks or other boundaries, as in *The World*. In the new designs, energy flows congeal into vaster urban sections, no longer mere buildings. This quantitative shift is also qualitative. It is one thing to imagine Zaha buildings as anchors in a broadly diverse landscape, but it is quite another to imagine entire districts that must conform to her designs. Reminiscent of the approach of Baron Hausmann to 19th century Paris, or of Robert Moses to 20th century New York, these are imperial in scale and intent: a unification by big gestures that confirm some form of centralized state, or private, corporate power to make such gestures in the complex fabric of a city. Fragmentation is inherently democratic, regardless of how dominated at any moment by one style or another—that, after all, remains a measure of choice. Big gestures, however elegant or effective they may be, are inherently autocratic. Here we stand at the precipitous divide between art and politics, which is exactly the domain of architecture in any age. It is the edge on which the drawings and projects of Zaha Hadid are, at this moment, delicately poised.


The legacies of Mesopotamia and Suprematism both suggest the long view. Over the ages it is the architecture that matters, that will endure. It is the architecture that vindicates the human condition, not the other way around. The work of a great artist, or architect, always poses the most difficult questions. We look to Zaha's work for her answers, and must wait, and see.


LW


The above text by LW was first published in [Protoarchitecture: Analogue and Digital Hybrids](http://www.amazon.com/Protoarchitecture-Analogue-Digital-Hybrids-Architectural/dp/0470519479), edited by Bob Sheil, Architectural Design, 2008. It is published here under my rights of fair use, for educational purposes only.


Zaha Hadid, Analysis of fields, *Vitra Firestation*, 1990:


![lwblog-zaha3-1](media/lwblog-zaha3-1.jpg)


Zaha Hadid, *Vitra Firestation* design stidy, 1990


![lwblog-zaha2-21](media/lwblog-zaha2-21.jpg)


Zaha Hadid, *National Center of Contemporary Art*, Rome, 1997:


![lwblog-zaha3-41](media/lwblog-zaha3-41.jpg)


Zaha Hadid, *One North Masterplan*, Singapore, 2001:


![lwblog-zaha3-3a](media/lwblog-zaha3-3a.jpg)


Zaha Hadid, *Herault Sports Center*, France, 2002:


![lwblog-zaha3-52](media/lwblog-zaha3-52.jpg)


Zaha Hadid, *Guggenheim Museum project*, Taiwan, 2003:


![lwblog-zaha3-2](media/lwblog-zaha3-2.jpg)







 ## Comments 
1. [Yefim Freidine](http://f-fr.planning.su)
3.31.09 / 4pm


Malevich is weaker than El Lissitzky and more formal. And the architecton's of Malevich were a result of his partnership with El in Vitebsk. Lissitzky made the “Proun” (Pro\_unovis, Unovis – a name of Malevich's and Lissitzky art group in Vitebsk). And I think Zaha Hadid makes the same – “prouns”, which she uses in architecture, urban landscapes, fashion design. As a technology. In formal ways she follows Malevich.





	1. Pedro Esteban
	3.16.11 / 7pm
	
	
	and the point is?
3. mero
5.18.09 / 2pm


you are the best architect i have ever seen zaha
5. [blog.lhli.net](http://blog.lhli.net/2010/06/02/zaha-hadid%e2%80%99s-drawings-3-%c2%ab-lebbeus-woods/)
6.2.10 / 5pm


[…] <https://lebbeuswoods.wordpress.com/2009/03/30/zaha-hadids-drawings-3/> Jun 02, 2010 18:39 av lhli. ffffound Inga […]
7. [Josh](http://www.ymstech.net/)
12.5.10 / 11pm


brilliant! how come i never read about it? [קידום אתרים](http://www.ymstech.net/)
9. Pedro Esteban
3.16.11 / 7pm


I prefer his first part!!!!! By far!!!  

She has the best curves now indeed, but they aren't so good as his straight lines……
