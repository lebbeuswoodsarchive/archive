
---
title: ZAHA HADID’S DRAWINGS 1
date: 2009-03-23 00:00:00
---

# ZAHA HADID'S DRAWINGS 1



When I first visited her small studio in 1984, I saw a watercolor Zaha Hadid was working on taped to a drawing board. It was a delicate and intricate drawing related to her breakthrough project for The Peak. Being one who drew, I asked her what brushes she used. Red sable? Without a comment, she showed me a cheap paint-trim ‘brush' that can be bought at any corner hardware store—a wedge of gray foam on a stick. I still remember my being shocked into silence. Years later, I came to understand her choice of tools as characteristic of her approach to architecture: a wringing of the extraordinary out of the mundane.    


From the beginning of her creative work, Zaha has used drawing, to an unusual degree, as a means of visualizing her architectural ideas. Her way of drawing has changed over the years, as her practice has changed from that of **a** radical visionary—a reference most commonly applied to those who don't build-**—**to an architect designing large-scale projects that are being built in various parts of the world. The evolution of her drawings is my primary interest here, and how they have affected the concepts of her architecture.


Most architects make drawings. Yet, Zaha's drawings of the 1980s are different, and in several ways. Most notably, she had to originate new systems of projection in order to formulate in spatial terms her complex thoughts about architectural forms and the relationships between them. These new projection methods were widely copied in their time, and influenced, I believe, the then-nascent computer modeling culture. More to the point, they enabled her to synthesize entire landscapes within which a project she was designing may have been only a small part. This has been crucial to her thought because she sees architecture as an integral part of the wider world. She was a global architect long before the term acquired its present meaning.


There is another way these drawings are not only unique, but uniquely important to Zaha's idea of architecture: they must carry the entire weight of her intellectual investment. Her written statements about the work are, frankly, blandly descriptive, betraying little of her philosophy, and even less of her aspiration to employ her architecture as a unifying force in the world. Her lectures, while getting a boost from her charisma, are no more revealing. But her drawings speak volumes about her outlook, her intellectual depth, and her ambition to place architecture at the dynamic center of an ever more dynamic world.


Her detractors have often said her drawings are only about style, ignoring their systematic and obsessively analytical construction. On close examination, we find that the drawings reveal complex and subtle rearrangements and reinterpretations of what most of us would call reality, portraying new forms of spatial order governing the relationships between sky and earth, horizon and ground, the artificial and the natural. Her drawing of *The World (89 degrees)* envisions a *tabula rasa* for a brave, new, Hadidian world. The seminal *Peak* drawings fragment both architecture and the mountainside overlooking Hong Kong, allowing them to intermingle in a startling, seemingly natural, synthesis. Who else had dared such all-embracing visions? Bruno Taut, in his Alpine Architecture? Le Corbusier, in his Radiant City? Perhaps, but never in the context of projects so intended to be realized. This work was serious theory in visual form, and more. The drawings were manifestos of a new architecture that Zaha was clearly determined to realize in building, and against any odds.


Hadid's work of the 80s was paradoxical. From one perspective, it seemed to be a post-modern effort to strike out in a new direction by appropriating the tectonic languages of an earlier epoch—notably Russian avant-garde at the time of the Revolution—but in a purely visual, imagistic way: the political and social baggage had been discarded. This gave her work an uncanny effect. The drawings and architecture they depicted were powerfully asserting something, but just what the something was, in traditional terms, was unclear. However, from another perspective this work seemed strongly rooted in modernist ideals: its obvious mission was to reform the world through architecture. Such an all-encompassing vision had not been seen since the 1920s. Zaha alluded to this when she spoke about ‘the unfinished project' of modernism that she clearly saw her work carrying forward. With this attitude she fell into the anti-post-modern (hardly popular) camp championed by Jurgen Habermas. Understandably, people were confused about what to think, but one thing was certain: what they saw looked amazing, fresh and original, and was an instant sensation.


Studying the drawings from this period, we find that fragmentation is the key. Animated bits and pieces of buildings and landscapes fly through the air. The world is changing. It breaks up, scatters, and reassembles in unexpectedly new, yet uncannily familiar forms. These are the forms of buildings, of cities, places we are meant to inhabit, clearly in some new ways, though we are never told how. We must be clever enough, or inventive enough, to figure it out for ourselves–the architect gives no explicit instructions, except in the drawings. Maybe we, too, must psychically fragment, scatter, and reassemble in unexpected new configurations of thinking and living. Or, maybe the world, in its turbulence and unpredictability, has already pushed us in this direction.


 *(To be continued)*


 LW


The above text by LW was first published in *[Protoarchitecture: Analogue and Digital Hybrids](http://www.amazon.com/Protoarchitecture-Analogue-Digital-Hybrids-Architectural/dp/0470519479)*, edited by Bob Sheil, Architectural Design, 2008. It is published here under my rights of fair use, for educational purposes only.


*The World (89 degrees)*, 1983


![lwblog-zaha1-4](media/lwblog-zaha1-4.jpg)


*Suprematist Snowstorm*, 1983


![lwblog-zaha1-21](media/lwblog-zaha1-21.jpg)


*The Peak* project, 1983


![lwblog-zaha1-5](media/lwblog-zaha1-5.jpg)


![lwblog-zaha1-3](media/lwblog-zaha1-3.jpg)



 ## Comments 
1. Pedro Esteban
3.24.09 / 6pm


Is interesting the evoution of contructivism in Zaha's works.  

The way that she fragments all the russian function, but it looks like constructivism in form anyway.


Lebbeus Woods.  

And when you will translate your drawings into a large scale projects? Believe me if I was an investor I would carry out your projects.  

In this case, the architecture has one of is major problems—the investor—the new architecture must be separate of this.
3. river
3.25.09 / 7am


I had a feeling, based on watching the final episodes of ‘Battlestar Galactica' at long last from my DVR, and a subsequent dream, that I might need to rekindle and share my interest in theoretical physics to help my students of 3d understand perspective better.


As I explored the new changes in a knowledge terrain I was barely more familiar with ten years ago, I found this accessible [article](http://mkaku.org/home/?page_id=416). It led me to think of a question which kinda goes: “Is there a body of literature (or preferably a single book) which attempts to describe and contextualize the evolution of architectural theory in a manner similar to this article's author?”


With that question on my tongue, I simply “checked in” on your blog, to see what was new. Darned if you hadn't just published a piece of the frakkin' answer!
5. [neon](http://...)
3.30.09 / 5am


Zaha Hadid could work like a graphic designer perfectly. I guess that she has a really pleasure when she does those drawings.


Visit my blog.  

<http://www.editorialpencil.es/wordpress/>
7. river
3.30.09 / 7am


Games are the new art genre.


Games add the layer of real-time user interaction to the classical strata of story, art, music, and architecture. Games are one step closer to synesthesia.


They are like the movies of the 18'80's-19'90's with exciting new emergent properties and complexities.


The most important aspect of games, imho, is that they are the defacto new context for many children's brain associative self-programming.


That last bit is a mouthful of words, I know. But let me break it down. The more we learn about brain development (with the same real-time computational and visualization tools that have brought us to a gaming culture, btw), the more we have to acknowledge the power of imprinting at vulnerable key points in one's cognitive development.


The maternal imprint at birth, the father-family-boundary imprint as a toddler, the sexual imprint at adolescence. 


Actually, cough-blush, pause. I'm going to leave my train of thought here. For I am suddenly reminded of a thesis that was popular in my private neurons during the y2k transition. Namely, that with design tools and technologies emerging that allow us almost infinite control over the details of our products, especially those products that are designed for future designer's learning, what ethics will guide us?


Microsoft and Apple have both agreed implicitly to deliver the post-1984 world to us in rectangular frames. We take this fact of engineering for granted now. Yet it was not the only possibility at the moment it became imprinted upon our new global culture. The old, analog oscilloscope was round. The first video games were actually developed on a round screen. The disciplines of photography and publishing in WYSIWIG have held to the rectangular by sheer force and mental habit.
9. [vicki](http://victoriatao.com)
3.30.09 / 8pm


Interesting, her style looks a lot like some of the futurist paintings I've seen… I remember seeing pictures of this lady's moving spacecraft thing/fashion advertisement in new york and thinking how extravagant it was. Design and ergonomic-looking forms are what's in vogue in the art world much to my chagrin.
