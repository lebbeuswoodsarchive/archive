
---
title: ZAHA HADID’S DRAWINGS 2
date: 2009-03-27 00:00:00
---

# ZAHA HADID'S DRAWINGS 2



In considering Zaha Hadid's early drawings, it is useful to recall that Modernism in architecture had two competing directions. One—prominently represented by the Bauhaus—aimed to redesign the world in conformance with the demands of industrialization, including its social dimensions, such as workers' housing. The other—represented by De Stijl and the Russian avant-garde—aimed at a transformation of spirit and the creation a new society taking radical forms of every kind. Of the latter, we can think of Tatlin, Leonidov, and Malevich. Malevich was primarily a painter and the creator of the Suprematist movement, which emphasized abstraction as an almost religious mode of spiritual change. Of the two Modernist directions, Zaha was strongly attracted to Suprematism. Her thesis project at the Architectural Association in the 70s, under the tutelage of Rem Koolhaas, took one of Malevich's seminal Arkitektons as a beginning point. Placing it along and across the Thames in central London, she left no doubt—to the cognoscenti, at least—as to her ideological position: she was reviving a neglected, almost stillborn modernist ideal and inserting it into the contemporary world.


But why?


It is, of course, interesting to speculate on the reason Zaha took Malevich as her starting point. She has never written or publicly spoken about it. In private, she is evasive about the subject. Koolhaas' own preference was for the Constructivists, with their technological symbolism and collectivist social programs, more attuned to the culture of his native Holland. I believe that Zaha's reasons were personal, emerging from her life journey from Baghdad, through Beirut, where she majored in mathematics, and Switzerland, to London. They are also cultural. Iraq is, after all, a descendant of the most ancient of civilizations, and its spiritual values predate religions such as Christianity and Islam, and modern ideas of nations and states. The transience of life, with its technological and social upheavals, pales before enduring philosophies. The abstractions and metaphysics of Suprematism have this character.  


 Fragmentation can be philosophical, too. It can be systematic and not merely chaotic or accidental. This can be seen in some of Malevich's earlier paintings. Or, even if it is chaotic, it can reflect an existentialist edge, a risky form of play with disintegration as a prelude or even an impetus to a higher re-formation. As long as forms remain whole, unified, coherent, they cannot be transformed. Only when established forms are broken up are they susceptible to change. This formal verity is a virtual metaphor for modern society: the break up caused by political revolutions and new technological capabilities has created a human world not only susceptible to new forms, but demanding of them. Hadid's work, as manifest in her drawings, emerged at a time when there was much soul-searching among serious architects and theorists about the why and how of reformulating architecture in response to the changes overtaking society. Her answer was like the blow of Alexander to the Gordian Knot: decisive, if nothing else. The battle was engaged.


 It is well known that Zaha has had an unusually difficult time getting her projects built. The radical forms of The Peak, the Hamburg Hafenstrasse project, the Berlin ‘sliver' building, and the Dusseldorf Media Park, among others proposed during the 80s, attracted developers who would have loved to capitalize on them, but in the end didn't have the courage. It took a patron, Rolf Fehlbaum of Vitra, to actually build her first fully realized project, a firestation for his furniture factory in Weil-am-Rhein in 1994. But, by then, her drawings were changing. To put it simply, fragmentation began to give way to fluid form.


 *(To be continued)*


 LW


 The above text by LW was first published in [Protoarchitecture: Analogue and Digital Hybrids](http://www.amazon.com/Protoarchitecture-Analogue-Digital-Hybrids-Architectural/dp/0470519479), edited by Bob Sheil, Architectural Design, 2008. It is published here under my rights of fair use, for educational purposes only.


 


Kazimir Malevich, *Arkit**ekton*, 1923:


![lwblog-zaha2-5](media/lwblog-zaha2-5.jpg)  




Zaha Hadid, *Malevich's Tektonik London**project*, 1977:


![lwblog-zaha2-1](media/lwblog-zaha2-1.jpg)


Zaha Hadid, *Office Building, Berlin,* 1986


![lwblog-zaha2-6](media/lwblog-zaha2-6.jpg)


Zaha Hadid, *Hafenstrasse Office and Residential Development,* Hamburg, 1989


![lwblog-zaha2-82](media/lwblog-zaha2-82.jpg)


Zaha Hadid, *Vitra Firestation* design stidy, 1990:


![lwblog-zaha2-2](media/lwblog-zaha2-2.jpg)




 ## Comments 
1. Pedro Esteban
3.28.09 / 6pm


I don't understand why some critics define Zaha as a non complete architect. Hers works have a strong identity, well she has a pritzker.
3. luke
3.30.09 / 4pm


Despite the overwhelming vigor of her drawings, I have always been skeptical of Zaha for what is really a technical issue. She consistently maintains a distanced aerial view in her (popular) work that creates these omniscient perspectival moments. I am seduced by this view of course because it implies power and transcendence. Lebbeus, I know you write about the desire to transcend…especially to leave behind the constraints of gravity, social convention, the trivial forms of capitalism. Nevertheless, you almost always provide numerous human point of view moments in your work. You imagine the human condition in a visceral, in situ manner that makes your work relevant as architecture (that is to say form that moves beyond the merely conceptual). You don't rely on p.o.v. to do the work for you. The architecture represented transcends the context rather than (in the case of zaha) the representation itself transcending the architecture. I can't help but think of De Certeau's view from above / view from the street dichotomy and the political implications associated.


Ultimately, all I am saying is that Zaha's fragmentation, due to her “distanciated” (to use a word from zizek) p.o.v. makes her work precious, otherworldly and disengaged in a way that feels uncomfortably bourgeois. Most of your work on the other hand, feels committed to the material world by confronting an urban context and engaging a human existential predicament at eye-level.
5. [Today's archidose #415 | Building Station](http://e.holmesian.org/2010/04/todays-archidose-415/)
4.30.10 / 8pm


[…] [original image source] […]
7. [Today's archidose #415 « Architects Directory](http://www.architects-directory.info/?p=79)
5.1.10 / 5am


[…] [original image source] […]
9. [Design Culture » Today's archidose #415](http://www.haynesarch.com/blog/2010/05/03/todays-archidose-415/)
5.3.10 / 4pm


[…] [original image source] […]
11. [Today's archidose #415](http://www.arch-times.com/2010/04/todays-archidose-415/)
5.30.10 / 2pm


[…] [original image source] […]
