
---
title: ARCHITECTURE SCHOOL 302
date: 2009-03-26 00:00:00
---

# ARCHITECTURE SCHOOL 302



Students working on *The Sphere* collaborative design project, The Cooper Union, New York City:


![lwblog-archschl302-11](media/lwblog-archschl302-11.jpg)


The premise of a school of architecture based on collaboration is that individuality in this field is best developed if students work on projects together, rather than separately and competitively. Individual talents, knowledge, and skills vary, but each can nevertheless contribute something to an end result that all can be proud of. In the process of working together, individual students must be able to freely express their own ideas and exercise their abilities without having to sacrifice  them to ‘norms,' and certainly not to a pre-conceived end product. Thus, the collaborative project has to be open-ended, the final form of which cannot be known at the outset, but will emerge through the collaborative effort.


 Professional practice does not, at the present time, work this way. The desires of real-estate developers and other clients, who must work with various financial, zoning and codes parameters, in most cases all but determine in advance of design the basic envelope of a building. What is left for the architect to design is but a refinement of this given form in terms of materials and ‘style.' However, if a school is to educate architects and not merely train them in a vocational way to fill existing job slots, then it must look beyond what is currently practiced to ways that can inform and, if necessary, transform the future of practice for the better.


 In the collaborative school, the design studio will remain the central activity, the place of and occasion for the weaving together of many strands of thought, work and knowledge. There, students and teachers will work closely together. It will be a studio, however, that works on a different basis than present ones, in that it will have a new structure.


 The most prominent feature of this new structure will be the nature of the question (the design ‘problem,' in present terms) put before the students as the focus of their work.


 The single most important task of the studio teacher is to ask the ‘right' question of the students. This is a question that will be the most fruitful for them to work on, individually and as a group. It is also a question that can be fully addressed within the timeframe of the studio—it is crucial that the work of the studio be carried to a reasonable conclusion that is satisfying for all. Work that remains unfinished is extremely discouraging, as is work that finishes too quickly. It takes skill and experience for a teacher to know the right question, then to frame it in terms of a realistic schedule of work. A footnote here is that a teacher should not underestimate what students can do and want to accomplish, if they are inspired by the question and its challenges to their imaginations and abilities.


 In a collaborative studio, the best questions are those that address urban, or ‘proto-urban' conditions.


 The students of a design studio constitute a community, a group of individuals united by a common set of interests and a common purpose. Indeed, their community is occupied with the same challenges that face any community—a neighborhood, a town, or a city. How do we work together to make an environment that is both just and productive? How do individual desires and needs fit into the need of the community for overall coherence and order? How can an individual flourish within a group? These are questions central to the human condition, and thus to architecture.


 The questions put before the students in a design studio need specificity—there must be clear parameters and boundaries. The older way of assigning a site and a ‘program' of functions for the students to work with is a rehearsal for practice as we now know it—everyone competing for the best, or most appealing, solution. In a sense, this older/existing type of design studio is a rehearsal of the competitions practicing architects engage in to get private or public commissions. The overall aim of the collaborative design studio, however, is to lay the ground for a new type of practice.


 There are two parallel or intertwining aspects of the questions that become the focus of a collaborative design studio. First is what we can call a framework or an ‘armature' onto which individual ideas can be imposed. If we think of a city as a model form of landscape to address, we recognize that every city has a pattern of public spaces—streets and plazas and parks—that are the spatial armature that accepts a great variety of separate, individual projects and yet remains an indefinable, even unique, whole.


 However—and here we come to the second aspect—it is superficial to simply take a city's street pattern as a studio project site, while ignoring the particulars of the lives of people who inhabit those streets and the buildings lining them. At the same time, it is not reasonable to expect students to understand most existing urban sites, in all their history and complexity of human exchange, in the timeframe of a design studio. Even though this is done, it usually results in student projects that relate to the most obvious issues and conditions, while ignoring others that may be equally important, but are revealed only through deeper experience. This is similar to today's international architects who fly into cities they do not know, make snapshots of a given site and the ‘local color', attempt some analysis from available information, then offer design proposals for buildings. The results can be seen in the look-alike skylines of many cities today, equating globalization with conformity and homogenization.  Schools of architecture should not be encouraging this way of working nor preparing young architects for this kind of professional practice. 


 The collaborative studio works, rather, in an ‘analogous' way. It takes the studio community, with its differences and commonalities, as analogous to communities outside schools. In other words, its treats the design studio as a ‘real' social situation in itself, a laboratory for living. But, exactly how might this work? And what types of ‘armatures' will best serve an ‘analogical' urban design studio in a collaborative school?


 *To be continued.*


*LW*


*![lwblog-archschl302-22](media/lwblog-archschl302-22.jpg)*


 




 ## Comments 
1. [r.c. reid](http://kensingtongalleries.com)
3.27.09 / 8am


a thought arises that tribal cultures are usually very collaborative, and also (especially the vanishing nomadic variety) fairly adept at a kind of spontaneous generation of evolving/evolvable variiations of the “armatures” you seem to be speaking of. its also interesting that wherever they once really existed they were conscientiously eradicated by modern governments w/their overriding concerns w/profitability and the domination of an enforced homogenaeity. oddly enough, the prevalence of war zones, refugee “camps”, disasters, sprawls, flavelas, etc. is resurrecting this ‘tribal'energy. e.g. the “bridge city”(W.Gibson).  

 in 1981 at a future systems symposium in anaheim, ca., eric drexler (“engines of creation”) was speaking on the subject of the prospect of nanotech. he remarked that soon we would be growing super strong lightweight steel to specs, in addition of course to being able to develop chimerical bio******* that would be capable of being programmed to be active only w/in specific coordinates. he was directing research at stanford u back then as well as working for the dod. he figured it would be 20-30 years before this would be a “reality”. don't the “design” opportunities boggle the mind? ( interesting take on this in “idoru” also w. gibson).so… the nature of “the real social situation” begins to be a very fluid thing. i find that not many architects daydream or rhapsodize a lot along these lines. though you apparently don't find it too difficult. good on you.
3. Nikola Gradinski
3.27.09 / 4pm


Collaboration and individuality can create some interesting results, especially if put together in a high pressure situation. The goal of reaching an anti-competitive collaborative architecture studio, and it follows – as an eventual model for general human interaction, is an admirable one, certainly one which any thinking creative person is surely trying to achieve. There are at present many ideas out there that deal with notions of social design in order to try to come up with a viable alternative to war, poverty, disease, social stratification etc. based on similar collaborative models. Some of these ideas present interesting starting points but sometimes walk the knife edge between collectivization and neo-socialism a little too closely. Although in all collaborative endeavors there will inevitably be a differential in ability and conflict may result as not all ideas can be put into play simultaneously and still achieve the ‘best' result (especially since there is a trend towards a subjective relativist approach to the ‘value' of individual ideas). Indeed many bad results can come about by excessive accommodation to everybody's input. In an architectural studio, there is still a teacher, the voice of guidance and reason (hopefully), who edits and moderates the process. This implies a hierarchy, so once again we seem to be back to square one. In open systems, outside the boundaries of the architecture studio, individuals who come into conflict with the group may become ostracized and leave, causing fragmentation of the group and possibly a loss of crucial input, or not… This may lead to the formation of other new groups in time, or the augmentation of other existing groups, in a large fluid movement of people, although this is what we have already to a large extent. Certainly the model of the RIEA workshops has been a resounding success, as the ideas stated in the post put into practice, and in other design studios based on a similar model. Perhaps these concepts can eventually be successfully integrated into the fabric of daily activity, although it may well require the elimination of an ego and profit driven competitive system, but which one comes first?
5. slothglut
3.30.09 / 4am


r.c. reid + Nikola Gradinski,


to answer your imposed questions, wouldn't you think that it would be very interesting if someone take this collective studio system into real situation, as opposed to a prototype model for a mere educational purpose only. let's test the water.  

i know that most probably it is happening as i write, but at least for me, the documentation of this effort has not been popularly known. i would be very thankful if anyone could point the direction i should take to pursue this very interesting model of practice.
7. [ARCHITECTURE: the solid state of thought [1] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/18/architecture-the-solid-state-of-thought-1/)
11.19.10 / 10pm


[…] lebbeuswoods.wordpress.com/2009/03/18/architecture-school-301 lebbeuswoods.wordpress.com/2009/03/26/architecture-school-302 […]
9. [ARCHITECTURE: the solid state of thought [complete] « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/10/architecture-the-solid-state-of-thought-complete/)
12.10.10 / 2am


[…] Category “LW” search for the ARCHITECTURE SCHOOL posts, 101, 102, 201, 202, 301, 302, and 401; also the AS401 […]
