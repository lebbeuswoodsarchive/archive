
---
title: NOTEBOOK 98-3
date: 2009-03-03 00:00:00
---

# NOTEBOOK 98-3



For a period of ten years, a part of my practice was carried on in a space of ten by fifteen by two centimeters—one might call it an extreme space. Beginning in 1991, I was traveling very much—lecturing, teaching, and working on the occasional project—and not often near my drawing board. As a way of coping with being on airplanes, trains, living in hotel rooms, cafes, and bars, I began to keep notebooks in which I could draw and write while on the move. This was very important to me, as my thoughts were alive with new ideas that could not be put on hold. So the pages of the notebooks became a studio I could keep in my pocket, unfolding its nearly limitless space whenever I needed.


 By the beginning of the new millennium, I was winding down my travels, though hardly the flow of new ideas. Still, I became a bit more settled and could work in a more settled spot. Making the notebooks had by that time become a habit. For a while, I continued to work in them until I realized, reluctantly, that they were really finished, and I moved on to other ways of working. In all, I made some thirty notebooks of the small size, and a dozen of various larger sizes. 


 The notebooks turned out to be a wise decision, as many ideas that became projects for Sarajevo, Havana, Eindhoven and elsewhere saw first light in transit. Not coincidentally, many of the ideas had to do with the transitory nature of living in social or political or natural conditions undergoing rapid, even extreme, forms of change. Shifting angles of view, abrupt arrivals and departures, confrontations with the unexpected and the strange become more than metaphors, but rather a way of living for many today.


 Notebooks are portable. They can be kept secret, or published. Technically, they are simple to make. Pen and paper. The hand, eye, and thought. Freed from any sort of burdensome apparatus, thought becomes more agile in confronting itself.


*This is the first in a series of glances into the notebooks.*


 LW   



![lwblog-nbcover-1a](media/lwblog-nbcover-1a.jpg)


Tectonic study:


![lwblog-nb1-3](media/lwblog-nb1-3.jpg)


![lwblog-nb1-3a-2](media/lwblog-nb1-3a-2.jpg)


Travel notes:


![lwblog-nb1-1a](media/lwblog-nb1-1a.jpg)


Tectonic study:


![lwblog-nb1-4a](media/lwblog-nb1-4a.jpg)


![lwblog-nb1-4a-11](media/lwblog-nb1-4a-11.jpg)


Initial drawings for The Hermitage, Eindhoven, The Netherlands:


![lwblog-nb1-2](media/lwblog-nb1-2.jpg)


![lwblog-nb1-51](media/lwblog-nb1-51.jpg)


The Hermitage, constructed in the Witte Dame (White Lady), Eindhoven:


![lwblog-nb98-3-herm22](media/lwblog-nb98-3-herm22.jpg)


![lwblog-nb98-3-herm11](media/lwblog-nb98-3-herm11.jpg)



 ## Comments 
1. [Mike Clemens](http://clickthing.blogspot.com)
3.3.09 / 5pm


Thank you for sharing these. I've been developing a greater appreciation for the simple utility of paper, perhaps as a contrarian response to the cloud of technology that envelops us. Paper and pencil make no demands on our attention, they offer no utility or features beyond those that we provide, they work at a human pace, and don't demand that we try to think at their tempo.
3. [workspace » Blog Archive » Notebooks: Lebbeus Woods](http://www.johndan.com/workspace/?p=872)
3.3.09 / 11pm


[…] first in what Lebbeus Woods promises will be a series on his notebooks. He started working with extensively with them in the 1990s as a way to work while traveling. The […]
5. ele
3.4.09 / 10pm


I was trying to read who you said was in that small but growing group re framing perception … 


Could you please pass on those names or projects?
7. [lebbeuswoods](http://www.lebbeuswoods.net)
3.5.09 / 12am


ele: What the scrawl says is: “a small but good group: Dan, Anne, Linda (Aarhus), Aviva (israel), Jonathan (USA), Chuck (USA), Sergio (Mexico), Ivan Furlan (Switzerland)….” I'd have to dig into dusty records to find all their last names. They were students in the RIEAvico school of architecture, in Vico Morcote, CH, in the autumn of 1998. “Framing Perception” was the title of a theory seminar I was teaching.
9. [rb.log» Blog Archive » NOTEBOOK 98-3 « LEBBEUS WOODS](http://www.richardbanks.com/?p=1953)
3.5.09 / 10am


[…] Woods, the architect, has started a series of posts on his notebooks. It sounds like this was a practice he went through and “finished” at some […]
11. w
3.6.09 / 5am


thank you for sharing these; their density is inspiring
13. [Lebbeus Woods' Notebook | Notebook Stories](http://www.notebookstories.com/2009/03/09/lebbeus-woods-notebook/)
3.9.09 / 9pm


[…] I think architects have the best notebooks. This one, from architect Lebbeus Woods, is certainly a great […]
15. [Dan Lenander](http://www.danlenander.com)
3.24.09 / 11pm


Framing perception was a amazing seminar. Ill never forget the 27 hours on the train to and from Vienna. The instrument i created was a higlight for me , since then I have reverted back to buildings.since I have so much time now on my hands I have started to revive drawings and paitings that go beyond my built imagination. My time at Vico was the foundation for all my thoughts and designs.  

best regards Dan
17. [Five Iconic Notebooks in Film | Forest Friend](http://www.forestfriend.ca/2012/02/22/five-iconic-notebooks-in-film/)
2.22.12 / 12am


[…] enough to inflict on the public. But this is not the case with American conceptual architect Lebbeus Woods, whose notebooks are veritable works of art and have been displayed in […]
19. [Darkness or Light « Phos + Skia](http://skiaphos.wordpress.com/2012/05/22/black-on-white/)
6.13.12 / 6pm


[…] WOODS     Notebook    97-3   98-3   01-2   […]
