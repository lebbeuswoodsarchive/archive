
---
title: NOTEBOOK 97-3
date: 2009-03-15 00:00:00
---

# NOTEBOOK 97-3



What makes home ‘home' are the constants. When we go out into the world, especially when traveling, we want a measure of discovery, adventure, the unpredictable, the inconstant. But when we come home we want to find the familiar and the predictable, perhaps only to provide a frame of reference for considering all we did not know or understand from our experiences elsewhere. Home is a sanctuary, where we feel safe. We can express how we feel, confide our fears and dreams. In these same ways, a notebook is something like a home, and it can be taken anywhere. In other words, we can have it both ways: to stay comfortably at home and to adventurously travel out and into the world.


The most important constant is the size of the notebook. It must always be the same. Also the paper, and the pen used to draw and write. It must be a pen and not a pencil, because a pencil smudges with the many openings and closings of the notebook, and it would soon become a mass of blurs. Perhaps that is the nature of memories, but the notebook is not about memories. It is work, sometimes joyful, often difficult, and we want it to last as long as it can. Also, the pen—a cheap but reliable instrument that can be bought anywhere, but has real ink, not dyes that fade—makes precise lines that cannot be altered once drawn. The images and words placed on the pages are not tentative, but definite. They are built up a line at a time, without preliminaries, and thus they are an accurate record of ideas and the process of thought that brought them into being. Each line, each word, brings an idea closer to realization, to completion. In this sense, the contents of the notebook are not ‘sketches,' preliminary attempts that will be finished later, but finished works in themselves.


The writer D. H. Lawrence once wrote, in his *Apocalypse,* about the difference between the idea of completeness in the ancient world and today. Comparing today's ‘rational' process of thought to the dragging of an endless, linear ‘logical chain,' he said that ancient thinkers would take a more circular or spiral approach. They would concentrate on a thought, following it deeper and deeper, until it reached a point of ‘fullness.' Both modes of thought have their virtues and the possibility of fusing them in some way informs the notebooks shown here.


LW



![nbk97-3-1](media/nbk97-3-1.jpg)


![nbk97-3-7](media/nbk97-3-7.jpg)


![nbk97-3-6](media/nbk97-3-6.jpg)


![nbk97-3-6a1](media/nbk97-3-6a1.jpg)  

![nbk97-3-5](media/nbk97-3-5.jpg)


Model of the *Balcony for the Frankopan Castle,* Kraljevica, Croatia, by Dwayne Oyler:


![lwblog-kral1](media/lwblog-kral1.jpg)


![nbk97-3-21](media/nbk97-3-21.jpg)


![nbk97-3-8](media/nbk97-3-8.jpg)


![nbk97-3-8b](media/nbk97-3-8b.jpg)


![nbk97-3-91](media/nbk97-3-91.jpg)


![nbk97-3-9a](media/nbk97-3-9a.jpg)


![nbk97-3-3](media/nbk97-3-3.jpg)


Model of the *Siteline Vienna project* by Dwayne Oyler:


![lwblog-slv1](media/lwblog-slv1.jpg)


 



 ## Comments 
1. Pedro Esteban
3.15.09 / 6pm


This maybe sounds like a kind of adulation, but I say anything I want.  

I learn a lot with you, bus this is beautiful, just that, your prose is maybe poetic, or sentimental, is your thought your prose. It is rare to bring those feelings to life, thanks for sharing them.
3. [workspace » Lebbeus Woods](http://www.johndan.com/workspace/?p=947)
3.16.09 / 2am


[…] Lebbeus Woods posts an entry on the idea of “home” from his 1997 notebooks: The most important constant is the size of the notebook. It must always be […]
5. wmwm
3.16.09 / 6am


Hi LW,


I wonder, how do you develop such discipline in your sketching and composition?
7. Kyle Schroeder
3.16.09 / 9am


The actual process of remembering is an act of destruction. When your brain follows a specific path to reach a memory its particulars change. The memory becomes more faint, and so it is the memories which occur the least which are the strongest. The pencil provides the worker with the ability to create layers, each consecutive mark born from the one before. The finished pencil sketch reveals the process of thinking. To me, sketches are finished ideas, small ideas like simple machines which work together to communicate their collective meaning. Carlo Scarpa and Giacometti come to mind when I think about people who capture time in their drawings. Given this, it surprises me to learn that you sketch with only ink. It seems to go against the nature of time which is to my understanding what your architecture is foremost concerned with; the fallen and the renewed. Could you not reach an equilibrium between the “endless logical chain” and the “spiral approach” by combining both ink and pencil? Furthermore, since pen is permanent, how do you refine your ideas if you cannot eliminate?
9. [lebbeuswoods](http://www.lebbeuswoods.net)
3.16.09 / 11pm


Kyle Schroeder: I refine by making more drawings, always working in series.
11. [lebbeuswoods](http://www.lebbeuswoods.net)
3.16.09 / 11pm


wmvm: I cannot claim an extraordinary degree of discipline. Rather, I would attribute the qualities you perceive to obsession, focus, and intensity.
13. [lebbeuswoods](http://www.lebbeuswoods.net)
3.17.09 / 12am


Pedro Esteban: This may sound like false humility, but I also say anything I want: its is a privilege to have intelligent and appreciative readers.
15. [Mark Primack](http://www.markprimack.com)
3.17.09 / 6am


I'm drawn to pencil out of weakness, but I respect tremendously those who coax depth and tone from ink.  

And though I am generally too lazy to decipher script, I took the time this time to study the words in your notebook and witnessed, to borrow from David's Richard Rorty quote, your writing as a kind of drawing:


 “….the only reason for this is that, without crisis, there is no NEED for innovation. Without crisis, innovation becomes gratuitous, altogether too easy, too sensational, too auto-erotic. It's not that stable, non-crisis cultures don't need change, but rather that the change they need is not innovative, not really new, but a transfiguration of the old. To introduce innovation in such conditions OVERSTIMULATES them, and, to the extent that it does so, is a dangerous waste…..a toxic waste…. a USELESS DANGER…


 You wrote that three years before Enron sponsored Gehry's retrospective at the Guggenheim, and twelve years before our ‘non-crisis culture' imploded. Nice call.  

 One could wonder at the serendipitous nature of timing. Should the visionary wish for crisis, to be in the right place at the wrong time? Or can we admit that it is the prolonged denial of crisis that leads us to act out so dangerously?


 And as for:


“the ideal would be to sponsor some construction.”


 That could re-open the Open Questions entry…
17. [The Paper Architects « dpr-barcelona](http://dprbcn.wordpress.com/2009/09/02/the-paper-architects/)
9.4.09 / 12pm


[…] Some other wonderful images at but does it float and Lebbeus Woods notebooks. […]
19. [Darkness or Light « Phos + Skia](http://skiaphos.wordpress.com/2012/05/22/black-on-white/)
6.13.12 / 6pm


[…] WOODS     Notebook    97-3   98-3   01-2   […]
