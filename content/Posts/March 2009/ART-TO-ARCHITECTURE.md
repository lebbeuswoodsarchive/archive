
---
title: ART TO ARCHITECTURE
date: 2009-03-05 00:00:00 
tags: 
    - architectural_design
    - art
---

# ART TO ARCHITECTURE




It was once thought that architecture, sculpture, and painting belonged together. Indeed, they were admirably intertwined at various points in history—in the ancient cultures of East and West, and in the European Gothic, Renaissance, and Baroque periods. With the rise of the merchant class to power in the 19th century, the plastic arts began to lose their interdependency and to separate. Part of this was due to the general fragmentation of society into competing social units. Part was the fragmentation of knowledge into various specialized fields, of which architecture, sculpture, and painting were, in the arts, prime examples. And no small part was economic. Buildings became real estate that often trades hands for money, as are paintings and sculptures. Today, it is rare to find these three arts united in any but the most tentative way.


Ironically, perhaps, their separation enabled them to influence each other in ways they never did before. It was not until the beginning of the 20th century that we find examples of paintings and sculptures having a direct influence on architectural design. It is as though their independence from each other gave them an intellectual and artistic parity, and allowed architects and painters (later, photographers, filmmakers, and video artists) to learn from each other's works. Before this separation, it is impossible to think of an architect who adopted in his (they were all male, back then) work, methods or forms from a painter. After the separation of the arts, this influence began to appear, and is sure to increase in the future.


The examples offered here are only the most obvious. In this sense, they are an invitation for others to contribute examples from their own points of view and knowledge. No doubt this topic will produce fascinating coffee-table books in times to come, as well as many PhD. dissertations. More importantly, the exchange between the visual arts will continue to transform each of them, and not least the art of architecture.


LW  



**Painting by Theo van Doesburg (1918)** and plan for the **Barcelona Pavilion, by Mies van der Rohe (1927):**



![paint-vandmies](media/paint-vandmies.jpg)


 


**Painting by Kasimir Malevich (1915):**


![paint-malevich1](media/paint-malevich1.jpg)


**Designs by Zaha Hadid for the Media Park, Dusseldorf (c.1990):**


![paint-zaha3](media/paint-zaha3.jpg)


 


**Collage by Richard Hamilton (1956)** [inset: collage by Marcel Duchamp (1919)


![paint-duchhamilton](media/paint-duchhamilton.jpg)


**Design by Peter Cook for Instant City (1968):**


![paint-cook1](media/paint-cook1.jpg)


 


**Painting by a Paramount studio artist for War of the Worlds (2005)** [inset: drawing by Lebbeus Woods for the High Houses, Sarajevo (1995):


![warworldslw](media/warworldslw.jpg)


**“Spider” Constructions by Hernan Diaz Alonzo (2007):**


![lwblog-paint-hernan1](media/lwblog-paint-hernan1.jpg)


#architectural_design #art
 ## Comments 
1. AJ
3.5.09 / 2am


Four more examples immediately come to mind:  

The sculptures of Louise Nevelson have a likeness to the “mat-buildings” of Candilis-Josic-Woods among others. Peter Eisenman's early house designs drew from the sculptures of Sol LeWiit. Many consider there to be a relationship between Richard Serra's torqued ellipses and the post-LA vernacular work Frank Gehry. The artist Richard Galpin has sited influences such as Kurt Schwitter and Lebbeus Woods.
3. Pedro Esteban
3.5.09 / 4am


I propose a new typology, art as architecture, jaja.  

The psychological aspects of the architect has an important value here, depending of his point of view, an their experience, the architect chooses the artist with whom he feels more identified.
5. Josh
3.5.09 / 4am


Architecture = Form + Function  

 or  

 Art + Science


Art and architecture may share two very important abilities, the abilities to simplify our life and expand our consciousness… two notions that when paired together become very powerful.
7. Nuno Varela
3.5.09 / 1pm


The problem (or fortune) with this ART TO ARCHITECTURE approach is that, in this new 21st century, we will have no more “artists” to follow, because, the so called modernist and imposed separation of the arts, was just another non democratic way to limit the human spirit. 


Nuno





	1. [generall](http://General.la)
	6.7.09 / 8pm
	
	
	ARTISTS AND SPIRIT are inevitable human archetypes/ and will exist in any generation/ beyond any societal oppression. Do not give up so easily, creativity dies first in antidecisions.
9. Mark
3.5.09 / 2pm


For the most part, my opinion on the relationship between art and architecture is simple:


Architecture is reality and art is a representation of reality and therefore, architecture should not try to mimic art. Instead, architecture should create an environment that an artist would want to represent in their art.


Although, I do find exceptions to this rule of thumb in art that attempts to portray how things could be…which it is then the architects responsibility to bring the idea into reality.





	1. Raed
	12.23.10 / 11am
	
	
	Totally wrong, where i disagree that art is just an environment that an artist would want to represent in their art,, No way.
	
	
	Architecture came from Art, the art of architecture is founded from the scratch of arts to from an Architecture.
	
	
	I am an Architect and studying in the field of arts.  
	
	thanks
11. [serraglia](http://www.serraglia.com/)
3.6.09 / 4pm


“A project, drawn on paper, it is not architecture, but only a more incomplete representation of the architecture, comparable to music paper.  

The music needs the execution.  

The architecture needs the realization.”  

 – Peter Zumthor –


In my opinion the relationship between art and architecture is very DANGEOURUS:


Even in the last Architecture Exhibition in Venice there was a lot of images instead of spaces.  

Architecture is not creating anymore spaces but images.


Creating a “flat” architecture / without any space experience.


What is Architecture now days?


is it to design a space?


or


is it to design a shape?
13. [s](http://sp.com)
3.7.09 / 1am


❤ how the coffee-table book and PhD dissertation are treated as analogous in the third paragraph, but unless the discourse about art and architecture is resolved in the schools there is little progress, except by the punks. Rock n Roll.
15. ysgs
3.8.09 / 2pm


After seen those comparisons above I couldn't help but finding the irony in getting a first look at the design by Saha Hadid Architects for the new port house in Antwerp.  

<http://www.architecturelist.com/2009/01/27/new-port-house-in-antwerp-by-zaha-hadid-architects/>


This of course brings in mind a different project;


<http://www.zugmann.com/online_exhibitions_viewer.php?exhibition=transforming&id=1>


I might agree with some of the comments above regarding the differences between built architecture and the one that's stays on paper, in its linguistic sense I think the meaning of architecture should probably exceed the two dimensional drawing or the computer screen.  

Never the less one can not dismiss the vast importance and the “after effects” some more theoretical works (if we choose to call them that way) done by artists and architects had on our actual built environment.  

I mean; haven't the works of Archigram exceeded their drawings in projects like the Pompidou centre (without taking the due respect from Richard Rogers and Renzo Piano)  

Don't their ideas get built (into some extent) into full scale living architecture in a vast variety of projects nowadays, changing our built environment?  

And if so, wasn't that the original goal?
17. Marc K
3.9.09 / 2am


In my own opinion, one may not say the line between Art and Architecture blurs, but that the two have been analogous since they have been placed together (be it baroque, rococo, neo-classicism, whatever) The fact remains that those who do not treat architecture as art have separated the two into ‘defineable realms. 


The quote by Zumthor makes complete sense, and rightfully so, but what is missing is the mutual relationship between the music composition as well as the executed piece; one exists not without the other; in which case it is improvised . And architecture may react in a similar way (much like the work of gehry).


Art and architecture are a means to an end, or an exploration in a spatial experience. This is where I fail to see the difference.
19. Pedro Esteban
3.9.09 / 3am


I see in the work of Lebbeus some of the Russian contructivism. His work is like contructivism but on ours days and more charged.
21. charles
3.17.09 / 5pm


it seems as if today both art and architecture are shedding rules with which to play the game. Architecture, however, has had a harder time in getting away with it, no matter how detached it try's to be it cannot escape every rule. Art however has been shoved off the precipice of logic and we look forward to when it reaches the bottom. In the mean time it is caught in a no space, no logic, no rule vacuum. There is no movement, no congruence, no spirit. Only a dry, sardonic, sarcastic cynicism for the world outside and even worse for itself. It is not even conceptual art now, because even that has a matter. It is a no-art, it is about using as much as possible to show the least, and ultimately nothing. Does anyone else feel this?
23. tabb
3.30.09 / 11am


The ultimate difference between art and architecture, in my opinion, would be that architecture always has to find certain solutions to certain problems or give answers to questions whereas art never has to deal with such a responsibility.
25. [Comments on ART TO ARCHITECTURE « LEBBEUS WOODS | A Synthetic Architecture](http://amcgoey.net/commentary/comments-on-art-to-architecture-%c2%ab-lebbeus-woods/)
5.6.09 / 2am


[…] ART TO ARCHITECTURE « LEBBEUS WOODS – Lebbeus Woods asks the question about the relationship of Art to Architecture.  He points out that as the arts like painting and sculpture became less integrated with Architecture, they actually gained a more direct influence on architecture.  Suddenly Art could be used as a direct inspiration for Architecture.  It is certainly an interesting observation. […]
27. [LIBESKIND'S MACHINES « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2009/11/24/libeskinds-machines/)
11.24.09 / 8am


[…] In the past, architects such as Mies found architectural inspiration in works of art (see the post Art to Architecture), while Le Corbusier produced his own paintings and sculptures to work out complex aesthetic […]
29. Nenad Stjepanovic
12.24.09 / 1pm


sometimes a picture can speak a million words
31. sara tilley
1.29.10 / 12am


the duchamp collage is amazing – i have never seen this as a precedent for hamilton's collage. What is it called?
33. [lebbeus woods on libeskind's machines | adomnitz\_gsd\_thesis2011](http://thesisshmesis.wordpress.com/2010/10/27/libeskinds-machines/)
10.27.10 / 3am


[…] In the past, architects such as Mies found architectural inspiration in works of art (see the post Art to Architecture), while Le Corbusier produced his own paintings and sculptures to work out complex aesthetic […]
35. [arronouri](http://dieta2011.jkr30.com)
5.21.11 / 4pm


Thank you this specific interesting blog post. I am going to glimpse for your weblog often right now. I am contemplating this theme considering decades in addition to you may have very good infos. Greetings via Indonesia  

<http://video.jkr30.com/a/> – mein blog
37. [Lebbeus Woods on Libeskind's Machines « Boris Cortes Sci-Arc Thesis Blog](http://sciarc2012cortes.wordpress.com/2011/09/30/lebbeus-woods-on-libeskinds-machines/)
9.30.11 / 10am


[…] In the past, architects such as Mies found architectural inspiration in works of art (see the post Art to Architecture), while Le Corbusier produced his own paintings and sculptures to work out complex aesthetic […]
39. bboyngui
2.24.12 / 8pm


Like any piece of art or architectural design, the concepts of art and architecture themselves can be interpreted in so many different ways that all your arguments here are pretty much moot.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.25.12 / 4pm
	
	
	bboyngul: Your dismissive point of view is not helpful in sorting out the actual differences—historically speaking—between architecture and art, and how they have impacted societies, as well as the development of architecture and art themselves. Fortunately, there as many people interested in these distinctions, in debating them, and adding thereby to human knowledge.
