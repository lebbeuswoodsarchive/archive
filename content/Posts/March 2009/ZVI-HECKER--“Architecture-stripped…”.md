
---
title: ZVI HECKER  “Architecture stripped…”
date: 2009-03-19 00:00:00
---

# ZVI HECKER: “Architecture stripped…”



**Architecture stripped of its ornate garment**


The continuous unfolding of the world economic crisis not only inflicts hardship on the well-being of individuals and societies, but will inevitably create radical shifts in our aesthetic sensibility.


Taken unguarded by the collapse of the world stock markets and the demise of financial institutions, we should not be surprised by the deepening of the moral-ethical breakdown that generated this economic crisis in the first place.


The erosion of the moral-ethical standards caused by the decline of personal responsibility and institutionalized social inequality and injustice may prove to be more destructive than a military force. History's graveyards carry the names of great military powers that ran their course and disintegrated into a total break down of their political structures, even before their legions reached the battlefield. Rebuilding the moral-ethical foundations that have been undermined in the present crisis will be more laborious and will take longer than to arouse the appetite of the panic-stricken credit-financed consumerism.


Architecture, while embracing the human dimension, constitutes an integral part of the economic landscape. It therefore can't be absolved from the moral-ethical dimension of the present crisis, nor is it immune from the fallout of the economic slowdown and the appearance of a new aesthetic perception.


For more than a decade architecture sucked-in cheap and abstract money that was channelled to fuel an excess of building construction, resulting in the infamous sub-prime mortgage meltdown.


Abstract projects solidified into Architectural form, and, sponsored by oil and stock market wealth, were “grounded” in the most socially unjust locations and in the most environmentally wasteful ways. Real-estate, disguised as Architecture, falsely credited with sustainability, turned out to become the profitable terrain-for-surplus capital, absorbing into its ever more elaborate shapes money that could have been invested otherwise.


The more obscure and environmentally irresponsible were the financial investments, the more excessive became the Architectural form. In its most extreme version the Architecture's mere existence became its function, just as the inflated growth of the financial market became its only *raison d'être*.


Architecture, like the world at large, turned a blind eye to global poverty and enduring conflicts. Equally indifferent to ethics, Architecture preferred instead to glorify the zeal and the leverage of financial wizardry. Draped in layers of ornate garments, glamorous and decorative, it carefully disguised its narcissistic genesis.


Strangely enough, this self-referential Architecture of negligible conceptual depth was embraced as long overdue evidence of the multifarious talents of the Architect. Long said to be inhibited from expressing his talent, the enterprising practitioner responded eagerly to overseas requests for colonial patronage to adorn repressive regimes with warmed-over-architectural images. Obsessed solely with maximum visibility, Architecture relied on the image of the “Architect as Artist” committed only to his or her inner fantasies and desires, “Architect as Designer” engaged in designing clothes, fashion collections, ashtrays and carry bags, and “Architect as Entertainer” staging pseudo-intellectual spectacles.


No longer required to follow the rules of logic, coherence and clarity of the plan, the “Architect as Architect” became rapidly irrelevant. This may explain why in recent years so very few significantly innovative designs emerged in Architecture's core fields of engagement: solutions for housing, urban design, and integration of the socially deprived, subjects which were the bedrock of the Modern Movement.


Denied any incentive to explore and innovate, the Architect thrived on the work of earlier generations in a kind of parasitic subsistence. Old architectural schemes and banal off-the-shelf plans were hastily recycled and wrapped within a dress of different materials, glass at the top of the list. To broaden its appeal, glass elevations were belligerently promoted as being ecologically sustainable and environmentally friendly. Heavily dependent on sophisticated high-tech for its functioning and maintenance, the environmental claims were never confronted nor seriously contested.


However, paradoxically, this all-glass Architecture found its partner and prey in the world of banking and international business. With its claim for the virtues of transparency, glass Architecture offered respectability and supplied the best possible alibi for the murky transactions it wrapped so elegantly. In today's crisis the glass alibi might be short-lived and insufficient in restoring the vanished trust in the operations of business.


Even Berlin, not yet carried away by the hysteria of capitalist development, yielded to the pressure of historians promoting architectural nostalgia in disregard of the legacy of radical modernism that the city harbours so proudly. Berlin's pseudo-aristocratic genealogy will be rightfully restored by rebuilding fake elevations of the eighteenth century castle. Of no great architectural merit in its original version, the fake replica of the castle will become a farce. The ultimate irony is however that the Berlin of today is unable to distinguish between stylistic novelty and true originality, thereby excluding any possibility for a refined masterpiece to be recognized and welcomed.


Essentially, every economic crisis not only breaks with the immediate past but provides moments of accelerated change, an opportunity to transgress the present status quo and to leave a contemporary footprint.


The crisis of the late 1920s and the Great Depression that followed was such an intense force that it wiped out the ostentatious ornament of late nineteenth century classicism. White, plain and undecorated, the emerging Architecture was a clear break with the past and was total anathema to that which it replaced.


The underlying roots of the two crises, though eighty years apart, stem from a soil contaminated by the level of dishonesty to which financial institutions had sunk.


A moral-ethical position will be needed to put into motion creative forces that were silenced by the wide spread decadence. A natural change of our aesthetic perception will follow.


The inevitable slowdown of building construction and the emergence of another aesthetic reality will provide a fertile ground for the germination of new ideas. They will be conceptualized, developed and codified, like musical notes, through architectural plans, built years later when the economy picks up again.


Architectural form is a reflected image of the idea that inhabits the plan. Hierarchies of human scale is its measure, and clarity of intention its means of to beauty. It unites needs and dreams into ever new aesthetic sensibilities. This inseparable duality is what makes Architecture such a uniquely profound profession.


Centuries of creative commitment and the endowment of new ideas generated a rich architectural tradition. It is entrusted to us on the condition that our own generation will enrich and broaden the horizons of this great heritage.


In our ever-changing world, Architecture's eternal relevance lies in its degree of idealism and its responsibility to alleviate the contemporaneity of the human condition. New ideas are the sole means of its attainment.


**Architecture is a human art, never humane enough.**


 **Zvi Hecker**


**January 2009**



Zvi Hecker is an architect living and working in Berlin and Israel. He has designed a number of memorable building projects in Europe and the Middle East.


<http://www.google.com/search?client=safari&rls=en&q=Zvi+Hecker&ie=UTF-8&oe=UTF-8>


<http://www.zvihecker.com/index_content.html>



 ## Comments 
1. [Simon's World » Blog Archive » An interesting take on today's digital baroque](http://cygielski.com/blog/2009/03/22/an-interesting-take-on-todays-digital-baroque/)
3.22.09 / 3am


[…] (more at <https://lebbeuswoods.wordpress.com/2009/03/19/zvi-hecker-architecture-stripped/>) […]
3. [orhan ayyuce](http://archinect.com)
7.23.09 / 2am


I have now connected Mr. Hecker's article to this conversation.


<http://www.archinect.com/features/article.php?id=90418_0_23_0_M>
5. [e-cultuur weblog» Weblog Archief » Rien Ne Va Plus at Bureau Europa in Maastricht](http://www.e-cultuur.be/weblog/?p=1936)
12.9.09 / 1pm


[…] absorbing into its ever more elaborate shapes money that could have been invested otherwise." Zvi Hecker, January […]
7. [Greenhoof » Blog Archive » Rien Ne Va Plus at Bureau Europa in Maastricht](http://www.greenhoof.com/2009/12/09/rien-ne-va-plus-at-bureau-europa-in-maastricht/)
12.9.09 / 9pm


[…] absorbing into its ever more elaborate shapes money that could have been invested otherwise.” Zvi Hecker, January […]
