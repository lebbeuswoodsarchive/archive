
---
title: NOTEBOOK 01-2
date: 2009-03-22 00:00:00
---

# NOTEBOOK 01-2



From the cover alone, it is possible to see that this notebook is different from the many that preceded it. First of all there are no places attached to the dates, indicating that this was no longer a travel notebook, but one I worked in only ‘at home.' Then, tiny flecks of color appear here and there, giving a clue that at least some of what is inside is different, as indeed it is, in method and materials.


 As I was not traveling at the time, I felt secure in expanding my drawing tools beyond the simple ink pen to crayons, though these, too, are more resistant to smudging than, say, graphite pencils. But this was a more important reason for this shift: the need to use color. Not only the color of lines and tones—which ‘advance' or ‘recede' according to well known principles—but also of the ‘ground' on which I drew, which was predominantly black. Black, in the ‘pigment' spectrum is—like white in the ‘light' spectrum—the ideal mixture of all colors.


 It is interesting how our perceptions of figure/ground relationships yield different readings of space. It is an accepted fact that black lines on a white ground reads as more spatial than white lines on a black ground. The reasons for this probably begin with convention, that is, what we are used to. White (or colored) lines on a black ground seem more abstract, thus also more flat, less suggestive of space, or the illusion of space on the flat page. Still, this very fact creates a paradox and conceptual tension when we draw white-on-black. We set up a more intense play between abstraction and representation, between flatness and spatiality. These reasons to do this are conceptual: our ‘subject' is itself ambiguous and gains clarity from the perceptual ambiguity.


 There are also philosophical issues. Are we drawing shadows (black on white), or, are we drawing light (white on black)? Is the ground a radiant field of light into which we introduce darkness? Or, is it a dark void into which we introduce light?


 Certainly, the black ground gives a somewhat foreboding impression, as though the lighter marks we make are in danger of being swallowed up by darkness, and lost. Light (which we often equate with life) struggles for survival. However, the white ground gives an impression of overwhelming radiance, within which we must find some shelter, some safety or comfort, from the overwhelming imperative to live that blind instinct imposes. Is it better to start from white or from black? These are two different views of existence, and we can take our choice at any given moment, realizing that it will not always be the same for all others.


 Drawing is a confrontation with meaning. The notebook is a first line of encounter and  engagement.


LW


![lwblog-nb001-2-cover](media/lwblog-nb001-2-cover.jpg)


Excavation studies:


![nbk01-2-5](media/nbk01-2-5.jpg)


![nbk01-2-6](media/nbk01-2-6.jpg)


![nbk01-2-2](media/nbk01-2-2.jpg)


![nbk01-2-2b](media/nbk01-2-2b.jpg)


![nbk01-2-14](media/nbk01-2-14.jpg)


![nbk01-2-111](media/nbk01-2-111.jpg)


Wall drawings with metal tension cables and suspended wood elements from *The Storm* installation:


![lwblog-storm11](media/lwblog-storm11.jpg)


![lwblog-storm2](media/lwblog-storm2.jpg)


Meditations studies:


![nbk01-2-3](media/nbk01-2-3.jpg)



![nbk01-2-3b](media/nbk01-2-3b.jpg)


![nbk01-2-161](media/nbk01-2-161.jpg)


![lwblog-nb001-2-20](media/lwblog-nb001-2-20.jpg)


Wall drawings with metal cable and rod vector fields from the *System Wien* installation:


![lwblog-nb01-2-sw2](media/lwblog-nb01-2-sw2.jpg)


![lwblog-nb01-2-sw11](media/lwblog-nb01-2-sw11.jpg)



 ## Comments 
1. Pedro Esteban
3.23.09 / 2pm


Lebbeus Woods:  

It's interesting the ground is so ambiguous—for some it could be something, and for others it is only pigment.


In the works of Lissitsky—I do not like to say pictures because these works have so much strength, that for my they were not realized in 2D, for me they need another classification—we can see an undeniable space, it's amazing. Is space only in the ground, in the figure, in all the (x)?
3. Diego Peñalver
4.3.09 / 6pm


Hi Lebb,  

I enjoyed this drawings and images very much. Interesting explorations of imaginary spaces that recalls some compositions from Lászlo Moholy Nagy, mainly your color sketches remind me of some of his photograms and light experiments.  

In these sketches light and movement are fascinating…
5. [Rocky Patel](http://www.texcigars.com/Cigars/Rocky-Patel/)
5.29.09 / 3pm


Lászlo Moholy Nagy? Maybe. I can't believe how well you're able to capture light movement. awesome!
7. [Darkness or Light « Phos + Skia](http://skiaphos.wordpress.com/2012/05/22/black-on-white/)
6.13.12 / 6pm


[…] WOODS     Notebook    97-3   98-3   01-2   […]
