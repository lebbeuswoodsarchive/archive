
---
title: FOR ONCE
date: 2010-11-11 00:00:00
---

# FOR ONCE


For once I really *like* the English. Respect and admiration are very different from *liking*, which is the first emotion I felt on [reading](http://www.nytimes.com/2010/11/11/world/europe/11london.html?scp=1&sq=protests%20turn%20violent&st=cse) about students protesting in London yesterday over the new Government's austerity program, which will drastically cut its support of education. The students are not going gently into that good night. Makes me wonder what—if anything—will happen in the USA when the new Congress announces its austerity program, with the same sorts of cuts to education and other ‘entitlements' like Medicare, rescinds health care reform, while extending tax breaks for the rich.


Interesting, is it not, how the protesters' violence is directed towards buildings symbolizing political and economic power, and not towards the actual persons responsible for the situation. Ah, architecture…. Ah, civilization!


LW


.


[![](media/protest-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-1.jpg)


[![](media/protest-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-1a.jpg)


.


[![](media/protest-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-2.jpg)


[![](media/protest-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-2a.jpg)


.


[![](media/protest-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-3.jpg)


[![](media/protest-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-3a.jpg)


.


[![](media/protest-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-4.jpg)


[![](media/protest-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-4a.jpg)


.


[![](media/protest-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-5.jpg)


[![](media/protest-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-5a.jpg)


.


[![](media/protest-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-6.jpg)


[![](media/protest-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-6a.jpg)


.


[![](media/protest-71.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-71.jpg)


[![](media/protest-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-7a.jpg)


.


[![](media/protest-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-8.jpg)


[![](media/protest-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-8a.jpg)


.


[![](media/protest-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-9.jpg)


[![](media/protest-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-9a.jpg)


.


[![](media/protest-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-10.jpg)


[![](media/protest-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-10a.jpg)


.


[![](media/protest-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-11.jpg)


[![](media/protest-11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/protest-11a.jpg)


.



 ## Comments 
1. Nicholas Pevzner
11.12.10 / 2pm


Yes, it seems that “violence” directed at buildings symbolizing institutions is as bad as VIOLENCE directed against persons. Or so it is reported in the news, where no distinctions are made between the two. But while violence in self-defense is accepted as legitimate under the law, breaking windows in exchange for harmful policy that hurts large numbers of actual people is treated as a crime, or recently even more severely, as “Terrorism” or “Conspiracy”…
3. [Stephen](http://skorbichartist.blogspot.com/)
11.12.10 / 10pm


Tory Scum,  

The building does look rather corporate/governmental. I'm sure there are some architecture students there protesting for purely aesthetic reasons.  

So subsidized education has finally caught up with the UK, too bad.
5. Steven Shimamoto
11.15.10 / 5am


I would be pissed too. Here in California, I have heard that some public schools have started to graduate students after a maximum of 120 credits or so, even if they do not have their desired degree. A student I went to high school with was trying to register for classes last fall and found out that he had already been graduated!! Ridiculous. Although California does have a huge economic problem as of now, it still does not make sense to take away the bearings of America.
7. Pedro Esteban
11.15.10 / 11pm


The architecture as a stress relief. 


SUPPORT to all them!!!!!  

 I know what is have problems with the university.
9. [Chris Teeter](http://www.metamechanics.com)
11.17.10 / 3am


I've been thinking about this “need for a college degree” thing for a while. let's say I went from high school into the labor union in NYC, I would be making more money with better benefits than my Masters in Architecture and most guys who run their own firms.


i'm not sure everyone needs to spend 120 credit hours learning more, especially very basic classes in open colleges (like most state colleges)…say Sociology 101 that is about as challenging as 2nd grade art class. most people just goof off another 4 years in college drinking.


why not just compress the education back into elementary and high school and decrease the demand for a college degree then tuitions would have to be competitive to attract students, instead of $50K a year to finance the football program…USC spends how much on football? (not even ranked this year)


sure many colleges would go out of BUSINESS, but it's not a business. 


I think if given the oppurtunity a country like England maybe just re-invent everything.





	1. Steven Shimamoto
	11.23.10 / 9am
	
	
	Yes, you make very good points. So I am a bit nervous about throwing myself into the field of architecture. I hear a lot about architecture graduates being out of work these days. I am determined to take on the challenge, but I wonder…is it worth it? I want to get my education to create something and not just theorize and talk about the great things that others have done. Hopefully it will all get better with the economy?
11. tom
11.17.10 / 3pm


<>  

This is already done in the UK, relative to education in the US .
13. tom
11.17.10 / 3pm


Compressing education…..
15. Dylan Fan
11.19.10 / 12am


Unlike Americans, the Brits are smart enough not to saddle themselves with debt. This enables them to be active protestors when the gov't screws them over. 


With the high level of debt in America, most college students and recent grads are pacified to work so they can pay off their ginormous debts.
17. Greg Miller
11.30.10 / 2am


What I've found helpful is understanding that no one owes me a damn thing. Try it as your new mantra. Say it with me; “No one owes me a damn thing!” Again; “No one owes me a damn thing!”


Here's another one; “My neighbor is not responsible for my education!” Again, “My neighbor is not responsible for my education!”


And another; “I can't afford a college education so I think I'll go to trade school!” Again; “I can't afford a college education so I think I'll go to trade school!”


One last one; “I'm responsible for my own happiness!” Again; “I'm responsible for my own happiness!”


Keep saying those over and over until they sink in and I guarantee that you'll not have the urge to throw a temper tantrum when you don't get your way. It's actually quite liberating.
19. Jah
12.16.10 / 9pm


I love this so much. Yes, what will happen in the good ol' USA I wonder. A much stronger police presence no doubt. Should be fun.
