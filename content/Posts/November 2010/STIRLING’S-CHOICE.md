
---
title: STIRLING’S CHOICE
date: 2010-11-17 00:00:00 
tags: 
    - architecture
    - architecture_books
    - James_Stirling
---

# STIRLING'S CHOICE


[![](media/stirling-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-7.jpg)


*(above) The History Faculty Library at Cambridge University, by James Stirling and James Gowan, (1968).*


This is a moment to celebrate—and critically reconsider—the architecture of [James Stirling](http://en.wikipedia.org/wiki/James_Stirling_(architect)). An [exhibition of his work](http://ycba.yale.edu/exhibitions/exhibition_future-details.asp?exhibitionID=%20301) is currently at Yale University and will travel to several venues in the year ahead. A new book, *[James Frazer Stirling: Notes from the Archive](http://www.amazon.com/James-Frazer-Stirling-Archive-British/dp/0300167237/ref=sr_1_1?ie=UTF8&qid=1289758317&sr=8-1)*, has come out and is filled with reproductions of his many drawings for projects built and projected, as well as excerpts from his notebooks over the years. It's a fascinating portrait of the architect and his ideas.


Whether one likes his work or not, one cannot help but admire Stirling's energetic commitment to architecture, his playful seriousness, his immense design discretion, and his stubborn willingness to explore the possibilities of architecture as he saw it—a field of human experience both wide and deep. While he never spoke publicly, in times of social upheaval, about his ethical stance, he did reveal it in his abiding concern for his buildings' contexts. Even when he imposed on a given site a building aesthetically at odds with its neighbors, he would find a way to respect them, through massing or scale or materials. In this regard he was eminently civilized, a trait that could end up as it did with many of his contemporaries in either fawning submission or ironic mockery, but it was never so with Stirling—his particular genius was to be individualistic and innovative, yet at the same time respectful of society and its history. His example is one that—in these days of often shallow and gratuitous form-making—we would do well to remember.


LW


.


*(below) The Engineering Building at the University of Leicester, by Stiring and Gowan, 1959:*


[![](media/stirling-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-5.jpg)


[![](media/stirling-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-4.jpg)


[![](media/stirling-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-6.jpg)


*(below) The State Art Gallery, Stuttgart, by Stirling and Michael Wilford, 1977-83:*


[![](media/stirling-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-1.jpg)


[![](media/stirling-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-3.jpg)


*(below) School for Music and Theater, Stuttgart, by Stirling, 1987*:


[![](media/stirling-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-8.jpg)


.


Anthony Vidler, the book's author, writes:


“Who was James Frazer Stirling, more popularly known as Jim Stirling or “Big Jim”? In my own opinion and in that of many others, he was simply one of the very best architects of his generation, if not, as Robert Maxwell has stated, the very best architect of the second half of the twentieth century. As an architect, together with his early partner James Gowan, and then with his long-time associate and partner Michael Wilford, he built some of the most iconic and ground-breaking buildings of the 1970s and 1980s, starting with the Leicester Engineering Laboratories, the Cambridge History Faculty Library, the Queen's College Oxford student housing. In each of these three he experimented with great exuberance and brilliant formal gestures to bring the traditional forms of these building types into contemporary relevance. In the realm of housing, a central problem in Britain after the war, the firm built a housing development at Preston in Lancashire, with a sensitivity to the existing context that was revolutionary for the time; a large portion of Runcorn New Town housing also referred to traditional urban contexts, this time the Georgian squares of Bath and London but in an entirely modern technology of pre-fabrication, and social amenities. Unbuilt and built projects in these years experimented with the vocabulary of traditional industrial building but with new materials and compositional techniques—Dorman Long headquarters, Olivetti Headquarters, and the Olivetti training center at Haselmere which was built.  A sequence of museum projects in the mid-1970s  from two competitions, Dusseldorf and Cologne, led to the winning competition at Stuttgart where the firm built the Neue Staatsgalerie, a work that I think is his masterpiece. His long affection for the United States resulted in a number of commissions, notably the Performing Arts Center at Cornell, the Science Library at Irvine, the Sackler Gallery at Harvard, and the School of Architecture at Rice. In each of these projects Stirling was sensitive to the architecture of his context, and to the associational values that stemmed from his own contributions to it. Thus the Italian Hill town aspect of the Cornell Campus was equally echoed in his Arts complex. Thus the school of architecture at Rice was clothed in a garb that took from the traditional Italo-Spanish vernacular of the campus within which was a wholly modernist composition.


All this however was not achieved without controversy. He caught it from both sides. From early on modernist critics were scornful of his break with modernism—Preston looked just too Victorian, and in Stuttgart his great cylindrical courtyard and use of stone led modernists to accuse him of returning to a Speer-like fascism; on the other side, traditionalists refused his modernism—the opposition of the classical side of the History Faculty at Cambridge has never given up their campaign to raze the building. Queen's College Oxford has let the Florey Building decay drastically in a move to tear it down; the resistance to his color schemes at the Tate was fierce; the opposition to No 1 Poultry has never quite died down. The fact that his housing developments suffered demolition led to critics comparing the fate of his work to that of Pruitt-Igoe.


He himself often said that his architecture tended to oscillate between tradition and modernity, and his deep respect for the neo-classical tradition, was evinced in the elegantly assembled gallery spaces of the Clore Gallery at the Tate Britain, constructed to house the J.M.W. Turner collection and evoking the spatial organization of John Soane's Dulwich Picture Gallery. This sense of embracing the entire history of architecture as a source for his own work, came to a head in the entirely original assemblage of the Wissenschaftzentrum in Berlin, where the different social science institutes are gathered as if in a small city composed of a basilica, an amphitheater, a castle, all brought together by a long stoa, evoking the stoa-like entrance of Schinkel's Altesmuseum. Finally the extraordinary complex of buildings for the Braun Headquarters in Melsungen, Germany, completed just before his untimely death, are a dramatic conclusion as a small industrial town set in its verdant landscape.”


**Anthony Vidler**, New York, November 12th 2010. [Anthony Vidler](http://cooper.edu/architecture/faculty2/anthony-vidler/) is Professor and Dean of the Irwin S. Chanin School of Architecture at The Cooper Union in New York City.


.


The following plates are not arranged in any way sequentially, but are meant to give the merest hint of  the rich and extensive trove of Stirling's work presented in the book:


Black notebook: Stirling's diagrams of the evolution of Modernism:


[![](media/js-7a1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-7a1.jpg)


Model of the Library at the University of Cambridge:


[![](media/js-4a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-4a.jpg)


For the Engineering Building at the University of Leicester:


[![](media/js-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-5a.jpg)


Study of faceted glass building wall:


[![](media/js-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-6a.jpg)


For the Florey Building, Queen's College, Oxford:


[![](media/js-31.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-31.jpg)


For the Siemens HQ, Munich:


[![](media/js-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-2a.jpg)


For the Science Center, Berlin:


[![](media/js-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-9a.jpg)


For the Wallraf-Richartz Museum, Cologne:


[![](media/js-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-8a.jpg)


For the British Olivetti HQ, Milton Keynes, England:


[![](media/js-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/js-1.jpg)


.


(below) From the exhibition, *[Notes from the Archive: James Fraser Stirling, Architect and Teacher](http://ycba.yale.edu/exhibitions/exhibition_future-details.asp?exhibitionID=%20301)*, curated by Anthony Vidler, at the Yale Center for British Art:


[![](media/stirling-exhib-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-exhib-1.jpg)


[![](media/stirling-exhib-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/stirling-exhib-2.jpg)


#architecture #architecture_books #James_Stirling
 ## Comments 
1. [Tweets that mention STIRLING'S CHOICE « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/11/17/stirlings-choice/?utm_source=pingback&utm_campaign=L2)
11.18.10 / 3am


[…] This post was mentioned on Twitter by Alfonso Medina, KpaxsArchi. KpaxsArchi said: STIRLING'S CHOICE: (above) The Library at Cambridge University, by James Stirling and James Gowan, (19–). This … <http://bit.ly/9bgyyc> […]
3. [MM Jones](http://www.bauzeitgeist.blogspot.com)
3.1.11 / 7pm


Does nobody love One Poultry?
