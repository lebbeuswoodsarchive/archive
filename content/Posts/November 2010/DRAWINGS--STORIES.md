
---
title: DRAWINGS, STORIES
date: 2010-11-02 00:00:00 
tags: 
    - architecture
    - storytelling
---

# DRAWINGS, STORIES


[![](media/dwg-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/dwg-1.jpg)


The truth is, the so-called New City was not so new. Many of the buildings seemed in a state of decay, or at least what we might call ‘deferred maintenance.' Also, the buildings appeared to each have been built in several very different styles or stages, which suggests that their construction—indeed their very conception—must have taken considerable periods of time, perhaps whole epochs. The other thing is that it was clearly not a city, at least in any usual sense. True, it was a collection of buildings densely gathered, and the buildings were large, suggesting a substantial population of inhabitants. We could only see the city from afar, so we could not see people—if any were actually there—or other signs of normal daily life, such as roads, parking lots, movie theaters, marketplaces, shopping malls. There were only imposing, austere structures grouped like sentinels guarding some secret. Sitting in an open, though not barren, landscape, the monumental ensemble reminded us of a mirage, something only imagined, desired, or feared.


.


[![](media/lwblog-drwgstory-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/lwblog-drwgstory-11.jpg)


The building looks like a ruin, but is far from it, only a few decades old (so we learned) and fully lived in over that span of time by the usual assortment of urban characters—workers, stock brokers, boutique managers, pole dancers, architects—all those dependent on the intensity and density of the city for survival. Wandering around the building for a while, but not actually going inside, we impulsively guessed that it had originally been built as a kind of free-standing, large-scale sculpture. One of the locals we spoke to explained its history this way: “In this town, we have the philosophy, build a building any way you want to, then we'll move in and figure out how to live in it!” At this we just shrugged. “Isn't that the way it always is? We're born into a world we didn't create and we just have to learn how to live in it!” She laughed, a little ruefully, and said, “Sure. But most people don't think that way. They think they design buildings for a purpose and that people move in to live that way.” “So?” we said. “Yeah, well there's a guy in this building who says he's an architect who designs what he calls “functional” buildings. He says he knows what people want, or at least what they really need. But he gets very upset when people move in and do something else, like knock a hole in the wall for a new window!” “Oh,” we said. “Or knock down the walls inside and maybe a hole in the floor, or even take out the floor or tilt it in a funny way” she went on. “It spoils his functional design, he says.” “Oh,” we said. Being architects ourselves (which we didn't bother to mention), we simply thanked her for her time, said goodbye, and went on our way.


LW


#architecture #storytelling
 ## Comments 
1. [Tweets that mention DRAWINGS, STORIES « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/11/02/drawings-stories/?utm_source=pingback&utm_campaign=L2)
11.3.10 / 12am


[…] This post was mentioned on Twitter by Ozgur Uckan, Cesar Reyes Nájera. Cesar Reyes Nájera said: Dystopic + poetic…Lebbeus > RT @ozuckan: DRAWINGS, STORIES « LEBBEUS WOODS <http://ff.im/-t3HpB> […]
3. [Chris Teeter](http://www.metamechanics.com)
11.3.10 / 2am


enjoy this stuff. forgive me for being uninformed possibly, is this an outake from something larger?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.3.10 / 3am
	
	
	Chris: Yes. An urban project of mine called Centricity. It hasn't been seen in a while, so I think I'll publish it here a bit at a time. The little stories are, of course, new.
5. [moon hoon](http://dollshoii.wordpress.com)
11.4.10 / 11am


techno\_Guadian\_eternity\_past\_future\_and\_darkly romamtic…
7. mchart
11.6.10 / 4am


Reminds me of Christopher Alexander's ideas, as well as those of Stewart Brand in How Buildings Learn … specifically that architecture will be more functional if it is  

customized by its users to adapt to their needs over time. Your drawing takes those ideas to a new aesthetic and theoretical level, however … I never before thought about it in the way that you described.
9. [Joseph Sarafian](http://myportfolio.usc.edu/jsarafia/)
11.6.10 / 11pm


This post addresses a fundamental paradox in the conception of architecture design and construction. In most cities, architects design buildings to meet the needs of people, but the inhabitants are subject to the intentions of the architect, given freedom only to “decorate” their dwelling on a superficial level. This excerpt from your Centricity project poses the question; do architects know what people really need? Can a profoundly new approach be applied to the dialogue between architect and client? 


As in all of your work, the ability to convey a narrative through compelling images sets the mood for the story, but I wonder if the “mirage” would be even more vivid if the illustrations were omitted. The success of such an approach is evident in “Invisible Cities” by Italo Calvino. His fictitious written descriptions of cities narrated by Marco Polo not only evoke mental renderings of endless horizons, they initiate curiosity from the reader. The open-endedness of this imageless approach would parallel the open-source architecture that you are describing in the “New City”. Residents are given the opportunity to not only inject meaning into their dwellings, but they are given the freedom to mold their surroundings to meet their needs. There is an existentialist underpinning in the nature of this argument. Philosophers such as Søren Kierkegaard and Friedrich Nietzsche pose that humans create their own meaning, instead of inheriting it from an omnipotent source. Just as man creates meaning in his life, he also creates meaning in his dwelling. Both notions are reactions to their respective opposites; man inherits the meaning of life from a higher source, and the resident inherits the use of his dwelling from the architect. The society depicted in the “New City” exhibits a democratic decision-making process in which the community is like a swarm of wasps, who initially must conform to the environment which they are presented, (a tree branch for example) but then shape it with a new agenda of reproduction and egg laying. If architecture has the potential for this role reversal, are architects presumptuous in assuming they can address the needs of the residents they design for?
11. Ben Dronsick
11.8.10 / 10pm


This reminds me of a conversation in Curzio Malaparte's “Skin” which I'll paraphrase from memory:


Erwin (Field Marshal) Rommel: “Did you design the house, or was it already here?”


Malaparte: “The house was here.., I designed the scenery.”


In one sense, the images read like designed scenery; an effect the decay only intensifies. Like Kahn's bath house where the architect's intent could be inferred as hastening the inevitable, conditions subsequent owners have ardently sought to reverse.


If truly time is the architect, time is also the client.
13. [The New City: Role Reversal « josephsarafian](http://josephsarafian.wordpress.com/2010/11/29/the-new-city-role-reversal/)
11.29.10 / 10pm


[…] his blog post, “Drawings, Stories“, Lebbeus Woods addresses democratic notions of architecture using a description of a […]
