
---
title: THOM MAYNE’S MIND
date: 2010-11-25 00:00:00 
tags: 
    - architecture
    - experimental_architecture
    - Thom_Mayne
---

# THOM MAYNE'S MIND


What's on Thom Mayne's mind these days? Better yet, what's in it? The reason this matters at all is that he is one of a handful of architects working today who is building projects that are powerful visually and experientially, and are at the same time challenging in terms of the ideas that shape them. There is something to be learned from what he thinks and the way he thinks it.


In his mid-sixties now and at what would seem the height of his career, Mayne is clearly restless. At the same moment when his large-scale commissions are in various stages of design or construction, he has embarked on an exploratory project—a series of physical models—that challenges the architecture he has created up to now, raising new questions about the nature of architectural form and its meanings.


Mayne is best known for his masterly, adventurous deployment of a technological architectural language in the service of complex programs of use that, more recently, also aim for ecological sustainability. There has never been any doubt about the importance he places on form and its transformations, as the name of his office—Morphosis—more than suggests. Whatever architecture may be, Mayne seems to think, it must all come together in tectonic, constructed form that coherently takes a place in the sometimes colliding worlds of the natural and the human. His new model studies push far beyond his previous work, coupling his growing interest in [architecture-as-landscape](https://lebbeuswoods.wordpress.com/2010/09/13/building-landscapes/) with evocations of innovative technologies of building and, perhaps more importantly, of designing built space.


It is pure speculation on my part, but it seems, looking at this series of model studies, that Mayne is thinking something like this: the new, computer-controlled techniques of  designing and building will liberate us from the older type of mass-produced architectural components and therefore the high-tech vocabulary that has, up to now, heavily influenced the formal qualities of modern architecture. Increasingly, all manner of forms will be designed and actually constructed. But why? For the sake of novelty? To create the illusion of progress by wrapping the same old ideas in [new skins](https://lebbeuswoods.wordpress.com/2010/10/11/gehrys-skyscraper/)? Or to serve genuinely new ways of living in relation to ourselves and the earth? Mayne's morphological approach in his series of models creates spaces that can only be inhabited in new ways. In other words, new ways of living must be invented. This architecture calls for hopeful beginnings. Further, its fusion of buildings and landscapes indicates an alternative to traditional, prescriptive planning practices, one that is based on an integration of the artificial and the natural. Further still, the model's differences from one another point to communities of diversity, dominated by no single style or approach. Their freer aesthetic enables a freer ethic. Architecture will no longer be an instrument enforcing an institutional, mass-social ideology, but becomes a vehicle for the expansion of human possibilities, both inwardly and outwardly. People are infinitely various, though they share common human traits, and need not be coerced or seduced into being stereotypes. The architecture they inhabit should be equally free of deterministic typologies, emerging instead from each person's or their community's shared and ever-changing necessities of body and spirit.


In my view, Mayne's is a brave vision, all the more so because it raises more questions than it gives answers. Not the least of these concerns the role of the architect in a continually emerging world. What is an architect to do, confronted with so many variables, possibilities, and unknowns? Fall back to the safety of the familiar, the known typologies, the already successful formulas certified by history? Or adopt an ‘anything goes' attitude? These are equally depressing prospects, I would say. Mayne's courage in creating undiluted architectural worlds is all the more admirable because he could easily go on creative cruise-control at this high point in his life, but that is simply not in his character. It seems that his curiosity, his need to push beyond what he knows, his stubborn faith in architecture as an instrument of thought as well as action, propels him only forward. These models are risky and problematic. They are certain to attract criticism for being unresolved, unexplained, unjustified by tendentious arguments. They may well come to nothing, in terms of applicability to the practice of architecture, or to the future of society. No matter, I say. They open unexpected doors. They excite the imagination. They inspire.


LW


.


(below) A sampling of the new models, each measuring six feet by six feet.


*HOLEYPOD:*


[![](media/holeypod-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/holeypod-2.jpg)


*INTENSECORNER:*


[![](media/intensecorner-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/intensecorner-2.jpg)


*SURFACECUTBYLINES:*


[![](media/surfacecutbylines-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/surfacecutbylines-2.jpg)


*SOLIDDIVIDEDBYVERTICALS:*


[![](media/solidvoidedbyverticals-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/solidvoidedbyverticals-2.jpg)


*BARNACLE:*


[![](media/barnacle-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/barnacle-2.jpg)


*LINESCUTBYSURFACE:*


[![](media/linescutbysurface-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/linescutbysurface-2.jpg)


*LIMABEANFIELD:*


[![](media/limabeanfield-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/limabeanfield-2.jpg)


*PODFIELD (detail):*


[![](media/podfield-det-01-0.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/podfield-det-01-0.jpg)


*PODFIELD (detail):*


[![](media/podfield-det-02-0.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/podfield-det-02-0.jpg)


*BUBBLES (detail):*


[![](media/bubbles-detail-011.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/bubbles-detail-011.jpg)


*BUBBLES (detail):*


[![](media/bubbles_detail-011.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/bubbles_detail-011.jpg)


*BUBBLES (detail):*


[![](media/bubbles-detail-021.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/bubbles-detail-021.jpg)


*HONEYCOMB (detail):*


[![](media/honeycomb-det-02-0.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/honeycomb-det-02-0.jpg)


*HONEYCOMB (detail):*


[![](media/honeycomb-det-01-0.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/honeycomb-det-01-0.jpg)


.


## **WARNING! DO NOT LOOK FURTHER UNLESS YOU WANT TO SEE UNAUTHORIZED RENDERINGS OF THOM MAYNE'S MODELS.**


**.**


I wrote that these are inspiring, so I admit that I could not help myself in taking two views out of their state as objects and into an evocative landscape. I trust to the readers' discretion in understanding my reasons for adding to Thom Mayne's work, without his permission. Also, I am hopeful of his indulgence and understanding!


.


*HONEYCOMB (detail) rendered by LW:*


[![](media/honeycomb-det-02-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/honeycomb-det-02-11.jpg)


*HONEYCOMB (detail) rendered by LW:*


[![](media/honeycomb-det-01-11.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/honeycomb-det-01-11.jpg)


#architecture #experimental_architecture #Thom_Mayne
 ## Comments 
1. [korzac](http://kafee.wordpress.com)
11.25.10 / 7am


Mayne's ( Morphosis) rendering of perhaps new habitable spaces is evocative of “The Shape of Inner Space”, a book on the geometry of the universe by Field medalist winner the mathematician Shing-Tung Yau, in the sense that Maynes is subjugating new visual geometries transgressing the usual views, just as mathematician Yau created the Calabi-Yau spaces which where found invaluable in the ‘rendering' of the inner space of the universe…Well for me the analogy is striking and it remains to see if on practical physical grounds ordinary humans like me and others will find relief an beauty in the new architectures… Just as you write: … “Mayne's is a brave vision, all the more so because it raises more questions than it gives answers”…Anyway, all the pictures are great.
3. quadruspenseroso
11.26.10 / 4am


Well, I think Mr. Mayne's inner mind here is empathetically driven; an ingratiating Calabrian defense of a Continental whore. I think Lebbeus's overlay reminds us of the pain she feels.
5. [THOM MAYNE'S MIND (via LEBBEUS WOODS) « Champion Of Delay](http://championofdelay.wordpress.com/2010/11/26/thom-maynes-mind-via-lebbeus-woods/)
11.26.10 / 2pm


[…] What's on Thom Mayne's mind these days? Better yet, what's in it? The reason this matters at all is that he is one of a handful of architects working today who is building projects that are powerful visually and experientially, and are at the same time challenging in terms of the ideas that shape them. There is something to be learned from what he thinks and the way he thinks it. In his mid-sixties now and at what would seem the height of his car … Read More […]
7. [Tweets that mention THOM MAYNE'S MIND « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/11/25/thom-maynes-mind/?utm_source=pingback&utm_campaign=L2)
11.26.10 / 2pm


[…] This post was mentioned on Twitter by MyarchN and Daniel Díaz, KpaxsArchi. KpaxsArchi said: THOM MAYNE'S MIND: What's on Thom Mayne's mind these days? Better yet, what's in it? The reason this matters at … <http://bit.ly/eeK9Lr> […]
9. chld
11.27.10 / 7pm


I saw Thom Mayne at the airport a few days ago at LAX, wanted to say hello but he looked busy.
11. rkirankumar
12.23.10 / 9am


Sir, reading this blog led me to deeper inquiry of his works which led me to this text ..written in 1989


Part of the ‘Foreword' to the book “Morphosis: Buildings and Projects”(Rizzoli,1997). A fragment of what was in his ‘mind' as a young practice


“Architecture is finally, a means of communication, a way to describe things for which words are inappropriate or inaccurate, and to speak about the culture from which it comes. We take a point of view. Our work concertizes the ephemerality of feeling. It transcribes the complexities of the world and the fragmented, disbursed and detached nature of existence. Our interest in indeterminacy parallels our interest in formal language. Our work reiterates the unfinished nature of things. We hope it unmasks the deceptions of first appearance and explores what we don't see.” Thom Mayne/ Michael Rotondi
13. dana
12.27.10 / 8pm


The honeycomb is my favorite…. as well… i mean
15. [THE DREAMS THAT STUFF IS MADE OF « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/04/the-dreams-that-stuff-is-made-of/)
1.4.11 / 1am


[…] like art than architecture. Interestingly, we both claimed the projects in question—Thom's series of large physical models and my series of large drawings—addressed spatial and tectonic issues most relevant to the […]
17. [SCIARC2012OH](http://sciarc2012oh.wordpress.com/2011/10/17/52/)
10.18.11 / 1am


[…] Thom Mayne landscape studies? I'm not 100% sure what these are, but Lebbeus Woods did a pretty thorough write-up on what he thought was going on in Thom Mayne's head while making these: read here […]
19. [TALKING WITH THOM MAYNE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/04/23/talking-with-thom-mayne/)
4.23.12 / 2pm


[…] But that's a leap, these new models we're making are conceptual territories. Like the last one I've shown […]
21. [Conversations with Lebbeus Woods and Thom Mayne « Just Urbanism](http://justurbanism.com/2012/04/25/conversations-with-lebbeus-woods-and-thom-mayne/)
4.25.12 / 4am


[…] Conversations with Lebbeus Woods and Thom Mayne […]
23. Phillip Brunk
7.11.12 / 5am


If i may make a correction, the models are not 6 feet by 6 feet. They are 3 feet by 3 feet.
