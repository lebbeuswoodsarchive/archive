
---
title: SHRINKING THE WORLD (update 2)
date: 2010-11-10 00:00:00
---

# SHRINKING THE WORLD (update 2)


*(This update includes the introductions offered at the beginnings of the two film showings.)*


The relationship of psychoanalysis to architecture is most intimately revealed in the uncanny number of architects, in psychoanalytic centers like New York and Buenos Aires, who end up married to or living with psychoanalysts and psychotherapists. Whether this is because they share certain professional interests about people and how they think and live, or because the analysts need the down-to-earth architects or the architects the deeply insightful analysts, it's difficult to say. But the connection is there.


On this coming Friday, November 12, the first part of a remarkable documentary film is being shown at [The New School](http://www.newschool.edu/) in Manhattan, New York (see the poster below for details)—*“The Century of The Self.”* While it is not about psychoanalysis and architecture per se, anyone who sees the film and is concerned with the ideas informing the field of architecture will not have difficulty in making the connections. The showing will be introduced by Jane Kupersmidt, a New York analyst (see text below), who will be introduced by the event's organizer, analyst and sociology professor Aleksandra Wagner, who has taught courses like *Psychoanalysis: An Urban Experienc*e at The New School, and the *Manhattan on The Couch* seminar at the Cornell University Graduate School of Architecture.


The event is free, open to the public, and—with people like the legendary Martin Bergmann and the author George Prochnik as discussants—promises to be quite lively.


LW


[![](media/centself-2-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/11/centself-2-1.jpg)


.


*The following is the introduction to the first screening, given by Jane Kupersmidt, PhD, psychoanalyst, [National Psychological Association for Psychoanalysis (NPAP):](http://www.npap.org)*


“We're about to watch the Hobbesian view of what emerged from the use of Freud's theories by those wishing to gain power and wealth.  With a take-no-prisoners single-mindedness, Curtis shows what can and has been done with the understanding Freud gave us about the dark forces of our inner life. Since the individual human psyche, unaided, may not lend itself to the enterprise of democracy, particularly when enterprise is the main idea, we—those having, or desiring to have, power—need to take full advantage of our knowledge of the unseen, brutish stirrings inherent in human nature. Curtis' film insists that we allow ourselves to be disturbed, and pushes us to acknowledge that, by elaborating the unconscious structures that shape our thoughts and actions, Freud left us in position to discover that some of his most pessimistic thoughts about our species were correct.   Given the keys to deciphering our “democratic” social system and how to work it, we have been adept students.


Tonight's two segments examine the marriage of crucial elements in the history of psychoanalysis and the growing science of public relations as an economic and political force. Inevitably, the marriage is nasty, brutish, but alas, not short.


Early in the film, we hear about the reigning ethos before, and often during, Freud's time: ”You just couldn't tell your bloody feelings…To put yourself into question you'd have to put everything else into question… [even] your society.” Once that questioning happened, however, the purpose of an idea of the self could be examined and, Curtis shows, cheerfully shaped and appropriated by America's eager, expansive embrace of the hidden new world of the self.  The brave new world phase would come later, but leading up to the fifties, aspects of psychoanalysis connected with secrecy and elitism came to the fore. There had, of course, always been conflicts between Freud and his followers; many of them had centered on protecting the image of the movement as well as the theoretical substance of psychoanalysis. By a similar logic, leading psychoanalytic figures were drawn into alliances with conservative social forces and their take on social good. Curtis provides one devastating example after another of how first, the repressed unconscious, and later, tenets of ego psychology, absorbed and manipulated by non-clinicians, encouraged individuals to gravitate toward a simplistic and enthusiastic form of democratic participation under the firm guidance of an elite.  That elite, in turn, relied on and rewarded those most expert at manipulating desires and beliefs—*The Hidden Persuaders* whom Vance Packard could expose, but not limit.  At once chilling and comical, the archival footage Curtis gathers here demonstrates the pathways and methods used to shape a stable citizenry. Those committed to limiting human waywardness, and so improving society, needed to ensure that the means of mass control remained in plump, effective capitalist hands.


Freud's nephew Edward Bernays is the pivotal figure in this transformation.  Not particularly related, as we might now say, he had enormous creative zeal for implementing his and others' belief that “happy consumers make for a more stable society.” And to make consumers happy, you instruct them in the practice of desire—that is, you move the nation from a need culture to a desire culture. Curtis speaks about Freud, not Lacan, but we can hardly escape seeing how, in confirmation of Lacan, desire is fed by observing the desire of the other:  “Irrelevant objects become powerful symbols of how you want to be seen by others.”  You can make use of this understanding to sell Betty Crocker cake mixes, or to stage a coup in Guatemala.


Bernays' rise began two decades before the heyday of American psychoanalysis. During and after the war, many here were part of the effort to deal with returning veterans, but soon psychoanalysts were consulted by more well-placed and visible figures. For a time, analysts in America enjoyed prominence and prosperity, complete with waiting lists for their services. Though Curtis is most interested in the misappropriation of psychological thinking, we do get glimpses of those, like Martin Bergmann, who were committed to knowing the human psyche for its own sake. But it is more to Curtis' point about analysts that even as they were supporting what you needed to fit in with, they were fitting in as never before.


The role of government in mental health also veered away from actual psychoanalytic practice, which aimed at being not only therapeutic, but also enriching for the individual. The films show how quickly the efforts to understand the war trauma seen in returning veterans violated boundaries, and led to experimentation with techniques of “healing” that reinforced good social adjustment. Once again, the individual had value primarily as a conduit to collective control. As we hear clearly stated, psychological manipulation will help create good residents of a democracy, docile, conforming citizens who value adjusting as much as their leaders do. After all, “Democracy couldn't do it alone.”


I suspect that, to an audience comfortable with psychoanalytic perspectives, Curtis' take can be infuriating. Anna Freud ‘s experiment with the Burlingham children, and her influence in America, are presented in a now all too familiar, one-sided fashion—tell the part of the story that most persuasively speaks to your point—that has transformed public discourse in our culture.  Further, Curtis draws on the same techniques that scandalize him (showing Bergmann's consulting room with a misleading narration in its connection to him). Too much that mattered is left aside, so the films seem as striking for what they don't say as for what they do.


We're left with few illusions, however.  The *deus ex machina* that comes in to save naïve Little Red Riding Hood from herself never made it to America.  Worse still, watching these films, and the largely political ones you will see next time, you cannot get away from feeling there's a naïve hubris in claiming to do one's own thinking, or in hoping to possess a mind of one's own, or, most of all, in believing one can own a self—even your own.”


.


*The following is the introduction by [George Prochnik](http://www.nytimes.com/2006/12/24/books/review/Kramer.t.html) to the second screening. Prochnik is the author of the books “Putnam Camp: Sigmund Freud, James Jackson Putnam and the Purposes of American Psychology” (2006), and “In Pursuit of Silence: Listening for Meaning in a World of Noise” (2010):*


How did British and American liberal politics devolve to the point where eight people sipping wine and popping back Cheerios have gained the power to define the policy choices of a national leader? How did our society become so selfish, trivial, materialistic and enslaved to its bottom-most register of desires? What enabled our businesses to gain a dictatorial control over the public imagination? Why did everything get so dark, dystopic and just plain dumb?


Adam Curtis's important film, *The Century of the Self*, supplies an answer to all these questions—and more. “There's A Policeman Inside All Our Heads: He Must Be Destroyed,” and “Eight People Sipping Wine in Kettering” are the two concluding episodes of a sweeping production. Curtis tracks the path by which a broad cultural revulsion against the notion of being possessed by corporate *sprachen* inspired a multifold liberation movement that crumpled into a new, ultimately vapid, ideal of self-empowerment, which, in turn, provided fertile soil for big business interests to sow a hyper-crop of rampant consumer desire. For the final twist, Curtis shows how the techniques developed by corporations to sell products were imported into the political arena. As mass surveys, aimed at determining how best to pander to lifestyle preferences of swing voters, became the strategy-of-choice for the Democratic Party under Clinton, and then—copycat killer-style—for New Labor in the election that brought Tony Blair to power, the higher social values of liberal politics became inexpedient, lost in a wave of redundancies. These two episodes present a devastating portrait of the reduction of the great democratic experiment to radically self-centered *niche* politics. The grand social collective doodles off into cocktail party banter.


There is, undoubtedly, a tremendous amount to learn here. No one who cares about—well—anything worth caring about, could applaud what passes for political process in this country today. We all want to know how we got here. Many of us, whatever historical methodology we espouse, cling to the hope that grasping how we arrived where we are today is theprerequisite to getting somewhere more palatable tomorrow.


Adam Curtis draws a crystalline vector of the steps that led from ‘there' to ‘here'. Grossly telescoped, Curtis's arc to the apocalypse of automaton-frivolousness goes something like this: Sigmund Freud develops a theory of psychology based on the premise that vast, dark, irrational, unconscious forces drive the individual, and that these forces must be restrained if civilization is not to be drawn into the abyss. Inheriting the mantle of her father's movement, Anna Freud adopts an even more restrictive, lockstep structural approach to both the internal politics and practice of psychoanalysis—one that cripples the lives of the more heterodox analysts and stifles many children who come under her sway. Freud's nephew, Edward Bernays, takes up Freud's theories and realizes that the same irrational forces his uncle had identified as destructive, could be tapped by corporations to make consumers see specific products as capable of satisfying their deepest, most primitive desires, while concurrently emancipating them from various socially-imposed shackles. Corporations take up the theories of Edward Bernays and establish focus groups geared to understand how to cater to a radicalized youth movement by creating a new consumer landscape of insidiously individualized products. Finally, in a process wherein Freud's great-grandson, Matthew Freud, plays a seminal role in Britain, politicians adopt the practices the corporate world had itself previously adopted from Freud's nephew.  And yet, Robert Reich eloquently remarks near the end of the film, “Politics and leadership are about engaging the public in a rational discussion and deliberation about what is best and treating people with respect in term of their rational abilities to debate what is best. If it's not that, if it is Freudian, if it is basically a matter of appealing to the same basic unconscious feelings that business appeals to, then, why not let business do it?”


If we follow the dancing cigar from the beginning of the film to the end, the narrative goes: Freud to Freud, to Freud to Freud—to contemporary corporate fascism.


*The Century of the Self* is intelligent, often mesmerizing and, I would argue, ultimately utterly mad. Mad not in its details, which are often sharp and revealing; but in all that's forgotten and which, in its potent absence, having become subject to a kind of massive anti-Freudian slip, enables the dots to be connected in the neat, master-convergence style common to all grand conspiracy theories.


The problem is not, really, that the movie betrays a kind of occult fascination with the Freud family gene pool, one which hints at a primitive curse passing through the different avatars of Freud's name to subvert progressive forms of collective identification across the generations. The problem, as a psychoanalyst might frame it, is that the fabric of the Unsaid is so great here that it pressures and forces distortion in the Said—making the Said everywhere symptomatic rather than etiological. What does it say that in a discussion of movements and cultural upheaval in the 60s and 70s there would be no mention of the role of Eastern philosophy, of drugs, Marx, the Civil Rights Movement, or of any of the indigenous American utopian movements and ideas of both the self and the social collective that long predated Freud?


One quote seems to flash subliminally beneath many scenes of the “There's A Policeman Inside All Our Heads”: “The one thing which we seek with insatiable desire is to forget ourselves, to be surprised out of our propriety, to lose our semipiternal memory and to do something without knowing how or why; in short to draw a new circle. Nothing great was ever achieved without enthusiasm. The way of life is wonderful; it is by abandonment.” Jerry Rubin? Huey Newton? Wilhelm Reich? In fact, no, Ralph Waldo Emerson, writing at a moment when ideals of the self and the social collective were so porous as to be promiscuous—achieving a fraught symbiosis, which in fact forms a recurrent pre-Freudian strain in American thought.


Even the film's completely understandable valorization of FDR is riddled with contradictions. If this was the Century of the Self, how did FDR and the New Deal ever come about? FDR's leadership didn't just birth a collective social self. It was involved with trying to harness the power of a massive American left-leaning political tide, more radical and socialist than the New Deal itself. Part of Roosevelt's task involved, indeed, attempting to co-opt left  wing ideals from the populist parties and tone them down for Washington in a manner suggestive of the struggle faced by the remnant of Republican moderates today to effectively harness Tea Party-type rage. Neither the powerful grassroots left wing of the American political process that helped drive many of FDR's policy decisions, nor the fundamentalist Christian-inspired new right have much, if anything, to do with Freud or Edward Bernays. This problem of the absence of context for FDR's appearance in the film points toward a much larger omission: The film provides no historical perspective through which we might begin thinking about the nature of the self humanity possessed before Freud. The implication in this absence is that, had there been no intervention of Freudian thought, class-based identities would have evolved by effective leadership and a natural process of human development into positive, self-sacrificing universal socialism. Really? How can one even glance at Twentieth Century history without acknowledging that humanity's capacity to form self-abnegating social collectives has at least as much potential to produce ghastly destruction as it does to nurture good?


Of course it is true that no film, however long, can include anything like everything. But, when a film purports to explain so much and yet leaves out everything, save one sinister Vienna-whelped strand, we must wonder what's actually going on.


I would ask that, in watching this film, you do as much as you can to look at the manner in which its polemic is presented: watch the nervous, quick cuts; listen to the disturbing music; track the tics and repetitions; follow the syntactical turns; study its slips and evasions. Pretend, if you will, that *The Century of the Self* is, itself, a patient that you've been called upon to psychoanalyze. Then pull out your personal version of the DSM.


In the end, what may be most interesting about *The Century of the Self* is the particular strain of nostalgia in which it indulges. Using a fast-slash, often frenetic visual style splashed with disparate soundbite interviews, Curtis postulates an essentially monolithic explanation for an enormously polyphonic phenomenon. For this reason, the documentary is more revealing as a harbinger of the Twenty-first Century decidedly post-Freudian cultural pathologies, than it is of the Freudian legacy. What *The Century of The Self* exposes is a whole other register of irrational desires from the ones for which Freud attempted to provide a taxonomy.


The film demonstrates that, along with our deep, dark cravings for sexual delight and territorial domination, we labor under a profound lust for overarching coherent narratives. In our own Century of the Scrambled Self, in which issues of mass distraction, ADD, ADHD, and all the overstimulation-driven kith and kin of these syndromes threaten—quite possibly on a neurological level—to overwhelm our capacity to construct, let alone sustain a cohesive self. Yet, even as they degrade our ability to function as a responsible electorate, how comforting it is to imagine we can still peak behind the curtain to find one smoking wizard— pinpoint A Villain—follow the yellow brick through-thread to our self-annihilatingly dissipated Ozymandias estate. However frightening this film might seem, how much more frightening it would be if—instead of this sophisticated, yet BBC-viewer-level accessible explanation for why we're so screwed—we had to confront an exurban-style sprawl of historical trends and individuals that all converged, overlapped and rear-ended one another to leave us floundering and fearful.


What if the thing we had most to fear right now was precisely the consequences of surrendering to belief in any master-narrative—of foregoing the granular excavation of all those sedimentary and disrupted strata of the self and society alike? Where does this kind of hyper-compressed history—McHegel, iHistory—that provides a playlist of easy targets for our encompassing dread actually leave us? Think, at the end of the film, about how you feel. When everything wrong is tracked through one channel emanating from a single font, everything must be undone or there is no hope. What possibilities for social change remain if this full arc is taken on faith?


In fact, at the end, what we might find ourselves remembering is that it is against just this sort of repressed-material-riddled history that psychoanalysis, in its most expansive, least dogmatically shrill iterations, can be wielded. The best of the Freudian legacy involves a patient, broadly attentive examination of the past that attempts not to shear off the complex root system nurturing the patient's frame of reference. It is an approach, we might add, in which the individual self, the socially constructed self, and the social collective itself are highly interpenetrative. If we're ever to come to a more fruitful understanding of why our current political system is so rotten, I suspect it will happen when we give over the idea of this *Weltanschauung* to which Freud himself was certainly susceptible—when we examine the range of phenomenon in and of themselves and go light on the ways they link together; when we accept that the very idea of periodization intrinsic to identifying a Century of the Self already entails an act of concealment. Might not the film as easily have been called *The Century of the Masses*, *The Century of War*, *The Century of Self-Annihilation*? What sort of history transpires under the rubric of the Century of Anything?


How might the film and its portrayals of myriad fascinating characters have been different? Well, I think of a remark by one Freud who goes unmentioned in this film. Freud's grandson, the painter Lucian, once noted in his very quiet, very precise, exquisitely non-alarmist voice: “I paint people not because of what they are like, not exactly in spite of what they are like, but how they happen to be.”





 ## Comments 
1. [Tweets that mention SHRINKING THE WORLD « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/11/10/shrinking-the-world/?utm_source=pingback&utm_campaign=L2)
11.10.10 / 7pm


[…] This post was mentioned on Twitter by Ozgur Uckan, KpaxsArchi. KpaxsArchi said: SHRINKING THE WORLD: The relationship of psychoanalysis to architecture is most intimately revealed in the uncan… <http://bit.ly/9TlC13> […]
3. [Francisco Vasconcelos](http://csxlab.org)
11.15.10 / 6pm


Hi, This is an excelent theme, my bad I live in Portugal, too faar, and worst, in this country nothing like that happens. After Tokyo I think I need an experience in New York.


When I was in Tokyo I've met Sylvia Lavin, in the UIA 2009 meeting, and curious that she has a work on Richard Neutra that it is:


Form Follows Libido: Architecture and Richard Neutra in a Psychoanalytic Culture, MIT Press, 2005


Very Interesting work.
