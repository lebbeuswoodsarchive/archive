
---
title: RULES OF THE GAME
date: 2009-11-30 00:00:00
---

# RULES OF THE GAME


[![](media/streetcrowd-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/streetcrowd-1a.jpg)


In Europe, Japan, and the United States the populations of cities are mostly stable, which means, growing at a manageable rate. The most serious problems are not the ones confronting cities in Asia, Latin America and Africa caused by population explosions, but are a matter of finding ways to use their  wealth to achieve the promised ends of advanced industrial civilization: universal social justice, liberation from poverty and drudge labor, opportunities to learn and grow and be creative, the chance of all people to be fully realized individuals and not merely statistics, numbers, cogs in the machinery.


Globalization, or what I call a trend toward monological thinking, exists only in terms of economic struggles for the worldwide domination of liberal capitalism. It keeps people submerged in a mass of consumers and works against many of these ends. So does demagogic political leadership, which maintains its power through the creation of fear, particularly of ‘others,' threatening us from the outside. While people in the capitals of global capital enjoy unprecedented benefits of human thought and invention, they live in an emotional atmosphere infected more and more by an almost medieval obsession with myths and magic, with fantasies of good and evil.


For the city in the most stable, developed countries, the greatest challenge today is to create a ground for individual human beings, one that not only supports their legal and economic and environmental rights, but nurtures their sense of autonomy and at the same time of responsibility, in a personal way, toward others. The way to do this is to show people, by example, that their autonomy—their freedom—is dependent on the autonomy of others. That is a fact of the human condition. I cannot be free unless those I live with are also free.     


There are many ways that architects can work to meet the challenges of this condition.


The most obvious is to become aware of and to acknowledge in their practices the crises affecting the city they live in. This means not buying into the propaganda that the causes lie elsewhere, whether it be with foreigners, who ‘hate our way of life,' or with clients, who ‘don't understand architecture.' It means accepting personal and therefore professional responsibility for a particular state of affairs. It does not mean taking on every crisis or problem but addressing the ones an architect feels most capable of responding to creatively and effectively. Not every architect will want or be able to assume responsibility for problems outside the normal client-architect relationship, or for seeing the larger social issues within that relationship. This places a heavier burden of responsibility on those who do.


What is necessary here is a fundamental change in attitude.


The architect who accepts responsibility is not someone who waits by the telephone for a client to call with a commission, but someone who is aware of the significant and complex role the design of space plays in realizing the social contract, and who by law is empowered to exercise authority over the process of design. This architect assumes a role of leadership in the design of space and takes the initiative. Clients have their vested interests, which include a very real social agency bestowed by virtue of the wealth and resources they control. But this does not supplant the agency of architects, which comes not from the control of society's wealth and resources, but from their stewardship of ideas and principles that govern how wealth and resources are to be used through the design of space.


Getting down to particulars, architects need to look critically at the whole question of currently accepted building types.


These types, which are the usual basis of architectural design, correspond to economic categories, in the first place, but also to social stereotypes that organize the conventions governing social interactions. Apartment buildings, single-family houses, office buildings, shopping centers, hotels, memorials, parks, hospitals, theaters, and even streets and public spaces are the labels we put on our projects, and relate almost mechanically to line items in accountants' ledgers: housing, clothing, food entertainment, medical, transportation, leisure. That makes practical sense, and certainly organizes clients' programmatic directives to architects. The obvious problem with typing comes when we realize that the flow of urban living intermingles these categories, both socially and spatially, in ways that cannot, should not, be predictable or entirely controlled. Most people find ways around the rigidity of being office-like at the office, or home-like at home, so there is a lot of flexibility in the typological system that makes for a workable compromise between typing and flow. But typological thinking becomes a serious problem in critical or ambiguous situations, when some form of radical change is underway and the prevailing typologies are insufficient to accommodate them. Examples may range from war and natural disaster to economic collapse and reformation to the emergence of a long-repressed social group that upsets the existing order of beliefs. Types, or stereotypes, break down when confronted with change, because they cannot genuinely, or generously, embrace the new. This includes, ironically enough, types labeled ‘flexible,' because they are commonly based on systems designed to accommodate only predicted changes.


 If I could sketch out for architects a task list addressing the urgent problems of cities in the developed countries, it would certainly include—at the top of the list—the design of spaces serving human complexity and diversity in new ways.


These spaces will not be characterized by ideas of indoor and outdoor, public and private, and a host of other commonly accepted distinctions. Instead, they will be conceived as elements in continually shifting urban fields that maintain a delicate balance between order and anarchy, between knowledge and the unknown. To design such spaces, architects will have to abandon stereotypes of the design process as well as the product of design. Old ideas of the master architect, who holds tight control of the final product will necessarily give way to the *magister ludi*, an orchestrator of the game. The design game, a very serious and consequential one, will involve many people, with diverse interests, all of which must find their place in the urban field; it cannot be played with pre-determined goals and fixed rules. All is in flux. Yet this cannot mean anything goes, quite the contrary.


Operating in the space between fixed order and unbridled chaos—and this is what has not yet happened in practice—the architect of the urban field must work continually to design and revise the rules of the game. As this is accomplished, others can design particular spaces without sacrificing the integrity of guiding principles.  The constructed results will be the outcome of a complex collaboration, which we can only begin to visualize at present. Nevertheless, it is the task of architects who want to accept the challenge of the new demands of cities for spaces of complexity and diversity to begin this visualization process and to experiment in the formulation of new types of design processes and the rules governing them.


LW


[![](media/alone-3a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/alone-3a.jpg)



 ## Comments 
1. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2009/11/30/the-design-game/)
11.30.09 / 7am


**Social comments and analytics for this post…**


This post was mentioned on Twitter by DesignUnderSky: The Design Game | <http://bit.ly/4GMAwE>…
3. [Mitch McEwen](http://www.superfront.org)
11.30.09 / 3pm


Thank you so much for writing this.
5. Chest Rockwell
12.1.09 / 6am


Who would win in a fight between you and Raimund Abraham?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.1.09 / 11pm
	
	
	Chest Rockwell: Win? Fist-fight or debate? Following the ‘last man standing' idea of winning, I have a shot at winning the fist-fight—I'm simply bigger. As for the debate, I think it would be a draw, though Abraham is an awesome opponent—the toughest I can think of, and the best counter-puncher!
	
	
	Anyway, you must be clairvoyant. Raimund Abraham and I are working on a book in which we duke it out, so to speak. Watch for it in a year or so, and judge for yourself.
7. martin
12.1.09 / 7pm


why does the guy in that picture look so sad? maybe its because hes sitting in a featureless concrete hallway instead of engaging in “the game.”


also, a fairly tangential point, japan's population is actually decreasing. or at least its about to start decreasing. dramatically. <http://news.bbc.co.uk/2/hi/7084749.stm>  

i wonder what kind of architectural investigations could be done within an unraveling society..


generally speaking, i find this kind of argument troubling, in that on the one hand you are arguing for a sort of disbursement of architectural action, beginning with the architect, and spreading to the users. but at the same time you are advocating the fact that architects are the stewards of ideas, and that this stewardship (is that a word?) should be respected, especially by architects. i guess my point is that something in there feels contradictory. limit architects to setting up the game, the rules, and see how it plays out. something about the fact that the architect is still making the rules, revised or not, seems to de-legitimize the removal of total control.. 


are you suggesting something more along the lines of cedric price's fun palace?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.1.09 / 10pm
	
	
	Martin: I think the guy is sad because he's deep into the (consumer) game and can only play by spending the money he earned at a job where he has no input but crunching numbers, or…..Well, there must be a game he can play with enjoyment, but this isn't it. Or, maybe he's a thief, or a terrorist, just mingling with the crowd—that's the beauty, and the spookiness, of anonymity, urban-style.
	
	
	I think the Fun Palace is a great reference here. Price (a great gamer and rule-maker) set up the rules of the almost spontaneous in the architecture itself—not a word, or instruction-booklet are needed. Perhaps this is the ideal, though it could take many different forms, not just the technological.
	
	
	What I have in mind is a reformation of the design process itself. It will take a future post for me to elaborate, but for now I can simply say that ‘collaboration' is the key. By this I mean real collaboration, not ‘coordination' between architect, engineers, builders. You might refer to my [A401: BUFFALO ANALOG](https://lebbeuswoods.wordpress.com/2009/05/15/as401-buffalo-analog/) and [ARCHITECTURE SCHOOL 401](https://lebbeuswoods.wordpress.com/2009/05/15/architecture-school-401/) posts to get some idea of what I mean by collaboration. There's more to be said about this, in my own work related to The Fall project in Paris and System Wien in Vienna (see lebbeuswoods.net). All of these, however, are only a tentative beginning toward where I think we should go.
	
	
	AHA! You're referring to the solo guy in the ambiguous architectural space (you call it a hallway). Why do you think he's sad? It seems to me he could be tired, from the existential struggle, just resting before he engages it again. He's alone, yes. Sad, maybe, knowing his—and our—ultimate fate. But that's the basic human condition. He'll endure. On occasion, he'll triumph. He's us.
9. [The Digital Unrevolution](http://www.cloudcollections.net/?p=15)
9.3.10 / 4pm


[…] “Rules of the Game” by Lebbeus […]
11. [Alex Bowles](http://alexbowles.com)
4.2.11 / 7am


Just re-read this essay again. I find myself coming back to it ever few months or so. It's an absolute gem. 


Thank you.
13. [Alex Bowles](http://alexbowles.com)
3.8.12 / 10pm


Here's one example of the autonomy idea developing. <https://alexqgb.wordpress.com/2012/03/08/not-so-sim-cities/>
15. [Alex Bowles](http://www.alexbowles.com)
6.5.12 / 2pm


And is this an example of the kind of space?



> The key thing about Willesden's French Market is that it accentuates and celebrates this concrete space in front of Willesden Green Library Centre, which is at all times a meeting place, though never quite so much as it is on market day. Everybody's just standing around, talking, buying or not buying cheese, as the mood takes them. It's really pleasant. You could almost forget Willesden High Road was ten yards away. This matters. When you're standing in the market you're not going to work, you're not going to school, you're not waiting for a bus. You're not heading for the tube or shopping for necessities. You're not on the high road where all these activities take place. You're just a little bit off it, hanging out, in an open air urban area, which is what these urban high streets have specifically evolved to stop people doing.
> 
> 


<http://www.nybooks.com/blogs/nyrblog/2012/jun/02/north-west-london-blues/>
