
---
title: THE QUESTION OF SPACE
date: 2009-11-19 00:00:00
---

# THE QUESTION OF SPACE


[![](media/lwblog-qsp-1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwblog-qsp-1.jpg)


Space is essentially a mental construct. We imagine space to be there, even if we experience it as a void, an absence we cannot perceive.


Space is always the implication of objects. For an object to exist, we think, it needs some kind of space. So, the first space we can imagine is the space occupied by objects.


**In order to see an object we must be separate from it. A space must exist between us and the object. Therefore, we imagine a space around the object, and also around ourselves, because, at some stage in our mental development, we realize that we, too, are objects. Space is the medium of our relationships with the world and everything in it, but, for all of that, we do not experience it in a palpable, physical sense. We must think space into existence.**


It is worth pausing to consider this assertion. If space is mental and non-material, what does this say about our relationships to the world? And to the idea and reality of architecture?


First, we should consider the probability of the assertion. Isn't space palpable? Isn't it filled with substance, the air we breathe, and move through and feel as a tangible presence? Isn't this the way we know space? No, it is not. When we feel the wind blowing, we do not say “I feel the space moving.” The air and the wind are only inhabitants of the space, like us. Space itself is something else. What is it?


Thinking of movies, we must admit that the spaces in which movies we see are acted out have only minimal physical reality, as projections on a screen. Nevertheless, they have a full spatial presence that we experience directly and also remember. If this were not the case, we would come away from a movie speaking of our experience of a two-dimensional surface we have seen in a dark room illuminated by moving patterns of light, shadow and color. But we do not. We speak as though we had ‘been there.' This ‘being there,' in the scenography of the movie, is a reality we experience only in our minds—still, we were there, because space itself is always only mental. On the other hand, we do not believe that we were part of the narrative, or were the characters acting it out. We identify ourselves with them, and the events of their lives, but we do not consider them real. The very fact that our experience of space is essentially mental and not physical makes the ‘movie space' real. For this reason, we can say that it is the reality of the space that gives the movie's action and the actors credibility, not the other way around. Even movies with the most unbelievable screenplays and the most inept actors can still leave a strong and entirely credible spatial impression, as countless ‘noir' B-movies attest.


Movies were the first ‘virtual' realities. Before them, paintings and other forms of graphic art worked in a similar way. Piranesi's etchings of ‘prisons' stay in our minds much more vividly than any similarly grand space—say, the atrium of an enormous contemporary hotel—that we have actually walked through. Surely part of this vividness is due to the superiority of Piranesi's spatial design, but this would matter little if the etching and the atrium were not in some important way the same in our experience. After the movies comes the computer, in all its manifestations. As we stand on the threshold of a world defined in terms of digitally-generated realities, we need to consider more carefully than ever before the question of space and the nature of its reality.


Consider the example given by Albert Einstein in his popular book *Relativity*. Here, he defines particular space as arising from the simple act of establishing coordinates within general space. So, simply drawing a box with conceptually thin—that is, non-physical—lines is enough to bring a distinct and separate space into being. Let us test his thought. Well, if we think of Austria or Thailand, we take the point. The existence of such spaces is conceptual, because the lines of the box, the ‘borders' of the nations,  drawn between the coordinate points only mentally are physical only on maps. Nevertheless, we regard them as real, even when we traverse the actual landscape they circumscribe. When it comes to space, the mental is as potent as the physical. What is the physical, after all, but sensations impacting the neural nets of our brains? Where do the sensations come from? How do we know that what we see is not an artifice of projections onto the brain? Ultimately, we do not. Space, in the end, is what we think it is.


It is easy to fool the senses, and therefore the mind. Epics of human history are largely written in terms of places that exist only as idea: motherland, fatherland, homeland, nation, country. The gullibility of human beings to be seduced by the reality of that we only think exists is the source of our dreams and fantasies, and also of our inventions—seeing what is not there, as though it were. But that is only half the story. The other half of intelligence is its skepticism. How true are our sensations? Our thoughts? Can we trust them? Is space real, just because we think it is? Are, then, dreams real? And movies? And projects drawn by architects that describe objects that might exist physically but do not?


The question of what is real touches on profound philosophical questions. The most critical of these concerns the limitations of our capabilities to know through our sense-organs, and our abilities to imagine through our cognitive faculties. For now, we must settle for provisional answers, and the most salient of these seems to be that the limits of the real are isomorphic with the limits of what we can conceive.


[![](media/lwblog-qsp-2.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/lwblog-qsp-2.jpg)


LW



 ## Comments 
1. [Stephen](http://skorbichartist.blogspot.com/)
11.19.09 / 4pm


Perception of space is a relative thing as you mentioned. I think that you need to throw the concept of time in there as well. How we move through space effects the space itself. Time enhances the sense of space, allowing us to move within it and thus giving it more reality. Time feeds memory. Memory helps with the perception of space as well in both positive and negative ways.  

In film, time and movement are very important and is what gives the medium such power in relation to still art.  

For me, this is the sublime part of making architecture. Yes, we need to meet programmatic, technical and budgetary requirements but the creation of meaningful space is my ultimate goal.  

We know when we are moved by a space and can sometimes explain why (easier for those of us trained to see it) and sometimes we can't. It is magic when it all comes together.





	1. david
	2.16.10 / 3am
	
	
	I think your consideration of time is important but would like to point out a few things. The duration of looking can effect our perception of the reality of the space. When looking at an image, like Piranesi's Carceri, the time allowed to gaze influences the impact of the space depicted. Time allows our eye to trace around the image analysing the truthfulness of its depiction. Errors in its depiction make it difficult to be understood as a real space. Just as well, a short glimpse of an image or space creates doubt in our mind about its reality because of we lack the ability to verify with movement through the space visually or physically. Therefore, an image or dream can be equally powerful as the experience of architectural space physically.
3. [we could build a large wooden badger… « laurence turner](http://laurenceturner.wordpress.com/2009/11/19/we-could-build-a-large-wooden-badger/)
11.19.09 / 5pm


[…] a large wooden badger… How Google can guess what you are thinking, guardian.co.uk / the question of space, lebbeuswoods.wordpress.com / the OS opportunity, daringfireball.net / Bah, humbug! Postal Service […]
5. Jordan
11.19.09 / 5pm


Space is not something that is entirely physical, definatly maybe. Without experiencing a space in a physical way, does it exist. One thought behind Deja vu is that it occurs because we are not consiously paying attention to our surroundings and when we do consiously pay attention Deja vu occurs. Would this not also be true of the space we see in movies. The feeling of actually being there. Perhaps we have been there, but fail to notice it consciously. Or even notice consciously and have an even stronger feeling of being there. This applies to buildings also. We cannot remember being born, yet we have many experiences that are etched in our mind. By the time we reach adulthood we have experienced one thing or another almost entirely. Perhaps this leads to our favorite spaces being memories that are buried so deep we cannot remember them
7. [Daily Digest for November 19th « blomstereng.org](http://blomstereng.org/?p=73)
11.19.09 / 10pm


[…] THE QUESTION OF SPACE. — 1:53am via Google shared items THE QUESTION OF […]
9. Pedro Esteban Galindo Landeira
11.20.09 / 4am


Please LW, I'm working now with the concept itself of space, could you tell me some contemporary authors who work with it, because here in Cuba I only can find greats books but olders, the most recient is Giedion whit “Arquitectura: Fenomeno de transicion” an espectacular book but it was written in the 60's and I will like to read the contemporary conception. Thanks.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.22.09 / 3pm
	
	
	Pedro: Anthony Vidler wrote a book not long ago called “[Warped Space](http://www.amazon.com/Warped-Space-Architecture-Anxiety-Culture/dp/0262720418/ref=sr_1_1?ie=UTF8&s=books&qid=1258904857&sr=8-1),” which is quite readable and up-to-date conceptually.
11. Christopher Otterbine
11.22.09 / 12pm


Leb do you suppose the vocabulary we have to consider space may be underdeveloped?  

Do you suppose a discussion of quality of space is possible, or would it become a discussion of surface, scale, color, texture, temperature…





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.22.09 / 3pm
	
	
	Chris: No doubt it is underdeveloped, limited for now to dimensional qualities—vast, cramped, continuous, fragmented, and so on. The qualities you mention belong to the objects defining the boundaries of particular spaces or to be found in them. The point I make is that because space is non-physical, its qualities can only belong to the mental. We might speak, then, of “paranoid” space, or “free” space, or even “hierarchical” space, and the like—qualities of mind. However, this is not a very common practice among architects.
13. L\_Son
11.23.09 / 10pm


Leb do you think spatial manipulations thru projections, manipulations of light & dark (lets say virtual manipulation) bear similar significance as the actual/physical demarcations of space? As architects shouldn't we adress the implication of programmable surfaces and other interactive tool availabe to us thru the advancement of technology?
15. [Victor Liu](http://www.n-gon.com)
11.24.09 / 3pm


I take the view that it's all real, all of it, even the crazy paranoid fantasies of your messed-up neighbor. His fantasies are certainly real to him, causing him to miss a whole week's work last week, and if there was a way to transplant those fantasies into your head, you might be catatonic, too. So the question rather to me is, how is the experience of the real shared? For physical objects, the question doesn't need to be asked. But for mediated realities — such as film, Piranesi, the computer-generated — the carrier is, at the physical level, the actual medium holding the work, but at a more crucial level, the carrier is culture, including all the institutional and archival apparatuses associated with culture. 


We're living fast, and hoping someone is around to pick up the pieces. Maybe some don't worry about it (picking up the pieces), but I do. What if the most exciting stuff of our times was lost, and only the static, boring stuff made it into future museums? (Not that being in a museum is the be-all-end-all, but more that our great-grandkids shouldn't think we didn't know how to have fun.)
17. [Hydroelectric Infrastructural Landscapes | On the Manipulation of Time and Nature « dpr-barcelona](http://dprbcn.wordpress.com/2011/11/10/hydroelectric-infrastructure-landscapes/)
11.10.11 / 5pm


[…] imagery and this kind of projects are a good reminder of that. We want to end remembering what Lebbeus Woods wrote: Space is essentially a mental construct. We imagine space to be there, even if we experience it as […]
19. [I think this statement is a basic untruth, though I operate from an orientation of basically believi |](http://marisarmiller.com/?p=721)
6.12.12 / 7pm


[…] Woods | The Question of Space Share this:Share I think this statement is a basic untruth, though I operate from an […]
