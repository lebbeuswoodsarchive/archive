
---
title: THE LIGHT, THE DARK
date: 2009-11-08 00:00:00
---

# THE LIGHT, THE DARK


Once I lived in an apartment on the 13th floor (called, of course, the 14th) of a building overlooking midtown Manhattan. The windows of the small room where I worked faced south and east, so I could watch, every day and night, the passage of light over—and within—the dense landscape of buildings. Over a period of eight years that I lived in this room, drawing, writing, looking from the windows, I knew each day that I was privileged to have the time to think and work, when so many must labor at jobs simply for survival, and also to have the views of an epic landscape that inspired much of what I was doing.


 This inspiration was not to reproduce what I saw, or even its effects, though I must say it was a schooling in the drama of an intricate play between light, geometry, and materials. No, I would say that what I beheld inspired in me a feeling of calm and reflection that was liberating. I had nothing to prove. It was all there already. This city in which most of the buildings would not pass as architecture was complete and whole in its embrace of light and its implications. I had only to celebrate the order and beauty in my own terms, the task of any architect inhabiting particular time, and I would do this unburdened by received notions of what architecture was or should be. Light freed me yet at the same time demanded a level of invention beyond any I had known before. Freedom is like that—it raises the stakes.


 I had long thought that architecture measures light. Light flows everywhere in space but can only be perceived when it is reflected from an object—a mountain, a cloud, a building. A building is a special case because of its regular geometry. A rectangular wall shapes light and gives it a rational form, taking it out of a seeming chaos and making it comprehensible, thus useful. I think of architecture as an instrument—such as a ruler or a telescope—something we use to understand the world and create knowledge. I also believe that architecture is an instrument that enables our actions—such as a hammer, or a violin. Architecture is instrumental, active, and only incidentally symbolic or representational, that is, passively present.


Walls such as those I observed through my high, but not too high, windows, measure the intensity and color of the light they reflect from the universal flow of waves, and by so doing select a part of the universal, reserving it for human use. Light becomes local, particular, accessible, and unique—the same as with individuals. No two reflections of light are ever exactly the same, owing to the complexity of atmospheric, geographical, and the slow but tangible transformations of the reflecting materials of the walls. The light we see one day will be different from what we see the next, either dramatically—it is cloudy, then sunny—or subtly—it is always sunny, but the sun itself has shifted a degree from one day to the next, at any rate enough to make today's sunlit wall different from yesterday's. Though we may not be perceptive enough to gauge such a small difference, intellectually we know it exists, and that is enough to transform our experience. In this way, light itself is a reflection of our evolving consciousness.


 With light, we know, comes darkness. Darkness is not the absence of light. Rather, it is a product of light, an active, opposing presence created by it. Without light we would not think of darkness as we do—it would be all we know, and we would live in a visually monological world. But we do not. We live in a world of contrasts between light and darkness, indeed a dialogical world of infinite gradations of the two. Urban darkness is still suffused with light. The darkest shadows on the brightest day are alive with soft echoes of light, moving from surface to surface. The struggles between light and darkness set the ground-tone of our human condition.


 A city such as the one I observed from a high place many years ago is the ultimate landscape of this condition, and its architecture is the instrument of continual reformation. But one need not have a grand view of the city to understand what I am saying here. Any street, any set of walls, any window opening onto them will do.


![9Boxes9](media/9Boxes9.jpg)


LW



 ## Comments 
1. [josh](http://re-place.org)
11.8.09 / 3pm


simply beautiful LW..
3. Damir
11.9.09 / 4am


I always identified more with limiting light in a space in order to make it feel like it belong to me or I belonged in it. It makes me more aware of the elements the sorround me. I tend to light a candle anytime I can, rather then turn on a light. My shadow on a wall, made me feel as if the space is shared. I wonder if in new York we will ever see the stars reflect on the same curtain walls the sun covers during the day? I wonder if that would change the cities self involvement? I wonder if the city would grow at night if we didn't turn on our lights and reflect ourselves?
5. [Quick Reading « CMU 2nd Year Studio](http://cmuarch2013.wordpress.com/2009/11/11/quick-reading/)
11.11.09 / 7pm


[…] Woods's “The Light, The Dark” Geoff Manaugh's “Editing the Shadow […]
7. [Learning from Ralph « sub.sist](http://kaihoahoawhare.wordpress.com/2012/04/21/learning-from-ralph/)
4.21.12 / 11am


[…] only has real meaning and hope in the way it contrasts with the light that separates.'  Lebbeus Woods makes a similar […]
