
---
title: MOSS IS MORE
date: 2009-11-17 00:00:00
---

# MOSS IS MORE


Complexity is a major theme that runs through Eric Owen Moss' work. Nothing is as simple as we have been led to believe. Nothing can be taken as given or assumed. Entering a building. Walking up a stair. Designing the structural frame for a high-rise building, the public lobby of a theater, or exhibition spaces for art. Through exhaustive iterations of drawings and models, Moss explicates what he finds as an inherent complexity arising from the nuance of human actions. In the process, he challenges us to rethink the nature of architecture. Moss' propositions, built or not, impel us in a particular direction. Architecture is not a confirmation of imposed order and control, but a celebration of our perceptions and our sense of play. Architecture is an ongoing dialogue between contrasting forms and the materials whose properties inspire them. Steel bends, wood supports, cement spreads, glass is molten. Their life together is complex, as is ours. Architecture negotiates the existential gaps, and Moss' work gives us a model of how to do so with grace and exuberance. No irony distances him, or us, from our experience of space and light and texture, color and movement.


The strong sub-theme running through all of Moss' work is invention. If nothing can be accepted as given; if architecture celebrates our unfolding experiences; if architecture, in effect, wakes us up to the complexity of the world, and of ourselves, then the architect must continually invent his designs from the shifting circumstances of each project. It will not do to have stock details plugged into one project after another, let alone overall building forms. Yet it also will not do to create an architecture of one-offs that devolve into mere distractions. Moss understands that something essential must carry over from project to project, without confining him to a style and stereotyping our responses. Astutely, he has understood this to be invention. Each project is an experiment, in the sense of a research that contributes to a body of knowledge—his and ours—that can be tapped in many different ways. His experiments with a wide range of architectural materials loom particularly large in this domain. The consequence is that no two of his projects are the same, yet we can perceive the irrepressibly original Moss in them all.


LW


SAMITAUR, Culver City, CA:


[![](media/moss-23.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/moss-23.jpg)


![Moss-1](media/Moss-1.jpg)


LAWSON WESTEN HOUSE, Brentwood, CA:


[![](media/moss-22.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/moss-22.jpg)


IRS RECORDS, Culver City, CA:


![Moss-10](media/Moss-10.jpg)


![Moss-3](media/Moss-3.jpg)


UMBRELLA, Culver City, CA:


![Moss-2a](media/Moss-2a.jpg)


JEFFERSON TOWERS, Los Angeles, CA:


![Moss-7](media/Moss-7.jpg)


![Moss-6](media/Moss-6.jpg)


MARIINSKY CULTURAL CENTER, MARIINSKY THEATER 2, St, Petersburg, Russia:


![Moss-11](media/Moss-11.jpg)


![Moss-8](media/Moss-8.jpg)


![Moss-9](media/Moss-9.jpg)


GATEWAY ART TOWER, Culver City, CA:


[![](media/moss-201.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/moss-201.jpg)


Structural glass tests:


[![](media/moss-211.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/moss-211.jpg)


![Moss-4](media/Moss-4.jpg)


For much (1500+ pages) more, see *Eric Owen Moss, Construction Manual, 1988-2008*, a new and most thorough publication of his works, available, for now, at [www.ideabooks.nl](http://www.ideabooks.nl/):


[![](media/moss-cov14.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/moss-cov14.jpg)



 ## Comments 
1. [chris teeter](http://www.metamechanics.com)
11.19.09 / 12am


Thanks to this post I know what book to get a client for the holidays.


I appreciate this post about Moss, so don't get me wrong when I say “its about damn time a conceptual edge pushing theoreticial building stararchitect understood practice”.


What I'd like to see happen in academia, and I believe I have now theoretical basis for making this point, a lot less conceptualizing and thinking about thinking outside the box and a lot more real world building technology studio scenarios. Clearly practice is complex enough to put you in a position for necessary invention. Try restoration.


In the book death of architecture, LW and moss were at this conference in viena with other greats discussing the early 90s and lack of architecture with a capital A in the world… One of the only statements I thought worth remembering was made by Moss, something like  

– Architecture is like chocolate, you want it, and when you grab it it melts in your hands…


And then I spend the rest of my day trying to rebuild that,
