
---
title: LIBESKIND’S MACHINES
date: 2009-11-24 00:00:00
---

# LIBESKIND'S MACHINES


By the mid-1980s, the reputation of Daniel Libeskind as a leading avant-garde figure in architecture was rapidly rising. This was based on his work as a teacher—he was director and principal teacher at the Cranbrook Academy School of Architecture from 1978 to 1985, establishing it as one of the most creative schools in the world—and on the publication and exhibition of a number of projects that, on their face, seemed to have little to do with architecture. Notable among these were his Memory Machine, Reading Machine, and Writing Machine.


Elaborately constructed and enigmatic in purpose, Libeskind's machines are striking and sumptuous manifestations of ideas that were, at the time he made them, of obsessive interest to academics, critics and avant-gardists in architecture and out. Principal among these was the idea that architecture must be read, that is, understood, in the same way as a written text.


The chief structural features of written language, grammar and syntax, which organize the ways words are put together into a coherent, meaningful ensemble, clearly relate to architecture. Indeed, so traditional a modernist as Mies van der Rohe was fond of speaking of architecture's ‘grammar' and declaring that an understanding of it was essential to design. However, when it comes to the meaning of word constructions—texts—it was believed, by the 70s and 80s, by leading linguistic and literary theorists, that written texts do not have a fixed meaning established by what its author intended to say, but rather multiple meanings that readers have to interpret for themselves by using various cultural codes and references. Peter Eisenman and the circle of architects and critics gathered around him, applied this to architecture, with the consequence that the ‘meaning' of architecture—symbolically and in terms of human purposes—was in their view not to be found in either the architects' or their clients' intentions. In short, form does not follow any *a priori* function but has autonomous existence that must, in the end, be ‘read' on its own terms. In short, the meaning of architecture is to be found only in architecture itself.


 This is a radical position that has not had an impact on most architects who, like it or not, must follow their clients', if not their own, desires and intentions. However, it is possible to imagine that the idea of architecture's autonomy liberated architects to design ever more idiosyncratically expressive forms having little to do with the client-programmed spaces wrapped by them. What began as a radical concept affecting the very core of architecture, is compromised, we might say reduced, to commercially marketable and client-acceptable styles—a fate much the same as idealistic modernism suffered in its time.


 In any event, the vogue for a linguistic interpretation of architecture has passed, and the avant-garde has moved on, or at least elsewhere. Libeskind's machines, inspired by reading and writing and implicitly interpreting texts, as well as memory (treated as text), would be of little interest today if the machines were only didactic illustrations of theory. But they are much more. As objects of design, they have powerful presence, as well as conveying a refined and highly rigorous aesthetic sensibility. As acts of the disciplined imagination of tectonic possibilities—how many parts might be assembled into a compelling whole—they are highly original, exemplary, and instructive. For example, in the diverse, even contrasting ways the same material, such as wood, can be used expressively in the same construction. Or, in the complexity of joints, from fixed to flexible, enabling the total assemblage. Of course, as hand-crafted constructions (a bit too ‘Renaissance' for comfort, as was Tatlin's Flying Machine), they are at once nostalgic and visionary, the latter if we believe that technology is not the main issue at stake in architecture.


 But the most important legacies of these machines, I believe, are conceptual, and of equal or greater value today as when they were made. Their use of analogy to inform the field of architecture is a potent tool for exploring much-needed new ideas of space and its human purposes than are afforded by the ordinary design process based on history and accepted building typologies. In the past, architects such as Mies found architectural inspiration in works of art (see the post [Art to Architecture](https://lebbeuswoods.wordpress.com/2009/03/05/art-to-architecture/)), while Le Corbusier produced his own paintings and sculptures to work out complex aesthetic problems in his architecture. Libeskind's machines are in this tradition, though the problems are different. More architects today could benefit from such an analogous method, if they set for themselves problems not already solved. This method, like the machines themselves, opens architecture to a wide range of knowledge coming from different fields of thought and work, which is sorely needed in a time such as the present, characterized by increasing diversity in the human situation.


LW


The *MEMORY MACHINE:*


[![](media/l-1.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-1.jpg)


[![](media/l-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-2a.jpg)


[![](media/l-2b.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-2b.jpg)


[![](media/l-2c.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-2c.jpg)


The *READING MACHINE:*


[![](media/l-7.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-7.jpg)


[![](media/l-6.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-6.jpg)


The *WRITING MACHINE:*


[![](media/l-3.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-3.jpg)


[![](media/l-4.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-4.jpg)


[![](media/l-5.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-5.jpg)


[![](media/l-12.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-12.jpg)


Appended here are two of Libeskind's *MICROMEGAS* drawings, made in 1979, before the making of the machines. Looking as though they might have been visual texts produced by the machines (they were actually hand drawn, with Rapidiograph drafting pens), they depict a new and ambiguous spatial world emerging from an older, familiar one of architectural form and representation: 


[![](media/l-8.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-8.jpg)


[![](media/l-9.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-9.jpg)


[![](media/l-10.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-10.jpg)


[![](media/l-11.jpg)](https://lebbeuswoods.files.wordpress.com/2009/11/l-11.jpg)



 ## Comments 
1. Pedro Esteban Galindo Landeira
11.25.09 / 1pm


Please could you explain more about the troubles with Derrida and the deconstructivist architects, and what do you think about the new “style” who become from that thougth.  

Thanks for the book.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.26.09 / 1am
	
	
	Pedro: I was never part of Peter Eisenman's circle, therefore do not know the gossip, let alone the facts, of the Derrida story. Apparently, Derrida was happy to be taken up by an avant-garde architect (Eisenman), but at some later point soured on the idea and backed away. For what actually happened, you'll have to inquire from those who know—Eisenman or maybe Jeffery Kipnis. Derrida, sadly, died a couple of years ago.
	
	
	As a footnote, it was Philip Johnson who coined the term “deconstructivist,” for the 1988 MoMA exhibition he and Eisenman curated. It was meant to play on certain superficial visual similarities between then-contemporary architects the curators imagined to be examples of Derrida's doconstruction theory and the Russian Constructivists of the 1920s. A clever marketing tactic. Nothing more.
	
	
	
	
	
		1. pedro
		12.1.09 / 2pm
		
		
		We sorry too the death of Derrida, a great lost to this occidental world. 
		
		
		LW, I'm desilusionated, I was looking for the catalogue of the famous MOMA exhibition (impossible to find) to see what they say, and if that was only marketing, I feel some how a product of the media.  
		
		Do you know the work of Chrisptopher Alexander, what is the position we must take to his theories?  
		
		Is only marketing everything?  
		
		Who have the true in their hands????????
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		12.1.09 / 11pm
		
		
		Pedro: Disillusioned is an OK place to be. Believe in nothing but your own good judgment. Follow your own strong feelings about ideas, things, people. Don't follow what any ‘authority' tells you, unless you feel it is right.
		
		
		Christopher Alexander is a very smart man who has spent his entire life following ideas he believes in. I respect him for that, even while I disagree with his ideas.
3. [Libeskind's Machines « Fire EXIT](http://fireexit.wordpress.com/2009/11/26/libeskinds-machines/)
11.26.09 / 12am


[…] am on November 26, 2009 | # | 0 Tags: Architecture Libeskind's Machines By the mid-1980s, the reputation of Daniel Libeskind as a leading avant-garde figure in […]
5. [Michael Gibbs](http://www.dasl.me.uk)
11.26.09 / 2pm


Thank you for the engaging and provocative piece.


With in this there is a suggestion to the power of the architect with the given or practiced ability to translate from abstraction into the built form. Is there a limit on the type of building that you can translate to? How does the simple low cost functional home lend itself to such a complex process and rich starting point?


The article implies the disempowerment of the architect as the client's demands are restricting. Rather than being in complete control over the process and thus the result the architect becomes a translator of client or clients' desires – a mere middle man/woman.


An understanding or translation of the machine into a built form is workable for ‘bricks and mortar' but we now face the question of augmentation – we are now infiltrated by our interaction with the digital. Our buildings are becoming the container for technology, feeding our desire for information – this blog for instance. Keep up the good work!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.26.09 / 4pm
	
	
	Michael Gibbs: There are three related questions you raise. I'll try to respond to each.
	
	
	First is the ‘translation from abstraction to built form.' I don't think there is any possible direct translation. The purpose of an analogy is to generate ideas about relationships between things, particularly the relationships of parts to a whole of some kind. The best possible result is that some rules or principles will emerge from making and refining an analogy. The next best outcome is a sensibility, more about ethics that aesthetics. The analogy is not about inventing forms that can be translated into buildings.
	
	
	Second is the ‘disempowerment' of the architect by clients. There is an old bit of wisdom: “No one can make a fool out of you but yourself.” Likewise, no one can disempower an architect but the architect him- or herself. Architects are empowered by what they believe architecture to be. Their task is to make architecture according to that belief. If they fail to do so, they cannot blame their clients.
	
	
	Third is our increasing engagement with digital technology. It's interesting to recall that before the invention of digital computers came electronic analog computers—electrical circuits designed to be analogous to particular problems people wanted to solve. When an electric current was passed through the circuit, with its various flows and resistances, ‘abstract' results were obtained that had to be interpreted in order to be applied to the problem—much the same as the architectural analogs of Libeskind's machines. Such interpretations were difficult and highly inventive acts in themselves, and gave the problem solvers direct involvement with the solutions. Digital computation, mediated by standardized software, gives us solutions without the need for interpretation, that is, in the same form as the desired solution. It is easier, for sure, a more detached process. All digitally designed architecture has this eerie quality of detachment. It is up to each architect to decide whether they believe this is good or not, and act accordingly.
7. martin
11.27.09 / 5am


i find the linguistic interpretation to be interesting, b/c i think in some sense its finding a certain a distorted revival. while i would not claim to be nearly an expert on the subject, a designer who is using scripting, for example, is operating in a similar realm. it is almost as if they are constructing the architecture, without knowing the end-piece, through the extremely elaborate construction of scripting text. while eisenmann may have been operating simply out of analogy, i do still find this relationship interesting. 


this also brings up an idea that vidler talked about in ‘the architectural uncanny,' specifically about how there has been a constant, gradual shift away from metaphors relating architecture and the body. 


i wonder if we are moving from an anthropomorphic metaphor to a techno-pomorphic metaphor. or if there even is a metaphor anymore…
9. pedro
12.2.09 / 2am


That is my first dogma( I can't find any other word): Follow my feelings, even when them make me pass a lot of bad moments.  

LW please where I can find some points opposite to the ideas of Alexander, cause I only read his toughts and who he do against the deconstructivism.  

Thanks a lot.
11. [chris teeter](http://www.metamechanics.com)
12.3.09 / 12am


Analog computers and scripting and langauge…I do scripting and I did an analog computer like calculation once. Glued magnets to plexi glass in a predetermined pattern, heated plexi to liquid like state, magnets pulled plexi together, removed plexi, electro magnetic forces frozen…  

Was scirpting a skyscraper and got frustrated with its perfection (digital precision) went to home depot, bought some pvc pipes, half asleep constructed my best model since sophomore year, 10 years ago…


Lw \_ what you say I could easily construe as contradictory logically based on a system that maximizes potential sets of logic and configurations of language. As analogy the digital to analog is like mathematics to english or mathematics to japanese, its so free and flexible for the thinker nothing is unpredictable, or without a clear historical development…


Pedro, feelings are the only thing that matter in architecture. I always thought what alexander said was so layman common sense I felt “a pattern laguage” was a waste of trees, but those are just my feelings…irrational


Eisenman, in my opinion and if I had a decades time for a dissertation, tried to do digital language for architecture, hence his text blue lines as call for grotesque and abaresque as the all-that's-left-let's-experiment. An experiment tends to be an analog experience until the digital logics are so rewritten to play the experiment forwards and backwards consitently.


Deleuze and guattari and every which way manual delanda interprets them is the philosophy of practice. There isn't this infinite space, a universal playing field on which abstract notions of architecture battle it out theoretically as in academia, its this analog quirky language that forces constant structures to arise and die, with the best beating out the other…when you are done you write a monograph about your experience, just don't tell anyone the means and methods for design can be doctrinated and conveyed in an autonomous world of free abstract ideals on architecture.


Eisenman putting a column in the middle of a clients bedroom is less interesting as a highly intellectual bullshit linguistic move than the fact that it improved the clients sex life.


Digital is absolute freedom – most people just get fat, drive hummers and buy britney spears albums – but practice is pretty much analog until some really really smart guy can formulate what appeared as a one time radical reconstruction into a quite often occuring anamolie under the guise of a language which logically breaks a history down into abstract bits. Wolfram, mandelbrot and chaos theory for example


What we all want, those who do want more than what if offered in freedom, is that moment of reconstruction of logic – but many more want to pin point it so that this moment can be controlled ideally with abstract notions \_ the quest for artificial intelligence in short.


What does a brick want to be? Ask that every other week and you will probably get a different answer…


Rambling on my blackberry..





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.3.09 / 1am
	
	
	chris: Like your rambling. Keep your blackberry. So many ideas here that I need time to absorb and respond. Will do so….
13. [Chris Teeter](http://www.metamechanics.com)
12.5.09 / 2am


LW will do, blackberry's are crack…but there is nothing like being able to read your writing anytime. i can't even imagine what it would of been like 20 years ago waiting for someone to produce some text in print format like yours.


don't mind my harping on Academia, it's not all bullshit, i.e. i won't accuse you of that…some poster mentioned your text is rare and original, i absolutely agree when it comes to architecture. for a free and accessible source of thoughtful inspiration on architecture this may be the only site….otherwise must wait for a book to get publish.
15. Leon
12.5.09 / 8pm


It's really nice to see the creator of these machines and “Micromegas” currently designing malls in Vegas, Bern and Dublin and skyscrapers in Milan, Singapore and Pusan (Korea.) Somehow the avant-grade has become mainstream.
17. Nenad Stjepanovic
12.24.09 / 1pm


I would say that, MICROMEGAS, are precisely opposite of the intellectual or academic trends at the time that evolved around ideas of Ferdinand de Saussure based on the semiology. The drawings bare deliberate markings or signs of physiognomic interpretations which are all personal and preconceived notions in epistemological terms. This does not mean they do not have architectural ‘value' or meaning, but my critique is reflected on ides in discourses of Roland Barthes, and J. Derrida. Perhaps more to do with terms that Noam Chomsky defined in the study of linguistics. Machines seem far more relevant.
19. zari
3.21.10 / 4pm


i am an architecture student, new to all this. i dont even understand decon properly. can anyone explain me the basic aim and idea behind this philosophy?  

how is literature related to architecture? more than that how is its fragmentation of any link?  

i am so confused  

help!
21. [» Reading Machines Micro Meta Mega](http://www.mediadesignprogram.net/micro-meta-mega/2011/06/reading-machines-2/)
6.15.11 / 10pm


[…] Libesbkind's Machines John […]
23. pratik
2.8.12 / 3pm


how will an object become an image (design) for deconstruction?  

can u gimme any examples?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.8.12 / 8pm
	
	
	pratik: No. I know little about deconstruction.
25. [References, Excerpts & Outtakes 2/9/12 | Library Test Kitchen](http://librarylab.law.harvard.edu/librarytestkitchen/?p=169)
2.11.12 / 10pm


[…]   Ramelli, Agostino, 1531-ca. 1600, [Engraving showing a man sitting in front of a waterwheel type machine supporting books.Jeffrey mentioned Daniel Libeskind's reading machine circa 1985, as photographed by Lebbeus Woods… […]
27. [» Amazon, Brasil e um breve panorama do mercado de e-livros Fluxos](http://fluxos.org/amazon-brasil-e-um-breve-panorama-do-mercado-de-e-livros/)
2.27.12 / 4pm


[…] Machine. Espécie de máquina de leitura construída na década de 1980 pelo arquiteto Daniel Libeskind; é uma materialização do conceito de Memory Theatre (publicado em um livro de 87 páginas, em […]
