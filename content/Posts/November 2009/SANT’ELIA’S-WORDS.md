
---
title: SANT’ELIA’S WORDS
date: 2009-11-02 00:00:00
---

# SANT'ELIA'S WORDS


![S'E-man1copy](media/S'E-man1copy.jpg)


The drawings Antonio Sant'Elia included in his August 1914 Futurist Manifesto of Architecture are, perhaps, the most famous and influential of the early 20th century, predating many of the avant-garde designs of architects in Germany, France, Holland, and Russia, made a few years later. They are certainly the first by a European architect to project a vertical city, one composed not only of towers, but also of stacked layers of streets, plazas, and the mechanical movement of cars, trams, and trains. Because he died so young, at the age of twenty-eight, killed in a war that he and the other Futurists celebrated as “the sole hygiene of mankind,” he was never able to carry these ideas beyond the few early perspective views, made in 1913 and 1914. Still, they resonate today, even as they seem part of an earlier, more architecturally innocent time.


The Manifesto itself remains of interest, though it gets little or no attention. This is largely because Sant'Elia's authorship is doubted by some distinguished historians. It is true that Umberto Boccioni wrote an architecture manifesto in which he condemned copying historical styles, just like Sant'Elia. It is obvious that F. T. Marinetti, the poet and founder of Futurism insisted on inserting the term ‘Futurism' wherever possible. But the fact is that Sant'Elia wrote a ‘Messagio' to accompany a 1913 exhibition he organized in Como with two other radical architects—before his contact with the Futurists—that is, in the main, identical with the Futurist Manifesto of Architecture as it appeared in an August, 1914 Futurist exhibition in Milan showing his now-famous suite of visionary drawings. As a result of my research, I am convinced that Sant'Elia is the Manifesto's principal author—the central ideas about architecture are his—and this makes a difference in our understanding of the drawings.


By publishing the drawings with the Manifesto, Sant'Elia himself was inviting comparisons between his words and architectural designs. When we make them, we find both thrilling conjunctions and puzzling contradictions. For example:


“We must invent and rebuild the Futurist city like an immense and tumultuous shipyard, agile, mobile and dynamic in every detail; and the Futurist [apartment] house must be like a gigantic machine. The lifts must no longer be hidden away like tapeworms in the niches of stairwells; the stairwells themselves, rendered useless, must be abolished, and the lifts must scale the lengths of the façades like serpents of steel and glass. The house of concrete, glass and steel, stripped of paintings and sculpture, rich only in the innate beauty of its lines and relief, extraordinarily “ugly” in its mechanical simplicity, higher and wider according to need rather than the specifications of municipal laws. It must soar up on the brink of a tumultuous abyss: the street will no longer lie like a doormat at ground level, but will plunge many stories down into the earth, embracing the metropolitan traffic, and will be linked up for necessary interconnections by metal gangways and swift-moving pavements.”


The drawings illustrate the words vividly and indeed give them credibility—a new world of possibilities is dramatically opened. But then:


 “We have lost our predilection for the monumental, the heavy, the static, and we have enriched our sensibility with a taste for the light, the practical, the ephemeral and the swift.”


Well, the architecture depicted in the drawings does not seem light, ephemeral and swift, but quite monumental and as permanent in its construction as any historical building. Here, we can only speculate that Sant'Elia's ideas, as expressed in words, are in advance of what he was able to conceive in drawing and design. It is as though, in his youthful stage of development, he was unable to unburden his imagination entirely of the weight of architectural history, with its classical modes of order and scale, and to free architecture in the ways he suggests in his closing words:


“From an architecture conceived in this way no formal or linear habit can grow, since the fundamental characteristics of Futurist architecture will be its impermanence and transience. Things will endure less than us. Every generation must build its own city.”


These words still terrify and inspire. With them he evokes a world very much like our own: fast changing, obsolete even as it is created; a world in which architecture is neither reassuring symbol nor comforting shelter, but a swift-moving vehicle of change, an instrument whose very instability creates a need for inventing new ways to be. It is a vision that architects, still burdened by history, have been hesitant to give form to, because it challenges their most secure beliefs.


LW


![S'E-man1a](media/S'E-man1a.jpg)


![S'E-man1b](media/S'E-man1b.jpg)


![S'E-man2copy](media/S'E-man2copy.jpg)


![S'E-man2a](media/S'E-man2a.jpg)


![S'E-man2b](media/S'E-man2b.jpg)


An English translation of the Futurist Manifesto of Architecture:


No architecture has existed since 1700. A moronic mixture of the most various stylistic elements used to mask the skeletons of modern houses is called modern architecture. The new beauty of cement and iron are profaned by the superimposition of motley decorative incrustations that cannot be justified either by constructive necessity or by our (modern) taste, and whose origins are in Egyptian, Indian or Byzantine antiquity and in that idiotic flowering of stupidity and impotence that took the name of neoclassicism.


These architectonic prostitutions are welcomed in Italy, and rapacious alien ineptitude is passed off as talented invention and as extremely up-to-date architecture. Young Italian architects (those who borrow originality from clandestine and compulsive devouring of art journals) flaunt their talents in the new quarters of our towns, where a hilarious salad of little ogival columns, seventeenth-century foliation, Gothic pointed arches, Egyptian pilasters, rococo scrolls, fifteenth-century cherubs, swollen caryatids, take the place of style in all seriousness, and presumptuously put on monumental airs. The kaleidoscopic appearance and reappearance of forms, the multiplying of machinery, the daily increasing needs imposed by the speed of communications, by the concentration of population, by hygiene, and by a hundred other phenomena of modern life, never cause these self-styled renovators of architecture a moment's perplexity or hesitation. They persevere obstinately with the rules of Vitruvius, Vignola and Sansovino plus gleanings from any published scrap of information on German architecture that happens to be at hand. Using these, they continue to stamp the image of imbecility on our cities, our cities which should be the immediate and faithful projection of ourselves.


And so this expressive and synthetic art has become in their hands a vacuous stylistic exercise, a jumble of ill-mixed formulae to disguise a run-of-the-mill traditionalist box of bricks and stone as a modern building. As if we who are accumulators and generators of movement, with all our added mechanical limbs, with all the noise and speed of our life, could live in streets built for the needs of men four, five or six centuries ago.


This is the supreme imbecility of modern architecture, perpetuated by the venal complicity of the academies, the internment camps of the intelligentsia, where the young are forced into the onanistic recopying of classical models instead of throwing their minds open in the search for new frontiers and in the solution of the new and pressing problem: the Futurist house and city. The house and the city that are ours both spiritually and materially, in which our tumult can rage without seeming a grotesque anachronism.


The problem posed in Futurist architecture is not one of linear rearrangement. It is not a question of finding new moldings and frames for windows and doors, of replacing columns, pilasters and corbels with caryatids, flies and frogs. Neither has it anything to do with leaving a façade in bare brick, or plastering it, or facing it with stone or in determining formal differences between the new building and the old one. It is a question of tending the healthy growth of the Futurist house, of constructing it with all the resources of technology and science, satisfying magisterially all the demands of our habits and our spirit, trampling down all that is grotesque and antithetical (tradition, style, aesthetics, proportion), determining new forms, new lines, a new harmony of profiles and volumes, an architecture whose reason for existence can be found solely in the unique conditions of modern life, and in its correspondence with the aesthetic values of our sensibilities. This architecture cannot be subjected to any law of historical continuity. It must be new, just as our state of mind is new.


The art of construction has been able to evolve with time, and to pass from one style to another, while maintaining unaltered the general characteristics of architecture, because in the course of history changes of fashion are frequent and are determined by the alternations of religious conviction and political disposition. But profound changes in the state of the environment are extremely rare, changes that unhinge and renew, such as the discovery of natural laws, the perfecting of mechanical means, the rational and scientific use of material. In modern life the process of stylistic development in architecture has been brought to a halt. Architecture now makes a break with tradition. It must perforce make a fresh start.


Calculations based on the resistance of materials, on the use of reinforced concrete and steel, exclude “architecture” in the classical and traditional sense. Modern constructional materials and scientific concepts are absolutely incompatible with the disciplines of historical styles, and are the principal cause of the grotesque appearance of “fashionable” buildings in which attempts are made to employ the lightness, the superb grace of the steel beam, the delicacy of reinforced concrete, in order to obtain the heavy curve of the arch and the bulkiness of marble.


The utter antithesis between the modern world and the old is determined by all those things that formerly did not exist. Our lives have been enriched by elements the possibility of whose existence the ancients did not even suspect. Men have identified material contingencies, and revealed spiritual attitudes, whose repercussions are felt in a thousand ways. Principal among these is the formation of a new ideal of beauty that is still obscure and embryonic, but whose fascination is already felt even by the masses. We have lost our predilection for the monumental, the heavy, the static, and we have enriched our sensibility with a taste for the light, the practical, the ephemeral and the swift. We no longer feel ourselves to be the men of the cathedrals, the palaces and the podiums. We are the men of the great hotels, the railway stations, the immense streets, colossal ports, covered markets, luminous arcades, straight roads and beneficial demolitions.


We must invent and rebuild the Futurist city like an immense and tumultuous shipyard, agile, mobile and dynamic in every detail; and the Futurist house must be like a gigantic machine. The lifts must no longer be hidden away like tapeworms in the niches of stairwells; the stairwells themselves, rendered useless, must be abolished, and the lifts must scale the lengths of the façades like serpents of steel and glass. The house of concrete, glass and steel, stripped of paintings and sculpture, rich only in the innate beauty of its lines and relief, extraordinarily “ugly” in its mechanical simplicity, higher and wider according to need rather than the specifications of municipal laws. It must soar up on the brink of a tumultuous abyss: the street will no longer lie like a doormat at ground level, but will plunge many stories down into the earth, embracing the metropolitan traffic, and will be linked up for necessary interconnections by metal gangways and swift-moving pavements.


The decorative must be abolished. The problem of Futurist architecture must be resolved, not by continuing to pilfer from Chinese, Persian or Japanese photographs or fooling around with the rules of Vitruvius, but through flashes of genius and through scientific and technical expertise. Everything must be revolutionized. Roofs and underground spaces must be used; the importance of the façade must be diminished; issues of taste must be transplanted from the field of fussy moldings, finicky capitals and flimsy doorways to the broader concerns of bold groupings and masses, and large-scale disposition of planes. Let us make an end of monumental, funereal and commemorative architecture. Let us overturn monuments, pavements, arcades and flights of steps; let us sink the streets and squares; let us raise the level of the city.


I COMBAT AND DESPISE:


All the pseudo-architecture of the avant-garde, Austrian, Hungarian, German and American;


All classical architecture, solemn, hieratic, scenographic, decorative, monumental, pretty and pleasing;


The embalming, reconstruction and reproduction of ancient monuments and palaces;


Perpendicular and horizontal lines, cubical and pyramidal forms that are static, solemn, aggressive and absolutely excluded from our utterly new sensibility;


The use of massive, voluminous, durable, antiquated and costly materials.


AND PROCLAIM:


That Futurist architecture is the architecture of calculation, of audacious temerity and of simplicity; the architecture of reinforced concrete, of steel, glass, cardboard, textile fiber, and of all those substitutes for wood, stone and brick that enable us to obtain maximum elasticity and lightness;


That Futurist architecture is not because of this an arid combination of practicality and usefulness, but remains art, i.e. synthesis and expression;


That oblique and elliptic lines are dynamic, and by their very nature possess an emotive power a thousand times stronger than perpendiculars and horizontals, and that no integral, dynamic architecture can exist that does not include these;


That decoration as an element superimposed on architecture is absurd, and that the decorative value of Futurist architecture depends solely on the use and original arrangement of raw or bare or violently colored materials;


That, just as the ancients drew inspiration for their art from the elements of nature, we—who are materially and spiritually artificial—must find that inspiration in the elements of the utterly new mechanical world we have created, and of which architecture must be the most beautiful expression, the most complete synthesis, the most efficacious integration;


That architecture as the art of arranging forms according to pre-established criteria is finished;


That by the term architecture is meant the endeavor to harmonize the environment with Man with freedom and great audacity, that is to transform the world of things into a direct projection of the world of the spirit;


From an architecture conceived in this way no formal or linear habit can grow, since the fundamental characteristics of Futurist architecture will be its impermanence and transience. Things will endure less than us. Every generation must build its own city. This constant renewal of the architectonic environment will contribute to the victory of Futurism which has already been affirmed by words-in-freedom, plastic dynamism, music without quadrature and the art of noises, and for which we fight without respite against traditionalist cowardice.


**Antonio Sant'Elia, Architect**


The six drawings by Sant'Elia that he chose for the Manifesto:  

![S'E-2b](media/S'E-2b.jpg)


![S'E-5a](media/S'E-5a.jpg)


![S'E-6a](media/S'E-6a.jpg)


![S'E-3a](media/S'E-3a.jpg)


![S'E-4a](media/S'E-4a.jpg)


![S'E-7a](media/S'E-7a.jpg)


Photo from 1916, the year Sant'Elia was killed on the Italian Front (l. to r.) Marinetti, Funi, Sant'Elia:


![S'E-11a](media/S'E-11a.jpg)



 ## Comments 
1. [c.smith](http://pureaesthetic.blogspot.com)
11.2.09 / 5pm


As a young man I came across these drawings and Giacomo Balla's sculptures. These drawings and not only the Manifesto of Architecture but the entire collection of manifestos, have been my greatest influence since then in both life and in expression. Especially Sant'Elia's ‘Stazione Centrale di Milano'.


When Sant'Elia died in 1916 his former coworker and friend, Mario Chiattone, whom shared a similar stylistic meme and was probably the chance for these forms to come to manifest, moved from Milan to Switzerland and left these ideas behind but the reasoning and means to inspire have never died. They retain a constant otherness that, until built will always seem just on the coming horizon of a futuristic idea.


Nearly one year ago science fiction author Bruce Sterling also published this manifesto in [his blog at wired.com](http://www.wired.com/beyond_the_beyond/2008/11/manifesto-of-fu/). He adjoined only one comment at the opening:



> “((If you don't like this, several useless centuries of decadent brick and marble should be built on top of your feeble traditionalist cowardice.))”
> 
> 


In coarse words he describes well the worst of what I have come to believe holds architecture in stasis. Cowardice. In more ways than even just Tradition. 


Academic systems indoctrinating cowardice. Money seems to demand cowardice in the recent past of architecture. Fear. Dead, static, compartmentalized cowardice. It comes forth even in Sant'Elia's words:



> “…this expressive and synthetic art has become in their hands a vacuous stylistic exercise, a jumble of ill-mixed formulae to disguise a run-of-the-mill traditionalist box of bricks and stone as a modern building.
> 
> 


I believe it still continues. I'm fairly certain I'm not alone in that belief and in the desire to challenge current design tropes in the professional and academic fields. In that way the work will challenge us in return I am certain.


In a more expressive way I can only borrow words from the manifesto. I desire to “sing the love of danger” in my life and work.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.2.09 / 7pm
	
	
	c.smith: You are certainly not alone in your belief. I confess that the reason for this post is not dryly historical, but ‘keeping the flame alive.'
3. [c.smith](http://pureaesthetic.tumblr.com)
11.2.09 / 5pm


Also thank you for sharing these images. I have not been able to find such quality scans of these anywhere.
5. laurablosser
11.3.09 / 4am


Just curious, do you know if there was any communication between Sant'Elia & Henri Sauvage while they were alive? Perhaps it is only a formal similarity, but the ziggurat forms are familiar. Sauvage was more of a socialist — practical — unable to achieve the tenor of Sant'Elia's manifesto. 


Also I can't help but feel as though his architecture was manifested in the Centre Pompidou, which is already over 30 years old… what is next?
7. [brad](http://experimentalprocesses.blogspot.com/)
11.3.09 / 4am


The historical arc in this post is incredible and it's a treasure to be able read your perspective – in real time.  

Additionally, It's amazing to wonder what Antonio might have produced had he only had lived longer. For the sake of argument: Had he returned from the war alive, could that experience have engendered a different nuance to his work much less be able to fulfill the lightness of his futurist calling? I wonder if the illustrative precision and crispness, along with the confident massing it depicted, would have carried through to what would have been his post-war days? 


Of course, the former is merely speculation, but I'm also reminded of how much your [LW's] career has responded/reflected/reacted to your life own experiences. i.e: content, v. depiction v. technique v. theory, etc. Thus, it's interesting to wonder about these things. Again, great read – especially considering your position within the strata of these guys, like Antonio, I'm positive your work will referenced a centuries to come.
9. Diego Peñalver
11.3.09 / 2pm


I wonder how this fertile imagination of Saint Elia would have worked in our contemporary world of the internet, with this incredible access and speed to information such as we have today..fastly becoming ingredients of a new polifaceted global view.  

Many of these captions and intuitions expresed in the manifesto are so alive still, it´s just amazing, as are his drawings filled with so much energy.  

Thanks Lebb for the treat.
11. [uberVU - social comments](http://www.ubervu.com/conversations/lebbeuswoods.wordpress.com/2009/11/02/santelias-words/)
11.3.09 / 4pm


**Social comments and analytics for this post…**


This post was mentioned on Twitter by meshula: Gorgeous article on Sant'Elia's architectural futurism, by Lebbeus Woods; it's a 2fer. <http://bit.ly/1SeRxk>…
13. [Stephen](http://skorbichartist.blogspot.com/)
11.3.09 / 6pm


I have always liked these drawings. The ideas and words are a bit overbearing for my taste, but the energy is exciting.  

I did a little research on Sant'Elia and some think that his work was strongly (if not directly) influenced by his peer Filippo Marinetti. There is an abrupt change in Sant'Elias drawings and ideas when this manifesto was published, which is curious, but academic at this point.  

The whole young rebel thing is very appealing and his death at such a young age, in battle no less, gives him a kind of rock star legacy.
15. steven holl
11.4.09 / 10pm


This is an inspiring article,leb, I am in the middle of a competition….reading this article …helped inspire another idea in ongoing work……your blog is rare ,, intelligent and provoking sh
17. [Chris Teeter](http://www.metamechanics.com)
11.7.09 / 7am


(6) leffe's + (2) Guisness


whatever Sant'Elia did or wrote is at best just visual representation of a radical tone beneath. as your buddy Steven Holl states above, in similar fashion to Futurismo, i put Radical Reconstruction on my table to just break out of whatever i'm thinking when doing a competition…


Sant'Elia didn't really contradict himself though, he was moving fast out from under Ottto Wagner (good sketches in the “teNeues” book) and steel construction was fairly new.


the statement you qoute is the very problem we face every day now and from now on. his very basis for monumentality was religious and historical and based on logical meaningful systems of thoughts of the past… 


“We have lost our predilection for the monumental, the heavy, the static, and we have enriched our sensibility with a taste for the light, the practical, the ephemeral and the swift.”


i think you'd agree that this type of monumentality disappeared when glass became a wall and the internet? monumentality has become impossible as peronsal preference of multiple individuals (i-pod internet etc…) overidees a unified agreement on social norms daily. 


it's all transparent and it all erupts or begins in one spot, and how does it begin, immense movement…


for me that's the underlying point of Futurism which rips at everyone who reads it, is it's constant re-structuring of the moment towards another moment of re-structuring, and each drawing is nothing more than a calculus derivative of that moment (a formula projecting the course at that moment).


it's good to bring this type of work up, it makes me wake up after i finish the rest of my Guisness, it moves the theory of architecture to it's edge.


someone mentioned good scans, well not exactly Sant'Elia, rather Hugh Ferris, check out this link


<http://www.columbia.edu/cu/libraries/inside/units/ldpd/avery/html/>





	1. [c.smith](http://pureaesthetic.tumblr.com/)
	11.9.09 / 7am
	
	
	The scans are fantastic. Thanks
19. atlatl2
11.12.09 / 1pm


great post, thanks for that. Finding a book on Sant'Elia's work when I was in high school was one of the most radical things I remember hitting me. There it was, equivalent to Malevich's black square, from the same time. Sixty years later “contemporary” art and architecture were congratulating themselves over their cleverness and I was struck by a young Italian mowed down in a senseless war.
21. [Mark Primack](http://www.markprimack.com)
11.13.09 / 11pm


Atlatl2,  

Sant'Elias was not mown down in a senseless war, not according to Reyner Banham. Rather, he and his Futurists buddies gleefully celebrated war as an instrument of unabashed, unapologetic progress. They enlisted in a bicycle corps, with the heroically ridiculous notion of pedaling nimbly through hostile fire to overwhelm a shocked and awed enemy by dint of their sheer audacity.  

It's been nearly forty years since I was awed and inspired by the sheer audacity of Sant'Elias' drawings. But now that I look at them through a more polished lens – thank you Lebbeus for posting them here- I find it very difficult to connect them to the words of the manifesto. It makes more sense that the drawings were appropriated to the Futurist cause. Studying them now without wonderment, they offer a vision far too rational for the likes of Marinetti, who once ‘modernized' his car by racing it into a ditch. Sans headlights, sans fender, sans ornament. I'm sure he expected his friends to emerge from war similarly streamlined and unencumbered.  

No, Sant'Elias drawings bypassed the middlemen of Futurism on the Italian road to Fascism. His buildings would guarantee that Mussolini's trains ran on time.  

But, as ever, we give too much credit to the conceit of ‘-isms'. In architecture they are most often merely catalysts, excuses or coattails. As hard as it is to admit that a chicken might only be an egg's way of making another egg, so is it painful to admit the possibility that any rhetorical stance (futurism, fascism, minimalism, critical regionalism) might just be a drawing's way of making another drawing.
23. [cclee](http://photographyisdead.tumblr.com)
12.1.09 / 4pm


thanks for keeping the flame alive Lebbeus!


…aniway “Citta Nuova” in contemporary London:


<http://atkinson-and-company.co.uk/folio/visualisation1.php>
25. [Brief Post | Archigram's Space-Comics « dpr-barcelona](http://dprbcn.wordpress.com/2010/01/01/brief-post-archigrams-space-comics/)
1.1.10 / 9pm


[…] much of its expression with thise dim, neurotic, enthusiastic days of the Ring, Der Sturm, and the Futurist Manifesto- the architectural weirdies of the time feeding the infant Modern Movement. Our document is the […]
27. [12\_Antonio Sant'Elia « Arquitectura del Siglo XX: una visita guiada](http://arquitecturasiglo20.wordpress.com/2010/08/26/12_antonio-santelia/)
8.26.10 / 4am


[…] por arquitecturasiglo20 en Agosto 26, 2010 Como lectura opcional, les recomiendo leer el Manifiesto Futurista de Antonio Sant'Elia.  La versión en inglés del manifiesto está al final del post al que […]
29. [blog.lhli.net](http://blog.lhli.net/2010/09/23/sant%e2%80%99elia%e2%80%99s-words-%c2%ab-lebbeus-woods/)
9.23.10 / 9am


[…] <https://lebbeuswoods.wordpress.com/2009/11/02/santelias-words/> Sep 23, 2010 11:25 by lhli. ffffound Inga […]
31. [INSTANT CITIES « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/13/instant-cities-2/)
1.13.11 / 12pm


[…] but a tumultuous, ever-changing process. “Each generation must build its own city,” Antonio Sant'Elia had proclaimed, and Archigram and Constant, in their own very different ways, fulfilled his demand […]
33. [MY CLASSROOM CRUSH — custhom designs](http://www.custhomdesigns.com/2011/07/my-classroom-crush/)
7.19.11 / 2pm


[…] REF IMAGES: https://lebbeuswoods.wordpress.com/2009/11/02/santelias-words/ […]
35. [Stephen Games](http://www.newpremises.org)
10.6.11 / 3pm


Lebbeus, you say that as a result of your research, you are convinced that Sant'Elia is the Manifesto's principal author, but then point out that the architecture depicted in the drawings does not seem to match his remarks about the need for lightness, ephemerality and speed. As far as I can see, you do not demonstrate why you are led to your conclusion about Sant'Elia's authorship, merely stress that you're convinced. What evidence has helped you to make your case?
37. Jerry
11.11.11 / 7pm


Most of the evidence points to the manifesto being heavily edited by Marinetti. The differences between the ‘messagio' (the essay that accompanied the Nouve Tendenze exhibition) and the manifesto are considerable, and it is easy to identofy Marinetti's traits and phrases where he has ‘beefed up' the text.


Personally I'm a huge admirer of his drawings and sketches, but I suspect his death (and the way it was subsequently appropriated by the Futurists) afforded his work a more idealised fate than had he survived. His last drawings were already turning back to a more traditional style, and whilst there is always the possibility that at such a young age he could have gone on to create even more powerful work, I can't help but think it is the very absence of completion that provides them with such inspiring charm.
