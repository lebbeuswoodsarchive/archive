
---
title: WALL GAMES
date: 2009-11-09 00:00:00
---

# WALL GAMES


*On the occasion today of the 20th anniversary of the ‘fall' of the Berlin Wall, I am posting this essay and project written and published in October of 2004:*


Israel, like any sovereign state, has the inherent right to defend itself, but not by any means. If it flouts international law, as it is doing in continuing the construction of the Wall, it becomes a renegade state, outside the law and its protection. Whatever Israel's power to get away with it for the moment, eventually it will come back to haunt Israel, and in ways that may be unforeseen now, when it has the backing of the United States and the tacit threat of its own nuclear weapons. Renegades eventually isolate themselves, as the United States itself is learning from its own unilateral and ‘pre-emptive' actions, and isolation ultimately threatens any nation's survival in a globally interdependent world. The power of even the strongest nations can be wasted by their own misplaced exertions of power, even to the point where weaker, jealous or vengeful nations can, by concerted effort, overcome them. It is in Israel's interests to stop building the Wall, as a sign that it truly wants to be part of the international community of nations and is willing to abide by prevailing international law. In judging its position, Israel should recall that walls built to keep others out ultimately imprison those within. By continuing the construction of the Wall, Israel is creating, in a grotesque historical twist, history's largest Ghetto, separating and isolating its own people from the world upon which their survival, and that of the Jewish State, depends. But this need not be the case. There is still time. The Wall is not yet completed. At this writing, only one-fourth of its planned extent is actually constructed. The rest weighs on the mind as though it already existed, but as yet it does not.


Let me be optimistic and imagine that the ruling powers in Israel come to, or are brought to, their senses and stop construction of the Wall. The space for exchange, however tilted toward Israel already by the existing imbalance of power, will remain open. But what should happen to the already constructed portions of the Wall? Many will want to tear them down, understandably so. They are ugly symbols, for many on the Israeli side and for most on the Palestinian side. Certainly, finished sections should be torn down to make the line more porous, so that Palestinians' direct access to their fields and jobs and villages is assured. But some sections of the Wall could remain. If they were left as free-standing artifacts in a still-divided and yet still-negotiable landscape, they might serve some useful purpose. One of these could be the Wall Game.


The Wall Game uses some sections of the wall as a two-sided playing field. Palestinians control one side, Israelis the other. Each side has a team of builders, architects, artists, and performers, who make a construction on their side of the wall, using it as sole support. In other words, the new constructions cannot rest in any way directly on the ground, but only on the wall. They are cantilever constructions. As such, the cantilever on one side must be balanced by the cantilever on the other side, or else the wall will fall to one side or the other, and the Game will be over.


It is a game only for two opposing sides. One side cannot play it alone, as unbalanced structural forces will bring the wall down very quickly. A one-sided game has no point, that is, no winner. The point of the Wall Game is to win.


There are three levels of winning. The first level is to keep the game going. In this sense, both sides win against the improbability of their continuing to play and against the Wall itself and the complex set of forces it activates. There is no time limit to the game. It ends only when one side wins over the other–the second level of winning–or they both lose. To understand how one side wins over the other, a little more must be known about the nature of the constructions they build.


It may be assumed that each team will build a different type of construction. Different materials, different configuration, different method. This is because the different teams represent different cultures, religions, histories, and aspirations. Even if both teams were to produce similar constructions, they could win on the first level only, because to win on the second level one construction must convert the other.


 Conversion of a construction occurs when its system of order, that is, its basic system of spatial reference is transformed by the system of order of the opposing side. This occurs during predetermined time periods when the construction is left open to infiltration by the opposing construction, through the wall. During these time periods, the opposing team may construct within the construction left open. Not every attempt to convert the opposing construction will be successful. If a construction left open to a conversion attempt is syntactically clear and strong, the attempt will have to be even clearer, stronger, and above all more succinct to reorder the open system of space and form in the time allowed.


The third level of winning is the most difficult to attain. It occurs when both constructions are converted, not according to the system of order of one side or another, but in such a way that an entirely new system of order is created. At this level, both sides win, because they transcend, together, their former states of opposition, and enter a more complex, multivalent state. Resulting from the fusion of the former systems, their constructions achieve a new and hybrid system of order. They meet each other not as contenders but as co-inhabitants of a new spatial condition to which they have both contributed, and which they must both work to not only maintain, but to evolve further.


The Wall Game is clearly designed to engage a new generation of Palestinian and Israeli players, who not only see the old games as destructive and self-defeating, but who want to create new and more productive modes of competition that may lead to new forms of cooperation. Because it is only a game, with neither territory nor lives at stake, winning and losing take on new meanings. As in any game, there is always next time to recoup pride and prestige, and thus the imperative to learn not only from failure, but also from success. The winning team in any single game can be assured that the next time it  plays, the opposing side will have learned not only their techniques, but also devised new tactics of their own. Neither victory nor defeat is ever final.


And there could be spin-offs. Certainly there will be in the development of new, computer-based technologies that oversee the conduct of the Wall Game. From monitoring the stresses in the wall that is the playing field, giving both sides the information they need to continue construction, to judging the success or failure of a conversion attempt, computers—programmed to be as objective as possible—will rely on software that could bring new dimensions to ethical thinking. After all, in this situation, what does ‘objective' mean?


Even the most bitterly opposed adversaries who learn to play together find it difficult to kill each other. It is instructive to consider the impact of the famous Ping-Pong matches between China and the United States in the seventies in ending open hostilities between these countries (such as the Korean War); or the role of the Olympics—and the United States refusal to play in Moscow in 1980–in the rapprochement between the Soviet Union and the United States in the eighties. It can be argued that these games were only a sign that the opposing sides were ready to cooperate more openly and that the games were only symbolic, but that does nothing to diminish the importance of the games as a method of approach. As the cultural historian Johan Huizinga wrote in Homo Ludens, “Play is a uniquely adaptive act, not subordinate to some other adaptive act, but with a special function of its own in human experience.”   This is an insight whose day of usefulness may, once again, be at hand.


Hypothetical Wall Game scenarios:


 ![LWblog-WallX1b](media/LWblog-WallX1b.jpg)


![LWblog-WallX1a](media/LWblog-WallX1a.jpg)


Schematic successful and failed conversions:


![LWblog-WallX2b](media/LWblog-WallX2b.jpg)


Hypothetical scenarios:


![LWblog-WallX2d](media/LWblog-WallX2d.jpg)


LW



 ## Comments 
1. gsidari
11.17.09 / 8pm


equal and opposite forces on opposite sides of a level playing field, so to speak. incredible. tension and compression acting though a plane. could be furthered by a provision for materials found at sites, as relics of their physical realities transformed into components of the game's construction. at a time when the negotiations on both sides could use some fresh input, perhaps those living the daily slog could help in this scenario.
3. [Articles « m:D @ AA DRL UK v11.2](http://matei23.wordpress.com/2009/12/03/articles/)
12.3.09 / 1pm


[…] some interesting thoughts on the Isreali wall from a former teacher of mine, Lebbeus Woods.  I like it not only in the […]
5. [WALLS OF CHANGE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/05/28/walls-of-change/)
5.28.10 / 2pm


[…] to an end; or, in a more hypothetical example, the Israeli ‘security fence,' as the site for the Wall Game bringing Israelis and Palestinians together in constructive play. At yet other times, walls that […]
7. [Walls of change « Archivio Caltari](http://archiviocaltari.wordpress.com/2010/07/05/walls-of-change/)
7.5.10 / 8am


[…] o, in un esempio più ipotetico, alla barriera di sicurezza d'Israele come sito per il Wall Game che unisce israeliani e palestinesi insieme in un gioco costruttivo. Altre volte ancora gli stessi […]
9. [A Wall Is a Wall Is a Wall Is a Wall « Analogia](http://anlogia.wordpress.com/2010/07/19/a-wall-is-a-wall-is-a-wall-is-a-wall/)
7.19.10 / 1am


[…] course, there have many calls to stop construction of the wall, even creative solutions that would inject the situation with bursts of piercing lucidity and […]
11. [MASSIVE CINCY](http://yaifcincyblog.wordpress.com/2010/11/14/214/)
11.14.10 / 9pm


[…] obstacle for the driver and pedestrian alike.  In the spirit of GPS Art and Lebbeus Woods' Wall Games (a bit more architectural), I've drafted up a few games to play while jogging through the […]
13. [On the Border « dpr-barcelona](http://dprbcn.wordpress.com/2011/01/05/on-the-border/)
1.5.11 / 1pm


[…] According to this, is also interesting to mention here Lebbeus Woods‘ project Wall Games, located in Israel [but there could be spin-offs everywhere] and responding to the fact that by […]
15. [Walls of change - Archivio Caltari | Archivio Caltari](http://www.archiviocaltari.it/2010/07/05/walls-of-change/)
11.9.11 / 1pm


[…] o, in un esempio più ipotetico, alla barriera di sicurezza d'Israele come sito per il Wall Game che unisce israeliani e palestinesi insieme in un gioco costruttivo. Altre volte ancora gli stessi […]
