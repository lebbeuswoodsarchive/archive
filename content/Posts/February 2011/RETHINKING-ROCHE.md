
---
title: RETHINKING ROCHE
date: 2011-02-27 00:00:00 
tags: 
    - architecture
    - architecture_criticism
    - urban_public_gardens
---

# RETHINKING ROCHE


[![](media/rd-9a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-9a1.jpg)


*(above) Kevin Roche, c. 1964, working on a model of the Ford Foundation Headquarters building, in the Hamden, Connecticut offices of Kevin Roche John Dinkeloo Associates.*


I was very happy to see the [recent article](http://www.nytimes.com/2011/02/23/arts/design/23roche.html?ref=design) reappraising the early work of Kevin Roche, an architect who—along with his partner John Dinkeloo—I worked for from 1964 to 1968. During those four intensive years I had the good fortune to have worked  primarily on the Ford Foundation Headquarters building in New York, in the ‘production' team—first in design development and construction drawings (in those days all by hand), then on coordination with the engineers and finally on construction coordination and supervision ‘in the field.' I have many stories to tell about what I call “my PhD. in architecture,” but will save them for another occasion. What concerns me here is the critical appraisal of this building, then and now.


Let's start with then. In the late 60s and early 70s of the last century this building—completed in 1968—was savaged by the influential post-Modernist critic and historian Vincent Scully, an avid follower of the then-new theories of Robert Venturi and Denise Scott Brown. They named architects such as Paul Rudolph, John Johansen, and Kevin Roche as villains, denigrating them as proponents of Modernism and as enemies of a new, supposedly populist architecture practiced by Venturi, of course, and his legions of followers. The Ford Foundation Building—arguably Kevin Roche's masterpiece—was labeled by Scully as “elitist” and “fascist.”


This building, which has worn well—in more ways than one—over the past forty years, made a superficially easy target for critics with populist theories to sell. Monumental in feel, although a mere thirteen stories high, it presents a one-story façade on Forty-second Street—very “fascistic.” Looking beyond the clear glass surface, however, we see the thirteen-story, landscaped Garden that is a visual amenity to the street and sidewalk, a lush interior landscape that was (in those pre-terrorism days of 1968) entirely open to the public. People could enter, walk through or sit for a while, enjoying a breather—literally—from the traffic, dust and noise of the bustling city. It was a genuinely populist piece of urbanism, much more so than the pandering, tongue-in-cheek kitsch of then more critically celebrated works such as Charles Moore's Piazza d'Italia in New Orleans. As a footnote, I should add that the Ford Foundation Garden was the first construction anywhere of a large-scale public atrium, one that inspired John Portman's more commercial Hyatt Hotel atria and many others of the decades to come.


Not only that, the critics intoned, but we had to understand that this was the headquarters of the Ford Foundation, an “elitist” institution if ever there was one, flush with its billions in Ford stocks. But, looking past the surface again, we learn that this foundation was dedicated to promoting better education throughout the world. Building schools, financing them and their programs in terms of local needs, often in poor and developing countries. No expensive art. No operas. No exclusive private schools. At the end of each year, the foundation was required by law to spend all their earnings, and this foundation's self-appointed mission was public education. Elitist indeed.


What the critics really hated, but didn't have the honesty to say, was the building's style. It didn't conform to the fashionable collage style of the day, with its ironic historical allusions, its vaguely surreal pastiche of contrasting parts. It was too old-fashioned, too Modernist. Actually, what Kevin Roche had created in this building was a new, humanized modernism, richer in materials and form, and infused with innovations—such as the Garden—and included the building's interior climate control and the efficient integration of mechanical, structural, and architectural systems. It was a building for a rich institution, no doubt, but many of its features had the potential for application anywhere. In truth, the critics resented its brusque elegance and beauty at the very moment they wanted to celebrate post-Modernism's self-conscious ugliness. A glance back at the most praised works of the 70s and 80s, will, I believe, confirm this assessment.


There is much more to be said about the Ford Foundation Headquarters Building that I hope will emerge from the present reappraisal of this and other early works of Kevin Roche and John Dinkeloo. In closing, I'll simply add a note about their Oakland Museum project, designed and built at about the same time as the Ford Foundation building. With its galleries slid beneath, and opening onto, gently cascading landscaped terraces, Roche's idea of the public garden in the city was taken fully into the open. Here is humanistic modernism at its most vibrant, a typology that was also proto-Green in its concern for bringing nature into the city's relentless artificiality.


LW


*(below) The Ford Foundation Headquarters building, c. 1968:*


[![](media/rd-4a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-4a1.jpg)


*The Ford Foundation building today:*


[![](media/rd-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-5a.jpg)


*The ‘bridge' floors, 11 and 12, over the glass walls of the Garden:*


[![](media/rd-6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-6a1.jpg)


*The upper part of the Garden:*


[![](media/rd-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-1a.jpg)


*The lower part of the Garden, where the public can walk, sit, and enjoy:*


[![](media/rd-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-7a.jpg)


*The terraced galleries of the Oakland Museum of Art, today:*


[![](media/rd-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rd-8a.jpg)


#architecture #architecture_criticism #urban_public_gardens
 ## Comments 
1. Pedro Esteban
2.27.11 / 10am


This is what architecture should be, this is what architects don't want to build…………….the paragraff about the Ford Foundation's ”objectives” should be the objectives of architecture…………….. too modernist my comment but, as modernist, I denied the waste of time of some postmodernists


As LeCourbusier denies eclecticism I refuse post-modernism, yes I'm a stupid, and arrogant who speak without any comprehension of history, thanks for that compliment…………….
3. Pedro Esteban
2.27.11 / 11am


<http://www.krjda.com/project/air-force-museum/?cat=2>
5. Michael
2.27.11 / 3pm


LW, I follow your blog but do not often write to these things, I have none the less been in concurence with a great deal. I teach at Pratt having recently lived in LA and was a student at KY when the Cooper folks were there in the 70's and 80's. Tony Rocc. an old and close friend speaks Highly with your regard. Being a painter as well I have much enjoyed the posts of your own paintings as well as the most recent. I have been so moved by the events of the time, that I changed a schedualed lecture on Organicism to the larger question of Humanizing Moderism…really from the 30's on… and so much enjoyed your reassesment of Kevinn Roche, I avidly vist buildings with my partner and on our return east we have visited many treasures by Roche in CT. It is the nuances of things in the world, none the less, and a question of conciousness, that causes us to seek out these buildings,out of genuine curiosity, often suprised by things discovered. I so much enjoyed you Post on t. Mayne, as I know him from LA, and most recently your Light Space in the new S. Holl project. Thanks for the blog, I am sure it is an intense effort, bringing somthing of the personal, ethical, poetic and the critical with such frequency.  

MC





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.27.11 / 5pm
	
	
	Michael: your encouragement gives me strength to continue. Yes, it is intense.
7. DHL
4.10.11 / 8pm


The suite of pavilions that Roche & Dinkeloo created for the arts buildings at Wesleyan University are among the most distinctive examples that illuminate the early and most authentic impact of their work in the forum of American architecture.  

There is raw, “crudo” quality of the material,  

and a series of exquisite and intimate proportions within the ambulatory to and within the  

“templos” volumes that each stand alone,  

in a sort of campus dale of archaic austerity. 


I have imagined that Roche's ability to capture the mystery of a rustic ancient precinct with totally contemporary construction of great structural expression, was inspired by their time at Yale with Rudolph and Sculley- and Kahn. These early sequestered temples for studio art, music, et al, comprise an intimate campus- within- a -campus, with a sense of remove- that develop the characteristics that Roche unbelievably , but successfully, sustained and re- invented within the  

larger project of the Ford Foundation complex-  

an internal to the interiority of the city.
9. [Welcome, « Deserae Foster](http://deseraefoster.wordpress.com/2012/01/23/hello-world/)
1.25.12 / 5am


[…] Art Museum Oakland, California Credit: photo borrowed from this blog. Share this:TwitterFacebookLike this:LikeBe the first to like this post. Comments RSS […]
