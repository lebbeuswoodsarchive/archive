
---
title: SLUMS  The barrios of Bogota
date: 2011-02-08 00:00:00
---

# SLUMS: The barrios of Bogota


**By HARRY MURZYN and DAVID VARON**


Located on a high plateau surrounded by the Andes Mountains, Bogota is a sprawling urban center, made up of numerous barrios (neighborhoods). It grows and changes like any other major Latin American city. Due to population growth from rural migration, the periphery of the city is composed of a continuously changing informal spatial fabric. Whereas the center of the city is located on the flatter portion of the plateau and its construction mandated by formally ordained city codes, the makeshift periphery is pushed to the mountainous regions surrounding the center. Its construction is based solely on need. The locals build what they want, when they want. The result is an extremely organic, interconnected conglomeration of houses, passages, and necessary utilities.


This type of spontaneous architectural organization requires an equally spontaneous and winding system of movement and access. Due to the fact that these are located on mountainous landscapes, roads can only go so far in providing this access. Major portions of the barrios are accessible only by means of a network of interconnected staircases. These stairs act like arteries or a web, which spread though the entire barrio. While these stairs may appear to be knot-like in their winding, interconnecting nature, the stair system functions as a whole spatial order.


.


IMAGE1: A section of the barrios in Bogota:


[![](media/bogota_11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_11.jpg)


IMAGE2: The rural/poorer population (in red) inhabits the mountainous periphery of the city:


[![](media/bogota_2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_2.jpg)


IMAGE3: Plan of Barrio Juan XXIII:


[![](media/bogota_3a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/bogota_3a.jpg)


IMAGE4: Roads provide limited access to the denser areas of the neighborhoods:


[![](media/bogota_4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_4.jpg)


IMAGE5: Staircases provide means of circulation into and through these areas. Once the stair is built, then there is an incentive for buildings to cluster around them:


[![](media/bogota_5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_5.jpg)


IMAGE6: An older, solidly constructed stairway:


[![](media/bogota_6a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/bogota_6a.jpg)


IMAGES7,8: The stairs and the buildings are in a reciprocal relationship. One aspect of their ‘spatial knot' formation is how these continue to relate to each other in an ever-increasing density:


[![](media/bogota_7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_7.jpg)


[![](media/bogota_8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_8.jpg)


IMAGE9: Potential earlier scheme (of the previous photograph). The staircase is built to provide access to several buildings on a sloped landscape.


[![](media/bogota_9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_9.jpg)


IMAGE10: Expansion and addition of buildings must respect the stair as a path of circulation. This leads to odd, disjointed and potentially precarious and unstable building types:


[![](media/bogota_10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_10.jpg)


IMAGE11: As the area becomes more densely populated, the stair expands to become a network. The relationship between stair and building is ever changing. As new buildings are built and others disappear, portions of the stair become obsolete. The stair then becomes a spatial labyrinth of sorts. Particular lines along its path are only understood by those who inhabit its spaces on a day to day basis. Those unfamiliar with its paths would have no means of navigation through it.


[![](media/bogota_111.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/bogota_111.jpg)


**October 2010,** **The Cooper Union**



 ## Comments 
1. [workspace » Staircase, Circulation, Architecture: Barrios in Bogota](http://www.johndan.com/workspace/?p=2204)
2.9.11 / 3am


[…] and … I was going to say urban planning, but it's less planning and more flaneur: The Barrios of Bogota As the area becomes more densely populated, the stair expands to become a network. The relationship […]
3. [Tweets that mention THE BARRIOS OF BOGOTA « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2011/02/08/the-barrios-of-bogota/?utm_source=pingback&utm_campaign=L2)
2.9.11 / 6pm


[…] This post was mentioned on Twitter by Christopher, shiftN. shiftN said: Slums in mountainous regions give rise to riveting informal architecture: <http://bit.ly/ifYD3s> […]
5. Jon
2.15.11 / 8pm


I lived in Bogota and had the opportunity to walk through some of these areas that are intertwined with staircases and ally ways on the mountainsides above carrera 7…interesting stuff. I enjoyed my walks through those areas.
7. [Are we nearly there yet? » Blog Archive » Spontaneous path-making](http://entretags.de/blog/spontaneous-path-making/)
2.24.11 / 11am


[…] paths can be as official as streets and walkways. Harry Murry and David Varon report about barrios of bogota in Lebbeus Wood blog. In the barrios houses are usually accessed through stairs, “these […]
9. Peter Hendry
6.15.11 / 8pm


Scuse me all you desktop experts but these staircases and all the buildings around them are built as a means of survival. They are what they are because that is how they have to be. They are built by people who have to build them because they have no other choice.
11. [Comment l'amérique latine a changé mon regard sur la pauvreté](http://www.readmeimfamous.com/2011/08/amerique-latine-change-regard-pauvrete/)
8.11.11 / 5pm


[…] Crédit photo: lebbeuswoods […]
