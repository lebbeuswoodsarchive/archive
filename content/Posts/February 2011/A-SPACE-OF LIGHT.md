
---
title: A SPACE OF LIGHT
date: 2011-02-15 00:00:00 
tags: 
    - Christoph_a__Kumpusch
---

# A SPACE OF LIGHT


[![](media/ext-3-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ext-3-11.jpg)


*(above) The Light Pavilion by Lebbeus Woods in collaboration with Christoph a. Kumpusch, in the Raffles City complex in Chengdu, China, by Steven Holl Architects.*


The Light Pavilion is designed to be an experimental space, that is, one that gives us the opportunity to experience a type of space we haven't experienced before. Whether it will be a pleasant or unpleasant experience; exciting or dull; uplifting or merely frightening; inspiring or depressing; worthwhile or a waste of time, is not determined in advance by the fulfillment of our familiar expectations, because we can have none, never having encountered such a space before. We shall simply have to go into the space and pass through it, perhaps more than once. That is the most crucial aspect of its experimental nature, and we—its transient inhabitants—are experimentalists in full partnership with the space's designers. Each of our experiences will be unique, personal.


Set within a more known three-dimensional geometry and framed by it, the Light Pavilion exerts its differences. Most apparently, the elements defining it do not follow the known, rectilinear geometry of its architectural setting. The columns supporting stairs and viewing platforms obey a geometry defined by a dynamic of movement. Their deviation from the rectilinear grid releases its spaces from static stability and sets them in motion, encouraging visitors to explore.


The structural columns articulating the Pavilion's interior spaces are illuminated from within and in the twilight and night hours visibly glow, creating a luminous space into which the solid architectural elements appear to merge. This quality is amplified by the mirrored surfaces enclosing the Pavilion, which visually extend its spaces infinitely. We might speculate that this new type of space stands somewhere between traditional architecture and the virtual environments of cyberspace, a domain we increasingly occupy in our homes and workplaces, but in the Light Pavilion with more emphasis on the physical than the mental or the virtual.


From distances across the city, the Pavilion is a beacon of light for the Raffles City complex. From within the buildings, and especially from the large public plaza between them, the glowing structure radiates subtly changing color symbolizing different holidays and times of day, month and year.


The space has been designed to expand the scope and depth of our experiences. That is its sole purpose, its only function. If one needed to give a reason to skeptics for creating such experimental spaces in the context of this large urban development project, it would be this: our rapidly changing world constantly confronts us with new challenges to our abilities to understand and to act, encouraging us to encounter new dimensions of experience.


**Lebbeus Woods**


**Christoph a. Kumpusch**


.


*(below) Development of The Light Pavilion's design:*


[![](media/cdusk0-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/cdusk0-2.jpg)


[![](media/cdusk1day.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/cdusk1day.jpg)


[![](media/ltpav-modst-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ltpav-modst-12.jpg)


[![](media/4h-154.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-154.jpg)


[![](media/ltpav-ddall-3a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-3a1.jpg)


[![](media/ltpav-ddall-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-5a.jpg)


[![](media/ltpav-ddall-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-6a.jpg)


[![](media/ltpav-ddall-10a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-10a1.jpg)


[![](media/ltpav-mockup-131.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ltpav-mockup-131.jpg)


[![](media/ltpav-ddall-11b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-11b.jpg)


[![](media/ltpav-ddall-12a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/ltpav-ddall-12a.jpg)


*Recent construction photograph:*


*[![](media/rc-2-17-111.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/rc-2-17-111.jpg)*


*Light and color studies:*


[![](media/ext-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ext-1-2.jpg)


[![](media/int-5-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/int-5-11.jpg)


[![](media/ext-7-0.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ext-7-0.jpg)


**LW and CaK**


#Christoph_a__Kumpusch
 ## Comments 
1. [differance](http://differencedifferance.wordpress.com)
2.15.11 / 9am


It's luminescence make appear less like a moment in the integrity of the structure and more like a friendly light installation. I feel the rawness of the steel needs to be exploited without the seaming effects; the sketches of the installation materially blending into or aggregating from its gridded context seem much more appropriate.


-Shane





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.15.11 / 1pm
	
	
	differance/Shane: Thanks for your thoughts. I confess, though, that I not sure what you mean by “a moment in the integrity of the structure,” or “the installation blending into or aggregating from its gridded context.” Could you clarify a bit?  
	
	As for “a friendly light installation,” I would say that it does aim to be friendly, inviting people in, but also challenging to their assumptions about how space is made and what it's supposed to do. The computer renderings (absolutely essential to rendering the light or, as you poetically call it, the luminescence) tend to neutralize the images. We'll have to wait and see what the built space is like, before we can judge the nature of its friendliness. I'm still hoping for an edginess that adds another dimension to its largely conventional experiental context.
	
	
	
	
	
		1. [differance](http://differencedifferance.wordpress.com)
		2.16.11 / 3am
		
		
		To clarify, it seems there is a discrepancy between your sketches and your renderings.  
		
		The sketches and models seem more sublime, in that your installation appears as a collapse in the structure of the facade. In this way, they remind me of your installation, “The Fall,” and have great potential because of the resemblance.  
		
		The use of light and materiality in the renderings of the light pavilion, on the other hand, seem less like a moment in the structure and more like a collection of objects in a field (this is especially true since the light pavilion is blue while the structure is white). I would say that a whiter and dimmer light might allow the light pavilion to appear more immersed into the surrounding facade. The edginess you're looking for would theoretically become more apparent in subtlety (the forms in the construction drawings are brilliant, so I only refer to the lights being more subtle).  
		
		Secondly, the renderings are made with such a high caliber that it seems appropriate to judge them for their desired affect. Whenever renderings are photorealistic, they are nearly as valuable as photos.
3. [Stephen](http://skorbichartist.blogspot.com/)
2.15.11 / 4pm


I love this. I love that one can interact with the light bars via stairs and platforms. It will be a meaningful, provocative place for both the building and the urban context.  

We were just talking about the use of light in architecture at our office the other day.  

Great stuff!
5. Pedro Esteban
2.15.11 / 11pm


I like spaces without light, or with a minimum light source.  

Interesting the ”context”. The negation of the context itself, why make a space with that characteristics in that kind of structures? Is like I have done all this and now you should see how you need to use it….. all the building should be like that small space
7. Anthony Titus
2.16.11 / 6am


This is a remarkable work created by two remarkable thinkers; I believe Lebbeus W. and Christoph K. have managed to share something quite special with us. I feel deeply privileged to have the opportunity to get a glimpse into the process of thought, as it appears in the spirited sketches, working drawings, fabrication photos and montages of the project. One of the things that strikes me as being most significant in this work is the way in which the pavilion presents itself as a luminous vital organ; akin to an urban brain or heart. The skeletal nature of the site and radiant quality of the pavilion are in a wonderfully dynamic relationship that manages to eclipse the purely quantitative and moves into a state of the sublime. 


At the risk of bending our beautiful language of Spanish, I say Viva La Luz!


AT
9. [seier+seier](http://www.flickr.com/photos/seier)
2.16.11 / 10am


I love the idea of building an invitation, which is how I see your work here. the change of scale and the use of light certainly suggests it. and the enormity of the complex needs it.


I wish I understood its chinese context better, though.


will it be different from your drawings? of course, how could it not be. I can't help thinking of piranesi who left us just one little church. do better and build more, with all the compromises and frustration it entails.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.16.11 / 2pm
	
	
	seier+seier: I appreciate your encouragement. I would build more if I attracted the kind of client who would be willing to truly experiment. So far, only a handful of exceptional people have commissioned installations: historian Jos Bosman (Hermitage, Eindhoven and Rotterdam, 1999 and 2006); historian Thomas Medicus (Civilization, Berlin, 1999); theorist Anthony Vidler (The Storm, New York, 2001); philosopher Paul Virilio (The Fall, Paris, 2002); art impresario Peter Noever (System Wien, Vienna, 2005);and architect Steven Holl (The Light Pavilion, Chengdu, 2008). All intellectuals, all adventurous. I've been lucky to attract such people, who were willing to take a chance on my work.
11. [lebbeuswoods](http://www.lebbeuswoods.net)
2.16.11 / 3pm


differance: Your clarification helps. Yes, the light and its interplay with the geometry and forms needs a lot of study. That the crucial issue in this stage of the development of the ideas.
13. [Oliver](http://baufunk.blogspot.com)
2.16.11 / 7pm


My interest is in the fact as an inhabitating experimentalist, the chances of me visiting it in person are slim. Thus my inhabitation is currently in drawings and photography. To me the crucial component of architecture is that it is real, concrete. Architectural drawings and renderings are not architecture; they suggest architecture. I quite like what I see, and yes, it is a space I haven't seen before, although I can think of many spaces I have seen that resembles it. Is my experience here valid or useful? When does my observation end and participation begin?


With that said best of luck!
15. [chris teeter](http://www.metamechanics.com)
2.16.11 / 11pm


Oliver, I think I will qoute you from now on – drawings and renderings are not architecture; they suggest architecture.


So with that said and so few commissions so far as noted above, I'm wondering Lebbeus if you have made adjustments in the materials and detailing to better evoke the drawings and renderings. 


Did you spec Rusting? Your drawings always feel to me like the metal has been rusting for a while.


How do you detail pencil smudge on trace?


From this blog I gather you and Holl are friends and I am sure you have had ample exposure to this Raffles Complex project. What I see from above images is that you guys are essentially creating the calculus mathematical equivalent (dx/dy) of the entire project by Steven Holl. In other words, the full process of the Holl project is a parabolic or similar line on graph paper, and this piece is the tangent vector at the turns. 


Now if math isn't your thing, one last try at this analogy – a derivative or what I see you guys are doing is not a still image snapshot of the architecture and its process, but rather it is a film of a moving force within the movie of reality. You pause the movie and yet the action hero is still moving, informing the movies sequence in play.


This was also my reading of Diane Lewis post on yor blog regarding the plan to architecture relationship.


Can't wait to see the final piece.


Good work
17. Pedro Esteban
2.17.11 / 12am


If that space causes a similar situation like this(all this differents comments) on the persons who inhabit or use the complex, we will be, my friends, on a next dimension of architecture…..


One of the ways to prove my work is analizing if it produces a diversity of opinions, and the acceptance of it, so I think that will be a great space, but when I will go there I will turn off the light………
19. Aida Miron
2.17.11 / 2am


The first time I saw images of the Light Pavilion, I immediately thought of your Berlin Free Zone project of the early 1990's,where a “free zone” or “free space” invaded and re-formed an open gridded structure, in what I interpreted a radical re-habitation of transforming socio-political spaces. The possibility of new types of habitations after the fall of the wall, during transitions and intersticial moments between political structures, was extremely alluring. As a student I carried this diagram with me, (it was the first architecture book I ever purchased). Abstractly it became a heterogeneous free space within a grid. The confrontation of a modular-regulated-modern space with this ambiguous space without walls (of a different scale) made me not only question historical predisposition towards Corbusian modernism  

but also the politics of a city and power structures (thinking of the rights of citizens/non-citizens) for they immediately brought to mind the open rooms and spaces of a few of the remaining communal and punk squats I had visited in Berlin in the late 90's. I never imagined I would confront this diagram with the regulated gridded space of capitalism, of mega-structures in overdeveloped cities of regulated densities, but research, travel and the attraction of google maps always directed me to the heterogenous spaces of favelas, informal settlements and villages as pockets in centers of over-developed mega blocks of growing cities.  

I see in the drawings of the Light Pavilion a similar diagram, and wonder how this free zone could confront or insert a different type of space in this regulated development. Can this free-light zone detach itself from the consuming homogeneous power of capitalism? From the mega-urban projects of so called hybrid space? I can imagine passing through this light structures at dusk, or at dawn, and experiencing the city within this new program, whether it becomes a spectacle to further increase the value of this new real estate, or a fleeting moment which brings new thoughts to the the transient inhabitant is to be seen, but the potential to confront from within, is still highly alluring.
21. RUI
2.17.11 / 11am


Everyone seems to speak didactically to a Master at his craft. Your ideas are spoken in a kind of arrogance, as if Lebbeus was immature in the creation of his experimental space.


That being said, renderings and pictures are a type of construction in a type of space- not Architecture . The BUILT form when completed will be only a construction- not yet Architecture.  

It is ONLY in experience that we determine if Architecture resides in our constructions- paper or concrete.


Kudos Mr Woods.
23. [Oliver](http://baufunk.blogspot.com)
2.17.11 / 12pm


I personally don't see a difference between construction and architecture. Fireworks can be architecture to me, a construction (explosion) in space (night sky). And I'm sure Lebbeus understands that it is part of young students' temper to be naively questioning that which they don't fully understand. Otherwise the world would be awfully silent.


Rereading the project, my curiousity still centers on the blurring of virtual and concrete experiences, and whether (or how far) I can explore this project without being there physically.
25. Mark Morris
2.17.11 / 4pm


It looks like a portal to another dimension. That it should simultaneously reference the gauge of Holl's elevation, but subvert its pattern and negate its depth, only helps make this reading richer. A portal to where? Not Narnia, not a fictive place, but space as defined by architecture. 


Perhaps, it is the third dimension reframed or clarified. Hilary Dannenberg in her essay “Windows, Doorways and Portals in Narrative Fiction and Media,” develops the notion of image-schemas and what the portal image conjures, quoting Turner: “…it is a schema which plays a key role in the human mind's perception and experience of three-dimensionality, i.e. the perception of the depth and extent of our spatial environment beyond the confines of the immediate container walls we find ourselves in at any particular moment.” What Lebbeus Woods and Christoph a. Kumpusch offer is a heightened experience of three-dimensionality sustained by the relationship of the given façade and the aberration of the pavilion in context. 


It seems key that the space of the light pavilion cannot be viewed from its host building's fenestration. One is reminded of that curious line from Evelyn Waugh describing Oxford, “…I went full of curiosity and the faint, unrecognized apprehension that here, at last, I should find that low door in the wall, which others, I knew, had found before me, which opened on an enclosed and enchanted garden, which was somewhere, not overlooked by any window, in the heart of that grey city.”





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.17.11 / 10pm
	
	
	Mark Morris: Thanks especially for the Waugh reference—he was always on target, and much despised for it. “A portal to another dimension” would be my fondest hope, but one just around the corner.
27. [chris teeter](http://www.metamechanics.com)
2.17.11 / 9pm


RUI good luck with that ideology…see you at Lebs cafe sometime…


I believe its a valid question to ask about virtual vs physical experience? And to ask the Master of the Craft how the virtual construction is turned into the physical construction?


We wouldn't ask some guy who does run of the mill masonry facade drawings this question ever, since the dwgs for this and construction are overwhelmingly available. 


These are questions for people who experiment.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.17.11 / 10pm
	
	
	chris teeter: Yes, indeed. At the heart of every architect is the desire to make ideas reality. This is either a great evil or a great good, depending on the nature of the ideas. In the best of all possible worlds, people will judge which is what for themselves. 
	
	
	Speaking of Leb's All-Night Cafe, drop by again sometime. From what I hear (and read), Leb the counterman is pretty lonely over there, talking mostly to himself….
29. Caleb Crawford
2.21.11 / 2pm


Lebbeus,  

Congratulations!


I am interested in one aspect: the photos we see are of the night time illumination. What is the daylight experience? What is the orientation? (it appears as though it might face east) Does it capture the rising or setting sun, illuminating the vectors and casting shadow? The study models indicate a possible answer, but the change in material to translucence suggest a different outcome.


For those of us engaged mostly in the pragmatic and familiar, it is a joy to see an investment in work that is “useless” and unexpected.
31. Mersiha Veledar
2.22.11 / 5pm


Dear Lebbeus,


This complex cut of intricate space of light seems to completely transform Holl's monolithic façades and gives them a new different reading in the urban context. It is almost as if the entire city of lights was compressed into a single space. A very celestial space….one that reminds me of another surreal dimension. 


It is inspiring to see you create such seminal work Lebbeus. 


I hope all is well,  

Mersiha
33. [Jeffry Burchard](http://architectureev.blogspot.com)
2.22.11 / 6pm


In the age of post exuberance that we are supposed to be living in now (post economic collapse) there is a danger of experimental refrain. How unfortunate it would be to reduce architecture's progress to the satisfaction of certain scientific qualifications or social injustices or constructable representation. It is absolutely imperative that now, as in every other point of our architectural history, that there are certain professionals, academics and provocateur's willing and ready to challenge axiomatic assumptions and to suggest something new or hidden from normative visions of the present/future. Here, Woods and Kumpusch have not only given us something strange and new but have addressed very directly the needs of constructable documents, contemporary materials, and the collectively shared presence of the architecture. This kind of project may be best suited for this scale, as an insertion into a larger builing as well as a singular moment in an experimental trajectory, but that is not to say that the project should never exceed this scale… indeed it must. To begin… (though this is obviously NOT the beginning of this work as Lebbeus has been around for quite sometime) …somewhere is not to ignore the constraints of other moments in time or alternate scales of deployment. 


This IS fantastic.
35. Dom Moisan
3.8.11 / 4am


As a former art and Architecture student currently learning 3d modeling, the thoughts in this tread are so enlightening. It made me remember the first time I experienced Ando Tadao's Benesse house and Chichu art museum. I was moved and began the understanding of built environnements that crucial day.  

I truly respect and congratulate the work of Lebbeus Woods and all the parteners involved whit him and his ideas as a symbole of what can be done.  

Thanks.
37. [Matthias Slavens Contrael](http://www.matthiasslavens.com)
3.11.11 / 5pm


I am really enjoying this project, I have been following your blog since my first semester of thesis at Pratt. It is nice to see this type of work come to fruition and leave paper. Making the seemingly impossible all too real. As much as I appreciate the value of a Construction Document. All the real beauty lies within those first few vector line sketches locked in their respective datums, but also breaking the “frame” at the right moments. They are embedded with the most potential. They remind me a bit of the Balthasar Holz post on Probable form (a favorite). Those however, seem to start from zero-point-inception and construct a datum or “frame” and a trajectory of forms around themselves. Wherein this the frame is seemingly set and the author decides how to erode-break and negotiate the frame. A true “trans-formation” of Genius loci.


Great work, both of you.  

-M
39. [gerridavis](http://gerridavis.wordpress.com)
3.13.11 / 9pm


I'm thrilled that you took on the challenge of a blog to share projects like this. It really is a treat for us all. Like Aida your drawings were my first intro to architecture, and will always resonate with with those frequencies acquired early. With so many like us around, I really think that the heyday of built LW work is around the corner.


One thing I wonder about in relation to your early work, which established a strong attitude about political circumstances in and around Serbia and the former Yugoslavian states, is how the effect of coming into contact with China and the strange communo-consumer culture might (or might not) influence you. Will certainly stay posted…
41. dazed
4.3.11 / 8pm


I bet einstein can see it… from alllll the way out there…. =) heheh – Nice Work, as always
43. [Architect Lebbeus Woods Refuses to Work in China Until Detained Artist Ai Weiwei is Released | Artists Speak Out](http://artistsspeakout.com/?p=35)
4.4.11 / 2pm


[…] a photo of Ai, a statement on Woods' website reads: “The Light Pavilion by me and Christoph a. Kumpusch is already under construction in Chengdu, China. I here state […]
45. [Top Ten: Flavinesque | Hartanah.Org](http://hartanah.org/architecture/top-ten-flavinesque.html)
4.29.11 / 5pm


[…] 6) (Ok, maybe not technically neon, but cool) Light Pavilion by Lebbeus Woods in collaboration with Christoph a. Kumpusch, in the Raffles City complex in Chengdu, China, by Steven Holl Architects. Image via Lebbeus Woods. […]
47. [LIGHT PAVILION Exhibition « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/06/03/light-pavilion-exhibition/)
6.3.11 / 3pm


[…] “LIGHT PAVILION” by […]
49. [interesting people « Scrapbook of C.S. Parrott](http://csparrott.wordpress.com/2011/07/26/interesting-people/)
8.1.11 / 12pm


[…] <https://lebbeuswoods.wordpress.com/2011/02/15/a-space-of-light-2/> […]
51. [LIGHT PAVILION: under construction « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2012/03/25/light-pavilion-under-construction/)
3.25.12 / 1am


[…] better, fun to track its progress, this post will be updated as construction proceeds.  Refer to this post for design and construction […]
53. Alexandr
3.30.12 / 6pm


I haven't seen this before. Our project has the same idea as wood installation on the demonstration screen for Showroom & Sales Office Design Competition.  

The Competition was started on May 2011.  

here <http://www.architizer.com/en_us/projects/view/house-on-the-roof-2011/35466/>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.31.12 / 6pm
	
	
	Alexandr—Thanks, but I'm not sure what you mean by “the same idea.”
