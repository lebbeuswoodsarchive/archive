
---
title: SOLIDARITY (updated 3 7)
date: 2011-02-27 00:00:00
---

# SOLIDARITY (updated 3/7)


# 


***Monday, March 7 update from the NewYork Times: <http://www.nytimes.com/2011/03/08/world/africa/08libya.html?hp>***


***Monday, March 7 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/blog/2011/mar/07/libya-uprising-live-updates>***


***.***


# U.S. Senators call for Libyan No-Fly Zone: *<http://www.nytimes.com/2011/03/07/world/middleeast/07nofly.html?hp>*


*Sunday, March 5 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/blog/2011/mar/06/arab-and-middle-east-protests-libya>*


*Sunday, March 6 update from the New York Times:<http://www.nytimes.com/2011/03/07/world/africa/07libya.html?hp>*


*(below) Photos from Ras Lanuf from the New York Times, March 6:*


*[![](media/l-3-6-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-6-3.jpg)*


*(above) The explosion of an ammunition depot killed and injured many. Photo by Ed Ou.*


*[![](media/l-3-6-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-6-2.jpg)*


*Funerals continue for soldiers and civilians killed in the fighting. Photo by Ed Ou.*


*[![](media/l-3-6-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-6-1.jpg)*


*Rebel fighters celebrate their victory over pro-Qaddafi forces. Photo by Lynsey Addario.*


*Saturday, March 5 update from the New York Times: <http://www.nytimes.com/2011/03/06/world/africa/06libya.html?_r=1&hp>*


*(Click on map below, from Saturday's New York Times, to see it at a larger, more readable size.)*


***[![](media/l-3-5-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-5-1.jpg)***


*Friday, March 4: Anti-Qaddafi protesters in Tripoli, armed only with stones, faced pro-Qaddafi troops who responded with tear gas and bullets:*


[![](media/l-3-4-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-4-1.jpg)


[![](media/l-3-4-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-4-2.jpg)


[![](media/l-3-4-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-3-4-3.jpg)


Friday, March 4 update from the New York Times: <http://www.nytimes.com/2011/03/05/world/africa/05libya.html?hp>


Friday, March 4 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/2011/mar/04/libya-protests-gaddafi-tripoli>


Friday, March 4 latest update from the New York Times: <http://www.nytimes.com/2011/03/05/world/africa/05libya.html?hp>


**.**


[![](media/brega-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/brega-1.jpg)


*(above) A rebel soldier during the battle for control of Brega yesterday (3/2). The rebels managed to drive back an assault by Qaddafi's forces, including mercenaries and Libyan warplanes.*


*[![](media/brega-map3-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/brega-map3-2.jpg)*


The meaning of the revolt in Libya, as well as in Egypt and Tunisia, lies in the fact that it is a rebellion by people against tyrants they have endured for more than forty years. If the United States or other Western powers intervene militarily on the side of the rebels, that meaning will be diminished, or even destroyed. Tempting though such intervention is, the rebel forces appear to be holding their own against Qaddafi's forces. The best thing that can happen would be that the rebels defeat these forces and drive the tyrant from power—all on their own. If that doesn't happen, then the United States and other countries (preferably Arab countries already liberated) should use their military power to stop the bloodshed . But it's too early in Libya's struggle for that to happen.


LW (3/3)


**Thursday, 3/3 updates from the New York Times: <http://www.nytimes.com/2011/03/04/world/africa/04libya.html?hp>**


**Thursday, 3/3 update from the New York Times: <http://www.nytimes.com/2011/03/03/world/africa/03military.html?ref=world>**


**Thursday, 3/3 opion piece by Nicholas D. Kristof: <http://www.nytimes.com/2011/03/03/opinion/03kristof.html?hp>**


As we sit here, reading this, a desperate and historic revolt is taking place in Libya. This oil-rich country in North Africa, geographically bracketed by Tunisia and Egypt, has been ruled for forty years by a strutting despot, Col. Muhamar el-Qaddafi. The rebels, inspired, no doubt, by the overthrow of dictators on either side–Ben-Ali in Tunisia and Mubarak in Egypt—have decided enough is enough. Qaddafi is different, however, from his authoritarian cohorts—he has unleashed his army, air force, and ‘security forces,' slaughtering unarmed protesters by the hundreds. The opposition forces are beginning to fight back. Seizing weapons caches with the help of defecting army personnel, they are in a violent struggle for their freedom from despotic rule. The outcome is far from certain at this date.


How can our thoughts and hopes not go out to these courageous people? How can we not but bow our heads in shame for our governments' toleration, even support, of the murderous tyrant they are fighting?


LW (2/23)


Wednesday, March 2 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/blog/2011/mar/02/libya-uprising-gaddafi-live>


Wednesday, March 2 update from the New York Times: <http://www.nytimes.com/2011/03/03/world/africa/03libya.html?hp>


Tuesday, March 1 update from the New York Times: <http://www.nytimes.com/2011/03/02/world/africa/02tribes.html?hp>


Monday, 2/28 Quadaffi forces counterattack:<http://www.nytimes.com/2011/03/01/world/africa/01unrest.html?hp>


Monday, 2/28 update from the New York Times:<http://www.nytimes.com/2011/03/01/world/africa/01unrest.html?_r=1&hp>


**[![](media/libya-20.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-20.jpg)**


I, for one, hope the above commitment will be fulfilled, and not too late to prevent more bloodshed. It would be heartening if the U.S. government acted on the side of the people for once, and not the autocrat. We shall see….


Sunday, 2/27 update from the New York Times: <http://www.nytimes.com/2011/02/28/world/africa/28unrest.html?hp>


Sunday, 2'27 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/blog/2011/feb/27/arab-and-middle-east-protests-libya>


Saturday, 2/26 update from the Manchester Guardian (UK): <http://www.guardian.co.uk/world/blog/2011/feb/26/libya-protests-middle-east-gaddafi>


Friday, 2/25 update from the New York Times: <http://www.nytimes.com/2011/02/26/world/africa/26libya.html?hp>


Thursday, 2/24, the latest report on the situation from the New York Times: <http://www.nytimes.com/2011/02/24/world/africa/24libya.html?hp>


Opinion piece by Nicholas D. Kristof: <http://www.nytimes.com/2011/02/24/opinion/24kristof.html?ref=opinion>


Update by the Manchester Guardian: <http://www.guardian.co.uk/world/2011/feb/24/obama-gaddafi-libya-violence-speech> (NOT reported in the New York Times.)


*The Libyan regime has all but closed down connection with the outside world. Still, a few photographs have leaked through. The ones below are a few of the best we have at the moment. Nearly all are taken in Benghazi, Libya's second-largest city, which is firmly under rebel control:*


*[![](media/l-10-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-10.jpg)*


*(below) At noon prayers on Friday, as an imam gives a sermon encouraging the overthrow of Qaddafi:*


*[![](media/l-111.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-111.jpg)*


*A funeral for a slain protester that begins in the streets and ends at a cemetery:*


*[![](media/l-12-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-12.jpg)*


*[![](media/l-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-13.jpg)*


*Soldiers who have joined the rebels are preparing to join the battle in Tripoli:*


*[![](media/l-14.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-14.jpg)*


*A poster of Qaddafi in the Benghazi courthouse, where rebels have formed an interim government made up of committees to run the city and coordinate with the rebellion in other Libyan cities:*


*[![](media/l-15.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/l-15.jpg)*


*Rebels and protesters, via cellphone:*


*[![](media/libya-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-2.jpg)*


*[![](media/libya-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-3.jpg)*


A rebel youth in a captured military uniform:


*[![](media/libya-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-8.jpg)*


*Protesters holding pictures of those killed by the army:*


*[![](media/libya-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-4.jpg)*


*The police station in Tobruk, destroyed by rebels:*


*[![](media/libya-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-5.jpg)*


*[![](media/libya-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-6.jpg)*


*Besides the sophistcated force of the military, many of whom have defected in refusal to fire on their own people, Col. Qaddafi has supportors among the people of Libya:*


*[![](media/libya-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-9.jpg)*


*[![](media/libya-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-7.jpg)*


*Rebels atop a public building in Tobruk:*


*[![](media/libya-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/libya-1.jpg)*


*The latest news from the New York Times: <http://www.nytimes.com/2011/02/24/world/africa/24libya.html?_r=1&hp>*



Below are comments made as of 2/25/11. For additional comments, see he bottom of this page:


## 5 Comments



1. ![](media/fd297f28376fb4ac64b280c60166b81d.jpg)Diego2.23.11 / 8am

The attitude of our “political leaders” is shameful. They should resign and apologize to these people. SHAMEFUL.



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3720#respond)
2. ![](media/cc6090237d180eb32c4e047918f40b4b.png)[chris teeter](http://www.metamechanics.com/)2.24.11 / 12am

I was in elementary school taking the bus to John F Kennedy school in Berlin shortly after the Berlin Disco bombings and we were escorted for a few weeks by US military men in a jeep following us to and from school. We'd do drawings and hold them up to the window for the US army guys to see. Did this blue marker drawing on white paper of an angry camel chasing my version of Quaddafi thru sand dunes…I was an american kid living in Berlin and got a jeep escort to school everyday due to a possible threat, what is the US doing now for these people?



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3724#respond)

	1. ![](media/0cba998c3580db5d208058bac9465891.jpg)[lebbeuswoods](http://www.lebbeuswoods.net/)2.24.11 / 1am
	
	chris: I'm writing to whitehouse.gov asking our President to intervene—with NATO or the UN—and prevent the bloodshed that is sure to come. Also, I'm going to keep the readers of this blog informed. For some reason, the newspapers—including the New York Times—are playing this down. Thanks for your story, and all it tells.
	
	
	
	[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3725#respond)
3. ![](media/74fde507a2c1b1e0e426e79cfa2597fc.png)Dustin Unrau2.24.11 / 5am

It has been very sad to follow the story of Libya and hear about the thousands of people who have died in their fight for freedom, my thoughts and prayers are with these people. I am not the least bit surprised by our governments' toleration to these issues. All we have to do is turn our heads south and we can see the results of a genocide that has been going on for the last three years in Mexico that has been extremely downplayed. Just today I was reading an article in one of the Mexico's national newspapers that talked about the 8,500 orphans in Ciudad Juarez as a result of the drug wars. 8,500 children had their parents killed in the last three years… in one city.  

It is sad to see that American and Canadian governments are doing next to nothing to help with this issue even though they are the source of the weapons being used to kill and the buyers of the drugs produced.  

As architects I think it is important that we focus our attention on these issues. We have the opportunity, more than ever before to use our resources to help these people. However, it seems we are more concerned with our “poor economic recovery”, our “low salaries” and the “lack of respect” our profession has generated than to look beyond our own noses and notice the terrible scenario the world is facing.  

cheers



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3727#respond)
4. ![](media/adf8e36ad2996c84ca341f21cc367e91.png)[Stephen](http://skorbichartist.blogspot.com/)2.25.11 / 4pm

What can architects do…  

This is some good news that is happening at Portland State University, just a few blocks away from where I work!  

Sergio Palleroni has been given a grant to study just how architects can help.


Here is the press release:


(Portland, OR) — The American Institute of Architects (AIA) last week named Sergio Palleroni, professor of architecture at Portland State University, and three of his colleagues from across the country as the recipients of AIA's 2011 Latrobe Prize. Palleroni and his colleagues won for their research proposal looking into the role architects play – and could play in the future – in public interest projects.  

The prize includes $100,000 in research funding for the team, which in addition to Palleroni, includes Bryan Bell, executive director of Design Corps; Roberta Feldman, professor at the University of Illinois at Chicago; and David Perkes, AIA, director of Gulf Coast Community Design Studio at Mississippi State University. Theirs was one of nearly 500 proposals considered for the prize, which is given every two years.  

“There aren't many bigger deals in the profession for an academic than the Latrobe Prize,” said Clive Knights, the chair of PSU's department of architecture.  

The award is granted by the AIA's distinguished College of Fellows for research leading to significant advances in the architecture profession. Palleroni said this is the first time the prize has been granted for a proposal addressing something other than a technical architectural challenge, such as cutting energy usage in buildings. This one affects the profession as a whole – looking into ways architects can be included more in public interest projects, such as public housing or the rebuilding of New Orleans. He said architects have the ability to design dwellings and structures that are greener, more functional, and more suitable for their owners than what are being constructed today.  

“At a time when billions of people around the world have a dire need for architectural services without the ability to pay the fees, the development of a public-interest practice manual may be one of the most urgent tasks facing the profession,” said Thomas Fisher, chair of the Latrobe Prize jury.  

Originally from southern Argentina, Palleroni has concentrated since the 1970s on issues of housing and community development in the developing world, working with organizations such as the United Nations, World Bank, and the governments of Chile, Colombia, Mexico, Nicaragua, India and Taiwan.  

Palleroni is a Senior Fellow of PSU's new Institute for Sustainable Solutions, and a founding member and faculty of the federally funded Green Building Research Lab. He is also a co-founder and director of BaSiC Initiative, which stands for Building Sustainable Communities, based at PSU's School of Architecture.


<http://www.pdx.edu/architecture/news/psu-architecture-professor-wins-prestigious-aia-prize>




 ## Comments 
1. ![](media/fd297f28376fb4ac64b280c60166b81d.jpg)Diego2.23.11 / 8am

The attitude of our “political leaders” is shameful. They should resign and apologize to these people. SHAMEFUL.



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3720#respond)
2. ![](media/cc6090237d180eb32c4e047918f40b4b.png)[chris teeter](http://www.metamechanics.com/)2.24.11 / 12am

I was in elementary school taking the bus to John F Kennedy school in Berlin shortly after the Berlin Disco bombings and we were escorted for a few weeks by US military men in a jeep following us to and from school. We'd do drawings and hold them up to the window for the US army guys to see. Did this blue marker drawing on white paper of an angry camel chasing my version of Quaddafi thru sand dunes…I was an american kid living in Berlin and got a jeep escort to school everyday due to a possible threat, what is the US doing now for these people?



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3724#respond)

	1. ![](media/0cba998c3580db5d208058bac9465891.jpg)[lebbeuswoods](http://www.lebbeuswoods.net/)2.24.11 / 1am
	
	chris: I'm writing to whitehouse.gov asking our President to intervene—with NATO or the UN—and prevent the bloodshed that is sure to come. Also, I'm going to keep the readers of this blog informed. For some reason, the newspapers—including the New York Times—are playing this down. Thanks for your story, and all it tells.
	
	
	
	[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3725#respond)
3. ![](media/74fde507a2c1b1e0e426e79cfa2597fc.png)Dustin Unrau2.24.11 / 5am

It has been very sad to follow the story of Libya and hear about the thousands of people who have died in their fight for freedom, my thoughts and prayers are with these people. I am not the least bit surprised by our governments' toleration to these issues. All we have to do is turn our heads south and we can see the results of a genocide that has been going on for the last three years in Mexico that has been extremely downplayed. Just today I was reading an article in one of the Mexico's national newspapers that talked about the 8,500 orphans in Ciudad Juarez as a result of the drug wars. 8,500 children had their parents killed in the last three years… in one city.  

It is sad to see that American and Canadian governments are doing next to nothing to help with this issue even though they are the source of the weapons being used to kill and the buyers of the drugs produced.  

As architects I think it is important that we focus our attention on these issues. We have the opportunity, more than ever before to use our resources to help these people. However, it seems we are more concerned with our “poor economic recovery”, our “low salaries” and the “lack of respect” our profession has generated than to look beyond our own noses and notice the terrible scenario the world is facing.  

cheers



[Reply](https://lebbeuswoods.wordpress.com/2011/02/23/solidarity/?replytocom=3727#respond)
4. ![](media/adf8e36ad2996c84ca341f21cc367e91.png)[Stephen](http://skorbichartist.blogspot.com/)2.25.11 / 4pm

What can architects do…  

This is some good news that is happening at Portland State University, just a few blocks away from where I work!  

Sergio Palleroni has been given a grant to study just how architects can help.


Here is the press release:


(Portland, OR) — The American Institute of Architects (AIA) last week named Sergio Palleroni, professor of architecture at Portland State University, and three of his colleagues from across the country as the recipients of AIA's 2011 Latrobe Prize. Palleroni and his colleagues won for their research proposal looking into the role architects play – and could play in the future – in public interest projects.  

The prize includes $100,000 in research funding for the team, which in addition to Palleroni, includes Bryan Bell, executive director of Design Corps; Roberta Feldman, professor at the University of Illinois at Chicago; and David Perkes, AIA, director of Gulf Coast Community Design Studio at Mississippi State University. Theirs was one of nearly 500 proposals considered for the prize, which is given every two years.  

“There aren't many bigger deals in the profession for an academic than the Latrobe Prize,” said Clive Knights, the chair of PSU's department of architecture.  

The award is granted by the AIA's distinguished College of Fellows for research leading to significant advances in the architecture profession. Palleroni said this is the first time the prize has been granted for a proposal addressing something other than a technical architectural challenge, such as cutting energy usage in buildings. This one affects the profession as a whole – looking into ways architects can be included more in public interest projects, such as public housing or the rebuilding of New Orleans. He said architects have the ability to design dwellings and structures that are greener, more functional, and more suitable for their owners than what are being constructed today.  

“At a time when billions of people around the world have a dire need for architectural services without the ability to pay the fees, the development of a public-interest practice manual may be one of the most urgent tasks facing the profession,” said Thomas Fisher, chair of the Latrobe Prize jury.  

Originally from southern Argentina, Palleroni has concentrated since the 1970s on issues of housing and community development in the developing world, working with organizations such as the United Nations, World Bank, and the governments of Chile, Colombia, Mexico, Nicaragua, India and Taiwan.  

Palleroni is a Senior Fellow of PSU's new Institute for Sustainable Solutions, and a founding member and faculty of the federally funded Green Building Research Lab. He is also a co-founder and director of BaSiC Initiative, which stands for Building Sustainable Communities, based at PSU's School of Architecture.


<http://www.pdx.edu/architecture/news/psu-architecture-professor-wins-prestigious-aia-prize>
