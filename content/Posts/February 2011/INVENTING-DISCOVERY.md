
---
title: INVENTING DISCOVERY
date: 2011-02-01 00:00:00 
tags: 
    - epistemology
    - theory_of_knowledge
---

# INVENTING DISCOVERY


[![](media/ri-4765-comp52.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/ri-4765-comp52.jpg)


*(above) A stone stela on a tiny rock island off the coast of Nova Scotia, Canada. When the apparently ancient artifact was cut open, a mechanical, geometric object was reportedly found inside. But was its presence invented? Why is it important to know which is true? Can we ever know, for sure?*


[Heinz von Foerster](http://en.wikipedia.org/wiki/Heinz_von_Foerster) told me a story once about one of his PhD. advisees. This young man had written what Heinz considered a brilliant dissertation in the field of physics and all that remained for him to get his doctorate degree was to present it to his faculty committee for their approval. In the course of his lucid presentation, he said, “And when Isaac Newton invented gravity….” One of his professors interrupted him, saying, “You mean, when Isaac Newton discovered gravity….” The young candidate replied, “No, Professor. When Newton invented gravity….” He failed the examination. After being given pause to reflect on his error, he again met with his faculty committee, and again said, “When Isaac Newton invented gravity….” Again he failed. Frustrated and fearful, he went to see Heinz for advice on what to do. Heinz told him, “The next time you get to the part about Newton, simply say, “And when Isaac Newton discovered gravity….” Next time, the young candidate said exactly that and passed ‘with distinction.'


The point Heinz wanted to make with this story is that the idea of knowledge generally accepted by professors of science and just about everyone else is that the truth—reality—is ‘out there' and it is our task to find, to discover it. In other words, reality exists independently of us. While Heinz and a number of cognitive scientists would not argue with this idea they would say that it is irrelevant, because the ‘closed' nature of the human nervous system makes perceiving any reality independent of it impossible. When we look out at the world, [what we perceive are our perceptions](https://lebbeuswoods.wordpress.com/2010/07/17/constructing-a-reality/), nothing more nor less. Hence, Isaac Newton did not discover gravity, a set of phenomena affecting our nervous system, and thus our perceptions, he invented it as a description of these phenomena that, as it turns out, has been agreed upon by a majority of knowledgeable people. Before Newton proposed his theory of gravity, people had other theories explaining the physical attraction of bodies to each other. Most knowledgeable people agreed with them. After Newton, Albert Einstein invented a new description/theory of gravity that most knowledgeable person have come to agree with and accept as true. Eventually there will be a theory that replaces Einstein's and most knowledgeable people…and so on.


Does reality change with the coming of a new theory describing it and people's belief in it? Or, are the theories simply wrong? Will there one day be a correct description of the reality that is out there, waiting to be discovered? Modern science does not attempt to answer this question, because it does not consider it relevant to understanding. What is relevant is inventing descriptions that lead to deeper knowledge of how we interact with the phenomena affecting us, including an understanding of understanding. What, after all, is knowledge?


As for the stela on the tiny rock island, we can at least be sure that if enough knowledgeable people believe what was found when it was opened is real, then it will be.


LW



#epistemology #theory_of_knowledge
 ## Comments 
1. [durere](http://durere.wordpress.com)
2.1.11 / 7pm


Well, I wouldn't say that Einstein's relativity theory replaced Newtonian physics. I think ‘extended' is a more appropriate term. Or, to put it otherwise, Einstein brought to light the limitations of ‘classic' (Newtonian) physics — when it comes to high speeds — and brought forth a model to accommodate objects moving at speeds closer to that of light.


But, aside from that, I find the concept of ‘inventing' gravity (rather then ‘discovering' it) mind-tingling (in a good way). Coming to think of it, it makes perfect sense: as reality is only what we make of it (as in, what we perceive), so are concepts like ‘gravity', ‘light' and so on. The discovery of gravity meant the invention of a model explaining attraction. Thus, gravity might as well be an invention. Just like languages: the fact that a population accepts it as a form of communication doesn't make it less of an invented convention. 


Great post.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.1.11 / 9pm
	
	
	durere: You are right, of course. Einstein's theory was necessary to describe the expanding scope of human exploration—into the universe and into the atom. Newton's physics still work at the human scale, but his theory of gravity is next to useless in astrophysics.  
	
	A footnote: one of the most important principles Einstein established was that the hypothesis—the theory—is essential in exploring the worlds beyond our direct perception. Newton had claimed that “I make no hypothesis”—he relied on what he could sense directly. Einstein, in writing about Newton, said “O, happy childhood of science! When we still believed we could understand nature by observing it….” Even with advanced instruments, we cannot sense the galaxies or the nucleus of an atom very well. With Einstein we enter an era dominated by theory. And not only in physics.
	
	
	
	
	
		1. tami
		10.21.11 / 10am
		
		
		hy  
		
		very interesting post and coversation. Can you pleases inform me as to the reference of Einstein's words?  
		
		thanks
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		10.23.11 / 5am
		
		
		taml: My reference here is to Albert Einstein's “General Theory of Relativity.” Go to Google or any other search engine and type those words in. You'll find a wealth of references. It's certainly one of the most famous, important, and influential theories of the 20th Century.
3. Leb, the counterman
2.1.11 / 9pm


You people talk about some pretty heavy stuff on this blog. That's nice. But you should stop over at Leb's All-Night Cafe [http://lebsallnightcafe.wordpress.com] sometime. The conversation's beginning to heat up over there and I'm sure you could get into it. You're welcome anytime, especially when you get burned out with this stuff,
5. wes
2.2.11 / 10pm


If we were to really believe that reality was mere reactions simulated by our perceptions, we couldn't believe anything at all. Our own perceptions are, in fact, physical reality. When you cop out of absolutes by saying experiences, knowledge and facts are relative to senses, you must be implying that your senses are somehow apart from the reality they observe. Our eyes, ears and skin do not deceive us unless something is wrong. We are mistaken when our minds misinterpret the reality of data perceived. That is to say, reality does exist outside of our own experiences or we wouldn't experience anything at all. But you can say that Isaac Newton invented gravity because that is the name of a concept for a statute in nature we believe we understand thus far.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.3.11 / 4pm
	
	
	wes: Clearly, you have nothing to worry about in your relationship with reality—your belief that it is ‘out there,' reliably independent of your ‘mere reactions,' seems unshakable. For those of us who believe that it may well be out there but is not directly accessible to us, our ‘reactions,' our perceptions transformed by our imaginations, constitute the only reality we can know. We (you and I) live, you might say, in different realities, indeed, different kinds of realities. However, the fact that we can communicate our beliefs about them to each other, different as they are, gives us hope that we can share the most interesting kind of community.
7. Nathan\_B
2.3.11 / 6am


I think the closest anyone has come to a plausible concept of an absolute reality is anthropologist and ethnobotanist Wade Davis who has come to recognize that “the earth itself can only exist because it is breathed into being by human consciousness.” To put it another way, “the world in which you were born is just one model of reality.” As someone who has dwelled with numerous indigenous groups around the world, each with their own unique mode of being in this world, Davis' perspective is the most diverse and complete I have come across. All realities are valid and true so long as the person dwelling in that reality believes it to be so.


It seems that reality is a means of understanding our place and existence. One of the ways we do this is by describing our environment. Gravity does not exist in an abstract sense, and nor did Newton invent it or discover it. He described it. What I find it fascinating is that Newton was able to describe something so fundamentally present in everyday life that few noticed it. What other components of our environment are so fundamental that we have not been able to describe?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.3.11 / 5pm
	
	
	Nathan\_B: I couldn't make the case better myself. As for your question, we can glibly say that there are many fundamentals, unnoticed and yet to be described. But if we go a bit deeper we realize that they will be ‘discovered' or ‘invented' only when they are needed in the ordinary course of human life. Newton's gravity—together with his ‘three laws of motion'— was introduced at the first glimmerings of an industrial society, with its emphasis on machines and mechanical production. Without these ‘laws', the machines couldn't have been conceptualized, or made. The question to ask therefore is, what sort of society is likely to emerge from the present one? If we know, or think we know, we'll also know what fundamentals to look for.
	3. [durere](http://durere.wordpress.com)
	2.3.11 / 5pm
	
	
	Excellent, poetic quote. I think it is the most wonderful way of putting how I feel about reality that I've heard thus far.
9. [chris teeter](http://www.metamechanics.com)
2.4.11 / 2am


Leb has a point, after all this heavy thought I need a beer. The cafe serve Weihenstephaner Weizen Bier?


rejected religion and discovered drugs and philosophy at the same time so…


Nathan B's Wade Davis qoute is damn good one, but really from start of blog to end of comments we went through Modern Philosophy 101.


This is a serious proposition: schizophrenia (un + sub)consciousness is the only absolute state of reality. One without lies(myths including logic) Absence of connection with the inventions and discoveries that define the effects on the senses as “real”. A state unlinked and over linked where discovery happens rapidly followed by denial and reformulation as one wades in nothingness, a hole of absence or elimination of static nouns and numerals, the verb of constant rejection of confirmation. It's a state that is hard to handle if you can't detach yourself from yourself and that self form the apparent “reality” the self exists in. The question of being and the eternal return to this question admitted as futile excercise but tied into the phenomenon of sensible existence. Deny and confirm at once and again and again, senseless over sensible. Make sense? (I”m touching it, its burning my hand, but does it need to be real?)





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.4.11 / 3pm
	
	
	chris: Leb the counterman informs me that the cafe is still waiting for a liquor license (he said,”Do you know how much a three-way liquor license costs? My god! The best I can do for your thirsty friend is give him a tour—virtual, of course—of ‘the oldest brewery in the world'”), and he passed on this address: <http://www.brauerei-weihenstephan.de/index2.html?lang=eng>. However, he reminds, it is always legal to bring your own.  
	
	Your comment proves its case by demonstration, a method I've always liked. The senseless that makes sense. The main reason any of this matters is that there are far too many people (including most architects) justifying what they do (often awful) by claiming an understanding and noble acceptance of ‘reality.' It's not their reality, of course, but ‘out there' and therefore they aren't responsible for it. A continual effort must be made to expose this self-deception or simple fraud and make them own up to their responsibility for reality. It's a losing battle, probably, but all the more reason it has to be carried on.
11. [chris teeter](http://www.metamechanics.com)
2.7.11 / 12pm


Thanks for the tour Leb, yes the oldest beer, nearly a millenium.


“It's not their reality, of course, but ‘out there' and therefore they aren't responsible for it.”


I find myself doing this in practice just by default sometimes, and its bad when the client says “I want you to be creative”, instead of constantly stating “that's a code or zoning issue” or “that's going to cost a lot”. This has partially to do with most the time when I tried to do something the response from client, boss, etc…was “code” or “money”. Those being the two “out there” items an architect could refer too.


For you kids getting out of school, if you have a really good idea, better than your boss, his response may be “code and money”, that's often BS to just shoot you down without saying it.


Anyway, I believe you are talking more about design and design principles. This bit baffles my mind sometimes, referring to say philosophically accepted texts and therefore the design is valid? Or putting a bunch of solar panels on a really tall building, only at the top, to ultimately incur more embodied energy than the energy that will ever be produced just to get LEED accredidation. 


Is “modernism” and “out there” accepted reality? School seemed this way, as in you always started with the basic principles of modernism and the bauhaus. All that decoration from before was a crime (loos). 


My question would be: is it possible to design organically? To enter each project without accepted “out there” realities? Can the project itself become the reality, but not in an isolated self referential language, rather a vector point in time and space? (That's very Kwinter at the end there)
13. Yeang Hong Ngui
2.24.11 / 6am


I find both ”invent” and ”discover”, rather out of context in discussing Newton and Einstein. Perhaps the word ”approximate” is more appropriate. If gravity was an apple, then Newton was looking at an apple underneath a cloth, and perhaps Einstein a thinner one. The truth is truth is never definite in the first place (heraclitus' the only constant is change). And even if it is ”out there”, it is only logical – given mankind's short lifespan- that we will forever be approximating. Perhaps Newton and Einstein are not good examples in presenting this little conundrum.  

Now, where's the beer…..
