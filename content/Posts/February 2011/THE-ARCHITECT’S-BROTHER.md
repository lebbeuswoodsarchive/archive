
---
title: THE ARCHITECT’S BROTHER
date: 2011-02-23 00:00:00 
tags: 
    - art
    - concepts
    - performance
    - photography
---

# THE ARCHITECT'S BROTHER


**Works by [ROBERT & SHANA PARKEHARRISON](http://en.wikipedia.org/wiki/Robert_ParkeHarrison).  A selection from their book, *The Architect's Brother*, published by [Twin Palms Publishers](http://www.twinpalms.com/) 2010.**


**.**


*Kingdom:*


[![](media/p-1b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-1b.jpg)


The Clearing:


[![](media/p-4b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-4b.jpg)


*Goddard's Dream:*


[![](media/p-6b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-6b.jpg)


*The Breath:*


[![](media/p-7b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-7b.jpg)


*Second Harvest:*


[![](media/p-8b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-8b.jpg)


*Breathing Machine:*


[![](media/p-5b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-5b.jpg)


*The Navigator:*


[![](media/p-2b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-2b.jpg)


*Making Rain:*


[![](media/p-9b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-9b.jpg)


*Pollination:*


[![](media/p-10b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-10b.jpg)


*The Sower:*


[![](media/p-3b.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/p-3b.jpg)


.


Thanks to Katerina Kefalogianni for the gift of this remarkable book.


LW


#art #concepts #performance #photography
 ## Comments 
1. Pedro Esteban
2.23.11 / 11pm


speechless……….





	1. Pedro Esteban
	2.23.11 / 11pm
	
	
	maybe isn't necessary say something
3. [sangahsuh](http://sangahsuh.wordpress.com)
2.23.11 / 11pm


They remind me of the paintings of Polish artist Beksinski. Amazing.
5. [Drafter](http://www.drafting-guy.com)
2.24.11 / 4pm


Wow. Does anyone know whether hard copies of these pieces exist? If so where and I can't help but imagine what size they are.
7. gb427
2.25.11 / 1am


Beautiful photographs..  

I am going to look these artists up.  

Thanks for sharing.


Gio
9. Mik
2.25.11 / 4pm


These feel like a visual Mattea Harvey poem with sound and temperature conveyed through an image.
11. dazed
4.3.11 / 8am


navigator!
13. [Aaahh!!! Real Monsters of Architecture | Hartanah.Org](http://hartanah.org/aaahh-real-monsters-of-architecture)
9.22.11 / 10pm


[…] Images from The Architect's Brother via Lebbeus Woods […]
