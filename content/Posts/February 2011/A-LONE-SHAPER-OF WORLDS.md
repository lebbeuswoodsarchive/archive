
---
title: A LONE SHAPER OF WORLDS
date: 2011-02-21 00:00:00
---

# A LONE SHAPER OF WORLDS


[![](media/kokines-9-11-copy2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/12/kokines-9-11-copy2.jpg)


*(above) September 11, an installation by [George Kokines](http://www.google.com/search?client=safari&rls=en&q=george+kokines&ie=UTF-8&oe=UTF-8) at the Elgin Academy, Chicago, 2010.*


At eighty years old, George Kokines is a man—and a painter—of remarkable vitality, humbling many younger than him. Part of it is due to his rugged Greek character—he was born on a tiny, rocky island in the Aegean Sea. But the more important part is his dedication to painting. For George (yes, we're friends), painting is more than a means of expressing thoughts and feelings, it is a way of living. What that means is that the act of painting, which is as physical as mental, is a condenser of his life and experiences. To be a painter, one must first have a life, rich with experiences.


The story of my knowing George is worth telling. Some years ago, I hung out three or four days a week at a bar called Fanelli's Cafe. In those days (no longer) it was a fashionably unfashionable spot in New York's Soho, at the end of the neighborhood's tenure as a bohemian nexus for artists and galleries. Dark and vaguely sinister, the long room with the long bar was populated in the morning hours (when I preferred to be there) with a motley crew of regulars—painters and hangers' on, writers, filmmakers, agents, and regular guys (yes, it was mostly male) who had discovered that interesting conversations would occasionally occur and that they were free to jump in. Crucial to the atmosphere of the place were the bartenders. They not only dispensed the needed medication, or fuel for talking, needed by the regulars, but set the general mood of the bar. One of the morning bartenders (they opened at 10) was Larry, a droll comedian who never cracked a smile as he made outrageously funny quips about the customers. Another was George, quiet, efficient, and not aiming to please.


The bar was rather empty in the morning and I always took a seat at the far end, away from the heavily imbibing regulars, sipping my drink and working in one of my small sketchbooks. When George was on, we would get into conversations about this and that, and increasingly about art. He learned I was an architect and I that he was a painter. I learned that he tended bar to earn money to keep his studio and continue painting—a very honorable path for a serious painter. He learned that I didn't “do buildings,” but taught and made speculative projects. Between his pouring drinks and my drawing in a sketchbook, we talked about many ideas and experiences, both trivial and profound.


By September 11, 2001, I had long since stopped being a regular at Fanelli's, but George and I kept in touch. I went to his studio, saw the large ‘cement' paintings he was working on, as well as smaller canvases in oils. He came to my loft-like apartment, where I was working on drawings and models for “The Fall” installation, and was also holding a workshop for international students called “Gr(o)und.” George experienced the destruction of the World Trade Center towers first hand—his studio was two blocks away. The military forced him and all residents of the neighborhood to evacuate. Some years later, after George had returned to Chicago, the city of his highly successful early career, the mural-sized painting *September 11 e*merged, slowly and laboriously, and I'm sure painfully. Looking at the [video](http://www.youtube.com/watch?v=Vl8VIpxBB18) of its emergence, we are struck not only by the painter's trial-and-error method, but more so by its seemingly inexorable move towards a final, unequivocal synthesis. The world of abstraction he takes us into is at once personal and universal. Or, it is better to say, universal precisely because it is intimately personal. That indeed is the meaning of *September 11*, the mural and the day.


*LW*



 ## Comments 
1. gb427
2.25.11 / 1am


In deed a very powerful painting, one can feel all that passed that day through the TV screen: The Two Towers, The Plane that cut through them and form the cross, The unbreathable fire, The suffocating dust and the view of the towers one receding always in perspective…very personal and yet universal.


Gio
