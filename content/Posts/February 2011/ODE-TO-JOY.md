
---
title: ODE TO JOY
date: 2011-02-12 00:00:00
---

# ODE TO JOY


Human solidarity is a powerful and rare event. When it occurs in the cause of freedom—which is really the cause of human creative potential—it is inspiring and terrifying. It inspires those who believe in people's goodness and need for each other; it terrifies those who believe that people must be controlled, for their own good, and for the good of the controllers. People's solidarity lasts only when it is protected by institutions that they alone can create. Indeed, that is the first creative task to be accomplished, and therein lies the rub.


But let us not dwell on the future. Rather, let us join the Egyptians in Cairo's Tahrir Square last night, for this moment of their freedom, in their joy and exultation. Long may it last!


LW


[![](media/2-11-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-10.jpg)


[![](media/2-11-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-2.jpg)


[![](media/2-11-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-3.jpg)


[![](media/2-11-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-5.jpg)


[![](media/2-11-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-1.jpg)


[![](media/2-11-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-6.jpg)


[![](media/2-11-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-7.jpg)


[![](media/2-11-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-9.jpg)


[![](media/2-11-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/02/2-11-8.jpg)


Photos from the New York Times [article](http://www.nytimes.com/2011/02/12/world/middleeast/12revolution.html?_r=1&scp=2&sq=Tahrir%20Square&st=cse)—thanks to this institution!


[18 Days at the Epicenter of Revolution.](http://www.nytimes.com/interactive/2011/02/12/world/middleeast/0212-egypt-tahrir-18-days-graphic.html) New York Times interactive graphic.



 ## Comments 
1. Caleb Crawford
2.14.11 / 2pm


Truly inspiring events, and something to be noted in our own country, where unemployment of 19-24 year olds currently tops 19% (official estimates). Seemingly immovable edifices suddenly crumbling, like the Berlin wall. The impossible suddenly not only possible but real.
3. [Stephen](http://skorbichartist.blogspot.com/)
2.14.11 / 6pm


Fantastic photos. I truly hope that the Egyptians find their freedom and there peace, but they are in for a bumpy ride. Who will take power? What is the plan?  

I don't think the USA needs a revolution, but we do need solidarity and focus.  

Long live Freedom!
