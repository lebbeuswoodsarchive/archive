
---
title: RETURN OF THE MANIFESTO
date: 2011-11-28 00:00:00 
tags: 
    - architecture
    - manifesto
    - Peter_Noever
    - urbanism
---

# RETURN OF THE MANIFESTO


[![](media/book-manifestos1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/book-manifestos1.jpg)


*(above) Front cover. More contributors are listed on the back cover.*


First, a little story. In 1994, when I attended an architecture conference in Havana organized by the Museum of Applied Art (MAK) in Vienna, the director of the museum, Peter Noever, had asked the participants to write their personal manifestos of architecture in order to shake up the discussions a bit. I was the only one who did. The others, including Thom Mayne, Eric Owen Moss, Carme Pinos, and Wolf Prix, ignored the request. It was only when Peter insisted, that several of us got together to hammer out something over some Mojitos. It was quickly obvious that there was little enthusiasm for manifestos, even under the influence, and when Wolf started reciting the lyrics to Dylan's “Desolation Row”—“…they're selling postcards to the hanging, they're painting the passports brown; the beauty parlor's filled with sailors, the circus is in town….” we knew the project was hopeless.


The assembled company at this conference, set in a fabled country that had run out of patience with ideological rhetoric, knew that the manifesto as a genre was dead. No one wanted manifestos. Nobody believed in them. So, what was their point? That was twenty-five years ago.


After a long period of often frivolous form making and unprincipled egoism in architecture, which have played into the hands of the most venal interests of real-estate developers and marketers, some architects are looking for more substantial ideas to serve, more meaningful goals to strive for, and the manifesto has come back. It is probably a temporary aberration, owing to an unsustainable idealism that lurks within statements of principle, but even their brief resurgence can help to regenerate—at least for a while—our beloved, beleaguered field.


Of half a dozen new manifesto compendia, the one that seems to have attracted the most critical attention is one titled “[Urban Future Manifestos](http://www.domusweb.it/en/book-review/urban-future-manifestos/),” collected and edited by—guess who?—Peter Noever, in collaboration with Kimberli Meyer. Persistence, it seems, sometimes pays off.


LW


#architecture #manifesto #Peter_Noever #urbanism
 ## Comments 
1. [Dave Chernin](http://www.davechernin.com)
11.30.11 / 11pm


“Architecture is ones perception which through a transformation emerges as an essence with an idea. The perception originates from ones soul, an idea is formulated from ones mind. Architecture does not exist if both are not embodied in the work. An idea from the mind and a perception from the soul in regard to Form must create something that has not yet been. Architecture like a poem surpasses it's origins. There is no true piece of Architecture without true Creation.” – Dave Chernin
3. underscore
12.4.11 / 5pm


“The revolution is against the corporation, against the corrupt politician, and against any oppressive force. The revolution is against every -ism and -ist for no -ism or -ist is true to itself, but are purely “the smoke screen puppet show put in front of us to keep us from seeing the ever present invisible third parties.” To revolutionize one must cut ties to the corporation, politician, and oppressive forces; to revolutionize one must become oneself and oneself must become invisible.


Revolutionary Architecture is ineffective when its visible. There is no revolutionary style, but merely revolutionary function and revolutionary performance. Revolutionary Architecture must not be reliant upon any ties or connections to any outside sources or providers. Revolutionary Architecture must be self reliant and self producing, self changing, self morphing, self revolutionizing. Revolutionary Architecture must respond to and revolutionize the users, by which Revolutionary Architecture, through itself, revolutionizes the local area while remaining invisible to outside sources.” – underscore, with ideas from eduardo mcintosh
