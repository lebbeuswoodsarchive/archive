
---
title: JAY MARK JOHNSON  Seeing at its limits
date: 2011-11-24 00:00:00 
tags: 
    - Einstein
    - Jay_Mark_Johnson
    - Relativity
    - spacetime
---

# JAY MARK JOHNSON: Seeing at its limits


Albert Einstein invented two theories of ‘relativity' that revolutionized our understanding of the physical world. The first one, which he called the “Special Theory,” deals with electro-magnetism, the physical force that governs the interactions of objects existing between the scale of molecules and, roughly speaking, the human body. The second theory, which he called the “General Theory,” deals with gravity, the physical force that governs the interactions between very massive objects, such as planets, stars, galaxies, and ultimately, the entire universe. It is the first theory, addressing the human scale, that concerns [Jay Mark Johnson](http://vimeo.com/user2610352) in the series of his studies presented here.


One of the primary tenets of the Special Theory is that space and time are not two separate phenomena, but a single phenomenon, which Einstein called “spacetime.” So, as an object moves faster in relation to another object, its time in relation to that object slows down. This results in a “time paradox” often written about in science fiction: astronauts traveling away from the earth at near-light speeds necessary to reach the nearest stars, will age only the few years of their journey, while those on Earth will be decades and more older by the time of the astronauts' return. Actually, this slowing down of time has been tested with atomic (very accurate) clocks circling at high speeds in earth-orbit.


The other prominent effect of relative motion is the so-called “shortening of rods.”


As objects move (accelerate) in relation to other objects, they become compressed (shorter) in the direction of acceleration. This is not simply an apparent shortening of the “rods,” or measuring sticks, hypothesized in Einstein's Special Theory, but an actual shortening. This was difficult for his contemporaries (such as Poincaré) to accept, because Newton and Descartes had proved that space and time were absolute and immutable. Nevertheless, experimental evidence produced since the creation of the Special Theory confirms that space and time are in fact elastic and interdependent. Jay Mark Johnson's studies are plays upon the relative mutability of our physical reality.


LW


.


[![](media/jay-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-1.jpg)


[![](media/jay-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-2.jpg)


[![](media/jay-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-3.jpg)


EL ANARQUISTA Y EL AUTOMOVIL


Zwischen Don Quijote und Rattenfänger Jay Mark Johnson at Galerie Deschler, Berlin September 9 – December 10, 2011


“Am I dreaming some impossible dream? Or just taking a reading with my spacetime machine?”


Over the past years I have been producing photographic artworks focused on concepts of space and time and their relationship to larger cultural narratives. As part of that undertaking I have developed a curiosity centered on how we perceive, represent and discuss our surroundings. Producing a series of spacetime artworks has been tantamount to that inquiry. Our experience of space and time appears to be shaped as much by innate abilities, intrinsic to our biology, as by historic tradition. My aim is to participate in contemporary discussions on these matters. When plausible, I make an effort to apply any attendant understanding, however oblique, towards a more comprehensive and accommodating sociopolitical critique.


I make photographic timelines. It is an engaging project with plenty of unexpected results. I use an unconventional camera system designed for shooting panoramic space. Though it has a lens up front, the device does not take static, instantaneous snap-shots. Instead, it produces smooth, uninterrupted scans of the surrounding environment. By making a few simple modifications – I click on one button and then wrap the whole thing in tape – I am able to convert it into an effective tool for shooting fluid, continuous timelines. The results can be shocking and unimaginable. Because they seamlessly blend visual depictions of space and time into a single hybrid image they provide an altered “spacetime” view of the world. In these pictures, the rules for perceiving reality are shifted. Shadows are crisscrossed, the relative speed of an object determines its size, moving subjects appear isolated from their backgrounds, and the backgrounds themselves have been decimated. The effect is like stepping “through the looking glass” with Alice. Viewed with an eye for creating provocative imagery, the poetic potential is wide open.


Finding one's bearings and learning to navigate through this strange new world requires concerted effort. My first experiments produced unrecognizable images filled with unpredictable shapes and forms. With increasing methodology and with ongoing tests, I am now able to separate subjects from backgrounds, bring fluid gestures into clear focus, trace moving subjects, and draw attenuated delineations of naturalistic events. My modified camera presents other technological quirks that I attempt to exploit. All lenses bend light. But my camera's unusual sensors permit me to register surprising prismatic refractions that normal cameras and retinas are unable to see.


Sometimes I feel my work is akin to the practice of *bricolage* in that I attempt to make meaningful compositions out of unwanted lens artifacts and other leftover technical “junk”. My camera's tendency to isolate the subject from its background carries great metaphorical potential. I have found myself, for example, attempting to recreate the mood of an empty piazza painted by De Chirico or the sense of estrangement and abandon depicted by Sartre. I am fascinated at how the shadows of pedestrians, because they are delayed by time, portray a second, ghostly rendering suggesting a hidden, contradictory character. And because my camera is blind to horizontal left-right space – it sees no such thing at all – all distinguishing elements of the landscape are entirely absent. They appear as though they have been obliterated by the speed of contemporary life.


The principal pieces of this exhibition were produced while walking the streets of Valencia with my friend, Jorge. A radio journalist whose daily broadcasts incline sympathetically towards Spain's tradition of independent, free governing, cooperative societies, Jorge presents an in depth understanding of the political and economic forces that have shaped Valencia's development over the millennia. He is critical of the grand utopian schemes that divided prewar Europe and is equally disapproving of new large scale urban projects when they are of questionable benefit to the population at large. My artworks contrast Jorge's confident pace with the frenzied onslaught of commuters in their cars. Where is everyone going? With what urgency and purpose? What if you do not wish to be swept along by their movements? Or you don't want to join their community? Sometimes the recorded figure of Jorge reminds me of Don Quixote. Is his independent stride an act of defiance or a sign of romantic self-delusion? And what about the commuters rolling around the new cityscape in their own tiny suits of armor? Are they chasing Jorge or following him? And if he is the Pied Piper of Hamlen, are they the precious children or the unwanted pests?


All of the images in my series are timelines. They bring to mind narrative structures which, in turn, raise questions about the nature of narrative thinking. Is there a relationship between our ancient skills of navigation and the universal virtuosity of our storytelling? Perhaps, as linguistic anthropologist Stephen Levinson has said, evolution can also be viewed as *bricolage*, a process in which leftover brain “junk” is reapportioned for a new creative use. Has our old expertise been readapted to a new, more critical task as we narrate the increasingly convoluted terrain of our complex social setting?


But if navigation gives us narrative form it does not deliver content. With my unorthodox camera, I enjoy producing unexpected images and presenting them to the world at large. I know that part of my pleasure comes from proffering a critical dialogue about our surroundings, part comes from the image making process itself and part from the responses I receive. In putting forth these eccentric inversions of space and time, I am hopeful of provoking reflection not just upon the unfamiliar images themselves or the curious perspectives they imply. My aim is to participate in larger discussions about how we perceive space and time and about the nature of perception itself. And though these works are inseparable from traditions within the history of art and photography, they also bear conceptual overlap within the sciences – in fields of study as far flung as theoretical physics or cognitive linguistics. In that broadest sense, I am inclined to look to both the arts and sciences to provide practices that challenge perceptions, shift perspectives, or otherwise broaden established insights – into the physical world, into our cultural constructions, and, ultimately, into the nature of our own behavior.


What relevance does this have in the sociopolitical realm? The value of gaining new insights into human nature can hardly be over stressed. For underlying every plan of action, whether a grand utopian scheme or something more local and manageable, lies an implicit model of who we are and what we are capable of. What if, for instance, the neurosciences suggest that brains are more varied than faces, that some individuals by nature feel tremendous empathy while others feel none at all? In that light, tolerating disagreement would be understood as a requisite first step for a large population to arrive at any kind of unity. Or what if contemporary behavioral sciences find that we are all born with an entrenched predisposition to form communities, define boundaries and distrust outsiders? Then the problem of racism, rather than being seen as a purely cultural construct, would be understood to have an inherited component. Taking that into account would be an important new step toward arriving at a more effective solution. The idea is quite simple. The better we understand our behavior, the more likely we are to come up with effective plans for whatever we want to do. For if we are going to be making real efforts, then we want to be making real progress. And not just tilting at windmills.


Am I dreaming some impossible dream? Or just taking a reading with my spacetime machine?


Jay Mark Johnson


Autumn 2011


*Inside the panoramas:*


[![](media/jay-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-4.jpg)


.


[![](media/jay-71.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-71.jpg)


[![](media/jay-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-6.jpg)


.


[![](media/jay-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/jay-5.jpg)


.


**JMJ**


#Einstein #Jay_Mark_Johnson #Relativity #spacetime
 ## Comments 
1. [Dave Chernin](http://www.davechernin.com)
11.30.11 / 11pm


“Architecture is ones perception which through a transformation emerges as an essence with an idea. The perception originates from ones soul, an idea is formulated from ones mind. Architecture does not exist if both are not embodied in the work. An idea from the mind and a perception from the soul in regard to Form must create something that has not yet been. Architecture like a poem surpasses it's origins. There is no true piece of Architecture without true Creation.” – Dave Chernin
3. underscore
12.4.11 / 5pm


“The revolution is against the corporation, against the corrupt politician, and against any oppressive force. The revolution is against every -ism and -ist for no -ism or -ist is true to itself, but are purely “the smoke screen puppet show put in front of us to keep us from seeing the ever present invisible third parties.” To revolutionize one must cut ties to the corporation, politician, and oppressive forces; to revolutionize one must become oneself and oneself must become invisible.


Revolutionary Architecture is ineffective when its visible. There is no revolutionary style, but merely revolutionary function and revolutionary performance. Revolutionary Architecture must not be reliant upon any ties or connections to any outside sources or providers. Revolutionary Architecture must be self reliant and self producing, self changing, self morphing, self revolutionizing. Revolutionary Architecture must respond to and revolutionize the users, by which Revolutionary Architecture, through itself, revolutionizes the local area while remaining invisible to outside sources.” – underscore, with ideas from eduardo mcintosh
