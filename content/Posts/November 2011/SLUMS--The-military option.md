
---
title: SLUMS  The military option
date: 2011-11-13 00:00:00 
tags: 
    - military_intervention
    - Rio
    - slums
---

# SLUMS: The military option


## *NOVEMBER 17:*Protesters and police [clash on Wall Street.](http://cityroom.blogs.nytimes.com/2011/11/17/protesters-and-officers-clash-near-wall-street/?hp)


## *NOVEMBER 15:***Occupy Wall Street protesters [removed by force](http://www.nytimes.com/2011/11/16/nyregion/police-begin-clearing-zuccotti-park-of-protesters.html?ref=global-home) from Zuccotti Park.**


.
[![](media/rio-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/rio-1.jpg)
November 13, 2011
#  Authorities Take Control of Rio's Largest Slum


By REUTERS



RIO DE JANEIRO — Three thousand troops backed by helicopters and armored cars occupied Rio de Janeiro's largest slum on Sunday, another step in the city's bid to improve security and reign in drug gangs.


The occupation of the area called Rocinha, a notorious hillside neighborhood that overlooks some of Rio's swankiest areas, is a crucial part of the city's preparations to host soccer's World Cup in 2014 and the Olympics two years later.


Security forces have occupied nearly 20 slums in the last three years but none as symbolically or strategically important as Rocinha, a sprawl of shacks, stores and evangelical churches at a traffic choke point between the main city and western areas where most Olympic events will be held.


With army helicopters overhead, troops began climbing the slum's winding roads just after 4 a.m. and declared the operation a success within two hours after encountering no resistance.


The takeover of Rocinha and the nearby Vidigal slum was as much a media event as a military operation. Hundreds of reporters followed soldiers and police through deserted, garbage-strewn streets. The authorities had announced their plans days in advance, giving gang members plenty of notice to flee.


According to TV news channel GloboNews, only one person was detained during the operation.


After years of living in fear of both gang members and the often-violent tactics of police, residents were wary of embracing the new reality.


“Let's hope for the best, but there's a lot more that needs to be done,” said Sergio Pimentel, a funeral director sitting outside his business watching the operation unfold.


He pointed to an alley that he said poured raw sewage on to the street whenever it rained.


“We need basic sanitation, health, education,” Mr. Pimentel said. “They have to come in with everything, not just the police.”


The sprawling hillside community, home to about 100,000 people, has one of Brazil's worst rates of tuberculosis, officials say. It is often described as the largest slum in Latin America and is believed to be the main drug distribution point in Brazil's second-largest city.


Under a so-called “pacification” program, Rio authorities are following up invasions by handing slums over to specially trained community police and providing services like health centers and formal electricity and TV supply. The aim is to foster social inclusion and give the city's one million or more slum residents a bigger stake in Brazil's economy.


Progress has sometimes been slow, however. A year after a similar operation to occupy a large slum called Alemao, the favela has yet to receive a community police force as the security forces struggle to train enough officers.


Most of the occupations have taken place in slums close to Rio's wealthier areas, leading to criticism that the program is aimed mostly at supporting the city's real-estate boom and preparing for the sports events.


[![](media/rio-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/rio-2.jpg)


[![](media/rio-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/rio-3.jpg)


*[This article originally appeared in the New York Times.](http://www.nytimes.com/2011/11/14/world/americas/authorities-take-control-of-rios-largest-slum.html?hp)*



#military_intervention #Rio #slums
 ## Comments 
1. [Pedro Esteban](http://gravatar.com/rafaeloshouldbe)
11.13.11 / 7pm


Interesting news this morning!  

Many questions and analysis, but the most important: this is good or bad?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.14.11 / 9pm
	
	
	Pedro Esteban: You tell me. What do you think?
3. David Long
11.15.11 / 12pm


It is surely an act of “cleansing”. Not dissimilar to that which took place here in South Africa for the recent hosting of the Fifa world cup. Military control should be kept well away from any quantitative or qualitative improvements to be made informal settlements. The military will be gone the moment the world cup is over and it will be business as usual. Solving the knot with no beginning and no end means that the solution should emerge from the knot itself.





	1. Teresa
	11.17.11 / 5pm
	
	
	What do you mean “from the knot itself”? In this case what and/or who is the knot that you say could potentially unravel itself?
5. Flavio
11.21.11 / 10am


Yes… What do you mean by that, Mr. Long?  

Brazilians only know the real importance of this. It's more than a cleanse and they might even leave BEFORE the World Cup, if the pacification is finally implemented in Rocinha. The people of Rio has too much to celebrate.  

Favelas are secular problems in Brazil, and something that went out of control on the last decades.  

This had to be done, and so far, its well done.
7. LIMBO
11.22.11 / 6pm


@ Pedro is this good or bad? Let us be real here. This is nothing different than what the Chinese government did AND got away with in 2008. The military and government gave notice to villages with ZERO incentive and said they had to leave by this date and time. Then – the villages and slums were cleared for the olympic torch run. How can this be good? There is no ethics involved in this. 


Not only did they tear down villages to make way for the torch run, they also built large walls to cover up the existing housing, so it would not ruin the ‘drive' to the olympics. Hyper-real sports like FIFA and the Olympics is merely a status full of iconic architecture and awe filled imagery. This is not something I agree with.





	1. Pedro Esteban
	11.24.11 / 8pm
	
	
	So, is bad for all the reasons all you have pointed here, we know that, this is a really complicated process. I ll never accept the intervention of Military forces in any way related to social, that is militarism-totalitarism.  
	
	But someone can think this is good, just because they are removing all the social factors who arrives from the narco, and I don't think remove that is a good action, is not the intervention of the military forces(we know that is an entirely wrong action) what I am more interested in, is about the drugs. My question is what happens if we remove the narco? That is good or bad?
9. David Long
11.23.11 / 12pm


Perhaps I was rather fleeting in my comment. My knowledge of the intricacies of the Favelas in Brazil is minimal. I was merely responding from the perspective I have of informal settlements in general, through my experiences and research here in South Africa. So if the comment was flippant, I apologize. In South Africa the promise of improvement to informal areas is used as leverage for politicians to boost their popularity among the masses. Consequently improvements which are made are extremely top down and do little to create qualitative long term improvements to the living environment. These improvements also shatter the fine scale social networks which form as a result of the organic growth and natural negotiations between neighbors within the settlements. While I do not wish to make light of the dire conditions existing within the informal, I do believe it should involve more than provision of a roof, which are planned in the thousands, each looking identical, resulting in the most sterile environment one could hope for. So it is the heavy handed top down approach which I detest. Is the military action in the favelas of Brazil not the flip side of the same coin? 


Or am I missing something? I agree, it must bring a lot of relief to the people of Rio knowing that their streets are safer, but how far does this go to make long term improvements? The rate of urbanization means that the informal will continue to expand and the related problems of crime (among the infinite others) shall re-emerge. Which brings me to the point of the knot. Mr. Woods made reference some time back to slums as being knots with no beginning and no end (I hope my memory is correct on this score). I find this to be a fitting description. Perhaps we should not be attempting to untie the knot in the first place. Knots can be useful too. If slums are to be the defining urban form of the cities of the global South, then perhaps we need to be understanding fibers of that which makes up the collective whole of the knot, as opposed to attempting to untie it through top down force.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.25.11 / 3pm
	
	
	David Long: I personally appreciate your candor and modesty before the enormity of the problem. On the other hand, a doctor doesn't need to have cancer in order to treat it effectively in someone else. Your observations are valuable as discussion points, at the very least. What is most valuable is your engagement with the problem.  
	
	Also, your interpretation of the knot analogy is spot on.
	
	
	
	
	
		1. Sally Bowles
		12.24.11 / 7pm
		
		
		But that doctor will only treat it if there is financial compensation or other ulterior motive. Just like you drumming up this blog as a way of parading your importance as a pseudo-architect. 
		
		
		However, in this case, the Brazilian government will be hosting the Olympics in the foreseeable future and needs to “clean house” beforehand for the television cameras and journalists. It happened in Mexico in '86 for FIFA, S. Africa 2010, China 2008. As long as FIFA or IOC continues to award the hosting to third world regions, expect this to continue.
