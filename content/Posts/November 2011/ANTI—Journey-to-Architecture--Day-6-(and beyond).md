
---
title: ANTI—Journey to Architecture  Day 6 (and beyond)
date: 2011-11-05 00:00:00
---

# ANTI—Journey to Architecture: Day 6 (and beyond)


## *BREAKING NEWS—The crisis at Cooper Union. Keep updated at [this student website](http://freeasairandwater.net/)*.


**DAY 6—October 26**


Raimund and I agreed that it was inconceivable we would not visit the last church designed by Le Corbusier at Firminy-Vert, only a two hour drive south from Roanne, and then make the day-long dash across France and Switzerland to Zurich, where we would catch our flight to New York the next day. Rising in early morning darkness, having our coffees, we drove to the town of Firminy, where Le Corbusier had designed not only a church, but several housing blocks—Unité—a cultural center, and a sports stadium, as part of a broad urban plan that was actually constructed in the 70s. All, that is, but the church, which was begun then but remained unfinished until a few years ago, when—due to the loyal efforts of Jose Oubrerie, who had worked for Le Corbusier on the original design—it was finally completed, though in rather different form than the original design. The first thing Raimund and I commented on when we arrived at the site was how the vertical proportions of the church were diminished, compared with the design drawings we knew—it seemed flattened and more squat. Putting off going inside, we first went to the cultural center adjoining the sunken stadium, which was unremarkable inside and out except for its inversely sloping concrete façade facing the sports field and the church beyond. There was one strange detail: oddly-shaped concrete benches in a full state of decay, suggesting that they'd been there for a long time. Were they designed by Le Corbusier, or added later? We found one that faced a concrete wall just outside the cultural center, which was a very peculiar arrangement—its photos are seen on the covers of this book.  

The interior of the church—an immense squared conical void with the familiar light tubes at the top—is impressive, but much less severe in feeling than the church at La Tourette, because of the relative softness of the form. There are two other factors: first, the concrete is highly finished, very smooth, and without the ‘brutal' character that confirms the human sacrifice at the core of Christian theology; and the form is here leavened with a large, decorative array of many tiny light holes in the concrete wall over the altar, meant I believe to symbolize the stars or cosmos. We are unmoved, much to our regret. Still, we cannot help but admire Oubrerie's dedication, and leave Firminy, driving east towards the Swiss border, feeling more that we have seen his work than that of Le Corbusier.  

It is a hard eight-hour drive, through beautiful mountains and cities that get uglier as we near Zurich. We are emotionally exhausted, if not physically, and rather depressed. We quarrel several times about whether to stay somewhere along the way. Fortunately we do not, because I have misread the itinerary and mistakenly think we depart from the airport later than we're actually scheduled—if we had stopped en route, we would have missed our flight. We quarreled when we got to Zurich, as we drove from hotel to hotel looking for rooms. Finally finding them in a nice four-star hotel, we took a walk along the river to a famous restaurant at the city center, where we had a fine dinner and good wine—“at last!”—and returned to the hotel in good spirits. Our odyssey, as Raimund called it, was effectively over. We had almost miraculously reached our destinations, but not our goal. Our dialogue on architecture had really only begun.


*Photos following are by Raimund Abraham, except as noted:*


The church at Firminy-Vert as originally envisioned by Le Corbusier:


[![](media/fv-0.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-0.jpg)


The church at Firminy-Vert as completed by Jose Oubrerie:


[![](media/fv-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-1.jpg)


.


[![](media/fv-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-3.jpg)


.


[![](media/fv-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-2.jpg)


The church interior, showing the pattern of holes in the upper walls, a ‘cosmic array', probably designed by Iannis Xenakis, Le Corbusier's former architectural collaborator and composer of revolutionary electronic music:


[![](media/fv-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-10.jpg)


The Choir space adjacent to the Altar area (with LW and his infernal camcorder):


[![](media/fv-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-8.jpg)


The Nave of the church:


[![](media/fv-91.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-91.jpg)


Looking up into what might be called the Dome of the church, though it is more a forced perspectival space, a diminishing, darkening void:


[![](media/fv-71.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/fv-71.jpg)


.


***AND BEYOND***


*The following is a transcription by Rachel Bursac of the taped dialogue between RA and LW, after our return to New York:*


LW: (handing RA a piece of paper). Here are some questions I have on my mind based on our conversation[[1]](#_ftn1). They're not all terrific, but they're related. You had mentioned talking about our work and I think it's good to give examples of our ideas, otherwise I don't want to get into some descriptions about our projects.


RA: No. No descriptions. I am always talking about the underlying philosophical position and the formal position, but what connects us is the spirit, the passion for architecture and our respect for each other's works despite the fact that formally our work is very different, even opposed. Also, I would say, the book is not a conversation, you see, it's a dialogue. And the dialogue has to be dialectical, controversial, should position the different origins of our work, intent or whatever, and should be a confrontation in a positive sense. One thought triggers another thought, and opposites are by far a clearer strategy for getting to the truth.


LW: Our differences. First of all, they're philosophical. Judging by what we say and do, it's obvious that I'm interested primarily in the things that change in life, while you are interested in those than don't change. In my life and my work—the two can't be separated—-the driving idea has been transformation.  My approach to thinking about architecture has always been based on the consequences of changes that are out of our control. I've tried to imagine not only forms but also ways—methods—of dealing with them in precise spatial terms. If I had to presume, forgive me, your work seems to be about the very opposite….


RA: Oh, I wouldn't say that. And ‘transformation' is such a generic term.


LW: I'd call it a philosophical term, like truth, or justice….very difficult to define but very important.


RA: To me it's generic. Because anything you make is the transformation of something else. When I make a pencil line on a piece of paper I transform the paper. So my question is how to transform it. I think where we really fundamentally differ is that I'm extremely suspicious about anything that is considered new. That when I try to make something original, something authentic. Not new—new alone is not good enough for me. So maybe in that respect my suspicion of the new forces me to always go back to the origins of things. I'm not interested in inventing any new formal condition that is not somehow coming from the guts, I mean the guts of history, the entire history of mankind.


I just got a book from a friend, by a rather unknown German philosopher Blumenbach, and this book is called “Cave Exits”. And what is incredible is that it goes to the origins of human habitation, which is found space, not made space. This found space was the cave, which provided shelter and security—outside the cave was the unknown world, an unknown horizon, the danger of wild animals, weather and climate. I was always more interested in the cave as a threshold between the unknown world outside and the protected world the cave provided. Crossing this threshold triggered all the cave drawings, which for me were the beginning of journalism, not art.  The guys who had the courage to go out there—not only courage because they were sometimes forced no?—but they needed courage to hunt—it was extremely dangerous. And when they came back home into the safety of the cave, they recorded it and they drew their experiences on the wall like bragging. They were bragging for having killed the lion. That's why those drawings had the authenticity of reporting, of reporting real things.


LW: In the outside world today, I think architecture has to be not the safe and known but the unknown space. Architecture has to provide the new territory, the new place to go hunting or to find something authentic because the outside world has become so surveilled, controlled, predictable. In my work, I have been trying to create unknown spaces.


RA: I know. But let me tell you something. To me is nothing more unknown than the economy that dictates the whole strategy of survival. It's a mystery. How this world functions is a mystery. In a way, one is forced to return to the cave, which is your home, where you start to dream up a world that is not the world outside but one that you create. So in that respect it is to me doing exactly the same thing as long ago: you retreat to a place of safety. The world out there is much more dangerous than the world where you have to fight a lion or any other wild animal because the enemy, or the adversary or other tribes or animals were known then. You could look an enemy in the eye, but now the enemy is unknown, completely unknown. The world out there is more dangerous than the older world, so the cave is still has to exist. And we must always maneuver the threshold we are forced almost to cross. We are forced to survive.


So, when you say you want to create new spaces, I also want to create new spaces but I want to challenge something that has been known for millennia and to make something new with that challenge. You wrote once a very beautiful piece about working, in my book about the archetype. That is really what the archetype is all about. It means it is I'm not at all interested in history—it doesn't effect my work. I never think about history. I never think about any kind of influence.


LW: I would never think you did.


RA: But on the other hand, you and I, and I'm not talking academically, we are loaded with information about history, because of our compassion for architecture in a way forces us to look at books and to look at photographs….


LW: And also it is inspiring to see what other people have done in the past.


RA: But then on the other hand, now, by going to La Tourette, in spite of the fact that we are maybe one generation apart….


LW: Well not really, there's a seven-year difference, hardly a third of a generation.


RA: Well, we now have made the journey, which one would normally make as a student of eighteen or twenty. But on the other hand, arriving now, the first time, at one of the seminal buildings of modern architecture, I'm sure that we had completely different insights and different experience than we would have had when we were students. For me it was interesting going to Ronchamp, where I went exactly 50 years ago. When I was a student it very exciting, I was very enthusiastic. This time I was a bit disappointed.  I think that kind of return to things through your own experiences in life is more important to reflect on than the intellectualized academic methodology of a historical critique.


LW: I want to get back to your idea about the authentic. OK? For me, what is authentic is that which comes from within yourself—not something you adapt or adopt from outside sources like apiece of clothing you buy at a shop, or an intellectual shop like a school where you can buy all kinds of philosophies and styles. The authentic is something that emerges from within, from your assimilated experiences. Do you have a different idea?


RA: No—but you also have authentic kitsch. Authentic bad art, authentic bad books, bad architecture. There is this authenticity that is important, or let's say a personal integrity that keeps you from consciously borrowing from influences, and you create a work that you haven't seen before and hopefully nobody else has either. If that is your intention, then the whole work becomes authentic.


LW: Then the intention is important?


RA: Yes.


LW: You know the essential post-modern theory: the death of the author—in the end, the author is not important. He makes something and puts it out there and people make of it what they will. The author's intention doesn't matter—it's people's interpretations that count.  

RA: But that is not post-modern.


LW: Well it is post-structural.


RA: It's deconstructivist. Which was very cleverly used—but we are changing subjects….  

LW: No. It's all about authenticity.


RA: I know.


LW: Post-structuralists like Barthes and Derrida would say that there is no authenticity. Everything is adapted from something else, everything has fluid meaning. You nail it down here, then it jumps up there as something else. Nothing has a single origin. You talk about origination, but origins are always multiple, hybrid, and very ambiguous.


RA: Architecture is a discipline which is defined by limits, just like any other discipline. While one may try always tries to break through those limits, they cannout be circumvented. They have to be confronted.  I think that for me this reality is very critical. I can only challenge the limits of my discipline if I know them. So that is the problem: when a drawing or a construct breaks through the limits and becomes something else, or breaks through the limits and remains architecture, it is because the limits of the discipline are known and confronted. There are many borderlines that are important to explore. There are borderlines that we cannot say clearly carry architectural conditions or structural conditions—they are hybrid in a way. One of your questions here is about the social significance of architecture—that is one of the limits challenged by any formal architectural condition, a pragmatic one. It has to be true. The architectural form is confronted with the inhabitant. Whatever habitation means, however broadly you interpret it, it is still a very archaic condition. The body of the inhabitant is a very archaic condition.


LW: Ok, but what about the mind of the inhabitant, that's something that we don't really know. You're saying that the pragmatic condition is connected with the physical.


RA: The physical dimension of the body of the inhabitant, because the mind—you cannot separate the mind from the body.


LW: Well, Descartes did and a few other philosophers—Berkeley and Hume—have separated them and said that the mind is one thing and the body is something else.


RA: Well, Mach was the first one who very clearly stated that architectural space is only conceivable through the collision between geometry, which is infinite and a pure invention of the mind, and the senses which are limited and bound by the limits of the body. If you just go with one or the other you end with a physical world or a mental world. Any mental world can be very easily manipulated, much more than a physical one.


LW: Well, “manipulation” can be creative. I would say that the ‘malleability' of the mental world is what makes it's the ground for creativity, more than the physical. But also the mental can be a pretty rigorous zone. Mach was operating in the realm of mathematics so there were certain rules.


RA: Einstein, by comparison, was extremely primitive in his philosophical position to recognize the authority of geometry in the mind space. Einstein was only interested in proving his relativity as applicable to the physical world: that ultimately he challenged that there is a straight line.  But mach said there is a straight line it just can't be written. So the program of geometry is actually text. When you draw it, when you physicalize it, you already violate it because you can never draw what you can write. You can never draw a tangent point. No way. It will look right, you can use the most precise instrument and if you would look at it through a microscope it would never be a point, it would always merge the two lines.


LW: Related to that I have a book of Einstein's collected papers. His first papers on relativity which was about electro-magnetism and you can see that half the paper is in words. It's very interesting that it's not purely mathematical—much of it is written concepts and then the math is there to formalize it as a precise practical tool.


RA: To ultimately discover that even a ray of light can be bent, right?


LW: Newton did that, but Einstein introduced the idea that space itself is bent….


RA: …and then say that there is no straight line is primitive. I can describe a straight line; therefore it exists.


LW: You can say, like Euclid, that it's the shortest distance between two points.


RA: No no, a point traveling in one direction infinitely and that is infinitely small. The point has no dimension. The line has no dimension.  What I'm saying—it's very important—is when you draw you are fully aware of that….


LW: OK, it's about the limits of what can be drawn. For me this impacts the question of the architect's desire to realize a design that is built by others. Architect's have ideas that are already compromised when he draws them, then compromised again by the builders….


RA: That is a very critical point.


LW: Architects make drawings that cannot be built in a sense because they are purely mathematical. You see?


RA: When I draw a line, as an architect, I always anticipate that a line can become an edge—a paper edge, wood edge, stone edge, concrete edge. So when you draw a line you have to anticipate the physical possibilities the line could become. That's the difference between the computer line which has no dimension. This line, I can, probably if I anticipate the steel line, I would have the weight in my hands. But coming back to the argument of someone else building it…that is one of the critical limits of architecture in comparison to other disciplines. Can you imagine a writer, a musician, a cinematographer, all those who I believe are closer to architecture than a sculptor or painter…there is a difference between a film maker and Hollywood—the film maker doesn't have the three hundred names you read at the end of a Hollywood film. The same way that we make architecture—instead of having three hundred people who make the drawings, we still make them ourselves.


LW: This is becoming rare….


RA: And if we could build what we drew it would be a different kind of architecture.


LW: Maybe that will be possible some day, with some new building technology. But it's true that all those others who are necessary to build provide a kind of social filter our ideas have to pass through to become part of the social—shared—landscape. I suppose what I'm also  driving at also is that the drawings we make, the ideas we have are transformed by the people who build it. They don't make exactly what we draw, let alone what we think—something is added. They add something as they're building it. What they add is important, in a wider sense….


RA: Yes, but let me tell you something. With the building I'm building in Germany [the Musikerhaus, in Hombroich], I decided what formwork to use to cast the concrete. I decided everything. I cannot control the quality, but I control exactly the materials, the materiality the builder is going to use. So it all depends on that. La Tourette—when you look at the materiality of it, that is the power of the building.


LW: Control. Control is a key issue here. Transformations usually reduce control because they introduce the unpredictable, things that weren't planned. The installation in Paris that I made with Alexis [Rochas] is a good example. I went into that with a clear idea and made a series of design drawings. But the scale and complexity of the construction and the idea of sudden collapse and transformation—-I called it “The Fall”—made it impossible for me to design or control the ultimate form. That's when my collaboration with Alexis began, first in a series of discussions and study models and a full-size mock-up of part of it in a Brooklyn warehouse. But then we realized that the only thing we could design was the method of constructing it in Paris. Once the installation began there, I turned it over to him and a team of three or four workers he ‘trained' on the spot. My point here is, that I gave up control, in order for my design to be transformed by a real collaboration, not just by workers carrying out a pre-conceived design. The result was new kinds of space, and also new ways of making space.  

RA: Well, on the other hand, you see now the question arises: what does this installation do besides just being an interesting structural object?  Whatever Alexis contributed was more in it's understanding of how the material—aluminum?— behaves.


LW: He transformed the design in order to realize my concept of unpredictable space, spaces of ‘the fall.' The tectonics were lines—bent aluminum tubes—edges, limits, the minimum for defining spaces…


RA: Which you also did in Cooper [“The Storm” installation].


LW: That was a much simpler example.


RA: Yes, but the lines….


LW: Yeah the lines were the thing.


RA: So, let's confront it. Let's look at my installations. My first installation was the black box, *Hyperspace*. All my installations were spaces that came to life with participation, not only with participation of physical bodies, but the physical bodies had a direct impact by interacting with that space in transforming it. I remember I went to your installation in Vienna [the “System Wien” installation] and I tried to walk through it and there was this girl who was there, the watchdog, who said you cannot do that—you cannot go there. So it was not meant to be inhabited. There was no strategy of moving through—it was accidental if you could sneak in and take a look.


LW: No—the way through it wasn't designed.


RA: It didn't invite you to participate.


LW: No, it challenged you to participate. A lot of people did—you could see their footprints on the black floor! I don't think new kinds of spaces are ever easy to enter, to inhabit. You have to be inventive to do it. That's the kind of participation I want to provoke.


Also there was the other thing, an interesting difference. In that your installations the inhabitants were the visitors to it, in a sense, taking what you offered. The inhabitants of the Vienna installation were the ones who were constantly remaking it. A team of constructors, builders—they were dancers by trade— would take those tubes out around Vienna—this went on for four months—and place them in different arrangements on different sites, temporarily, then bring them back to the gallery and rearrange them. They were completely unsupervised—they understood the method and the goal. It might serve as a micro-model of how to create and recreate a city…


RA: You have to think about how much this moving around really transformed them. On what level did it transform the space?


LW: Well, in the gallery space it was more like a series of variations, not a radical transformation.


RA: But we have to be very honest now and precise.


LW: Well I'm trying to be.


RA: Well, what I'm saying is that it was a different mechanical and spatial concept than at Cooper. There, you had lines both drawn and built. It was an installation to look at, not to participate in.


LW: That is true, at Cooper.


RA: So.  Referring to is my *Hyperspace* in New York and my *Zero-Zones.* in Copenhagen—where I had electronic devices that triggered sounds through the proximity of the participants. There was a total transformation of space. If you figured out what the strategy was, you could make a symphony there. And in Scotland it was reversed, you had a very controlled movement. There were corridors, and you also passed through electronic beams that also triggered different things. So, what I'm saying is that these installations created the radical spaces where the inhabitant is not just tolerated by the space but creates the space by interacting with it.


LW: That wasn't what my installations were about.


RA: I'm not making a value judgment, I just want to show what the differences are, what the differences are so that…


LW: Ok and I accept that but let me tell you. I think that's very wonderful distinction you're making. What I was after was something different. My goal in Paris and Vienna were to make a statement about how space comes into being. So I was more interesting in exposing the way elements are manipulated to make a certain kind of space. I was interested in the process, the way that the design is transformed creatively by those directly involved in the making of it, not so much with the others—visitors—interacting with it.


RA: So to what extent does this space remain an architectural space and when does it becomes just space. Not everything is architectural space. I believe architectural space has to be space in the most radical philosophical sense: a confrontation with the inhabitant. Otherwise it is not architecture. It becomes culture; it becomes whatever other new discipline that you invent, but it is a demonstration of spatial ideas, but I was never interested in only that. I have always in mind that on the most trivial level of making architecture, you have to think about the place where you take a shit, you have to think about the place that you cook, so if this now remains an indigenous condition, you say ok, now I make a space but I don't care where the office is or where the toilet is, or you start to become aware of the toilet in the exits of the Empire State Building is urban design. So you can make other, so called trivial, I don't think there are trivial activities, that's why I am always so amazed in the banalization of program that you have living rooms and bedrooms and this room and that room. What is the living room, you don't live in the bedroom. When I take a bath, I'm not trying to clean myself, but to feel water and think about other things. That's the difference between a shower and bath.


LW: I like showers! But, no, I don't know who the inhabitant is, who the inhabitants will be in the future. I can't presume to know. Architects are always presuming they know, talking about the inhabitants like they are puppets they can somehow control with their designs. I have more respect for people, I suppose—they will make of spaces what they will. My job to construct space that has a particular kind of order that those who come to inhabit them will or won't choose to come to terms with, physically and mentally. My challenge is  not to control the inhabitants, if there are any—it is to create the order, the structure of space.  

RA: But that is actually untrue. We all have our own history in our work; we all started somewhere. We weren't born visionaries. At some point we discovered that we were going to make architecture. So, when I think about your Sarajevo projects and my favorite houses you designed there, it had lots to do with locations. Without location there is no architecture. The ‘high houses,' even though they are formally different, are much  closer to the work I'm doing than your latest, or your work with earthquake. When I look at these. it is definitely new, authentic, but what I'm missing there is the confrontation, the being anchored in the recognition of limits in the discipline we are working with.  Let's say that's not any challenge to the limits, you've jumped over them, you are now free.  You're in free space.


LW: I use that term.


RA: I know that.


——————————-


RA: Let's look now at your latest work and mine. I'm not talking about the drawings I'm making, but about my built work. That's where my focus is now. I'm not building with my hands, but I am still building in my thoughts, in what I draw to solve questions about materials, details . So, but when you consider the Musikerhaus in comparison to the spaces you are trying to create or investigate, it seems rather reactionary. And it is true, my building is very reactionary. It is symmetrical which is almost a sacrilege today. And yet it is an interesting symmetry because actually it's dictated by the triangle—the equilateral triangle—and there is almost no way to violate it—it's almost like a magical tranquility.


LW: Well what's interesting about the equilateral triangle and your project is that the building is intended for four musicians. Right?


RA: Right.


LW: So, a triangle has three sides and three corners, so for four musicians it is something of a perverse contradiction.


RA: It by far supersedes the space of the inhabitants, which is the space of music. When I made it it was pure formal instinct.  

LW: I can't get into your motives because I'm not in your head, but it seems to me that you challenged yourself with that triangle in some way.


RA: Well, yes.


LW: I mean it wasn't that the triangle is symbolic.


RA: No, no.


LW: It was like the triangle is there and you have to deal with it.


RA: A triangle in the circle and then you deal with it. One of my assistants started to work on the computer drawings and said, “It's amazing—it designs itself.”  And it is absolutely true, because the geometrical consequence of responding to the triangle is that it imprints itself in other things. It's geometric power it forces you to recognize certain things like the axis, but on the other hand, the axial symmetry in the plan is almost immediately turned into a circular symmetry when you enter. The center starts to rotate.


LW: But it's a very interesting because the idea that the building designs itself once this particular geometry is imposed means that….


RA: The design becomes insignificant.


LW: The inherent properties that are in place create the consequences and you just have to deal with those—the ultimate form of determinism.


RA: I also believe that architecture has to be an intervention. It violates the site. Whatever the site may be, it always violates the site in drawing a concept into physical reality. So in a way the triangle is an intervention into the cylinder. Now the design is simply an attempt to reconcile that violation. So whatever the conditions are—a hole in the ground—ultimately the principle of architecture is simply that—a reconciliation. A hole in the ground, or a mound. Coming back to geometry as the space of the mind and the senses as the limits of the physical space, you could say the grave-digger and the surveyor were the first architects.


LW: How so the surveyor, by imposing geometry?


RA: Imposing a geometry upon an infinitely amorphous world.  The surveyor imposes the idealized. And the grave-digger digs the hole that makes the mound. So, the mound became the pyramids and the hole in the ground is ultimately, any violation of the earth. That's why the Indians [Native Americans] never violated the earth because the earth was sacred. The teepee is a movable structure and they only built permanent structures on rock. So there is something about all this, this mystique about architecture as violence, in a way. It is essential to define your own sensibilities: if this becomes part of your convictions and sensibilities than you would never consider any piece of land a building site. The origins of architecture are rather abstract, but they are always present. If I don't confront myself with them, then I'm not interested.


———————————————————————-


RA: I think the physical world of architecture—so-called built architecture, as it existed for thousands of years—has had to do with making structures which would survive entropy. When technology was by far more primitive than today, the architect had to know much more about conditions of reality. He had to know what stone to use that would last longer.


LW: Vitruvius wrote about exactly that.


RA: The notion of building for eternity—that's what permanency has meant—but this is not what permanent means for me. Permanent for me is the desire to create the impossibility. The impossibility of nothing lasts forever. Nothing. So, with that in mind, I understood all of a sudden the fragility of Le Corbusier's architecture—because he did not build for eternity, he built for the moment.  And that is the incredible thing, that despite the denial of eternity, and even in its state of cotemporary decay, it created incredible power. It's very hard to describe where its power comes from. It was a very different building when it was raining, than when the sun came out. It was the most physiological experience I ever had in a building. Ever.


There is also another fragility at la Tourette. The details were made almost crudely by hand, by his own hands. To put glass in thin concrete, and imprecise metal frames with some caulking, he was creating an obligation for the inhabitants—the monks—to take care of this building, like watering a plant that will otherwise die. If you have a material like caulking that's not made for eternity, you have to regularly fill the cracks; you have to repaint certain things; you have to keep the building in this fragile state of impermanency.


LW: At the edge—at the limit.


RA: At the limit. That was the limit that you see. There are many other limits in this building…..


LW:  ….to keep it at the limit and not let it go completely over the edge into ruin. The first day we were there we said it was a ruin. But the second day we, it wasn't any longer a ruin.


RA: It is almost like a structure of decay. The structure is revealed in its decay. That's why, in the ruins of a Greek temple you can find the little capital of a column, and you can reconstruct the column because—in this fragment—the intelligence of the whole structure is imprinted.


(December 12, 2007)


Footnote:


---



[[1]](#_ftnref1) 11 December 2007


Raimund: these questions are closely related (and interrelated) to the impetus behind and aspirations for my own work. I'm trying hard to imagine that I don't already know your answers to these particular questions.


I eagerly anticipate reading your questions for tomorrow.


Lebbeus


Does architecture have a role to play in the social, political, technological changes affecting contemporary society?


At La Tourette, you said (as I recall) that my work celebrated violence and destruction. I don't agree. I think our experiences–especially those tainted by trauma–change us and that the architecture we inhabit should actively enable us to change.


Should architecture have an activist role, that is, instigate changes that only the architect believes are necessary?


What is the role of the client in determining a work of architecture? What I'm after here is how much is the program determined by the client?


You have often said that architecture has to challenge a program of inhabitation. Who says what the program to be challenged is?


When we talked at La Tourette you said that architecture should be made to last? Does that mean that it should embody only enduring values, ideas?


Why shouldn't all architecture be temporary, connected only with ‘the quick of life?' As Sant'Elia said, shouldn't each generation build its own city?


Do you believe in the ‘iconic building?'


*More to follow, when other tapes are transcribed, eventually.*





 ## Comments 
1. [Lebbeus Woods Corb Pilgrimage | CribcandyThe Best from Household and Interior Design Blogs Around the World, Every Day](http://cribcandy.com/home/picks/lebbeus-woods-corb-pilgrimage/80300/)
11.7.11 / 3pm


[…] prick – the architecture of Corbusier is sometimes just sublime. Lebbeus explores. Visit site » lebbeuswoods.wordpress.com comment […]
3. [metamechanics](http://metamechanics.wordpress.com)
11.8.11 / 1am


Lebbeus – I hope at some point you go on the offensive in the dialogue (or maybe that isn't your style?)


Its a good conversation (all conversations to date) with reference to ‘approach' as an architect. I'd guess you are more interested with ‘becoming' and verbs existenially and RA with ‘solutions' and nouns categorically to address ‘becoming' indirrectly (Nietzsche vs Kant).


You point out and RA dances around a point made by Henri Lefebvre in Cities (I think) that basically states the obvious – the architects intentions are not obvious or even true to the inhabtants.


RA's last point about the greek column is very much incorrect even by his logic with an intent on origins. A cave is natural a capital is not. Whether Fibonacci or Golden Means there is nothing natural about a capital, it is a mathematcial figment of our imagination and not our senses like a cave would be. Husserl makes it clear that math (the basic language of all sciences legitmacy) is based on vision. And Deleuze (furthering much of Husserls thought) states in Difference and Repetition that repetition, true repetition exists only in the imagination…in other words math is imagination, so the whole straight line arguement, arguement about limits of the human imagination…a tangent is an imagined occurance in nature…when you draw you realize it was your imagination that invented and understood the concept. To be honest RA lost me a bit in his discussion and transition to architecture about this.


With all that said, what ‘approach' to architecture should be practised and taught?


I'm going to throw this out there based on my academic experience, but ‘becoming' in the sense you speak of Lebbeus has been the chosen method. I'd argue this is one of the main reason our profession is financially unsuccessful. We spend 3-6 years teaching how to ‘become' a designer instead of design.


The alternate being we teach solutions, nouns, and categories…classical design. You could shorten the design education to a year. Create engineers of the ‘beautiful' as per imagined rules that seem to capture the imagination of the laymen when viewed.


Practice based on the American Vignola.


But none of want to do that, otherwise we'd all studied engineering right?


The interest in ‘becoming' and the desire to allow things to ‘become' I'd suggest is the nature of the creative and architecture appears to remain in the sphere of the creative…even RA's approach is interested in the ‘becoming' of materials.


So with that said, I have arrived (becoming of a thought)…why not skip the whole ‘concept' bit of architecure and battle with architecure on RA's terms, with materials, origins of senses, and pretend we build it with out hands. Skip program (clients, code, zoning does that now), skip pretty drawings, skip psychology, and just imagine we were building with our hands to the solutions already provided? More building technology in study and less talk and pretty dwgs. More construction drawings and less schematics.





	1. [metamechanics](http://metamechanics.wordpress.com)
	11.8.11 / 1am
	
	
	Apologize for spelling and grammer (blackberry) unless multiple spellings I think its Iannis Xenakis
	
	
	
	
	
		1. [lebbeuswoods](http://www.lebbeuswoods.net)
		11.8.11 / 10pm
		
		
		metamechanics: You're right. It's corrected. Thanks.
	3. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.8.11 / 10pm
	
	
	metamechanics: RA was a difficult man to go on an offensive against. It would take a while to explain why that was so. Let's just say that he didn't like it, however often he advocated the dialectic.  
	
	Your erudition is inspiring, especially for one who advocates such a hands-on, direct approach. If the world were a different place today, I would very likely advocate the same approach. It harks back to the guild system of the Middle Ages, when ‘the cathedrals were white.' O, the cathedrals! O, towns that grew round them! Our system of exchange today is so complex and getting more so, that one day soon the architect's role will be confined almost entirely to the conceptual. Of course, the exceptions will always be most welcome—in the same way as your comments on these posts.
	
	
	
	
	
		1. [metamechanics](http://metamechanics.wordpress.com)
		11.9.11 / 1am
		
		
		Lebbeus, I am finding on my own and working with those who practice the way they like to – you have to be the architect the world wants you to be to make money (paper pushing conceptualist) and then take that money made and invest in the way you'd like to practice…unless of course you already have money (your parents)
		
		
		I propose a future, a fourth wave (one more than Toffler? Third Wave)…first there was agriculture, second industry, third information, and fourth – agriculture information age (archispeak – agriformational?!?)
		
		
		This isn't far off…take the farming in brooklyn on roofs for instance. Is that really the best way to treat high price real estate and best way to spend our overly educated minds – I'd argue absolutely yes. Friend of the family had sheep in missouri, made wool scarves, majority of sales – skandanavians. Local agriculture – internet sales. Another example – friends/family cross breading lillies on small plot of farm – internet sales. The local craftsmen like the specialist farmer has a worldwide web to promote their unique product.
		
		
		The cathederal days will return in the sense you mention, maybe by end century. Industry can only provide so much for human existence. Why would a person join the monastary at la tourette today? 
		
		
		As a joke I told my brother the CIA probably operates in pencil and paper these days, if you want to codify anything do it analog, you know carve a stick with a message…point is, machines and the digital will only serve us as much as we want them to, there is no “matrix” scenario and there won't be. 
		
		
		RA's desire to imagine building I think will come around again, once the glorified architects task of pushing paper becomes automated and perfomed by computers or the laymen using a 99 dollar software package purchased at Staples and plans delivered to Home Depot and house assembled like Ikea by your kids in 2 days….
		
		
		Look forward to more excerpts from you anti-journey
		3. Roger Broome
		11.15.11 / 9pm
		
		
		Lebbeus,
		
		
		I'm glad that you have inserted this observation about the difficulty of confronting Abraham on one's own terms. I have read your descriptions and transcriptions of this trip with great interest. 
		
		
		Much of Raimund's personality and many of his statements still resonate with me more than 20 years after having him as a professor, and yet, I am also left with a stinging awareness of his petty power games and meanness towards students and other faculty. This was personalized when I was in my 4th year at Cooper with you, Raimund, Todd Williams and Diane Lewis as our professors. I along with 8 other students entered the Lyceum competition. Raimund insisted that we submit our entries to him for vetting and then abruptly told me that he had decided that my entry would not be included with the school's submissions. When I asked him with what authority he was intervening in a competition over which he had no jurisdiction, he curtly said that it was his “privilege” as faculty. I went to the post office immediately and sent in my entry with a short note explaining that I was sending it on my own behalf (Raimund insisted that photocopies of Cooper's entries be sent in lieu of originals and submitted them as a group). When the results were announced, Yuval Gluska from our class won 3rd place and I received an honorable mention; the first time in the history of the competition that Cooper had not come away with 1st and 2nd prizes. Shortly thereafter, Monica Shapiro told me that Raimund was “furious” with me over my insubordination. Interestingly, he didn't have the courage to confront me himself.
		
		
		Years later I told this story to the founder of the Lyceum competition, Jon McKee, who told me he was deeply saddened to hear about Raimund's intervention and that it was entirely contrary to the intent and spirit of the competition.
		
		
		Precisely because of Raimund's over-sized personalty, boisterous manner and insistence on uncompromising standards (his own, of course), I believe it is important to incorporate an understanding of his severe limitations into a portrait of him. And I don't mean the simply difficult and rude behavior; we should also include the episodes of his actively blocking open discourse and of simply being a bully.
		5. [lebbeuswoods](http://www.lebbeuswoods.net)
		11.19.11 / 3pm
		
		
		Roger Broome: actually, I remember that semester quite well, as it was one of my first teaching at Cooper. John Hejduk wanted to test me by throwing me into ‘the lion's den', or so I think. It was a very tough time, with many conflicts between faculty. One unpleasant exchange between Raimund and Todd Williams, on a review, resulted in Todd not returning to teach at Cooper—a real loss to the school.  
		
		As I recall the competition fiasco, your description is accurate.
5. -------------
11.9.11 / 6pm


The talk of the roots of architecture and the differentiation of ‘man' to the unknown outside world through the analogy of the cave works in discussions concerning architecture as a physical distinction between the subject and the other. However, what about for urban design? Can the roots of the urban be understood through the relationship between a series of caves or teepee's? Does that make the understanding of the urban roots merely a series of Caves or varying buildings? Is there a difference between “the outside, inside and the area between caves or buildings, a common space? Nowadays where is the line? When does surveillance and control begin and end? Does it merely depend on the presence of other? Or the presence of a threat or danger?  

If one was to examine the cave or series of caves as a precursor to architecture and the city, what needs to be analyzed? Does materiality only apply to the individual cave? What happens to the issue of materials and who is building when applied to a bigger scale?  

Perhaps a whole community or several individuals, several families lived within one cave, the cave ceases to become a piece of architecture but becomes a building block/town, city?  

If one was to analyze a series of caves, the line between unknown and known, outside and inside becomes diluted… Where is the in and out? What does it depend on? What becomes the threat?
