
---
title: ANTI—Journey to Architecture  Dialogue of work
date: 2011-11-18 00:00:00
---

# ANTI—Journey to Architecture: Dialogue of work


*This is the last of the serialized installments of the book documenting the journey made by Raimund Abraham and me in October of 2007.*


Raimund and I agreed that the most telling way we spoke to each other was in our work. So, we devoted a closing section of ANTI to a Dialogue of Work, comprised of alternating double-spreads between his projects and mine. This is presented below.


An index of works is at the end.


LW


.


[![](media/r-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-1.jpg)


## RA-1


.


[![](media/l-1-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-1-2.jpg)


## LW-1,2


.


[![](media/r-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-2.jpg)


## RA-2


.


[![](media/l-3-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-3.jpg)


## LW-3


.


[![](media/r-3-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-3-5.jpg)


## RA-3, 4, 5


.


[![](media/l-456.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-456.jpg)


## LW-4,5,6


.


[![](media/r-678.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-678.jpg)


## RA-6, 7, 8


.


[![](media/l-789.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-789.jpg)


## LW-7,8,9


.


[![](media/r.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r.jpg)


## RA-9, 10


.


[![](media/l-10-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-10.jpg)


## **LW-10**


.


[![](media/r-11-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-11-12.jpg)


## RA-11, 12, 13


.


[![](media/l-1112.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-1112.jpg)


## **LW-11, 12**


.


[![](media/r-1415.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-1415.jpg)


## RA-14, 15, 16, 17


.


[![](media/l-13-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-13.jpg)


## **LW-13**


.


[![](media/r-16.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-16.jpg)


## RA-18, 19


.


[![](media/l-1415.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-1415.jpg)


## **LW-14, 15**


.


[![](media/r-20211.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-20211.jpg)


## RA-20,21


.


[![](media/l-1617.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-1617.jpg)


## **LW-16, 17**


.


[![](media/r-2223.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-2223.jpg)


## RA-22, 23


.


[![](media/l-1819.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-1819.jpg)


## **LW-18,19**


.


[![](media/r-2425.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-2425.jpg)


## RA-24, 25


.


[![](media/l-202122.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-202122.jpg)


## **LW-20,21,22**


.


[![](media/r-2627.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-2627.jpg)


## RA-26, 27


.


[![](media/l-23-24-25-26.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-23-24-25-26.jpg)


## **LW-23, 24, 25, 26**


.


[![](media/r-2829.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-2829.jpg)


## RA-28,29


.


[![](media/l-272829.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-272829.jpg)


## **LW-27, 28, 29**


.


[![](media/r-3031.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-3031.jpg)


## RA-30,31


.


[![](media/l-30-33.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-30-33.jpg)


## **LW-30, 31, 32**


.


[![](media/r-34351.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-34351.jpg)


## RA- 34, 35


.


[![](media/l-34.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-34.jpg)


## **LW-34**


.


[![](media/r-363738.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-363738.jpg)


## RA-37, 38


.


[![](media/l-3637.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-3637.jpg)


## **LW-35, 36, 37**


.


[![](media/r-wtc.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-wtc.jpg)


## RA-39, 40, 41


.


[![](media/l-3839.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-3839.jpg)


## **LW-38,39**


.


[![](media/r1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r1.jpg)


## RA-42, 43, 44, 45


.


[![](media/l-40-41-42.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-40-41-42.jpg)


## **LW-40, 41, 42**


.


[![](media/r-4849.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-4849.jpg)


## RA-46,47


.


[![](media/l-43-44-45.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-43-44-45.jpg)


## **LW-43, 44, 45**


.


[![](media/r-4647.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-4647.jpg)


## RA-48, 49


.


[![](media/l-46.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-46.jpg)


## **LW-46**


.


[![](media/r-505152-53.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-505152-53.jpg)


## RA-50, 51, 52, 53


.


[![](media/l.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l.jpg)


## **LW-47**


.


[![](media/r-54-56.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-54-56.jpg)


## R-54,55


.


[![](media/l-100t.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-100t.jpg)


## **LW-48, 49, 50**


.


[![](media/r-mhaus-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/r-mhaus-2.jpg)


## RA-56,57,58,59, 60


.


[![](media/l-5152.jpg)](https://lebbeuswoods.files.wordpress.com/2011/11/l-5152.jpg)


## **LW-51,52**


.


## INDEX OF WORKS:


***RAIMUND ABRAHAM***



***1 Metropolitan Core  1963*** 



***2 Universal City 1966***



***3 Glacier City 1964*** 



***4  Urban Interchange***



***5 Transplantation II 1967***



***Universal House 1967***


*6 Exterior view* 


*7 Interior View*



***8 Air Ocean City 1966***



***Black Box 1968***


*9 5pm December 10th* 


*10 5pm December 11th*



***Zero-Zones 1969***


*11 Axonometric projection* 


*12 Partial elevation* 


*13 Sequence of passage*



***Hyperspace 1969***


*14 Axonometric, audio zones* 


*15 Axonometric, instruments* 


*16-17 Transitional Passage* 



***Hinge-Chair 1970,71***


*18 Sequence of kinetic transformation* 


*19 Chair with model*



***20 Earth-Cloud House 1970***



***21 House without Rooms 1974***



***Seven Gates to Eden 1976***


*22 Super-imposed plans*


*23 Title page (catalog)*



***Monument to Aviation 1979***


*24 Horizontal projection* 


*25 Model, perspective*



***Hospital, Nine Projects for Venice 1980***


*26 House of Hope*



***27 House with Curtains 1975***



***28 Time Square Tower 1984***



***29 Tower of Wisdom (Nine Projects for Venice) 1980***



***30 Church on the Berlin Wall 1982***



***31 IBA-Berlin Residential Block 1980***



***Austrian Cultural Forum 2000 (completed)***


*34 Urban landscape* 


*35 In construction* 


*36-37 Section elevations* 


*38 Perspective (built)*



***Ground Zero 2001***


*39 Elevations*


*40 Interior Perspective*


*41 Aerial View*



***Stargazer, Installation in SCI-ARC, 2003***


*42 Gallery view* 


*43 Sketch*


*44 Plan of towers* 


*45 Interior View*



***Jin Bao Project, Beijing 2005***


*46 Exterior skin (built)*


*47 Perspective (digital model)*


*48 Elevation south*


*49 Elevation east*



***50-52 Loci Utlimi 2000***



***53 Inner City 2007***



***Music House 2009***


*54 South view (raw construction)* 


*55 Plan*


*56 East view* 


*57 Music library, Interior*


*58 Northwest View*


*59 Plan*


*60 Details*





***LEBBEUS WOODS***



***Centricity 1987*** 


*1 Upper chamber, Geomechanical Tower* 


*2 Biomechanical structures*



***DMZ (Korean De-militarized Zone) 1988*** 


*3 View of Landform construction* 


*4  View of Watch Tower* 


*5   Horizontal section below Landform* 


*6   View below Landform*



***Solohouse 1988*** 


*7 Exterior view of house*


*8 Exterior view of interior*


*9 Exterior view of house*



***Berlin Free Zone 1990***


*10 vertical section through Freespace structure*


*11 View inside Freespace structure* 


*12  Plan of Mitte, showing Freespace structures*



***War and Architecture 1993***


*13 View of fragment field*


*14-15 Meditation*



***Bosnia Free State***


*16 Views of meta-structural wall* 


*17 Views of meta-structural wall*



***Meta-institute, Havana 1995***


*18 View of rally in the Plaza of the Revolution (c. 1992)*


*19 View of the Meta-institute*



***Old City, Havana 1995***


*20 View of new Urban Walls* 


*21 View of new Urban Walls* 


*22 Sections and plans, showing Urban walls*



***Malecón, Havana 1995  with Ekkehard Rehfeld***


*23-24 Beach-seawall lowered*


*25-26 Beach-seawall raised*



***Electrical Management Building, Sarajevo 1996***


*27 Reconstructed building*


*28 Elevation*


*29 War-damaged building*



***High Houses, Sarajevo 1996***


*30 Elevation*


*31 War-damaged site*


*32 View of High Houses*



***Terrain 1999 with Dwayne Oyler***


*34 View of seismic landscape*



***Excavations 2001***


*35 View of excavated landscape*


*36-37 Notebook studies*



***The Fall, Paris 2002, with Alexis Rochas***


*38 Exterior view of construction, with reflections*


*39 Interior view*



***World Center, New York 2002***


*40 View from Hudson River*


*41 Vertical section through The Ascent*


*42 Exterior view of The Ascent*


*43 View of The Ascent* 


*44 View of the Museum at The Summit* 


*45 View of The Summit*



***System Wien, Vienna 2005 with Christoph a. Kumpusch*** 


*46 View of construction*



***Utopx 2006***


*47 Utopian field drawing*



***100 Towers 2007***


*48 Tower vectors*


*49 Tower elevation/section* 


*50 Tower elevation/section*



***Formations 2008***


*51 Horizon formation* 


*52 Horizon formation* 




 ## Comments 
1. [rafaeloshouldbe](http://gravatar.com/rafaeloshouldbe)
11.18.11 / 8pm


I am scared! 


Thank you,  

Pedro





	1. Pedro Esteban
	11.28.11 / 5am
	
	
	Where Abraham wants to go?  
	
	All his work have a sense of direction, is not movement or dynamism, is direction, similar to give a path to somewhere, but where?  
	
	Universal City, is the most clear example, but when you see the constructed work(quite hard to separate the constructed work of the drawings) this becomes more intense, more evident; you have to go there…
	
	
	I cannot read the dialogues between you for now, but I can feel what are you talking about wtih this post.  
	
	Perhaps Abraham is giving the salvation to what Woods evokes.
	
	
	
	
	
		1. Pedro Esteban
		11.28.11 / 5am
		
		
		In The Austrian Cultural Forum, you have to move, to have to go somewhere, you have to explore, is not a static sensation of space
3. [K.S](http://otherscapes.wordpress.com)
11.21.11 / 9pm


intensely engaging work.
5. [Ugljesa Janjic](http://www.ugljesajanjic.com)
11.23.11 / 3am


Incredibly strong impressions. The abstract has a power to open doors into our inner selves. Love it. Thank you very much for sharing.
7. [ANTI-Journey to Architecture « Imagination](http://archimagination.wordpress.com/2011/11/24/anti-journey-to-architecture/)
11.24.11 / 1am


[…] Finale Seite: <https://lebbeuswoods.wordpress.com/2011/11/18/anti-journey-to-architecture-dialogue-of-work/> […]
9. Sally Bowles
12.24.11 / 7pm


Lebb,


How much cash did you get from the 12 Monkeys lawsuit that's shown in LW 1,2? Did the lawyer get his usual cut? A windfall like that sure buys nice real estate.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	12.25.11 / 11am
	
	
	Sally Bowles: Whatever I received, if any, in terms of cash, certainly would not have gone to “buy” a piece of the planet. There are many things more important things than ‘ownership.'
11. brad
4.12.12 / 4pm


can you elaborate on the photos of raimund's black box? i'm even more intrigued by the difficulty of finding more info on it.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.12.12 / 4pm
	
	
	brad: Sorry, but I cannot. I assume you're referring to the room with the torn up books. His mention of the installation is all I know. You might look into his book (un)built (first and second editions).
