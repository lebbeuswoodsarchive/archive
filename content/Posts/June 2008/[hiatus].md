
---
title: hiatus
date: 2008-06-21 00:00:00
---

# [hiatus]


After nearly eight months of blogging, I'm going to take a break. It's been an intense experience for me, largely because of a number of engaged and articulate people who have taken the time to read my posts and respond to the ideas and opinions they express. They established a critical dialogue, often among themselves, that greatly expanded the meaning and value of the original posts, and I am very grateful to them.


I intend to return to posting, perhaps later this summer, or even sooner—we'll see. In the meantime the blog will be open as an archive. I will check in from time to time, in order to respond to any comments that might come in.


I wish you all a good summer, in whatever terms you define it.


LW



 ## Comments 
1. [David Burns](http://so-ad.com)
6.24.08 / 7am


Dear Lebbeus,


I think I speak for a lot of people when I say, “Hurry back.” 


Your presence is needed now more than ever and this blog has been a much needed source of common sense and critical clarity that has been sorely missing in our community. 


Thank you for the time you've invested in this blog and for sharing your thoughts with us.


Sincerely,


David
3. river
7.5.08 / 6am


I concur! Meanwhile [here's](http://www.treehouses.com/treehouse/construction/engineering.html) a cool link.\g curriculum.
5. river
8.13.08 / 5am


[check it, yo](http://www.engadget.com/2008/08/08/cern-rap-video-about-the-large-hadron-collider-creates-a-black-h/)
7. [Roxingyoursox](http://roxingyoursox.wordpress.com)
8.22.08 / 2pm


This will be a very sad time, when you aren't blogging away! Very much of people's research will have to be postponed for a while. I will be waiting until your next post.


Waiting,


Roxingyoursox
9. [Dag](http://www.studarch.com)
9.13.08 / 10pm


When im reading here i really understand that i dont know shit about architecture, but im still trying..  

fantastic blog, i wish you could be professor at my school aswell Lebbeus
