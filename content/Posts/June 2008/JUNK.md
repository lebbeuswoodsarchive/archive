
---
title: JUNK
date: 2008-06-01 00:00:00
---

# JUNK



[![Junk 3](media/Junk_3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/lwblog-junk3.jpg)[![Junk 2](media/Junk_2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/lwblog-junk2.jpg)[![Junk 1](media/Junk_1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/lwblog-junk1.jpg)


Junk piles. I have always been fascinated by them, ever since, as a student, I was taken by a drawing teacher to some railroad yards in central Illinois. There were many things to draw in the landscape of tracks, roundhouses, sidetracked train cars (including long-empty living cars for railway workers), old and empty brick utility sheds, and the monumental cylindrical grain elevators lined up along the tracks, all of which had fallen into some state of disuse or abandonment, for reasons that were never clear. The flat, treeless plain on which these wonders lay was heavy with a feeling of something lost, but not dead, something living if only as a mystery. Still, on the sunny day we first went to the yards, everything was sharp and clear and unburdened. We had come to make freehand drawings on our large drawing pads, in the fresh Spring air, and to spend an hour or two exploring the world of forms that we might find. Later, these drawings in pencil, charcoal, or ink would be slipped into our portfolios, judged and graded as a course requirement. On that first day, however, they were pure fun.


I was immediately drawn to the large metal scrap piles that lay near the tracks. Looking back on that attraction, I can understand it as a reflection of sensibilities deeply felt. Complexity. Richness of form. Accident. Undesigned design (they are entirely of human origin). The extraordinary in the ordinary. But also, the mystery of objects, in our apprehension of them, and in their origins, their originality (every scrap pile is different, even if, at first glance, they all look the same). How does their sameness/difference call the reality of memory into question? And, it occurred to me then and still occurs to me, what that I cannot see is inside the junk pile? Are there different things piled inside, or only more of what is outside? In either case, what does light and darkness matter to them? Then, of course, there is the question of space. Space is created by objects, between and around them. What is the nature of these spaces? Do they have any meaning? For sure, they are a-systematic—-or are they the products of a new system? Or, are they just meaningless accidents, the products of a randomness that human beings cannot devise, except (as Paul Virilio claims) indirectly, by creating the technological products that end up as scrap thrown into piles along railroad tracks? Is this, in fact, their meaning for us? Clearly, the scrap pile is loaded with meaning as the detritus of human striving, but is its meaning something more? Is it a model for a human future, in both negative and positive senses? Are they piles simply waiting to be recovered and converted into new objects, or, can we somehow inhabit them, and why would we?


None of my drawings from that first visit, and subsequent ones, survive. At least not in their original form. Looking back, I have to think that they—and the questions they provoked—have echoed down and through a lifetime of work.


LW




 ## Comments 
1. egrec
6.7.08 / 1pm


This is one of the joys of a Lebbeus Woods blog — these personal and philosophical reflections on formative influences and inspirations; something perhaps more difficult to go into in, say, a monograph or a speech. And yes, the photos that accompany this post do very much recall aspects of projects like Sarajevo, San Francisco, Berlin. There is beauty in the light on these heaps of forms, the darkness between them, and the questions they raise.


Also — holy shit — he said “fun”!
3. Adam
8.25.08 / 3pm


junk piles you say?


I hope you receive this.


<http://www.chrisjordan.com/>
5. [Not out of the Woods yet « ubiwar . conflict in n dimensions](http://ubiwar.com/2008/06/03/lebbeus-woods-virilio-accident/)
8.28.09 / 2pm


[…] Having mentioned Paul Virilio and the Accident yesterday, I was pleased to see that Woods also had Virilio in mind when he decided to blog Junk: […]
7. [citizenjoe1](http://gravatar.com/citizenjoe1)
11.19.11 / 6pm


The photo on the right, is that yours? Was it someone elses? I wanted to use it for background on a self published music CD and wanted your permission.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.19.11 / 7pm
	
	
	citizenjoe1: The photos are from the internet, uncredited. Take your chances.
