
---
title: (NOT) UNDER CONSTRUCTION
date: 2008-06-06 00:00:00
---

# (NOT) UNDER CONSTRUCTION



[![](media/vb-moss2.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/vb-moss2.jpg)[![](media/vb-moss1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/vb-moss1.jpg)[![](media/vb-moss3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/06/vb-moss3.jpg)


The American Pavilion in Venice's Giardini, site of the Biennale, is a piece of neo-classical design that strikes an odd note, especially among other national pavilions speaking more eloquently of modernist aspirations. America chooses to represent itself with a faux-historical architecture that frames whatever is exhibited with disingenuousness. The message it sends would be merely absurd were it not for the baleful effect of disingenuous American foreign policy in recent years.


All that has had a chance to change in this year's Architecture Biennale. Under the overall theme of “Out There: Architecture Beyond Building,” the design for the installation at the American Pavilion was addressed by Eric Owen Moss in a remarkably ingenious and straightforward design. Using ordinary construction scaffolding, he creates a lyrically diaphanous structure that serves as a carrier for two- and three-dimensional representations of a diverse selection of architectural projects, including my own. Evoking the ideas of transition and the transitory, he calls the installation “Under Construction: American architecture around the world; international architecture in America.”


As Moss writes in a recent communiqué, “the proposal was as much a public policy argument as it was a conceptual design pro forma. The intention was to subvert, to the extent possible, the current perception of American behavior and American prospects around the world, using the international design discourse as the essential remodeling tool.” However political in intent, it is nevertheless upbeat: America can change and can become a critical, constructive force in the world. And it is whole-heartedly architectural: an innovative spatial intervention with the simplest (and cheapest) of materials. The message it sends is provocative, multilayered, but instantly intelligible. “Architecture at its best,” Moss writes, “ suggests that the world can be other than it is, that the world progresses and that architecture can facilitate that progress: New prospects for the city. New visions for design and building. A new built world for the inhabitants of a new world. Hope and optimism. That's the promise.”


Last week, the U.S. Department of State, which controls the American Pavilion, rejected Moss' proposal.


LW


 [biennale\_proposal-2](https://lebbeuswoods.files.wordpress.com/2008/06/biennale_proposal-2.pdf)


[pungent commentary](http://geniusoftheplace.blogspot.com/2008/06/straw-man-in-venice.html)


 




 ## Comments 
1. [lebbeuswoods](https://lebbeuswoods.wordpress.com)
6.13.08 / 12pm


[comment deleted in error]  

betadinesutures:  

Perhaps “change, hope and optimism” relate too much to a certain presidential candidate?
3. Maria Fueyo
6.22.08 / 7pm


Since when these three words can be attributed only to a “candidate”?  

Without change there is no progress.  

Without optimism there is no encouragement.  

And without hope…well, turn off the lights and close the door!
5. Darrin Harvey
7.1.08 / 8pm


There is nothing wrong with hope and change, but this merely rhetoric – how do we achieve this hope and change – this is what we must focus on. 


Mr. Woods is correct – this relates too much to a certain candidate – by the way where is the consistency and clarity to this “candidate's plan” – by the way I can't accept retread political philosophy's as “new” and “audacious hope” 


Maria, If I may respond, are you stating that there has been no progress, and only this “change” will bring progress?


Look around, I am not sure where you live, but the global economy and all its complexities are a result of much progress – there will always be difficulties and setbacks and a certain presidential candidate will not cure your, or all the world's ills.


By, the way, great to see your site Lebbeus – I met you at the 1992 LAX/SCI-ARC and I will never forget my experience there and meeting you. I have always been moved, and influenced, by your drawings and insights – what a gift you have…..


Darrin Harvey  

University of Houston 1993  

Houston, TX
