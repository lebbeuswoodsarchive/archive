
---
title: KRASOJEVIC UNBOUND
date: 2012-02-21 00:00:00 
tags: 
    - architectural_drawing
    - Margot_Krasojevic
    - visionary_architecture
---

# KRASOJEVIC UNBOUND


[![](media/mk-8a1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-8a1.jpg)


In a recent post I spoke of drawing “what architecture could be.” Since the days when I first set out to do that with hand-made drawings, the digital computer has become commonplace and has enabled many architects with similar ambitions to create provocative images that 1) could never be hand-drawn; 2) speak a visual language previously unknown; and 3) at least in theory promise to tie into a world of construction that is increasingly digitally controlled, making their actualization possible, if not probable.  In a sense, the only thing that stands in the way of building many astonishing digital images is the question, “Why?”


The images of Margot Krasojevic are for me as compelling as most I have encountered in the current flood. What sets them apart is not their style or character—they have the same anonymous, mechanical look and feel as most other digitally produced drawings—and not their spatial accessibility—we never get inside the depicted forms, another common feature of such drawings. Rather, the appeal of her drawings is found in their playful yet refined tectonic language, which offers us big, generalized forms, and a flurry of fragments that either inhabit the big forms, or move in opposition to them. It is a dialectical approach that she handles in a very architectural way, embracing several different scales and speaking of the necessity for architecture to assert its presence in even the smallest details.


Yet—the question persists—“why?” Could we inhabit these designs, if they were constructed? Are they meant to be constructed—and inhabited—at all, or might she intend another purpose for them? We can only speculate about possible answers.


It's intriguing to imagine that in some future time, when not only technology but also human conceptions of how to live have evolved far beyond what we now know, and that buildings such as Kraselovic's might become commonplace. In such a world, the social and political and perhaps also the economic relationships between people and what we would call a wider nature would be very different from today, though in ways we can only infer from the drawings. What the drawings suggest is an isolation in which things do not interact with each other very much, but are self-contained and self-sustaining. Each building is a world unto itself, a domain of individuality. Or so I see it.


Somehow, though, I do not think the architect sees her designs as belonging to a distant future. Her texts make no reference of this kind and seem much more focused on her concern with formal details of technique and method—how she made the drawings more than why. Back at square one, we can only conclude that the drawings, and what they depict, are intended for the present, one that shares her concerns and projects architecture as essentially self-referential—“art for art's sake.” As such, architecture severs its conscious ties with other fields of knowledge, from social science to psychology to literature, aiming for a pure expression of its essence—the design of space. This perspective can be and has been justified in countless modernist arguments by the need for existential integrity: each thing must be true to itself, before it can come into a ‘true' relationship with other things. A powerful but problematic perspective, to be sure.


In the 20th century, modernism and its counterpart, existentialism, played themselves out against a backdrop of war, totalitarianism, social disintegration, and personal alienation. The “death of God” announced by Friedrich Nietzsche at the end of the 19th century was really the birth of a wholly anthropocentric society. Modern technologies were merely the enablers of a historically new form of freedom, defined in a famous pop song as “having nothing left to lose.” Post-modernism in philosophy, art and architecture emerged as a supposed antidote to the toxic and deadly consequences of the demise of moral values that had, for many centuries, given human relationships cohesion in spite of their many contradictions. In a sense, the digital computer has been the ultimate liberator, making anything possible while remaining totally amoral. The digital computer (there is also the analogue computer, underused and misundertood today) makes the work of Krasojevic possible, but does not help us make any sense of it. To do that, we have the choice of going by consensus—whatever is fashionable—or interpreting it in private terms.


This says more about our contemporary state of affairs than the works of Krasojevic. I have no doubt that she presents them in the expectation that others will understand her intentions. And well they might. The constructed elements of a new world she has drawn glimpses of reach out to those intrepid souls who know that not only the future but also the present demand our imaginative participation in continually reshaping what we already know. The answer to “why”—draw it or build it—becomes obvious: we really have no other choice.


LW


*The following images are from an article called **Dimensions of Concept—Illusions: The Fractal Tower:***


[![](media/mk-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-1a.jpg)


.


[![](media/mk-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-2a.jpg)


.


[![](media/mk-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-5a.jpg)


.


[![](media/mk-6a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-6a.jpg)


.


[![](media/mk-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-7a.jpg)


.


[![](media/mk-3a-det.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/mk-3a-det.jpg)


.


For further reading about this architect's thought and work, this book is an excellent resource: <http://www.amazon.com/Spatial-Pathology-Floating-Realities-RIEAeuropa-Book-/dp/3211715339/ref=sr_1_1?ie=UTF8&qid=1330098462&sr=8-1>


#architectural_drawing #Margot_Krasojevic #visionary_architecture
 ## Comments 
1. [durganand balsavar](http://www.artesindia.com)
2.23.12 / 5pm


The autonomous artefact, despite powerful, struggles in its dilematic condition, in the sense that it depends and engages in the perception of the human species. Several ancient cultures surmised an evolution of a species beyond “homo sapien”. While one does not dispute the fact that the pace and scale of change and technology is synchronic with a sense of alienation and isolation, most discources of the autonomy of the ‘architectural artefact' rarely take account of the continual evolution (and futurity) of the species itself and its “consciousness' and perceptions. “Modernism” is a state of mind. The krasojevic spatiality creates another mythology of existence no different from the existence of unicorns or minotaurs and ancient supernatural elements, wherein the ritual of the ‘familial' is reasserted in the alienated and the desolate landscape with an ambivalent sense of future as well as a nostalgia of a loss of relationships.
3. Rothko George holden manz
2.23.12 / 8pm


congratulation to dr.krasso for putting into words and compelling imagery a concept that surely allows for a possibility of future environments in line with a clearly holistic and yet engaging and technological and architectural feasible and refreshingly possible frame work and mindset.  

like many visionaries she will have to stay willing, strong and consistently adaptable to proof her worth and (re-)write spatial history for our time count beyond history as we knew it.  

we can all but wish for dr. Krasso to be able to spread this spatiality beyond all borders – thanks so much for sharing your insight and inspire with your work(s) and words. rg
5. Hamis
2.24.12 / 10am


I am very pleased to see Dr. Margot Krasojevic's work getting more exposure.  

Her work exists in magical realm of mixed disciplines from which  

she exposes us to architectural possibilites that ignites our conscious  

to question the possibilites that exist beyond our current understanding  

of space,tectonics and forces that allows us to arrive to  

architectural forms. In her work , particulary her drawings, lies a delicate  

balbalance that intrigues and draws us to a realm, where we can only comphrend her work  

with the aid of our imagination after isolating ourselves from branded symbolism  

that have come to dominate our understanding of the present.
7. [Ana D'Apuzzo](http://www.anadapuzzo.com)
2.25.12 / 6pm


As member of the editorial board of DaNS, a technical journal for architects, I am honored that the projects of Dr. Krasojevic will be presented and discussed in the June issue of DaNS. Her different way of thinking and her new architectural visions represent one of the many innovations which can contribute to expand the borders of current architectural thought and it‘s manifestation.  

Radical ideas of some of the greatest architects needed time in order to be recognized. The works of Zaha Hadid were, for example, considered “paper architecture” for many years until finally, after a lot of energy spent believing in and fighting for her projects, she could find her place alongside the greatest contemporary architects.  

One of the most beautiful building I have ever seen, the Disney Concert Hall in Los Angeles, project by Frank Gehry, is also an other example. We should remember all the verbal and written attacks that the architect endured and all the negative descriptions of his work as cold and non-human. By contrast, the reality showed how functional and beautiful the project truly is and his valuable contribution to our society.


It is important to recognize and support advanced individuals in their creative process and give a large space of freedom to new ideas.  

I would also like to add that I am grateful to live in a modern time with the advantage of having new possibilities offered by new technologies. I am convinced that talented contemporary architects exploit the computer mouse as an additional tool, without replacing the pencil. Digital and classic projecting tools compliment each other whilst modern technology allows for the presentation of new ideas to a larger public forum in a more accessible way, in the process opening opportunities for public dialogues.
