
---
title: WHY I BECAME AN ARCHITECT—Part 2
date: 2012-02-08 00:00:00 
tags: 
    - architecture
    - memoir
    - motivation
---

# WHY I BECAME AN ARCHITECT—Part 2


[![](media/tools-x.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/tools-x.jpg)


*(above) Slide from my introductory lecture in ARCHITECTONICS,  First Year Studio at The Cooper Union.*


It is revealing that the phenomenon and experience of light became important to me through art rather than direct experience. After all, the actual world is alive with light and its effects with a vividness that no work of art can equal, but works of art, at least certain ones, pierced my consciousness with the presence of light in ways I had never experienced before. Perhaps it was the focus they gave it by their emphasis on particular qualities, but looking back I think not. Rather, it was the art's celebratory aspect, its exaltation of light, uniting its presence with profoundly important concepts—the struggle to be fully human—that touched me so deeply. Works of art could lift experience out of the commonplace to a realm of meaning that, for me, would otherwise be unreachable.


Certainly, even in my teens I knew people who simply loved life as they found it and needed no exaltation to enhance or ‘elevate' it for them, but I also knew I was not among them. The reasons do not matter to this story. I can only see that I was lucky to have stumbled upon visual art as a transformative medium of my experience, even though, at that young age, I was not at all sure what to do with it. Of course, I would continue to emulate the art that inspired me, but never for a moment imagined I could become a real artist, someone who could devote his life to making art. To make a living someday, I would have to do something really useful that people would be willing to pay for in the world I came from and expected to live out my life in. And I had no particular encouragement to think otherwise.


About the same time, I took a class in my high school called Mechanical Drawing 1. In it I was introduced to the t-square, the triangle, the protractor and the compass. It's possible that I took the course because of dim memories from my early childhood of my father's engineering work, though I cannot recall any now. As distant as these instruments seemed from my awakening passion for art and its celebration of light and lofty concepts, I was immediately attracted to what could be made with them—straight lines, circles, geometric figures. There were also the strict rules that governed their construction, a rigorous order that was precise, if not exactly self-evident. The order of geometry that could actually be made by anyone was its own form of exaltation, a lifting of thought and action out of the messiness of the everyday to a realm of truth, at least a human truth. Once again, my instincts to transcend the ordinary—perhaps escape it is more accurate—were awakened. What this has to say about my personal deficiencies is all too obvious, and best saved for an analyst's couch—but they, too, are part of the story.


From the present, it's easy to see the direction this is leading, but all those years ago it was by no means clear. The marriage of light and geometry does indeed find its consummation in architecture, but for me it did not come about so easily. At age eighteen I entered a fine school of engineering, then transferred to a fine school of architecture, finishing there when I was twenty-four. After ten or so years of working in corporate offices, learning what it meant to build—and leading a rather turbulent life—I went out on my own. Throughout all this time, I continued to make paintings, hoping that in this way I would give worthy form to the questions that had beset me since the days I confronted the easel in the living room of my mother's house—but never doing so. It was not until I was thirty-eight that I began to put the pieces together in drawing my idea of what architecture could be, and made a total commitment. The turbulence, of course, has continued, sometimes on the paper and sometimes off, yet it was only then that I finally did become an architect.


[![](media/lwblog-asp-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/lwblog-asp-3.jpg)


*(above) From the Architecture-Sculpture-Painting series, 1978. All the ingredients are there.*


LW


#architecture #memoir #motivation
 ## Comments 
1. [Stratis Mort](http://arcsickocean.blogspot.com/)
2.11.12 / 9pm


I find both articles ( ‘why i became an architect' ) really interesting and as i have watched a lecture of L. Woods on Youtube ( from European Graduate School EGS ) and being aware of his career and studies, i would really like to know his opinion on Patrick's Schumacher article on the President's Medal Awards ( UK Architecture School awards ) 


( <http://www.architectural-review.com/view/overview/ar-exclusive-schumacher-slams-british-architectural-education/8625659.article> )


and further the whole debate created on this blog ( L. Lambert response to Schumacher)


(<http://thefunambulist.net/2012/02/02/architectural-theories-open-letter-to-mr-patrick-schumacher-yes-architects-are-legitimized-and-competents-to-address-the-political-debate/> ).


Even if i have to disagree with some comments and take a strong position on the fact of how architecture schools are ‘handling' next generation architects, i would be glad to have a short answer of L.Woods on this matter (and maybe anyone interested in this blog) I really appreciate the work that he has created and the topics that discusses on this blog, following him through his books and projects, i feel glad to be able to raise a conversation with him.  

As P. Schumacher claims, architecture schools in the UK (spotting the Bartlett and the AA mostly ) have spread a utopian architecture of non-realism, fantasy speculation and bad approach to architectural elements.


I see architecture as a project itself. No one can define it, but he can declare a specific interest in it, exploring and raising awareness for his interest and the topic he approaches. I appreciate the part of architecture called ‘buildings' , structures and materials, but i concern it as another medium of architecture; not the only one. If architecture concentrates and only aims in building (architecture as profession), it will soon become blind and sooner or later dead.


I would like to wait response to my post and then talk about it more, if desired and possible.  

Thank you.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.12.12 / 11pm
	
	
	Stratis Mort: The links you gave open to us a fascinating debate to consider. I encourage readers of this blog to read what had been written, consider the implications, and respond if they care to. And I thank you for clueing us in.
	
	
	It's unfortunate that most of the commenters don't know how to write—or to think—clearly. As a result, we are forced to wade through long diatribes, filled with repetitions and redundancies, which is not only boring, but also discourages our participation.
	
	
	Schumacher throws down the gauntlet by claiming that the work of students today is not relevant to the pressing social and political challenges of our time. He finds their projects unrelated to reality. For such a young man himself, he sounds like an old fogey, but we know—given his collaboration with Zaha Hadid, and his advocacy of “parametrics”—that he is not, unless we think of these thigs as already out-of-date. Well, come to think of it, they might be. His defensive posture suggests that HE considers them as such.
	
	
	Regarding the projects of the students, his broad judgment is simply wrong. Many of their projects open the doors on a hopeful, if difficult, future. I don't pretend to understand them all, but I do understand their importance. I'll have to leave it at that for now.
3. [Léopold Lambert](http://weaponizedarchitecture.wordpress.com)
2.13.12 / 6am


Thank you for linking the debate Stratis Mort.  

Mr Woods, just as a point of detail, Mr Schumacher is not claiming that those students work ‘is not relevant to the pressing social and political challenges of our time' but much worse that architecture in general cannot take part in those challenges, hence my violent reaction to such discourse which, rather than escaping from the political realms, constitutes a perfect embodiment to the already established relationships of power.  

In any case, you are very welcome to participate to this debate, I know for a fact that a lot of people are still reading the various comments that have been written so far.


Have a good night


Léopold





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.13.12 / 1pm
	
	
	Léopold Lambert—Yes, that outlook is much worse. As you may know, I have based my work over many years on the conviction that architecture is vital to making social and political changes for the better. Most of the leading architects of my generation do not share this conviction. So be it.  
	
	I will continue to follow the debate on the blogs. If there is anything I have to add to what has already been said, I will certainly do so.  
	
	I'm glad you've shown initiative and leadership in carrying this forward.
5. 2002-2007
2.13.12 / 7am


Thank you very much for sharing this. I thank the use of the blog format for allowing the communication of personal thoughts to be expressed in a public manner. This insight would have been wonderful to have had when I was a young architect in your studio, though I understand why it might not have been shared in that context.
7. [On becoming Lebbeus Woods « typologica](http://typologica.com/2012/02/13/on-becoming-lebbeus-woods/)
2.13.12 / 3pm


[…] In other words, to finally be something, one first has to have the courage to become it. … … Lebbeus Woods: Why I Became an Architect—Part 1 Why I Became an Architect—Part 2 […]
9. zale
2.14.12 / 8am


we are so grateful for you becoming an architect ❤ it wouldn't be the same without you, period
11. [Stratis Mort](http://arcsickocean.blogspot.com/)
2.16.12 / 1pm


The combination of these two ‘opposed' architectures, the static one of the buildings, and the abstract, free geometries, is really intriguing for me, to understand the relationship and connection they create. As i was searching the internet yesterday, i found this work, <http://www.thisiscolossal.com/2011/04/suspended-geometric-rope-sculptures/> (suspended geometric ropes) by an artist called Moneyless. It is also interesting how the drawing of Lebbeus becomes materialized, as these are installations made with ropes, though i have to say the drawing and the aspect of 2d is always more inspiring.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.17.12 / 12am
	
	
	Stratis Mort: Wonderful works. Worth a post. Thank you again for your links.
13. [chris](http://metamechanics.wordpress.com)
2.21.12 / 2am


These days I think we're better off calling ourselves “artists” …the profession and academic worlds of architecture have been so ridiculously narrowed to hopeless specialties of professional paper pushing paralegals and academic spectacle conceptual eye candy producers – you'd have more influence on the design envrionment if you just went with “artist”. 


Lebbeus its good to know 38, at 33 I get a bit tired of bouncing between professional and academic architecture and as noted above probably I'd prefer being called an artist (of building). I have a creative to do list now in my desk drawer (about 40 items and counting)..must pay bills though and play the game (occasionally).


I think its a minor issue to figure out you want to be an “architect” and its a far more complicated issue to figure out what kind of “architect” given the available market. 


If I remember correctly Gehry came into his own after 50? Calatrava was close to 50 after ETH phD? Ando young at 40's (granted boxer first) and Philip johnson 9 years at Harvard? Lou Kahn, mid 30's right, coming into his own? Even the young Bjarke Ingles is 37 and he want(ed) to be a cartoonist?


I read a lot of the funambulist stuff and first I would like to note the “old timer” architects (including zaha and rem) would never have the “balls” (small enough ego) to just comment back on a blog. Patrick is right about a lot of stuff including “parametricism”, which is the most obvious thing about architecture for the last few decades and HE gave it a name. No more bullshit (ok bullshit can be a parameter, ha)- there you have it – an architect deals with “parameters”, call your parameters – clients, zoning, fire code, po-mo, deconstruction, pencil and paper representation, etc…all parameters for limiting/narrowing in the moments for creativity or architectural design thought process. Now does it have to happen in 3d computer software? This is the modern day tool.


Before the computer I imagine the great architects were like Shamans who just solved all the parameters of a project with a sketch on a napkin (I predict this will be the future of architecture again)





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.22.12 / 8pm
	
	
	chris: Adopting the name “artist” would only serve to marginalize those of us who strive for some creativity in our field of knowledge and practice. Everyone is ‘an artist.' I have fought for a long time to claim the term ‘architect,' as my profession, though many have wanted to relegate me to ‘artist.' An architect has special responsibilities and social agency. I am not willing to give them up.
	
	
	
	
	
		1. [metamechanics](http://gravatar.com/metamechanics)
		2.23.12 / 4am
		
		
		you're right…but man is it hard to not already feel like you're marginalized as an architect when everything is a commodity (including art) or lawyer driven drivel.
		3. [lebbeuswoods](http://www.lebbeuswoods.net)
		2.23.12 / 2pm
		
		
		metamechanics: Yes. I guess my tendency towards wishful thinking is exposed here! Architecture was once considered a profession serving the good of the whole public. But over the years architects have sold out such principles to political and business interests, so that architecture is now a profession in name only and it is perfectly fine to design in ways that serve only an elite. This is seen in the most celebrated buildings today: stand-alone projects that contribute little or nothing to the cohesion of the whole public space. A younger generation could change this, but will they?
15. [metamechanics](http://gravatar.com/metamechanics)
2.26.12 / 2am


had a longer response regarding your question.


the short answer: dissemination of information.


how does architecture disseminate information though?
17. Anthony Titus
3.16.12 / 7pm


This is one of the most precise and beautiful pieces that I have had the pleasure of reading. It's brief, yet has incredible depth and clarity, and most importantly: it gives us insight into one of the finest architectural minds of out time. Thank you for your generosity//
19. [The Fox Is Black » A few thoughts for Valentine's Day](http://www.thefoxisblack.com/2012/02/14/a-few-thoughts-for-valentines-day/)
3.31.12 / 7pm


[…] The relationship is between Lebbeus Woods and Architecture. In two posts from his personal blog, the architect talks about how his relationship with architecture began. […]
