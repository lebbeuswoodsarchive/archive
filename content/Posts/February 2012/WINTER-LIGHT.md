
---
title: WINTER LIGHT
date: 2012-02-20 00:00:00 
tags: 
    - Ingmar_Bergman
    - Winter_Light
---

# WINTER LIGHT


It is not just the sometimes gloom of New York City in early February that inspires me to write about Ingmar Bergman's film *Winter Light.* In rural Sweden where the story of the film spins itself out in nearly real time, it is not so much gloomy as gray, a soft but relentless half-light that casts no shadows. Inevitably, our focus turns inward, where experiences are lived out in painfully sharp contrasts. The story is a parable of our self-absorption and consequent inability to connect—with each other, with our world, even with ourselves.


And yet, parable is not quite the right word. That somehow belongs to an epic world, far removed from what is for many, a desperately personal world in which morality tales give little comfort and even less help. In *Winter Light,* the only character who deeply connects with the wider world—Jonas, a modest fisherman—kills himself because he has read in the newspaper that the Communist Chinese will soon have the atomic bomb and, “having nothing to lose,” will eventually use it against the West. His sense of life's meaning is destroyed by the prospect of a global apocalypse that negates all human effort. Tomas, the Lutheran pastor whose counsel Jonas seeks in a last effort to save himself, is so caught up in his own confusion about “God's silence,” that he can only “spout senseless drivel” to Jonas, like “We must trust God,” and—when Jonas' desire to end his life comes up—“Life must go on.” To which Jonas replies, “Why?” Tomas has no answer, and we can see that he is probably close to suicide himself. The schoolteacher Marta, a former mistress of Tomas, cares only about him, and her caring—a true, unselfish love—seems in the end to offer a hint of hope and even, perhaps, redemption for them all. As the church organist says to her at the film's conclusion, “God is love, and love is God; love is a real force for mankind—see, I know the drill!”


There is another character—Algot, a disabled railroad worker who “receives a pittance” for helping in the church—who is central to the story. Near the end he speaks to Tomas about reading the Gospels describing Christ's Passion. “Don't you agree,” he says, “that the emphasis on Christ's physical suffering is all wrong?” What he suffered mentally must have been far worse, Algot says, and especially his fear, at the moment of his death from crucifixion, that God had abandoned him—“Father, why have you forsaken me?” he cried out. God's silence.


All of this sounds very much about religion, but it is not. Ingmar Bergman, the son of a strict Lutheran pastor, was indeed working through personal issues in this film, but he was too much of the present, and too much an artist, to limit his work to such a narrow frame. The film acknowledges the limits of any religion in an age when people have been left on their own to find their ways through the existential maze. The very human questions it so clearly puts before us are more important than any readymade answers it could possibly give.


[![](media/wl-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/wl-1.jpg)


*(above) Tomas (Gunnar Bjornstrand) and Jonas (Max von Sydow), discussing Jonas' crisis.*


[![](media/wl-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/wl-2.jpg)


*(above) Tomas, self-absorbed.*


[![](media/wl-6.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/wl-6.jpg)


*(above) Marta (Ingrid Thulin), in an intense exchange with Tomas.*


[![](media/wl-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/wl-4.jpg)


*(above) Tomas and Marta.*


[![](media/wl-5.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/wl-5.jpg)


(above) Tomas and Marta, outside his church near the film's end.


LW


#Ingmar_Bergman #Winter_Light
 ## Comments 
1. [Individuos opacos](http://gravatar.com/opacos)
2.20.12 / 12pm


It's terrible the scene where Pastor Tomas offers the body of Christ, saying: The blood that Christ shed for you.





	1. [Individuos opacos](http://gravatar.com/opacos)
	2.20.12 / 12pm
	
	
	I`m sorry this the good translation of the scene: the blood of christ, shed for thee.
	3. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.20.12 / 3pm
	
	
	individuos opacos: The Holy Communion, which is offered in the ‘high' Lutheran Swedish (Protestant) church, really indicates the pagan roots of Christianity. Human sacrifice to prove the fealty to a Divine Being, followed by eating the flesh and drinking the blood of the sacrificed victim. For the most part, though, Protestantism abolished the practice.
	
	
	
	
	
		1. [Individuos opacos](http://gravatar.com/opacos)
		2.21.12 / 11am
		
		
		In the movie “Offret”, Tarkovsky expresses the sacrifice from an opposing view to Winter Light. Curiously Tarkovsky filmed in Sweden with Bergman´s working group a story in which the sacrifice was the door to the common good and redemption, a person chooses to kill himself because he believes that this act will save his loved ones, I think that this act is more than a suicide.
