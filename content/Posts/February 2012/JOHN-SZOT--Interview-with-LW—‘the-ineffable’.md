
---
title: JOHN SZOT  Interview with LW—‘the ineffable’
date: 2012-02-26 00:00:00
---

# JOHN SZOT: Interview with LW—‘the ineffable'


*[![](media/lowman1a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/lowman1a.jpg)*


*(above) The World Trade Center, Lower Manhattan, after the attacks of September 11, 2001.* 


The following interview was published in [the journal MAS CONTEXT](http://www.mascontext.com/issues/12-aberration-winter-11/):


**JOHN SZOT:** *Looking back at your major building proposals – for Sarajevo, Berlin, Havana, San Francisco, and New York City – in each case there is an intense form of tension stemming from conflict, be it natural disaster, political turmoil, or otherwise. These conditions set the stage for radical intervention by identifying the ineffable as* a *pre-existing* *condition. However, in [‘Terrible Beauty 2'](https://lebbeuswoods.wordpress.com/2010/07/24/terrible-beauty-2-the-ineffable-2/) you seem to advocate pursuing the ineffable**as a legitimate, independent architectural enterprise. Does this mark a shift in the trajectory of your work?*


**LEBBEUS WOODS:** I believe that in our era, architects have a responsibility to face the historically unprecedented problems confronting people everywhere today. Not only that but they have been entrusted with the social agency for doing so, by virtue of their education, special knowledge and skills, and being legally empowered to assume design stewardship over the human environment. If architects don't take on this job, who will? Politicians? Engineers? Builders? Commercial corporations? They need to help, certainly, but I have always believed that architects, with their commitment to the total situation, should be the leaders in the reconstruction of human landscapes transformed by violence and dominated by the ineffable, and therefore must take the initiative. That's what leaders do, not waiting for someone else to do so and then call them in.


**JS:** *In your essay you establish a strong case for engaging the ineffable despite the difficulties it may present. Although you have a compelling moral argument for doing so, you also note that the intrinsic nature of the ineffable conflicts with expectations placed on the architect. Would you care to elaborate further on the difference between the values that embrace the ineffable and those that guide the way we approach building design, or is it merely about control?*


**LW:** No, it's not about control; it's about the strength and validity of ideas. I base my way of thinking on the presumption that really good architects will have the best ideas for reconstruction than other specialists, again, because of their comprehensive understanding of environments and their abilities to give this understanding concrete form. Of course, they don't do this in isolation, but working with others. That's what I meant in [a recent manifesto of education](https://lebbeuswoods.wordpress.com/2011/06/06/manifesto-the-reality-of-ideals/) when I declared that “the era of the collaborative genius must begin.” The architect must be the collaborative genius who has the best ideas about how to structure the work process involving others leading to the designs for reconstruction.


I don't want to spend the time or space here on elaborating why so few architects have acted decisively as leaders in the continual reconstruction of cities—damaged or not—up to now. Rather, I am convinced that we must concentrate our energy on changing architect's attitudes now and into the future, beginning in the schools of architecture.


***JS:*** *Many of the other creative disciplines have explored for decades the kind of trauma and anxiety associated with the ineffable. Some might argue that media with a deep virtual dimension (like film) are effective (and safe) surrogates for the ineffable experience. What is your position?*


**LW:** Films are fine media for what Freud called ‘sublimating' dreadful feelings, so their energy can be transformed into something useful, positive, even creative. But the rechanneling of emotional energy often only relieves and diffuses personal suffering, unless it is actively put to creative use. Vicarious experiences and voyeurism can lead to passivity and the acceptance of unacceptable norms, far more easily and often than inspiring creative actions for changing them.


The more hopeful outcome of films and other virtual experiences is addressed by Aristotle in his *Poetics,* which explains his theory of Greek theater. In it, he states that the events and characters we see on the stage create models for our thoughts and actions in the world we actually inhabit. The aesthetical worlds of the theater have, in short, a moral and an ethical dimension in relation to the ineffable—they give us help in how to live our often difficult lives.  In the same way, the imaginary worlds of architectural design—and especially so-called ‘visionary' architecture—offer us similar models for everyday living. We're free to follow them or not, or to adapt them to our purposes.


***JS:*** *Today one can easily see a connection between the network culture of social media and the heterarchical organizational strategies you described 20 years ago. However, it is debatable whether our buildings have risen to the challenges presented by this use of technology. Your work emphasizes the relationship between buildings and physical phenomena – do you believe there is a connection between your concept of heterarchical space and the social dynamic of a ‘wired' society, or does architecture's preoccupation with the physical world make for an entirely different set of obligations and opportunities?*


My work definitely aims to explore the consequences of freedom and choice, often by technological means. In my *Berlin Free-Zone* project, the center of the city is regenerated not by traditionally hierarchical urban planning methods but by spontaneous architecture without predetermined purpose and meaning, evolving through the unpredictable exchanges by electronic means of people living there.


Our buildings today are still designed according to outdated models, what Paul Rudolph called ‘background' and ‘foreground' types—a few extraordinary ‘masterpieces' designed by star architects, set against an urban fabric of what Robert Venturi called “the ugly and the ordinary.” It is a hierarchical formula that has worked throughout human history, for example in Europe's Middle Ages, when the Cathedral was a brilliant jewel set off against the dark and dense texture of a surrounding town built by and for its ordinary inhabitants. It is easy to understand why this model endures—it is familiar and safe—even though the society it supported has passed. What is needed today are new models that give form to the democratic society struggling to emerge from the older autocratic, oligarchic ones that have dominated history for so long. It is a very difficult struggle because even the concepts of democracy inherited from the past are outmoded and must be reinvented for the present and coming age of technological revolution. The ‘wired society' and its imperatives for architecture is a good place to begin.


Questions by **John Szot**


Answers by **Lebbeus Woods**


November 14, 2011


*(below) The Berlin Free Zone, 1990, urban plan concept—a moment in time, after the fall of the Berlin Wall on November 9, 1989:*


[![](media/bfz-plan.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/bfz-plan.jpg)




 ## Comments 
1. [Tyler](http://gravatar.com/tylerkvochick)
8.10.12 / 3am


Addressing the points of democratization in the conclusion of this interview, I think the facilities for a “new model” of democratization will be through new methods of making. Currently, we are suffering from a material economy handed down from the Industrial Revolution, i.e. modes of production are controlled through the economic structure of the building industry. This means that the physical means of architecture are restricted to an economic bracket that is far out of reach of the typical consumer and architectural innovation which is realized in physical, constructed form is typically seen in large-scale projects cultural or commercial projects. This, as you point out, is a continuation of the urban model of “landmark” building against a landscape of “ugly and ordinary” buildings. 


However, the work by MIT's Center for Bits and Atoms in the field of digital fabrication has already shown that placing the means of material production in the reach of the populations that are most wanting for them yields incredible ingenuity when objects are fabricated by the people who will use them in the environment where they will be used. The next scale of this will be innovation in the creation of environments. If a consumer wants their house to be (capital A) Architectural, it can be fabricated on-site; eliminating current prohibitive labor and material costs. We're already seeing the nascence of this in experiments with 3D printing walls with concrete. This also allows reduced material cost in that consumers must only buy raw material (concrete) and can print any form they can conceptualize with digital tools. 


So I think it is absolutely right that a ‘wired' society is a good place to start, but the journey that we start is a re-engagement of that digitally-enabled society with their physical world. And this is fundamentally what architecture is about, the cultural creation of built forms. Only now it must become a bottom-up phenomenon rather than top-down. Or, as you call it, the Era of Collaborative Genius.
