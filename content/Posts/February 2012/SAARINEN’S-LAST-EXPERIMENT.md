
---
title: SAARINEN’S LAST EXPERIMENT
date: 2012-02-27 00:00:00 
tags: 
    - architecture
    - Bell_Laboratories
    - Eero_Saarinen
---

# SAARINEN'S LAST EXPERIMENT


[![](media/bell-labs-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/bell-labs-1a.jpg)


Occasionally, history comes back and gives me a slap in the face. The slap in this instance came in the form of [an article on innovation](http://www.nytimes.com/2012/02/26/opinion/sunday/innovation-and-the-bell-labs-miracle.html?hp) that appeared in the New York Times recently. It recounts the all-too-familiar story of how a giant American corporation sponsored some of the most important research in science and technology in the 1950s, 60s, and 70s, establishing the United States as the world leader in innovation, at least in that era. The [Bell Laboratories](http://en.wikipedia.org/wiki/Bell_Labs_Holmdel_Complex), initially located in Holmdel, New Jersey, by the monopolistic American Telephone and Telegraph Company (AT&T), was the epicenter of this story.


But this wasn't the sting in the slap. Rather, it was that the Bell Labs building was designed by Eero Saarinen, whose firm I worked for, beginning two years after the completion of this building in 1962. By then, Saarinen was dead of a brain tumor at age fifty-one, and his practice was in the hands of his partners, Joseph N. Lacy and John Dinkeloo, and his design protégé Kevin Roche. Saarinen's great fame was founded on exciting curvilinear buildings like the Yale Hockey Rink and the TWA Terminal at New York's Idewild (now J. F. Kennedy) Airport. The Bell Labs building was the very opposite, an immense glass box, relentlessly organized on a Cartesian grid.


As an architect, Eero Saarinen was a shameless experimenter. He had no signature style, which might be the reason historians have not treated his work with much interest—they tend to favor trademarked brands, as they are easier to package and sell. I tend to think of him as an expressionist: each of his buildings expresses what he saw as the essence of its function. A hockey rink is about gliding and flow; an airplane terminal is about arrivals and departures and the symbolism they evoke; a research laboratory is about logic. His many other buildings, such as the CBS Headquarters in New York—an unassailable black granite column—express Saarinen's ideas of their very different purposes. The lesson we take away from his work is that one style cannot fit all. We can only wonder at how successful he would have been today.


Did the ultra-rational design of the Bell Labs contribute at all to the innovative work carried on there? From this article it would seem so. Yet, I have some doubts. Does creative thinking in any field have any relationship with the architecture it takes place in? It's a hard case to prove, one way or the other, but architects are continually claiming the connection. In the case of Bell Labs, the argument is made that the layout of the building encouraged interdisciplinary contact that led to the most productive, innovative collaborations. If this is true, what does this tell us about the most celebrated architecture of today, with its eccentric, computer-generated complexities. What about the creativity of everyday life? “Dumb boxes,” as [I have written elsewhere](https://lebbeuswoods.wordpress.com/2008/04/08/dumb-boxes/), may be the wave of the most creative future. Whether yes or no, the sharp sting of Saarinen's Bell Labs building, and the story behind it, lingers on.


We must not avoid the questions it raises, especially those of us who have made considerable effort to explore other ways of thinking.


LW


[![](media/bell-labs-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/bell-labs-4.jpg)


*(above) The long corridors* *were encouraging to interdisciplinary exchange, according to the Times article. Photo from 1966.*


[![](media/bell-labs-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/bell-labs-2.jpg)


#architecture #Bell_Laboratories #Eero_Saarinen
 ## Comments 
1. [Caleb Crawford](http://calebcrawford.wordpress.com)
2.27.12 / 2pm


Bruno Quesane, an historian of architecture, used to describe American balloon framing as democratic building. The plethora of elements privileges none – it is the non-hierarchical assembly that provides the resiliency. The Saarinen Bell Labs, from the photo of the hallway, has much of the same quality, a sense of equality. There is something filled with potential about such “background buildings.” SCI-Arc has always inhabited such spaces. The original buildings at Pratt were designed in such a way that should the school fail, it could be converted to a factory. The open and somewhat anonymous nature of the “architecture” at these schools provided a framework of possibility – the spaces weren't “done,” but in a perpetual state of “becoming.” This corresponds with Stewart Brand's thesis in “How Buildings Learn.” He feels that contemporary architects do not think about the long term, about how buildings adapt and change over time. I recall you once said something like “we should design our buildings and then figure out how to live in them.” Perhaps the Bell Labs offers us a glimpse of that?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.28.12 / 2pm
	
	
	Caleb—you quote me correctly, thank you. What I had in mind is that architecture of imagination and conceptual depth should take the lead in shaping how we live—a controversial idea, for sure, because, for one thing, its sounds elitist. This is opposed to the idea that architecture should be built around how people ‘want' to live. The semi-quotes refer to the reality that people want—or think they want—what they've been taught to want by their parents and other authority figures, and therefore want to reproduce conventions, norms, traditions that the authority figures were themselves taught to want. It's a fine formula for sustaining social stability, but not as democratic as it may seem or pretend to be. So, I am shifting the responsibility to architects to break the old molds and offer people the chance to truly shape their own lives by designing spaces and networks of spaces that “you can't move into with your old furniture.” In short, to offer people a real choice. Architecture is abstract enough to offer many interpretations of use, as you so rightly say.
3. [Caleb Crawford](http://calebcrawford.wordpress.com)
3.2.12 / 3am


I'm sorry, I was being a bit of a devil's advocate. I often use that quote to egg on my students to challenge their pragmatic leanings, even when engaged in overtly pragmatic studio work. 


I understand completely the need to challenge people's assumptions about how they want to live. Consumer culture is one of those received wisdoms that requires challenging. Oddly, we are in a current condition where those cultural norms are producing great instabilities. Yet I can't help feel that a destabilizing element is our loss of connection to history. One of the things that often gives your work the power is the juxtaposition with the known. I think of scenes with conventional furniture, section cuts through familiar building sections, but with unfamiliar interventions. It puts us into a condition where we can question accepted norms. But there is also a kind of eternal return here: the original fabric, which someone figure out how to live in, is then occupied again with another vision of space (yours), which someone else now needs to figure out how to live in. It would be fun to figure out how to move in with my “old furniture.” We also have Borromini's St. Ivo, reached through an anonymous courtyard that seems to bulge and warp – made powerful by that contrast with the older fabric. Thus past and potential coexist in uneasy simultaneity. Saarinen gives us that quiet background into which a Berlin free zone can be inserted. 


There is a fascism embodied in the complete mega work. While I generally support the concept of the completed vision, I also have reservations. There is a totalitarianism – the Egyptian pyramid, a Greek temple, Zaha's work for BMW – a despotic control in service of dominant power regimes.Do we need that abuse to give us beauty? Are we doomed to versions of culture like, as Harry Lime (Orson Wells), in “The Third Man” says: “in Italy for 30 years under the Borgias they had warfare, terror, murder, and bloodshed, but they produced Michelangelo, Leonardo da Vinci, and the Renaissance. In Switzerland they had brotherly love – they had 500 years of democracy and peace, and what did that produce? The cuckoo clock.” ?
5. zale
3.10.12 / 10am


I love Saarinen. And not because my uncle worked for bell labs. I honestly had no idea what his workplace looked like until much later. I guess Saarinen kind of grew on me, maybe because he does have that feel– like no style. Sometimes I think I've always enjoyed him but his name got in the way. I know, silly but I'm still not sure how to pronounce his or really many many architects I love. Nevermind. His work is really becoming. I thoroughly enjoy his experiment. He does feel under appreciated though. It is a shame. He was outstanding in many ways, at least in my eyes. 51, and that's not fair!
