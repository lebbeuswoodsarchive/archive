
---
title: THE NEXT REVOLUTION?
date: 2012-02-29 00:00:00 
tags: 
    - analogue_computers
    - computers
    - computers_in_architecture
    - the_human_brain
---

# THE NEXT REVOLUTION?


In a recent panel discussion with Hernan Diaz Alonso, we exchanged differing opinions about digital computers but agreed on one crucial point: debates about digital computers versus hand drawings are over. The digital computer is not only an established part of architectural practice, but is central to it. This is because it can do things that hand drawing cannot do and in particular facilitate a type of construction ever more prevalent in the building industry. Then our discussion turned in another direction: the future development of computing machines in architecture.


My contention was and remains that there will be a resurgence of [analogue computers](http://en.wikipedia.org/wiki/Analog_computer), with which Hernan did not disagree. The digital revolution is over. While refinements in software and hardware will continue, digital computers have won their central role and will not lose it, short of a collapse of the present civilization. However, they have their limitations and these are already becoming clear. The breaking of the world down into infinitely manipulable bits and bytes leaves a vast empty space in human thought that cannot be filled, or should I say ‘represented' in that way. Without representation in some form thought cannot exist. This is where analogue representation comes into the story.


First of all, what is an analogue? An analogue is something that shares certain qualities with a subject or an object under our consideration but not others. In other words, it is not a literal, ‘virtual' representation of the subject or object, but rather a symbolic one, Analogue thinking is thinking in symbols and produces representations or (a better term) descriptions. The advantage this analogous kind of thinking has over literal descriptions is, in the first place, that it can describe things that have not been known or described before. Types of space, systems of order, even emotions. While this same possibility is often claimed for virtual representations or descriptions, I contend that every virtual invention has a history of models on which it is based. Let me give a small example.


I once asked my students, as an overnight sketch problem, to make a drawing of a being from another, very different world—an alien. The next day, the drawings they showed me looked like fusions of human beings and snakes, beetles, various plants, and so on. The point is, my students—very bright and creative—could only create hybrids of living things they already knew. Part of the reason for this, I concluded, was because they made representational, ‘virtual' drawings. If they had given me a mathematical equation, or a matrix of different colors, they would certainly have been able to make aliens we had never seen before. At the same time their description would have had to be translated—interpreted—to have resulted in a conventional representation or portrait of the alien. Then again, why would we need to render them conventionally?


The answer to this question is: we are used to it. The rendering of what a thing looks like is the way we are accustomed to getting descriptions. The digital computer is so popular and accepted because it specializes in exactly this kind of description. However, it can describe what we don't already know only in very limited ways, usually by montage or collage, that is, by combining things that we do know into descriptions of something we don't. This is a serious limitation when we are exploring the unknown. In that situation the analogue computer is a far superior tool.


The most powerful analogue computer known is the human brain. Consisting of millions of neural nets—electrical circuits—it can compute numerous complex operations simultaneously—not only walking and chewing gum at the same time, but many other involuntary and voluntary body functions while working through subtle emotions and complicated philosophical questions, all at the same time. Minute after minute, day after day, throughout a lifetime. Some two billion neurons comprise the circuits in a nearly infinite number of continually changing interactions. The statistics go on, but the point remains that the analogue computer works by abstract descriptions, not literal ones.


Now, if the world inhabited by human beings could be controlled only by electrical impulses that could command bricks to be moved, concrete to be poured, steel to be made; or crops to be planted and harvested; or laws to be enacted and enforced, then the story could begin and end with the human brain. Perhaps that will be the future direction of human technological evolution on the planet. But until such a time, it will remain for us to interpolate between the analogical and the digital, between  abstract descriptions and the literal representations of things. To a large extent, the task of this kind of interpolation makes up the history of science and art.


The ‘education' of the human brain is an ongoing, increasingly important task. But so is the invention and development of the technological prostheses we require to interpolate, to bridge the gaps between abstractions and representations. With digital computers advancing so rapidly, we have neglected the potentials of their analogue cousins. such as those that would enable:


—slum-dwellers to analyze their own complex communities, the better to organize politically and economically;


—urban planners to understand the continually-changing layerings of human activities within a dense city center;


—architects to incorporate available recyclable materials in the design stages of their projects.


—?


A coming generation of analogue computers will differ from digital computers in many ways, but the most crucial is that they will each be designed and built for a particular situation and task, rather than as a ‘generalized' machine usable for all situations. If we think about it, this follows the example of our brains, which would not serve a cheetah very well. Indeed, my brain would not serve you very well, as it is continually being constructed by my unique life experiences. But this takes us in a direction this post cannot go. It must serve here to say that the analogical might well be and perhaps should be at the center of the next great technological revolution.


LW


.


## WORTH READING: Though not directly related to the above, [a recent article](http://opinionator.blogs.nytimes.com/2012/03/02/why-dont-we-read-about-architecture/?hp) by Allison Arieff about writing architectural criticism raises some worthwhile points.


#analogue_computers #computers #computers_in_architecture #the_human_brain

## Comments 
1. ampexian
3.1.12 / 9am


In many ways the predicament is not about digital vs analogue computing, but rather the profound issue of memory. Currently, the technology and means of calling context and instances into play exists – what seems to be the problem is the meaningful acquisition and storage of data that embodies the symbolic and metaphysical aspects of reality and perception. Sure sensors can read and record data, but they cannot realize the true essence of the subject. We as human beings still do not understand the phenomenon of how our brain collects, translates, and deciphers the multivalency of experience simply due to the fact that it is not something that can be easily (as of now) described quantitatively. If this were possible, data structure and function would be obtained simultaneously and the dichotomy that exists between digital and analogue would collapse ultimately making the argument between the two irrelevant.


3. [Margot krasojevic](http://gravatar.com/krasojevic)
3.1.12 / 9pm


I completely agree that we become accustomed to certain representative qualities, we recognise digital computational processes and we either fall in line with these abbreviations of spatial configuration or we attempt to work with scriptwriters to in some way introduce ideas that translate between human condition, ritual etc. to a tooling system which may reflect the intention. Once again we are faced with the question where in the process of creating new formal tools can we create a symbiotic relationship with appropriation questioning typology and social habit? Digital computation has a strong visual identity which can suggest so much more than really exists, we as designers look for current trends attempting to understand space and the physical world armed with the latest theoretical or process led lexicon that it has now become easier to recognise buidling design through a serious of steps involving keyboard shortcuts which have most definately isolated from what it is that is being explored and why. The anaolgue computer demystifies and I believe makes room for a stumbling over technique, finding out a design approach accidentally, this will be the true mass customisation tool as it won't be separating the designer from the process as is currently the case but it will more of an accessible way of redefining the dialogue between scale, reading space, re-appropriation of typology and an adaptation which works together. I have worked in too many offices whereby the designers don't use computers as effectively as scriptwriters and it is increasingly difficult to communicate between idea intention and outcome when faced with someone who is digitally fluent but does not address issues that designers do, resulting in keeping up with rather than working through criteria.


5. [metamechanics](http://metamechanics.wordpress.com)
3.2.12 / 2am


To think just 5 years ago in grad school I thought we were using scripts to find new ways of describing things…or forcing emergence. Afterall, mathematics digitally expressed and calculated by machines wasn't typical for analysis of design problems – Genetic algortihms, cellular autoamata, john conways game of life (von neumann), etc…


To margots point and woods – drawing is analagous mode of thinking about architecture, one that is most prevalant in classical design and overly obsessed and nearly irrelevant to the reality of architecture in theory by the likes of Peter Eisenman. 


Sketch-up was a halfway software between the computer modeling software of industrial design and hollywood and architectural hand drawings.


I tell my students modeling in the computer is object oriented and not best described by a line analogy but best described by itself – geometry. Don't think of a box as 4 sides, its 8 vetices, a volume you can explore, something that looks different from every angle, effects and is affected by lighting in different ways – you know like reality.


Which is why the napkin sketch is the future again, reality gets boring fast.


What the digital has done and can do is make seeing and experiencing the real very easy. You ask me “I want to know what brushed stainless steel tubes placed on the floor like pick-up sticks in a white gypsum painted room with 4 CFL downlights (13watts each) with 10 foot high ceiling look like using a 35mm camera set to 1/60 second shutter speed” and 1 hour later I email you an image of me standing in the corner looking down thru my 35mm camera. Then you ask me “what about an ant climbing through the tubes who is 6 inches from the opening and only sees things in shade of red?” And 1 hour later I give you that image. Next you challeng me ” ok use this manufacturers light and assume the ant catches fire and explodes into 15 voronoi cut pieces that expand at a certain rate splattering on the walls if the ant is exposed to lighting levels of 5 candelas” and 2 hours later I give you the animation and the candela readings. (I could do all that for you in 3dsMax in time mentioned, not kidding).


I see the digital the same way Edmund Husserl saw the mathematization of everything. Make it data that is measurable and not only can you say it must be true but you can control it. 


Data data data – hence the desire for spectacle.


As good as I may be at translating design intent into digital format, one artist I have done work for always inevatibly kills my computer no matter the hardware or software. She doesn't think in George Boolean language, but like an artist or herself and whatever analogous mode of thought she works in it pushed my “mathematization” of it to its limits.  

In general, if the design students push the desing and don't limit themselves to their skills and software, the design often kicks the computers ass everytime.


Will contine shortly, from my blackberry typing with one thumb as I walk a quarter mile…


7. [Margot krasojevic](http://gravatar.com/krasojevic)
3.2.12 / 5pm


With the excessive interventionist practices heralded as a freedom from formulaic planning/design there is also the need to understand their role within society. Programs, typologies and their manifestation need to be liberated from iconicism as it currently stands, however, I believe it has gone to the extreme…a social network and urban exorcism, the exposure of what was once considered a marginal culture has gone into overdrive whereby thought out reasons for intervention and appropriation from a cultural point have been replaced with the free license granted by projects nurtured by absurdist takes on the built environment, culture jams which exist only because they can, because people who don't belong to this mindset treat it as an interesting research project, not because they necessarily offer any clues or ideas as to a progression of urban life. Culturally we are under the illusion of being set free, for example, we can attribute the success of what seems to be a spontaneous urban intervention such as the Parking Day San Francisco and Dumpster Pools, (spawning a rebound of intervention across U.S.as a result) to the fact that they push the boundaries of programmatic aesthetics, they may challenge the very notion of what an appropriate urban retrofit is, whilst rejecting both design aesthetics and good taste, to subscribe to this has become another following, another social group whose participation accesses a certain borrowed credibitlity. As much as I love to see urban life creating it's own uses, proposals, suggestions, appropriations it also needs to be understood that some trends are an immediate conditioned response rather than a strategy which can defend an idealism or even subsidise a revolution. With these sudden bursts of creative dislocation are we rejecting a longer term understanding of which criteria can be embraced in order to abstract and redefine the essence of typology and ritual? Our attention spans have shortened so much that we may just about uphold the chronic cyclic catalystic nature evident in technology. We are an easily bored nation spurred on by compilation and miniature bottles of perfume, alcohol, samples of life never really committing long enough to one approach to be able to define it enough to understand it's flaws or it's strengths. Nostalgia makes me sad yet the need for change is contagious.



1. [metamechanics](http://metamechanics.wordpress.com)
3.2.12 / 11pm
	
	
Margot the first posters post about memory and meaning may be relevant to your post (although I don't agree with the first posters last post)…but mainly the point on memory and meaning.
	
	
These urban interventions that seem to be hot these days are probably similar to the situationist stuff a few decades ago (guy debord, etc…) But now we have the internet so that we may all experience virtually the experiences of a few physically social people. 
	
	
The people who take too many photos on a trip so that they may post on facebook tend to forget the trip without the photos. When they try to remember the trip they use the photos to reconstruct the experience and perhaps only then understand the meaning of the trip, which may be a totally different meaning then the meaning that could of been created while on the trip.
	
	
This natually leads me to the difference between an analogous computer and a digital one.
	
	
The beauty of the digital is that in theory all possible programs and formula of typologies could be in theory defined by a very basic vocabulary – 1 and 0 (binary, yes and no, black and white). The digital memory of the the defined program or formula can be the “string” of binary data – a number, a word, a symbol of a concept/abstracted pattern. Its a symbol you can put to memory, rote memory, a picture worth a thousand words. Something not requiring time to comprehend once comprehended, no need to work thru just poof and its in your head and you know what it means (technically).
	
	
The analagous requires a sequence in time of reading the symbol and trying your best to form memory and meaning of the reading, often reading the symbol over and over. This analagous reading will ultimately form a memory about experience, not a picture worth a thousand words, but an intuition worth a thousand pictures. The analgous is a digital paradox (a heisenberg kind of thing) the act of running the analogy in ones mind changes the analogy itself, one can never be certain what the analogy could have meant prior to ones involvement. Computing in analagoy requires contrasting meaning and memories by running the analogies together in ones mind looking for moments of meaningful connections which then create the resulting intuitive moments that are stored in life experience.
	
	
The analogous computer requires work and force to work. It cannot produce abtract concepts that can be proven true by measuring them based on some accepted means of determing truth, rather the ontological time sequence journey calculation is the truth regardless of results.
	
	
The digital by its very nature will always be able to define an analogous computation, but first the digital needs to wait and see what the analogous computer churns out.
	
	
Another way to think of this is to randomly choose a tree and then apply concepts about architecure and urban planning to the tree parts. Then try to figure out how the tree would grow based on the applied concepts and when finished look at the and read back to yourself the tress as it would be read by applied architecture and urban concepts. You may have to invent a new concept when you read the tree. In the world of the digital this excercise is like taking to very different math formulas and using the constructed logics of each to work back and forth to form a newly assembled formula. You are taking the “one off” condition as the truth about the subject matters.
	
	
Is the “digitial” a one-off condition of the ananlogy of the visual sense in numbers to the nature of the world? Edmune Husserl I believe was saying this, derrida went on with the origin of geometry, deleuze expanded on husserl, and in architecture we have Delanda. I suggest Delandas philosohpy is not one about the “digital” but rather the “analog”.
	
	
Two thumbs on a blackberry on a bus.
	
	
	
	
	
1. [metamechanics](http://metamechanics.wordpress.com)
3.3.12 / 12am
		
		
Another example
		
		
The poetry of TS Eliot requires massive amounts of analog computing when reading. Each line in a TS Eliot poem is either an allusion, metaphor, analogy, or some other means of referencing more than is visible in text, it requires research and more reading about reading the poems. When done prufrock and other observations means a hell of a lot more than meets the eye. (In school read 6 books of about 200 pages to then right a 10 page paper on prufrock)
		
		
Poetry is analog computing. (Maybe?)

3. [lebbeuswoods](http://www.lebbeuswoods.net)
3.3.12 / 1am
		
		
metamechanics: Absolutely. And it is not only about the ‘use of the brain,' but about the way Eliot uses those abstract symbols we call ‘words.' More on this topic to follow.
		
		
9. [metamechanics](http://gravatar.com/metamechanics)
3.3.12 / 4am


can't wait.


11. [metamechanics](http://gravatar.com/metamechanics)
3.3.12 / 5am


any thoughs on Ezra Pound?





1. [lebbeuswoods](http://www.lebbeuswoods.net)
3.4.12 / 1am
	
	
metamechanics: Pound was Eliot's mentor and critic (see—and actually read—his corrections of The Wasteland, <https://lebbeuswoods.wordpress.com/2010/08/22/by-hand/>) and a founder of Modernist language and imagery, for sure. Oddly, he got trapped by his scholarly but skewed obsessions with Classicism and “money-lending” (“usura”) that derailed his poetry. You might say that he started out as a founder of Modernism, but ended as a prophet of post-Modernism. Have you tried to read his “Cantos?” Tough going. There's no “Prufrock” there.
	
	
13. [Margot krasojevic](http://gravatar.com/krasojevic)
3.3.12 / 2pm


Charles Babbage in the Wasteland, inspiring stuff metamechanics





1. [metamechanics](http://gravatar.com/metamechanics)
3.6.12 / 4am
	
	
while googling for your post (remember the machine not the names) – (should probably go old school library on this one and read the poem?)
	
	
found this so far nonetheless
	
	
<http://existentialistcowboy.blogspot.com/2011_01_23_archive.html>
	
	
* Charles Babbage designed the difference engine which was programmed  
	
by Ada Lovelace(incidentally, she is the world's first programmer).  
	
* Lord Byron had an equally famous daughter & that was none other than  
	
Ada Lovelace.  
	
* Microsoft had a picture of Ada Lovelace as its logo originally.


15. [Dan](http://manchesteroblique.blogspot.com/)
3.4.12 / 6pm


Awesome post. This sounds like something out of Hofstader. For supplemental entertainment, this very idea seemed to be the foundation for the now defunct show “Caprica”, where the analog versus digital computer was the defining line between cognition and ignorance. Definitely worth the viewing time.


17. [metamechanics](http://metamechanics.wordpress.com)
3.6.12 / 2am


To qoute you qouting on the other link…Marshall McLuhan noted—the way we write is part of the content of what we write—“the medium is the message” or at least a part of it.


I see your point(s) – for example – if you answer with conviction you do not have to think about what you say, but if your convictions change you will have to change what or how you say your answer. If you calculate your answer you are expected to maintain calculation as it assumed convictions stay the same. If the calculated answer changes your conviction then at least paper and pen marked up on parchement illustrate development and process with history all in one visual memory together pointing towards a consistant response, but blogging and comments illustrate seperate combinations of vocabulary with no development present, consistancy has to be calculated. (Naturally its on a computer). 


Waste Land and Cantos. 


Poetry professor warned us of Cantos, we looked at it, none of us tried. He also warned us of Prufrock and said Waste Land was definetly out of our sophomore league. In retrospect Prufrock was poetry for a young 20 year old philosopher (believe that was TS Eliots age when he wrote it). After completeling Prufrock I started looking at Waste Land but the summer came I started framing for the framer referenced in the link Woods posted above (ha).


Margot, no idea, will look up and add to my list of things I'd rather be doing than working.


Its been a learning experience.


Or as Chris would say in “lock stock and two smoking guns” ? (guy ritchie flikc)


Its been emotional.

19. [Margot krasojevic](http://gravatar.com/krasojevic)
3.7.12 / 12am


Great reference metamechanics.  

Letters between Babbage ,Tennyson and Browning expose the very strong relationship between poetry and science in the 19th Century, whether it's as a result of slower changes in technology than we are currently experiencing or the wide reaching effects of the industrial revolution that reason this bond between forward thinkers, their relationships, nevertheless were more evident in the thought processes rather than their manifestations; comparatively perhaps that is what is less prominent today, whereby technology and fabrication cater to visualisation and end products more so than a cross-disciplinary dialogue which is evident in their correspondence. Anyhow, rambling aside the connectivity between words and sentiment translated beautifully.  

<http://www.uh.edu/engines/epi879.htm>





1. [metamechanics](http://metamechanics.wordpress.com)
3.8.12 / 4am
	
	
Nice link, will research more time permitting (the next epsiode was interesting as well)  
	
Ramblings are good, maybe the poets and scientists rambled more? 
	
	
I see your point and funny enough a section I read last night in Husserl pointed out that before Galileo and Descartes there was no distinction between the scientific and psychic (I think he was using the latter word a little more “scientificly”, or so I am lead to believe right, ha?)
	
	
I like this cross pollination you mention…
	
	
21. [simongrew](http://simongrew.wordpress.com)
3.18.12 / 11am


Reblogged this on [Don't Sleep!](http://dontsleepdesign.wordpress.com/2012/03/18/123/) and commented:  

Great thought provoking Blog post by Lebbeus Woods….these thoughts are infiltrating my thoughts on this project…could be interesting.


23. arcajb
3.23.12 / 9pm


What is a notable occurrence in many digital vs analogue discussions is the perceived difference and disconnect between the brain and the computer. In reality, everything we have made as a species – from tools to supercomputers, we made with our mind. A canon 5D can't shift it's autofocus as quickly as the human eye, the computer cannot pull experience and memory into its computational abilities. However, the latest Intel IvyBridge 22nm microarchitecture can perform a task such as turning a light bulb on and off more in one second that we could physically do in around one billion years. The idea is that we've created all of these digital devices that mirror the ‘analogue' devices found in ourselves or in nature. But what is the weakness? They need input from us. They need someone to press the button. We are no different! We require input. Our input is our environment – the books we read, the images we see, the space we experience. We are nothing more than computers ourselves. However, we have narrowed down and expanded upon the ideas in our own brain through advancements in microarchitecture to use billions of trigate transistors in a hyper threaded way to accomplish a certain task or solve a certain problem. 


My point: what is happening is a natural evolutionary process. It is a physical extension of our own bodies, our own brains, whose electrical impulses are separate only because we have to translate it into the physical world by typing with our fingers. This is the disconnect. there is not yet a electrical connection between our minds ideas and the extended nervous system of our emerging ‘macro-organism' the internet. It's Kurzweil's singularity. It's considering yourself as a bloodcell in this new society. Cumulative brainpower, something we've learned from making processors. 


In a nutshell, it's integration. With everything.


25. [Lebbeus Woods – The Next Revolution? at Sub-Studio Design Blog](http://blog.sub-studio.com/2012/04/lebbeus-woods-the-next-revolution/)
4.17.12 / 1pm


[…] the LW blog: The Next Revolution: In a recent panel discussion with Hernan Diaz Alonso, we exchanged differing opinions about […]
