
---
title: WHY I BECAME AN ARCHITECT—Part 1
date: 2012-02-06 00:00:00 
tags: 
    - architecture
    - inspiration
    - memoir
    - motivation
---

# WHY I BECAME AN ARCHITECT—Part 1


[![](media/dore-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/dore-21.jpg)


*(above) At the outset of Dante's epic poem [The Divine Comedy](http://en.wikipedia.org/wiki/Divine_Comedy), the ancient Roman poet [Virgil](http://en.wikipedia.org/wiki/Virgil) leads [Dante](http://en.wikipedia.org/wiki/Dante_Alighieri) to the entrance of Hell (The Inferno). Above the opening is inscribed “Abandon all hope, you who enter here.” Gustave**Doré**‘s illustrations date from the latter 19th Century; Dante's poem dates from the 14th Century.*


.


I would like to tell a short story—or perhaps not such a short story—about the reasons why I chose to become an architect. Exactly why this blog's readers should be interested in my recollections about such a matter I cannot say, and perhaps I am mistaken in spinning out such a story here. Still, I feel compelled to do so and can only hope for the readers' tolerance.


I'll quickly pass over my early obsession with drawing, and the first time I saw Gustave Doré's engravings for Dante's Inferno, however pleasurable it might be to linger on the great illustrator's use of pure line in his startling visions of Hell. I will pass just as quickly over my childhood memories of the engineering works built by my father, co-mingled with memories of jet bombers and fighters that enflamed my imagination—as I have already spoken of them [here](https://lebbeuswoods.wordpress.com/2012/01/02/origins/). Instead, I'll take up the story at age sixteen or seventeen when, some years after my father's death, when I—an only child—lived with my mother in Indianapolis, Indiana, and was attending high school. This was the mid-1950s.


For whatever reason, I had taken up painting, oil painting. Setting up my easel in our modest living room, I painted pictures of everything from copies of paintings in a how-to book of bowls of fruit to images of pure light, that is, light that emanated from within the little canvas boards I'd bought at an art store. Where the impulse to make such images came from I cannot say, but oil paint was perfectly suited to the task (acrylics had not been invented yet), though I do recall that I tended not to blend the strokes into smooth transitions, but preferred to build them up from separate dabs and daubs, so that the light was broken down into elements proceeding from an intensely radiant center to a gradually deepening darkness. I only wish now that even one of these paintings had survived.


I cannot recall, as well, whether I painted these pure light images before or after I had come across pictures published In Life Magazine of Michelangelo's Sistine Chapel ceiling frescos. I suspect it was after, because I had been so moved by them. No doubt my light paintings were a form of imitation, without the heroic figures I couldn't draw anyway, of the great artist's expressions of a troubled spirtituality. The twisting and turning, the contrapposto (I later learned the term) of the figures portrayed an unnamed inner conflict having more to do, it seemed, with the struggle of the human psyche against itself, rather than the Biblical stories the paintings ostensibly illustrated, or against the domineering will of God.


Christianity is in some ways a religion of self-torment. Fear of afterlife retribution for our sins against the laws of the church weighs on many less heavily than the pain of personal guilt from moral failures and of an erosion or outright loss of faith. This has had a baleful effect of European society by placing conflict at its core, as well as a mood of negation. But also, it's been a boon for the arts, giving them an inexhaustible source of affecting ideas. Or, to put it another way, the arts have not been merely ornamental, but central to people's struggle to “find themselves' in a world without clarity, or certainty, of meaning. The very different worlds of Dante and Michelangelo testify equally to this condition, and led me slowly, inevitably towards architecture.


*to be continued*


.


[![](media/dore-12.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/dore-12.jpg)


*(above, below) Two of *Doré**‘s* many scenes of Divine punishment for the sinners in Hell.*


[![](media/dore-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/dore-3.jpg)


.


*From Michelangelo Buonarroti's frescos on the [ceiling of the Sistine Chapel,](http://en.wikipedia.org/wiki/Sistine_Chapel_ceiling) The Vatican, Rome, painted in the early 16th Century:*


[![](media/sistine-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/sistine-1a.jpg)


*(above) God dividing the light from the darkness, from the Biblical Book of Genesis.*


[![](media/sistine-4.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/sistine-4.jpg)


*(above) The Prophet Jonah, from the Biblical Old Testament.*


.


#architecture #inspiration #memoir #motivation
 ## Comments 
1. [matthewdarmourpaul](http://matthewdarmourpaul.wordpress.com)
2.6.12 / 8pm


I'm not entirely sure why I'm in school for architecture (it's my first year, second semester), but I find it easy to resonate with your work, particularly your extremely detailed (almost pornographic, by means of excessive detail) line drawings. Both my parents are Christian ministers, and the struggle you closed with here is definitely real. Architecture seems logical when you're well practiced at art, and looking to make a more positive influence (or just more influence). Visionary architecture especially I think speaks to ‘the struggle', I'm interested to see where this article continues…
3. [winter patterns « patterns and other pretty things](http://patternsand.wordpress.com/2012/02/07/winter-patterns/)
2.7.12 / 5pm


[…] read this post by lebbeus woods yesterday and can't stop thinking about it. many designers share these […]
5. [Oliver](http://baufunk.blogspot.com)
2.7.12 / 7pm


I think the moment I made the choice to become an architect is not so important as the continuous choice I make everyday of what kind of architect I want to be. To me, the struggle has been between maintaining an open and inquisitive mind at the same time as standing up for the political ideas and beliefs I hold. It's not easy, to know when to listen and know when to speak, but I believe I need to do both to grow. There must be a reason I was born both with a mouth and a pair of ears.
7. dkzo
2.8.12 / 8pm


I think I should be a fourth or fifth year student this coming fall. Instead, I'm back in my “first” year. My mom gets angry with this, but I keep telling her I'm the best first year student ever. 🙂 


I often think about why I want it so badly, all the education seems to do to me – is set me back. Enough passion to fill a room up, really. It's expensive. What I thought was going to be a cheap starting solution, is now… um..I too started oil painting. And I have drawn since I was basically born.


When I first started to take art seriously I related my work with “outsider” artists. I guess I was around 22 years old. It is how I came across your drawings (again) and things just kind of started to make more sense. Actually, at that point, it was as if every new person I met or every new job I was interviewing for had some surprising connection to an architect or architecture. In New Jersey, I had painted with a professional oil painter for six months before she told me her father had been an architect working on pen station. Before I moved back with my parents, I had applied for an “assistant” position by writing a 4 page poem, only to discover after they had asked me to come in – that it was for a COO position at reputable firm…


I find that the architects I end up working for, or end up connecting to – have this outstanding love of the arts. I don't want to be “an artist”, I know I already am. I think when I finally get there to being an architect, I may have the confidence to return whole-heartedly to art before my take into the architectural world. Because of the ability to blend, and blend so well – Architecture is to the built environment what oil painting is to the visual arts – this is how I feel. I think it does something to everyone. I kind of rely on my synchronicity(s) at this point… but then again, I notice I always have.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	2.8.12 / 11pm
	
	
	dkzo: It sounds like you know what you want and have a clue how to get it. Just stay on course and don't get diverted, by anyone or anything. I appreciate very much your candor.
9. [Jami Primmer](http://www.facebook.com/profile.php?id=782048234)
2.9.12 / 8pm


In my search for clarity, for meaning, I chose to become an architect. Lebbeus you were my first inspiration. My first love.
11. [Lebbeus Woods on Why He Became an Architect | Hartanah.Org](http://hartanah.org/lebbeus-woods-on-why-he-became-an-architect)
2.11.12 / 1am


[…] emerged from Western faith and its accompanying explosion of artistic renderings. As he writes in Part 1, his ideas and early fascinations have crystallized in retrospect: “[T]he arts have not been […]
13. 2002-2007
2.13.12 / 7am


You were an inspiration even before I started at Cooper. Thank you so very much.
15. [jghunter](http://gravatar.com/jeremyghunter)
2.13.12 / 10am


I am a second year student and I struggle to work out still why I chose to be an architect. To be clear, I am undertaking Interior Architecture, but considering we rarely stay inside the building it doesn't make much difference. I struggle to understand because I don't understand. I find being able to know how to apply a design process when I don't have any experience of a working studio. Unfortunately, the world of academia is so separate from the working world we just have to be hopeful at the end of year show when the wolves decide whether we're worth taking home. We get no chance to know how the real world works. Until that happens, will never know what I really got into architecture for, as I will have no way to validate myself.
17. [On becoming Lebbeus Woods « typologica](http://typologica.com/2012/02/13/on-becoming-lebbeus-woods/)
2.13.12 / 3pm


[…] other words, to finally be something, one first has to have the courage to become it. … … Lebbeus Woods: Why I Became an Architect—Part 1 Why I Became an Architect—Part […]
19. [The Fox Is Black » A few thoughts for Valentine's Day](http://thefoxisblack.com/2012/02/14/a-few-thoughts-for-valentines-day/)
2.14.12 / 8pm


[…] The relationship is between Lebbeus Woods and Architecture. In two posts from his personal blog, the architect talks about how his relationship with architecture […]
21. [andy Hickes](http://www.rendeing.net)
4.16.12 / 6pm


Why I became an architect in 1965.


 My father was a general contractor and the only original “artwork” I ever saw in rural Pa were renderings. I myself had always loved drawing 


“futuristic” cities. Since I was good in math and I was good in art, my junior hs principal said, “that means you should be an architect”! so I


 became an architect. Unfortunately in 1970 after graduating the only people I found drawing buildings were the “lowly” renderers. I resigned 


myself to the plummet in status, made some samples, and walked door to door. it was the best decision I ever made. 


 I learned how to make renderings that surpassed what I ever dreamed I was capable of. 


What more satisfaction can life give us?


I accidentally stumbled into L. Wood's gallery show on Saturday, April 14, the last day. After losing interest in “Architecture” about 30 years ago


and always having had a hard time understanding the words written about drawing and architecture, your work was a REVELATION! thank 


you. I came to your site becasue I had to see who this person was.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.16.12 / 10pm
	
	
	andy Hickes: I identify with your story very much. All I can say is ‘welcome to the blog and back to architecture' I hope.
