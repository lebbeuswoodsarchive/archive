
---
title: TWO WORLDS
date: 2012-02-10 00:00:00 
tags: 
    - housing
    - politics
---

# TWO WORLDS


[![](media/pruitt-igoe.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/pruitt-igoe.jpg)


[![](media/pennsouth.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/pennsouth.jpg)


Two housing projects. Pruitt-Igoe in St. Louis, was public (government operated) housing; Penn South in New York City was, and is, private. The latter succeeded, rather quietly; the former failed, spectacularly. The message, according to Michael Kimmelman's article is that private enterprise works, whereas government doesn't. It is a message that fits almost perfectly into the American political ideology of today—“let's get government out of people's lives and let the free market run things!”—and it's a point of view that seems vindicated by this one comparative example. But like most things it's not quite that simple.


Government—democratic government—which uses money raised by taxes to finance public works projects, is subject to more—and more abrupt—changes than private business. Voters, depending on their personal circumstances can be in favor of publicly supported housing at one time, and just a little later, against it. CEOs and shareholders tend to take a longer view of their projects and are more willing to ride-out downturns, in order to reap profits in due time. What happened in St. Louis, in the case of Pruitt-Igoe, is a case of voter vacillation: the tax-dollars to build were there; those to maintain were not.


But this is not the most critical point. The people who bought the Penn South apartments, in an urban-style fulfillment of The American Dream, were financially solvent enough to get banks to loan them money, that is, to get mortgages. The renters in the Pruitt-Igoe housing were not. Un- or under-employed, with poor credit and not the most outstanding future prospects, they needed help to be in a decent apartment to raise their families. They got it, but then were, effectively, betrayed. The bigger question is, why did they need help to begin with? The easy answer is that they were too lazy to work and too irresponsible with what they were given. Even if we believe that is true, what do we do with such people? They cannot simply be ignored or written off. And if it is not true, then why were people put in such a position of need? Where was private business then, providing education, job-training, and the jobs themselves that would give the underclass a chance to join their fellow beings in productive, rewarding lives? With no profits in sight, the free-market did—and does—little to nothing. Only government, embodying the values of the whole society, can fill the vacuum.


It seems to me that the free-market fable evoked in Kimmelman's article, and the article itself, do not dig nearly deeply enough. Many questions remain. Not incidentally, the architecture of the two projects featured in this article are quite similar. Oh, and didn't anyone mention that Penn South's population is largely White, while Pruitt-Igoe's was largely Black?


LW


.


# **Towers of Dreams: One Ended in Nightmare**


**By MICHAEL KIMMELMAN**


Hortense Davis sat beside neat stacks of new magazines in the pristine administrative office at Penn South, the 1960s high-rise housing cooperative development for low- and moderate-income workers in Chelsea. She was asked to ponder life there across 20 years.


“Perfect,” she said after a shrug, stumped to come up with some complaint. “Especially if, like me, you live alone and don't drive, because it's close to shops and transportation, it's secure, the grounds are beautiful in the summer.” Ms. Davis, a 76-year-old former Brooklynite, has an apartment on the ninth floor, facing the Hudson. “It's like a penthouse,” she told me. “I like to stand at my window and watch the ships.”


I went to Penn South this week, having seen [“The Pruitt-Igoe Myth,”](http://www.pruitt-igoe.com/) Chad Freidrichs's shattering documentary, now at the [IFC](http://www.ifccenter.com/films/the-pruitt-igoe-myth/) Center. Pruitt-Igoe was the notorious St. Louis public-housing complex, demolished in 1972. Images of imploded Pruitt-Igoe buildings, broadcast worldwide, came to haunt the American consciousness. Critics of welfare, big government and modern architecture all used the project as a whipping boy. “The day that modern architecture died,” Charles Jencks, the architect and apostle of postmodernism, called the demolition.


[Penn South](http://www.pennsouth.coop/) is a cooperative in affluent, 21st-century Manhattan past which chic crowds hustle every day to and from nearby Chelsea's art galleries, apparently oblivious to it. It thrives within a dense, diverse neighborhood of the sort that makes New York special. Pruitt-Igoe, segregated de facto, isolated and impoverished, collapsed along with the industrial city around it.


But they're both classic examples of modern architecture, the kind Mr. Jencks, among countless others, left for dead: superblocks of brick and concrete high rises scattered across grassy plots, so-called towers in the park, descended from [Le Corbusier's “Radiant City.”](http://www.google.com/search?q=Le+Corbusier%E2%80%99s+%E2%80%9CRadiant+City.%E2%80%9D&hl=en&prmd=imvnso&tbm=isch&tbo=u&source=univ&sa=X&ei=z0AgT5qQE-Ha0QHw2-AF&ved=0CDgQsAQ&biw=981&bih=522) The words “housing project” instantly conjure them up.


Alienating, penitential breeding grounds for vandalism and violence: that became the tower in the park's epitaph. But Penn South, with its stolid redbrick, concrete-slab housing stock, is clearly a safe, successful place. In this case the architecture works. In St. Louis, where the architectural scheme was the same, what killed Pruitt-Igoe was not its bricks and mortar. (Minoru Yamasaki, who designed the World Trade Towers, was the architect.)


The lesson these two sites share has to do with the limits of architecture, socially and economically, never mind what some architects and planners promise or boast. The two projects, aesthetic cousins, are reminders that no typology of design, no matter how passingly fashionable or reviled, guarantees success or failure: neither West Village-style brownstones nor towers in the park nor titanium-clad confections. This is not to say architecture is helpless, only that it is never destiny and that it is always hostage to larger forces.


Pruitt-Igoe opened to great fanfare in 1954. The St. Louis Housing Authority advertised a paradise of “bright new buildings with spacious grounds,” indoor plumbing, electric lights, fresh plastered walls and other “conveniences expected in the 20th century.”


Ruby Russell, one of Pruitt-Igoe's early tenants, recalls in Mr. Freidrichs's film: “It was a very beautiful place, like a big hotel resort. I never thought I would live in that kind of a surrounding.” Having moved from dismal slums, she marveled at the light and views from her “poor-man's penthouse” on the 11th floor. Then “one day we woke up and it was all gone,” she says.


The documentary unpacks the factors conspiring against the project — all the things that don't apply at Penn South — among them inadequate money set aside for maintenance and unfathomable welfare rules stipulating that no able-bodied man could live in a home where the woman received government aid. A night staff from the Welfare Department patrolled apartments searching for fathers to evict.


As disastrously, opponents of public housing blocked federal money that could have helped local authorities improve conditions. The [1949 Housing Act](http://www.knowledgeplex.org/kp/text_document_summary/scholarly_article/relfiles/hpd_1102_martinez.pdf), which promoted urban renewal at federal government expense, paved the way for Pruitt-Igoe and simultaneously promoted the white flight that doomed it, financing new segregated towns on city outskirts that drained jobs from the inner city. There's an unspeakably sad clip in the documentary in which a soft-spoken young father breaks down before a television interviewer over his failure to find anyone willing to hire a black man, and a vintage clip of a young white suburban mother explaining in a shaky voice, “We have our home here, and if the colored move in and run real estate prices down, it's bound to create tension.


“I think their aim is mixed marriages and becoming equal with the whites,” she feels compelled to say.


Besieged and increasingly abandoned, Pruitt-Igoe was overtaken by drug dealers and murderers, broken pipes and shattered windows, set afire and adrift. Public authorities and cultural experts after the fact blamed the residents or the architecture, but these both were more the victims than the cause.


Like Pruitt-Igoe, Penn South was a midcentury slum-clearance project. Designed by Herman Jessor as a modern cooperative for working class families, backed by the International Ladies Garment Workers Union, it called for 10 buildings spread across 20 acres between Eighth and Ninth Avenues, from 23rd Street to 29th Street. It was not public housing. Two bedroom apartments cost $3,000 when the complex opened in 1962. (Those same apartments now sell for $100,000.) President John F. Kennedy stood atop a flag-decked dais in the bright May sun to dedicate it.


Steady income from maintenance payments and retail units in commercial buildings the co-op owned guaranteed Penn South a stable income. Tax relief from the city shielded it from escalating real estate values. Residents poured money into improvements. Repeatedly they declined the right to sell their apartments at market rates, preserving the ideal of moderate-income dwellings, adding facilities for toddlers and the elderly, playgrounds, a community garden and a ceramics studio.


Few people chose to leave, aging in their apartments, and in 1986 Penn South became the country's first Naturally Occurring Retirement Community, or NORC. That's an official designation for housing that wasn't built for elderly people but is occupied by enough of them to become eligible for special grants.


It turns out that the very architectural traits that conventional wisdom said made tower-in-the-park projects like Pruitt-Igoe inhumane actually make these buildings ideal for retirees: the elevators (so long as they work) and communal spaces, the enclosed green areas where it's possible to walk safely, the openness and in-house programs. Once again architecture evolves in bottom-up ways that architects and planners can never fully predict. Today New York has dozens of NORCs, most of them towers in the park.


I stopped into the Penn South Program for Seniors the other afternoon, and Elaine Rosen, the program coordinator, showed off the book-lined common room, the basket of donated eyeglasses, and a long program sheet of concerts, lectures and free Swedish massages. “It's a community,” she said.


So was Pruitt-Igoe, whose fleeting, forgotten Eden the film movingly excavates in the recollection of Sylvester Brown, an early resident. He remembers the smell of pies and cakes in the hallways. Jacquelyn Williams, who moved with her 11 siblings from a shack where her mother slept beside the kitchen's potbelly stove, recalls “friendships and bonds formed there that have lasted a lifetime.”


“I know a lot of bad things came out of Pruitt-Igoe,” she says. “I know they did. But I don't think they outweighed the good.”


Another early Pruitt-Igoe tenant in the film, Valerie Sills, remembers as a little girl moving into Pruitt-Igoe when it was ablaze with Christmas lights. Then she describes returning to the project as a policewoman when enraged residents dropped homemade firebombs on her car.


“There is enough blame to go around,” she concludes. But “I can see it,” she insists, meaning the Pruitt-Igoe of her childhood. “I remember dancing in the street, I remember riding my bike, I remember we raced up and down the hills. It was our *home*.”


Ms. Sills pauses and starts to weep. I think of Ms. Davis, staring at the Hudson from Penn South. “When I feel bad, I don't intend to, but I dream about Pruitt-Igoe,” Ms. Sills says.


“And I always see myself standing at the window, looking out the window.”


.


*From Pruitt-Igoe:*


[![](media/pruitt-igoe-3.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/pruitt-igoe-3.jpg)


*(above) From the architect's brochure, c. 1958.*


[![](media/pruitt-igoe-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/pruitt-igoe-1.jpg)


*(above) Playground in Pruitt-Igoe, c. 1961.*


[![](media/pruitt-igoe-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/pruitt-igoe-2.jpg)


*(above) Apartment in Pruitt-Igoe, c. 1962.*


*From Penn South:*


[![](media/penn-south-11.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/penn-south-11.jpg)


*(above) Penn South today.*


.


[![](media/penn-south-21.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/penn-south-21.jpg)


*(above) “Seniors” today at Penn South. Homeowners, they are set for life.*


#housing #politics
 ## Comments 
1. [metamechanics](http://gravatar.com/metamechanics)
2.14.12 / 3am


Lebbeus there is a competition for Pruitt-Igoe announced- <http://www.bustler.net/index.php/competition/pruitt_igoe_now/>
