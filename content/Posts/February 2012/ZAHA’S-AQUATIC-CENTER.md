
---
title: ZAHA’S AQUATIC CENTER
date: 2012-02-17 00:00:00
---

# ZAHA'S AQUATIC CENTER


[![](media/aquatic-1.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/aquatic-1.jpg)


Why do I go out of my way to openly criticize Zaha Hadid's Aquatic Center for the 2012 Olympics in London? Actually, this is a kind of love letter, the kind that begins, “Don't you love me anymore?”


I feel abandoned and bereft because one the most gifted architects of my time has been reduced to wrapping such conventional programs of use in merely expressionistic forms, without letting a single ray of her genius illuminate the human condition. Am I being pretentious and overly demanding? Of course. But that's the way disappointed lovers behave. Exaggerated emotions. Absurd demands. Anger that transgresses all reason. She has let me down, and what makes it worse is that she apparently couldn't care less.


Did she ring me up and ask me what I thought the design should be? No. If she had I would certainly have told her to propose rethinking the aquatic events themselves, reconfiguring the competition pools or at least the relationship of spectator seating to them. How (it might be asked) can an architect challenge such rigid rules of a sport and the traditions surrounding it? Well, how can an architect challenge the equally entrenched conventions of how people inhabit their houses and the streets of their town or city? Simply by having a better idea. Zaha has done it before.


And did she consult with me about the way the Center's form should somehow express the “fluid geometries of water in motion?” No. If she had, I would have counseled her to forget this idea, because it is too easy and obvious. Even if it could be achieved in architectural forms (which it isn't here, because water's fluidity is formless and boundless) it would be much more compelling to competitors and their audience to be confronted with actualities of their relationship. For example, the most interesting photograph in the suite of images presented on the blog [Arthitectural](http://www.arthitectural.com/zaha-hadid-architects-london-aquatics-centre-for-2012-summer-olympics/) is the underwater shot, where the motive power of the competitors' choreographed arms and legs can most closely be observed.


Does every work of a great talent have to reach a new peak of achievement? No, of course not. Or wait.  Maybe it has to at least aim for it. In the Aquatic Center, so prominent on the world stage just now, Zaha was obliged, I believe, to set an example measuring up to her status and, more so, to her talent. The finished design shows no signs of any such attempt. So says the dejected lover.


LW


*Photos below by Hufton + Crow:*


[![](media/aquatic-2.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/aquatic-2.jpg)


*(above) The same old thing, with elegantly shaped windows.*


.


[![](media/aquatic-31.jpg)](https://lebbeuswoods.files.wordpress.com/2012/02/aquatic-31.jpg)


*(above) An underwater world, at a quiet moment, just waiting to come alive.*


LW



 ## Comments 
1. Pedro Esteban
2.17.12 / 3am


Thank you.  

I laughed a lot reading it, I like very much the humor of the text.  

She have to relax and get back to the old days…
3. [John M.](http://uncertaintimes.tumblr.com/)
2.17.12 / 5am


looks like a whale


not a very pleasing whale, sort of a bored doodle whale
5. [Individuos opacos](http://gravatar.com/opacos)
2.17.12 / 8am


I'm reading “All that is solid melts into air” of Marshall Berman. The first chapter explains the modernity compared with “Faust” by Goethe …


Indeed, the underwater image seems much more “aquatic”.
7. tomer
2.17.12 / 11pm


wow!!  

you just gave an amazing image in my head for how an ‘AQUATIC CENTER' should look like!!


i'm dying now to have such an opportunity!!  

thank you!!  

it's amazing how few words can paint you a whole picture at the top of your head..
9. thanasis
2.17.12 / 11pm


i m a student of architecture.her drawings inspire me very much.they talk to me.i hope they ll talk to her again soon
11. quadruspenseroso
2.19.12 / 4am


Her spaces are so attractively coiffured, of a hip-gyrating suggestiveness, why even the most orthogonal blue-prude can't help but sneak-a-peak, even tap to the beat. What a kingly charm of unfettered self-interest, backstage? oh yes the promoter's smile; adoring fans fainting, screaming, smashing barricades – a sold out show. The post-performance vacuum? Nothing a prescription can't fill. Nothing a fellow architect can't stage, And if it blows? The discarded condom in the urinal? – un-flushable, even for a super star. <http://quadruspenseroso.wordpress.com/2012/02/19/elvizaha/>
13. CDC
2.19.12 / 2pm


It is indeed a sad story, but again she doesn't seem to be interested in architecture more than of being considered a diva (at least that's what she shows in every of her extremely uninteresting lectures.) On the other hand the leader of her office Mr. Shumacher gives explanations that completely crash with the concepts of the early Zaha. So the early Zaha doesn't exist anymore (or maybe it never existed)…All we are left is just some rhino amateurs trying to find their nurbs utopia in hyperexpensive, horribly detailed buildings (just go to guangzhou to see their lack of interest on “detail).
15. [Bryan](http://premontb.wordpress.com)
2.24.12 / 3am


It literally looks like a beached whale. I would put more effort into an insightful criticism if she put more effort into her buildings. It is truly disappointing that someone with so much talent and so much star power could not resist the fall into thinly veiled ubiquity. She could actually have built some amazing buildings that would have progressed architecture, but she gave up, and this is the result. A true flounder.
