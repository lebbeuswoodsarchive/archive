
---
title: MATRIX
date: 2008-05-29 00:00:00
---

# MATRIX



[![Overall model view](media/Overall_model_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-puc1a2.jpg)[![Model view](media/Model_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-puc21.jpg)[![Campus plan](media/Campus_plan.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-matrix101.jpg)[![Theaters](media/Theaters.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-matrix9.jpg)[![Spanish culture school](media/Spanish_culture_school.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-matrix8.jpg)[![Digital library](media/Digital_library.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-matrix11.jpg)


 On a large site in West Harlem, Columbia University is planning to build an extension of its main campus over the next twenty-five years. The urban planning and architectural designs it has commissioned so far are pragmatic at best—maximizing square-footage of usable space—and banal, having nothing to do in particular with ideas about teaching, study, and interdisciplinary research. They also fail to relate in any meaningful way to the existing urban fabric, simply filling the existing city block pattern. For these reasons, the opportunity to conceive and build a truly contemporary university campus is lost.


The intention of the Fourth Year design studio at The Cooper Union was to explore the potentials of a 21st century university campus in New York City. We used as a focus the West Harlem site proposed for the Columbia University campus extension. Searching for the best relationships between different programs of study and research, independently of Columbia's planned programs for the site, our aim—working individually and in teams—was to create a Proto-Urban Campus, that is, one that anticipated creative developments in education.


The approach involved the creation of *a planning matrix* for the site. A matrix differs fundamentally from a master plan. Instead of being based on predetermined programs of use, such as School of Business, School of Art, and the like, it is a three dimensional *armature* designed for the site that will accept many interdisciplinary and hybrid educational programs likely to develop in the future. We arrived at the design for the planning matrix through an in-class competition. Seven teams of four students conceived, designed, and presented seven matrix schemes. From these, a jury that included Steven Holl, Diana Agrest, and Michael Bell selected their idea of the best matrix, which was based on idealized paths of movement through and above the site, and in much the same spirit (if different form) as the Commissioners' Plan in 1811 that established the Manhattan grid matrix.


It was this selected matrix on which individuals in the studio worked for the remainder of the semester, developing educational programs and projects. These may be understood as temporary or semi-permanent constructions in an evolving architectural and educational landscape. The overall idea of the project is that the campus is not a hierarchy of the known, but rather a network of complexly interacting modes of learning, exploration, and space.


LW


**The Irwin S. Chanin School of Architecture of THE COOPER UNION**: Anthony Vidler, Dean; **Fourth Year Design Studio: Faculty**: Lebbeus Woods, Christoph a.Kumpusch, Mersiha Veledar, Jennifer Lee; **Students**: Catherine Ahn, Dorit Aviv, Tom Brooksbank, Kathleen Brumder, Brian Cha, Jaeho Chong, Chia Chou, Dionisio Cortes, Theodora Doulamis, Timothy Evans, Jun Hayatsu, Yoonyoung Hur, Seung-Hyun Kang, Keyleigh Kern, Anna Kostreva Alex La Cruz, Raye Levine, Kalle Lindgren, Jacob Mautner, Bryan Morrison, Dennis Murphy, Matthew Ostrow, Andrea Pinochet, Christopher Pounds, Arnold Wu, Lythia Xynogala, Kinu Yamamoto, Sy Lyng Yong, Anna Zagol.


  


Columbia University website showing its campus expansion plans:


<http://neighbors.columbia.edu/pages/manplanning/>


Article about the project:


<http://www.aiany.org/eOCULUS/2008/2008-06-24.html>


 




 ## Comments 
1. csmith
5.29.08 / 6pm


I live in Manhattanville, not the housing projects specifically but the neighborhood, near the NY Structural Biology Lab. It is a pleasure to see this.


This *aramture* is inspiring. I would enjoy seeing the other schemes as well.
3. Mhuett
5.30.08 / 3pm


First let me state that I have admired your work for years and will continue to do so in the future. My comment are only based on the few images attached and the brief description of the project. 


I understand the idea behind the matrix and I completely agree with interdisciplinary approach to design. I don't however understand why this place is floating above the city. Does the existing city remain as is below, as if there is a cloud of intellect hovering above that only elite members of society may experience fully? What is the idea behind this? It is my belief that education involves interaction, exploration, and challenge, not simply between student and professor, but student/student; student/society; and professor/society. 


I would like to see a stronger connection with the street and the people who live and work below the Heavenly Cloud of Education. Doing so would benefit both parties.


Those are my two cents. I would enjoy seeing the other schemes as well.
5. edward
6.16.08 / 12pm


I had high hopes for the Matrix as a realistic yet imaginative proposal but the “Archigram” images seem as disspiriting as the universities corporate structures. Admittedly I am only going by the several images you show, but it seems to me what is desperately needed, is a way of building more cohesive communities as opposed to the usual collection of individual ego statements. Perhaps the Free U. of Berlin was wrong headed, but it was a honest attempt.
7. [Cклад | TOTAL GREEN ROOF](http://goshaz.com/?p=1102)
6.28.09 / 3pm


[…] – planless skyscraper <https://lebbeuswoods.wordpress.com/2008/05/29/matrix/> – какой-то корпус какого-то универа, отмеченный […]
