
---
title: TOWER SPACE
date: 2008-05-19 00:00:00
---

# TOWER SPACE



[![](media/towersplan1.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/towersplan1.jpg)[![Interior installation view](media/Interior_installation_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-tsp52.jpg)[![Overall installation view](media/Overall_installation_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-tsp61.jpg)[![Installation--interior tower view](media/Installation--interior_tower_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-tsp3.jpg)[![Installation--exterior tower view](media/Installation--exterior_tower_view.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-tsp14.jpg)


High-rise towers rarely develop the verticality of spaces they create, remaining instead only iconic objects in the urban landscape. Their interiors consist of stacked-up floor plates, maximizing leasable or usable floor area, and in urban centers where groupings of towers crowd together on the most expensive land, the spaces between the towers are ignored. No doubt, these conditions result from the single-minded interests of commercial developers and the isolation enforced by private property ownership. The potential remains, regardless of the limitations of current attitudes, to invest the latent and actual verticality of towers with new programs of habitation that expand the meaning and experience of urban tower space. This was the aim of the sixth semester design studio in the Graduate School of Architecture at Pratt Institute this past semester. It was realized in a one-to-one installation constructed by the members of the studio in the main space of a recent addition to the architecture building, designed by Steven Holl. 


“The studio set out to explore a ‘proto-urban' condition observed in cities throughout the world,” write the members of the studio. “Tower projects are rising to previously unimaginable heights, employing the very latest in technology, materials, design, and construction methodology. While many such endeavors enjoy great acclaim, the projects, typically ‘single point' towers, rarely address the existing or emerging urban landscape.  In this way, the tower, despite the use of expressive shapes and complex skins, is rapidly becoming the world's generic building unit. Regulatory and economic realities often force this unit's construction in a kind of non-contextual vacuum. Our studio explores what might occur if a *complex* of interrelated towers were to be commissioned. What types of relationships, physical or otherwise, might be formed? How might these new relationships change (for better or for worse) the ‘proto-urban' environment?


 Our proposal emerges from the spirit of research and is born of a commitment to an entirely collaborative design/build process.  Our collective vision is the creation of four integrated towers. The structures are shaped and informed by a matrix of vertical urban planes based on an aggregate of the world's many urban grids.  Three of the emerging towers stand vertically while a fourth is set on a diagonal. The complex composition permits rich relationships between the structures and the ground plane while also giving rise to an entirely new form of public zone. The architecture will incorporate interactive experiences that fuse light, sound, and moving images in order to explore our studio's interest in programs for verticality that relate primarily to the psychological desires and realities of ‘proto-urban' dwellers.”


The results are visually powerful and evocative of new possibilities. What remains to be accomplished is a critical discourse about them, and a way to evaluate—or even answer—the questions invoked by “for better or for worse.”


LW



**Pratt Institute Graduate School of Architecture and Urban Design: Thomas Hanrahan, Dean; William MacDonald, Chair; **Sixth Semester Design Studio: Liam Ahern, Tia Maiolatesi, Adam Grassi, Benjamin Keiser, Brian Choquette, Kurt Altschul, Johanna Helgadottir, Andrew Miller, Tapasi Mittal, Rob Jarocki, Dhruv Chandwania, and Lebbeus Woods, Professor; Guest critics: Narelle Sissons, Christopher Otterbine, Christoph a.Kumpusch, Steven Holl. This project was made possible in part by a generous grant from the Research Institute for Experimental Architecture (RIEA.ch), Bern, Switzerland.****


Link to students' website, showing project development:


<http://www.tabblo.com/studio/invitation/1375011/75f42fe02048ed742773960ecfd9748d>



 


 


 


 




 ## Comments 
1. Will Vachon
5.20.08 / 4pm


I like the study. But where does a plan involving four towers become one tower, with respect to these relationships: one isolated project?  

 Or is this taking the plan of a city and extruding it vertically, so that a given tower may build itself into another? Could another building come and design itself into these four?
3. river
5.21.08 / 3am


Every five to seven years, I seem to go through a period of recapitulation. Where temporal experiences both accelerate and recede in equal intensity like a feverish dream. Synchronicity intensifies with my counter-urge to shut the noise of the world out. During these times, I usually run across a book which either literally of figuratively jumps off the shelf at me. Then I read it, i am so totally thankful, and I'm lost for a week or a month in a kind of reverent, spongy, cognitively engaged stasis. Which is where I am right now, BTW. Then i leapfrog into tomorrow like the nimble dancer I always thought I could be, for a few days. Then I encounter problems with the resolution of my vision that need chewing on for another five to seven years. Then I get lost in ‘that' level of detail. and goto repeat etc..


The last book I tripped over was “Gaia's Garden” by Toby Hemenway while I lived in a tipi. My present (as yet narrowly defined and newly experienced) neural explosion of narrative meaning is by Ray Kurzweil, “The Singularity is Near,” even as I test the waters of my internetworked loaner self-built quadcore. Before those two, was Robert Anton Wilson's “Prometheus Rising” while experimenting with the consciousness that Jack Kerouac and Allen Ginsberg and Basho implied to me with the rhythms of De Chirico, John Cage and Duchamp, while I was still in college. And slightly before them was the underpinning of Joseph Campell and Carlos Castaneda and Maya Angelou and Alan Watts in the manner of John Hejduk and many old farts like Ben Franklin and Thomas Jefferson that are much too droll for my kids attention spans in these MySpace'd iPod Halo3nowadays!


Please, please, architects! Do not ignore the spaces between the towers. My intuitive criticism of this Kurzweil's optimism (which I totally share and cohere with) is that the parallel, and inefficient redundancies of our genetics are not dispensable. They are elements of design which we are not adequately prepared to comprehend yet. Silence, repetition, and whitespace are not..


<http://www.thefreedictionary.com/dispensable>
5. Nathan Bishop
5.22.08 / 2am


Due to my rationalist tendencies, I have a predisposition to labelling these sorts of projects as almost irrelevant. However, one image in particular caught my attention:


[![](media/lwblog-tsp3.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-tsp3.jpg)


I both love and loathe this image because it destroys my frame of reference. It is analogous to an earthquake (if any of you have experienced a ‘good' one), in which everything one knows to be stable is suddenly dynamic. I was in Tokyo not too long ago and experienced a 6.0 on the Richter scale. The fear I experienced from this was the absolute removal from my frame of reference. This image accomplishes the same thing. There really is no ‘architecture' here, in a rational sense, but the lines, light, reflections, planes and transparencies create an incredible space. 


I have, however, two concerns for the project (which are usually common to similar projects). First, I cannot experience it, or see it for myself. This may be part of the reason why I have no frame of reference, and why the image is so intriguing. Thus the image walks the knife-edge of architecture and graphic design. Secondly, it has no context. This is of particular concern, especially when the creators say “regulatory and economic realities often force this unit's construction in a kind of non-contextual vacuum.” It is my, perhaps naive belief that context is just as important to architecture as the ‘basics' such as light, space, form. This may turn into a discussion of what is architecture, but to me there is an indispensable connection to the built environment, where programme acts as the distinguisher of architecture and sculpture. I find it difficult to imagine a programme for this space, if in fact one believes programme should be adapted to space. Perhaps this brings back the idea of the dumb-box, where spectacular architecture should be reserved for a spectacular temporal-context or programme. 


In any event, the project has opened my eyes to new spatial possibilities of architecture. I am also impressed at the scale of the installation.


I would also like to make a fleeting comment about the computer-generated massing models. I find the project to be formally weak and with the exception of the angular tower, comparable to similar point tower projects. This may be due to the photography, which to reiterate, limits my frame of reference.
7. adam grassi
5.22.08 / 1pm


@Nathan,


As a member of the team responsible for this installation, I thank you for your comments and would like to respond.  

First of all, I am of the opinion that all architecture discussed in publications is really a discussion of graphic design. Limited by the medium, we are critiquing representations of architecture, not architecture itself. If such a trip is feasible, I invite you to experience the installation in person at 61 St. James Place in Brooklyn, where the project will stand at least until September and is open to the public.


The repercussions of building a 1:1 installation, rather than a scale model, presented us with many challenges. The context of the piece switches from an urban condition (or a scale-model representation of such) to the constraints and necessities of the space in which it is built. The project's exterior is therefore closely tied to the volumes, materialities, and performance of its container, and responds to those various capacities. Since the project is meant to be experienced primarily from its interior, we raised up large planes to define a “site” and allowed the four towers to create their own context within that space. Thus, the context changes from the interior to the exterior; the exterior referencing the atrium designed by Steven Holl, the interior referencing our own surrounding towers and forms. 


The towers' interplay is point-counterpoint: without the three “dumb” vertical towers, the diagonal landscape loses its impact. There is also a programmatic counterpoint, which does not appear in these images. We designed the negative space between the towers to be imposing and frightful. To enter the space, one must duck beneath the obstructive diagonal tower, which looms overhead throughout the interior. The interiors of the other three towers (all occupiable) are meant to be a respite from this overwhelming condition. Through light, sound and video, respectively, each tower relays an abstract program of pleasure and relaxation.


Though the timing may be inappropriate, I like your analogy of a “good” earthquake destroying reference and I think you are grasping our intent. We wanted to destroy the architecture of the diagonal tower's interior, and instead present it as a landscape. Without figure/ground, scale, or axial orientation, it exists as a field rather than a Thing. This fits with our notion of the space as a new public zone, and springs from our discussions of the tragic and frightful attempts to create outdoor plazas between towers in New York.
9. Diego Penalver
5.22.08 / 1pm


Very intriging and complex set of images, these are. A puzzle for the mind and the body. I was enchanted by the design process of the Studio, an attempt to the development of a cristaline articulation of architectural thoughts in the search for a solution of a urban problem in hold for a long time, very interesting. Our cities have open spacial oportunities to be discovered and decoded-encoded, in order to make them feasible in reality. But a free, in-advance exploration in schools is a great advantage, for the development of a language and design tool-ideas for these situations.  

I would asume that there are now two general conditions for a search of this kind, one in outer space where the lack of gravity makes posible for people and spaces to float in any desired location and orientation, with movement routes of any kind, and then this problem on earth where gravity imposes certain limitations and implications to the human body and activities, for which evolution has bonded our mind to a muscle and bone structure. I see here in this study, with the introduction of the diagonal vectors and tower, a posible dialectics between the planes paralel to the ground and the free order of the multi-directional forces. I also agree with Nathan Bishop that a context is lacking in the game, in order to elevated the tension and discourse to a vision and perspective of the city for anew. Maybe the programmatics of an urban problem like this, can be introduced freely as another layer in the conceptual framework.  

Lebb, It´s a very good idea to connect the work of the Pratt studio to a larger audience than the “salon critique”, through the use of this media. Excelent, and thanks for sharing…, very stimulating for me .
11. [lebbeuswoods](https://lebbeuswoods.wordpress.com)
5.22.08 / 2pm


Diego and Nathan: As the person who put the question to be answered before this group, I should note that what I asked was purposely a-contextual. Tower space was framed as a ‘proto-urban' condition, occurring in cities anywhere. The goal was to develop this condition in principle, as a demonstration of possibilities, rather than a definitive design. As it turned out, however, the context the installation responds to is the space it occupies in the architecture building. In this sense, it is very site specific.


Diego: As you suggest, this is real research in architecture, not a ‘student exercise' in the realm of the already known. As such, it deserves to be taken seriously, which includes publication.
13. Wien8836
5.24.08 / 12am


I am just now finding this blog after being told it actually exists-great!


I was wondering if the study took into account internal spaces within a skyscraper? As the summary begins, 


”High-rise towers rarely develop the verticality of spaces they create, remaining instead only iconic objects in the urban landscape. Their interiors consist of stacked-up floor plates, maximizing leasable or usable floor area…” 


The beginning had me excited that study would consider the way in which ”place” could be rediscovered within a highrise. It is true, highrises do sit iconically within the urban fabric of our cities like little children's toys completely out of scale, and reconsidering their relationship to each other on an urban scale is important, but the project could have gone a step further to speculate a new internal logic, and then possibly speculate the role in which highrises play as important pieces within a city.


Was this considered, or was it not a part of the project brief?
15. adam grassi
5.24.08 / 2pm


@Wien8836:  

[Have we met?]  

Interesting question. It certainly was posed in the project brief. (See LW post “dumb boxes.”)  

We approached this project with a specific goal of redefining tower interiors and questioning the given scenario of site-shaped floor plates stacked to the heavens. But as the project morphed into a 1:1 installation (it was not initially conceived as such), the human-to-tower relationship changed. Normally something like 1:100 (6-foot human, 600-foot tower), it became roughly 1:3 (6-foot human, 20-foot tower). The issue then ceased to be a “stacking” of people-spaces, but a desire to make the grounded visitor feel the tower's verticality. I feel that this is a more honest way to experience a tower, whose primary characteristic is its height.  

It is peculiar that a skyscraper's external expression is of infinite vertical expanse, while every effort is made on the interior to compress floor-to-floor heights for maximum revenue.  

It was important for us to keep in mind that these things are not scale models of skyscrapers, but 20′ volumes with their own sets of qualities and capabilities.
17. Wien8836
5.26.08 / 12pm


that makes sense. no, I doubt we have met, unless you have been to the Angewandte in Vienna this past year.
19. [Jonathan](http://www.mr-martini.com/archislave/)
6.4.08 / 4am


*“Their interiors consist of stacked-up floor plates, maximizing leasable or usable floor area, and in urban centers where groupings of towers crowd together on the most expensive land, the spaces between the towers are ignored. No doubt, these conditions result from the single-minded interests of commercial developers and the isolation enforced by private property ownership.”*


I certainly agree that space tends to be forged from commodity rather than as a progeny of function, movement or form. This is certainly a failing of the developer, but I would argue that model codes, zoning ordinances and, by extension, an overly litigious society are as much to blame for restraining design within the vice. 


I have a project right now, dense urban residential, 4-stories, which has open corridors and is located in a very warm climate. We've proposed a thermal siphon concept at interior corners, open air shafts with a sculptural metal canopy on top that would help create a natural circulation of air. However, by breaking that strict notion of horizontal, linear level fire area separations, we create, by definition an “atrium” and bring with that moniker a whole host of new (an expensive) requirements which preclude its inclusion in the design. Long story short, no variance will be allowed to the models codes on this point, not because it is any less safe, but because the officials fear litigation and will not deviate from their gospel.


I don't know that there is a solution, but still, its unfortunate.
21. [organicMobb](http://organicmobility.com)
9.21.08 / 9pm


This project was very site specific. Some of you have said that it is not. If you did not go to Higgins Hall to see and experience it for your self you are at a loss by thinking it is autonomous. The view from the top of the stairs down the main oblique shaft is beautiful.
23. [VERTICAL VIBRATIONS « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2009/07/05/vertical-vibrations/)
7.6.09 / 12am


[…] refer to the post TOWERSPACE on this […]
25. [Liam Ahern](http://www2.binghamton.edu/watson/features/student/ahern.html)
11.24.09 / 1am


I agree with will. Where does a plan involving four towers become one tower, with respect to these relationships: one isolated project?
