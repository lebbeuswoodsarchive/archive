
---
title: O, ORDOS
date: 2008-05-07 00:00:00
---

# O, ORDOS


[![Ordos overall development plan](media/Ordos_overall_development_plan.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-ordos1.jpg)



At first glance, the Ordos 100 project looks like a great chance for younger architects to build something exciting and significant. Located on a barren desert site near the Inner Mongolian city of Ordos, the development of a grouping of one hundred houses for the wealthy is in the planning and design stage. The developer, who has made his fortune in coal, asked the Chinese artist Ai Weiwei to plan and organize the project, and he, in turn, has asked the Swiss architects Herzog and de Meuron to select—from their personal “network”— a hundred architects to design the houses. The idea, apparently, is to create a showcase for up-and-coming design talent, and a highly marketable, and profitable, real estate venture.


 A hint of trouble appears when we notice that Ai Weiwei's design company is called Fake Design. Sure enough, when we look at his overall plan for the development, we find that it copies American suburban tract developments from the 50s, say, in California's San Fernando valley. Cf. the movie, “The Two Jakes.” Sand-blown, treeless, lifeless for all human purposes, but soon to contain “your Dream House”— just sign here! The picture published in the New York Times of the invited architects surveying their desolate sites is absurdly comic and at the same time sad. What must be going through their minds?  Is this the Weissenhof Siedlung for the new age? Can I make great architecture here? Will I be mentioned in next Times article? Or, did I come halfway around the world for this? Am I here as an architect, or as a pawn in Ai's latest art game?


The idea of building large private houses on three-quarter acre plots jammed together without regard for the spaces between or the relationship of one house to the next must be unsettling to many of the invitees, especially considering the history of American suburbs. Some have questioned the lack of even basic design or ecological guidelines in the planning, and may be wondering, too, if Ordos, of all the rapidly developing places on the planet, really needs a retro typology—however updated and upgraded—as the most visible symbol of its future. It would be a more hopeful harbinger of the future not only for this city, but the field of architecture in general, if a number of the Ordos 100 architects banded together and came up with a coordinated overall plan and insisted that it be adopted. And, if it were not, they would simply decline the opportunity.


LW


[New York Times article](http://www.nytimes.com/2008/05/01/garden/01mongolia.html?scp=1&sq=Ordos&st=nyt)


[Ordos 100 website](http://www.ordos100.com/index.htm)


 




 ## Comments 
1. Diego Penalver
5.8.08 / 12pm


Hi Lebb,  

To think that only here in this country (venezuela) there is a deficit of more than a million homes…a calamity.
3. [lebbeuswoods](http://www.lebbeuswoods.net)
5.8.08 / 1pm


Diego: The relationship of architects—and, consequently, architecture—to economic class, is another, but strongly related, issue. The point here is that if the rich cannot reach higher than this, then it does not bode well for the prospects of the rest of us.
5. spencer
5.9.08 / 6pm


Diego,


At first I though I want to be a part of this to fulfill some desire to express an idea on housing but on second thought I agree with you. this is an example of the tramping around of our profession to the whims of those with money…see Dubia for another example.


You could use this as an opportunity to advocate for architect's to prioritize and redefine where our needs are best in service to. I'm not sure what this could materialize into but I'd be willing to help create it if you are up for it.
7. manolo marquez
5.9.08 / 6pm


Considering the amount of money involved, the urban layout seems mediocre, absurd, and lacking any interest for community, leisure and green areas. This cluster at best, resembles a mediocre revival of those 50's suburban neighborhoods you talk about.  

There are better examples of aspirational middle class neighborhoods in mexico which at least attempt to achieve a sense of community by bringing parks, gardens and trails within. Its really sad to see another opportunity to redesign and imagine new ways of living go to waste, just like the case you presented before, with Rem Koolhaas' Manhattan clone chunk implanted in Dubai.  

The press and media attention of having 100 young firms designing at unison seems to me a (not so) cheap trick of marketing, and profit is the main architecture premise today.
9. Josh Horowitz
5.9.08 / 9pm


Hi Leb, Diego,  

Architecture is dead. A return to indigenous design is needed.
11. amp
5.10.08 / 4pm


Josh – interesting proclamation. The only indigenous craft of the American people at this point in history is a tricked out myspace page. Can't live in it – but it is work of the people, from the people and for the people.
13. Elan
5.11.08 / 7am


I remember reading that H+dM said their bird's nest is a political breakthrough because people will pick their own seats, and in that regard it will develop democracy. (since certainly 1.4 billion people will use it). I wonder if they are thinking, if we give a bunch of rich people a boring community it will make them feel out of touch. In either case it seems more like an excuse than an idea. They should make all the houses empty inside and put the money into the landscape.
15. Josh Horowitz
5.11.08 / 5pm


Free space is not a function of architecture or form of any kind. It is more a function of culture. Form ought be more symbol than anything else.  

Form does not contain space as we have been hoodwinked to believe. It most certainly does not limit quantum, cosmic or mental space.
17. Diego Penalver
5.12.08 / 1pm


Hi to all,  

Josh, nice to hear from you.. so many years!


Lebb,  

Following your thoughts, does´nt this project seam like an architectural collection of a sort, where all architects have been called to solve nothing, or a very conventional program at best, an architectural figure show?  

I could imagine a whole set of interesting and relevant architectural problems-situations for young talents to contribute.(Spencer..;-) And I imagine you too..  

Josh: I think architecture is still a relevant factor for humanity, for society, you can actually be a determinant element to human welfare, happyness, from the very simple to the most complex, if this is what you call indigenous design: I´m with you.
19. elan
5.13.08 / 4pm


josh, ‘form does not contain space' is perplexing. what do you think about gordon matta-clark's “Conical Intersect” in Paris? Is this form or space? i don't understand what you mean that it ‘ought be more symbol than anything'. are you facetious? my sense is: if the void is a kind of figure that is public, this seems to be what is disregarded in the master plan for Ordos. the emphasis of ‘the object' reinforces this collective disregard.
21. amp
5.13.08 / 6pm


Interesting Josh.  

Perhaps meaningful form can be symbolic, where as abstracted form is phenomenal/semiotic (not that those two are directly related). To take the old cliche out of the trunk, it can be anything to anyone. But space is where we live out culture, form is where we politicize it (and interestingly enough, deposit images of culture into the collective)


Elan – That work reminds of how weak, in an electrical sense, spatial-formal implication is. An observer's attitude directly effects the intelligibility of a concept over purely abstracted or mathematical relationships. Matta-clark's work is interesting because it uses the mathematical (conical section) abstraction of volumetric geometry to foreground the attitude of the observer in relation to the given context (the apartment, cultural field).
23. [Dave "Diamonds are forever"](http://intheblackbox.blogspot.com)
5.13.08 / 9pm


Diego, I am drawn to your ideas of this being an “an architectural collection of a sort”. While these structures are going to be commercial permanent residences, all i can see is an opportunity for these architects to let loose. 


The universal expositions and world's Fair of the past offered architects a similar opportunity. With the bureaucracy of modern life, and the severe lack of space in most western countries this would be nearly impossible today in a western country. Looking at old photos of Paris with the temporary pavilions of each country creating an amazing juxtaposition along the Seine I cant help but draw a comparison to what could be achieved here. As i have already admitted, the structures will be permanent and must be functional but the opportunity for architectural development is huge. 


It is in these rare events that changes are made possible, and it is plainly obvious to me that we desperately need a change. Popular Architecture has become global, no longer regional, the Zeitgeist has removed local character and with this the opportunity for gradual development within the architectural medium has disappeared.


If a change is going to be made, or a new direction discovered it is going to have to happen at a time like this, with an opportunity like this, in a place like this… where these architects have nothing holding them back, feel no obligation or connection to the local or international stigma's that would restrict expression in the western world.


There is nothing but positive development in this venture, i don't think the planning of the site takes from this. The buildings themselves will be the talking point in years to come and perhaps if anything the retro planning of the site will challenge these architects to create new unexpected relationships within what can only be considered an exhausted and boring residential plan.
25. amp
5.14.08 / 6pm


Perhaps there is an architectural anxiety about the voracity of the sitelines circumscribing a space that is a true ‘free for all' – a true free space. Who has power over who when the only enforced architectural relationships are infrastructural? 


Fabric and monument are interesting considerations – old Europe shows what this archetypal image of the city looks like. But consider New York – the fabric and its monuments are held in check by the value placed on the quickest, cheapest methods of construction. The emotional impact of concentrated differences in scale and articulation were far less important.


Is it not exciting, even frightfully so, to imagine a city where each lot is an architecturally independent nation-state?
27. [Will Vachon](http://archplan.dal.ca)
5.15.08 / 11pm


I agree. More context is needed. I guess I'm just not convinced the homes will be any good. I feel sorry for the designers involved. At such a scale, you would think something more interesting could be drawn up if they would work out a master plan. And yes, Mr. Woods, I agree that we're in trouble.  

What the profession needs is less coffee table books.
29. Josh Horowitz
5.17.08 / 2am


Space cannot be controlled or “contained”. There is space everywhere there is matter. To think of space as something which is captured between four walls and a roof is incongruous.


All material reverts to energy. All energy reverts to light. Space reverts to light (stay with me). The equivalence of matter and energy is a function of the speed of light. Where there is light there is space. No light, no space. The darkest of singularities (Hawking) emit no light and are virtually without space. 


My point is that I think we ought to reimagine what we mean by space, which is curved, by the way. Not to mention loaded with electricity.


Our experience of architecture is primarily a function of light…….and symbol. Why do we feel a certain way when we encounter architecture? For instance, a church, just for the sake of discussion. The classic plan; nave, apse, side aisles is not functional, it is symbolic. The design a cross, one of our oldest symbols. The stain glass allows light to penetrate. We feel elevated. It is not the “space” which inspires us as much as the presence of light and the symbol and the iconography. Maybe the incense. .


Architecture should really be reserved for public buildings. Ordinary (dumb) boxes do not need architects. Indigenous ingenuity is enough. One of the problems ,however, is that the public “space” has become corporate space. We have lost a lot of public space recently. Here in San Francisco we used to have Candlestick Park which owed its name to Candlestick Point upon which it was built. It is now known as 3Com Park. The new downtown stadium is called AT&T Park. The old Boston Garden where the Celtics have always played is now known as TDBanknorth Garden. Now this may not seem like much unless you like sports, but I think it is indicative of what is happening to the commons. It has become almost entirely usurped commercial interest. It is one thing to build architecture for a corporation and another thing entirely to build for the public. It is a different mission, and so the integrity of the profession is sullied.
31. Diego Penalver
5.17.08 / 6pm


Hi to All,  

Dave,  

It´s possible to have innovation happen in a development like this, it will depend on the participant´s talent and expertice and probably it will be the case of isolated examples. But as a whole, the development does not reveal anything new in respect to urbanism and settlement nor general architectural intent. When you review experimetal developments like the Weissehof Siedlung, as mentioned by Lebb, where the master plan was design by Mies Van der Rohe, and participating architects where important exponents of the modern movement, you see here a clear intent to materialize new conceptual ideas shared and discussed (not always succesfully) by the architects of the time, and this involved not only the individual intervention but the whole urban proposal.  

When you had in California the Case Study House development arround 1945.. organized by Mr. John Entenza. The project included many new and prominent architects like Richard Neutra, Pierre Koenig, Charles and Ray Eames, and many others. In this case these houses were not intended for the rich, as a chowcase of fancy, but they were an effort to design and build afordable, modern and experimental prototipes for the housing industries, the models of which are still very influential today. For me they are very interesting examples of american architecture at it´s best, in spite of their modesty.  

In many cases like these, through history , the distinctive gesture of a collective effort of this kind is a definite and well structure intent, the need to solve certain problems that would lead to the betterment of the profession and what it has to offer.  

Aside from the general urban scheme that does not say much to me, here is the explicit intent from the developer:  

“Mr. Cai, who …, said he conceived the Ordos 100, as the residential development is called, as a way to raise both the region's profile and the aesthetic acumen of its newly affluent residents.”


It is obviously a commercial proposition of housing a bunch of rich people, and to make it more atractive to them they are bringing in an army of international architects (hopely good ones). Good opportunity for them to have a job (I wouln´t mind..) but an insignificant endeouver for the overall of things. MHO…:-)
33. Diego Penalver
5.17.08 / 6pm


Sorry for my spelling errors, english is not my mother tongue, I´ll try to get better.
35. river
5.18.08 / 7am


It is Walter Gropius' birthday!


I think a blasphemous Chinese knockoff of lousy American Idealism is a better place than any on Earth to bring new talent to bear! 


Can a precisely measured and realistically lit, virtual alter-Ordos be conceived, organized, and exectued before the real one takes shape? 


A dumb, fun idea. Very silly. Stupid really. Run from it!


Where is Ordos from this widescreen LCD I'm staring at now? Left. right, front, back, up, down, before, after, light, dark, in, out  

Frak!?  

Google it. It's right after Feeling Lucky and right before Ordos100.com.


Hint: a sci-fi game-related Wiki entry about things creative young people did with Frank Herbert's novel universe after he died, just after the personal computer took off in my adolescence..


Eenglish is not my mother tongue either, though by all jurisprudence it should be. (I was born and razed in the Indiana Sixties-Seventies, when America still had undeveloped quiet places at it's heart. They are bustling with new families like mine now)


I will try to get better too.
37. [Will Vachon](http://archplan.dal.ca)
5.18.08 / 5pm


I would encourage any interested in this Ordos plan to read up on some of the “Garden City” pioneers, specifically Thomas Adams. I live in a city (Halifax, Nova Scotia) which was the site of particularly successful development by Adams, built after the Halifax Explosion of 1917. The Neighborhood, for those unfamiliar, is a 23 block subdivision in peninsular Halifax which was intended as re-housing for the population in the short term, but has since seen several generations of tenants. Only in the late fifties, in fact, were the units sold to private owners, most of whom had been tenants themselves. Today the area remains a thoughtful yet financially accessible homestead within walking distance to downtown, defying the usual logic of low-rise/high-cost housing.  

I cite this example not as a template how development should happen, but merely as something interesting to ponder as precedent.  

The Hydrostone neighborhood housing scheme is proof that successful neighborhoods must, aside from architectural intervention in buildings, be concerned with contextual issues.  

It might sound simple, but I believe these are well-learned lessons.  

I'm open to review if I've been naive and overlooked something, being still a student and whatnot.  

Thanks.
39. Nathan Bishop
5.27.08 / 9pm


Sounds like the Weissenhof Estate all over again. Sometimes I think architecture really has not progressed very much since the inception of Modernism. Henry Van Der Velde and Hermann Muthesius had a debate in 1914 at a Deutscher Werkbund conference, in which Van Der Velde argued for the individualist outlook while Mutheius defended the philosophy of the typical. 94 years later and we are still having the same argument.
41. [Projetos Urbanos » Ordos 100](http://www.projetosurbanos.com.br/2008/05/29/ordos-100/)
5.29.08 / 2pm


[…] Comments by Lebbeus Woods (US) <https://lebbeuswoods.wordpress.com/2008/05/07/o-ordos/> […]
43. [On Lebbeus Woods and Architecture | varnelis.net](http://varnelis.net/on_lebbeus_woods_and_architecture)
8.25.08 / 1pm


[…] Has the profession (and increasingly architecture is a profession not a discipline, incapable of a critical or even intelligent discourse) produced any architecture of value in the last decade? Not much. I can think of Casa da Musica, which is a great building, and perhaps the Seattle Public Library, which is a good building, but OMA's well has run dry. I don't think much of the firm's embrace of authoritarianism or by the revamp of Eisenman's Max Reinhardt Haus for CCTV and Rem's last good essay, "Junkspace," was written before 9/11. Herzog and de Meuron haven't offered anything since the 1990s. Other firms fair worse. Gehry, which epitomizes the boom, has been a free-fall disaster. Like puppies distracted by biscuits, architects have been so eager to build that they have unable to see anything else. Who among them would write a critique of the next hot place, Ordos, like Woods did here? […]
45. [ORDOS REVISITED « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/10/20/ordos-revisited/)
10.20.10 / 2pm


[…] couple of years ago, the architectural world was buzzing with news that a hundred or so up-and-coming architects had been chosen to design villas in a new real estate […]
47. [AI WEIWEI'S COURAGE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/13/ai-weiweis-courage/)
1.13.11 / 5pm


[…] prominent Chinese artist, whose architecture firm—Fake Design—I have criticized in a previous post, nevertheless has my respect for his courageous stand for art and political protest. The […]
49. kabukin0
1.15.11 / 12pm


You misunderstand ordos. It is not a responsible attempt at the perfect plan with perfect houses. I see it more as an architectural document, of architecture with a disregard to the context. The houses are to some degree self referencing, and without much care for the inbetween spaces, but they are also so large, that the outdoors is pherhaps uninteresting. 


The fact that it is also built in such a short time adds to it having the same property as a document of its time , as the more industrial siblings in the usa.
