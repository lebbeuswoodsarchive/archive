
---
title: LINE
date: 2008-05-05 00:00:00
---

# LINE



[![D. Libeskind, Micromega, 1981. One of the greatest architectural line drawings.](media/D._Libeskind,_Micromega,_1981._One_of_the_greatest_architectural_line_drawings.jpg)](https://lebbeuswoods.files.wordpress.com/2008/05/lwblog-line23.jpg)


*D. Libeskind, Micromegas, 1981. One of the greatest architectural drawings in line.*


Even though I am best known for my drawings, and have spent many years as a teacher of architects, I have never taught drawing. The reason is that each person who wants to draw should devise his or her own way. It makes no sense to teach a method or style of drawing, because drawing is a way of thinking, and it would be wrong to didactically teach a method or style of thinking. Each person must learn from the drawers—and the thinkers—who appeal most to them, and then devise their own ways. Originality—in drawing and thinking—is important, for the same reasons that individuality in all matters of existence is important: it confirms the wonder, and the terror, of the human condition.


 Essentially, each of us is alone. Biologically and psychically we are not only separate from other living things, but also from others of our kind. We cannot feel or think what another person feels or thinks, nor can they know exactly our feelings or thoughts. Because we are social creatures and dependent upon each other, we spend much of our time and energy trying to communicate our thoughts and feelings, and also to understand, as deeply as possible, those of others. It is a uniquely human task and one that requires all our intellectual effort, emotional commitment, and expressive skills. Spoken and written languages are the foremost of these, but drawing—in all its variants—runs a close second. Indeed, as we know, there are ideas and feelings that can only be expressed in drawn form. We might imagine, if we look at the caves of Lascaux, that drawing came before writing and was, in its narrative making of marks, its source.


 When very young, let's say in my late teens and twenties, I had a fierce desire to communicate my thoughts and feelings through drawing. In high school I had some mechanical drawing courses, where I learned to use t-square and triangles, but little else. For inspiration, I had to look in magazines and art books. I was tremendously moved by the etchings of Francisco Goya, the drawings of Peter Brueghel, the ink and watercolor drawings of Paul Klee. And, of course, the drawings of Michelangelo Buonarotti and Leonardo Da Vinci. Each artist conveyed something different in their work, something that resonated with my own sensibility, but did not sum it up. Different as these artists were, they all had one thing in common: a mastery of line.


 What I mean by ‘line' is exactly that: a single mark, short or long, drawn with a pen, pencil, stylus, or any sharply pointed instrument that is held in the hand and commanded by it, in coordination with the brain, to inscribe on paper, tablet, plate, or any chosen surface exactly that mark and not another.


 This last qualification is important. When rubbing a piece of charcoal, pastel, or blunt pencil on a surface, one accepts (even hopes for) a certain degree of approximation, even of accident. The resulting tone is, from an analytical point of view, vague, when compared with line. Line is precise and unequivocal. It is here, not there. Making a line is not about accidents. Rather, it is about contour, edge, shape. It is about where one space begins and another ends. It can be spontaneous or studiously deliberate, but it always carves space in a decisive way. It has a clear ethical, as well as aesthetic, impact. The drawn line is one of the great human inventions, and it is available to all of us, a tool both common and esoteric, personal and universal.


LW 


 



 ## Comments 
1. amp
5.6.08 / 5pm


I share your affections for the world of the line. I was taught, and continue to learn, by imitation and my own initiative. If it weren't for the instruction of others I wouldn't have learned how the lines of drawings from eastern cultures vs western cultures reflect a different way of being. Had I not worked these in conscious imitation, I wouldn't have come to know the difference. Line is seductive because of its precision, even when its body is highly implicit. The following is a website with drawings I have come to love and read through over and over and over again – the line which can be so ‘dead' to some can also create music. This ability I admire and cannot fathom. 


<http://pickler.net/thesis/process/>
3. sander boer
5.7.08 / 9pm


Dear Lebbeus,  

I love your work and you're also pleasant and inspiring overall, but are you suggesting that architecture=image?  

We are architects, not marketeers with their flip-over, not designers with their renders, not politicians with their agenda and not artists with their twisted view on reality.  

Or are we?
5. [lebbeuswoods](http://www.lebbeuswoods.net)
5.7.08 / 10pm


sander boer: Drawing is essential to design, for architects and artists alike. As architects, we must master drawing and, in doing this, we can learn a lot from artists, just as they can from us.
7. Luke Pulliam
5.9.08 / 2am


Hi Lebbeus,


I have always preferred the malleability of charcoal. With the mark of the pen or the hard drawing line, one has to maintain a consciousness that they are working from a light value contrast to a dark contrast.  

But Charcoal can adroitly move form dark to light and back again without any ambiguity of presence. Of course the eraser plays a key role to this approach to drawing, but I consider erasing apart of my analytical drawing process. 


Thanks for the thoughts on the line.


luke
9. spencer
5.9.08 / 6pm


Luke makes a good point that drawing is more than what we do with one tool. I've always appreciated Lebbeus' use of pen and colored pencil in the same way I enjoy and am inspired by Lars Lerup's use of those tools.


My greatest inspiration comes from an illustrator friend of mine who has transgressed physical drawing with the digital. He has mastered both tools to both streamline and enhance his desired result. When I first met him his skill with marker, pencil and pen was unparrelled. He since has adapted to our digital world while retaining his high skill level as a hand drawer.


This is particulary emphatic to me when I see new people coming into our work who can hardly draw thier ideas by hand. Maybe I'm still stuck in the stone age.
11. aes
5.10.08 / 5am


Drawing, which is derived from the same root as the word ‘drafting', has always implied the action of pulling (as one would draft water from the depths of the earth), and is then fundamentally connected to the line by both a literal and conceptual tension (i'm thinking about cables as opposed to columns): that is, the tension between the technically trained, practiced hand; and the roaming, uninhibited imagination. In a way, the more powerful the two dialectical forces are in resistance to the other, the tighter, more precise the line that holds them in tension. One pulls, instead of pushes, for clarity and control.


And, of course, there is also the other tension, between the drawer and the drawn, between the pencil-point just above the paper's surface and the white leviathan just beneath.
13. Josh V
5.12.08 / 12pm


To me, a sketch shows thought process. I've always preferred working with a pen on my sketches to prevent myself from erasing. I want to see my mistakes or places where I change my mind and moved a line. It can show where I started and where I ended in one drawing. This is just my style. [I've been told I have a very “hairy” style of sketching because I often draw over the same line multiple times, haha].


I like how you address the fact that each person can express their thoughts in their own style of drawing. A friend of mine draws site analysis and conceptual studies similar to Liebeskind, but neither his or Leibeskind's drawings mean anything to me.This is not to say they are worthless. Obviously, if they benefitted the artist, then it has worth. I can appreciate that fact, but if these artists are to publish or display their sketches, descriptions of their methods or stream of thought might help understand. Otherwise, it's just a pretty image to me, drawn in a foreign language.


I have younger coleagues who sketch in absurd, abstract styles for the sake of being different. When asked what they mean, they reply, “I dunno”. What worth is that? Their intriguing image is now a pile of rubbish in my eyes. Trying something different is good for learning, but know what your doing. Have intent.
15. glen
5.12.08 / 10pm


Lebbeus, I've had some difficulty with the concept of ‘line,' in particular what does it represent and where do I locate it. Looking out my window just now, I search for the line and am unable to see it…anywhere, only abstractions that were perhaps conceived-that had an origin in another abstraction…


A line is impossible. Look deep enough and it's true identity will emerge…broken by surface or molecular fragmentation. I'm wondering if digital lines are possible, then again ‘digital' is another means of abstraction…


Perhaps abstractions are like Wittgenstein's proposition on language, that they are successful to the extent that they are general..and so ultimately we are alone.
17. [pixo](http://pixohammer.smugmug.com)
5.14.08 / 6am


The artist whose lines I admire most is Picasso. I can stand in front of his paintings for hours, “reading” them line by line, “tasting” the pictorial spaces formed in between. 


For my own approach to drawing, I look at handwriting for inspiration. I find two characteristics of handwriting to be useful. First, handwriting uses simple lines that anyone can learn through practices. Second, a written word is composed out of resusable writing-parts such as alphabets. Yet amazingly, everyone writes differently; everyone developes its own style of writing over time. And some even invents its own words.


It is probably not incorrect to say handwriting has evolved out of drawing, and over time, developed its own set of characteristics. My approach to drawing is to apply these characteristics of writing back into drawing – i.e. use simple lines, and apply reusable drawing-parts – treat drawing as if it is a form or writing.
19. betadinesutures
5.15.08 / 1pm


for me the line or mark on the page has been the “incision” and has in some way rendered a permanence that charcoal can never achieve. 


anyone that has laid down a line on vellum, with sufficient force, and then “tried” to erase the mark, can attest to the presence of the cut. the graphite is removed but the |graphic| remains, it is the memory, the scar of the violent act let upon the skin.


|graphic| 1610, “traced” (implied in graphical), from L. graphicus “picturesque,” from Gk. graphikos “of or for writing, belonging to drawing, picturesque,” from graphe “writing, drawing,” from graphein “write,” originally “to scratch” on clay tablets with a stylus. Meaning “of or pertaining to drawing” is from 1756; that of “vivid” is from 1669, on the notion of words that produce the effect of a picture. 


thank you lebbeus…
21. joe
5.15.08 / 11pm


just as the drawing is an interface to relate thoughts and ideas to other people, the line is the interface relating distinct spaces. by creating a line, an instintaneous relationship of seperate space is defined. i remember kandisky talking about how each of his pieces of artwork were their own universe. the line is what brings space together and allows us to see the bank across the river.
23. river
6.9.08 / 7am


Check the check box [here](http://www.hitta.se/3d/3d_splash.aspx) beside ‘Jag godkänner villkoren'. (Which means in Swedish that ‘I agree to the terms')


Then click on the ‘Till 3D-kartan' button.


If you have a Java-enabled browser, this will allow you to view some completely new kinds of lines which are, to me, both fascinating and incisively relevant.. And not just for Stockholm, or Van Gogh fans worldwide. 


This is a novel and gestural expression of an emergent artistic property based on such rigorous technological constraints, that style was presumably low on the list of the developers initial priorities.. Or maybe not, since SAAB was the parent company? Yet in it's presentation, it is the style of this presentation that really takes one's breath away, its apparently implicit agreement with surrealism, from a computers perspective. I can't imagine finding a more relevant link, for this moment.


I hope you will click it, and then speak about what you think, in the first person, of what you see there.


I found it a half hour ago by way of [this link](http://news.slashdot.org/news/08/06/07/2024254.shtml)
25. [organicMobb](http://organicmobility.com)
9.21.08 / 8pm


Intrinsic to the line is the rawness present in your own mind. Personally I have found drawing from the subconscious, as Faulkner did in writing. The idea of it I have found to be superb. This is not to say that any of it is random. Imagine a chest of drawers, I simply create the chest. The drawers will manifest themselves along the way. The idea's I have prior to a piece resonate throughout the action of drawing. The important thing is that I allow them to be expansive and to grow through the development of the piece as a whole.
