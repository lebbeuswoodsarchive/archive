
---
title: PRETTY DAM PURE
date: 2010-10-25 00:00:00 
tags: 
    - aesthetics
    - dam_construction
    - ethics
    - Sotirios_Kotoulas
---

# PRETTY DAM PURE


In my last post I mention the idea of “pure engineering and construction.” I wanted to give some examples and, as good luck would have it, I just received an email from a colleague, including a report of his summer work, that perfectly illustrates what I mean by ‘pure:' works of design unspoiled by styling or other arbitrary aesthetic impositions. With his permission, I share it with you here.


LW


.


“I left Puerto Rico in early June and went north, far north to work….I was a masonry coordinator and foreman at a new hydroelectric dam project—[Wuskwatim](http://www.hydro.mb.ca/projects/wuskwatim/status.shtml). It was one of the most incredible experiences, working with 600 unionized people, living in a work camp, working 12 hour shifts and getting all the overtime double pay madness that comes with it! The work camp was pretty wild, we lived in trailers, ate at a common cafeteria, the food was decent, very health-conscious. Woke up every day at 4:30 am; first meeting with the other 10 project coordinators started at 6 am. I love that kind of responsibility—if I don't get mega responsibility I prefer to sleep in. The architectural drawings had missing details all over, and some pretty retarded masonry details as well, so I spent a lot of time making changes, issuing field memos and drafting rfi's with the engineers. LaFarge built a cement factory on site that was out of this world. Every time I ordered a specific batch of concrete it's slump and other factors had to be tested 4 times, even while it was being transported! super super precise. I've never been on a jobsite that was so precise. There were about 150 safety inspectors that trolled the site with cameras taking pics and videos of safety hazards, so I had to constantly make sure the crew was operating by the book. They kick out workers who grossly defy the rules—they give no [second] chances. The dam requires a very small flood footprint, 500 sq meters, to work and it is the first dam built with the local First Nations [Native Canadians] as major shareholders. It is a government project, pretty much all utilities are public in Canada. All the energy will be sold to the USA and helps pay for universal healthcare. The work camp has a baseball diamond, squash courts, a mini-cinema, pool hall, indoor basketball/floor hockey, a gym, and a bar where all drinks are 2 or 3 $ and last call is at 10:45 pm! The sun barely set for about 3 hours—the light was so intense. I loved seeing the welders put the turbines together—I even saw one lifted into place. Hydroelectric dams emit a lot of energy into the atmosphere so all the door frames had to be grounded to prevent painful shocks. Met many incredible people, the majority of the workforce was from Quebec and all the engineers were from Quebec so the site was thoroughly bilingual, sometimes only French, so I really freshened mine up in the middle of nowhere….”


Sotirios Kotoulas


 **Sotirios Kotoulas** is an architect who makes [exploratory projects](https://lebbeuswoods.wordpress.com/2009/01/22/seeing-space/) and currently teaches architectural and urban design and theory in the school of architecture of the University of Puerto Rico, San Juan. His book *[Space Out](http://www.amazon.com/Sotirios-Kotoulas/e/B001JOZWA4)* is published in the RIEA book series.


.


(below) Photos by Sotirios Kotoulas of the Wuskwatim dam under construction:


The river:


[![](media/dam-9a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-9a.jpg)


Excavations for the dam:


[![](media/dam-10a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-10a.jpg)


The dam construction site:


[![](media/dam-11a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-11a.jpg)


Into the earth:


[![](media/dam-16a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-16a.jpg)


The turbine room, beginnings:


[![](media/dam-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-5a.jpg)


Into the dam:


[![](media/dam-22a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-22a.jpg)


[![](media/dam-21a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-21a.jpg)


The turbine room, taking shape:


[![](media/dam-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-2a.jpg)


The turbine room, progressing:


[![](media/dam-13a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-13a.jpg)


One of the turbines, being assembled:


[![](media/dam-23a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-23a.jpg)


Deep inside the dam:


[![](media/dam-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-1a.jpg)


Emerging from within:


[![](media/dam-7a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-7a.jpg)


The dam, progressing:


[![](media/dam-8a.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/dam-8a.jpg)


#aesthetics #dam_construction #ethics #Sotirios_Kotoulas
 ## Comments 
1. [Tweets that mention PRETTY DAM PURE « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/10/25/pretty-dam-pure/?utm_source=pingback&utm_campaign=L2)
10.25.10 / 9am


[…] This post was mentioned on Twitter by Ethel Baraona Pohl, KpaxsArchi. KpaxsArchi said: PRETTY DAM PURE: In my last post I mention the idea of “pure engineering and construction.” I wanted to give some … <http://bit.ly/90hfPI> […]
3. [Pearce](http://www.carbon-vudu.com)
10.25.10 / 2pm


Amazing and beautiful.
5. Dimitris
10.27.10 / 6am


Hey Sotiri, it looks chaotically pure
7. Evan
10.27.10 / 12pm


The images of the dam and the idea of “pure engineering and construction” reminds me (superficially perhaps) of the images in Paul Virilio's “Bunker Archeology”.  

<http://www.papress.com/html/book.details.page.tpl?isbn=9781568980157>
9. Honus Wagner
10.28.10 / 7pm


Hey Leb,


When are you gonna build something like this? 


Lets quit dicking around with comic book drawings, and build something for once.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.29.10 / 12am
	
	
	Honus Wagner: A question: Is building “something” all that matters for you?
11. Pedro Esteban
10.31.10 / 10am


Do you refer to the make of architecture guided of mechanical principles?
13. Pedro Esteban
11.2.10 / 8pm


The question here for me is:  

Why looks so chaotic if is designed by estricted laws?  

How a pragmatic rule can at the end make spaces so complicated?
15. [chris teeter](http://www.metamechanics.com)
11.2.10 / 9pm


Pedro I have a simple answer for you – when simple principles are full filled against a complicated world the feedback causes complexity. You continue to pursue the materialisation of a simple principle and in this mission pay little attention to all the various methods and actions you perform to make it happen. 


Lebbeus previous post about the artistic poetic interjection, this approach addresses the little things called feedback that confront the vision. The poetic solution is less violent usually. 


Blind design versus vision, hard to tell the difference sometimes.





	1. Pedro Esteban
	11.2.10 / 10pm
	
	
	Thanks for the answer, It's in the list of things who has passionated me so much.
	
	
	Please can we keep in contact, I have others questionns?
	3. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.3.10 / 12am
	
	
	Chris: Thanks.
17. Ben Dronsick
11.12.10 / 7pm


Did anyone else catch: “all the energy will be sold to the U.S. and pays for universal healthcare.” ?
19. [SOTIRIOS KOTOULAS: Emission Architecture (updated) « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/01/27/sotirios-kotoulas-emission-architecture/)
2.1.11 / 1pm


[…] Kotoulas continues his adventures in electromagnetic wave worlds, back at the Wuskwatim Dam. He […]
