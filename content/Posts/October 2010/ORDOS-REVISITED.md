
---
title: ORDOS REVISITED
date: 2010-10-20 00:00:00 
tags: 
    - China
    - housing
    - real_estate_speculation
---

# ORDOS REVISITED


A couple of years ago, the architectural world was buzzing with [news](https://lebbeuswoods.wordpress.com/2008/05/07/o-ordos/) that a hundred or so up-and-coming architects had been chosen to design villas in a new real estate development in Ordos, Inner Mongolia. While I am not aware of the present status of that project, the latest New York Times [report](http://www.nytimes.com/2010/10/20/business/global/20ghost.html?_r=1&hp) on Ordos puts it and other such opportunities in perspective.


It is hard to say which aspect of this perspective is more unsettling—the irresponsible overdevelopment and land speculation; the conspicuous waste of natural and human resources; the utter banality of the architectural design of the buildings; the paucity of imagination and innovation in the conception and planning of an entire, new urban settlement; or, not least, the realization that all of these aspects mark trends evident anywhere and everywhere today. This is not only a confirmation of globalization's relentless march into the future, with its bland homogeneity leveling all human diversity, but a depressing forecast of things to come.


LW


.


[![](media/ordos-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-1.jpg)


[![](media/ordos-1b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-1b1.jpg)


[![](media/ordos-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-2.jpg)


[![](media/ordos-2b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-2b.jpg)


[![](media/ordos-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-3.jpg)


[![](media/ordos-3b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-3b.jpg)


[![](media/ordos-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-4.jpg)


[![](media/ordos-4b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-4b.jpg)


[![](media/ordos-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-5.jpg)


[![](media/ordos-5b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-5b.jpg)


[![](media/ordos-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-7.jpg)


[![](media/ordos-7b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-7b.jpg)


[![](media/ordos-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-8.jpg)


[![](media/ordos-8b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-8b.jpg)


[![](media/ordos-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-9.jpg)


[![](media/ordos-9b.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/ordos-9b.jpg)


Credit for the above photos and captions: **Adam Dean** for the New York Times.


#China #housing #real_estate_speculation
 ## Comments 
1. Kiel
10.20.10 / 2pm


The last photograph is terrifying. Utterly anonymous. Veneer — social, political, aesthetic — victorious. 


What are your thoughts, Lebbeus, on developments like Masdar City? Disneyland gimmicks or catalysts for tectonic progress across the board? I like to think such islands will, eventually, become the sea — but the iron shovel of nearsighted pragmatism may be too powerful to overcome, sealing the fate of Masdar and the thousand little pseudo-progressive Eastern arcologies currently rising toward completion as isolated indices of our time, a la Brasilia, Islamabad, Curitiba, Canberra, etc.
3. [Francisco Vasconcelos](http://csxlab.org)
10.20.10 / 10pm


Hi, Mr Woods.  

Thank you one more time for presenting this view. 


I believe this is Terrible for severall human factors pointed to above. This is a result of the global direction that the architecture world is taking, which only purpose is to form an image, or utterly a way to hide a picture. This kind of speculation, neglets humans, human behaviours, human cultures, human phenomenology, human dimensions in all ways. For a plan of this dimensions in an zeitgeist shift times that we are on, it would be asked more than simply architectural competitions for plots, and simply seperated areas designed by real estates and construction companys.  

In a globalization process we shouldn't forget the hidden dimension of culture and consequentelly glocalization itself.


I'm sad, but won't give up. 


Best wishes,


p.s.: Sorry for my bad english, i'm not native.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.20.10 / 11pm
	
	
	Francisco Vasconcelos: Your intelligence and passion come through your English just fine!
5. [kuu world » WHAT HAPPENED TO ORDOS 100?](http://www.kuuworld.com/2010/10/what-happened-to-ordos-100/)
10.21.10 / 1am


[…] Woods comments […]
7. aranka
10.21.10 / 4am


i am most disappointed in this Ordos vision. The New generation of architects, designers, etc. help!! Stop your bosses (I'm in that age group) from continuing to think ‘ego' and whats only good for man and for profit! We've got to think ecology and the human as blending into the design of mother earth for new community designs. I worked in several countries (UK, AU and US, Austria a little) and these sort of developments are going up everywhere (a Calif. or Florida look) as a general model of ‘image' and lifestyle…. it's not sustainable, it is completely against the natural environments needs. Thanks everyone!!
9. [Francisco Vasconcelos](http://csxlab.org)
10.21.10 / 2pm


Thank you mr Woods for your compliment. I was completely surprised and overwhelmd.


As “aranka” wrote :


‘Stop your bosses (I'm in that age group) from continuing to think ‘ego' and whats only good for man and for profit!'


I had the oportunity to work at the office of Sou Fujimoto in Tokyo, with a scholarship from my government. I choosed Fujimoto's office, not because of his works, but because what I've read of what he said. has son I've entered in that office I realised that I was only meat for work. Ok I accepted, but has soon I've asked what was the relation of the projects with the client no one could tell me correct who was the client. So when I realize that the main argument in that office was that architecture must be “crazy” I couldn't understand why. Now I know. In that office it is more concerned the making of an object, indenpendently of the costumer needs, and also the actual needs. So it was my deception that in fact that was not architecture, it was sculpture, a very good sculpture but poor architecture.  

There was a Portuguese architect called Fernando Távora (the father of Portuguese modernism, our Corbusier) and it splashed into my mind his statement: “Architecture begins where sculpture finishes.” Indeed, architects today are onlu concerned in an final object imagery, becoming mostly designers. The architects of today, are forgeting, psychology, science, phenomenology, history, sociology, all subjects that an architect must study to comply with the human (client) needs. we have too much responsability, that the major of humans don't realize.


I would like to leave this paragraph of Richard Neutra's book  

The Late Essays:


“Contemporary architecture has revolutionized the appearance of our buildings and surroundings, but not always their sensory and psychological essence. The opulent hollowness of traditional design has been supplanted with the ostentatious sterility of an ostensibly rational style, but the stark, numbing structures of our own era have their own hollowness, often leaving us empty of energy and emotion.”


This is still happening today, and I believe the guilt are from the education, and mostly magazines. We live in a world that we see everything in images … magazines or internet.  

we became stuck to a 2d imagery, even the 3D's are used to make a 2D Render. 


“In nature there are flowing transitions and dynamic connections between all phenomena. Only man has imagined an intellectualized antithesis, giving everything a name, category, or classification. The phenomenon of one good world, pictured in the first chapter of genesis as having a primordial unity, perfection, and innocence, was almost immediately fissured into fragments. This has not only compartmentalized, factionalized and debased our comprehension of the material world, but also driven a wedge between it and our spiritual and moral values. All of human history has been a struggle to rediscover that primordial state, when the ‘whole earth was one language and one speech.' – Richard Neutra.


When I read architects like Neutra, they speak from theyr most inner human soul to practice architecture for human brothers.


Well I could be wrong, but when I look back I believe that there is more than we meet the eye. Personally i'm writing my master dissertation, in the fields of Quantum Physics | Phenomenology and Architecture, and I can see in my friends eyes, architects eyes, and teachers eyes, the discredit of my study, stating that architecture doesn't need phylosophy, and it should be construction.


Am I being too ambitious? Or too wrong … this is what makes me sad. Sometimes I losse my strenght in this country because everyone fights this, but sometimes, because everyone rejects this thinking it makes me strong enough to pursue what I think it could be valuable to architecture and to the users, that is US.


I'm sorry for this big response, but this is what I see today around me. Maybe I'm alone in this, or not.


Thank you very much, for the pleasure of exposing my ideas to you.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.21.10 / 4pm
	
	
	Francisco Vasconcelos: For what its worth, I agree with the views you express here. Many of our colleagues, and many others in the world, do not. So it would be easy to say that philosophical arguments are not going to stop or even slow down the engines of globalization that are transforming our human settlements in the horrific ways seen in Ordos. While this may well be true, it is not an absolute certainty, and those of us hoping and working for a better human—and planetary—future, may be more numerous than we think, and with more potential influence on events than we realize. The most daunting obstacle we in this seeming minority face is not the money and political power of the globalizers, but our own fear that we are isolated and powerless. Confronting this fear we should realize that great (or at least very good!) ideas are and always have been the greatest sources of power, and such ideas are not the property of any elite, but belong to us all. Keep working on your dissertation! Make it the best you can, publish it, make it accessible to as many people as possible, and put it out into the world where it can join with the understanding and actions of other thinking and caring individuals. That is the source of the real power to change the world for the better.
	
	
	
	
	
		1. [Francisco Vasconcelos](http://csxlab.org)
		11.15.10 / 6pm
		
		
		Mr Woods, your words couldn't be better.  
		
		My deadline is 15th of December, hope I can have more time to change dialogues with you and your blog visitors.
		
		
		Best Wishes.
11. [Chris Teeter](http://www.metamechanics.com)
10.22.10 / 3am


there is hope…Francisco would love to see your master thesis, right up my ally…PHENOMENOLOGY…


Ordos unfinished museum takes the cake for me. Obviosly all forms and spaces have been exhausted, not that meaningful to make a blob or a curve or a glass box anymore – it can be done.


i'd love to see it, or maybe i'll try it, an architect who designs neo-colonialism one day and contermporary blob ribbon the next – not because it's fashionable but because it's meaningful to the client – ontologically.


i just see little Lebbeus creatures infesting Ordos, like little angry natives making their case.





	1. [csxlab.org](http://csxlab.org)
	11.17.10 / 7pm
	
	
	There is hope … indeed.
13. Eric Ross
10.22.10 / 11pm


I see in these images the monuments of the biggest disaster of our time. If the Acropolis, the Pyramids, and other monumental works of architecture from our collective past represent the climax and subsequent fall of great societies and civilizations. I see a direct parallel in these images of Ordos and those of Dubai and the other developments mentioned above. In the long view of human history, I feel these projects will represent similar threads of progress and eventual self destruction. What will people say about us and our monuments of destruction in another 100, 300, 500 years.
15. [ORDOS REVISITED « LEBBEUS WOODS « Journal](http://www.bianucci.net/journal/?p=272)
10.26.10 / 8am


[…] ORDOS REVISITED « LEBBEUS WOODS. […]
17. [deepa](http://clinical-observation.blogspot.com/)
10.26.10 / 4pm


Ordos for me stands for most of the issues that deter me from practice. The random, unnecessary construction, design bereft of research, excessive focus on the image and the complete disinterest in acknowledging the responsibility the architect owes towards his site and its context. The above mentioned focus on the image is evident when practices are willing to spend huge amounts of money for a rendering, while refusing to invest in research and maybe sometimes refusing a project.  

I know this is a very personal experience, and I also know that there are excellent practices out there, and every time I speak of this experience, I am reminded that my opinions stem from time spent at a corporate firm; but that essentially seems the point to me, as it is often the large corporate firms that have tremendous influence globally, where nondescript californian homes are built all over, in Ordos or small towns outside Mumbai.Maybe its time to acknowledge the corporate firms and their influence and make them a part of our discussions.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.26.10 / 8pm
	
	
	deepa: When's the last time you ever read a comment on this blog or anywhere else written by a proud corporate architect in response to criticism of corporate architecture? I can't recall one. Either they don't read these sorts of blogs or posts, or they don't care to defend their work, or simply don't have much to say. Whichever, it is they who have refused to acknowledge; not us.
	
	
	
	
	
		1. [Francisco Vasconcelos](http://csxlab.org)
		11.23.10 / 7pm
		
		
		Hi, after sometime, I was reading a book from Frank Lloyd Wright, ‘Two lectures on architecture' for students in the Art Institute of Chicago.
		
		
		This was in 1931:
		
		
		“I again refer you to those simple, sincere attempts to be ourselves and make the most of our own opportunities which are tucked away in out-of-the-way places or found in industrial life as homes or workshops. Our rich people do not own them. Great business on a large scale does not invest in them unless as straight-forward business buildings where “culture” is no consideration and columns can give no credit.”
		
		
		So as Mr. Lebbeus said : Whichever, it is they who have refused to acknowledge; not us.
19. Honus Wagner
10.28.10 / 7pm


When did Lebbeus Woods become the Perez Hilton of architecture?
21. [ANOTHER ONE « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/12/13/another-one/)
12.13.10 / 11pm


[…] backed by state capitalism. In the case below, the place is Saudi Arabia, looking very like Ordos, Inner Mongolia, and Dubai's displaced Manhattan by Koolhaas. A formula that works, so it seems. […]
23. Pedro esteban
12.14.10 / 8pm


Nice detail the horses!!!! Lov'ya!!
25. [Francisco Vasconcelos](http://csxlab.org)
3.10.12 / 1am


Well, this should be interesting, a documentary about Ordos, by Ai Weiwei


<http://www.bravdesign.net/2012/03/ordos-100-documentary/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+bravdesign+%28Bravdesign%29>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.10.12 / 3pm
	
	
	Francisco Vasconcelos: Should be interesting, yes, but isn't—except in a negative way. This video just shows how silly this over-hyped event is. And the models shown by the invited “international architects” are at best B level student projects. The whole thing has the feel of a bad Ai Weiwei joke.
	
	
	
	
	
		1. [Francisco Vasconcelos](http://csxlab.org)
		3.10.12 / 4pm
		
		
		Exactly, has our previous conversations before, the present of today is stil blind-folded by means of promotion (I bet Edward Bernay must be very happy on his grave). And has I belive Ai-Weiei promoting this, is the “cherry on top of the cake” in terms of this neo-liberalist promotion of global culture.
		
		
		I don't wan't to be arrogant or seem a bad joke, but my belief today is that the majority of the architects, have became the “new prostitutes” of the corporative endeavor in capitalizing peoples lifes.
		
		
		 Has this being a bad Weiwi Joke, it could be a very powerfull one, if it was indeed a joke. But looking at the political-social aspects of China, it is not hard to believe that he could be lead to do something like this to keep him free. China has been known for being a maoist-governement succeding in the world of the capitalist-economy, now they are trying to beat the westerns in theyr own turf again, sell images and a material life.
		
		
		I can only remember the sentence of the movie “THX1138”:
		
		
		– “Be happy, …buy more!”
27. [Francisco Vasconcelos](http://csxlab.org)
3.29.12 / 11am


You Know Mr. Woods… Architecture is Dead. It has been 3 long years looking for work, I'm hungry, disappointed and sad.  

Architects only want to build their images and nurture their ego's. It striked me yesterday, architecture only exists in the architect's minds, the architecture dialogue is confined to some individuals in the profession, so if not even architects share architecture arguments, how will society understand the architects value? What strikes me was that I was speaking with several dutch, and what they said to me was:


 “It must be tough in your area, there is an “old style neighbourhood” being built in Amsterdam, and the construction office had all the plans and buildings designed in India by people that have never been to Netherlands.”


 So when the world is becoming like China, we architects should think if … are we needed anymore? No one seem to cares about human beings anymore, just an image and egos. It's no use for me having investigated and dedicated to architectural thinking, while in this world there only exists jealous people and egotistic people. I've been looking for work in all architecture offices in Europe, with no success, Portugal … Germany … England … Netherlands …. When I read the great thinkers of our time, I see that they nurtured the knowledge of each others … Neutra wrote to Einstein …. Foster worked with Bucky Fuller, there are so many examples of architects dedicating to architecture and that was enough for them to be unique and have the greatness of passing knowledge with people whom worked and shared they'r ideas. today … architects work alone in competitions (the big majority at least). 


So sadly, I will try to organize an Architecture and Phenomenology conference in Rotterdam alone while working at a peanut factory, gaining 9€/hour … better than working for Rem Koolhas at 1€ an hour …. and so this world is going. Where you gain more as a corporate slave (at least they have jobs) than you are compensated by the joy of working in architecture. I'm doing this not because my ego needs architecture and phenomenology to be debated, but my profession and the society need it, but I know that they don't want it, maybe another fail, but I believe.


So I believe the world has became: – peanut's





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	3.29.12 / 2pm
	
	
	Francisco Vasconcelos: I certainly understand the bitterness you are feeling. Anyone who is intelligent and serious about their life and work feels it at some time or other. Confronted with the massive amounts of hypocrisy in the field of architecture, it is hard to keep a positive outlook, especially when you can't earn a decent living. The way I have personally dealt with this kind of despair is to keep focused on what I care about and not get caught up in others' games. As a wise man once said, “I can't control what other people do, but I can control what I do.”  
	
	The gainful employment problem you describe is a very tough one. Having to work in a peanut factory just to survive has to be disheartening. I would advise that you look into teaching, if you have any talent for it, not as a “last resort,” but as a first resort. Teaching young architects-to-be doesn't pay well (but enough) but can be really uplifting of the spirit. Teaching allows—demands—that you direct your idealism and high aspirations in architecture to a greater, non-egoistic good. Also, you can keep up your own architectural practice with pencil and paper expressing ideas and designs that matter to you.  
	
	No matter which way you go, it is a tough slog. Just don't leave your fate in the hands of others.  
	
	I hope I haven't uttered too many cliches.
	
	
	
	
	
		1. [Francisco Vasconcelos](http://csxlab.org)
		8.25.12 / 11am
		
		
		Mr Woods, I must say I am terribly sorry for my words, and most certainly by express them here.  
		
		Life doesn't go well, but the architectural discourse that I've been reading through you, and cultivated by myself, has been my life suport aswell as my photography. I apreciate your kind words, and the will to reply to my expression. One more time, you gave me the strengh to pursue what I want, as in my master degree, your words are always full of wisdom and maturity. I'm still trying, but unfortunately time is passing through me, and I think I can't grab my time. I should have been born in your time Sir Lebbeus Woods; at least a time where persons and profesionals like you still care about each others, and not the time of today … the time where ego supports the self.
		
		
		I'm a bit sad that you won't continue the blog, but at least it was a wonderfull way of knowing your feelings towards the architectural practice. For now I wish to express all the good luck for your life and your new book, which I'm already anxious to read it. And thank you so much by writing and not posting photos with coments.
		
		
		One more time, it was a pleasure, and I hope to meet you one time in person.
		
		
		Francisco c.p. Vasconcelos
