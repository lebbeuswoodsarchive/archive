
---
title: TOWER OF POWER
date: 2010-10-29 00:00:00
---

# TOWER OF POWER


 


[![](media/mumbaitower-1b1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mumbaitower-1b1.jpg)


.


October 28, 2010
.
## Soaring Above India's Poverty, a 27-Story Home


###### By [JIM YARDLEY](http://topics.nytimes.com/top/reference/timestopics/people/y/jim_yardley/index.html?inline=nyt-per "More Articles by Jim Yardley")



MUMBAI — The newest and most exclusive residential tower for this city's superrich is a cantilevered sheath of steel and glass soaring 27 floors into the sky. The parking garage fills six levels. Three helipads are on the roof. There are terraces upon terraces, airborne swimming pools and hanging gardens in a Blade Runner-meets-Babylon edifice overlooking India's most dynamic city.


There are nine elevators, a spa, a 50-seat theater and a grand ballroom. Hundreds of servants and staff are expected to work inside. And now, finally, after several years of planning and construction, the residents are about to move in.


All five of them.


The tower, known as Antilia, is the new home of India's richest person, Mukesh Ambani, whose $27 billion fortune also ranks him among the richest people in the world. And even here in the country's financial capital, where residents bear daily witness to the stark extremes of Indian wealth and poverty, Mr. Ambani's building is so spectacularly over the top that the city's already elastic boundaries of excess and disparity are being stretched to new dimensions.


“One family is going to live in that?” said Prahlad Kakkar, an advertising filmmaker and prominent city resident. “Either it is a landmark, or a symbol, or it is Mammon.” He added: “There is shock and awe — both at the same time.”


Mr. Ambani, his wife, Nita, and their three children are expected to move into the building after a housewarming party with 200 guests scheduled for Nov. 28. For his part, Mr. Ambani has refused to comment about the project and required his designers, decorators and other contractors to sign confidentiality agreements, as if a cone of silence could be erected around a skyscraper rising near the edge of the Arabian Sea.


Predictably, and perhaps by design, the opposite has happened. Details have spilled out — many of them confirmed or disputed anonymously. Some reports have estimated the total residential space at 400,000 square feet, though people close to the project say the real number is a humbler 60,000 square feet. Press accounts also have estimated the value of the building at $1 billion, a figure disputed by people familiar with the project.


Regardless, a gawking city has greeted the new tower with a mixture of moralizing and astonishment, envy and condemnation, all sprinkled with Freudian analysis of the most basic question: Why did he do it?


“We are all sort of perplexed,” said Alyque Padamsee, a long-time advertising executive and actor in the city. “I think people see it as a bit show-offy.”


A bit.


For decades, the Ambani family has been India's most famous corporate soap opera. The father, Dhirubhai Ambani, was a brazen, rags-to-riches tycoon who established Reliance Industries after rising out of the city's Dickensian tenements, known as chawls. Today, Reliance is the world's biggest producer of polyester fibers and yarns and accounts for almost 15 percent of India's exports, according to the company's annual report. The two sons, Mukesh and Anil, inherited and divided the empire and have spent years feuding, including a nasty recent fight over natural gas rights that brought a reprimand from the prime minister before India's Supreme Court settled the case in Mukesh's favor.


Of the two brothers, Anil is the more flamboyant and outgoing, while Mukesh is regarded as more staid — less likely, at least, to build at 27-story house for himself. The new tower is located on Altamount Road, the same leafy residential street in south Mumbai where the father bought his first home after moving the family out of the tenements. Later, he purchased a 14-story apartment building named Sea Wind, where both Mukesh and Anil have lived with their families on different floors, even during their feud. (Their mother refereed from her own residence in the building.)


Now Mukesh is moving into a tower that makes Sea Wind seem like a guest house.


“It's kind of returning with a vengeance to where they made it into the middle class and trumping everybody,” said Hamish McDonald, who chronicled the family's history in his new book, “Mahabharata in Polyester: The Making of the World's Richest Brothers and Their Feud.”


“He's sort of saying, ‘I'm rich and I don't care what you think,' ” Mr. McDonald said.


Mumbai, once known as Bombay, is India's most cosmopolitan city, with a metropolitan area of roughly 20 million people. Migrants have poured into the city during the past decade, drawn by Mumbai's reputation as India's “city of dreams,” where anyone can become rich. But it is also a city infamous for its poor: a recent study found that roughly 62 percent of the population lived in slums, including one of Asia's biggest, Dharavi, which houses more than one million people.


Real estate prices are among the highest in the world, pushing many working-class residents into slums, even as developers have brazenly cleared land for a new generation of high-rise apartment towers for the affluent. High-rises are considered necessary, given the city's limited land, yet the rising towers have further insulated the rich from the teeming metropolis below. With his helipads, which still await operating approval, Mr. Ambani could conceivably live in Mumbai without ever touching the ground.


“This is a gated community in the sky,” said Gyan Prakash, author of the new book “Mumbai Fables.” “It is in a way reflective of how the rich are turning their faces away from the city.”


Along Altamount Road, which is also home to other industrialists, the reaction to the new neighbor is mixed. Some senior citizens along the street worry about the noise from the comings and goings of helicopters. But Utsav Unadkat and Harsh Daga, college students who grew up in the neighborhood, stared up at the tower on a recent afternoon as if it were a dream realized.


“I heard he has a BMW service station inside,” said Mr. Unadkat, dragging on a cigarette (unconfirmed). “There's also a room where you can create artificial weather,” Mr. Daga added (apparently true).


Standing nearby, Laxmi Kant Pujari, 26, a decorator's assistant, waited to carry glass samples into the building. If his samples are selected, Mr. Pujari, a migrant, would handle the installation — a task he considered an honor. “Whether it is a beggar or an Ambani, the desire to be rich is in everyone's heart,” he said.


Farther down the street, Sushala Pawar admitted struggling to comprehend the difference in Mr. Ambani's life and her own. She cooks for a family in a nearby apartment, earning 4,000 rupees a month, or about $90. She sleeps on the floor of the hallway after the family has gone to bed.


“I'm a human being,” she said. “And Mukesh Ambani is a human being. Sometimes I feel bad that I live on 4,000 rupees and Mukesh Ambani lives there.”


But then, nodding toward the building, she perked up.


“Maybe,” she said, “I could get a job there.”


.


*This article can be seen in its originally published form by clicking [here](http://www.nytimes.com/2010/10/29/world/asia/29mumbai.html?pagewanted=1&hp).*




 ## Comments 
1. [Tweets that mention TOWER OF POWER « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/10/29/tower-of-power/?utm_source=pingback&utm_campaign=L2)
10.30.10 / 12am


[…] This post was mentioned on Twitter by Ankit Surti, KpaxsArchi. KpaxsArchi said: TOWER OF POWER: . October 28, 2010 . Soaring Above India's Poverty, a 27-Story Home By JIM YARDLEY MUMBAI — The … <http://bit.ly/drHtw0> […]
3. Naeem Mohaiemen
11.1.10 / 7pm


Time for Mike Davis to add a new chapter to “Evil Paradises: dreamworlds of neoliberalism”.
5. [deepa](http://clinical-observation.blogspot.com/)
11.4.10 / 2am


When I read about the Antilla towers built by Mukesh Ambani, I somehow feel that this kind of wealth inequality is not new for Mumbai. I should know, Mumbai is my home-city. Its only the scale of this project which has finally gotten people from around the world to notice, and people in India to comment. But I also have to say, that within the Mumbai psyche, is also a deeply ingrained neoliberalistic tendency, a belief in capitalism, that somehow pardons and justifies such a blatant show of wealth, that somehow wants to point out that Mukesh Ambani's army of 600 ‘staff' generates jobs..





	1. [Francisco Vasconcelos](http://csxlab.org)
	11.15.10 / 6pm
	
	
	600 jobs. What kind of jobs? career opportunity jobs? or 600 maids and waiters to full-fill this guys needs?
	
	
	This is the what we encounter, for the corporate economy based capitalism, only maters the jobs and material living.  
	
	Somehow I can easy see the lack of quality living, quality jobs and quality growth in therms of social equivalence and personal fulfillment.
7. tom
11.8.10 / 5pm


From the picture, it seems like a joke; it looks like a vertical stack of slum shanties. The endless shanty?
