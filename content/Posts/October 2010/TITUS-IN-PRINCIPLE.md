
---
title: TITUS IN PRINCIPLE
date: 2010-10-23 00:00:00 
tags: 
    - Anthony_Titus
    - architecture
    - contemporary_art
---

# TITUS IN PRINCIPLE


Personal vision, a sense of the poetic and the expressive skills of particular, unique individuals are essential to the creation of architecture. This distinguishes it from what we might call pure engineering and construction, which are the result of purely technical considerations. Works of architecture will never follow the lead of purely technical considerations and may indeed compromise them in order to achieve a different order of meaning and purpose, depending on the architect's personal vision. In architecture of a higher order, the technical is situated in a larger conception about the world and our place within it.


The recent works of Anthony Titus—a series of wall constructions that look at first glace more like paintings than anything to do with architecture—actually inform architecture as much as the contemporary world of art. The distinction is important because these works appear in a gallery—an art-world context—that makes it easy for them to be passed over by architects without much thought.


The constructions offer us a world-view within their carefully chosen, if modest, material means. The modesty is itself a part of Titus' world view—use only what is necessary for your purposes. And choose what is necessary according to its richness of potential to express concepts and feelings. Canvas, wood, paint. Brick, concrete, metal and glass. The latter belong to the gallery where the constructions are placed on the walls, but which are no less carefully selected by him, or so we can assume from the subtle modulations of color and texture of the total assembly—the constructions and their site. The canvas is used raw and painted, stretched tightly and in varying degrees of looseness and hanging. The wood is exposed, or hidden, or an implied presence used to weigh-down a piece of canvas. The paint is employed in the full range of its fluid possibilities, from simple, unmodulated flatness to richly varied surfaces; from stark black and white to nuanced color; from opacity to transparency. Titus has explored the expressive possibilities of the few materials methodically and with a restraint that reflects his humility before their very existence—and ours—yielding an unforced, finally lyrical complexity. They emphasize an order of inter-relationships rather than expressiveness for its own sake. Ethics before aesthetics—or, perhaps it is more precise to say, aesthetics that emerge from a personal creative ethos. I can't think of anything more sorely needed than this in architecture today.


LW


[![](media/titus-comp-12.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/titus-comp-12.jpg)


(above) Constructions are entitled—from top to bottom—*Dream Weaver, Lazy, Eye, Get Lifted, Drop Low, Weak Faith, Hazed High, Hard Slumber.* They are shown in the Kidd Yellin Gallery, Brooklyn, New York.


***ANTHONY TITUS** teaches architectural design at Rensselaer Polytechnic Institute, Pratt Institute, and The Cooper Union.*


#Anthony_Titus #architecture #contemporary_art
 ## Comments 
1. [A Synthetic Architecture » Blog Archive » Comments on TITUS IN PRINCIPLE « LEBBEUS WOODS](http://amcgoey.net/commentary/comments-on-titus-in-principle-lebbeus-woods)
10.24.10 / 5pm


[…] TITUS IN PRINCIPLE « LEBBEUS WOODS – In his discussion of Anthony Titus's recent work, Lebbeus Woods argues that an architecture of expression stemming from a “personal creative ethos” is what is needed in contemporary architecture. It certainly isn't a surprise that Wood's values such an enterprise as his own career can be seen as a manifesto for exactly that theory of practice. […]
3. Honus Wagner
10.28.10 / 7pm


The hanging canvas looks like a kangaroo pouch. 


Yawn….another two minute idea executed in a gallery. In this case, it looks like Titus was too lazy to figure out how to frame a canvas and gesso it, and just said screw it….this will be the work. An incomplete canvas frame!…Genius!!! 


This installation makes Bob Ross look like Da VInci.





	1. WA Sulaiman
	10.31.10 / 7am
	
	
	Dear Honus Wagner,  
	
	You have been posing pretty good comments in this blog. I believe that you might have better things than what in here that you would like to share with the reader. I would really like to see your work, or your thoughts, perhaps your interest on the subject.
5. tom
11.7.10 / 9pm


Using painting to illustrate architectural ideas results in just that; illustrations.  

As two dimensional objects, they are very conservative.


I think they would have served themselves better in three dimmensions  

…maybe like huts? or screens?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	11.8.10 / 2am
	
	
	tom: Don't you see Titus' work as 3D? I do.
	3. Pedro Esteban
	10.28.11 / 7pm
	
	
	The fun part is I can see universes… 
	
	
	most of the comments here are really fun! jajaja
7. tom
11.8.10 / 4am


Its very similar to myriad examples of post minimal painting that uses elements of 3D, but essentially to just refer to the 2d, as a deconstruction of “painting”. In those terms,the work is very academic Greenberg- ism that uses elements of Greenberg's heroes, Morris Louis for one. Its basically a pastiche.  

So I guess I was suggesting that it move off of the wall to lessen the conceptual and historical connections to painting.  

If the work is about poetic combinations of materials, why employ a frame?  

I agree that a personal ethos is needed in architecture today, as well as in art.





	1. Pedro Esteban
	10.28.11 / 7pm
	
	
	I am trying to understand your idea, but I think you are completely out of focus. Could you explain the relations you can see in this paintings?
9. der flaneur
8.24.11 / 11pm


I would say the work is 2-1/2d
