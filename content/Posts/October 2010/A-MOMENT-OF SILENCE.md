
---
title: A MOMENT OF SILENCE
date: 2010-10-22 00:00:00
---

# A MOMENT OF SILENCE


[![](media/iraq-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/iraq-1.jpg)


How can we not, each of us, take a moment of silence in memory of the tens of thousands of Iraqi civilians—men, women, and children, each with the unique stamp of their humanity—killed and still being [killed in the violence](http://www.nytimes.com/2010/10/23/world/middleeast/23casualties.html) unleashed by the United States invasion in 2003. A moment of silence.


LW



 ## Comments 
1. [Tweets that mention A MOMENT OF SILENCE « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/10/22/a-moment-of-silence/?utm_source=pingback&utm_campaign=L2)
10.22.10 / 11pm


[…] This post was mentioned on Twitter by Pablo Lara H, KpaxsArchi. KpaxsArchi said: A MOMENT OF SILENCE: How can we not, each of us, take a moment of silence in memory of the tens of thousands of I… <http://bit.ly/bq18LV> […]
3. [Francisco Vasconcelos](http://csxlab.org)
10.23.10 / 1am


[ ]
5. Curtis
10.28.10 / 10am


Yes moment(s) of silence are warranted, but did the writer also observe the same when Saddam and his sons were executing thousands?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.29.10 / 12am
	
	
	Curtis: No, I'm ashamed to say. Does that disqualify me from doing so, after the country I live in opened Pandora's dreadful box?
