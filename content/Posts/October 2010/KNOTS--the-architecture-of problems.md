
---
title: KNOTS  the architecture of problems
date: 2010-10-12 00:00:00 
tags: 
    - philosophy
    - problem_stating
    - slums
    - society
---

# KNOTS: the architecture of problems


*The following is a summary of a lecture given by LW in the **KNOTS Advanced Concepts seninar** at The Cooper Union School of Architecture; Anthony Vidler, Dean; LW, seminar leader; and students: **ARCH 205** (undergraduate): Pamela Cabrera, Ezegbebe Eribo, Andres Larrauri, Standish Lee, Louis Lipson, Emily Martinez, Harry Marzyn, Natalie Savva, David Varon, Galen Wolfe-Pauly; **ARCH 485** (graduate): Melanie Fessel, Jonathan Heckert, Aikaterini Kefalogianni, Katerina Kourkoula, David Ross.*


We should always keep in mind that the goal of our work in this seminar is ***stating problems clearly***, not solving them.


Most problems—especially the very difficult and ‘knotty' ones—are never formulated, verbally or visually, clearly enough to understand exactly what needs to be solved, so we tend to throw up our hands in frustration and avoid them. Also a solution is often contained in a well-articulated problem. In any event, we should not let the lack of a ready answer be a reason to avoid asking a question. Indeed, the only questions worth asking are those for which we do not already have an answer. In this seminar we will not shy away from looking at the most daunting problems.


The approach we will take is based on a way of breaking down—analyzing—problems in terms of three components of every problem we as architects confront: the spatial, the social, and the philosophical. Certainly there are other possible categories we could employ, but I have chosen these based on my experiences and also to work well within the structure of our seminar and its time-frame. The following presentation is an example of how the three chosen categories work in attempting to formulate a particularly intractable ‘knot' confronting us today: [the problem of slums](https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/):


.


[![](media/slide110.jpg)](http://arch205485.files.wordpress.com/2010/10/slide110.jpg)


[![](media/slide24.jpg)](http://arch205485.files.wordpress.com/2010/10/slide24.jpg)


[![](media/slide31.jpg)](http://arch205485.files.wordpress.com/2010/10/slide31.jpg)


[![](media/slide44.jpg)](http://arch205485.files.wordpress.com/2010/10/slide44.jpg)


.


(below) A knot without apparent ends that therefore cannot be united. Such a knot was presented (so the legend goes) to Alexander of Macedonia as he led his army on a mission of conquest into Persia; he was told that the man who could untie it was destined to become Master of all Asia. Studying it briefly and grasping that it could not be untied, he drew his sword and in a single stroke cut the Gordian Knot in half. He had untied the knot by means that have set a violent course for solving problems that continues up to the present day.


[![](media/slide5.jpg)](http://arch205485.files.wordpress.com/2010/10/slide5.jpg)


.


Knots without ends by M.C. Escher, a master of spatial paradox. The paradox here has nothing to do with untying the knots, but with how they are tied at all. Without a point of origin, how can they exist?


[![](media/slide6.jpg)](http://arch205485.files.wordpress.com/2010/10/slide6.jpg)


.


Knot and cube by LW. The knot engages the cube, so it is difficult to know where there are ends to the knot. The drawing also addresses ‘measurability' and ‘immeasurability', the interaction and conflict between the organic and the artificial, the unpredictable and the predictable.


[![](media/slide71.jpg)](http://arch205485.files.wordpress.com/2010/10/slide71.jpg)


.


As the knot morphs into a more unified (measurable) figure, the drawing's narrative becomes one of interaction and transformation.


[![](media/slide81.jpg)](http://arch205485.files.wordpress.com/2010/10/slide81.jpg)


.


When the interior of the cube is seen, the organic is revealed as originating in the artificial and vice versa—a paradox of the human and the natural.


[![](media/slide9.jpg)](http://arch205485.files.wordpress.com/2010/10/slide9.jpg)


.


[![](media/slide101.jpg)](http://arch205485.files.wordpress.com/2010/10/slide101.jpg)


.


The most obvious—and historically significant—type of spatial knot is the *labyrinth.* This one is modeled after the Labyrinth of the Palace at Knossus, built (according to legend) by Minos, a Cretan king, to hide the indiscretion of his wife, whose union with a sacred bull resulted in their offspring, the Minotaur—a very angry monster who was half man and half bull. The future king of Athens, Theseus, was led into the Labyrinth for his own indiscretion with the king's daughter, Ariadne, with the expecttaion that he—unable to find his way out— would be killed by the Minotaur. However, Minos underestimated Theseus and Ariadne, who both lived to tell the tale, which abounds in paradoxes about the human and the natural.


[![](media/slide11.jpg)](http://arch205485.files.wordpress.com/2010/10/slide11.jpg)


.


“Heeeeere's Johnny!” The line is spoken by Jack Nicholson in Stanley Kubrick's “The Shining.” With an axe, he has just broken down the door of a bathroom in a spooky old hotel, intending to kill his wife and son hiding there. He has, shall we say, totally lost him mind.


[![](media/slide12.jpg)](http://arch205485.files.wordpress.com/2010/10/slide12.jpg)


.


Or has he? Maybe it is more accurate to say that he is lost IN his mind, which is a labyrinth, much like the one next to the hotel where he later pursues, axe in hand, his young son:


[![](media/slide13.jpg)](http://arch205485.files.wordpress.com/2010/10/slide13.jpg)


.


Labyrinths are not descriptions of diabolical traps alone, but also of architetcural constructions, often in unintended ways. The Palace of Diocletion, built in Split, Croatia around 300CE, was modeled on typical Roman military camps designed for maximum security—a perimeter wall divided into four quadrants by two ‘streets' forming a cross for quick and easy access to each quadrant and to gates in the perimeter defensive walls. Maximum security also meant maximum clarity:


[![](media/slide14.jpg)](http://arch205485.files.wordpress.com/2010/10/slide14.jpg)


.


Over the centuries following the collapse and dissolution of Rome's empire, Diocletion's palace was filled in with new buildings, following no overall plan, other than that of the original Palace, parts of which fell into ruin or were otherwise demolished:


[![](media/slide15.jpg)](http://arch205485.files.wordpress.com/2010/10/slide15.jpg)


[![](media/slide16.jpg)](http://arch205485.files.wordpress.com/2010/10/slide16.jpg)


.


Today, this urban construct within the port city of Split, serves as a model of what we might call an ‘organic' or 'emergent' urbanism with an intricate spatial order that is saved from chaos only by the slow pace of its growth, which has allowed for deliberate, if unplanned, continuity of construction:


[![](media/slide17.jpg)](http://arch205485.files.wordpress.com/2010/10/slide17.jpg)


.


In many ways these slums in Rio de Janeiro and Sao Paulo, Brazil, share features of the evolved Diocletian Palace in Split: density and intense spatial complexity resulting from small-scale building without an overall plan over a period of time, responding only to the existing features of the landscape. The major difference is the radically compressed time-frame of the slum and the less deliberate, more chaotic relationships of the buildings to each other:


[![](media/slide18.jpg)](http://arch205485.files.wordpress.com/2010/10/slide18.jpg)


[![](media/slide191.jpg)](http://arch205485.files.wordpress.com/2010/10/slide191.jpg)


.


[![](media/slide20.jpg)](http://arch205485.files.wordpress.com/2010/10/slide20.jpg)


[![](media/slide21.jpg)](http://arch205485.files.wordpress.com/2010/10/slide21.jpg)


.


The less deliberate, hastily built character of the slum buildings is reflected in their physical construction and careless maintenance, resulting in a polluted, unsanitary environment.


[![](media/slide22.jpg)](http://arch205485.files.wordpress.com/2010/10/slide22.jpg)


.


**Underlying the physical qualities of slums are particular social conditions.** In terms of our discourse we can define these as a social knot within a spatial knot, contributing to a larger knot (problem) in which the social and spatial are so folded into one another that it is difficult—but not impossible—to separate them for analysis. Taking on whatever risks the analytical separation entails, it is our task to do so, with the ultimate goal (not to be accomplished in this seminar) of understanding and indeed untying the larger, whole knot, creating a solution for the problem of slums.


[![](media/slide231.jpg)](http://arch205485.files.wordpress.com/2010/10/slide231.jpg)


.


The social knot of slums is tied with the extremes of poverty and wealth in contemporary societies around the world. These extremes are the result of an unequal control of and benefits from the natural and human resources within any society. Societies with the more unequal division of this wealth—the larger gap between rich and poor—have more slums than those where ‘income disparity' is less. In the places where economic disparity is greatest are to found the worst slums. It is a painfully simple equation.


[![](media/slide1-2.jpg)](http://arch205485.files.wordpress.com/2010/10/slide1-2.jpg)


In Rio de Janeiro, the rich can stand on the pooled terraces of their condominiums and look down on the poor neighborhoods literally next door, proving that the continued existence of poverty is not a matter of ignorance on either side of the social divide.


[![](media/slide25.jpg)](http://arch205485.files.wordpress.com/2010/10/slide25.jpg)


.


A shopping street in a Mumbai slum—fairly upscale as slums go—that serves the working poor. It is surprising to some that slums around the world are largely inhabited by people who work, often very hard, but for wages too low to escape from poverty. Most slum dwellers come to large cities from impoverished rural lives, in search of work in factories, sweatshops, selling cast-off goods (many slums are located next to city trash dumps) and working as day-laborers:


[![](media/slide26.jpg)](http://arch205485.files.wordpress.com/2010/10/slide26.jpg)


.


A shopping street in midtown Manhattan—not for the super-rich but the rich-enough. Manhattan is a magnet for upwardly-mobile people working through the management level of globally corporate businesses. Despite appearances, their future economic well-being is not absolutely assured. An economic downturn or corporate ‘restructuring' could throw them out of work, without credit cards, foreclosed, even homeless. They hope that even in the worst circumstances their experience and education will inevitably assure them of economic security:


[![](media/slide27.jpg)](http://arch205485.files.wordpress.com/2010/10/slide27.jpg)


.


Slums are attached to cities and live from their industries and their waste, but not their services such as potable water, electricity, gas, telephone, solid waste disposal and trash collection. Sometimes slum dwellers can illegally tap into power and water lines; otherwise they must get along with deprived and primitive conditions. City governments are reluctant to ‘legitimize' slums, for several reasons, by making city services available to them. The poles and overhead wires seen below are almost certainly pirated:


[![](media/slide28.jpg)](http://arch205485.files.wordpress.com/2010/10/slide28.jpg)


.


The most typical shopping street in Singapore, Manila, Rio or wherever—malls are all alike, anywhere in the world, with the same franchised shops and goods, which are also manufactured anywhere, many in sweatshops and factories employing the working poor. A keystone of globalization:


[![](media/slide29.jpg)](http://arch205485.files.wordpress.com/2010/10/slide29.jpg)


.


While shopping malls are designed by architects, who often work directly for the real estate developers who build them, the buildings in slums have no professional architects, engineers, or licensed builders. Instead, they are built by self-taught people inhabiting slums, often working together. The results may be primitive by professional standards, and structurally unsound when confronted with hurricanes and earthquakes, but they nevertheless reflect considerable inventiveness and ingenuity in the face of daunting economic and social conditions. Their very existence is a a manifestation of human spirit and community.


[![](media/slide30.jpg)](http://arch205485.files.wordpress.com/2010/10/slide30.jpg)


.


In contrast, the guarded, gated ‘communities' that exist in cities everywhere today are highly commercialized and standardized in design, and security-obsessed in planning and construction. The rich-enough are rarely concerned with working together, because they are more competitive than cooperative:


[![](media/slide311.jpg)](http://arch205485.files.wordpress.com/2010/10/slide311.jpg)


.


Within the enclaves secured by fences and walls, security cameras and guard posts, the houses are often considered by their owners not as homes, but rather as financial investments for the upwardly-mobile hoping always to move on to more and better. The houses' standardization and conformity—with just enough ‘standard deviation' to make the illusion of difference—makes them more easily re-salable, protecting their owners' investments:


[![](media/slide32.jpg)](http://arch205485.files.wordpress.com/2010/10/slide32.jpg)


.


The ‘terrible beauty' of a slum street facade is found, for more sophisticated (or difficult) sensibilities, in the uniqueness, variation, and unpredictability resulting not from deliberate design, but only from the accidents of decay.


[![](media/slide33.jpg)](http://arch205485.files.wordpress.com/2010/10/slide33.jpg)


.


It is fair to say that architects most commonly work for the rich-enough and, when they can, for the really-rich, whose sensibilities, at their best, crave directness and simplicity. Expensive to construct and maintain, “Less is more” is a luxury.


[![](media/slide34.jpg)](http://arch205485.files.wordpress.com/2010/10/slide34.jpg)


.


**Underlying every social condition are philosophical ideas about what it means to be human.** These are presumably universal ideas considered to be applicable to that social condition anywhere, in any form, and at any time it is encountered. In terms of our discourse, different and often contending philosophical ideas form a knot complexly intertwined with the social and the spatial knots to create the larger problem:


[![](media/slide35.jpg)](http://arch205485.files.wordpress.com/2010/10/slide35.jpg)


.


At the root of all ideas about the nature of the human are assumptions about whether any person is first and foremost an individual or a member of a  group. Is a person's first responsibility to himself or herself, or to the group of people on which, in many ways, he or she depends. This is similar to “the chicken or the egg” question and the intelligent answer is that any person is responsible to both. However, establishing the priority is important because there are many times when the interests of the individual and the individual's community are in conflict and a choice of which is to be served first is necessary.


[![](media/slide36.jpg)](http://arch205485.files.wordpress.com/2010/10/slide36.jpg)


.


The 17th century philosopher Thomas Hobbes was an influential voice in his time arguing for the priority of the individual over any form of organized society:


[![](media/slide38.jpg)](http://arch205485.files.wordpress.com/2010/10/slide38.jpg)


In his writings, Hobbes “was a champion of absolutism for the sovereign but he also developed some of the fundamentals of European liberal thought: the right of the individual; the natural equality of all men; the artificial character of the political order (which led to the later distinction between civil society and the state); the view that all legitimate political power must be “representative” and based on the consent of the people; and a liberal interpretation of law which leaves people free to do whatever the law does not explicitly forbid.” [[Wikipedia](http://en.wikipedia.org/wiki/Thomas_Hobbes)]. The inevitable conflict of interest between individuals and their society produced a continual state of tension between them, and even various types of war:


[![](media/slide39.jpg)](http://arch205485.files.wordpress.com/2010/10/slide39.jpg)


.


17th century philosopher John Locke followed the basic tenets of Hobbes' thought, especially the ‘natural' rights of the individual living in the artifice of society, but was much more optimistic about their prospects, believing that conflicts could be avoided by the use of reason:


[![](media/slide401.jpg)](http://arch205485.files.wordpress.com/2010/10/slide401.jpg)


.


“Locke's theory of mind is often cited as the origin of modern conceptions of identity and the self, figuring prominently in the work of later philosophers such as Hume, Rousseau and Kant. Locke was the first to define the self through a continuity of *consciousness*. He postulated that the mind was a blank slate or *tabula rasa*. Contrary to pre-existing Cartesian philosophy, he maintained that we are born without innate ideas, and that knowledge is instead determined only by [e](http://en.wikipedia.org/wiki/Experience "Experience")xperience derived from sense perception.” [[Wikipedia](http://en.wikipedia.org/wiki/John_Locke)].


[![](media/slide411.jpg)](http://arch205485.files.wordpress.com/2010/10/slide411.jpg)


.


The earliest known ‘social contract' between individuals and the state—personified in its sovereign—is the 13th century Magna Carta, striking a balance of power, and responsibility, between land-owning ‘nobles' and their king. While it was hardly a democratic document, as it concerned only the wealthy elite, it nevertheless introduced the principle of legal parity in social relationships:


[![](media/slide421.jpg)](http://arch205485.files.wordpress.com/2010/10/slide421.jpg)


.


The American Declaration of Independence was greatly influenced by the philosophy of Locke, and led to what might be fairly called a Hobbesian ‘war of independence' settling the conflicts of interest between the English sovereign and the land-owning American aristocrats who wrote the Declaration as the founding document of not only a new state, but a new type of state:


[![](media/slide431.jpg)](http://arch205485.files.wordpress.com/2010/10/slide431.jpg)


.


Land-owning, shopping malls, corporate commerce, factories, and indeed the very concepts of wealth and poverty that drive much of social dynamics are all underlaid with theories of economics that establish the relative value of products and labor, of things and of people. The broad principles addressing individuality and community are, in effect, made tangible by these theories. In modern times, two rival economic theories have come to dominate the discourse about what is the best way for people to live. It is important to realize that these theories do simply simply explain *post facto* why things work as they do but actually inspire and instruct people about what to do in the first place:


[![](media/slide441.jpg)](http://arch205485.files.wordpress.com/2010/10/slide441.jpg)


.


Capitalism, in theory, is certainly the way of the free and independent individual, primarily pursuing his or her economic interests, often in the context of a hostile society and state, and always in conflict with other competing individuals. The 18th century theorist Adam Smith established, at the beginning of the Industrial Revolution, capitalism's basic tenets:


[![](media/slide45.jpg)](http://arch205485.files.wordpress.com/2010/10/slide45.jpg)


.


In Adam's view, an ‘invisible hand', one that is not controlled by laws but which moves on a deep psychological level, guides the economic exchanges between people in a society in such a way as to avoid destructive conflicts (such as war) between competing individuals and states, and leads to a peaceful secure, stable, and prosperous society. Any interference with this guiding hand, say, by government regulations, will upset and even destroy the natural, if fragile, dynamics of an economy. This is the single most influential principle of the theory of capitalism ever formulated. Its power to shape politics and ethics as well as economics is seen in the passionate intensity of public debates up to the present day.


[![](media/slide46.jpg)](http://arch205485.files.wordpress.com/2010/10/slide46.jpg)


.


If Adam's theory is correct, then what is good for the upwardly-mobile and rich-enough on Fifth Avenue (first photograph below) will be good, eventually, for the workers protesting poor working conditions and wages in Bangladesh and Greece (second and third photos below). Some economists call this “the trickle down effect”—the wealth accumulating at the top, or at least part of it, will eventually get down to the bottom. After all, it's in the best interests of owners and managers to keep their workers reasonably contented and productive, isn't it?:


[![](media/slide271.jpg)](http://arch205485.files.wordpress.com/2010/10/slide271.jpg)


[![](media/workers-21.jpg)](http://arch205485.files.wordpress.com/2010/10/workers-21.jpg)


[![](media/workers-1.jpg)](http://arch205485.files.wordpress.com/2010/10/workers-1.jpg)


.


By the middle of the 19th century, with the Industrial Revolution fully underway, it became obvious to many that factory and mine owners, many of whom were becoming very wealthy, were treating workers unfairly, paying subsistence wages or below, creating unsafe working conditions, spawning filthy and environmentally destructive ‘company towns,' that drove workers and their families deeply into debt, without considering in the least their health, education or other essential needs. It was obvious, too, that governments in the industrialized cities and nations were indifferent to the workers' situation. There were no labor unions to engage owners and management in collective bargaining on the workers' behalf, yet they could not strike legally or quit because of their debts. In response to this desperate situation, philosopher Karl Marx and sociologist Friedrich Engels published books and pamphlets that showed a way out, the most influential one being:


[![](media/slide49.jpg)](http://arch205485.files.wordpress.com/2010/10/slide49.jpg)


.


The Communist Manifesto was a call to revolution, violent if necessary, in which the workers would overthrow the owners and managers—the capitalists—assume control of the ‘means of production', and institute a new form of government—called ‘communist'—that would oversee and control the equitable distribution of the wealth generated by industrialization of a society's human and natural resources. The first and last lines of the Manifesto were these:


[![](media/slide50.jpg)](http://arch205485.files.wordpress.com/2010/10/slide50.jpg)


.


The Manifesto was based on Marx's theory of history, politics, and economics published as Capital, and in numerous essays following the Manifesto. As a central principle, Marx created a new maxim from an old New Testament saying:


[![](media/slide51.jpg)](http://arch205485.files.wordpress.com/2010/10/slide51.jpg)


.


Communism was a more radical and militant version of socialism, which was a philosophy and a practice in various forms that originated centuries earlier, but which had never had broad social impact. In the later 19th century and throughout the 20th century, Communism had a profound impact on a global scale. It inspired revolutions around the world and, at the height of its influence, the formation of formidable governments in Russia and China that rivaled and competed with traditional capitalist powers in Europe and the Americas. Capitalism was seriously threatened and countered ferociously in actions and in theory. Ayn Rand, a novelist and would-be philosopher, recast Adam Smith's credo in extreme moral terms in books that were widely read and followed during the most intense and dangerous decades of the “cold war.”


[![](media/slide52.jpg)](http://arch205485.files.wordpress.com/2010/10/slide52.jpg)


.


In Rand's novel, Atlas Shrugged, its hero leads a revolt of owners, managers and technocrats against the masses, who have gullibly and ungratefully fallen under the influence of socialism. At the end of the novel he declares to the world “mission accomplished,” then traces with his finger the sign of the dollar against the glowing evening sky.


[![](media/slide53.jpg)](http://arch205485.files.wordpress.com/2010/10/slide53.jpg)


.


For the present, it seems that capitalism has indeed defeated socialism in any form. Even formerly communist governments have adopted hybrid forms of capitalism, exploiting its supposedly free-markets while maintaining autocratic state control of their societies. So, while it lasts….


[![](media/slide54.jpg)](http://arch205485.files.wordpress.com/2010/10/slide54.jpg)


.


**Will triumphant capitalism find the solution for the problem of slums?** How will it be able to untie the Gordian Knot of their existence without two ends to grasp? Let's hope that it won't be by Alexander's method.


[![](media/slide55.jpg)](http://arch205485.files.wordpress.com/2010/10/slide55.jpg)


(end)


#philosophy #problem_stating #slums #society
 ## Comments 
1. [Tweets that mention KNOTS: the architecture of problems « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2010/10/12/knots-the-architecture-of-problems/?utm_source=pingback&utm_campaign=L2)
10.12.10 / 6pm


[…] This post was mentioned on Twitter by BibliOdyssey and adalberto alves, KpaxsArchi. KpaxsArchi said: KNOTS: the architecture of problems: The following is a summary of a lecture given by LW in the KNOTS Advanced Con… <http://bit.ly/dxpTWF> […]
3. [SLUMS: The problem « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2008/01/18/slums-the-problem/)
10.13.10 / 12am


[…] <https://lebbeuswoods.wordpress.com/2010/10/12/knots-the-architecture-of-problems/> […]
5. [KNOTS: the architecture of problems « LEBBEUS WOODS | Splitting Skulls](http://splittingskulls.com/chrismoffett/knots-the-architecture-of-problems-%c2%ab-lebbeus-woods)
10.13.10 / 8pm


[…] via KNOTS: the architecture of problems « LEBBEUS WOODS. […]
7. [Alexander Strugach (SPb, Russia)](http://www.strugach.ru/)
10.14.10 / 2pm


Interesting lecture, Mr. Lebbeus. And nice graphics.  

What do you think about (so-called) Classicism —- (so-called) Modernism knot? Is it real? Or just another myth of the XX century?
9. [Alexander Strugach (SPb, Russia)](http://www.strugach.ru/)
10.14.10 / 2pm


Reading about knots I also remembered my 2008 drawing:
11. [Alexander Strugach (SPb, Russia)](http://www.strugach.ru/)
10.14.10 / 2pm


(the sketch for unralised desing for Mus(ic) Mus(eum): <http://www.flickr.com/photos/alexander_str/5080950218/>
13. [dave](http://organicmobb.wordpress.com)
10.15.10 / 5am


I see the knot as humans trying to figure this world out. The slums are raw, they are humble and unashamed; they show the knot.
15. [dave](http://organicmobb.wordpress.com)
10.15.10 / 5am


Interesting could be thinking about the material of the knot. What is a knot made of finely made nylon rope v. a knot made of pasta or stone. Though the ideas about the knot obviously are not tangible, I find it interesting, the parallels you can extract if you give it a material.
17. Gio
10.15.10 / 7pm


Very Interesting and informative.  

Yes, capitalism seems to be more free depending on the social and economic status that we live in (as you presented it) but I do tend to like more in priciple the socialist movement, the idea of equality and share of resources sounds always wonderful. Like all knots (no end nor begining), Alexanders'solution seem to be soon approaching. There is so much humans can take, everywhere people are starting to speak, to voice out loud the oppression: France, India, Tibet, Brazil, Argentina, Lebanon and so on.
19. [chris teeter](http://www.metamechanics.com)
10.15.10 / 11pm


Pretty concise history of the western world and very informative, good work.


Did anyone review or consider what Pablo Escobar did for ther Columbian slums? He built soccer fields, a mircocosm of competition and conflict as islands of hope and retreat for the poor of the slums. 


Sports are the very embodiement of this invisible hand I think that Smith writes about. 


I guess you don't kill the knot, you fight it. I would argue communism was the sword to the knot, which was much needed though at the time, but now I think we can go with the invisible hand.


Conflict is not bad and refuse to allow my 3 year old daughter to be brainwashed by overly positive lacking real value cartoons on nick jr, like Dora the Explorer. A good dose of Tom and Jerry or Scooby Doo or Top Cat to keep her grounded is needed. 


Tom and jerry just try to kill each other all the time, playfully of course.


So here ya go, thanks for the brain wrinkl, Huizingas play guided by the invisible hand and human nature for conflict?


But what are architects to do, the inventors of systems?
21. [lebbeus wood's knots « ARCHIVAL CIRCUS](http://concretecircus.wordpress.com/2010/10/16/lebbeus-woods-knots/)
10.16.10 / 9am


[…] here a summary of a lecture given by LW in the KNOTS Advanced Concepts seninar at The Cooper Union […]
23. [knots: the architecture of problems « runteddyrun](http://runteddyrun.com/?p=3355)
10.22.10 / 9pm


[…] below from the lecture.  Here's a link to LW's site. […]
25. [smallestforest](http://www.smallestforest.net)
11.26.10 / 10am


has it occurred to anyone that “socialism vs. capitalism” is an oversimplified dichotomy, and that we may very well end up with something completely, utterly different than either of these economic models?  

Also, I question the assumption that Capitalism has been ‘triumphant'. If “the proof is in the pudding” then there are some huge, miserable, impossible-to-ignore failures wrought by capitalist economies.  

The US, with its enormous middle class and homogenized incomes, is perhaps the best example of communism that the world has managed to produce. “You pretend to work, we pretend to pay you”, isn't that the old joke commies made about their own economies?
27. Pradheepa
3.9.11 / 8pm


Do you have any references as to where you obtained these pictures? I'm particularly interested in the Rio de Janeiro shot (rich with pooled condos look out at the slums). Please let me know when you get the chance. Thanks!
29. [SLUMS: a too-long story « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2011/04/29/slums-a-too-long-story/)
4.29.11 / 2pm


[…] Note: For a more detailed discussion of the ideas presented here, together with numerous illustrative images, follow this link. […]
