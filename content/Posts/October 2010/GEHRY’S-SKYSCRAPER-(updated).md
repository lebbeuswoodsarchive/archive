
---
title: GEHRY’S SKYSCRAPER (updated)
date: 2010-10-11 00:00:00 
tags: 
    - Frank_Gehry
    - Manhattan
    - skyscrapers
    - style
---

# GEHRY'S SKYSCRAPER (updated)


[![](media/gehrytower-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehrytower-1.jpg)


(above) Beekman Place apartment tower, September 2010, designed by Frank Gehry—construction nearing completion.


Frank Gehry certainly knows how to breathe new life into an old idea. He also knows how to create dramatic forms that create an impression of being daringly innovative even while he meets the demands of clients for more or less conventional spaces serving entirely conventional purposes. He is the undisputed master in our time of architectural styling. His new seventy-six storey residential tower in Lower Manhattan is stunning confirmation.


The importance of styling should be neither over- nor under-estimated. Changing the appearance of a product without significantly altering the way its works or other aspects of its content is indeed superficial—literally—and yet, today, appearance is an important part of any product's content. This is increasingly so in a time characterized by continual change and that extensively uses mass-media to present products to potential consumers. A new style signals a new development in the ways people live, or that producers hope people will want to live. Also, a new style alters people's sensibilities about many important things that cannot be directly experienced or expressed; for example, their community's priorities and values.


The blocky, gridded facades of tall buildings of the 1950s testifies to the ideal of a rational and predictable social order based on conformity to presumably universal norms. Gehry's skyscraper, while conforming in its interiors (the living units) to the accepted rational norms, gives the appearance of being idiosyncratic, unique, and transformative rather than stable. Its facades appear to be bent, folded, or wrinkled as though by accident—even though these ‘accidents' are not accidental at all, but rather the result of careful design and highly controlled and sophisticated construction technology. In our present media-dominated, image-conscious society, it is not the causes that are most important, but the effects.


This is hardly a new phenomenon. Ever since, in the early 20th century, product advertising through all forms of communications media was designed to appeal to the emotions rather than the rational intellect, the image of a product has taken precedence in people's perceptions, and choices. Below, the crystal set radio—one of the first mass-media products—reflects the straight-forward, ‘functional' appeal of a new technology in the 1920s. By 1950, the same radio—techincally—is wrapped in a new cover reflecting the streamlined, speeded-up character of modern life and also its increasing concern with appearances and the symbolic impact of form. The crystal set is, in a sense, innocent, while its sleek, stylish descendent is unashamedly self-conscious.


.


(below, l. to r.) Crystal set radio from the 1920s, and 1950 version of the same product:


[![](media/radios-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/radios-2.jpg)


.


(below) Lower Manhattan today, an ensemble of skyscrapers and sensibilities spanning the last century. From [a recent article](http://www.nytimes.com/2010/10/10/realestate/10posting.html?ref=manhattan) in the New York Times: “What is important to him, Mr. Gehry said, isn't how the folds look but what they do to the interior, which unfurls in a riot of angled alcoves and bay windows.”


[![](media/gehry-tower-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-tower-7.jpg)


[![](media/gehry-tower-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-tower-6.jpg)


[![](media/gehry-tower-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-tower-5.jpg)


[![](media/gehry-tower-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-tower-9.jpg)


[![](media/gehry-21.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-21.jpg)


[![](media/gehry-tower-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/gehry-tower-8.jpg)


LW


#Frank_Gehry #Manhattan #skyscrapers #style
 ## Comments 
1. [dave](http://organicmobb.wordpress.com)
10.11.10 / 4pm


The radio is an excellent example. I enjoy this post immensely. I agree that the social-climate is reflected within architecture of a specific time. This brings up an excellent point, we are on the edge of something that will begin to come full circle in regards to design, especially when dealing with the internalization of technology. This begins to leave little evidence in the hardware of the exterior of the software functioning on the interior. 


With that said, I feel like architecture must go back to something much closer to the 1920's Crystal set radio. It is imperative!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.12.10 / 1am
	
	
	dave: sadly, we can never go back to that kind of innocence. All we could do is imitate it—not a good alternative. The honesty that innocence makes possible is, however, something to aspire to, even it takes the form of the difficult and complex. Just what I mean by that is something I've spent the better part of my life trying to work through and clarify. The only place where we're still innocent and not jaded is in the ideals each of us still harbors and somehow keeps alive.
3. slothglut
10.11.10 / 6pm


LW,


one question: how does this gehry skyscraper different from the kinds you attacked on your previous post, same old thing with a new skin?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.12.10 / 1am
	
	
	slothglut: it seems you've answered your own question!
5. kuniaki
10.12.10 / 1am


now that it's also ok for architects to be superficial stylists to conventional (not “rational”) content, dressers to fixed systems, it remains debatable whether the new appearance of the product is actually new/exciting/interesting, never mind relevant.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.12.10 / 2am
	
	
	kuniaki: ok according to whom?
	
	
	
	
	
		1. spencer
		10.12.10 / 4am
		
		
		historians.
		3. kuniaki
		10.12.10 / 12pm
		
		
		you've garnished this piece with lots of irony that will make gehry fans sullenly brood over. so, nice one. i question also the commonplace attitude among young architects and students that “anything goes”, if you survey the design porn sites around.
		5. [lebbeuswoods](http://www.lebbeuswoods.net)
		10.12.10 / 2pm
		
		
		kuniaki: thanks for the astute reading.
	3. Joe
	10.17.10 / 5am
	
	
	kuniaki,  
	
	how do you feel about Studio Gang's tower in Chicago?
	
	
	
	
	
		1. kuniaki
		10.19.10 / 6am
		
		
		its a conventional tower with central lift core and wavy facade that occasionally becomes token balconies to “relate” to the “outside” (very windy one would imagine). you experience the outside in also a conventional sense — going out to the balcony (found only at some instances). what is new about this? on the other hand, sanaa's novartis campus, although not exactly a skyscraper, instead, has a new thinness that allows you to experience a new feeling of outside everywhere inside.
7. spencer
10.12.10 / 4am


Personally, I agree with slothgut (and btw I don't think he answered his own question). We won't see a new type of architecture until we see a new mode of corporeal movement. Buildings radically changed with the invention of the elevator, the telephone and partially with the computer. I too, see Gehry's building as much of the same old thing especially if it conforms “to the accepted rational norms.” It is when we challenge and change these norms that revolution results.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.12.10 / 4pm
	
	
	spencer: Revolution is sorely needed where inadequate and unacceptable conditions exist, because they prove all the ‘normal' processes have failed—this includes existing building typologies. 
	
	
	If we think about living in 70+-storey towers, made attractive from the outside by a sexy wrapping and inside by slanted alcoves and bay windows, we really have to question being so divorced from the ground, the streets, the city. In my view the existing building typology fails, even though it is justified, financially, by putting a lot of people on a little bit of expensive ground. People do need places to live, that is certainly the case. But some new approach to the design of high-density living space needs to be imagined by architects in he first place, and then discussed vigorously by all concerned— but I think architects have to lead the way. New approaches may well need new types of buildings and will certainly need new ideas about urban planning (for earlier examples, see [this prior post](https://lebbeuswoods.wordpress.com/2008/12/11/visionary-architecture/)). It needs, to use your term, a revolution.
	
	
	I like your thought about changes in “corporeal movement” impacting architecture. What you're saying is very true, but not often said or discussed, perhaps because everyone just takes it for granted. In the context of this post you imply that today and in the future new modes of human movement have already and will continue to emerge. Identifying and understanding them will be the key to creating new building typologies and planning strategies, no doubt.
9. [korzac](http://kafee.wordpress.com)
10.13.10 / 8pm


Lebbeus you wrote….”Identifying and understanding them [future new modes of human movement] will be the key to creating new building typologies and planning strategies,..” and ..” Revolution is sorely needed where inadequate and unacceptable conditions exist, because they prove all the ‘normal' processes have failed—this includes existing building typologies.”


Well architecture is not an axiomatic system. But nevertheless it can be asked: when do you change one of the ‘axioms' or norms of architecture to see a part or the whole of architecture from a new vantage point..something like Duchamp's pissoir exhibited in a respectable gallery, or in the same way mathematicians are playing around with axiomatic systems changing their premises to obtain new theorems… Where are the new ‘theorems' of architecture hiding?





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.14.10 / 1pm
	
	
	korzac: The new theorems are out there, emerging from the thinking and works of a few architects who are taking on radical new conditions of living emerging from wars, poverty, natural disasters and the less physically, more mentally violent ‘game-changers' such as global migration, economic shifts, modes of communication and exchange, the advent of human rights and ecology movements—the list is growing. Just who these architects are and what they do and say is a subject too big for this ‘comments' section, needing a long, well-researched post or a series of them. Of course, the work proposing or based on new theorems and paradigms is relatively little, but it is there.
	3. spencer
	10.19.10 / 1am
	
	
	If the normal processes have failed that could include the architect lead process as well. We might just need to look at examples of building and construction typologies that exist outside the normal boundaries of our everyday knowledge and experience. Like Korzac is saying, getting rid of the expected from architecture might open new ideas from people who are not normally considered “experts.”
	
	
	Lebbeus, I have always appreciated your involvement in abject locations to understand architecture better (and in part you have influenced my own experimentation). You seem to do well at entering other places that few choose to go and extracting the gem of knowledge. I look forward to your continued exploration, particularly in slums.
11. Eric Ross
10.23.10 / 12am


In the movie objectified Karim Rashid makes an interesting comment regarding the plethora of chair designs in our world. 


….many have tried to design a chair (tens of thousands of different styles and looks), and all of them feel they did the best job possible and that their chair is the right culmination of aesthetics and function…..yet we continue to reinvent and restyle the same functional object with no real evolution or discernible advancement of the archetype…..(paraphrased)


The skyscraper is like the chair….


where is the value in attacking another installment in this genre….the critique of the skyscraper hasn't changed since the first designs were proposed and built at the beginning of the last century…..we don't need a revolution in designing skyscrapers to find the solution to a stale idea we need a revolution in how we live on the planet in order to necessitate a new archetype.
13. [T. Caine](http://progressivetimes.wordpress.com)
11.1.10 / 2pm


I have to agree with Eric Ross. Part of our country's problematic fixation with consumption is the redressing of products that are fundamentally the same, creating the illusion that we have new needs to fill and new “things” to buy, when in truth we are often just accumulating more stuff and generating more waste. 


Are we really helping consumers appreciate architecture more by falsely displaying an evolution that is only skin deep? Fundamental changes in the design of tall buildings have to respond to changing the way we approach urban living. With all hope, it will be a new generation of buildings that operate in a sense of balance and stasis with the rest of the urban environment to create a sustainable cityscape.
15. [THOM MAYNE'S MIND « LEBBEUS WOODS](https://lebbeuswoods.wordpress.com/2010/11/25/thom-maynes-mind/)
11.27.10 / 4pm


[…] why? For the sake of novelty? To create the illusion of progress by wrapping the same old ideas in new skins? Or to serve genuinely new ways of living in relation to ourselves and the earth? Mayne's […]
