
---
title: MIES IS MORE (amended)
date: 2010-10-16 00:00:00 
tags: 
    - Mies_van_der_Rohe
    - modern_architecture
    - row_housing
    - social_diversity
---

# MIES IS MORE (amended)


[![](media/mieslaf-10.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-10.jpg)


(above) *Lafayette Park row-housing, Detroit, designed by Mies van der Rohe.*


Is this what Mies van der Rohe meant by his remark, “Less is more”?


A minimal architecture enables a maximum variety of living within.


The spare and relatively neutral frame that such an architecture places around space not only allows without conflict all manner of furniture, bric-a-brac, appliances, artworks, as well as human activities to exist within it, but actively invites them in order to relieve its own plainness and sameness. Heavily ornamented or aggressively shaped walls, floors and ceilings defining spaces demand not only attention but also respect for what they express in themselves. If they are not respected, by juxtaposing against them aggressively different things, then the result will be aesthetically uneasy and perhaps unpleasant, depending on one's tolerance of or taste for conflict.


The neutrality of modern architecture such as that of Mies, Rietveld, Le Corbusier, and later works by Gropius, Breuer, and Bunshaft was argued as its great virtue because it did not (or so the argument goes) impose aesthetic values on an open, free, democratic society. Many modernist architects were, in fact, socialists, or flrted with democratic-socialist ideals, who placed on the exterior, public space of architecture an emphasis on the broader social good over individual self-expression and other forms of self-interest. This was a position challenged by post-modernist architects who proposed designs more in keeping with capitalist, free-market ideals of ‘anything goes,' juxtaposing—collaging—widely and sometimes wildly disparate things. In this way, the aesthetic sensibilities of our time evolved.


But in the days when Mies designed the row-housing for Detroit's Lafayette Park middle-class housing development, architectural neutrality still seemed to hold the promise of great social freedom. Seeing this series of recent interior photographs in identical units in this development, is convincing testimony that at least here the promise was kept.


LW


The photos below were made by photographer Corine Vermeulen (corinevermeulen.com):


[![](media/mieslaf-1.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-1.jpg)


[![](media/mieslaf-2.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-2.jpg)


[![](media/mieslaf-3.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-3.jpg)


[![](media/mieslaf-4.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-4.jpg)


[![](media/mieslaf-5.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-5.jpg)


[![](media/mieslaf-6.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-6.jpg)


[![](media/mieslaf-7.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-7.jpg)


[![](media/mieslaf-8.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-8.jpg)


[![](media/mieslaf-9.jpg)](https://lebbeuswoods.files.wordpress.com/2010/10/mieslaf-9.jpg)


The photos above appear in a recent New York Times article, authored by Danielle Aubert, Lana Cavar, Natasha Chandani (placementpublication.org):


<http://opinionator.blogs.nytimes.com/2010/10/14/living-with-mies/>


#Mies_van_der_Rohe #modern_architecture #row_housing #social_diversity
 ## Comments 
1. Kiel
10.16.10 / 1am


As near to “active” user-selectable architecture as has yet been achieved. Bones to be fleshed by individual taste. Gordon Bunshaft played with both sides of the same result at the poles of his long career — the participatory and the private, glazed naked Lever House and the impregnable superbunker called Hirshhorn.
3. Pedro Esteban
10.16.10 / 8am


yes, mies is more.


Will be interesting a rule to controll the use by the people in architecture, but not a rule made by layers or dictated by the architect inself. I think the architecture itself made the way of living of the people.
5. [nemethrolanddaniel](http://mocskosepiteszet.wordpress.com)
10.16.10 / 3pm


Le Nemausus social housing from Nouvel comes into my mind. He forbid the inhabitants to paint the concrete walls of their flats, fearing that it would ruin the aesthetics…
7. [Maria del vicente](http://Www.mariadelvicente.es)
10.16.10 / 8pm


Good post! 


We need to design an architecture Clean without ornament, where The flexibilty and the economy are the most important specifications.


Fernando Diaz.  

 FDG arquitectura MARIA del VICENTE
9. [planement](http://placementpublication.org/)
10.17.10 / 5pm


Thank you very much for posting the photographs from the New York Times story  

<http://opinionator.blogs.nytimes.com/2010/10/14/living-with-mies/>


We would really appreciate if you could credit the authors of the project: Danielle Aubert, Lana Cavar, Natasha Chandani (placementpublication.org) and the photographer Corine Vermeulen (corinevermeulen.com)





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	10.17.10 / 11pm
	
	
	planement: Thank you very much for pointing out my omission. I have added the credits to this post and noted the amendment in its title.
	
	
	
	
	
		1. [planement](http://placementpublication.org/)
		10.19.10 / 9am
		
		
		Thank you very much! We are most pleased you're featuring it on your blog
		
		
		Lana
11. [Diego](http://opacos.wordpress.com/)
10.18.10 / 8am


The knowledgeable void as a possibility of fullness, as a latency, it seems a game to that which are invited to complete the work… I guess.
13. spencer
10.19.10 / 1am


Lebbeus,


I enjoy your point about architecture reflecting a political ideology. Most of what we create has a political agenda buried within it. Seeing an architect use, in this case, his medium to intentionally express this boarders on genius. I have always appreciated what Mies has given to us later architects and his legacy has led to amazing buildings.


But (of course), I do not agree with you in the notion that, in this building at least, Mies has created a maximization of living. Unless you mean the inhabitants have been provided X options for furniture arrangement or are referring to the infinite styles of accouterments that can be place in the same unit, I hardly see this version of the reductionist box for living with the qualities expressed. 


If you pay close attention to furniture you will see in the included photos about 4/5 layouts of sofas, chairs and tables and none of which revolutionize the space. Three use a large mirror to create the sense of a larger living area and, probably, to reflect natural light from the one wall available for entering sunlight. Almost all use a wall of color, but those that don't seem a little more spacious. 


If you mean that this building provides a blank canvas that allows anyone to decorate it to their personal tastes, this building succeeds beyond belief. I do believe this could be done in most any building just some furniture and “art” styles just have a more difficult time meshing with their background. Hence I would say in this study I have an uneasy feeling about several of the units because the inhabitants style clashes either too little or not enough with the Architecture or goes way overboard on following the manifesto. Only one reaches the fullest potential of personal style merging with the architecture but, in my opinion, I can't understand how they can live without a family photo!


Still, I understand and firmly believe in your point. Buildings should allow for the adaptation of a multitude of users over the course of its lifetime. I also believe that Mies' greatest contribution was not in Less is More, but God is in the Details. I suppose my feet are more firmly planted in Venturi's (old) camp (thank you Bill Bowler!). As the photos above are proof of, we are creatures of decoration and adornment (sometimes little or no ornament is, well, ornament). I prefer to see buildings that reflect culture at its point in time rather than being a blank backdrop. For me, Mies' outsides will always exceed his insides as furthering the greatness of buildings. There are other architects who have taken his lead and transformed his architectural theory into greatness in Both outside And inside.
15. [Pearce](http://www.carbon-vudu.com)
10.23.10 / 12am


It appears to me that this is “Safe Architecture”, inside and out. I agree ‘Mies is more' than this.


I find it amazing how many people jump on the Modern Movement bandwagon and cater their lifestyles and architectural designs to such a safe haven. That is just my limited perspective though. I love seeing what Mies has done, hearing the way he ran his practice–great stuff. I am just not fond of these spaces or of the architecture. I can see why Frank Lloyd Wright had a problem with these folks back in the day.


Nice words Spencer! I love the part about, “reflect culture at its point in time”.  

mp
17. jenny
10.24.10 / 2pm


Good point Spencer. Furniture arrangement is not aesthetic approach to space. Besides this photos revealed little about quality and use of space in these lots. Maybe the problem is in the editorial approach of that project since very little of political, cultural, architectural themes relevant for Mies architecture is opened with it. People will find their styles of living (and decoration) in any architecture whether good or bad.
19. [Pearce](http://www.carbon-vudu.com)
10.29.10 / 4pm


I just want to re-comment on my last line above: “Nice words Spencer! I love the part about, ‘reflect culture at its point in time'”.


Mies did reflect culture, politically, ideological, in time and culture. This is social freedom as LW states.


However, the architecture, exterior/interior is so safe in its design, pure to Mies's ideology of “universal space” perhaps but feels spiritless. To design for the mainstream, the vast majority, and to be the end all be all use, seems to lose its connection with people—the spirit of people.


Can this notion of design have charisma, wisdom, maybe have a embedded energy about it that encourages people to be in awe, like Wood's latter posts, “Pretty Dam Pure”?


Probably not at that level, but what Woods, Eisenman, and many others have written and designed spaces/programs/functions that were once designed for a specific purpose/client, but were abandoned and another space/program/function adapted to it, making it truly dynamic as a use—it is mandatory creativeness. Perhaps this post is in reference to Lebbeus's, Radical Reconstruction, “The Question of Space” paragraph 6, is Mies, but the information before and aft is just.]


A simple module of a grain silo, a mining town, a military craft seems sexier and livelier than the retro modern box.  

Again, this is just taste, my taste, like all the finishes, interior décor and the vast array of people above in the images of this post.  

mp
21. [The Fox Is Black » Less is More](http://thefoxisblack.com/2010/10/18/less-is-more/)
11.15.10 / 7am


[…] Venturi (Less is a Bore) or Bjarke Ingels (Yes is More). But Lebbeus Woods has rephrased the quote here and added few paragraphs that describe larger, politically-charged goals of modern architects that […]
