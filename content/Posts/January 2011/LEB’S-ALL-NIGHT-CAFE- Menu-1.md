
---
title: LEB’S ALL-NIGHT CAFE  Menu-1
date: 2011-01-26 00:00:00
---

# LEB'S ALL-NIGHT CAFE: Menu-1


[![](media/lebscafe-logo12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lebscafe-logo12.jpg)The Cafe is finally, actually, open, after the usual construction delays, and of course the picky architect, who fussed endlessly over details.


So, drop in, check out the [Menu-1](http://lebsallnightcafe.wordpress.com/2011/02/20/menu-1-much-simplified-navigation/), created by my selecting the most pointed questions from the many submitted by the readers of the LW blog.   To all who submitted questions, I want to express my gratitude, and to them I want to say that while all questions were not used, it's still   possible for some to appear on future Menus. I also want to ask for the understanding of those whose questions were used that it was necessary to do some editing, which I think has not altered any meaning. If it has, please add a comment below informing me of any errors—I will correct them as quickly as possible.


All who visit the Cafe are invited to respond to the questions on the Menu. A number of the regulars have received special email invitations for the grand opening. I'm hopeful they'll drop by, select a choice question or two, and leave their responses in the Comments section. Remember, all Comments will be moderated.


Hope you enjoy!



 ## Comments 
1. Cheri
1.31.11 / 3pm


silent, but violent…
3. [chris teeter](http://www.metamechanics.com)
2.1.11 / 2am


AWESOME, man right up my alley. Going to be feverishly researching this if more is out there. Spent grad school playing with magnets and studying fungi and spores distribution.


Was using particle systems and dynamics in 3dsMax to re-create fungi growth in backyard forest based on geography and dominant weather patterns. The magnets were glued to plexi glass close enough to form shapes when plexi was heated..this blog presents a fusion here for me, thank you.


Getting back into Howard T Odum who came up with a very ingenious way of observing energy flow in the enviroment via electrical like diagrams also just learned of the Baxter Experiment (I think) where I believe an ex CIA agent got into using a lie detector on plants and noticed something to the effect that plants can pick\_up or react to human emotions if in vicinity (may be incorrect on a few points here).


Nothing esoteric or trippy about any of this really, its all fact latent waiting for discoveries and work like above.


Very good post. And I thought I was going to do some construction details tonight…
5. [chris teeter](http://www.metamechanics.com)
2.1.11 / 2am


Cleve Backster experiment. Off the top of my head pretty close, used a polygraph on leaf to detect electrical resistance, etc…
7. Sotirios
2.3.11 / 4am


Dear Chris, your magnetic fungus experiments sound amazing. How did magnetism affect fungal growth? Baxter's bio-communication theory is truly fascinating.  

Wireless linkup to other biological life is occurring all around us and we can learn so much from it. You are right this stuff is definitely not esoteric but totally concrete. I believe many of the ‘sustainable' designers will continue to walk in the dark unless they comprehend the world as a network of visible and invisible exchanges of energy. Today many architects try to outdo one another with low carbon prescriptions; they trim some energy consumption here and there, protest a bit and call it green. These timid actions only serve to defend superannuated models of thinking. We are slowly beginning to understand the intensities of energy required to produce and provide goods and services through carbon calculations, but then we process these numbers with an old problematic model: the market. Why would we run environmental action through the illusions and bubbles of a market as the carbon credit trading system attempts to do? Why should we let speculation drive the exchange of energy? Why is the energy consumption discourse so asymmetrically focused on carbon? I think we should tune into the vast array of electromagnetic frequencies to get a sense of how different forms of energy are really transmitted and exchanged out there. How do biological forms of life embody energy emissions? Imagine the construction details we will be designing in that zone!
9. [chris teeter](http://www.metamechanics.com)
2.4.11 / 2am


Sotirios – need to clarify, I was so excited when I read this I inadvertantly presented two separate experiments as one, the fusion was inspired by this post. I see a point now to those dabblings, how to make it one. Fungi is a very interesting specie, there are fungis 1000's of years old and the size of football fields slowly digesting the world around them.


To answer your questions its about man's obsession with speed and raw power, and I mean “men” not all of humanity. What's faster than fast – explosions. Burning fossil fuels in seconds that took 100's and 1000's of years to create. It supposed to feel like the industrial age was a quantum leap forward for humanity, arrive at desired concept first regardless of balancing the matter and energy leading up to it.


We are essentially slowing down now, tweaking back.


Biology was always a complicated subject for me since simple logics that exist in math couldn't solve every problem in biology simply. What amazed me was the fact electric current exists at the small biological level, but the current is so much less than the explosive power we are used to.  

We are starting to tweak down and harness the subtle currents. The carbon obsession is an anti explosion power stance, but if architects are to lead the way we should just tune in now to electromagneticm and skip the downsizing of the carbon footprint, the general public has caught on, they know we need to do this.


Keep up the good work and I will look out for you stuff…and when time allows – electomagnetic fungi spore fields…


BIM and parametric time based geometry as morphing modules of energy transformation or translation – construction details. Appearing artificially intelligent, cellular auotomata, managing the states of energy as we coast through space zoned out in virtuality. The new brick, a building built and unbuilt, formed and digested, energy collapsing and exploding slowly at points of interest, points of interest based on the human desire to experience the senses and to snap out of virtuality for a few minutes. Put the crackberry down and smell the roses, a space built by biological blocks acting on electromagnetic wavelenghts..
11. [General](http://www.general.la)
2.7.11 / 9am


To be precise, the level of our awareness defines the perceptual resolution at which we consider our interaction with our environments. The more we realize what it means to be human beings, the more our built environments will reflect this awareness. thus, the consideration at the human is more than the corporeal body, but also consists of a subtle electromagnetic body that resonates in harmony with the earth's electromagnetic field, itself pulsing at the Schumann resonance of 8htz. When this is considered, then perhaps the electromagnetic effects of our creations, both materially and geometrically, may be redesigned to better holistically suit this more complete notion of harmonic space. perhaps even this ‘new' awareness will actually be a rediscovery of ancient principles and eternal sacred sciences of the human applied to create truely sustainable and mythological starchitecture.
13. Scott R
7.21.11 / 12pm


Good to read the new stuff Sotor! Might quote some for an upcoming article if that's okay.
