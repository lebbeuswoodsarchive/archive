
---
title: SOTIRIOS KOTOULAS  Emission Architecture (updated)
date: 2011-01-27 00:00:00 
tags: 
    - architectural_research
    - architecture
    - space
---

# SOTIRIOS KOTOULAS: Emission Architecture (updated)


[![](media/wave-14a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-14a.jpg)


*Following on two previous posts ([1](https://lebbeuswoods.wordpress.com/2010/12/03/da-vincis-blobs/), [2](https://lebbeuswoods.wordpress.com/2010/12/18/slipstreaming-2/)) concerned with the structure of fluid dynamic bodies, the following research by Sotirios Kotoulas is presented.*


*LW*


**EMISSION ARCHITECTURE**


**SIXTY CONDITIONS AND CONCEPTS OF A HALLUCINATORY ARCHITECTURE**


Manual for the Circumpolar City diagram


When one swims in ocean water between the break of a powerful wave and the shore, the currents pull the swimmer to and fro. When the intensities of the break and backwash, the push and pull, cancel each other out, the swimmer becomes trapped. He will not be pushed to shore because the pull away is too strong. In this moment the swimmer has to invent new strokes to navigate the resistant currents while expending the least energy. The currents will quickly exhaust a swimmer who does not tune into micro currents that could help him reach the shore. Feeling the force of the oncoming currents the swimmer thinks of her or his surroundings in terms of energy.


1.     Forces are energies that pulsate between bodies.


2.     The environment and its inhabitants interact through energy transfers.


3.     All organic and inorganic matter metabolizes on frequencies that are both visible and invisible.


The trapped swimmer trying to reach shore must consider how a wave recedes, how much water it can lift depending on current fluctuations, and therefore how much energy to expend. The swimmer disturbs the currents, creating other bodies of force. New eddies take shape, new forms with which the swimmer travels. The currents of energy define the structure of hallucinatory architecture.


4.     Electromagnetic activity is particularly intense at the North Magnetic Pole. The Arctic is prone to powerful solar electric storms attracted to the region by the immense pull of the magnetic north, the same magnet that provides us with cartographic and navigational orientation.


5.     Electromagnetic storms can disrupt orientation and communications systems. They can trip generators and cause blackouts to major cities in the South. The exchange of forces between solar/geo-magnetic activity and the body's electrical field induces apparitions.


6.     This hallucinatory state is produced when the body makes direct contact with electromagnetic activity.


7.     What results is a space within a space, what we might call emission space, where the conditions, forms, dimensions, qualities, proportions, and narratives are authored by an effected body.


8.     Mild electrical currents can trigger hallucinations and immerse the hallucinator in a series of intense somatic environments. Some are audible, others spatial, atmospheric, and tactile.


9.     Hallucinatory space exists in measurable space. We carry it with us as a spatial extension of our contact with the external world.


10.   When hallucinating, one usually knows where the experience takes place, and thus the event is somewhat framed[.](https://www.me.com/my/core_mail/mail_theme/en/2F23/empty.html#_msocom_6)


11.   Between bodies and electromagnetism immeasurable space is produced within measurable space.


12.   Moving through the range of charged electromagnetic frequencies, one's cognitive faculties are disturbed and the physical conditions of the place transform. Spaces mutate differently as the body contacts x-rays, gamma rays, microwaves, visible light, or infrared radiation. Electromagnetic radiation triggers hallucinations that envelop the inhabitant. If the space is audible and tactile, the more excited parts of the body may define the inhabitant, perhaps a network of organs and the nervous system .


13.   The blend of emissions stimulates other hallucinations in nearby bodies.


14.   Emissions may be thought of as machines since they act on the environment around them.


15.   Unlike machines, emissions cannot be precisely programmed.


16.   As emissions integrate with the surrounding environment, their levels and intensities adjust, sometimes very unevenly.


17.   The architect programs energy. She or he identifies and relates the variety of intensities from the cosmic scale to the quantum: from the force of an ocean current to the intensity of an x-ray shower, from a man-made electromagnetic climate to the energy of labor.


18.   The body has little control as an active contributor. It does not oversee energy transfers.


19.   The architect adjusts the atmospheric charges to induce hallucinations and enhance perceptual cognition.


20.   In some sectors the architect creates gardens with different species of stimulating frequency waves. In other areas the city may have to regulate the flows.


21.   Traditional tectonics, which have held architecture together until now, are no longer sufficiently explanatory. In the x-ray, walls are no longer barriers but thresholds, gamma waves host the neural systems of bodies, and brain waves rapidly change the scenery and scramble scale.


22.   The intersection of cosmic, terrestrial and bodily orders orients emission architecture. New modes of orientation and language are required.


23.   This dimension of architecture goes beyond the tired subject-object dichotomy.


24.   Matter, body, and electromagnetic frequencies band together and form an interdependent environment hovering between the visible and invisible.


25.   Emission architecture reorients the invisible, palpable electromagnetic activity between bodies, materials and landscapes.


26.   The space of emission architecture is not defined by four walls. The vertical axis is bent.


27.   Emission space is a constructed discharge involving bodily organs gently electrocuted by their host environments.


28.   Emission space is unhinged from the horizontal datum and assaults habit.


29.   Repetitive, ordinary, and familiar environments breed habits that over time conceal and numb perception.


30.   Hallucinatory environments pull the inhabitant from his areas of comforts. Hallucinatory space is seductive.


31.   Hallucinatory space is not necessarily three-dimensional and it is not always visual.


32.   The exact kind of space this exchange produces remains in question, as does its materiality.


33.   Who are the inhabitants of this space and how do they inhabit it?


34.   How is turbulence absorbed by emission space and how does one enter and exit it?


35.   Can the architect fully program the electromagnetic transmissions between bodies and matter?


36.   What effects do intensities have on different people?


37.   How do memories of hallucinatory space re-inform the construction of emission architecture?


38.   How will this architecture bind itself to the organs of the body?


39.   It is possible to spread electronically charged particles across a large geographic region. Charges can be turned into weapons when fired from high altitudes and carried along ionization belts down to the lower elevations where the activity can burn through electrical transmission systems and black out a city.


40.   The survey is a diagram of a section of forces across different frequencies of electrical and corporeal activity around the Arctic Circle.


41.   The survey is a diagram of the Circumpolar City


42.   The survey maps the ionized network of Circumpolar City


43.   The survey records the energy transfers of a Circumpolar City


44.   The circumference of the circle spans the 60°- 75° degree North bands of latitude.


45.   The Circumpolar City connects Arctic settlements.


46.   The underlying diagrams, rendered with a marker, map the higher intensity electric flows.


47.   The ionized activity is engraved in glass with a diamond tip.


48.   Lines of various intensities notate the fields. The marks inscribe the force traveling through the line.


49.   Data must be processed in order to create something from it.


50.   In this model the data are processed to create a sectional survey of a hallucination.


51.   The lines are engraved into the glass surface. These appear when they are illuminated.


52.   The drawing is a light membrane.


53.   Light intensities are photographed. They map the survey.


54.   The survey adjusts to the intensity and type of light.


55.   This survey tracks data whose intensities are strong enough to induce hallucinations in people.


56.   The survey materializes. Drawing electromagnetic activity as water.


57.   The wave cycle meets electromagnetic light.


58.   The electromagnetic fields are captured in 3 states: liquid, solid, vapor.


59.   Lines cycling between magnets and ice jams.


60.   Lines that explore, wander, burn out.


Wave 01:


[![](media/wave-1a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-1a1.jpg)


[![](media/wave-1a-det13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-1a-det13.jpg)


[![](media/wave-1a-det2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-1a-det2.jpg)


[![](media/wave-1a-det3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-1a-det3.jpg)


Wave03:


[![](media/wave-3b1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-3b1.jpg)


[![](media/wave-3a-det1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-3a-det1.jpg)


[![](media/wave-3a-det21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-3a-det21.jpg)


[![](media/wave-3a-det31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-3a-det31.jpg)


WAVE 06:


[![](media/wave-6a1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-6a1.jpg)


[![](media/wave-6a-det11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-6a-det11.jpg)



[![](media/wave-6a-det2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-6a-det2.jpg)


[![](media/wave-6a-det31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wave-6a-det31.jpg)


**SOTIRIOS KOTOULAS**


*LW: And the next wave is on the way….*


*Sotirios Kotoulas continues his adventures in electromagnetic wave worlds, back at the [Wuskwatim Dam](https://lebbeuswoods.wordpress.com/2010/10/25/pretty-dam-pure/). He writes:*


“It is incredible, the dam has progressed so much. They were detonating the landscape today, I saw them set up dynamite all over, the explosion sends a shock wave. The last course of freshly laid concrete blocks shook out of place ! The site is totally sealed and it is very warm so you can work in your t shirt. The winds outside are nasty, they have teeth. The dam is a little city, I have been exploring it every day. It is an Italian futurist city, been thinking of Antonio Sant' Elia.  

There are all these mysterious electronic machines all over the place, men and women soldering parts together, copper wires all over, and panels that look like parts from a space craft.  

The river that was next to the dam is no longer there, it has been rerouted through the structure. Man-made magic!”


[![](media/wusklhiver-1a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wusklhiver-1a.jpg)


[![](media/wusklhiver-5a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wusklhiver-5a.jpg)


[![](media/wusklhiver-2a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/wusklhiver-2a.jpg)


#architectural_research #architecture #space
 ## Comments 
1. Cheri
1.31.11 / 3pm


silent, but violent…
3. [chris teeter](http://www.metamechanics.com)
2.1.11 / 2am


AWESOME, man right up my alley. Going to be feverishly researching this if more is out there. Spent grad school playing with magnets and studying fungi and spores distribution.


Was using particle systems and dynamics in 3dsMax to re-create fungi growth in backyard forest based on geography and dominant weather patterns. The magnets were glued to plexi glass close enough to form shapes when plexi was heated..this blog presents a fusion here for me, thank you.


Getting back into Howard T Odum who came up with a very ingenious way of observing energy flow in the enviroment via electrical like diagrams also just learned of the Baxter Experiment (I think) where I believe an ex CIA agent got into using a lie detector on plants and noticed something to the effect that plants can pick\_up or react to human emotions if in vicinity (may be incorrect on a few points here).


Nothing esoteric or trippy about any of this really, its all fact latent waiting for discoveries and work like above.


Very good post. And I thought I was going to do some construction details tonight…
5. [chris teeter](http://www.metamechanics.com)
2.1.11 / 2am


Cleve Backster experiment. Off the top of my head pretty close, used a polygraph on leaf to detect electrical resistance, etc…
7. Sotirios
2.3.11 / 4am


Dear Chris, your magnetic fungus experiments sound amazing. How did magnetism affect fungal growth? Baxter's bio-communication theory is truly fascinating.  

Wireless linkup to other biological life is occurring all around us and we can learn so much from it. You are right this stuff is definitely not esoteric but totally concrete. I believe many of the ‘sustainable' designers will continue to walk in the dark unless they comprehend the world as a network of visible and invisible exchanges of energy. Today many architects try to outdo one another with low carbon prescriptions; they trim some energy consumption here and there, protest a bit and call it green. These timid actions only serve to defend superannuated models of thinking. We are slowly beginning to understand the intensities of energy required to produce and provide goods and services through carbon calculations, but then we process these numbers with an old problematic model: the market. Why would we run environmental action through the illusions and bubbles of a market as the carbon credit trading system attempts to do? Why should we let speculation drive the exchange of energy? Why is the energy consumption discourse so asymmetrically focused on carbon? I think we should tune into the vast array of electromagnetic frequencies to get a sense of how different forms of energy are really transmitted and exchanged out there. How do biological forms of life embody energy emissions? Imagine the construction details we will be designing in that zone!
9. [chris teeter](http://www.metamechanics.com)
2.4.11 / 2am


Sotirios – need to clarify, I was so excited when I read this I inadvertantly presented two separate experiments as one, the fusion was inspired by this post. I see a point now to those dabblings, how to make it one. Fungi is a very interesting specie, there are fungis 1000's of years old and the size of football fields slowly digesting the world around them.


To answer your questions its about man's obsession with speed and raw power, and I mean “men” not all of humanity. What's faster than fast – explosions. Burning fossil fuels in seconds that took 100's and 1000's of years to create. It supposed to feel like the industrial age was a quantum leap forward for humanity, arrive at desired concept first regardless of balancing the matter and energy leading up to it.


We are essentially slowing down now, tweaking back.


Biology was always a complicated subject for me since simple logics that exist in math couldn't solve every problem in biology simply. What amazed me was the fact electric current exists at the small biological level, but the current is so much less than the explosive power we are used to.  

We are starting to tweak down and harness the subtle currents. The carbon obsession is an anti explosion power stance, but if architects are to lead the way we should just tune in now to electromagneticm and skip the downsizing of the carbon footprint, the general public has caught on, they know we need to do this.


Keep up the good work and I will look out for you stuff…and when time allows – electomagnetic fungi spore fields…


BIM and parametric time based geometry as morphing modules of energy transformation or translation – construction details. Appearing artificially intelligent, cellular auotomata, managing the states of energy as we coast through space zoned out in virtuality. The new brick, a building built and unbuilt, formed and digested, energy collapsing and exploding slowly at points of interest, points of interest based on the human desire to experience the senses and to snap out of virtuality for a few minutes. Put the crackberry down and smell the roses, a space built by biological blocks acting on electromagnetic wavelenghts..
11. [General](http://www.general.la)
2.7.11 / 9am


To be precise, the level of our awareness defines the perceptual resolution at which we consider our interaction with our environments. The more we realize what it means to be human beings, the more our built environments will reflect this awareness. thus, the consideration at the human is more than the corporeal body, but also consists of a subtle electromagnetic body that resonates in harmony with the earth's electromagnetic field, itself pulsing at the Schumann resonance of 8htz. When this is considered, then perhaps the electromagnetic effects of our creations, both materially and geometrically, may be redesigned to better holistically suit this more complete notion of harmonic space. perhaps even this ‘new' awareness will actually be a rediscovery of ancient principles and eternal sacred sciences of the human applied to create truely sustainable and mythological starchitecture.
13. Scott R
7.21.11 / 12pm


Good to read the new stuff Sotor! Might quote some for an upcoming article if that's okay.
