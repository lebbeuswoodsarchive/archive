
---
title: THE STATE OF THE ART
date: 2011-01-25 00:00:00 
tags: 
    - architecture
    - real_estate_development
    - social_class
---

# THE STATE OF THE ART


[![](media/miami-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/miami-1.jpg)


January 23, 2011


# **A Miami Beach Event Space. Parking Space, Too.**


by [Michael Barbaro for the New York Times](http://www.nytimes.com/2011/01/24/us/24garage.html?src=ISMR_HP_LO_MST_FB)


MIAMI BEACH, Fla. — For her wedding over the weekend, Nina Johnson had worked through a predictable checklist of locations in town: hotel ballrooms, restaurant halls and catering outfits.


In the end, though, she opted for the most glamorous, upscale and stylish setting she could find — a parking garage.


“When we saw it, we were in total awe,” said Ms. Johnson, 26, an art gallery director. “It's breathtaking.”


Parking garages, the grim afterthought of American design, call to mind many words. (Rats. Beer cans. Unidentifiable smells.) Breathtaking is not usually among them.


Yet here in Miami Beach, whose aesthetic is equal parts bulging biceps and fluorescent pink, bridal couples, bar mitzvah boys and charity-event hosts are flocking to what seems like the unimaginable marriage of high-end architecture and car storage: a $65 million parking garage in the center of the city.


They are clamoring to use it for wine tastings, dinner parties and even [yoga](http://topics.nytimes.com/top/reference/timestopics/subjects/y/yoga/index.html?inline=nyt-classifier) classes. Or taking self-guided tours, snapping photographs and, at times, just gawking.


Created by a colorful Miami developer and a world-renowned architecture firm, it appears to be an entirely new form: a piece of carchitecture that resembles a gigantic loft apartment, with exaggerated ceiling heights, wide-open 360-degree views and no exterior walls. The structure, 1111 Lincoln Road, is so distinctive that Ms. Johnson put its image on her 230 wedding invitations.


It is, in many ways, an ode to Miami's flashy automobile culture. Rather than seeking to hide cars, as garages have done for decades, it openly celebrates them.


While car enthusiasts rejoiced, eager to showcase their Aston Martins and Rolls-Royces, something unexpected happened. Ordinary people, many from far beyond Miami, came too — with no intention of parking there.


“I went to the top and worked my way down,” said Peter Lampen, an architect who traveled 1,200 miles from New Jersey to see the seven-story garage.


Ben Traves, a graduate student, has taken so many photographs of the building that security guards have shooed him out. “I am just really drawn to it,” he confessed the other day as he toted his camera around the structure.


The garage has an unlikely back story. Its developer, a contemporary art collector named Robert Wennett, bought the property in 2005, inheriting a drab-looking bank office and an unremarkable parking lot at the corner of two well-known boulevards, Lincoln and Alton Roads.


Quirky zoning regulations in the city, which is chronically short on parking, made it profitable to build a large garage — not everyone's vision of a grand gateway to the retail and restaurant-filled streets that surround the site.


Mr. Wennett, who sprinkles his properties with $1 million [Dan Graham](http://topics.nytimes.com/top/reference/timestopics/people/g/dan_graham/index.html?inline=nyt-per) sculptures and admits that he hates most of the garages he has ever parked in, aimed high, interviewing 10 top architects around the world. He settled on Herzog & de Meuron, a Swiss firm best known for transforming a power station into the Tate Modern gallery in London and designing the Olympic stadium in Beijing (known, by its appearance, as the Bird's Nest).


Mr. Wennett told the architects that he wanted something close to the grand hall of a train station — big, airy, light-filled and head-turning. What they produced, in early 2010, was all those things: a garage with floor heights of up to 34 feet, three times the norm; a striking internal staircase, with artwork embedded in its base; precarious looking (and feeling) ledges that rely on industrial-strength cable to hold back cars and people; and a glass cube that houses a designer clothing store, perhaps the first in the middle of a parking garage.


In a final flourish, the architects created a soaring top floor that doubles as an event space, with removable parking barriers. It can be rented for about $12,000 to $15,000 a night.


“This is not a parking garage,” Mr. Wennett said. “It's really a civic space.”


And a private home. Mr. Wennett built himself a large penthouse apartment on the roof.


A handful of well-known architects have dabbled, reluctantly, in parking — in the 1960s, [Paul Rudolph](http://topics.nytimes.com/top/reference/timestopics/people/r/paul_rudolph/index.html?inline=nyt-per), dean of architecture at Yale, designed a giant garage in downtown New Haven. But it seemed to reinforce rather than buck convention, with its dark corridors and imposing scale.


The structure in Miami Beach, by contrast, “sets a new bar for what parking garages could and should be,” said Cathy Leff, the director of the Wolfsonian museum of design here. Garages in Miami and around the country, in their attempts to blend in, she said, “become the most identifiable buildings because they are these big, hideous boxes.”


Not all the reviews are fawning. In interviews, several Miami drivers grumbled about the cost of parking in the garage — typically $4 an hour, depending on the time of day, compared with about $1 an hour in nearby municipal lots.


And a few wondered if Miami Beach, already legendary for its kitsch, should become known around the world for creating a thrilling place to park. “It says something about the aesthetic down here,” said Lisa Gottlieb, a film professor who lives in Miami Beach. “I guess this is what we bring to the table — a fancy parking garage.”


For the ultra-luxury-car set here, however, the garage has become an irresistible display case to make their consumption more conspicuous.


“I wouldn't even think of parking anywhere else when I'm downtown,” said Douglas Sharon, a financial adviser, who steers his gray Ferrari into the garage several times a week.


Perhaps fittingly, Ms. Johnson was introduced to the building as a driver, not a bride. Parking her car there, she was taken by the views and theatrical space, which she softened for the wedding with potted trees, candles and red roses.


Even so, she acknowledged that it was somewhat tricky explaining to her guests why she decided to hold her black-tie wedding amid the concrete, and just above their own parked cars. Some family members wondered if she was kidding. One cracked a joke about the band playing from the back of a pickup truck.


“When they saw the space,” she said, “they got it.”


[![](media/miami-31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/miami-31.jpg)


[![](media/miami-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/miami-4.jpg)


[![](media/miami-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/miami-2.jpg)


#architecture #real_estate_development #social_class
 ## Comments 
1. [Tweets that mention THE STATE OF THE ART « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2011/01/25/the-state-of-the-art/?utm_source=pingback&utm_campaign=L2)
1.25.11 / 5am


[…] This post was mentioned on Twitter by Christopher, KpaxsArchi. KpaxsArchi said: THE STATE OF THE ART: January 23, 2011 A Miami Beach Event Space. Parking Space, Too. by Michael Barbaro for th… <http://bit.ly/h4icJH> […]
3. Mark
1.25.11 / 7pm


I don't believe we live in a golden age of automobiles any longer; I think they have become a symbol of vanity, waste and a corrupt industry closely associated with oil wars. 51% of deaths among 18-24 year olds in America. How can we continue to celebrate the core of such an epidemic? It's simply irrational. 


Parking garages as a vestigial relics to be re-inhabited are fascinating, however. Extant buildings re-purposed, with all their idiosyncratic twists, ramps, over-built structures, open-air perimeters and rooftops, would introduce far more revolutionary architecture. It would be an architecture embodying the changing of a culture back to the scale of people and civility.
5. ...
1.25.11 / 9pm


Nothing but Yuck…
7. Caleb Crawford
1.25.11 / 11pm


I agree completely with Mark's post regarding the end of the age of the automobile and all that it represents. However, this structure is pretty fascinating in a primal way – a Maison Do-Mi-No left in its elemental condition. I am sure that many have experienced the desire to freeze a building in construction to preserve the raw simplicity of its structure.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.26.11 / 12pm
	
	
	My reason for posting this article is shown in the difference of opinion between Caleb and Charlotte. Charlotte's “Yuck” is I suspect a reaction to the social condition—one of conspicuous wealth and consumption—while Caleb's ‘fascination' is with architectural expressiveness of the structure. No doubt, Herzog and de Meuron are very clever and talented designers, and for some—like me—to see their gifts put in service of the rich, while so many more urgent problems (refer to many past posts here) go un-addressed by “the top architects in the world” is very disheartening. From my point of view, the best talent should work on the most difficult problems. But, I recognize, opinions will continue to differ on this point.
9. Manel R
1.27.11 / 11am


As it was posted, i failed to understand if the meaning was ironic or in a way a praise. Maybe I still do.  

It's discourse is intelligent. It tackles everyday urban problems, it reinvents and designs a building in subtle ways.  

But it is a piece which survives based on its idiosyncrasy. It won't work the same way when it's multiplied, repeated and vulgarized.  

It preys on anonymity. Effectively and beautifully.  

Nice challenge!





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.27.11 / 2pm
	
	
	Manel R: Thanks. You understand perfectly.
11. [The Antiplanner :: A Parking Garage Even (Some) New Urbanists Can Love :: http://ti.org/antiplanner](http://ti.org/antiplanner/?p=5211)
6.2.11 / 7am


[…] As difficult as it may be to believe, the high ceilings of some floors make them attractive for weddings and similar […]
