
---
title: INSTANT CITIES  Haiti
date: 2011-01-13 00:00:00 
tags: 
    - earthquake
    - Haiti
    - instant_cities
    - urban_migration
---

# INSTANT CITIES: Haiti


When the avant-garde group Archigram introduced the term [‘instant city'](http://www.archigram.net/projects_pages/instant_city.html) into architectural discourse nearly a half-century ago, they were inspired by the mobility of contemporary society and its dependence on high technology to enable urban migration and make it creative and exciting—a dynamic, uniquely contemporary way of living. Their model was the circus, which they admired for its élan as much as the excitement of its continuous performances. In their drawings and models they were projecting a future of adventurous hedonism, a liberation of the sort projected by [Constant](https://lebbeuswoods.wordpress.com/2009/10/19/constant-vision/), though emphasizing human creativity less than leisurely consumption. Their visions fulfilled the idea that the city was not to be a monumental srtifact of civilization, but a tumultuous, ever-changing process. “Each generation must build its own city,” [Antonio Sant'Elia](https://lebbeuswoods.wordpress.com/2009/11/02/santelias-words/) had proclaimed, and Archigram and Constant, in their own very different ways, fulfilled his demand at the same that that they speeded-up the process.They never realized—or acknowledged—that the modern age could create utterly different kinds of ‘instant cities,' the hastily constructed communities of urban dwellers displaced by catastrophes of both human and natural origin—war, economic disaster, hurricane, earthquake. After all, where was the role for architects in them? What could architects do to turn these instant cities into affirmations of the human spirit? Architecture is about planning. How can architects plan for the unplanned, for the inpredictable? Exactly….


.


The following images are reprinted from a [New York Times article](http://www.nytimes.com/packages/flash/newsgraphics/2011/0109-haiti-map-html/?hp) showing many examples of the transformation of Port-au-Prince, Haiti, as a result of the earthquake that struck on January 12, 2010, causing building collapses killing more than 200,000 people.


Satellite photo of the Port-au-Prince golf course, before January 12, 2010:


[![](media/haiti-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-11.jpg)


January 16, 2010:


[![](media/haiti-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-21.jpg)


One year later, January 12, 2011:


[![](media/haiti-31.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-31.jpg)


Today, inside the ‘instant city'—a school classroom:


[![](media/haiti-42.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-42.jpg)


An impromptu market:


[![](media/haiti-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-5.jpg)


A typical street:


[![](media/haiti-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-6.jpg)


A resident entrepreneur has set up a video  games shop, for which he charges players a fee:


[![](media/haiti-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-7.jpg)


Satellite photo of the Port-au-Prince airport, before January 12, 2010:


[![](media/haiti-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-8.jpg)


January 16, 2010:


[![](media/haiti-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-9.jpg)


One year later, January 12, 2011:


[![](media/haiti-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-10.jpg)


Inside the ‘instant city': a relatively rare ‘permanent' house:


[![](media/haiti-111.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-111.jpg)


[![](media/haiti-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-13.jpg)


[![](media/haiti-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/haiti-12.jpg)


LW


#earthquake #Haiti #instant_cities #urban_migration
 ## Comments 
1. Pedro Esteban
1.13.11 / 1pm


Oh my god!  

I don't have words if we speak about the social matter.


I love this question!! Working on it!  

 How can architects plan for the unplanned, for the inpredictable? Exactly….
3. [Francisco Vasconcelos](http://csxlab.org)
1.13.11 / 6pm


The sadness of the situation is that if this was an economic crisis, there would be no lack of help, from FMI and other institutions. So has this is an humanitarian suffering, the world leaders don't care because they can't get profit by being kind.


Has Architects in this situation we can't do much unless public and leaders opinions shift radically. Off course we can get imagination on low cost constructions and all sort of imaginative solutions, but we will always be stopped by the general economic zeitgeist times that we live.


The Architect today, I believe should enter politics with its ethics towards the humans and against the mormonist economist moralism.
5. [Emmanuele](http://www.piliaemmanuele.wordpress.com)
1.13.11 / 11pm


An italian “instant cities” in L'Aquila, by the “utopian” italian government…


before the heartquare: <http://upload.wikimedia.org/wikipedia/commons/4/48/L'Aquila_centro.JPG>


Later: <http://www.laquilanuova.org/wp-content/uploads/2010/05/case.jpg>





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.13.11 / 11pm
	
	
	Emmanuele: Your idea of an instant city differs radically from the ones I refer to in the post, no?
	
	
	
	
	
		1. [Emmanuele](http://www.piliaemmanuele.wordpress.com)
		1.13.11 / 11pm
		
		
		Off course, it was just a bit of macabre irony: our (sic) Premier calls himself a “utopian”. I'm sorry if I was inappropriate (thinking about it, I have not been “delicate”), but I thought it was “relevant”: the inhabitants of L'Aquila have lived and still live in tents…
7. Pedro Esteban
1.14.11 / 5pm


Do you speak of instant cities in extreme situations? Now I was working on a project with ruins, all in Habana Vieja.  

La Habana is an instant city, and it isn't marked for any external factor. And there too exists a video game shop, who has the same function of that in Haiti, has more conditions in deed.  

La Habana is my Instante City oppost that of Archigram. La Habana denies tecnology, but develop a full degree of cultural process, maybe Archigram never think on what can happend with culture in a non technological context. Or it's better study the behaviour of culture in differents technological contexts.  

What would be the approach of Haiti to his culture in this moment? The cult to the economy? Here we only see the pictures of a photograph, it's better go there to see the social process and after we will know.
9. [Zoran](http://www.bifstudio.com)
1.15.11 / 8am


touching the deepest core of archicecture  

and civic activity as a human beeing  

Thanks for opening eyes that where asleep  

for some 50 years.
11. [Urban Change – Projetos Urbanos » Blog Archive » Instant Port-au-Prince](http://www.projetosurbanos.com.br/2011/01/19/instant-port-au-prince/)
1.19.11 / 12pm


[…] usual, Lebbeus Woods' weblog is featuring stunning topics: the sudden rise of informal settlements in Haïti's capital […]
13. Ben
1.27.11 / 5am
