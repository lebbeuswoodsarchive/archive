
---
title: THE DREAMS THAT STUFF IS MADE OF
date: 2011-01-04 00:00:00 
tags: 
    - architecture
    - drawing
    - spatial_design
---

# THE DREAMS THAT STUFF IS MADE OF


A while ago, Thom Mayne and I were talking about our recent projects, some of which have nothing explicitly to do with buildings. This is the exception for Thom but the norm for me these days, and at least part of our discussion was about why we would want to make work that looked to most people more like art than architecture. Interestingly, we both claimed the projects in question—Thom's [series of large physical models](https://lebbeuswoods.wordpress.com/2010/11/25/thom-maynes-mind/) and my series of large drawings—addressed spatial and tectonic issues most relevant to the design of habitable space. I suspect that we will have a hard time convincing our more skeptical colleagues about this—so be it. Experimentation and research don't aim for immediate acceptance, but rather at opening new ways of thinking and working, for their authors as well as anyone who might creatively interpret the outcome, which is the only justification for their publication.


[![](media/lwb-confsp-121.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-121.jpg)


*(above) A corner of LW's large-drawing room, cleaned up to display the drawings.*


To regular readers I say, don't worry, I'm not going to trot out the arguments for experimentation that I've made in many posts and indeed quite [recently](https://lebbeuswoods.wordpress.com/2010/08/12/the-experimental/). Instead, I'd like to address some specifics of the *Conflict Space* series of drawings, shown here. This is partly in answer to recent requests by readers for me to discuss how I draw. While I remain skeptical of putting too much emphasis on drawing, for fear of  distracting from its content, the way a drawing is constructed is, as in a building, part of its content.


[![](media/lwb-confsp-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-11.jpg)


The impulse to make these large drawings—they are, with one exception, 74 inches high by 120 inches wide (188 by 305 centimeters)—came first from my desire to make drawings at the scale of a room, that is, at an architectural scale. The reason for this is rather simple: to see if one could physically and not only mentally inhabit the space of a drawing. The second driving force was to see if drawing at that scale would produce something different than I'd imagined or drawn before.


[![](media/lwb-confsp-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-21.jpg)


Regarding the question of scale, I would say that the drawings in this series, experienced in the flesh, do invite our physical participation in the drawn spaces. This can be attributed to their size, but also to the fact that their presence is quite tactile, that is, one sees that the drawings' surfaces, lines, and textures are made by a hand on actual, not simulated or virtual, material. The same effect could not be achieved through a blow-up of a smaller drawing. Secondly, drawing at this larger scale did inspire a different way of making marks and thus a type of space I had not drawn before, one emerging from the subtle variety and scale of marks only in drawings large enough to permit such a range, from the light to he heavy, the thick to the thin. Of course, viewing them on a computer screen negates these very qualities. This is similar to the experience of seeing an actual building and then photos of it. Following this thought leads us in the direction of considering representation as reality, which is not where I want to go in this post. Rather, I want to stay focused on the drawings and how they came to be what they are.


[![](media/lwb-confsp-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-3.jpg)


At the outset, I decided to work within strict, clearly defined limits. Even more, I wanted to restrict myself severely, in order to see what could be accomplished with a minimum of means. I won't deny that this was in part a reaction to the surfeit of means I had employed in the past—tone, color, shading, accomplished through a variety of media, graphite and color pencils, pastels, ink—not to mention the rendering capabilities of digital computers, which enable a truly infinite range of possibilities. I wanted to put all that aside, to free myself of it, and also to free architecture—always the subject of my drawings—of an excess that increasingly seems to me to be suffocating its true spirit.


[![](media/cs-1-det11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/cs-1-det11.jpg)


Architecture does begin with the plow, the brick, and acts of making. In different epochs, their uses are inspired by different ideals, and in our own, the plow, the brick, and the making must serve our complexity and diversity, the thousand subtle variations on a thousand human themes, even as they remain plow, brick and making. Architecture has become the most difficult and daunting of arts.


[![](media/lwb-confsp-5-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-5-2.jpg)


Such were the thoughts in mind as I prepared to draw. I chose as a ground a fine linen canvas that I prepared with black (actually very dark grey) acrylic paint. The result was a matte surface, perfect for receiving any drawing medium. For mine I chose an artists' quality, water-based white crayon, because of its opacity and also its ability to be manually sharpened to a fine point by a hand-held sharpener. I would make white marks on a black field. The reason was, I admit, counterintuitive. Our cultural habits prejudice black lines on a white field as being the most evocative of space. White lines on a dark field are abstract, even flat and two-dimensional. Regardless, I reasoned that the drawing of a line is in itself positive, an assertion of edge and boundary that will overcome spatial ‘reading' habits—my first big gamble.


[![](media/cs-5-det11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/cs-5-det11.jpg)


The second gamble was that I would use only straight lines drawn with a specially made straightedge that enabled me to hold it firmly against the vertical surface of the primed canvas. The straight line is a spatial vector, conveying direction and magnitude of energy, not just symbolically, but also in the physical, intellectual, and emotional energy it takes to draw the line exactly as it is. Moreover, any curve can be created with straight lines, the smoothest with a mathematically infinite number. With straight lines, the boundaries of any form can be established.


[![](media/lwb-confsp-6-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/lwb-confsp-6-2.jpg)


Using this approach, the drawings aim to evoke in two dimensions three-dimensional figures that overlap and interpenetrate one another in spaces at once ambiguous and precise. In one sense, this echoes the goal of early Modernism to achieve spatial continuity and social universality, but differs radically from it in that here a universal visual language of vectors yields the idiosyncratic and discontinuous. This is important to an era such as the present one in which individuals are locked in a decisive struggle for their identities with the leveling forces of commercialized mass culture. Modernism, in its day, sought to reinforce a then-emerging demand for social justice, challenging a rigid class system by the establishment of an egalitarian environment. Today, the ‘egalitarian environment' of consumerism does little to support social justice in any form. These drawings aspire to finding, in spatial terms, what I have [elsewhere](https://lebbeuswoods.wordpress.com/2009/01/13/same-difference/) called ‘the differences in radical similarities.' Too grandiose? No doubt. But such are the dreams that provide the energy to drive such projects.


LW


ps. Thom Mayne and I have agreed to carry forward our discussion of his exploratory models. Keep an eye out for future posts….


#architecture #drawing #spatial_design
 ## Comments 
1. [Tweets that mention THE DREAMS THAT STUFF IS MADE OF « LEBBEUS WOODS -- Topsy.com](http://topsy.com/lebbeuswoods.wordpress.com/2011/01/04/the-dreams-that-stuff-is-made-of/?utm_source=pingback&utm_campaign=L2)
1.4.11 / 3am


[…] This post was mentioned on Twitter by bldgblog, KpaxsArchi. KpaxsArchi said: THE DREAMS THAT STUFF IS MADE OF: A while ago, Thom Mayne and I were talking about our recent projects, some of … <http://bit.ly/gEUFGl> […]
3. [Guy](http://www.glad.ch)
1.4.11 / 5pm


dear labbeus 


it is great to start the year with an insight into your big scale moves. i wish you a creative and healthy year to come, to bad i was catched in the storm in new york dec 26th, we had to stay arround uptown by feet. best wishes  

guy





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.5.11 / 1am
	
	
	Guy: Sorry we missed each other. I'm pleased that you are keeping up with the posts. We need one from you on RIEA's activities at present, or perhaps an overview, or a position statement at this oddly quiet moment in our field. Any ideas?
5. [chris teeter](http://www.metamechanics.com)
1.5.11 / 12am


Nice post. If you didn't give all that explanation about technique and material and why you did what you did I'd probably not spend more than 5 seconds looking at them, but now I am looking for something. Maybe its just me but when the means and methods behind something that you as the viewer are supposed to find meaning in is explained I think the meaning is better explained and we get to be lebbeus woods ontologically for a few split seconds. Now I hate the study of process in architecture. So to reconcile the two arguments here based on the notion of experimentation mentioned above –


The act of technique on matter to create a virtual vision out of physical materials, the confrontation between idea and matter, this is the experimental for the creator and sometimes the result appears experimental to all. The moments of idea meeting matter are interesting in and of themselves, they can be self-referential, no reason for further explanation.


Studying the process, the moves made and not made in an abstract nature, a nature in which references upon references to historical and ideological contexts are required to justify the reasons decisions were made, a process that is not directly tied to execution, one that relies on education outside the object being created; this process and the study thereof is futile and reveals merely the critic's or observers of own virtual biases and logic. These biasis and logic could be a result of experience with technique on matter but could also be a result of educational persuasions.


I think academia spends far too much time on the latter, I don't really think architecture needs to be that intellectual.


Thanks lebbeus, I just worked something out.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.5.11 / 1am
	
	
	chris: You've been one of the very thoughtful readers of this blog for some time, so I'm glad this post was useful to your thinking through some things important to you.
7. [namhenderson](http://www.namhenderson.wordpress.com)
1.5.11 / 2am


wonderful post. and stark but beautiful images. I think you succeed in this perfectly.  

*the drawings aim to evoke in two dimensions three-dimensional figures*
9. [chris teeter](http://www.metamechanics.com)
1.5.11 / 12pm


Your blog is very thought provoking, a good kind of intellectual exercise and you still tolerate my posts. (I find no time to write essays so I take posting as an opportunity to sort out potential essay bits, and frankly your blog is the only one that requires a challenge to formulate a post) thank you for all the great posts.
11. [Stephen](http://skorbichartist.blogspot.com/)
1.5.11 / 4pm


I like the title. I love the drawings.  

I really get the sense of space and depth of field from the photos alone, but I wish that I could see them in person.  

I am trying to imagine you actually drawing these by hand and what a physically challenging exercise it may have been.  

Sure the images could have been created by computer, but the added sense of the lines being draw in by hand is fantastic. An old school technique but very forward looking.  

Keep drawing!
13. [Guy](http://www.glad.ch)
1.5.11 / 5pm


dear lebbeus thank you for the answer  

i will send you a proposal by mail the next days  

cheers  

guy
15. [all but… « CUPtopia](http://cup2013.wordpress.com/2011/01/05/all-but/)
1.5.11 / 6pm


[…] THE DREAMS THAT STUFF IS MADE OF […]
17. [john cantwell dot net / Lebbeus Woods Draws Big](http://www.johncantwell.net/?p=46)
1.8.11 / 11pm


[…] Woods explains. […]
19. [Margot krasojevic](http://Margotkrasojevic.wordpress.com)
1.9.11 / 10pm


Beautiful drawings that separate the anaesthetised computer scriptwriter renders from the artist, designer and architect, something that has sadly been misplaced because of our obsession with cartel technology and reproduction. Truly stunning!
21. [Topicco – Blogs » blog wunderlust : 10 January 2011](http://blogs.topicco.com/2011/01/10/blog-wunderlust-10-january-2011/)
1.10.11 / 5pm


[…] | architecture at a crossroads | dress woven from rubber bands | how do we materialise peace? | dreams The blog wunderlust is a weekly round up summarizing the architectural highlights, news and web […]
23. [blog wunderlust : 10 January 2011 « Modern Building « Architectural Design Concept](http://www.nachiadrs.com/modern-building/blog-wunderlust-10-january-2011.html)
1.10.11 / 11pm


[…] | architecture at a crossroads | dress woven from rubber bands | how do we materialise peace? | dreams The blog wunderlust is a weekly round up summarizing the architectural highlights, news and web […]
25. [lines and stripes « patterns and other pretty things](http://patternsand.wordpress.com/2011/01/29/lines-and-stripes/)
1.29.11 / 6pm


[…] Image of Lebbeus Woods's drawing room, courtesy of Lebbeus Woods […]
27. dana
3.22.11 / 8am


Gorgeous! 


your very, very first time at such large scale? 


When I draw standing up, I like to achieve that feeling myself. Your writing is so advanced. 


All I have to say is wow, and thank you.


I can't wait to catch up on your reading. Intox.


LW
29. Gio
4.16.11 / 3pm


Really impressive work!  

To say that is beautiful is beyond words, because really is.


The concept of scale did open the idea of inhabiting these spaces. Zooming in to really see.  

The care and precision of each drawn line invites me to these locations.


In your large drawn spaces, the solitud of these locations, is in a constant dialogue to emerge, to appear, to manifest itself in the void. One can see how small particles of these dialogues become appareant in the lines through time and process, like old paths, roads and trayectories.


I would like to know, why just lines and not curves.  

 ( I guess because I find myself doing curves)  

I know that a curve is an extended line in infinite space but the locality of your limits ( 74″ x 120″ ) can allow for that.  

I am going right now through the investigation of how tectonic space is manifested, created, formed. You really pointed out a way through your work as well as all the posts of Raimund Abraham and Neil Spiller. Thank you.


I can definitely see your project in China here.  

Gio





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	4.16.11 / 8pm
	
	
	Gio: I very much enjoy the subtle play of your language in description. It is an appropriate counterpart to the drawings.  
	
	Why straight lines? Several answers there. I turned to straight lines ten or twelve years ago as the ‘purest' boundaries defining spaces. Curves, as you note, can be understood as being made of an infinite number of straight lines. In some of my drawings (not the Conflict Space series) curves emerge from aggregations of straight lines. Finally, the straight line is the most practical to construct, either in crayon or steel.  
	
	I'm very glad you've derived something of value for your work from the works of Abraham and Spiller.  
	
	Do stay in touch!
31. [Charles Roche](http://www.facebook.com/profile.php?id=1661934478)
1.2.12 / 9pm


Awesome art work with a very peculiar intention, Please Read, this is  

very interesting and AMAZING!
33. zale
1.4.12 / 5pm


I guess I would like to comment more on this post. Only because I received this in e-mail. Does this mean that I have signed up to receive e-mail alerts on a particular discussion? Pretty Cool. I really enjoyed reading Gio post and agree that the comment was pretty awesome. It really makes you wonder if every comment should be the reaction of immediate discourse on an intellectual level; after all, you are reading LW. He's only constantly reminding us that.. When I come to your blog, I am always prepared for some sentence that blows my train of thought. A conversation with the viewer that puts me there. But if I spent all day with such comments, I wouldn't have time dreaming. Of course dreaming about working for you, kind of goes both ways. I think that graffiti, whether you like it or not, plays a strong role in the communication and interaction of architectural scale. I couldn't really explore my intention, but my ideas on signs hold a similar value. What gives me the darkest pleasure, is that everyone has this voice. Loud or not. Some people like it, others don't. Your invitation to the world through your eyes is what led me to Ai Weiwei sculpture at LACMA. The same voice that isn't afraid to speak. And as an artist… Keep singing and dancing, on and off paper. Paper Architect? Misuse of the phrase, I'd say. But, I do wonder if you made the switch.
