
---
title: 41 COOPER SQUARE
date: 2011-01-06 00:00:00 
tags: 
    - architecture
    - criticism
    - Thom_Mayne
---

# 41 COOPER SQUARE


I came across this article on [another website](http://www.architakes.com/?p=4140) recently and found it to be quite a good piece of criticism—well-researched, thoughtfully argued, and willing to look at all opinions about the [recent building](http://morphopedia.com/projects/cooper-union) by Thom Mayne and Morphosis in New York City. The main theme of the article is the relationship of science-fiction imagery to the design of the building, with a sub-theme of its other design influences. I hesitated to post it here because my work is prominently mentioned, but, as you will see, the article looks critically at it, too, and shows its influences. For my part—and I suspect Thom Mayne's, as well—I dislike the science-fiction reference. Still, I've gotten used to it over the years and by now see its usefulness as a critical trope in the hands of a serious critic, such as the author of this article. What is most obviously missing from the article is the more important impact of Mayne's own experimentation and research, carried on steadily in his practice for the past nearly forty years, but that is beyond its intended focus.


LW


[![](media/mayne-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-11.jpg)


[![](media/mayne-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-2.jpg)


[![](media/mayne-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-3.jpg)


[![](media/mayne-4.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-4.jpg)


[![](media/mayne-5.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-5.jpg)


[![](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-6.jpg?w=600&h=635 "MAYNE-6")](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-6.jpg)


[![](media/mayne-20.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-20.jpg)


[![](media/mayne-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-7.jpg)


[![](media/mayne-8.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-8.jpg)


[![](media/mayne-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-9.jpg)


[![](media/mayne-10.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-10.jpg)


[![](media/mayne-12.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-12.jpg)


[![](media/mayne-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-13.jpg)


[![](media/mayne-14.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-14.jpg)


[![](media/mayne-15.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-15.jpg)


[![](media/mayne-16.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-16.jpg)


## **David Holowka is the author of the article above.**


[![](media/mayne-17.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-17.jpg)


[![](media/mayne-18.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-18.jpg)


[![](media/mayne-19.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/mayne-19.jpg)



#architecture #criticism #Thom_Mayne
 ## Comments 
1. [Stephen](http://skorbichartist.blogspot.com/)
1.6.11 / 3pm


Thanks for sharing this. I think that for the very reason that Mayne' architecture ruffles feathers makes it successful and incredibly relevant.  

As for the science fiction analogy, I think people have a tough time talking about architecture that pushes the boundaries of what we know as a society. We then tend to put such things into categories like “fantasy” or “science fiction”, but I don't think that makes the ideas any less valid.  

Lebbeus, I first became enamored with your work back in grad school and was even inspired to include it in a research piece on visionary cities. I included the work of architects, artists and film makers who I felt have had great influences on how we see the city. I reached some similar conclusions, or assumptions, as Mr/Ms “anonymous” has come to above, with the exception that I think there is much more to be gained from these explorations.  

I feel like all of this is great discourse and is all relevant to how we live and how we design for living.
3. [MM Jones](http://www.bauzeitgeist.blogspot.com)
1.8.11 / 11pm


“An image-starved world” —Wow, we are a long way from Banham's 1965, I guess.
5. [Patricio De Stefani](http://www.facebook.com/pages/Juan-Borchers/47782617043)
1.10.11 / 1am


I agree Joe W. and I think that it is interesting to look at the relations of this kind of architecture and the so-called ‘postmodern theory' e.g. baudirllard, deleuze, lyotard and so on. One of the questions that I always kept in mind is about postmodern politics and its expression in architecture, a complicated matter.
7. Chenoe Hart
1.11.11 / 1pm


Ever since this building was completed, I have always wanted to like it. Every time I attempt to appreciate it, however, my thoughts always return to a concern which,  

although seemingly minor, ultimately undermines my entire perception of the building. In particular, how can this building hold “civic value,” let alone be “rebellious” in  

its agenda, when it is not even open to the public? True, its exterior does makes a noticeable statement in the urban realm. But its complete architectural experience (exterior and interior, which I believe are architecturally an inseparable whole) is only available to what is, by virtue of Cooper Union's very low applicant acceptance rate, an astronomically exclusive elite. When I walk past the building as a denizen of the public realm, this policy makes me feel as though the prominence of the facade's urban presence only rubs in the fact that I am unwelcome to it. The management policy skews my impression of the architecture. Ultimately I am left with the feeling that the building, for all of its formal invention, is in practice  

reinforcing the most ancient and traditional of power relationships. If this is the stuff of science fiction, then its story must be of the dystopic nature.


Lebbeus, I have been greatly inspired by your previous calls for a more civic and socially responsible architecture, as discussed in writings such as your Bad Faith article series. The Cooper Union building makes me uncomfortable precisely because I feel that in practice, at its present state, it undermines those values. How do you reconcile your admiration for this building's ideas with the reality of its actual operation?
9. quadruspenseroso
1.11.11 / 2pm


An ambitious professor witnesses a gun-brandishing whore shooting at a California crescent moon. In time, he teaches her sons and daughters impressionable rhetoric of kinked metallic metaphors so shiny and new. “Look who has the weapon now stud[ents]”. Furiously they draw, miming her twisted pain, magically building clever scallops and curves; yes America, business as usual.


A pregnant prostitute is firing a pistol at the full moon. Her destitute grandfather persuades the rusty district to turn off artificial lighting entirely. With newly darkened streets, the hulking power grid is hardly missed, reddened by street side fires, the facades appear less plain, on faintly moist lawns we casually sat and chatted, shadow free. Who's bad and who's good? Without fear, she is cleansed and no longer open for business.
11. [OLGV.Diary » Morphosis and my mold.](http://diary.olgv.ro/?p=105)
1.15.11 / 4pm


[…] already feel the mold growing inside my lungs. But the good thing is that Thom Mayne's “41 Cooper Square” building got the Pritzker Prize… is it good ? by OLGV. Architecture, News, 41 cooper […]
13. [ArchiTakes | Architecture Meets Science Fiction at 41 Cooper Square](http://www.architakes.com/?p=4140)
1.18.11 / 3am


[…] Update:  Lebbeus Woods has commented on this post in his own blog:  <https://lebbeuswoods.wordpress.com/2011/01/06/41-cooper-square/> […]
15. [https://lebbeuswoods.wordpress.com/2011/01/06/41-cooper-square/ « Atelier 10 Greenwich](http://atelier10.wordpress.com/2011/01/20/httplebbeuswoods-wordpress-com2011010641-cooper-square/)
1.20.11 / 12pm


[…] <https://lebbeuswoods.wordpress.com/2011/01/06/41-cooper-square/> […]
17. johnkariyannis
3.16.11 / 1pm


what about the church behind it, that it seems to have completely ignored? Mr. Woods Im a big fan of yours, i wonder why you or the cooper faculty did not find the space yourselves, an urban set maybe, in constant transformation. I know this is not possible, I would only mention this to cooper union, who I would expect pure drama! A dialog with “Peter Cooper” across the street. Who turns away from the building now, if I remember correctly. why thom maine? I enjoy his work don't get me wrong, but this is cooper union for gods sake, Maybe an urban portrait of john Heyduk would have been more appropriate than this building. Hey Mr. Woods, I did not have the pleasure of meeting Prof. Heyduk, would he have approved? When i see this building I think just that, “building” not necessarily “space”.
