
---
title: FOUR IDEAL HOUSES  First Year Studio
date: 2011-01-26 00:00:00 
tags: 
    - architectural_education
    - architecture
    - ideal_houses
---

# FOUR IDEAL HOUSES: First Year Studio


**THE COOPER UNION School of Architecture, ARCH111-S11—ARCHITECTONICS; Professors Lebbeus Woods, Anthony Titus, Uri Wegman, Aida Miron; Professor Anthony Vidler, Dean. Semester PROGRAM:**


## **FOUR IDEAL HOUSES**


**Project**


This semester we will focus on the design of **four Houses**, which we term ‘ideal' because each occupies a different elemental volume—*cube, cylinder, cone,*or *pyramid*—and each embodies a program of habitation based on a different natural element—*earth, air, fire,* or *water.* Furthermore, the **inhabitants** of each House are assumed to be ‘ideal,' in the sense that they embody, for our purposes, only certain universal human characteristics, such as physical size, capabilities of movement, perception of their environment, and interaction with it. The **site** of each of the four Houses will also be ideal, meaning sloped or flat, horizontal or vertical, and will disregard any inherently idiosyncratic features. In the design of each House, equal emphasis will be placed on the**interior and exterior** of its volume. In taking this approach, we realize that these ideal types exist only as ideas, yet find these ideas useful in the laboratory of the design studio as a means of **understanding the fundamental tectonic elements of architecture.**


**Historical background**


There is considerable historical precedent for our project. We find ideal architecture—of exactly the sort we are engaging—in examples from Vitruvius, through Alberti and Da Vinci, Ledoux, Semper, Taut and Le Corbusier, Archigram, up to the present in ideal projects by Hadid and Holl. These and other architects have found it important to define their personal principles of design, as well as to set a standard against which to measure their more reality-based work. Ideal architecture has been essential to defining building typologies, which serve the purpose of bringing a level of order to the diversity of urban landscapes. Each student is encouraged to look up and into historical examples, the better to understand the broader context of our work this semester.


*(below) Ideal House by Claude-Nicolas LEDOUX, c. 1770. Bi-lateral symmetry is a fundamental  way of giving clear order to forms and spaces, and thus is a classical characteristic of ideal architecture:*


[![](media/4h-1.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-1.jpg)


*Glass House by Bruno TAUT, c. 1914. Taut and his compatriots in the ‘Glass Chain' were convinced that light and transparency were the main features of an ideal architecture of the future. This project was an early attempt to give form to this ideal:*


[![](media/4h-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-2.jpg)


*Dom-Ino House by LE CORBUSIER, c. 1914. His way of representing the ideal emphasizes the reinforced concrete slab-and-column structure (a then-innovative construction method), and makes whatever skin that might wrap it irrelevant:*


[![](media/4h-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-3.jpg)


*Brick Country House by **MIES** van der Rohe, c. 1914. His ideal architecture was an asymmetrical composition of free-standing vertical and horizontal planes, creating an overlapping, interpenetrating continuum of space:*


*[![](media/4h-41.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-41.jpg)*


*Dymaxion House by R. Buckminster **FULLER,** c. 1925. A free-standing open living space suspended above the ground on a central mast, Fuller's ideal was symmetrical, technologically determined, and mass-produced, a prototype for a better,  future architecture:*


[![](media/4h-6.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-6.jpg)


*House of the Suicide by John **HEJDUK**, c. 1985. The conceptual opposite of Fuller's ideal, Hedjuk's is based on deeply personal feelings and experiences, yet manifest with classical symmetry and the precise geometry of  the fully rational, which accounts for its uncanny aesthetic power:*


[![](media/4h-7.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-7.jpg)


*Ideal House by Raimund **ABRAHAM,** c. 1985. His ideal is a dialectical combination of symmetry and asymmetry; classical geometry and organic formation; spaces created by building on the earth and digging into it—positive and negative forms—creating an architecture that is both philosophical and sensuous:*


[![](media/4h-81.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-81.jpg)


*A ‘house of God' (Church on the Berlin Wall ) by Raimund ABRAHAM, c. 1988:*


[![](media/4h-9.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-9.jpg)


*Bridge of Houses by Steven HOLL, c. 1980. There is no single ideal form, but rather an ideal sets of variations on a simple theme of his own invention, growing in complexity as it is developed, like J. S. Bach's ‘Art of the Fugue':*


*[![](media/4h-104.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-104.jpg)*


*High Houses by Lebbeus WOODS, c. 1993. Ideally, the two houses form the seed of a community of individuals who want to be free of any particular site, yet remain connected to one in the war-damaged city of Sarajevo, Bosnia. Perched atop sets of steel beams which are bent like catapults by cables anchored in the ground, the houses present the opportunity for an ideal, experimental way of living:*


[![](media/4h-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-11.jpg)


**Method**


We will arrive at the designs of the Four Ideal Houses by a series of steps or **stages**, working both individually and in four teams, one for each House. As the design of each House progresses, it will evolve from the ideal forms of its beginnings to the particular forms of its development and conclusion. If we assume, for example, that the House of earth has the form of a cube, we can expect that its ‘earth-like' material stability will resist any changes made to the volume; yet, human inhabitation requires changes, for example in the need for openings for going in and out of the cube, and letting in light and air. Consequently, these openings will be determined by the rules inherent in material stability, say, the regularity of its cubic geometry. This transformation will, in itself, be considered a next higher level of the ideal, in that it embodies a fundamental aspect—a continual evolution in time— of both the human and natural worlds.


*Capital buildings at Dacca, East Pakistan by Louis I. KAHN, c. 1964. Kahn practiced a theory based on the concepts of ‘Form' and ‘Design.' Form is pure geometry, say an unadorned cylinder, while Design is the transformation of that pure volume because of programmatic necessity, producing asymmetrical interventions. The ‘compromised' cylinder is the ideal, human form, imbued with both universality and individuality.*


*[![](media/4h-121.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-121.jpg)*


**Stage One** will be **studies** of the four elements—earth, air, fire, water—and the four elemental volumes—cube, cylinder, cone, pyramid. This study focuses on identifying the essential (universal) characteristics of each. It is accomplished by dividing the class into two reams, one concerned with elements, one concerned with volumes. These teams are further divided into four teams, one for each element or volume. These studies will be presented to the whole class in a pin-up review (2/1). The conclusion of this stage will be the student's **selection** of one of the four elements for each of the four volumes. These choices will be presented in a pin-up review (2/8).


***Required:*** *Documentation of research and speculations in the media judged most appropriate by each student: drawings, models, written texts, photography, videos, and other media, as well as any combination of these. It is necessary that the chosen media be readily presentable in the Review.*


*A montage of sample documentations: geometric volume studies and visual explorations of elemental phenomena:*


*[![](media/4h-13.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-13.jpg)*


*A visual exploration: ice cube and city and the interplay of refracted and reflected light:*


*[![](media/4h-13a.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-13a.jpg)*


**Stage Two** begins with the class being divided into **four teams**, one for each volume, now with the program of its selected element, which we will refer to as House. These teams will work together for the rest of the semester.


Each team will conduct **a design competition**, in which each team member will propose a design scheme for their House. These will be presented in two pin-up reviews (2/15, 2/17). After each review a single scheme will be selected for each house, as the basis for the teams' work this semester.


**Required:** Sketches and more developed drawings in plan, section, and elevation, supplemented by axonometric and perspective drawings; conceptual and sketch models in any material; photographs; conceptual and explanatory texts. It is necessary that the chosen media be readily presentable in the Review.


*Light Pavilion by Lebbeus WOODS, for Raffles City, Chengdu, China, c. 2009. At this early stage, the drawings are freehand, the model assembled as all quick, intensive study models are, with an emphasis on ideas and their exploration, not finished appearances:*


*[![](media/4h-142.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-142.jpg)*


**Stage Three** will be the development of a **schematic design** for each House by its respective team. The distribution of tasks within each team will be discussed with the faculty at the time this stage begins. The schematic designs for the four Houses will be presented in a Mid-term Review (3/8).


**Required:** Precise drawings in plan, section, and elevation, supplemented by axonometric and perspective drawings. Fully developed scale model of the House; photographs of the model. Text.  More detailed requirements for the drawings and model and text will be given.


**Light Pavilion.* As the design is developed, forms and spaces not only change, but they also become more precisely defined and constructed in the drawings and models:*



*[![](media/4h-153.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-153.jpg)*



**Stage Four** will be the development of a **final design** for each House by its respective team. The final designs will be presented in a Final Review (c.5/10).


***Required:*** *Final drawings in plan, section, and elevation, supplemented by axonometric and perspective drawings. Final scale model of the House; photographs of the model. Text. More detailed requirements for the drawings and model and text will be given.*


*Solohouse by Lebbeus WOODS, c. 1988. Of special interest in this example of a Stage Four design is the model. It is six feet (72 inches, 72 inches, 183 centimeters), large enough to be built showing all essential details, and in materials the same as or similar to those intended for the full-sale house—in this case sheet steel and wood:*


*[![](media/4h-16.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-16.jpg)*


*Solohouse final design drawings and model detail:*


*[![](media/4h-171.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-171.jpg)*



*Solohouse, interior detail and use of illumination:*


*[![](media/4h-181.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-181.jpg)*



**Goals**


**Collaboration and teamwork**


Each of us will approach this project with our own aspirations, our own ideals of architecture. It is crucial that, even when we work in a tightly knit team, we keep our own personal ideals and goals in mind. Teamwork is at its best when individuals who are clear about what they want to achieve collaborate. The key to their successful cooperation is for them to emphasize what they have in common, not their differences [of course, if they have no important ideas in common, they should not be on the same team]. In that way, collaboration is never a compromise of what each believes, but rather a reinforcement of the most important aspects of it by the similar ideas of other team members.


*The Sphere by STUDENTS of a Fifth Year Thesis Design Studio, 2002. This project, which incorporates the research of seven teams in the studio class on aspects of a New York Subway ride, was created in a single semester. It began with a pure sphere, which evolved into a highly articulated model (upper left, below) and to the final construction, with the seven teams working in coordination, with guidance from the faculty:*


[![](media/4h-19.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-19.jpg)


*The Sphere. An intersection of the projects of several teams. The interplay of differing oncepts, designs and materials—far beyond the scope or capacity of a single person—makes the whole project ‘urban,' even as it retains the imprint of distinct individuals. This is truly ideal:*


[![](media/4h-20.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-20.jpg)


*The Sphere, completed:*


[![](media/4h-27.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-27.jpg)


When working on our projects, we should keep in mind that the making of architecture is always a team effort. At the same time, we should recall that successful teamwork bringing together the work of strong individuals, requires **leadership**. It is never a committee effort, with decisions made by voting. Instead, at each stage of the design work and, later, the work of building, a leader must guide the collaborative effort; it may well be that each stage a team's work could have a different leader. This leader usually emerges quite naturally, as he or she is the one who has the best idea about how to accomplish a particular stage of the work. It is rare that all members of the team will not recognize the best idea and agree on a leader for the stage of work it addresses**.**This sounds complicated but it will not be if all members of a team communicate with one another in an open-minded, relaxed and honest way. The best way to preserve one's ego is, strangely enough, to put it aside, be un-defensive, and let the best ideas emerge by their inherent strength. **Achieving successful design collaborations is one of our goals this semester.**


*Part of The Sphere teams. The camaraderie created by collaboration on a difficult and challenging project is unique and enduring. The feeling of personal achievement is amplified by shared experiences and the knowledge of  a job well done, together.*


**[![](media/4h-21.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-21.jpg)**


**Human scale**


We will emphasize in our work this semester the attainment of human scale for our projects. ‘Human scale' is a term used freely by architects, but often with very little understanding of what it means or how it can be achieved in their designs. All too often, they indicate human scale in their drawings by drawing human figures in or next to their buildings, or, in models, little figures and model cars. These are actually very poor techniques because they do not attain human scale in the architecture, but merely indicate it graphically. Human scale in even uninhabited architecture is attained in two basic ways:


1) **the presence of tectonic elements required by human use**—stairs, windows, doors, and other elements that facilitate human use of spaces. Their size in proportion to a building's overall form, and their relationship to each other do not merely indicate the relative size of people, but are necessary for people to inhabit the building and are therefore integral with it. Most of these elements are not arbitrarily sized, but confirm to the dimensional limits of the human body and its capacities.


*Solohouse. Looking through a transparent wall, the relative size of a person—hence the human scale—is immediately understood by the sizes of the ladder/stair, the door, and strutural, tectonic elements.*


[![](media/4h-22.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-22.jpg)


2) **the presence of tectonic elements used to construct a building**—its walls, ceilings, floors, and other elements defining and articulating spaces. Buildings are not made of a single, solid material (*contra* CNC milling machines), but are constructed of many parts and pieces put together by human beings, and the pieces are sized accordingly. Whether they are assembled directly by hand—bricks or wood panels—or by the hand operation of construction equipment—steel beams, pre-cast concrete slabs—when these parts are visible in the completed building, they immediately establish the relative size of a person (the builder) and, thus, human scale. [Note that even the most seemingly monolithic of materials—reinforced concrete—is poured in parts and thus bears the marks of form panels and construction joints.] **Achieving human scale in our projects is one of our main goals this semester.**


*Solohouse. The weathered steel panels of the enclosure walls reveal the material's surface texture; the panel sizes indicate the human-scaled limits on the construction:*


**[![](media/4h-23.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-23.jpg)**



**Ideal Houses**


The conception and design of ideal houses realizes the highest hopes of their designers, giving form and structure to their aspirations for themselves, architecture and through its place in the broader scheme of things, the many people engaged by it. The truth is that ideal architecture in the sense that we speak of it here can be constructed in the real world and with real materials—indeed, it must be constructed. The final drawings and models of the four Houses will—if made with intelligence, passion, and courage—**achieve the reality of ideals. This is our most important goal.**


*Musicians' House (Musikerhaus) by Raimund ABRAHAM, Hombroich, Germany, c. 2008-11. Four musicians will live and work here, joining together in the central space for an ensemble performance several times a year. *This work is a realization of ideals of architectural design integrated with a way of life. The ‘program' and ‘the form' cannot be separated from one another:**


**[![](media/4h-242.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-242.jpg)**



*Musicians' House:*


**[![](media/4h-25.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-25.jpg)**


*Musicians' House:*


**[![](media/4h-261.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/4h-261.jpg)**



**[January 20, 2011]**



**12 Comments as of January 25:**



1. anthony Vidler  1.22.11 / 11pm
.  

 

BRAVO!
2. […] This post was mentioned on Twitter by MyarchN and Orlando Calrissian, KpaxsArchi. KpaxsArchi said: FOUR IDEAL HOUSES: First Year Studio: THE COOPER UNION School of Architecture, ARCH111-S11—ARCHITECTONICS; Profe… <http://bit.ly/fOQV7b> […]
.
3. tom  1.23.11 / 9am
.  

 

Thanks for posting.


I'm an amateur, but thinking about these makes me a possibly better one.
4. Mark Bank  1.23.11 / 3pm
.  

 

Sounds like a fantastic project. The universal nature of pure geometry, the dynamic compromise humans cause, fundamental tectonics underpinning the expression of architecture. I wonder if the first years will teach the professors who've designed such an experiment anything about the nature of architecture, or if it's all just an introduction for the students.




	1. [lebbeuswoods](http://www.lebbeuswoods.net/) 1.23.11 / 3pm
	.  
	
	 
	
	Mark Bank: In this case, the professors are very open to being taught by the students, and this is often what happens. Teaching is always a dialogue. When it becomes a monologue, it kills all creativity.
5. PE  1.23.11 / 8pm
.  

 

Done!  

Great excersice!! WOW!!! In a day the excersice has let me study: textures, tectonic, pure forms, spaces created by all this elements, and how to make a correlation between all, and too how can this result be used by humas(a differents kind of humas indeed)


WOW! I was serching for this kind of excersices, the Cooper rules, it's a fact. hahaha
6. joshua nason  1.24.11 / 12am
.  

 

What a great formative studio. As one who is also currently teaching a first year studio, I find this very interesting, informative and inspirational.


All too often introductory studios become awash with the obsession to develop only technical skills and they ignore other important skills such as thinking. No remarkable work has ever been accomplished if done detached from critical and creative thought. More times than not, great work is rooted in linking the purity of an idea to an authentic method of making. I do not want to read my own interpretation into your work, but that is what I see here and it is greatly appreciated.
7. Caleb Crawford  1.24.11 / 3pm
.  

 

Hi Lebbeus,  

Its nice to see someone else working with the archaic Aristotelian elements (although I see you also leave out aether). My grad thesis was based on them and I have been working with them on and off in the studio for the last few years as a way of recognizing the formative force of the natural world. My own ideals include the roof and gargoyle of Ronchamp, Scarpa's play with water at Querini Stampalia, Holl's play with water at Cranbrook and his play with light in Seattle. I'd love to visit the studio when I am next in town if I may.
8. Pedro Esteban  1.24.11 / 4pm
.  

 

I'm lost in the materials part.  

About materials, how can I define a material? What do you mean with earth like materials? I can do a new material or I need to adapt me to some who alredy exist?
9. Marius  1.24.11 / 5pm
.  

 

I have to admire the Cooper program so much mr. Woods. Especially the will to inhabiting the individual understanding of architecture at this early stage; preferably “un-conditioned” as far as not first being taught the more classical pedagogical principles of light and space in the most static show-and-tell kind of way. As you so eloquently put it – scale is not the graphical relationship between a human sized figure and a mass, at a certain stage of the visualization, but the intricate construction and composition of a geometry that inquires the complexity of the relationship between the human body and its surroundings.


This has been important for me to pursue, as I'm in the situation of having a bachelors degree in literature, working highly theoretical, an then moving on to studying architecture. The first school I attended had a dogmatic approach to the first year understanding of architecture, meaning getting to know specific tools for representing it. In my opinion this simply underestimates the complexity of the kind of mirroring effect architecture has upon a society – highly related to the intuitive force the individual student of architecture seemingly has, providing a narrative for an understanding of a context (in its widest sense), which has a strong continuity.


I have actually switched schools because of my previous school's lack of critical approach on architecture. I think this recent post of yours made me realize to a further extent what it is we're really doing in our search for answers. A couple of weeks ago, I went to a guest lecture by David Gersten about his literary/architectural structures which I found really interesting. Not just the fact that he was dealing with the relationships between the structure of text, and the structure of architecture, but also the way to establish a kind of fictitious logic. Concerning this, I read your post, and I wonder about the ideal. The ideal as something that could eventually lead to idealism, as the previous historic examples would suggest. Also the way you write about working together which, to me, is very inspiring, makes me wonder about the common ground of logic one establishes as a part of a group. I don't even know if it is a question, but I nevertheless think it is worth while writing oneself into this issue. I think it concerns the thought of the idealism which is also a part of exploring the understanding of a state of mind. I like the idea of ideal premises, being something that starts the logic in a sense.


Thanks
10. [chris teeter](http://www.metamechanics.com/) 1.25.11 / 2am
.  

 

For an intro, man that's intense, if the student does their due diligence in research that's 40 credit hours of schooling, easily one years worth.


I like how this is proposed as shown above, its really a clear introduction to the ontology of practice. If you are into creating meaningful matter beyond pre-conceived ideals and concepts the method of collaboration and material production described above is on point. Sure they are giving you pre-conceived ideals and concepts up there but the collab bit and the working it thru bit is what you have to jump into…just need a diving board to get you started.


Man I wish I had had studios like that…


True story by the way, I got this thing in the mail once in high school from cooper union. I had put architecture on my PSAT and attended Wash.U. Architecure discover camp, but no one in the rural mid-west had told me about cooper. So I threw whatever it was out thinking cooper union was some vo-tech school…man was I clueless.




	1. [lebbeuswoods](http://www.lebbeuswoods.net/) 1.25.11 / 3am
	.  
	
	 
	
	chris: You're right that this is pretty demanding for First Year. The students will need—and will get—a lot of guidance and encouragement in realizing their thoughts and feelings and dreams for architecture, which is why they came to this school. I'm touched by your story. I, too, am from the provincial midwest, and attended the University of Illinois. In those days, I'd never even heard about Cooper Union. But, come to think of it, Cooper Union didn't come to be what it is until long after my school days. Anyway, here we are now, not only in the great city, but in the global metropolis of the internet. Could be worse….
	
	
	.
	
	
	***See additional comments below:***



#architectural_education #architecture #ideal_houses
 ## Comments 
1. anthony Vidler  1.22.11 / 11pm
.  

 

BRAVO!
2. […] This post was mentioned on Twitter by MyarchN and Orlando Calrissian, KpaxsArchi. KpaxsArchi said: FOUR IDEAL HOUSES: First Year Studio: THE COOPER UNION School of Architecture, ARCH111-S11—ARCHITECTONICS; Profe… <http://bit.ly/fOQV7b> […]
.
3. tom  1.23.11 / 9am
.  

 

Thanks for posting.


I'm an amateur, but thinking about these makes me a possibly better one.
4. Mark Bank  1.23.11 / 3pm
.  

 

Sounds like a fantastic project. The universal nature of pure geometry, the dynamic compromise humans cause, fundamental tectonics underpinning the expression of architecture. I wonder if the first years will teach the professors who've designed such an experiment anything about the nature of architecture, or if it's all just an introduction for the students.




	1. [lebbeuswoods](http://www.lebbeuswoods.net/) 1.23.11 / 3pm
	.  
	
	 
	
	Mark Bank: In this case, the professors are very open to being taught by the students, and this is often what happens. Teaching is always a dialogue. When it becomes a monologue, it kills all creativity.
5. PE  1.23.11 / 8pm
.  

 

Done!  

Great excersice!! WOW!!! In a day the excersice has let me study: textures, tectonic, pure forms, spaces created by all this elements, and how to make a correlation between all, and too how can this result be used by humas(a differents kind of humas indeed)


WOW! I was serching for this kind of excersices, the Cooper rules, it's a fact. hahaha
6. joshua nason  1.24.11 / 12am
.  

 

What a great formative studio. As one who is also currently teaching a first year studio, I find this very interesting, informative and inspirational.


All too often introductory studios become awash with the obsession to develop only technical skills and they ignore other important skills such as thinking. No remarkable work has ever been accomplished if done detached from critical and creative thought. More times than not, great work is rooted in linking the purity of an idea to an authentic method of making. I do not want to read my own interpretation into your work, but that is what I see here and it is greatly appreciated.
7. Caleb Crawford  1.24.11 / 3pm
.  

 

Hi Lebbeus,  

Its nice to see someone else working with the archaic Aristotelian elements (although I see you also leave out aether). My grad thesis was based on them and I have been working with them on and off in the studio for the last few years as a way of recognizing the formative force of the natural world. My own ideals include the roof and gargoyle of Ronchamp, Scarpa's play with water at Querini Stampalia, Holl's play with water at Cranbrook and his play with light in Seattle. I'd love to visit the studio when I am next in town if I may.
8. Pedro Esteban  1.24.11 / 4pm
.  

 

I'm lost in the materials part.  

About materials, how can I define a material? What do you mean with earth like materials? I can do a new material or I need to adapt me to some who alredy exist?
9. Marius  1.24.11 / 5pm
.  

 

I have to admire the Cooper program so much mr. Woods. Especially the will to inhabiting the individual understanding of architecture at this early stage; preferably “un-conditioned” as far as not first being taught the more classical pedagogical principles of light and space in the most static show-and-tell kind of way. As you so eloquently put it – scale is not the graphical relationship between a human sized figure and a mass, at a certain stage of the visualization, but the intricate construction and composition of a geometry that inquires the complexity of the relationship between the human body and its surroundings.


This has been important for me to pursue, as I'm in the situation of having a bachelors degree in literature, working highly theoretical, an then moving on to studying architecture. The first school I attended had a dogmatic approach to the first year understanding of architecture, meaning getting to know specific tools for representing it. In my opinion this simply underestimates the complexity of the kind of mirroring effect architecture has upon a society – highly related to the intuitive force the individual student of architecture seemingly has, providing a narrative for an understanding of a context (in its widest sense), which has a strong continuity.


I have actually switched schools because of my previous school's lack of critical approach on architecture. I think this recent post of yours made me realize to a further extent what it is we're really doing in our search for answers. A couple of weeks ago, I went to a guest lecture by David Gersten about his literary/architectural structures which I found really interesting. Not just the fact that he was dealing with the relationships between the structure of text, and the structure of architecture, but also the way to establish a kind of fictitious logic. Concerning this, I read your post, and I wonder about the ideal. The ideal as something that could eventually lead to idealism, as the previous historic examples would suggest. Also the way you write about working together which, to me, is very inspiring, makes me wonder about the common ground of logic one establishes as a part of a group. I don't even know if it is a question, but I nevertheless think it is worth while writing oneself into this issue. I think it concerns the thought of the idealism which is also a part of exploring the understanding of a state of mind. I like the idea of ideal premises, being something that starts the logic in a sense.


Thanks
10. [chris teeter](http://www.metamechanics.com/) 1.25.11 / 2am
.  

 

For an intro, man that's intense, if the student does their due diligence in research that's 40 credit hours of schooling, easily one years worth.


I like how this is proposed as shown above, its really a clear introduction to the ontology of practice. If you are into creating meaningful matter beyond pre-conceived ideals and concepts the method of collaboration and material production described above is on point. Sure they are giving you pre-conceived ideals and concepts up there but the collab bit and the working it thru bit is what you have to jump into…just need a diving board to get you started.


Man I wish I had had studios like that…


True story by the way, I got this thing in the mail once in high school from cooper union. I had put architecture on my PSAT and attended Wash.U. Architecure discover camp, but no one in the rural mid-west had told me about cooper. So I threw whatever it was out thinking cooper union was some vo-tech school…man was I clueless.




	1. [lebbeuswoods](http://www.lebbeuswoods.net/) 1.25.11 / 3am
	.  
	
	 
	
	chris: You're right that this is pretty demanding for First Year. The students will need—and will get—a lot of guidance and encouragement in realizing their thoughts and feelings and dreams for architecture, which is why they came to this school. I'm touched by your story. I, too, am from the provincial midwest, and attended the University of Illinois. In those days, I'd never even heard about Cooper Union. But, come to think of it, Cooper Union didn't come to be what it is until long after my school days. Anyway, here we are now, not only in the great city, but in the global metropolis of the internet. Could be worse….
	
	
	.
	
	
	***See additional comments below:***
