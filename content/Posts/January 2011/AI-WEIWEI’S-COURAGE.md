
---
title: AI WEIWEI’S COURAGE
date: 2011-01-13 00:00:00
---

# AI WEIWEI'S COURAGE


[![](media/aiweiwei-3.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/aiweiwei-3.jpg)


Ai Weiwei, the prominent Chinese artist, whose architecture firm—Fake Design—I have criticized in a [previous post](https://lebbeuswoods.wordpress.com/2008/05/07/o-ordos/), nevertheless has my respect for his courageous stand for art and political protest. The destruction of his studio in Shanghai by government authorities must be deeply troubling for all who value intellectual and artistic freedom. It also serves as a reminder of what an authoritarian government embracing capitalism is capable of: combining the worst of both systems of manipulation and repression.


LW


.


January 12, 2011


# **Chinese Authorities Raze an Artist's Studio**


**[By Edward Wong](http://www.nytimes.com/2011/01/13/world/asia/13china.html?ref=design)** **[for the New York Times](http://www.nytimes.com/2011/01/13/world/asia/13china.html?ref=design)**


BEIJING — The studio would have stood at the heart of an embryonic arts cluster on the outskirts of Shanghai, a draw for luminaries from around the world.


It took two years to build, and one day to tear down.


An order to raze the [studio](http://www.nytimes.com/2010/11/06/world/asia/06china.html) — designed by Ai Weiwei, a protean artist who is one of the most outspoken critics of the Chinese Communist Party — was issued last July. Mr. Ai took the move to be retribution for rankling the authorities. He said officials told him that the demolition would not take place until after the first day of the Year of the Rabbit, which falls on Feb. 3.


So he was shocked to discover that workers had begun knocking it down early Tuesday, Mr. Ai said in a telephone interview from Shanghai on Wednesday. Mr. Ai said a neighboring studio he had designed for a friend had also been destroyed.


“Everything is gone,” he said. “It's all black now. They finished the job at 9 o'clock last night.”


“I called the officials and said, ‘You promised us not to take it down until after New Year's Day,' ” Mr. Ai recounted. “They said, ‘If the studio is to be taken down, it doesn't matter if it's sooner or later.' ”


Mr. Ai said that the officials might have moved ahead with their plans so that the destruction would take place without a spotlight. Neighbors of the studio called Mr. Ai's assistant on Tuesday morning when they heard heavy machinery next door. Mr. Ai said he rushed onto an airplane in Beijing, where he lives, and arrived in time to see four machines and dozens of workers toiling away on the site. About 80 percent of the structures had been destroyed by the afternoon, he said.


Shanghai city officials could not be reached on Wednesday evening for comment.


Mr. Ai's studio was to be used as an education center and a site for artists in residence. He had invited a group of university graduates from Oslo to come to the studio next month to study architecture with him.


Mr. Ai said he believed that his advocacy in two causes might have prompted Shanghai officials to order the razing. The first was that of Yang Jia, a Beijing resident who killed six policemen in a Shanghai police station after being arrested and beaten for riding an unlicensed bicycle. Mr. Yang became a hero among many Chinese, and was later executed. The second was the Kafkaesque case of Feng Zhenghu, a lawyer and activist who spent more than three months in Tokyo's Narita Airport after Shanghai officials denied him entry. Mr. Ai made a documentary about Mr. Feng's predicament.


Mr. Ai has also demanded democracy for [China](http://topics.nytimes.com/top/news/international/countriesandterritories/china/index.html?inline=nyt-geo), criticized government corruption for playing a role in the deaths of schoolchildren in the 2008 Sichuan earthquake and stridently supported [Liu Xiaobo](http://topics.nytimes.com/top/reference/timestopics/people/l/liu_xiaobo/index.html?inline=nyt-per), a political prisoner who was awarded the Nobel Peace Prize last year.


Mr. Ai said that Shanghai officials had originally supported his plan for a studio on the site, which is in a village known for its grape farms. He said he spent $1 million to transform a dilapidated warehouse into a vast working space. He began designing the building in summer 2008, and construction ended in July 2010.


Mr. Ai has come to see his escalating conflict with government officials over the Communist Party's authoritarian rule as performance art. In November, he spread the word that he was throwing a [river crab feast](http://artsbeat.blogs.nytimes.com/2010/11/03/ai-weiwei-plans-a-celebration-of-sorts-to-mark-demolition-of-his-studio/) at the studio to protest the destruction order. The word for river crab, hexie, sounds nearly identical to the word for harmony, which the Communist Party claims to promote; the party's critics like to say censors are “harmonizing” the Internet and other forms of media.


Mr. Ai was put under house arrest in Beijing two days before the feast, but about 800 people showed up at the studio anyway. “You can't imagine that in Communist history, this would happen,” he said.


[![](media/aiweiwei-11.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/aiweiwei-11.jpg)


(below) Ai Weiwei, in the ruins of his studio:


[![](media/aiweiwei-2.jpg)](https://lebbeuswoods.files.wordpress.com/2011/01/aiweiwei-2.jpg)



 ## Comments 
1. [Francisco Vasconcelos](http://csxlab.org)
1.13.11 / 6pm


One Word: RESPECT
3. John
1.13.11 / 10pm


“Laissez faire” capitalism by definition doesn't exist in countries with ‘authoritarian' governments.





	1. [lebbeuswoods](http://www.lebbeuswoods.net)
	1.13.11 / 11pm
	
	
	John: you are correct, and I have corrected the post. Thank you.
5. [chris teeter](http://www.metamechanics.com)
1.13.11 / 11pm


Another word: balls.
7. Pedro Esteban
4.3.11 / 9am


<http://www.guardian.co.uk/world/2011/apr/03/china-ai-weiwei-detained-police-beijing>
