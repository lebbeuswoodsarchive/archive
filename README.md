# A Lebbeus Woods Archive

[Visit the archive.](https://lebbeuswoodsarchive.gitlab.io/archive/)

This is an archive of Lebbeus Woods' Wordpress blog. I don't know who currently maintains it, so I decided to make a markdown archive of it, just in case. [Here's a link to his original wordpress site.](https://lebbeuswoods.wordpress.com/)

The site graph is still an experiment in progress.

To do:
[] Go through all the notes and establish links between articles
[] Expanded view for site graph
[] Different colours for site graph
